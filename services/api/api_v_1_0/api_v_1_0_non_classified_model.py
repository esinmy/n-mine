﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os
from flask_socketio import SocketIO, emit
import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from services.utils.validation import is_blank

from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, get_micromine_version
from utils.logger import  NLogger

try:
	import MMpy
except:
	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_non_classified_model (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Non Classified Model"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/NonClassifiedModel"

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('on_message_recived_handler entry')
			logger.info(input_json)

			output_json = {"models": []}

			if not 'segments' in input_json:
				raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута '{1}'".format(self.api_contract_name, "segments"))
			segments = input_json['segments']
			for	segment in segments:
				try:
					segment_id = segment['id']
					extent = segment['extent']
					extent_point_min = extent['point_min']
					extent_point_min_x =  extent_point_min['x']
					extent_point_min_y =  extent_point_min['y']
					extent_point_min_z =  extent_point_min['z']
					extent_point_max = extent['point_max']
					extent_point_max_x =  extent_point_max['x']
					extent_point_max_y =  extent_point_max['y']
					extent_point_max_z =  extent_point_max['z']
					extent_block_size = extent['block_size']
					extent_block_size_x_ = extent_block_size['_x']
					extent_block_size_y_ = extent_block_size['_y']
					extent_block_size_z_ = extent_block_size['_z']
				except Exception as e:
					raise Exception("Входящее сообщение формата '{0}' не содержит одного из обязательных атрибутов. {1}".format(self.api_contract_name, str(e)))


			
				logger.info('segment_id ')
				logger.info(segment_id )


				data = MM_Executor(self.thread)
				data.setParams(segment_id, extent_point_min_x, extent_point_min_y, extent_point_min_z, 
								extent_point_max_x, extent_point_max_y, extent_point_max_z, 
								extent_block_size_x_, extent_block_size_y_, extent_block_size_z_,
								self.api_contract_name)
				data.prepare()

				bm_header = data.bm_header
				bm_data	 = data.bm_data


				#logger.debug('bm_header')
				#logger.debug(bm_header)

				#logger.debug('bm_data')
				#logger.debug(bm_data)

				idx_east = bm_header.index(ID_EAST)
				idx_north = bm_header.index(ID_NORTH)
				idx_rl = bm_header.index(ID_RL)

				idx_east_size = bm_header.index(ID_EAST_SIZE)
				idx_north_size = bm_header.index(ID_NORTH_SIZE)
				idx_rl_size = bm_header.index(ID_RL_SIZE)

				idx_segment = bm_header.index(ID_SEGMENT)

				try:
					for record in bm_data:
						cell = {}
						cell["id"] = str(uuid4())
						cell["segment"] = record[idx_segment]
						cell["extent"] =  JsonHelper.box3d(JsonHelper.getPoint(record[idx_east], record[idx_north], record[idx_rl])
										 , record[idx_east_size], record[idx_north_size], record[idx_rl_size])

						output_json["models"].append(cell)
				except Exception as e:
					raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.api_contract_name, str(e)))

			#logger.debug('output_json')
			#logger.debug(output_json)
			callback(output_json)

		except Exception as e:
			code = 1
			message = str(e)
			json_error = {"code": str(code),
						  "error": message}
			callback(output_json, json_error)



class MM_Executor(ProcessWindow):
	def __init__(self, parent, *args, **kw):
		super(MM_Executor, self).__init__(parent, *args, **kw)
		self.__createVariables()

	@property 
	def bm_data(self):
		return self.__bm_data

	@property 
	def bm_header(self):
		return self.__bm_header
	

	def __createVariables(self):
		self.__xmin = None
		self.__ymin = None
		self.__zmin = None 
		self.__xmax = None
		self.__ymax = None 
		self.__zmax = None 
		self.__x_size = None
		self.__y_size = None 
		self.__z_size = None
		self.__contract = None
		self.__segment	= None

		self.__bm_path  = None
		self.__bm_data = None
		self.__bm_header= None

	def setParams(self, segment, xmin, ymin, zmin, xmax, ymax, zmax, x_size, y_size, z_size, contract):
		self.__segment = str(segment)
		self.__xmin = float(xmin)
		self.__ymin = float(ymin)
		self.__zmin = float(zmin)
		self.__xmax = float(xmax)
		self.__ymax = float(ymax)
		self.__zmax = float(zmax) 
		self.__x_size = float(x_size)
		self.__y_size = float(y_size)
		self.__z_size = float(z_size)
		self.__contract = str(contract)



	def __get_bm_data(self, bm_path):

		self.__bm_data = self.extractBlockModelData(bm_path, [ID_SEGMENT], [self.__segment])
		bmInFile = MicromineFile()
		if not bmInFile.open(bm_path):
			raise Exception("Невозможно открыть файл '%s'" % bmInPath)

		self.__bm_header = 	bmInFile.header
		records_count = bmInFile.records_count
		bmInFile.close()	

		if len(self.__bm_data) != records_count:
			raise Exception("Не удалось получить данные из временного файла, не соответствие количества записей в источнике и результате выборки")


	def check(self):
		if 	self.__xmin is None:
			raise Exception ("xmin is None")
		if 	self.__ymin is None:
			raise Exception ("ymin is None")
		if 	self.__zmin is None:
			raise Exception ("zmin is None")

		if 	self.__xmax is None:
			raise Exception ("xmax is None")
		if 	self.__ymax is None:
			raise Exception ("ymax is None")
		if 	self.__zmax is None:
			raise Exception ("zmax is None")

		if 	self.__x_size is None:
			raise Exception ("x_size is None")
		if 	self.__y_size is None:
			raise Exception ("y_size is None")
		if 	self.__z_size is None:
			raise Exception ("z_size is None")

		if 	is_blank(self.__segment):
			raise Exception ("Сегмент не может быть пустым значением")

		

		return True
	
				
	def prepare(self):
		if not super(MM_Executor, self).prepare():
			return

		if not self.check():
			return		


		contract_splited = self.__contract.split('/')
		if len(contract_splited) > 0:
			contract_name = contract_splited[-1]
		else:
			raise Exception ("Contract name not defined in '{0}'".format(self.contract))



		self.__bm_path = os.path.join(self._tmpfolder, self._tmpprefix + contract_name)
		self.__bm_path = self.__bm_path + ".DAT"
		self.__bm_path = self.__bm_path.upper()



		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			raise Exception ("Невозможно создать неклассифицированную блочную модель. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(MM_Executor, self).run():
			return

		major, minor, servicepack, build = get_micromine_version()
		if major > 16:
			assing_params = {}
			assing_params["bmpath"] = self.__bm_path
			assing_params["segment"] = self.__segment
			assing_params["xmax"] = self.__xmax
			assing_params["xmin"] = self.__xmin
			assing_params["ymax"] = self.__ymax
			assing_params["ymin"] = self.__ymin
			assing_params["zmax"] = self.__zmax
			assing_params["zmin"] = self.__zmin
			assing_params["xsize"] = self.__x_size
			assing_params["ysize"] = self.__y_size
			assing_params["zsize"] = self.__z_size

			create_blank_bm_form = create_blank_bm_2018(assing_params)
		else:
			raise Exception ("Micromine version not satisfied requirements")

		create_blank_bm_form.run()

		self.__get_bm_data(self.__bm_path)

	

