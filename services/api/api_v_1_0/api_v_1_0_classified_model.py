﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os
from copy import deepcopy
from utils.osutil import get_temporary_prefix
from itertools import groupby

import  shutil



from flask_socketio import SocketIO, emit
import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from services.utils.validation import is_blank

from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, Tridb, get_micromine_version
from utils.logger import  NLogger

from utils.fsettings import FormSet	


#try:
#	import MMpy
#except:
#	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_Model (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Classified Model"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/ClassifiedModel"

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('Api_v_1_0_Model - on_message_recived_handler entry')

			output_json = {"models": []}

			for attr in ['models', 'types', 'rules']:
				if not attr in input_json:
					raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута '{1}'".format(self.api_contract_name, attr))


			models = input_json['models']

			types = input_json['types']

			rules = input_json['rules']

			for key, segment_models in groupby(models, lambda x: x['segment']) :
				logger.debug('key')
				logger.debug(key)
				cm = Model(self, self.api_contract_name)
				cm.load_from_non_Model(segment_models, types, rules)
				data = MM_Executor(self.thread)
				data.setParams(cm)
				data.prepare()
				try:
					json_out = cm.dump_to_json()
					output_json['models'].extend(json_out)

				except Exception as e:
					raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.api_contract_name, str(e)))

				logger.debug('output_json')
				#logger.debug(output_json)
				#output = {'return' : 1}
				#callback(output)
			callback(output_json)

		except Exception as e:

			code = 1
			message = str(e)
			json_error = {"code": str(code),
							"error": message}
			callback(output_json,  json_error)







class Model(ProcessWindow):
	def __init__(self, parent, api_contract_name, *args, **kw):
		super(Model, self).__init__(parent, *args, **kw)
		self.__createVariables()

		self.parent = parent
		self.__contract = api_contract_name
		self._settings = None

	def __createVariables(self):
		self.__types = None
		self.__rules = None
		self.__model = None
		self.__contract = None

		self.static_bm_fields =(
					{"head": "{0}".format(ID_X), "f_head": "{0}".format(ID_EAST), "stru": "{0}:R:0:6".format(ID_EAST)}
					,{"head": "{0}".format(ID_Y), "f_head": "{0}".format(ID_NORTH), "stru": "{0}:R:0:6".format(ID_NORTH)}
					,{"head": "{0}".format(ID_Z), "f_head": "{0}".format(ID_RL), "stru": "{0}:R:0:6".format(ID_RL)}
					,{"head": "{0}".format(ID_X_SIZE), "f_head": "{0}".format(ID_EAST_SIZE), "stru": "{0}:R:0:6".format(ID_EAST_SIZE)}
					,{"head": "{0}".format(ID_Y_SIZE), "f_head": "{0}".format(ID_NORTH_SIZE), "stru": "{0}:R:0:6".format(ID_NORTH_SIZE)}
					,{"head": "{0}".format(ID_Z_SIZE), "f_head": "{0}".format(ID_RL_SIZE), "stru": "{0}:R:0:6".format(ID_RL_SIZE)}
					,{"head": "{0}".format(ID_ID), "f_head": "{0}".format(ID_ID), "stru": "{0}:C:40:0".format(ID_ID)}
					,{"head": "{0}".format(ID_SEGMENT), "f_head": "{0}".format(ID_SEGMENT), "stru": "{0}:C:15:0".format(ID_SEGMENT)}
					,{"head": "{0}".format(ID_CHANGED), "f_head": "{0}".format(ID_CHANGED), "stru": "{0}:C:1:0".format(ID_CHANGED)}
			)

		self.quality_bm_fields =()
		self.quantity_bm_fields =()

	def check_params(self):

		for	cell in self.__model:
			try:
				row_id = (cell['id'], cell['segment'])
				center = cell['extent']['center']
				row_center = (center['x'], center['y'], center['z'])
				box3d = cell['extent']
				row_size = (box3d['_x'], box3d['_y'], box3d['_z'])
				row =  row_center + row_size  + row_id
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит одного из обязательных атрибутов. {1}".format(self.__contract, str(e)))




			for type in self.__types:
				for attr1 in ['id', 'wireframe', 'quality']:
					if not attr1 in type:
						raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'types.{1}' ".format(self.__contract, attr1))

				for quality_item in type['quality']:
					for attr2 in ['id', 'name', 'value']:
						if not attr2 in quality_item:
							raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'types.quality.{1}' ".format(self.__contract, attr2))

			if not self.__rules is None:
				for rule in self.__rules:
					for attr1 in ['quality', 'quantity', 'interpolation']:
						if not attr1 in rule:
							raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'rule.{1}' ".format(self.__contract, attr1))

					#for inter in rule['interpolation']:
					#	for attr2 in ['id', 'method', 'parameters']:
					#		if not attr2 in inter:
					#			raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'rule.interpolation.{1}' ".format(self.__contract, attr2))



	@property
	def contract(self):
		return self.__contract

	@property
	def contract_name(self):
		contract_splited = self.__contract.split('/')
		if len(contract_splited) > 0:
			result = contract_splited[-1]
		else:
			raise Exception ("Contract name not defined in '{0}'".format(self.__contract))

		return result 

	def load_from_non_Model(self, model, types, rules = None):
		self.__model = model
		self.__types = types
		self.__rules = rules

		self.__in_bm_data = []
		self.__out_bm_data = []

		for	cell in self.__model:
			try:
				row_id = (cell['id'], cell['segment'])
				center = cell['extent']['center']
				row_center = (center['x'], center['y'], center['z'])
				box3d = cell['extent']
				row_size = (box3d['_x'], box3d['_y'], box3d['_z'])
				row =  row_center + row_size  + row_id
				self.__in_bm_data.append(row)
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит одного из обязательных атрибутов. {1}".format(self.__contract, str(e)))

		for type in self.__types:
			for quality_item in type['quality']:
				if not quality_item['name'] in [qual["head"] for qual in self.quality_bm_fields]:
					self.quality_bm_fields = self.quality_bm_fields	+ ({"head": quality_item['name'], "f_head": "{0}".format(quality_item['name']), "stru": "{0}:C:20:0".format(quality_item['name'])},)
				

		if not self.__rules is None:
			for rule in self.__rules:
				rule_quantity = rule['quantity']
				self.quantity_bm_fields = self.quantity_bm_fields	+ ({"head": rule_quantity['name'], "f_head": "{0}".format(rule_quantity['name']), "stru": "{0}:R:0:4".format(rule_quantity['name'])},)

	def load_from_file(self, path):
		if self.__types is None:
			raise Exception ("Не определены классифицирующие параметры")

		header = []

		bm_field_set = self.static_bm_fields + self.quality_bm_fields + self.quantity_bm_fields
		for item in bm_field_set:
			header.append(item['f_head'])

		self.__out_bm_data = self.extractAllBlockModelData(path, header)
		bmInFile = MicromineFile()
		if not bmInFile.open(path):
			raise Exception("Невозможно открыть файл '%s'" % path)

		records_count = bmInFile.records_count
		bmInFile.close()	

		if len(self.__out_bm_data) != records_count:
			raise Exception("Не удалось получить данные из временного файла, не соответствие количества записей в источнике и результате выборки")


	def dump_bm_to_file(self, path):
		if self.__in_bm_data is None:
			raise Exception("Данные неклассифицированной модели отсутствуют")
		header = []
		stru = []
		bm_field_set = self.static_bm_fields + self.quality_bm_fields# + self.quantity_bm_fields
		for item in bm_field_set:
			if item['head'] in 	header:
				continue
			header.append(item['head'])
			stru.append	(item['stru'])
		logger.debug('header')
		logger.debug(header)
		logger.debug('stru')
		logger.debug(stru)

		MicromineFile.write(path, self.__in_bm_data, header, MicromineFile.to_filestruct(stru))


	def dump_to_json(self):

		if  self.__out_bm_data is None:
			raise Exception("Данные БМ отсутствуют")
		if  self.__types is None:
			raise Exception("Данные качественных  характеристик отсутствуют")


		output_json = []

		header = []

		bm_field_set = self.static_bm_fields + self.quality_bm_fields + self.quantity_bm_fields
		for item in bm_field_set:
			header.append(item['f_head'])

		idx_east = header.index(ID_EAST)
		idx_north = header.index(ID_NORTH)
		idx_rl = header.index(ID_RL)

		idx_east_size = header.index(ID_EAST_SIZE)
		idx_north_size = header.index(ID_NORTH_SIZE)
		idx_rl_size = header.index(ID_RL_SIZE)

		idx_segment = header.index(ID_SEGMENT)

		idx_changed = header.index(ID_CHANGED)
		idx_parent = header.index(ID_ID)


		for type in self.__types:
			for quality_item in type['quality']:
				quality_item['idx'] = header.index(quality_item['name'])


		if not self.__rules is None:
			for rule in self.__rules:
				rule_quantity = rule['quantity']
				rule_quantity['idx'] = header.index(rule_quantity['name'])




		try:

			changed_guids = []
			for record in self.__out_bm_data:
				if record[idx_changed] == 'Y':
					changed_guids.append(record[idx_parent])

			changed_guids = set(changed_guids)


			for record in self.__out_bm_data:

				#if record[idx_parent] == '2615c9ca-bfcd-4022-9085-93078b32ac39':
				#	logger.debug('header')
				#	logger.debug(header)
				#	logger.debug('record')
				#	logger.debug(record)


				cell = {}
				if record[idx_parent] in  changed_guids:
					cell["id"] = str(uuid4())
					cell['parent'] = JsonHelper.not_classified_model(id = record[idx_parent], segment = JsonHelper.segment( id = record[idx_segment]))
					
					
				else:
					cell["id"] = record[idx_parent]
					cell['parent'] = JsonHelper.not_classified_model(id = cell["id"], segment = JsonHelper.segment( id = record[idx_segment]))

				cell['extent'] =  JsonHelper.box3d(JsonHelper.getPoint(record[idx_east], record[idx_north], record[idx_rl])
									, record[idx_east_size], record[idx_north_size], record[idx_rl_size])

				quality_array = []
				for type in self.__types:
					for quality_item in type['quality']:
						if not quality_item["name"] in [ qual["name"] for qual in  quality_array]: 
							bm_quality_item = deepcopy(quality_item)
							del bm_quality_item['idx']
							bm_quality_item['value'] =  record[quality_item['idx']]
							quality_array.append(bm_quality_item)


				quantity_array = []
				if not self.__rules is None:
					for rule in self.__rules:
						rule_quantity =  rule['quantity']
						bm_rule_quantity = deepcopy(rule_quantity)
						del bm_rule_quantity['idx']
						bm_rule_quantity['value'] =  record[rule_quantity['idx']]
						quantity_array.append(bm_rule_quantity)

				#logger.debug('quantity_array')
				#logger.debug(quantity_array)

				cell['quality'] = quality_array
				cell['quantity'] = quantity_array

				output_json.append(cell)

		except Exception as e:
			raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.__contract, str(e)))

		return output_json

	def dump_wfs_to_files(self):
		if  self.__types is None:
			raise Exception("Данные качественных  характеристик отсутствуют")

		for type in self.__types:
			temp_wf_file = os.path.join(self._tmpfolder, get_temporary_prefix() + "type_acc_obj.tridb")
			temp_wf_file = temp_wf_file.upper()
			type['temp_wf_file'] = temp_wf_file
			shutil.copyfile(type['wireframe'], temp_wf_file)

			dsttri = Tridb(temp_wf_file)
			dsttri.clear_user_attributes()
			for quality_item in type['quality']:
				dsttri.add_attribute(quality_item['name'])
				for dstname in dsttri.wireframe_names:
					#logger.debug("replace slashes")
					#logger.debug(quality_item['value'])
					#logger.debug("replace slashes")
					#logger.debug(quality_item['value'].replace('\\\\','\\'))
					#dsttri[dstname].set_user_attribute(quality_item['name'], quality_item['value'].replace('\\\\','\\').encode('latin1').decode('utf-8-sig'))
					dsttri[dstname].set_user_attribute(quality_item['name'], quality_item['value'].encode('utf-8').decode('utf8'))
					dsttri.commit()

			dsttri.add_attribute(ID_CHANGED)
			for dstname in dsttri.wireframe_names:
				dsttri[dstname].set_user_attribute(ID_CHANGED, 'Y')
				dsttri.commit()

			if not self.__rules is None:
				dsttri.add_attribute(BMFLD_SYS_DIR)
				dsttri.add_attribute(BMFLD_SYS_DIP)
				dsttri.add_attribute(BMFLD_SYS_ROT)

				for dstname in dsttri.wireframe_names:
					dsttri[dstname].set_user_attribute(BMFLD_SYS_DIR, '0')
					dsttri[dstname].set_user_attribute(BMFLD_SYS_DIP, '0')
					dsttri[dstname].set_user_attribute(BMFLD_SYS_ROT, '0')
					dsttri.commit()



			dsttri.close()

	def classify_bm_file(self, path):

		if  self.__types is None:
			raise Exception("Данные качественных  характеристик отсутствуют")


		for type in self.__types:
			wfdata = []

			attributes = []

			temp_wf_file = type['temp_wf_file'] 
			dsttri = Tridb(temp_wf_file)


			for wfname in dsttri.wireframe_names:
				wfdata.append([temp_wf_file, DEFAULT_ATTR_NAME, wfname])




			for quality_item in type['quality']:
				attributes.append([0, quality_item['name'], quality_item['name']] )

			attributes.append([0, ID_CHANGED, ID_CHANGED] )

			if not self.__rules is None:
				attributes.append([0, BMFLD_SYS_DIR, BMFLD_SYS_DIR] )
				attributes.append([0, BMFLD_SYS_DIP, BMFLD_SYS_DIP] )
				attributes.append([0, BMFLD_SYS_ROT, BMFLD_SYS_ROT] )

			#logger.debug('wfdata')
			#logger.debug(wfdata)
			wfset = create_wireframe_set(wfdata)

			assing_params = {}
			assing_params["attributes"] = attributes
			assing_params["sub_xsize"] = 8
			assing_params["sub_ysize"] = 8
			assing_params["sub_zsize"] = 4
			assing_params["overwrite_bool"] = True
			assing_params["purge_bool"] = False
			assing_params["wireframe_set"] = wfset
			assing_params["bmpath"] = path

			assign_wireframes (assing_params)

	def interpolate_bm_file(self, path):

		if  self.__rules is None:
			raise Exception("Данные количественных характеристик отсутствуют")


		self.readSettings()
		#logger.debug('self._settings3')
		#logger.debug(self._settings)

		locBmPath = path

		self._wrkpath = self._settings.getValue(ID_NETPATH)
		netExpAssayPath = os.path.join(self._wrkpath, self._settings.getValue(ID_DBDIR), self._settings.getValue(ID_EXPASSAY))
		locExpAssayPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netExpAssayPath))

		shutil.copyfile(netExpAssayPath, locExpAssayPath)


		indexFld = self._settings.getValue(ID_IDXFLD)


 
		idwParamIds = (ID_IDWSECTOR, ID_IDWMINPTS, ID_IDWMAXPTS, ID_IDWFACTOR1, ID_IDWFACTOR2, ID_IDWFACTOR3, ID_IDWRADIUS)
		idwParams = {paramId: self._settings.getValue(paramId) for paramId in idwParamIds} 

		#logger.debug('idwParams[ID_IDWRADIUS]')
		#logger.debug(idwParams[ID_IDWRADIUS])



		getGeoCodes = list(zip(*self._settings.getValue(ID_GEOCODES)))[0]

		codes = list(filter(lambda x: x[-1] == 2, getGeoCodes))
		emptyCode = None if not codes else codes[0][0]




		for rule in self.__rules:
			rule_quantity = rule['quantity']
			element = rule_quantity['name']


			oreCodes, oreTypes, oreIndexes = [], [], []
			for (oreCode, oreDescription, oreType), assayAndBmIndexes in self._settings.getValue(ID_GEOCODES):
				if oreCode == emptyCode or oreType > 2:
					continue
				# if not oreType in [0,1]:
				# 	continue
				#
				oreCodes.append(oreCode)
				oreTypes.append(oreType)
				oreIndexes.append([row[0] for row in assayAndBmIndexes["ASSAY"]])

				oreIndexesDict = dict(zip(oreCodes, oreIndexes))

				for radius in idwParams[ID_IDWRADIUS]:
					err = self.exec_mmproc(idw, locBmPath, locExpAssayPath, indexFld, oreCode, element, radius[0], oreIndexesDict[oreCode], BMFLD_SYS_DIR, BMFLD_SYS_ROT, BMFLD_SYS_DIP,
							idwParams[ID_IDWMINPTS], idwParams[ID_IDWMAXPTS], idwParams[ID_IDWSECTOR], idwParams[ID_IDWFACTOR1], idwParams[ID_IDWFACTOR2], idwParams[ID_IDWFACTOR3])
					if err:						print ("Ошибка. %s" % err)
					if self.is_cancelled:		return
				if self.is_cancelled:		return
		if self.is_cancelled:		return

	def readSettings(self):
		if self._settings:
			self._settings.close()
		try:		
			self._settings = FormSet(ID_SETTINGS)
		except Exception as e:	raise Exception("Невозможно считать настройки. %s" % str(e))
		self._settings.open(0)
		#
		if not self._settings.values():
			raise Exception("Невозможно считать настройки. %s" % str(e)) 


class MM_Executor(ProcessWindow):
	def __init__(self, parent, *args, **kw):
		super(MM_Executor, self).__init__(parent, *args, **kw)
		self.__createVariables()

	

	def __createVariables(self):
		self.__bm_path = None


	def setParams(self, cm_object):
		self.__cm_object = cm_object

	def check(self):
		self.__cm_object.check_params()
		return True
	
				
	def prepare(self):
		if not super(MM_Executor, self).prepare():
			return

		if not self.check():
			return		

		self.__bm_path = os.path.join(self._tmpfolder, self._tmpprefix + self.__cm_object.contract_name)
		self.__bm_path = self.__bm_path + ".DAT"
		self.__bm_path = self.__bm_path.upper()

		

		
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			raise Exception ("Невозможно создать неклассифицированную блочную модель. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(MM_Executor, self).run():
			return

		major, minor, servicepack, build = get_micromine_version()
		if major > 16:	 
			
			self.__cm_object.dump_bm_to_file(self.__bm_path)
			#self.__cm_object.dump_wfs_to_files()
			#self.__cm_object.classify_bm_file(self.__bm_path)
			self.__cm_object.interpolate_bm_file (self.__bm_path)
			self.__cm_object.load_from_file(self.__bm_path)

		else:
			raise Exception ("Micromine version not satisfied requirements")

	


