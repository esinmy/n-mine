﻿class JsonHelper():
	@staticmethod
	def getPoint( x, y, z):
		result = {}
		result['x'] = float(x)		
		result['y'] = float(y)		
		result['z'] = float(z)		
		return result

	@staticmethod
	def box3d( point, _x, _y, _z):
		result = {}
		result['center'] = point		
		result['_x'] = float(_x)		
		result['_y'] = float(_y)		
		result['_z'] = float(_z)		
		return result

	@staticmethod
	def segment( id, extent = None):
		result = {}
		result['id'] = id		
		result['extent'] = extent		
		
		return result

	@staticmethod
	def quality_feature( id, name, value = None):
		result = {}
		result['id'] = id		
		result['name'] = name		
		result['value'] = value		
		
		return result

	@staticmethod
	def not_classified_model( id, segment, extent = None):
		result = {}
		result['id'] = id		
		result['segment'] = segment		
		result['extent'] = extent		
		
		return result
