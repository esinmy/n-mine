﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os
from flask_socketio import SocketIO, emit
import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from utils.constants import *
from services.utils.validation import is_blank , is_number
import re
from datetime import datetime
from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, get_micromine_version	,Tridb
from utils.logger import  NLogger

try:
	import MMpy
except:
	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_visualization (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Visualization"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/TestMicromine/show"  

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('on_message_recived_handler entry')

			output_json = False

			_room = input_json["room"]
			if _room is None:
				raise "Не определён room"

			_body = input_json["body"]
			if _body is None:
				_body = []
			_input_files = 	input_json["body"]



			if not isinstance(_input_files, list):
				raise Exception("Входящее сообщение формата '{0}' не является списком'".format(self.api_contract_name))




			data = MM_Executor(self.thread)
			data.setParams(_input_files,
							self.api_contract_name)
			data.prepare()

			output_json = True
			callback(output_json, room = _room)

		except Exception as e:
			code = 1
			message = str(e)
			json_error = {"code": str(code),
						  "error": message}
			callback(output_json, json_error, room = _room)



class MM_Executor(ProcessWindow):
	def __init__(self, parent, *args, **kw):
		super(MM_Executor, self).__init__(parent, *args, **kw)
		self.__createVariables()



	def __createVariables(self):
		self.__tridbFilePath = None

	def setParams(self, files, contract):
		self.__files = files
		self.__contract = str(contract)




	def check(self):

		return True
	
				
	def prepare(self):
		if not super(MM_Executor, self).prepare():
			return

		if not self.check():
			return		


		contract_splited = self.__contract.split('/')
		if len(contract_splited) > 0:
			contract_name = contract_splited[-1]
		else:
			raise Exception ("Contract name not defined in '{0}'".format(self.contract))

		self.__tridbFilePath = os.path.join(self._tmpfolder, self._tmpprefix + contract_name + ".tridb")






		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			raise Exception ("Невозможно визуализировать объекты. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(MM_Executor, self).run():
			return


		self.runVisioWireframes()

		self.runVisioBlockmodels()

		self.runVisioString()

		self.runVisioPoint()


	def runVisioWireframes(self):

		Tridb.create(self.__tridbFilePath)

		tridb = Tridb(self.__tridbFilePath)

		tridb.clear_user_attributes()

		wfdata = []
		u_attributes = []

		def_attributes_dict = {"Name" : "MINEPRO_ИМЯ", "Title" : "MINEPRO_ИМЯ_СЛОЯ", "CreateDateTime":"MINEPRO_ДАТА_ДОБАВЛЕНИЯ", "CreateBy": "MINEPRO_АВТОР", "EditDateTime": "MINEPRO_fileLastUpdated"
				   , "XMinimum" : "MINEPRO_X_MIN", "XMaximum" : "MINEPRO_X_MAX", "YMinimum" : "MINEPRO_Y_MIN", "YMaximum" : "MINEPRO_Y_MAX", "ZMinimum" : "MINEPRO_Z_MIN", "ZMaximum" : "MINEPRO_Z_MAX"}


		def_attributes_dict_rev = {value:key for key, value in 	def_attributes_dict.items()}

		wf_set_dict = {}

		for file in  self.__files:

			if file['type'] == "Каркас":

				path = file['pathOnServer'].replace('\\\\','\\').encode('utf-8').decode('utf-8-sig')
				fileName = os.path.basename(path)

				if not 'params' in file:
					raise Exception("Отсутствуют параметры файла")
				params = file['params']

				wf_set_dict_key = (file['treePath'], file['formId'])

				with open(path, mode='rb') as fileDescriptor :

					fileContent = fileDescriptor.read()
					tridb.add_wireframe(params['MINEPRO_ИМЯ'], fileContent)


				for param_name in params:
					if not param_name in def_attributes_dict_rev and not param_name in u_attributes:
						tridb.add_attribute(param_name)
						u_attributes.append	(param_name)

					param_value_original = params[param_name]
					param_value = None
					if isinstance(param_value_original, float):
						param_value =  param_value_original
					if isinstance(param_value_original, int):
						param_value =  param_value_original
					elif isinstance(param_value_original, str):
						param_value =  param_value_original.encode('utf-8').decode('utf8')

						re_result = re.search("\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d\{2}.\d{3}",param_value)
						logger.debug("re_result")
						logger.debug(re_result)
						if 	not re_result is None:
							date_param_value = datetime.strptime(re_result.group(0), "%Y-%m-%dT%H:%M:%S.%f")
							param_value = date_param_value.timestamp()



					if param_name in def_attributes_dict_rev:
						tridb[params['MINEPRO_ИМЯ']].set_default_attribute (def_attributes_dict_rev[param_name], param_value)
					else:	
						tridb[params['MINEPRO_ИМЯ']].set_user_attribute(param_name, param_value)
					tridb.commit()

				if not wf_set_dict_key in wf_set_dict:
					wf_set_dict[wf_set_dict_key] = {'wfdata' : []}

				wf_set_dict[wf_set_dict_key]['wfdata'].append([self.__tridbFilePath, DEFAULT_ATTR_NAME, params['MINEPRO_ИМЯ']])

		for wf_set_dict_key in wf_set_dict:
			wfset = create_wireframe_set(wf_set_dict[wf_set_dict_key]['wfdata'])
			wf_set_dict[wf_set_dict_key]['wfset'] = wfset

		major, minor, servicepack, build = get_micromine_version()
		if major > 16:
			for wf_set_dict_key in wf_set_dict:
				treePath, _formId = wf_set_dict_key
				
				formId = None
				if is_number(_formId):
					formId = int(_formId)

				loadTriangulationSet(wf_set_dict[wf_set_dict_key]['wfset'], treePath, formId)
		else:
			raise Exception ("Micromine version not satisfied requirements")

	

	def runVisioBlockmodels(self):
		
		for file in  self.__files:

			
			if file['type'] == "Блочная модель":
				path = file['pathOnServer'].replace('\\\\','\\').encode('utf-8').decode('utf-8-sig')
				fileName = os.path.basename(path)
				treepath = file['treePath']
				formId = None
				if is_number(file['formId']):
					formId = int(file['formId'])


				if not 'params' in file:
					raise Exception("Отсутствуют параметры файла")
				params = file['params']

				loadBlockModel(path, treepath, formId)

	def runVisioString(self):
		
		for file in  self.__files:

			
			if file['type'] == "Стринг":
				path = file['pathOnServer'].replace('\\\\','\\').encode('utf-8').decode('utf-8-sig')
				fileName = os.path.basename(path)
				treepath = file['treePath']
				formId = None
				if is_number(file['formId']):
					formId = int(file['formId'])


				if not 'params' in file:
					raise Exception("Отсутствуют параметры файла")
				params = file['params']

				logger.debug("path")
				logger.debug(path)

				logger.debug("formId")
				logger.debug(formId)

				loadStrings(path, treepath, formId)

	def runVisioPoint(self):
		
		for file in  self.__files:

			
			if file['type'] == "Точки":
				path = file['pathOnServer'].replace('\\\\','\\').encode('utf-8').decode('utf-8-sig')
				fileName = os.path.basename(path)
				treepath = file['treePath']
				formId = None
				if is_number(file['formId']):
					formId = int(file['formId'])


				if not 'params' in file:
					raise Exception("Отсутствуют параметры файла")
				params = file['params']

				logger.debug("path")
				logger.debug(path)

				logger.debug("formId")
				logger.debug(formId)

				loadPoints(path, treepath, formId)



		
