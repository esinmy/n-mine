﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os
from copy import deepcopy
from utils.osutil import get_temporary_prefix
from itertools import groupby

import  shutil
from mm.mmutils import Tridb



import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from services.utils.validation import is_blank

from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, get_micromine_version
from utils.logger import  NLogger

from utils.fsettings import FormSet	


try:
	import MMpy
except:
	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_Model (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Test Micromine"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/TestMicromine/micro"

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('Api_v_1_0_TestMicromine - on_message_recived_handler entry')

			_room = input_json["room"]
			if _room is None:
				raise "Не определён room"
			
			callback({'result':'ok'}, room = _room)

		except Exception as e:

			code = 1
			message = str(e)
			json_error = {"code": str(code),
							"error": message}
			callback(False,  json_error, room = _room)





	


