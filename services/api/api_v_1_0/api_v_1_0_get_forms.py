﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os, sqlite3
from copy import deepcopy
from datetime import datetime
from utils.osutil import get_temporary_prefix
from itertools import groupby
from utils.constants import *
import  shutil



import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from services.utils.validation import is_blank

from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, Tridb, get_micromine_version, get_fldval_path
from utils.logger import  NLogger

from utils.fsettings import FormSet	

import time

#try:
#	import MMpy
#except:
#	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_GetForms (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Get Forms"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/TestMicromine/GetForms"

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('GetForms - on_message_recived_handler entry')

			_room = input_json["room"]
			if _room is None:
				raise "Не определён room"

			_body = input_json["body"]
			if _body is None:
				_body = []
			

			output_json = []

			data = MM_Executor(self.thread)
			data.setParams(_body)
			data.prepare()
			try:
				json_out = data.dump_to_json()
				output_json.extend(json_out)

			except Exception as e:
				raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.api_contract_name, str(e)))


			#logger.debug('_output_json_file')
			#logger.debug(_output_json_file)



			callback(output_json, room = _room)

		except Exception as e:

			code = 1
			message = str(e)
			json_error = {"code": str(code),
							"error": message}
			callback(output_json,  json_error, room = _room)




class MM_Executor(ProcessWindow):
	def __init__(self, parent, *args, **kw):
		super(MM_Executor, self).__init__(parent, *args, **kw)
		self.__createVariables()

	

	def __createVariables(self):  
		self._forms = {}
		self._cursor = None
		self._cmds = None



	def setParams(self, cm_object):
		self._cmds =  [ i.encode('utf-8').decode('utf-8-sig') for i in cm_object]


	def connect(self, dbpath):
		if not os.path.exists(dbpath):
			raise FileNotFoundError("Файл '%s' не существует" % dbpath)
		cnxn = sqlite3.connect(dbpath)
		self._cursor = cnxn.cursor()

	def disconnect(self):
		self._cursor.connection.close()

	def load(self, cmdId ):
		if not cmdId is None:
			
			setCmd = """
			select
				CASE
					WHEN F.path = '' or F.path is NULL
					THEN P.title
					ELSE F.path || '|' || P.title
				END Заголовок,
				P.setId ID,
				P.szEditAuthor [Изменен],
				P.lEditDateTime [Дата изменения],
				P.szCreateAuthor [Создан],
				P.lCreateDateTime [Дата создания],
				P.cmdId [cmdId]
			from PRMSETS P left join FOLDERS F on P.cmdId = F.cmdId and P.folderId = F.folderId
			where P.cmdId = %d and P.setId != 0
			order by P.setId
			""" % cmdId


		#
		formsets = []
		self._cursor.execute(setCmd)
		for row in self._cursor:
			row = list(row)
			path = str(row[0])
			row[2] = row[2] if row[2] is not None else ""
			row[3] = datetime.fromtimestamp(row[3]) if row[3] is not None else ""
			row[5] = datetime.fromtimestamp(row[5]) if row[5] is not None else ""
			formsets.append(row)

		return 	formsets




	def check(self):
		if self._cmds is None:
			raise Exception ("Не установлены параметры")

		return True
	
	def dump_to_json(self) :

		result = []
		for key in self._forms:
			forms = {}
			forms["type"] = key
			forms["forms"] = []

			for _form in self._forms[key]:
				form = {}
				form["id"] = _form[1]
				form["path"] = _form[0]
				form["editAuthor"] = _form[2]
				form["editDate"]  = str(_form[3])
				form["createAuthor"]  = _form[4] 
				form["createDate"] = str(_form[5])

				forms["forms"].append(form)


			result.append(forms)

		return result
				
	def prepare(self):
		if not super(MM_Executor, self).prepare():
			return

		if not self.check():
			return		
		
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			raise Exception ("Невозможно получить данные. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(MM_Executor, self).run():
			return

		FORMSET_COMMAND = {"Блочная модель" : MMTYPE_BM, "Каркас" : MMTYPE_TRIDB, "Файл" : MMTYPE_DATA, "Стринг" : MMTYPE_STRING}

		notFoundCommands = set(self._cmds) - set (FORMSET_COMMAND.keys())

		if 	len(notFoundCommands) > 0:
			raise Exception ("Комманды ({0}) отсутствуют в списке допустимых комманд ({1})".format(",".join(notFoundCommands), ",".join(list(FORMSET_COMMAND.keys())) ))

		self.connect(get_fldval_path())

		for	 key in FORMSET_COMMAND:
			if len(self._cmds) > 0 and not key	in self._cmds:
				continue
			forms = self.load(MMTYPE_COMMAND_ID_DICT[FORMSET_COMMAND[key]])
			self._forms[key]  = forms

		self.disconnect()





