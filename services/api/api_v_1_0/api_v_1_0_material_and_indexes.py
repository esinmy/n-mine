﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os
from copy import deepcopy
from utils.osutil import get_temporary_prefix
from itertools import groupby

import  shutil



from flask_socketio import SocketIO, emit
import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from services.utils.validation import is_blank

from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, Tridb, get_micromine_version
from utils.logger import  NLogger

from utils.fsettings import FormSet	

import time

#try:
#	import MMpy
#except:
#	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_MaterialAndIndexes (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Material and Indexes codes"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/TestMicromine/MaterialAndIndexes"

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('MaterialAndIndexes - on_message_recived_handler entry')

			_room = input_json["room"]
			if _room is None:
				raise "Не определён room"
			

			output_json = []

			data = MM_Executor(self.thread)
			data.prepare()
			try:
				json_out = data.dump_to_json()
				output_json.extend(json_out)

			except Exception as e:
				raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.api_contract_name, str(e)))


			#logger.debug('_output_json_file')
			#logger.debug(_output_json_file)



			callback(output_json, room = _room)

		except Exception as e:

			code = 1
			message = str(e)
			json_error = {"code": str(code),
							"error": message}
			callback(_output_json_file,  json_error, room = _room)




class MM_Executor(ProcessWindow):
	def __init__(self, parent, *args, **kw):
		super(MM_Executor, self).__init__(parent, *args, **kw)
		self.__createVariables()

	

	def __createVariables(self):  
		self._settings = None
		self.oreIndexesDict = None



	def setParams(self, cm_object):
		pass

	def check(self):
		return True
	
	def dump_to_json(self) :

		result = []
		for key in self.oreIndexesDict:
			materialAndIndexes = {}
			materialAndIndexes["material"] = key
			materialAndIndexes["indexes"] = self.oreIndexesDict[key]
		
			result.append(materialAndIndexes)

		return result
				
	def prepare(self):
		if not super(MM_Executor, self).prepare():
			return

		if not self.check():
			return		
		
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			raise Exception ("Невозможно интерполировать блочную модель. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(MM_Executor, self).run():
			return

		major, minor, servicepack, build = get_micromine_version()
		if major > 16:	 
			
			self.readSettings()

			oreCodes, oreTypes, oreIndexes = [], [], []
			for (oreCode, oreDescription, oreType), assayAndBmIndexes in self._settings.getValue(ID_GEOCODES):
				#if oreCode == emptyCode or oreType > 2:
				#	continue

				oreCodes.append (oreCode)
				oreIndexes.append([row[0][0] for row in assayAndBmIndexes["BM"]])

			self.oreIndexesDict = dict(zip(oreCodes, oreIndexes))

			self.oreIndexesDict['ЗАКЛ'] = ['ЗАКЛ', 'СУХЗ']

		else:
			raise Exception ("Micromine version not satisfied requirements")

	
	def readSettings(self):
		if self._settings:
			self._settings.close()
		try:		
			self._settings = FormSet(ID_SETTINGS)
		except Exception as e:	raise Exception("Невозможно считать настройки. %s" % str(e))
		self._settings.open(0)
		#
		if not self._settings.values():
			raise Exception("Невозможно считать настройки. %s" % str(e)) 


