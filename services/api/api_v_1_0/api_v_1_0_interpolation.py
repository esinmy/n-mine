﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os
from copy import deepcopy
from utils.osutil import get_temporary_prefix
from itertools import groupby

import  shutil



from flask_socketio import SocketIO, emit
import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from services.utils.validation import is_blank

from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, Tridb, get_micromine_version
from utils.logger import  NLogger

from utils.fsettings import FormSet	

import time
from random import randint


#try:
#	import MMpy
#except:
#	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_Interpolation (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Interpolation Model"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/TestMicromine/interpolation"

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('Api_v_1_0_Interpolation - on_message_recived_handler entry')

			_output_json_file = None

			_room = input_json["room"]
			if _room is None:
				raise "Не определён room"
			_input_json_file = 	input_json["body"]

			with open(_input_json_file, encoding='utf-8-sig') as in_file:
				input_json = json.load(in_file)

			output_json = {"models": []}

			for attr in ['models',  'rules']:
				if not attr in input_json:
					raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута '{1}'".format(self.api_contract_name, attr))


			models = input_json['models']

			rules = input_json['rules']

			cm = Model(self, self.api_contract_name)
			cm.load_from_non_Model(models, rules)
			data = MM_Executor(self.thread)
			data.setParams(cm)
			data.prepare()
			try:
				json_out = cm.dump_to_json()
				output_json['models'].extend(json_out)

			except Exception as e:
				raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.api_contract_name, str(e)))

			_output_json_file = os.path.splitext(_input_json_file)[0] + ".out"

			#logger.debug('_output_json_file')
			#logger.debug(_output_json_file)

			#time.sleep(60)

			with open(_output_json_file, "w") as out_file:
				json.dump(output_json, out_file)

			callback(_output_json_file, room = _room)

		except Exception as e:

			code = 1
			message = str(e)
			json_error = {"code": str(code),
							"error": message}
			callback(_output_json_file,  json_error, room = _room)







class Model(ProcessWindow):
	def __init__(self, parent, api_contract_name, *args, **kw):
		super(Model, self).__init__(parent, *args, **kw)
		self.__createVariables()

		self.parent = parent
		self.__contract = api_contract_name
		self._settings = None

	def __createVariables(self):
		self.__rules = None
		self.__model = None
		self.__contract = None

		self.static_bm_fields =(
					{"head": "{0}".format(ID_X), "f_head": "{0}".format(ID_EAST), "stru": "{0}:R:0:6".format(ID_EAST)}
					,{"head": "{0}".format(ID_Y), "f_head": "{0}".format(ID_NORTH), "stru": "{0}:R:0:6".format(ID_NORTH)}
					,{"head": "{0}".format(ID_Z), "f_head": "{0}".format(ID_RL), "stru": "{0}:R:0:6".format(ID_RL)}
					,{"head": "{0}".format(ID_X_SIZE), "f_head": "{0}".format(ID_EAST_SIZE), "stru": "{0}:R:0:6".format(ID_EAST_SIZE)}
					,{"head": "{0}".format(ID_Y_SIZE), "f_head": "{0}".format(ID_NORTH_SIZE), "stru": "{0}:R:0:6".format(ID_NORTH_SIZE)}
					,{"head": "{0}".format(ID_Z_SIZE), "f_head": "{0}".format(ID_RL_SIZE), "stru": "{0}:R:0:6".format(ID_RL_SIZE)}
					,{"head": "{0}".format(ID_ID), "f_head": "{0}".format(ID_ID), "stru": "{0}:C:40:0".format(ID_ID)}

			)

		self.quality_bm_fields =()
		self.quantity_bm_fields =()

	def check_params(self):

		for	cells in self.__model:
			try:
				
				for attr1 in ['quality', 'blocks']:
					if not attr1 in cells:
						raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута '{1}' ".format(self.__contract, attr1))


				for	cell in cells["blocks"]:
					row_id = (cell["id"],)
					center = cell['extent']['center']
					row_center = (center['x'], center['y'], center['z'])
					box3d = cell['extent']
					row_size = (box3d['_x'], box3d['_y'], box3d['_z'])
					row =  row_center + row_size  + row_id
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит одного из обязательных атрибутов. {1}".format(self.__contract, str(e)))


			if not self.__rules is None:
				for rule in self.__rules:
					for attr1 in ['quality', 'quantity', 'interpolation']:
						if not attr1 in rule:
							raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'rule.{1}' ".format(self.__contract, attr1))

					#for inter in rule['interpolation']:
					#	for attr2 in ['id', 'method', 'parameters']:
					#		if not attr2 in inter:
					#			raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'rule.interpolation.{1}' ".format(self.__contract, attr2))



	@property
	def contract(self):
		return self.__contract

	@property
	def contract_name(self):
		contract_splited = self.__contract.split('/')
		if len(contract_splited) > 0:
			result = contract_splited[-1]
		else:
			raise Exception ("Contract name not defined in '{0}'".format(self.__contract))

		return result 

	def load_from_non_Model(self, model,  rules = None):
		self.__model = model
		self.__rules = rules

		self.__in_bm_data = []
		self.__out_bm_data = []

		try:
			for	cells in self.__model:

				for quality_item in cells["quality"]:
						if not quality_item['name'] in [qual["head"] for qual in self.quality_bm_fields]:
							self.quality_bm_fields = self.quality_bm_fields	+ ({"head": quality_item['name'], "f_head": "{0}".format(quality_item['name']), "stru": "{0}:C:20:0".format(quality_item['name'])},)		

				quality_row_items = ()
				for quality_item in self.quality_bm_fields	:
					if quality_item["head"] in [qual["name"] for qual in cells["quality"]]:
						quality_model_item = list(filter(lambda x: x["name"] == quality_item["head"],  cells["quality"]))[0] 
						quality_row_items = quality_row_items + (quality_model_item["value"],)  
					else:
						quality_row_items = quality_row_items + (None,)

				for cell in cells["blocks"]:

					row_id = (cell["id"],)
					center = cell['extent']['center']
					row_center = (center['x'], center['y'], center['z'])
					box3d = cell['extent']
					row_size = (box3d['_x'], box3d['_y'], box3d['_z'])
					row =  row_center + row_size  + row_id	+ quality_row_items


					self.__in_bm_data.append(row)

					### Удалить
					self.__out_bm_data.append(row)

					### Удалить
		except Exception as e:
			raise Exception("Входящее сообщение формата '{0}' не содержит одного из обязательных атрибутов. {1}".format(self.__contract, str(e)))

		if not self.__rules is None:
			for rule in self.__rules:
				rule_quantity = rule['quantity']
				self.quantity_bm_fields = self.quantity_bm_fields	+ ({"head": rule_quantity['name'], "f_head": "{0}".format(rule_quantity['name']), "stru": "{0}:R:0:4".format(rule_quantity['name'])},)

	def load_from_file(self, path):

		header = []

		bm_field_set = self.static_bm_fields + self.quality_bm_fields + self.quantity_bm_fields
		for item in bm_field_set:
			header.append(item['f_head'])

		self.__out_bm_data = self.extractAllBlockModelData(path, header)
		bmInFile = MicromineFile()
		if not bmInFile.open(path):
			raise Exception("Невозможно открыть файл '%s'" % path)

		records_count = bmInFile.records_count
		bmInFile.close()	

		if len(self.__out_bm_data) != records_count:
			raise Exception("Не удалось получить данные из временного файла, не соответствие количества записей в источнике и результате выборки")


	def dump_bm_to_file(self, path):
		if self.__in_bm_data is None:
			raise Exception("Данные неклассифицированной модели отсутствуют")
		header = []
		stru = []



		bm_field_set = self.static_bm_fields + self.quality_bm_fields +  self.quantity_bm_fields
		for item in bm_field_set:
			if item['head'] in 	header:
				continue
			header.append(item['head'])
			stru.append	(item['stru'])

		header.append (BMFLD_SYS_DIR)
		header.append (BMFLD_SYS_DIP)
		header.append (BMFLD_SYS_ROT)

		stru.append("{0}:R:0:6".format(BMFLD_SYS_DIR))
		stru.append("{0}:R:0:6".format(BMFLD_SYS_DIP))
		stru.append("{0}:R:0:6".format(BMFLD_SYS_ROT))


		#logger.debug('header')
		#logger.debug(header)
		#logger.debug('stru')
		#logger.debug(stru)


		MicromineFile.write(path, self.__in_bm_data, header, MicromineFile.to_filestruct(stru))


	def dump_to_json(self):

		if  self.__out_bm_data is None:
			raise Exception("Данные БМ отсутствуют")

		output_json = []

		header = []

		bm_field_set = self.static_bm_fields + self.quality_bm_fields + self.quantity_bm_fields
		for item in bm_field_set:
			header.append(item['f_head'])

		idx_east = header.index(ID_EAST)
		idx_north = header.index(ID_NORTH)
		idx_rl = header.index(ID_RL)
		idx_east_size = header.index(ID_EAST_SIZE)
		idx_north_size = header.index(ID_NORTH_SIZE)
		idx_rl_size = header.index(ID_RL_SIZE)
		idx_parent = header.index(ID_ID)

		if not self.__rules is None:
			for rule in self.__rules:
				rule_quantity = rule['quantity']
				rule_quantity['idx'] = header.index(rule_quantity['name'])

		try:

			for record in self.__out_bm_data:
				cell = {}
				cell["block"] = {}
	
				cell["block"]["id"] = record[idx_parent]

				#cell["block"]["extent"] =  JsonHelper.box3d(JsonHelper.getPoint(record[idx_east], record[idx_north], record[idx_rl])
				#					, record[idx_east_size], record[idx_north_size], record[idx_rl_size])

				quantity_array = []
				if not self.__rules is None:
					for rule in self.__rules:
						rule_quantity =  rule['quantity']
						bm_rule_quantity = deepcopy(rule_quantity)
						del bm_rule_quantity['idx']
						try:
							bm_rule_quantity['value'] =  record[rule_quantity['idx']]
						except:
							bm_rule_quantity['value'] = randint(0, 11)
							if bm_rule_quantity['value']  > 10:
								del bm_rule_quantity['value'] 

						quantity_array.append(bm_rule_quantity)

				cell['quantities'] = quantity_array

				output_json.append(cell)

		except Exception as e:
			raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.__contract, str(e)))

		return output_json



	def interpolate_bm_file(self, path):

		if  self.__rules is None:
			raise Exception("Данные количественных характеристик отсутствуют")

		self.readSettings()
		locBmPath = path

		self._wrkpath = self._settings.getValue(ID_NETPATH)
		netExpAssayPath = os.path.join(self._wrkpath, self._settings.getValue(ID_DBDIR), self._settings.getValue(ID_EXPASSAY))
		locExpAssayPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netExpAssayPath))

		shutil.copyfile(netExpAssayPath, locExpAssayPath)


		indexFld = self._settings.getValue(ID_IDXFLD)


 
		idwParamIds = (ID_IDWSECTOR, ID_IDWMINPTS, ID_IDWMAXPTS, ID_IDWFACTOR1, ID_IDWFACTOR2, ID_IDWFACTOR3, ID_IDWRADIUS)
		idwParams = {paramId: self._settings.getValue(paramId) for paramId in idwParamIds} 

		getGeoCodes = list(zip(*self._settings.getValue(ID_GEOCODES)))[0]

		codes = list(filter(lambda x: x[-1] == 2, getGeoCodes))
		emptyCode = None if not codes else codes[0][0]




		for rule in self.__rules:
			rule_quantity = rule['quantity']
			element = rule_quantity['name']


			oreCodes, oreTypes, oreIndexes = [], [], []
			for (oreCode, oreDescription, oreType), assayAndBmIndexes in self._settings.getValue(ID_GEOCODES):
				if oreCode == emptyCode or oreType > 2:
					continue

				oreCodes.append(oreCode)
				oreTypes.append(oreType)
				oreIndexes.append([row[0] for row in assayAndBmIndexes["ASSAY"]])

				oreIndexesDict = dict(zip(oreCodes, oreIndexes))

				for radius in idwParams[ID_IDWRADIUS]:
					err = self.exec_mmproc(idw, locBmPath, locExpAssayPath, indexFld, oreCode, element, radius[0], oreIndexesDict[oreCode], BMFLD_SYS_DIR, BMFLD_SYS_ROT, BMFLD_SYS_DIP,
							idwParams[ID_IDWMINPTS], idwParams[ID_IDWMAXPTS], idwParams[ID_IDWSECTOR], idwParams[ID_IDWFACTOR1], idwParams[ID_IDWFACTOR2], idwParams[ID_IDWFACTOR3])
					if err:						print ("Ошибка. %s" % err)
					if self.is_cancelled:		return
				if self.is_cancelled:		return
		if self.is_cancelled:		return

	def readSettings(self):
		if self._settings:
			self._settings.close()
		try:		
			self._settings = FormSet(ID_SETTINGS)
		except Exception as e:	raise Exception("Невозможно считать настройки. %s" % str(e))
		self._settings.open(0)
		#
		if not self._settings.values():
			raise Exception("Невозможно считать настройки. %s" % str(e)) 


class MM_Executor(ProcessWindow):
	def __init__(self, parent, *args, **kw):
		super(MM_Executor, self).__init__(parent, *args, **kw)
		self.__createVariables()

	

	def __createVariables(self):
		self.__bm_path = None


	def setParams(self, cm_object):
		self.__cm_object = cm_object

	def check(self):
		self.__cm_object.check_params()
		return True
	
				
	def prepare(self):
		if not super(MM_Executor, self).prepare():
			return

		if not self.check():
			return		

		self.__bm_path = os.path.join(self._tmpfolder, self._tmpprefix + self.__cm_object.contract_name)
		self.__bm_path = self.__bm_path + ".DAT"
		self.__bm_path = self.__bm_path.upper()

		

		
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			raise Exception ("Невозможно интерполировать блочную модель. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(MM_Executor, self).run():
			return

		major, minor, servicepack, build = get_micromine_version()
		if major > 16:	 
			pass
			#self.__cm_object.dump_bm_to_file(self.__bm_path)
			#self.__cm_object.interpolate_bm_file (self.__bm_path)
			#self.__cm_object.load_from_file(self.__bm_path)

		else:
			raise Exception ("Micromine version not satisfied requirements")

	


