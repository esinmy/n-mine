﻿from services.api.interfaces.IApi import IApi
from PyQt4 import QtGui, QtCore
import os
from copy import deepcopy
from utils.osutil import get_temporary_prefix
from itertools import groupby

import  shutil
from mm.mmutils import Tridb



import json
from services.api.api_v_1_0.libs.json_helper import JsonHelper
from services.api.api_v_1_0.libs.mm_forms import *
from  uuid import uuid4
from services.utils.constants import *
from services.utils.validation import is_blank

from services.utils.ProcessWindow import	 ProcessWindow
from mm.mmutils import MicromineFile, get_micromine_version
from utils.logger import  NLogger

from utils.fsettings import FormSet	


try:
	import MMpy
except:
	pass

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class Api_v_1_0_Model (IApi, QtCore.QObject):
	def __init__(self, thread):
		IApi.__init__(self, thread)
		QtCore.QObject.__init__(self)
		self.thread = thread


	@property
	def plugin_name(self):
		return "MinePro Sevice API Accounting Model"

	@property
	def plugin_version(self):
		return 1.0

	@property
	def api_contract_name(self):
		return r"v1.0/AccountingModel"

	def on_message_recived_handler(self, input_json, callback):
		try:

			logger.info('Api_v_1_0_Model - on_message_recived_handler entry')

			output_json = {"models": []}

			for attr in ['models', 'objects']:
				if not attr in input_json:
					raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута '{1}'".format(self.api_contract_name, attr))






			models = input_json['models']



			objects = input_json['objects']



			am = Model(self, self.api_contract_name)


			am.load_from_classified_model(models, objects)



			data = MM_Executor(self.thread)
			data.setParams(am)
			data.prepare()
			logger.debug('prepare exit')

			try:	  

				json_out = am.dump_to_json()
				output_json['models'] = json_out

			except Exception as e:
				raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.api_contract_name, str(e)))

			#logger.debug('output_json')
			#logger.debug(output_json)
			callback(output_json)

		except Exception as e:

			code = 1
			message = str(e)
			json_error = {"code": str(code),
							"error": message}
			callback(output_json,  json_error)



class Model(ProcessWindow):
	def __init__(self, parent, api_contract_name, *args, **kw):
		super(Model, self).__init__(parent, *args, **kw)
		self.__createVariables()

		self.parent = parent

		self.__contract = api_contract_name

		self._settings = None



	def __createVariables(self):
		self.__bm_data = None
		self.__objects = None
		self.__model = None
		self.__contract = None

		self.static_bm_fields =(
					{"head": "{0}".format(ID_X), "f_head": "{0}".format(ID_EAST), "stru": "{0}:R:0:6".format(ID_EAST)}
					,{"head": "{0}".format(ID_Y), "f_head": "{0}".format(ID_NORTH), "stru": "{0}:R:0:6".format(ID_NORTH)}
					,{"head": "{0}".format(ID_Z), "f_head": "{0}".format(ID_RL), "stru": "{0}:R:0:6".format(ID_RL)}
					,{"head": "{0}".format(ID_X_SIZE), "f_head": "{0}".format(ID_EAST_SIZE), "stru": "{0}:R:0:6".format(ID_EAST_SIZE)}
					,{"head": "{0}".format(ID_Y_SIZE), "f_head": "{0}".format(ID_NORTH_SIZE), "stru": "{0}:R:0:6".format(ID_NORTH_SIZE)}
					,{"head": "{0}".format(ID_Z_SIZE), "f_head": "{0}".format(ID_RL_SIZE), "stru": "{0}:R:0:6".format(ID_RL_SIZE)}
					,{"head": "{0}".format(ID_ID), "f_head": "{0}".format(ID_ID), "stru": "{0}:C:40:0".format(ID_ID)}
					,{"head": "{0}".format(ID_SEGMENT), "f_head": "{0}".format(ID_SEGMENT), "stru": "{0}:C:15:0".format(ID_SEGMENT)}
					,{"head": "{0}".format(ID_CHANGED), "f_head": "{0}".format(ID_CHANGED), "stru": "{0}:C:1:0".format(ID_CHANGED)}
					,{"head": "{0}".format(ID_WORK), "f_head": "{0}".format(ID_WORK), "stru": "{0}:C:40:0".format(ID_WORK)}
					,{"head": "{0}".format(ID_KIND), "f_head": "{0}".format(ID_KIND), "stru": "{0}:C:40:0".format(ID_KIND)}
			)

		self.quality_bm_fields =()
		self.quantity_bm_fields =()

	@property
	def contract(self):
		return self.__contract

	@property
	def contract_name(self):
		contract_splited = self.__contract.split('/')
		if len(contract_splited) > 0:
			result = contract_splited[-1]
		else:
			raise Exception ("Contract name not defined in '{0}'".format(self.__contract))

		return result 

	def check_params(self):

		for	cell in self.__model:
			try:
				row_id = (cell['id'], )
				parent = cell['parent']
				parent_id = parent['id']
				parent_segment = (parent['segment'], )
				center = cell['extent']['center']
				row_center = (center['x'], center['y'], center['z'])
				box3d = cell['extent']
				row_size = (box3d['_x'], box3d['_y'], box3d['_z'])
				row =  row_center + row_size  + row_id + parent_segment
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит одного из обязательных атрибутов. {1}".format(self.__contract, str(e)))
			

			try:
				for quality_item in cell['quality']:
					quality_name = quality_item['name']
					row = row + (quality_name , )
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'model.quality.{1}' ".format(self.__contract, 'name'))


			try:
				for quantity_item in cell['quantity']:
					quantity_name = quantity_item['name']
					row = row + (quantity_name , )
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'model.quantity.{1}' ".format(self.__contract, 'name'))


			for obj in self.__objects:
				for attr1 in ['work', 'kind', 'wireframe']:
					if not attr1 in obj:
						raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'objects.{1}' ".format(self.__contract, attr1))

				

		


	def load_from_classified_model(self, model, objects):
		self.__model = model
		self.__objects = objects

		self.__in_bm_data = []
		self.__out_bm_data = []

		for	cell in self.__model:
			try:
				row_id = (cell['id'],)
				parent = cell['parent']
				parent_id = parent['id']
				parent_segment = (parent['segment']['id'],)
				center = cell['extent']['center']
				row_center = (center['x'], center['y'], center['z'])
				box3d = cell['extent']
				row_size = (box3d['_x'], box3d['_y'], box3d['_z'])
				row =  row_center + row_size  + row_id + parent_segment
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит одного из обязательных атрибутов. {1}".format(self.__contract, str(e)))
			
			row = row + ("",) # CHANGED атрибут
			row = row + ('', '')   # work, kind


			try:
				for quality_item in cell['quality']:
					quality_value = quality_item['value'].encode('utf-8').decode('utf8')
					row = row + (quality_value ,)
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'model.quality.{1}. {2}' ".format(self.__contract, 'name', str(e)))

			try:
				for quantity_item in cell['quantity']:
					quantity_value = quantity_item['value']
					row = row + (quantity_value,)
			except Exception as e:
				raise Exception("Входящее сообщение формата '{0}' не содержит обязательного атрибута 'model.quantity.{1}' ".format(self.__contract, 'name'))


			self.__in_bm_data.append(row)


		row_first = self.__model[0]
		for quality_item in row_first['quality']:
			self.quality_bm_fields = self.quality_bm_fields	+ ({"head": quality_item['name'], "f_head": "{0}".format(quality_item['name']), "stru": "{0}:C:20:0".format(quality_item['name'])},)

		for quantity_item in row_first['quantity']:
				self.quantity_bm_fields = self.quantity_bm_fields	+ ({"head": quantity_item['name'], "f_head": "{0}".format(quantity_item['name']), "stru": "{0}:R:0:4".format(quantity_item['name'])},)


		
	def load_from_file(self, path):
		if self.__objects is None:
			raise Exception ("Не определены отчётные параметры")

		header = []

		bm_field_set = self.static_bm_fields + self.quality_bm_fields + self.quantity_bm_fields
		for item in bm_field_set:
			header.append(item['f_head'])

		self.__out_bm_data = self.extractAllBlockModelData(path, header)
		bmInFile = MicromineFile()
		if not bmInFile.open(path):
			raise Exception("Невозможно открыть файл '%s'" % path)

		records_count = bmInFile.records_count
		bmInFile.close()	

		if len(self.__out_bm_data) != records_count:
			raise Exception("Не удалось получить данные из временного файла, не соответствие количества записей в источнике и результате выборки")


	def dump_bm_to_file(self, path):
		if self.__in_bm_data is None:
			raise Exception("Данные неклассифицированной модели отсутствуют")
		header = []
		stru = []
		bm_field_set = self.static_bm_fields + self.quality_bm_fields + self.quantity_bm_fields
		for item in bm_field_set:
			if item['head'] in 	header:
				continue
			header.append(item['head'])
			stru.append	(item['stru'])

		logger.debug('header')
		logger.debug(header)
		logger.debug('stru')
		logger.debug(stru)
		logger.debug('self.__in_bm_data')
		logger.debug(self.__in_bm_data)
		MicromineFile.write(path, self.__in_bm_data, header, MicromineFile.to_filestruct(stru))


	def dump_to_json(self):

		if  self.__out_bm_data is None:
			raise Exception("Данные БМ отсутствуют")
		if  self.__objects is None:
			raise Exception("Данные отчётных  характеристик отсутствуют")


		Models_json = []

		header = []

		bm_field_set = self.static_bm_fields + self.quality_bm_fields + self.quantity_bm_fields
		for item in bm_field_set:
			header.append(item['f_head'])

		idx_east = header.index(ID_EAST)
		idx_north = header.index(ID_NORTH)
		idx_rl = header.index(ID_RL)

		idx_east_size = header.index(ID_EAST_SIZE)
		idx_north_size = header.index(ID_NORTH_SIZE)
		idx_rl_size = header.index(ID_RL_SIZE)

		idx_segment = header.index(ID_SEGMENT)

		idx_changed = header.index(ID_CHANGED)
		idx_parent = header.index(ID_ID)

		idx_work = header.index(ID_WORK)
		idx_kind = header.index(ID_KIND)

		row_first = self.__model[0]
		qualities = deepcopy(row_first['quality'])
		for quality_item in qualities:
			quality_item['idx'] = header.index(quality_item['name'])

		quantities = deepcopy(row_first['quantity'])
		for quantity_item in quantities:
				quantity_item['idx'] = header.index(quantity_item['name'])

		bm_inside_accounting_objects = list(filter(lambda x: x[idx_changed] == 'Y' , self.__out_bm_data))
		bm_inside_accounting_objects_sorted =  sorted( bm_inside_accounting_objects, key = lambda x: (x[idx_work], x[idx_kind]), reverse = False)

		for key, values in groupby(bm_inside_accounting_objects_sorted, lambda x: {'work': x[idx_work], 'kind': x[idx_kind]}) :


			if any([is_blank(item) for item in key.values()]):
				raise Exception ("Один из параметров отчёта в результирующей БМ пуст") 

			try:

				account_object_bm = {}
				account_object_bm['work'] = key['work']
				account_object_bm['kind'] = key['kind']
				models = []
				for record in list(values):

					#if record[idx_parent] == '2615c9ca-bfcd-4022-9085-93078b32ac39':
					#	logger.debug('header')
					#	logger.debug(header)
					#	logger.debug('record')
					#	logger.debug(record)


					cell = {}
					
					cell["id"] = str(uuid4())
					#cell['parent'] = {'id': record[idx_parent], 'segment': record[idx_segment], 'extent': None}
					cell['parent'] = JsonHelper.not_classified_model(id = record[idx_parent], segment = JsonHelper.segment( id = record[idx_segment]))

				

					cell['extent'] =  JsonHelper.box3d(JsonHelper.getPoint(record[idx_east], record[idx_north], record[idx_rl])
										, record[idx_east_size], record[idx_north_size], record[idx_rl_size])

					quality_array = []
					for quality_item in qualities:
						if not quality_item["name"] in [ qual["name"] for qual in  quality_array]: 
							bm_quality_item = deepcopy(quality_item)
							del bm_quality_item['idx']
							bm_quality_item['value'] =  record[quality_item['idx']]
							quality_array.append(bm_quality_item)


					quantity_array = []
					for quantity_item in quantities:
						if not quantity_item["name"] in [ qual["name"] for qual in  quantity_array]: 
							bm_quantity_item = deepcopy(quantity_item)
							del bm_quantity_item['idx']
							bm_quantity_item['value'] =  record[quantity_item['idx']]
							quantity_array.append(bm_quantity_item)

					#logger.debug('quantity_array')
					#logger.debug(quantity_array)

					cell['quality'] = quality_array
					cell['quantity'] = quantity_array

					models.append(cell)


				account_object_bm['models'] = models
				Models_json.append( account_object_bm)


			except Exception as e:
				raise Exception("Произошла ошибка при формировании исходящего сообщения формата '{0}'. {1}".format(self.__contract, str(e)))



		return Models_json

	def dump_wfs_to_files(self):
		if  self.__objects is None:
			raise Exception("Данные отчётных  характеристик отсутствуют")

		for obj in self.__objects:
			temp_wf_file = os.path.join(self._tmpfolder, get_temporary_prefix() + "type_acc_obj.tridb")
			temp_wf_file = temp_wf_file.upper()
			obj['temp_wf_file'] = temp_wf_file
			shutil.copyfile(obj['wireframe'], temp_wf_file)

			dsttri = Tridb(temp_wf_file)
			dsttri.clear_user_attributes()

			dsttri.add_attribute('work')
			dsttri.add_attribute('kind')

			for dstname in dsttri.wireframe_names:
					dsttri[dstname].set_user_attribute('work', obj['work'].encode('utf-8').decode('utf8'))
					dsttri[dstname].set_user_attribute('kind', obj['kind'].encode('utf-8').decode('utf8'))
					dsttri.commit()


			dsttri.add_attribute(ID_CHANGED)
			for dstname in dsttri.wireframe_names:
				dsttri[dstname].set_user_attribute(ID_CHANGED, 'Y')
				dsttri.commit()



			dsttri.close()

	def classify_bm_file(self, path):

		if  self.__objects is None:
			raise Exception("Данные отчётных  характеристик отсутствуют")


		for obj in self.__objects:
			wfdata = []

			attributes = []

			temp_wf_file = obj['temp_wf_file'] 
			dsttri = Tridb(temp_wf_file)


			for wfname in dsttri.wireframe_names:
				wfdata.append([temp_wf_file, DEFAULT_ATTR_NAME, wfname])




			attributes.append([0, 'work', 'work'] )
			attributes.append([0, 'kind', 'kind'] )

			attributes.append([0, ID_CHANGED, ID_CHANGED] )

			wfset = create_wireframe_set(wfdata)

			assing_params = {}
			assing_params["attributes"] = attributes
			assing_params["sub_xsize"] = 8
			assing_params["sub_ysize"] = 8
			assing_params["sub_zsize"] = 4
			assing_params["overwrite_bool"] = True
			assing_params["purge_bool"] = False
			assing_params["wireframe_set"] = wfset
			assing_params["bmpath"] = path

			assign_wireframes (assing_params)


	def readSettings(self):
		if self._settings:
			self._settings.close()
		try:		
			self._settings = FormSet(ID_SETTINGS)
		except Exception as e:	raise Exception("Невозможно считать настройки. %s" % str(e))
		self._settings.open(0)
		#
		if not self._settings.values():
			raise Exception("Невозможно считать настройки. %s" % str(e)) 


class MM_Executor(ProcessWindow):
	def __init__(self, parent, *args, **kw):
		super(MM_Executor, self).__init__(parent, *args, **kw)
		self.__createVariables()

	

	def __createVariables(self):
		self.__bm_path = None


	def setParams(self, am_object):
		self.__am_object = am_object






	def check(self):
		self.__am_object.check_params()

		return True
	
				
	def prepare(self):
		if not super(MM_Executor, self).prepare():
			return

		if not self.check():
			return		



		self.__bm_path = os.path.join(self._tmpfolder, self._tmpprefix + self.__am_object.contract_name)
		self.__bm_path = self.__bm_path + ".DAT"
		self.__bm_path = self.__bm_path.upper()

		

		
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			raise Exception ("Невозможно создать неклассифицированную блочную модель. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(MM_Executor, self).run():
			return

		major, minor, servicepack, build = get_micromine_version()
		if major > 16:	 
			
			self.__am_object.dump_bm_to_file(self.__bm_path)
			self.__am_object.dump_wfs_to_files()
			self.__am_object.classify_bm_file(self.__bm_path)
			self.__am_object.load_from_file(self.__bm_path)

		else:
			raise Exception ("Micromine version not satisfied requirements")

	


