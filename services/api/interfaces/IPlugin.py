﻿class IPlugin():
	def __init__(self):
			pass

	@property
	def plugin_type(self):
		raise NotImplementedError

	@property
	def plugin_name(self):
		raise NotImplementedError

	@property
	def plugin_version(self):
		raise NotImplementedError