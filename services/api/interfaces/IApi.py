﻿
from services.api.interfaces.IPlugin import IPlugin

class IApi(IPlugin):
	def __init__(self, parent):
				pass

	@property
	def plugin_type(self):
		return "API"

	@property
	def api_contract_name(self):
		raise NotImplementedError


	def on_message_recived_handler(self, input_json, callback):
		raise NotImplementedError

	def AnyMethod (self, input_json):
		raise NotImplementedError






