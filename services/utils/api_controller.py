﻿from PyQt4 import QtCore, QtGui
from flask_socketio import emit
import json
from utils.logger import  NLogger 
LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class Api_controller(QtCore.QObject):
	message_received_handler = QtCore.pyqtSignal( object, object)  #  input_json , callback_function (api_version, output_json, message)

	def __init__(self, contract_name, parent, **kwargs):
		super().__init__(**kwargs)
		self.parent = parent
		self.contract_name = contract_name


	def get_message(self, msg):
		logger.info('get_message entry')
		self.message_received_handler.emit(msg, self.send_message)


	def send_message (self, json_output,  json_error = None, room = None):
		logger.info('send_message entry')

		if json_error is None:
			self.parent.socketio.emit('get_json', json.dumps(json_output, ensure_ascii  = False), namespace='/{0}'.format(self.contract_name), room = room)
		else:
			self.parent.socketio.emit('get_json', json.dumps(json_error, ensure_ascii  = False), namespace='/{0}'.format('error_space'), room = room )






