import os
import re

SQL_INJECTION_PROTECT_PATTERN = r'[^a-zA-Z0-9_]'


# текстовые проверки
def is_blank(v):
	return v.replace(" ", "") == ""

# числовые проверки 
def is_number(v):
	try:		float(v);	return True
	except:		return False
def is_nonnegative(v):
	if is_number(v) and float(v) >= 0:		return True
	else:		return False
def is_positive(v):
	if is_number(v) and float(v) > 0:		return True
	else:		return False
def is_nonpositive(v):
	if is_number(v) and float(v) <= 0:		return True
	else:		return False
def is_negative(v):
	if is_number(v) and float(v) < 0:		return True
	else:		return False
def in_range(v, a, b, include_left = True, include_right = True):
	gt = v >= a if include_left else v > a
	lt = v <= b if include_right else v < b
	return gt and lt


# проверка дат
def is_valid_date(v):
	if len(v) != 7:					return False
	if v[4] != "-":					return False
	numbers = v.split("-")
	if len(numbers) != 2:			return False
	if len(numbers[0]) != 4:		return False
	if not numbers[0].isdigit():	return False
	if not numbers[1].isdigit():	return False
	if int(numbers[1]) > 12:		return False
	if int(numbers[1]) < 1:			return False
	return True