try:	import MMpy
except:	pass
import os
import sys
import importlib
import inspect
import platform
import functools
import logging
import logging.handlers
from inspect import signature , getargspec
from services.api.interfaces.IPlugin import IPlugin
from utils.logger import  NLogger , register_logging_for_plugins


LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



class ApiManager():
	def __init__(self, parent, *args, **kw):
		self.main_app = parent
		self.plugins = {}
		self.__addPlugins()

	def __addPlugins(self):
		initialdir = os.getcwd()
		pluginsdir = os.path.join(initialdir,"services", "api")
		plugin_packages = []
		for w_path, w_dirs, w_files in os.walk(pluginsdir):
			plugin_packages = [ i for i in w_dirs if i != "interfaces" and i != '__pycache__']
			del w_dirs[:] # смотрим только верхний уровень
		
		for plugin_package in plugin_packages:
			plugin_folder = os.path.join(pluginsdir,plugin_package)
			# Перебирем файлы в папке плагина
			module_objs = []
			for fname in os.listdir(plugin_folder):

					if fname.endswith (".py"):
							module_name = fname[: -3]
							if module_name != "__init__":
									module_obj = importlib.import_module("services.api." + plugin_package + "." +  module_name)	 #
									register_logging_for_plugins(module_obj)
									module_objs.append(module_obj)

			for module_obj in module_objs:
				for elem in dir (module_obj):   
						obj = getattr (module_obj, elem)

						# Это класс?
						if inspect.isclass (obj):
								# Класс производный от ISwa?
								if issubclass (obj, IPlugin) and not obj.__name__.startswith( "I") :
									try:
										swa_object = obj(self.main_app)
									except Exception as e:
										logger.info("Не удалось загрузить плагин. {0}".format(str(e)))
										continue
									current_swa_object = self.plugins.get(swa_object.plugin_name)
									if current_swa_object is None:
										self.plugins[swa_object.plugin_name] = {}
										self.plugins[swa_object.plugin_name]['object'] = swa_object
									else:
										if current_swa_object['object'].plugin_version < 	swa_object.plugin_version:
											self.plugins[swa_object.plugin_name]['object'] = swa_object



	def get_plygins_by_interface(self, interface):
		result = {}
		for plugin_name in self.plugins:
			if issubclass(type(self.plugins[plugin_name]['object']), interface):
				result[plugin_name] = {}
				result[plugin_name]['object'] = self.plugins[plugin_name]['object']

		# Проверка плагинов на удовлетворение условиям
		#for plugin_name in list(result.keys()):
		#	try:
		#		self.plugins[plugin_name]['object'].check_db()
		#	except Exception as e:
		#		logger.info("Не удалось загрузить плагин. {0}".format(str(e)))
		#		del result[plugin_name]

		return result
				 
	 