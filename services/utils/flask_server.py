﻿from PyQt4 import QtCore, QtGui

import os
from flask import Flask, request, Response, session, g, redirect, url_for, abort, render_template, flash ,copy_current_request_context	
from flask_socketio import SocketIO, emit, disconnect  , join_room
from flask_login import current_user, LoginManager
from flask_cors import CORS


import json

import sys
try:
	import MMpy
except:
	pass

from  services.utils.api_controller import Api_controller

from utils.logger import  NLogger 
LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class FlaskServer(QtCore.QObject):
	def __init__(self, parent,  **kwargs):
		super().__init__(**kwargs)

		self.parent = parent
		self.plugins = parent.plugins

		self.socket_server_init()

		self.api_plugins_registration()

		self.http_routes_registration()


	def socket_server_init(self):
		self.app = Flask('__main__')
		cors = CORS(self.app, headers={'Content-Type':'application/json', 'Access-Control-Allow-Origin':'*'})

		self.app.secret_key = os.urandom(48)
		self.app.debug = True
		self.app.template_folder = os.path.join(os.getcwd(), "services", "templates")
		self.app.static_folder =  os.path.join(os.getcwd(), "services", "static")
		self.socketio = SocketIO(self.app)


	def api_plugins_registration(self):
		api_objects = [plugin['object'] for plugin in self.plugins.values()]
		api_controllers = [ Api_controller(api_object.api_contract_name, self) for api_object in api_objects]
		logger.info("api_controllers")
		logger.info(api_controllers)

		for api_object, api_controller in zip (api_objects, api_controllers):
			api_controller.message_received_handler.connect(api_object.on_message_recived_handler)
			
			#self.socketio.on('put_json',  namespace='/{0}'.format(api_object.api_contract_name))(lambda e: api_controller.get_message(e))
			self.socketio.on('put_json',  namespace='/{0}'.format(api_object.api_contract_name))(lambda e, f =  api_controller.get_message: self.room (e, f))

			#self.socketio.on('put_json',  namespace='/{0}'.format(api_object.api_contract_name))(self.room)
			


	def room (self, msg, controllerCallback):
		try:
			logger.debug("msg")
			logger.debug(not msg is None)
			logger.debug(type(msg) is dict)
			logger.debug("room" in msg)

			logger.debug("socket.id")
			logger.debug(request.sid)

			if type(msg) is str:
				msg =  msg.replace("\\\\","/").replace("\\","/").encode('latin1').decode('utf-8-sig')
				jsom_msg = json.loads(msg)
				logger.debug("json loads")
			else:
				jsom_msg = msg
				logger.debug("json as is")

			if not jsom_msg is None and type(jsom_msg) is dict and "room" in jsom_msg:
				logger.debug("room")
				logger.debug(jsom_msg["room"])
				join_room(jsom_msg["room"])
				join_room(jsom_msg["room"], namespace='/{0}'.format('error_space'))



				logger.debug("room in socket")
				logger.debug(self.socketio.server.manager.rooms['/v1.0/TestMicromine/interpolation'])

			controllerCallback(jsom_msg)

		except Exception as e:
			self.socketio.emit('get_json', json.dumps(str(e), ensure_ascii  = False), namespace='/{0}'.format('error_space'), room = None )


	def http_routes_registration(self):
		self.app.add_url_rule('/', '', self.index)
		self.app.add_url_rule('/stop', 'stop', self.stop, methods = ['POST'])

		self.app.before_request(self.allowed_hosts)
			


	def index(self):
		return render_template('index.html')



	def stop(self):
		self.shutdown_server()
		response = Response()
		return response

	def shutdown_server(self):
		func = request.environ.get('werkzeug.server.shutdown')
		if func is None:
			raise RuntimeError('Not running with the Werkzeug Server')
		func()


	def allowed_hosts(self):
		#TRUSTED_HOSTS = ['127.0.0.1', 'GGIS1', 'GGIS7', '172.25.160.148', '192.168.137.38', '192.168.0.118']
		TRUSTED_HOSTS = ['127.0.0.1']
		req_host = request.remote_addr

		#if req_host not in TRUSTED_HOSTS:
		#	logger.info("Forbidden access from ip: {0}".format(req_host))
		#	abort(403)  # Forbidden


