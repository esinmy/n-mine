from PyQt4 import QtGui, QtCore
import os
import re

SQL_INJECTION_PROTECT_PATTERN = r'[^a-zA-Z0-9_]'


class IntValidator(QtGui.QIntValidator):
	def __init__(self, *args, **kw):
		super(IntValidator, self).__init__(*args, **kw)

class DoubleValidator(QtGui.QDoubleValidator):
	def validate(self, string, pos):
		state, string, pos = QtGui.QDoubleValidator.validate(self, string, pos)
		if state == QtGui.QValidator.Invalid:
			return QtGui.QValidator.Invalid, string, pos

		if string == "":				return QtGui.QValidator.Acceptable, string, pos
		elif string in [".", "-"]:		return QtGui.QValidator.Intermediate, string, pos

		try:		value = float(string)
		except:		return QtGui.QValidator.Invalid, string, pos

		if value >= self.bottom() and value <= self.top():
			return QtGui.QValidator.Acceptable, string, pos
		
		return QtGui.QValidator.Invalid, string, pos

class MineSuffixValidator(QtGui.QValidator):
	def validate(self, string, pos):
		# разрешаем только русские буквы и цифры,
		# пробелы заменям на подчеркивания и переводим
		# к верхнему регистру
		string = string.replace(" ", "_").upper()
		vsymb = [chr(i) for i in range(48, 58)] + [chr(i) for i in range(1040, 1104)] + [" ", "_"]
		# символы, которые необходимо удалить
		rsymb = [sym for sym in string if sym not in vsymb]
		# удаляем неверные символы
		for sym in rsymb:
			string = string.replace(sym, "")
		return QtGui.QValidator.Acceptable, string, pos

class DistanceValidator(QtGui.QValidator):
	def validate(self, string, pos):
		# разрешаем только целые числа с обязательным знаком +\- перед ними
		string = string.strip()
		sym_to_del = []
		for index in range(len(string)) :
			if string[index] not in list('+-1234567890'):
				sym_to_del.append(string[index])
			if string[index]  in list('+-') and index != 0:
				sym_to_del.append(string[index])
		sym_to_string = [sym for sym in list(string) if sym not in sym_to_del]
		string = "".join(sym_to_string)

		if len(string) > 0 and string[0] not in ['+', '-']: 
			string = '+'+string
			pos = len(string)
		return QtGui.QValidator.Intermediate, string, pos

class StringValueValidator(QtGui.QValidator):
	def validate(self, string, pos):
		# разрешаем только целые числа с обязательным знаком +\- перед ними
		string = string.strip('\r\n ')
		return QtGui.QValidator.Intermediate, string, pos

class SqlInjectionValidator(QtGui.QValidator):
	def validate(self, string, pos):
		# 
		string = string.strip()
		string = re.sub(SQL_INJECTION_PROTECT_PATTERN, "", string)
		return QtGui.QValidator.Intermediate, string, pos

# текстовые проверки
def is_blank(v):
	return v.replace(" ", "") == ""

# числовые проверки 
def is_number(v):
	try:		float(v);	return True
	except:		return False
def is_nonnegative(v):
	if is_number(v) and float(v) >= 0:		return True
	else:		return False
def is_positive(v):
	if is_number(v) and float(v) > 0:		return True
	else:		return False
def is_nonpositive(v):
	if is_number(v) and float(v) <= 0:		return True
	else:		return False
def is_negative(v):
	if is_number(v) and float(v) < 0:		return True
	else:		return False
def in_range(v, a, b, include_left = True, include_right = True):
	gt = v >= a if include_left else v > a
	lt = v <= b if include_right else v < b
	return gt and lt


# проверка дат
def is_valid_date(v):
	if len(v) != 7:					return False
	if v[4] != "-":					return False
	numbers = v.split("-")
	if len(numbers) != 2:			return False
	if len(numbers[0]) != 4:		return False
	if not numbers[0].isdigit():	return False
	if not numbers[1].isdigit():	return False
	if int(numbers[1]) > 12:		return False
	if int(numbers[1]) < 1:			return False
	return True