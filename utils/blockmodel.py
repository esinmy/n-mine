try:		import MMpy
except:		pass

from PyQt4 import QtCore

import os
from datetime import datetime

from utils.constants import *
from utils.nnutil import prev_date, fieldname_to_date, date_to_fieldname
from utils.osutil import *
from utils.validation import is_valid_date
from mm.formsets import loadBlockModel
from mm.mmutils import MicromineFile, get_micromine_version
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def assign_wireframes(kwargs):
	bmpath = kwargs.get("bmpath")
	wireframe_set = kwargs.get("wireframe_set")
	dbfilter = kwargs.get("dbfilter")
	attributes = kwargs.get("attributes")
	sub_xsize = kwargs.get("sub_xsize")
	sub_ysize = kwargs.get("sub_ysize")
	sub_zsize = kwargs.get("sub_zsize")
	overwrite_bool = kwargs.get("overwrite_bool")
	purge_bool = kwargs.get("purge_bool")
	delete_bool = kwargs.get("delete_bool")

	if wireframe_set is None:	raise Exception("Необходимо задать набор каркасов")
	if attributes is None:		raise Exception("Необходимо задать атрибуты для присваивания")
	if sub_xsize is None:			raise Exception("Необходимо задать субблокирование по X")
	if sub_ysize is None:			raise Exception("Необходимо задать субблокирование по Y")
	if sub_zsize is None:			raise Exception("Необходимо задать субблокирование по Z")
		
	formset = MMpy.FormSet("ASSIGN_WIREFRAME","16.0.875.2")
	formset.set_field("INPUT","1")
	dataGrid = MMpy.DataGrid(4,1)
	dataGrid.set_column_info(0,4,6)
	dataGrid.set_column_info(1,1,7)
	dataGrid.set_column_info(2,2,-1)
	dataGrid.set_column_info(3,3,4)
	for i, (atype, value, field) in enumerate(attributes):
		if atype in ["0", 0]:		dataGrid.set_row(i, ["0", value, "", field])
		if atype in ["1", 1]:		dataGrid.set_row(i, ["1", "", value, field])
	formset.set_field("GRID", dataGrid.serialise())
	formset.set_field("SET_BOOL","1")
	formset.set_field("FACTOR","1")
	formset.set_field("CLEARBLOCKFIELD","0")
	formset.set_field("ANYPORTION","0")
	formset.set_field("ACCUMULATE","0")
	formset.set_field("RL", str(sub_zsize))
	formset.set_field("NORTH", str(sub_ysize))
	formset.set_field("EAST", str(sub_xsize))
	formset.set_field("POINT_DATA","0")
	formset.set_field("BLOCKMODEL_DATA","1")
	if delete_bool:		formset.set_field("WF","1")
	else:				formset.set_field("WF","0")
	formset.set_field("BLOCK_BOOL","1")
	formset.set_field("FACTOR_BOOL","0")
	if overwrite_bool:	formset.set_field("OVERWRITE","1")
	else:				formset.set_field("OVERWRITE","0")
	if purge_bool:		formset.set_field("PURGE","1")
	else:				formset.set_field("PURGE","0")
	formset.set_field("Z_FLD","RL")
	formset.set_field("Y_FLD","NORTH")
	formset.set_field("X_FLD","EAST")
	if dbfilter:
		formset.set_field("FILTER", dbfilter)
		formset.set_field("FILTER_BOOL","1")
	else:
		formset.set_field("FILTER", "0")
		formset.set_field("FILTER_BOOL","0")
	formset.set_field("TYPE","0")
	formset.set_field("DATA_FILE", bmpath)
	formset.set_field("OBJECTGROUP", wireframe_set)
	formset.run()

def create_blank_bm(kwargs):
	def create_blank_bm_2016(kwargs):
		bmpath = kwargs.get("bmpath")
		tripath = kwargs.get("tripath")
		triname = kwargs.get("triname")
		xmin = kwargs.get("xmin")
		xmax = kwargs.get("xmax")
		ymin = kwargs.get("ymin")
		ymax = kwargs.get("ymax")
		zmin = kwargs.get("zmin")
		zmax = kwargs.get("zmax")
		xsize = kwargs.get("xsize")
		ysize = kwargs.get("ysize")
		zsize = kwargs.get("zsize")
		sub_xsize = kwargs.get("sub_xsize")
		sub_ysize = kwargs.get("sub_ysize")
		sub_zsize = kwargs.get("sub_zsize")
		fields = kwargs.get("fields") if "fields" in kwargs else []

		BlankBm_FormSet9 = MMpy.FormSet("BLANK_BM","16.0.875.2")
		BlankBm_FormSet9.set_field("DTMASSIGN_GRID_FAST_FACTOR","0")
		BlankBm_FormSet9.set_field("FCT_BELOW","0")
		BlankBm_FormSet9.set_field("FCT_ABOVE","1")
		BlankBm_FormSet9.set_field("DEL_BELOW","0")
		BlankBm_FormSet9.set_field("DEL_ABOVE","0")
		BlankBm_FormSet9.set_field("DTMASSIGN_GRID_SUB_BLOCKMODEL_BOOL","0")
		BlankBm_FormSet9.set_field("DTMASSIGN_GRID_BLOCKMODEL_BOOL","1")
		BlankBm_FormSet9.set_field("SUBB_METHOD","0")
		BlankBm_FormSet9.set_field("DTMSIDE","0")
		for i, field in enumerate(fields, start = 1):
			fname, ftype, fwidth, prec, fvalue = field.split(":")
			if ftype == "N":		BlankBm_FormSet9.set_field("TYPE%d" % i, "0")
			if ftype == "C":		BlankBm_FormSet9.set_field("TYPE%d" % i, "1")
			if ftype == "R":		BlankBm_FormSet9.set_field("TYPE%d" % i, "2")
			if ftype == "F":		BlankBm_FormSet9.set_field("TYPE%d" % i, "3")
			if ftype == "L":		BlankBm_FormSet9.set_field("TYPE%d" % i, "4")
			if ftype == "S":		BlankBm_FormSet9.set_field("TYPE%d" % i, "5")
			BlankBm_FormSet9.set_field("DEC%d" % i, prec)
			BlankBm_FormSet9.set_field("EXTRAFLD_VALUE%d" % i, fvalue)
			BlankBm_FormSet9.set_field("LENGTH%d" % i, fwidth)
			BlankBm_FormSet9.set_field("NAME%d" % i, fname)
		BlankBm_FormSet9.set_field("TEMPLATE_FILE_TYPE","0")
		BlankBm_FormSet9.set_field("TEMPLATE_BOOL","0")
		BlankBm_FormSet9.set_field("USE_DATASEARCH","0")
		BlankBm_FormSet9.set_field("ANGLES","0")
		BlankBm_FormSet9.set_field("ROTATION","1")
		BlankBm_FormSet9.set_field("MAX_RL", str(zmax))
		BlankBm_FormSet9.set_field("BCOUNT_RL","")
		BlankBm_FormSet9.set_field("BS_RL", str(zsize))
		BlankBm_FormSet9.set_field("MIN_RL", str(zmin))
		BlankBm_FormSet9.set_field("MAX_NORTH", str(ymax))
		BlankBm_FormSet9.set_field("BCOUNT_NORTH","")
		BlankBm_FormSet9.set_field("BS_NORTH", str(ysize))
		BlankBm_FormSet9.set_field("MIN_NORTH", str(ymin))
		BlankBm_FormSet9.set_field("MAX_EAST", str(xmax))
		BlankBm_FormSet9.set_field("BCOUNT_EAST","")
		BlankBm_FormSet9.set_field("BS_EAST", str(xsize))
		BlankBm_FormSet9.set_field("MIN_EAST", str(xmin))
		BlankBm_FormSet9.set_field("TARGET_FIELD","0")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_FAST_FACTOR","0")
		BlankBm_FormSet9.set_field("ANYPORTION","0")
		BlankBm_FormSet9.set_field("ACCUMULATE","0")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SB_RL", str(sub_zsize))
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SB_NORTH", str(sub_ysize))
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SB_EAST", str(sub_xsize))
		BlankBm_FormSet9.set_field("FCT_BOOL","0")
		BlankBm_FormSet9.set_field("SINGLE_BOOL","1")
		BlankBm_FormSet9.set_field("NAME", triname)
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_WFTYPE", tripath)
		BlankBm_FormSet9.set_field("SET_BOOL","0")
		BlankBm_FormSet9.set_field("SUBBLOCKING_METHOD","1")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SUB_BLOCKMODEL_BOOL","1")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_BLOCKMODEL_BOOL","0")
		BlankBm_FormSet9.set_field("OUT_FILE_TYPE","0")
		BlankBm_FormSet9.set_field("OUT_FILE", bmpath)
		BlankBm_FormSet9.set_field("DTM","0")
		BlankBm_FormSet9.set_field("WF","1")
		BlankBm_FormSet9.set_field("RESTRICT","1")
		BlankBm_FormSet9.set_field("RL_FLD","RL")
		BlankBm_FormSet9.set_field("N_FLD","NORTH")
		BlankBm_FormSet9.set_field("E_FLD","EAST")
		BlankBm_FormSet9.run()

	def create_blank_bm_2018(kwargs):
		bmpath = kwargs.get("bmpath")
		tripath = kwargs.get("tripath")
		triname = kwargs.get("triname")
		xmin = kwargs.get("xmin")
		xmax = kwargs.get("xmax")
		ymin = kwargs.get("ymin")
		ymax = kwargs.get("ymax")
		zmin = kwargs.get("zmin")
		zmax = kwargs.get("zmax")
		xsize = kwargs.get("xsize")
		ysize = kwargs.get("ysize")
		zsize = kwargs.get("zsize")
		sub_xsize = kwargs.get("sub_xsize")
		sub_ysize = kwargs.get("sub_ysize")
		sub_zsize = kwargs.get("sub_zsize")
		fields = kwargs.get("fields") if "fields" in kwargs else []

		BlankBm_FormSet9 = MMpy.FormSet("BLANK_BM","18.0.598.0")
		BlankBm_FormSet9.set_field("DTMASSIGN_GRID_FAST_FACTOR","0")
		BlankBm_FormSet9.set_field("FCT_BELOW","0")
		BlankBm_FormSet9.set_field("FCT_ABOVE","1")
		BlankBm_FormSet9.set_field("DEL_BELOW","0")
		BlankBm_FormSet9.set_field("DEL_ABOVE","0")
		BlankBm_FormSet9.set_field("DTMASSIGN_GRID_SUB_BLOCKMODEL_BOOL","0")
		BlankBm_FormSet9.set_field("DTMASSIGN_GRID_BLOCKMODEL_BOOL","1")
		BlankBm_FormSet9.set_field("SUBB_METHOD","0")
		BlankBm_FormSet9.set_field("DTMSIDE","0")

		DataGrid0 = MMpy.DataGrid(5, 1)
		DataGrid0.set_column_info(0, 1, -1)
		DataGrid0.set_column_info(1, 2, -1)
		DataGrid0.set_column_info(2, 3, -1)
		DataGrid0.set_column_info(3, 4, -1)
		DataGrid0.set_column_info(4, 5, -1)
		DataGrid0.set_row(0, [str(xmin), str(xsize), str(xmax), "175", ""])
		DataGrid0.set_row(1, [str(ymin), str(ysize), str(ymax), "79", ""])
		DataGrid0.set_row(2, [str(zmin), str(zsize), str(zmax), "52", ""])
		BlankBm_FormSet9.set_field("EXTENTS_GRID", DataGrid0.serialise())

		DataGrid1 = MMpy.DataGrid(5, 1)
		DataGrid1.set_column_info(0, 0, 4)
		DataGrid1.set_column_info(1, 1, 6)
		DataGrid1.set_column_info(2, 2, -1)
		DataGrid1.set_column_info(3, 3, -1)
		DataGrid1.set_column_info(4, 4, -1)
		for i, field in enumerate(fields):
			fname, ftype, fwidth, prec, fvalue = field.split(":")
			if ftype == "N":		DataGrid1.set_row(i, [fname, "0", fwidth, prec, fvalue])
			if ftype == "C":		DataGrid1.set_row(i, [fname, "1", fwidth, prec, fvalue])
			if ftype == "R":		DataGrid1.set_row(i, [fname, "2", fwidth, prec, fvalue])
			if ftype == "F":		DataGrid1.set_row(i, [fname, "3", fwidth, prec, fvalue])
			if ftype == "L":		DataGrid1.set_row(i, [fname, "4", fwidth, prec, fvalue])
			if ftype == "S":		DataGrid1.set_row(i, [fname, "5", fwidth, prec, fvalue])

		BlankBm_FormSet9.set_field("FIELDS_GRID", DataGrid1.serialise())

		BlankBm_FormSet9.set_field("TEMPLATE_FILE_TYPE","0")
		BlankBm_FormSet9.set_field("TEMPLATE_BOOL","0")
		BlankBm_FormSet9.set_field("USE_DATASEARCH","0")
		BlankBm_FormSet9.set_field("ANGLES","0")
		BlankBm_FormSet9.set_field("ROTATION","1")
		BlankBm_FormSet9.set_field("MAX_RL", str(zmax))
		BlankBm_FormSet9.set_field("BCOUNT_RL","")
		BlankBm_FormSet9.set_field("BS_RL", str(zsize))
		BlankBm_FormSet9.set_field("MIN_RL", str(zmin))
		BlankBm_FormSet9.set_field("MAX_NORTH", str(ymax))
		BlankBm_FormSet9.set_field("BCOUNT_NORTH","")
		BlankBm_FormSet9.set_field("BS_NORTH", str(ysize))
		BlankBm_FormSet9.set_field("MIN_NORTH", str(ymin))
		BlankBm_FormSet9.set_field("MAX_EAST", str(xmax))
		BlankBm_FormSet9.set_field("BCOUNT_EAST","")
		BlankBm_FormSet9.set_field("BS_EAST", str(xsize))
		BlankBm_FormSet9.set_field("MIN_EAST", str(xmin))
		BlankBm_FormSet9.set_field("TARGET_FIELD","0")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_FAST_FACTOR","0")
		BlankBm_FormSet9.set_field("ANYPORTION","0")
		BlankBm_FormSet9.set_field("ACCUMULATE","0")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SB_RL", str(sub_zsize))
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SB_NORTH", str(sub_ysize))
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SB_EAST", str(sub_xsize))
		BlankBm_FormSet9.set_field("FCT_BOOL","0")
		BlankBm_FormSet9.set_field("SINGLE_BOOL","1")
		BlankBm_FormSet9.set_field("NAME", triname)
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_WFTYPE", tripath)
		BlankBm_FormSet9.set_field("SET_BOOL","0")
		BlankBm_FormSet9.set_field("SUBBLOCKING_METHOD","1")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_SUB_BLOCKMODEL_BOOL","1")
		BlankBm_FormSet9.set_field("WIREFRAME_GRID_BLOCKMODEL_BOOL","0")
		BlankBm_FormSet9.set_field("OUT_FILE_TYPE","0")
		BlankBm_FormSet9.set_field("OUT_FILE", bmpath)
		BlankBm_FormSet9.set_field("DTM","0")
		BlankBm_FormSet9.set_field("WF","1")
		BlankBm_FormSet9.set_field("RESTRICT","1")
		BlankBm_FormSet9.set_field("RL_FLD","RL")
		BlankBm_FormSet9.set_field("N_FLD","NORTH")
		BlankBm_FormSet9.set_field("E_FLD","EAST")
		BlankBm_FormSet9.run()

	major, minor, servicepack, build = get_micromine_version()
	if major > 16:
		create_blank_bm_2018(kwargs)
	else:
		create_blank_bm_2016(kwargs)

def optimize_bm(kwargs):
	bmpath = kwargs["bmpath"]
	key_fields = kwargs["key_fields"]
	outpath = kwargs["outpath"]
	xx_missing = True if not "xx_missing" in kwargs else kwargs["xx_missing"]
	blank_missing = True if not "blank_missing" in kwargs else kwargs["blank_missing"]
	char_missing = True if not "char_missing" in kwargs else kwargs["char_missing"]

	OptimiseBm_FormSet1= MMpy.FormSet("OPTIMISE_BM","16.0.896.3")

	if xx_missing:		OptimiseBm_FormSet1.set_field("XX","1")
	else:							OptimiseBm_FormSet1.set_field("XX","0")
	if blank_missing:	OptimiseBm_FormSet1.set_field("BLANK_MISSING","1")
	else:							OptimiseBm_FormSet1.set_field("BLANK_MISSING","0")
	if char_missing:	OptimiseBm_FormSet1.set_field("CHAR_MISSING","1")
	else:							OptimiseBm_FormSet1.set_field("OUTFILE", "0")

	OptimiseBm_FormSet1.set_field("OUTFILE", outpath)
	for i, fld in enumerate(key_fields, start = 1):
		OptimiseBm_FormSet1.set_field("KFLD%d" % i, fld)
	OptimiseBm_FormSet1.set_field("ZFLD","RL")
	OptimiseBm_FormSet1.set_field("YFLD","NORTH")
	OptimiseBm_FormSet1.set_field("XFLD","EAST")
	OptimiseBm_FormSet1.set_field("FILTER","0")
	OptimiseBm_FormSet1.set_field("FTYPE","0")
	OptimiseBm_FormSet1.set_field("MMFILE", bmpath)
	OptimiseBm_FormSet1.run()

def merge_bm(kwargs):
	dbfilter1 = kwargs.get("dbfilter1")
	dbfilter2 = kwargs.get("dbfilter2")
	bmpath1 = kwargs.get("bmpath")
	bmpath2 = kwargs.get("bmpath2")
	outpath = kwargs.get("outpath")

	AddTwoBm_FormSet2= MMpy.FormSet("ADD_TWO_BM","16.0.896.3")
	AddTwoBm_FormSet2.set_field("MODEL2","0")
	AddTwoBm_FormSet2.set_field("MODEL1","1")
	AddTwoBm_FormSet2.set_field("OUTFILE", outpath)
	AddTwoBm_FormSet2.set_field("ZFLD2","RL")
	AddTwoBm_FormSet2.set_field("YFLD2","NORTH")
	AddTwoBm_FormSet2.set_field("XFLD2","EAST")
	if dbfilter2:
		AddTwoBm_FormSet2.set_field("FLTNO2", dbfilter2)
		AddTwoBm_FormSet2.set_field("FILTER2", "1")		
	else:
		AddTwoBm_FormSet2.set_field("FLTNO2", "")
		AddTwoBm_FormSet2.set_field("FILTER2", "0")
	AddTwoBm_FormSet2.set_field("FTYPE2","0")
	AddTwoBm_FormSet2.set_field("MMFILE2", bmpath2)
	AddTwoBm_FormSet2.set_field("ZFLD1","RL")
	AddTwoBm_FormSet2.set_field("YFLD1","NORTH")
	AddTwoBm_FormSet2.set_field("XFLD1","EAST")
	if dbfilter1:
		AddTwoBm_FormSet2.set_field("FILTER1", "1")
		AddTwoBm_FormSet2.set_field("FLTNO1", dbfilter1)
	else:
		AddTwoBm_FormSet2.set_field("FILTER1","0")
		AddTwoBm_FormSet2.set_field("FLTNO1", "")
	AddTwoBm_FormSet2.set_field("FTYPE1", "0")
	AddTwoBm_FormSet2.set_field("MMFILE1", bmpath1)
	AddTwoBm_FormSet2.run()

def report_bm(kwargs):
	bmpath = kwargs["bmpath"]
	grade_grid = kwargs["grade_grid"]
	outpath = kwargs["outpath"]
	default_density = kwargs["default_density"]
	density_field = kwargs["density_field"]

	append_flag = kwargs.get("append_flag")
	material_grid = kwargs.get("material_grid")

	CutoffSet_Resrpt_FormSet1_Nested0 = MMpy.FormSet("CUTOFF_SET","16.1.70.0")
	DataGrid3 = MMpy.DataGrid(2,1)
	DataGrid3.set_column_info(0,1,-1)
	DataGrid3.set_column_info(1,2,-1)
	DataGrid3.set_row(0, ["0","10000"])
	CutoffSet_Resrpt_FormSet1_Nested0.set_field("GRID",DataGrid3.serialise())
	CutoffSet_Resrpt_FormSet1_Nested0.set_field("MISSINGINT","0")
	CutoffSet_Resrpt_FormSet1_Nested0.set_field("ORDER","0")

	Resrpt_FormSet1= MMpy.FormSet("RESRPT","16.1.70.0")
	DataGrid0 = MMpy.DataGrid(3,1)
	DataGrid0.set_column_info(0,1,-1)
	DataGrid0.set_column_info(1,2,-1)
	DataGrid0.set_column_info(2,3,-1)
	DataGrid0.set_row(0, ["","",""])
	Resrpt_FormSet1.set_field("LIST",DataGrid0.serialise())
	Resrpt_FormSet1.set_field("RANGES_BOOL","0")
	Resrpt_FormSet1.set_field("WASTE_BOOL","0")
	Resrpt_FormSet1.set_field("COLOUR_BOOL","0")
	Resrpt_FormSet1.set_field("FLAT","1")
	Resrpt_FormSet1.set_field("TOTALS","0")
	DataGrid1 = MMpy.DataGrid(3,1)
	DataGrid1.set_column_info(0,2,4)
	DataGrid1.set_column_info(1,3,6)
	DataGrid1.set_column_info(2,4,6)
	for i, row in enumerate(grade_grid):
		DataGrid1.set_row(i, list(map(str, row)))
	Resrpt_FormSet1.set_field("GRADE_GRID",DataGrid1.serialise())
	if material_grid:
		DataGrid2 = MMpy.DataGrid(1,1)
		DataGrid2.set_column_info(0,1,4)
		for i, field in enumerate(material_grid):
			DataGrid2.set_row(i, [field])
		Resrpt_FormSet1.set_field("MATERIAL_GRID",DataGrid2.serialise())
	else:
		Resrpt_FormSet1.set_field("MATERIAL_GRID","")
	Resrpt_FormSet1.set_field("FACTOR_BOOL","0")
	Resrpt_FormSet1.set_field("SET_BOOL","0")
	Resrpt_FormSet1.set_field("WF_BOOL","1")
	Resrpt_FormSet1.set_field("CALC_BOOL","0")
	Resrpt_FormSet1.set_field("MIKBINS_BOOL","0")
	Resrpt_FormSet1.set_field("RESRPT_CUTOFF_SET",CutoffSet_Resrpt_FormSet1_Nested0)
	Resrpt_FormSet1.set_field("CUTOFF_BOOL","1")
	Resrpt_FormSet1.set_field("OUT_FILE_TYPE","6")
	Resrpt_FormSet1.set_field("RESRPT_FILE_TYPE","0")
	Resrpt_FormSet1.set_field("TYPE_5","0")
	Resrpt_FormSet1.set_field("TYPE_4","0")
	Resrpt_FormSet1.set_field("TYPE_3","0")
	Resrpt_FormSet1.set_field("TYPE_2","0")
	Resrpt_FormSet1.set_field("TYPE_1","0")
	Resrpt_FormSet1.set_field("OUT_FILE", outpath, MMpy.append_flag.none if not append_flag else MMpy.append_flag.output)
	Resrpt_FormSet1.set_field("DEFAULT", str(default_density))
	Resrpt_FormSet1.set_field("SG_VAR", density_field)
	Resrpt_FormSet1.set_field("DATA","1")
	Resrpt_FormSet1.set_field("L_VAR","RL")
	Resrpt_FormSet1.set_field("NORTH_VAR","NORTH")
	Resrpt_FormSet1.set_field("EAST_VAR","EAST")
	Resrpt_FormSet1.set_field("FILTER","0")
	Resrpt_FormSet1.set_field("RESRPT_FILE", bmpath)

	Resrpt_FormSet1.run()



def cut_bm_by_dtm(kwargs):
	bmpath = kwargs["bmpath"]
	typepath = kwargs["typepath"]
	dtmname = kwargs["dtmname"]
	isCutAbove = kwargs["isCutAbove"]
	sub_x_geo = kwargs["sub_x_geo"]  
	sub_y_geo = kwargs["sub_y_geo"]  
	sub_z_geo = kwargs["sub_z_geo"]  


	DtmAssign_FormSet1= MMpy.FormSet("DTM_ASSIGN","16.1.1161.0")
	DtmAssign_FormSet1.set_field("PROJ","0")
	DtmAssign_FormSet1.set_field("BM_DEL_OUTSIDE","0")
	DtmAssign_FormSet1.set_field("BM_DEL_BELOW","0" if isCutAbove else "1")
	DtmAssign_FormSet1.set_field("BM_DEL_ABOVE","1" if isCutAbove else "0")
	DtmAssign_FormSet1.set_field("BM_BELOW","0" if isCutAbove else "1")
	DtmAssign_FormSet1.set_field("BM_ABOVE","1" if isCutAbove else "0")
	DtmAssign_FormSet1.set_field("SWITCH","1")
	DtmAssign_FormSet1.set_field("PBF","0")
	DtmAssign_FormSet1.set_field("TRANSFORM","0")
	DtmAssign_FormSet1.set_field("ORTHOGONAL","1")
	DtmAssign_FormSet1.set_field("BLOCKMODEL","1")
	DtmAssign_FormSet1.set_field("INPUT","1")
	DtmAssign_FormSet1.set_field("OVERWRITE_WFATTR_FIELD","0")
	DtmAssign_FormSet1.set_field("CLEAR_WFATTR_FIELD","0")
	DtmAssign_FormSet1.set_field("USE","0")
	DataGrid0 = MMpy.DataGrid(4,1)
	DataGrid0.set_column_info(0,4,6)
	DataGrid0.set_column_info(1,1,7)
	DataGrid0.set_column_info(2,2,-1)
	DataGrid0.set_column_info(3,3,4)
	DataGrid0.set_row(0, ["0","","",""])
	DtmAssign_FormSet1.set_field("GRID",DataGrid0.serialise())
	DtmAssign_FormSet1.set_field("POINTS","0")
	DtmAssign_FormSet1.set_field("FACTOR","0")
	DtmAssign_FormSet1.set_field("FCT_BELOW","0")
	DtmAssign_FormSet1.set_field("FCT_ABOVE","1")
	DtmAssign_FormSet1.set_field("DTMASSIGN_DEL_OUTSIDE","0")
	DtmAssign_FormSet1.set_field("CLEARBLOCKFIELD","0")
	DtmAssign_FormSet1.set_field("RL",sub_z_geo)
	DtmAssign_FormSet1.set_field("NORTH",sub_y_geo)
	DtmAssign_FormSet1.set_field("EAST", sub_x_geo)
	DtmAssign_FormSet1.set_field("SUB_BLOCKMODEL_BOOL","1")
	DtmAssign_FormSet1.set_field("DTMASSIGN_BLOCKMODEL_BOOL","0")
	DtmAssign_FormSet1.set_field("DTMASSIGN_DEL_BELOW","0")
	DtmAssign_FormSet1.set_field("DTMASSIGN_DEL_ABOVE","0")
	DtmAssign_FormSet1.set_field("OVERWRITE_CODE_FLD","0")
	DtmAssign_FormSet1.set_field("PURGE_CODE_FLD","0")
	DtmAssign_FormSet1.set_field("DTMSIDE","0")
	DtmAssign_FormSet1.set_field("WFTYPE",typepath)
	DtmAssign_FormSet1.set_field("ZFLD","RL")
	DtmAssign_FormSet1.set_field("YFLD","NORTH")
	DtmAssign_FormSet1.set_field("XFLD","EAST")
	DtmAssign_FormSet1.set_field("FILTER","0")
	DtmAssign_FormSet1.set_field("FTYPE","0")
	DtmAssign_FormSet1.set_field("MMFILE",bmpath)
	DtmAssign_FormSet1.set_field("DTMFILE",dtmname)
	DtmAssign_FormSet1.run()

class BlockModelMetadata:
	def __init__(self, cnxn, bmid):
		self._bmid = bmid
		self._cnxn = cnxn
		self._read_metadata()
	def _read_metadata(self):
		if self._cnxn and self._bmid:
			cmd = "SELECT * FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] WHERE BM_ID = %d" % (self._cnxn.dbsvy, self._bmid)
			self._metadata = {colname: value[0] for colname, value in 
				self._cnxn.exec_query("SELECT * FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] WHERE BM_ID = %d" % (self._cnxn.dbsvy, self._bmid)).get_columns(header = True).items()}
		else:
			self._metadata = {}

	@property
	def is_empty(self):			return self._metadata == {}
	@property
	def current_date(self):		return datetime.strptime(self._metadata["CURRENT_DATE"], "%Y-%m-%d") if self._metadata["CURRENT_DATE"] else None
	@current_date.setter
	def current_date(self, value):
		now = datetime.now().replace(microsecond = 0)
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [CURRENT_DATE] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET DATE_TRANSFERED = CONVERT(smalldatetime, '%s', 121) WHERE BM_ID = %d" % (self._cnxn.dbsvy, now, self._bmid))
		self._metadata["CURRENT_DATE"] = value
		self._metadata["DATE_TRANSFERED"] = now
	@property
	def name(self):				return self._metadata["NAME"]
	@name.setter
	def name(self, value):
		cmd = "UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [NAME] = N'%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid)
		self._cnxn.exec_query(cmd)
		self._metadata["NAME"] = value
	@property
	def path(self):
		ret = self._cnxn.exec_query("SELECT [PATH] FROM [{0}].[dbo].[NM_BLOCKMODEL_METADATA] WHERE BM_ID = {1}".format(self._cnxn.dbsvy, self._bmid))
		return ret.get_row()
	@path.setter
	def path(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [PATH] = N'%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["PATH"] = value
	@property
	def layer(self):			return self._metadata["LAYER"]
	@layer.setter
	def layer(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [LAYER] = N'%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["LAYER"] = value
	@property
	def geounit(self):			return self._metadata["GEOUNIT"]
	@geounit.setter
	def geounit(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [GEOUNIT] = N'%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["GEOUNIT"] = value

	@property
	def xmin(self):				return self._metadata["XMIN"]
	@xmin.setter
	def xmin(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [XMIN] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["XMIN"] = float(value)
	@property
	def xmax(self):				return self._metadata["XMAX"]
	@xmax.setter
	def xmax(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [XMAX] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["XMAX"] = float(value)
	@property
	def ymin(self):				return self._metadata["YMIN"]
	@ymin.setter
	def ymin(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [YMIN] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["YMIN"] = float(value)
	@property
	def ymax(self):				return self._metadata["YMAX"]
	@ymax.setter
	def ymax(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [YMAX] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["YMAX"] = float(value)
	@property
	def zmin(self):				return self._metadata["ZMIN"]
	@zmin.setter
	def zmin(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [ZMIN] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["ZMIN"] = float(value)
	@property
	def zmax(self):				return self._metadata["ZMAX"]
	@zmax.setter
	def zmax(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [ZMAX] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, value, self._bmid))
		self._metadata["ZMAX"] = float(value)
	
	@property
	def date_created(self):		return datetime.strptime(self._metadata["DATE_INSERTED"], "%Y-%m-%d")
	@property
	def created_by(self):		return self._metadata["INSERTED_BY"]
	@property
	def date_updated(self):		return datetime.strptime(self._metadata["DATE_UPDATED"], "%Y-%m-%d")
	@property
	def updated_by(self):		return self._metadata["UPDATED_BY"]
	@property
	def date_transfered(self):	return datetime.strptime(self._metadata["DATE_TRANSFERED"], "%Y-%m-%d")

	@property
	def is_need_transfer(self):	return self._metadata["IS_NEED_TRANSFER"]
	@is_need_transfer.setter
	def is_need_transfer(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [IS_NEED_TRANSFER] = %d WHERE BM_ID = %d" % (self._cnxn.dbsvy, int(value), self._bmid))
		self._metadata["IS_NEED_TRANSFER"] = bool(value)

	@property
	def expansion(self):	return self._metadata["EXPANSION"]
	@expansion.setter
	def expansion(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [EXPANSION] = %d WHERE BM_ID = %d" % (self._cnxn.dbsvy, int(value), self._bmid))
		self._metadata["EXPANSION"] = int(value)

	@property
	def is_use_dynamic_density(self):	return self._metadata["IS_USE_DYNAMIC_DENSITY"]
	@is_use_dynamic_density.setter
	def is_use_dynamic_density(self, value):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [IS_USE_DYNAMIC_DENSITY] = %d WHERE BM_ID = %d" % (self._cnxn.dbsvy, int(value), self._bmid))
		self._metadata["IS_USE_DYNAMIC_DENSITY"] = bool(value)

	def trigger_update(self):
		self._cnxn.exec_query("UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [NAME] = '%s' WHERE BM_ID = %d" % (self._cnxn.dbsvy, self.name, self._bmid))
		self._read_metadata()

class BlockModel:
	def __init__(self, bmpath, main_app):
		self._main_app = main_app
		self._bmpath = bmpath

		self.__create_variables()
		
	def __create_variables(self):
		if self._main_app.srvcnxn:
			cmd = """SELECT BM_ID FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] WHERE [PATH] = N'%s'""" % (self._main_app.srvcnxn.dbsvy, self._bmpath)
			self._bmid = self._main_app.srvcnxn.exec_query(cmd).get_column("BM_ID")
			self._bmid = self._bmid[0] if self._bmid else None
		else:
			self._bmid = None
		self._metadata = BlockModelMetadata(self._main_app.srvcnxn, self._bmid)
	def _exec_function(self, function, *args, **kwargs):
		try:					function(*args, **kwargs)
		except Exception as e:	raise Exception(str(e))
	def add(self):
		if not self.has_metadata:
			basename = os.path.basename(self._bmpath)
			basename = basename[:basename.rfind(".")] if "." in basename else basename
			cmd = "INSERT INTO [%s].[dbo].[NM_BLOCKMODEL_METADATA] (NAME, [PATH]) VALUES ('%s', '%s')" % (self._main_app.srvcnxn.dbsvy, basename, self._bmpath)
			self._main_app.srvcnxn.exec_query(cmd)
			self.__create_variables()
	def remove(self):
		if self.has_metadata:
			cmd = "DELETE FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] WHERE BM_ID = %d" % (self._main_app.srvcnxn.dbsvy, self._bmid)
			self._main_app.srvcnxn.exec_query(cmd)
			self._bmid = None
			self._metadata = BlockModelMetadata(self._main_app.srvcnxn, self._bmid)
	@property
	def metadata(self):			return self._metadata if self.has_metadata else None
	@property
	def has_metadata(self):		return not self._metadata.is_empty

	def save(self):
		self._main_app.srvcnxn.commit()

	#
	def assign_wireframes(self, kwargs):
		kwargs["bmpath"] = self._bmpath
		self._exec_function(assign_wireframes, kwargs)
	def optimize(self, kwargs):
		kwargs["bmpath"] = self._bmpath
		self._exec_function(optimize_bm, kwargs)
		outpath = kwargs.get("outpath")
		ret = None if not outpath else BlockModel(outpath, self._main_app)
		return ret
	def create_blank(self, kwargs):
		kwargs["bmpath"] = self._bmpath
		self._exec_function(create_blank_bm, kwargs)
	def merge(self, kwargs):
		kwargs["bmpath"] = self._bmpath
		self._exec_function(merge_bm, kwargs)
		outpath = kwargs.get("outpath")
		ret = None if not outpath else BlockModel(outpath, self._main_app)
		return ret
	def report_bm(self, kwargs):
		kwargs["bmpath"] = self._bmpath
		self._exec_function(report_bm, kwargs)

	def create_dtm_from_string(self, kwargs):
		self._exec_function(create_dtm_from_string, kwargs)

	def cut_bm_by_dtm(self, kwargs):
		kwargs["bmpath"] = self._bmpath

		self._exec_function(cut_bm_by_dtm, kwargs)

	#
	@property
	def str_bounds_path(self):		return os.path.join(os.path.dirname(self._bmpath), "%s.STR" % self.name)
	@property
	def tri_wireframes_path(self):	return os.path.join(os.path.dirname(os.path.dirname(self._bmpath)), self._main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1], "%s.TRIDB" % self.name)
	@property
	def backup_path(self):			return os.path.join(os.path.dirname(self._bmpath), "%s_BACKUP.DAT" % self.name)
	@property
	def unit(self):					return os.path.basename(self.unit_path)
	@property
	def unit_path(self):			return os.path.dirname(os.path.dirname(self._bmpath))
	@property
	def layer(self):				return os.path.basename(os.path.dirname(os.path.dirname(os.path.dirname(self._bmpath))))
	@property
	def name(self):
		basename = os.path.basename(self._bmpath)
		basename = basename[:basename.rfind(".")] if "." in basename else basename
		return basename
	@property
	def path(self):					return os.path.join(os.path.dirname(self._bmpath), "%s.DAT" % self.name)
	@property
	def exists(self):				return os.path.exists(self._bmpath)
	@property
	def is_defined(self):			return os.path.exists(self.str_bounds_path) and os.path.exists(self.tri_wireframes_path)

	@property
	def blockmodel_date(self):
		f = MicromineFile()
		if not f.open(self.path):
			raise Exception("Невозможно открыть файл %s" % self.path)
		datefields = sorted([colname for colname in f.header if is_valid_date(colname)])
		f.close()

		if len(datefields) != 2:
			raise Exception("Невозможно определить текущее состояние блочной модели из-за неверной структуры файла")

		return fieldname_to_date(datefields[-1])

	def load(self, display_name = None, setId = None, hatch_field = None):
		if not hatch_field:
			hatch_field = date_to_fieldname(self.blockmodel_date)
		if not display_name:
			display_name = remove_file_extension(os.path.basename(self.path))
		loadBlockModel(self.path, display_name, setId, hatch_field)

