try:	import MMpy
except:	pass
import os
import sys
import inspect
import platform
import functools
import logging
import logging.handlers
from inspect import signature , getargspec
### Включение \ Отключение логирования ###
IS_LOG_ENABLE=True
LOGING_LEVEL = logging.DEBUG

def register_logging():
	if not IS_LOG_ENABLE:
		return
	try:
		logger = NLogger()
		_n_mine_loaded_modules = []
		for module in sys.modules:
			if hasattr(sys.modules[module],'LOG_INJECT_THIS_MODULE_ALLOW') and sys.modules[module].LOG_INJECT_THIS_MODULE_ALLOW == True:
				_n_mine_loaded_modules.append(sys.modules[module])

		_n_mine_loaded_modules = set(_n_mine_loaded_modules)

		for module in _n_mine_loaded_modules:
			classes = inspect.getmembers(module, lambda x:  inspect.isclass(x) and  not inspect.isbuiltin(x) and x.__module__ == module.__name__)
			for name ,cls in classes:
				for attr in cls.__dict__:
					if callable(getattr(cls, attr)) and not attr.startswith("__") :
						try:
							setattr(cls, attr, decorator(getattr(cls, attr),logger))
						except Exception as e:
							print( str(e))
	except Exception as e:
		logger.exception("Возникли ошибки при регистрации логгера")
		return
		 

def register_logging_for_plugins(module):
	if not IS_LOG_ENABLE:
		return
	try:
		logger = NLogger()
		classes = inspect.getmembers(module, lambda x:  inspect.isclass(x) and  not inspect.isbuiltin(x) and x.__module__ == module.__name__)
		for name ,cls in classes:
			for attr in cls.__dict__:
				if callable(getattr(cls, attr)) and not attr.startswith("__") :
					try:
						setattr(cls, attr, decorator(getattr(cls, attr),logger))
					except Exception as e:
						print( str(e))
	except Exception as e:
		logger.exception("Возникли ошибки при регистрации логгера")
		return



if IS_LOG_ENABLE: 
	class NLogger():
		def __init__(self, **kwargs):  
			try:
				_log_folder = MMpy.Project.path() + "LOG"
			except:	
				_log_folder = "C:\\TEMP\\LOG"

			if not os.path.exists(_log_folder):
					try:	
						os.mkdir(_log_folder)
					except:			
						_log_folder = os.getcwd()

			try:
				_user_name = os.getlogin() 
			except: _user_name = 'unknown'
			try:
				_platform = platform.uname()
				_pc_info = _platform.node
			except:  _pc_info = 'unknown'
			_log_filename = _log_folder + '\\' +_pc_info+'_'+_user_name+'.log'

			self._create_logger(_log_filename)

		def _create_logger(self, log_filename):
			"""
			Creates a logging object and returns it
			"""

			self._logger = logging.getLogger("nmine_logger")
			if not len(self._logger.handlers) :
				self._logger.setLevel(LOGING_LEVEL)
 
				handler = logging.handlers.RotatingFileHandler(log_filename, maxBytes=50000000, backupCount=5, encoding='utf-8')
				fmt = '%(asctime)s - [LINE:%(lineno)d] - %(filename)s # - %(levelname)s - %(message)s'
				formatter = logging.Formatter(fmt)
				handler.setFormatter(formatter)
				self._logger.addHandler(handler)
			
		def exception(self, message):
			self._logger.exception(message)

		def critical(self, message):
			if self._logger.isEnabledFor(logging.CRITICAL):
				self._logger.critical(message)

		def error(self, message):
			if self._logger.isEnabledFor(logging.ERROR):
				self._logger.error(message)

		def warning(self, message):
			if self._logger.isEnabledFor(logging.WARNING):
				self._logger.warning(message)

		def info(self, message):
			if self._logger.isEnabledFor(logging.INFO):
				self._logger.info(message)

		def debug(self, message):
			if self._logger.isEnabledFor(logging.DEBUG):
				self._logger.debug(message)

		def close(self):
			if self._logger.handlers:
				n_handlers = self._logger.handlers
				for handler in n_handlers:
					handler.close()
					self._logger.removeHandler(handler)



	def decorator(func, logger):
			if not hasattr(func, "__name__") or func.__name__ == 'wrapper':
				return func
 
			def wrapper(*args, **kwargs):
				try:
					args2, varargs, varkw, defaults = getargspec(func)
					if not varargs:
						sig = signature(func)
						lenSig = len(sig.parameters)
						lenArgs = len(args)
						lenKwargs = len(kwargs)

						args_1=args
						kwargs_1= kwargs

						if lenArgs  >  lenSig - lenKwargs:
							lenExtra =	lenArgs -  (lenSig - lenKwargs)
							lenExtra = -1 *  lenExtra
							args = args[:lenExtra]

					return func(*args, **kwargs)
				

				except Exception as e:
					err = " Exception in:  "
					err += ' Descr:' + func.__doc__	if 	func.__doc__ != None else ''
					if len(args) != 0:
						err += " Args: "
						err += ("-{}" * len(args) + "-").format(*args)
					if len(kwargs) != 0:
						err += " Kwargs: "
						err += str([ str(k)+' : '+str(v) for k,v in kwargs.items()])
					err += '\n'
					logger.exception(err)
					raise 


 
			
			return wrapper


else: # Если логирование выключено
	class NLogger():
		def __init__(self, **kwargs):
			pass
		def _create_logger(self):
			logger = None
			return logger
		def exception(self, message):
			pass
		def critical(self, message):
			pass

		def error(self, message):
			pass

		def warning(self, message):
			pass

		def info(self, message):
			pass

		def debug(self, message):
			pass





