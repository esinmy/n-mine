class ICheckCriterion():

	@staticmethod
	def CriterionName():
		raise NotImplementedError


	def __init__(self, headers, data):
		self._file_headers = headers
		self._file_data = data

		self._report_data = []

		self._result = 0

		self._row_indexes_to_delete =  {}

		self._initVars()

		self.run_checking()

	def _initVars(self):
	   raise NotImplementedError

	def run_checking(self):
		raise NotImplementedError

	@property
	def report_title(self):
		raise NotImplementedError

	@property
	def report_header(self):
		raise NotImplementedError

	@property
	def report_data(self):
		raise NotImplementedError

	@property
	def result(self):
		raise NotImplementedError


	@property
	def row_indexes_to_delete(self):
		raise NotImplementedError
