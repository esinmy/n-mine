﻿from PyQt4 import QtGui, QtCore
from mm.mmutils import MicromineFile
from utils.dh_checking.criterions.CollarCoordsCriterion  import CollarCoordsCriterion 
from utils.dh_checking.criterions.AssayOverlapCriterion  import AssayOverlapCriterion 
from utils.dh_checking.criterions.AssayIsCenterCoordsCriterion  import AssayIsCenterCoordsCriterion 
from utils.dh_checking.criterions.SurveyIsExistsCriterion  import SurveyIsExistsCriterion 
from utils.dh_checking.criterions.CollarDepthCriterion  import CollarDepthCriterion 
from utils.dh_checking.criterions.CollarDoublesCriterion  import CollarDoublesCriterion 
from utils.dh_checking.criterions.SurveyIsZeroDepthExistsCriterion  import SurveyIsZeroDepthExistsCriterion 
from utils.dh_checking.criterions.SurveyAzimuthCriterion  import SurveyAzimuthCriterion 
from utils.dh_checking.criterions.SurveyDepthCriterion  import SurveyDepthCriterion 
from utils.dh_checking.criterions.AssayFromToCriterion  import AssayFromToCriterion 
from utils.dh_checking.criterions.AssayElementsCriterion  import AssayElementsCriterion 
																						 				 
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DhChecking(QtCore.QObject):
	next_check_calling= QtCore.pyqtSignal(str) # Наименование проверки
	def __init__(self, main_app, params, *args, **kw):
		super(DhChecking, self).__init__(*args, **kw)

		self.main_app = main_app
		self.params = params
		self._reports = []

		self.check_module_params_stru = {'collar' : {'path' : None, 
													'fields' : {'id' : None,
																'east': None, 'north' : None, 'rl' : None, 'depth' : None}
																},
										'assay' : {'path' : None,
													'fields' : { 'id' : None,
																'from' : None, 'to' : None, 'length' : None, 
																 'xcollar' : None, 'ycollar' : None, 'zcollar' : None},
													'elements' : []
												},
										'survey' : {'path' : None,
													'fields' : { 'id' : None,
																'depth' : None, 'az' : None, 'dip' : None}
												}
									}


		self.headers = {}
		for table_name in self.check_module_params_stru:
			self.headers[table_name] = []

		self.data = {}
		for table_name in self.check_module_params_stru:
			self.data[table_name] = [[]]


		self.row_indexes_to_delete = {}
		for table_name in self.check_module_params_stru:
			self.row_indexes_to_delete[table_name] = []


		self.assay_elements = self.params['assay']['elements']

		self._check_params()



	def _check_params(self):
		tables_names = set(self.check_module_params_stru.keys())
		diff = tables_names - set (self.params.keys())
		if len(diff) > 0:
			raise Exception ("В настройках проверки отсутствуют парамметры таблиц: {0}".format(",".join(diff)))
		


		for table_name in tables_names:
			fields =  set(self.check_module_params_stru[table_name].keys())  
			diff = fields - set (self.params[table_name].keys())
			if len(diff) > 0:
				raise Exception ("В настройках проверки скважин для таблицы {0} отсутствуют парамметры : {1}".format(table_name ,",".join(diff)))

			fields =  set(self.check_module_params_stru[table_name]['fields'].keys())  
			diff = fields - set (self.params[table_name]['fields'].keys())
			if len(diff) > 0:
				raise Exception ("В настройках проверки скважин для таблицы {0} отсутствуют парамметры полей: {1}".format(table_name ,",".join(diff)))

		


	def copy_files_to_lists(self):
		tables_names = set(self.check_module_params_stru.keys())
				
		for table_name in tables_names:
			path = self.params[table_name]['path']
			fields = [ field for field in  self.params[table_name]['fields'].values()]
			self.headers[table_name] = [ header for header in self.params[table_name]['fields'].keys()]

			elements = []
			if 'elements' in self.params[table_name]:
				elements = list(self.params[table_name]['elements'])

			self.headers[table_name].extend(elements)

			self.headers[table_name].append('_row_index')

			f = MicromineFile()
			if not f.open(path):
				raise Exception("Невозможно открыть файл '%s'" % path)

			try:
				self.data[table_name] = [row + [i] for i, row in enumerate(f.read(columns = fields + elements, asstr = True))]
			except Exception as e:
				raise Exception( "Невозможно считать файл. %s" % str(e))
				f.close()
				return
			f.close()


	def exec(self):

		self.next_check_calling.emit("Копирование данных в память.")
		self.copy_files_to_lists()


		self.next_check_calling.emit(CollarCoordsCriterion.CriterionName())
		self._reports.append(CollarCoordsCriterion(self.headers, self.data))
		self.next_check_calling.emit(AssayOverlapCriterion.CriterionName())
		self._reports.append(AssayOverlapCriterion(self.headers, self.data))
		self.next_check_calling.emit(AssayIsCenterCoordsCriterion.CriterionName())
		self._reports.append(AssayIsCenterCoordsCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(SurveyIsExistsCriterion.CriterionName())
		self._reports.append(SurveyIsExistsCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(CollarDepthCriterion.CriterionName())
		self._reports.append(CollarDepthCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(CollarDoublesCriterion.CriterionName())
		self._reports.append(CollarDoublesCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(SurveyIsZeroDepthExistsCriterion.CriterionName())
		self._reports.append(SurveyIsZeroDepthExistsCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(SurveyAzimuthCriterion.CriterionName())
		self._reports.append(SurveyAzimuthCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(SurveyDepthCriterion.CriterionName())
		self._reports.append(SurveyDepthCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(AssayFromToCriterion.CriterionName())
		self._reports.append(AssayFromToCriterion(self.headers, self.data))	  
		self.next_check_calling.emit(AssayElementsCriterion.CriterionName())
		self._reports.append(AssayElementsCriterion(self.headers, self.data, self.assay_elements))	  
																					  		   
		result = 0
		for report in self._reports:
			if report.result > result:
				result = report.result

		# Если Код окончания = 1, то удаляем некорректные записи, если = 2, то можно не удалять, так как всё равно откатывать с сервера
		if result == 1:
			for key in self.row_indexes_to_delete:
				if key in report.row_indexes_to_delete:
					self.row_indexes_to_delete[key].extend(report.row_indexes_to_delete[key])


			tables_names = set(self.check_module_params_stru.keys())

			for table_name in tables_names:
				path = self.params[table_name]['path']
				f = MicromineFile()
				if not f.open(path):
					raise Exception("Невозможно открыть файл '%s'" % path)
				try:
					for row_index in sorted(set(self.row_indexes_to_delete[table_name]), reverse = True):
						f.delete_record(row_index+1)

				except Exception as e:
					raise Exception("Невозможно удалить запись. {0}".format( str(e)) )
					f.close()
					return
				f.close()





		return result

	@property
	def reports(self):
		return self._reports