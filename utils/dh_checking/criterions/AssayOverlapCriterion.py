﻿from utils.dh_checking.ICheckCriterion import ICheckCriterion 
from utils.validation import is_blank
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class AssayOverlapCriterion(ICheckCriterion):
	@staticmethod
	def CriterionName():
		return "Проверка пересечения интервалов."

	def __init__(self, headers, data):
		super(AssayOverlapCriterion, self).__init__(headers, data)

	def _initVars(self):
		self._report_header = ['№', 'Скважина', 'Сообщение']
		self._checking_table = 'assay'
		self._slave_tables = {'collar' : {} ,
								'survey' : {}
						}

	def run_checking(self):
		_id = self._file_headers[self._checking_table].index('id')
		_from = self._file_headers[self._checking_table].index('from')
		_to = self._file_headers[self._checking_table].index('to')

		ids_for_removing = []

		report_row_num = 1

		for row in  self._file_data[self._checking_table]:
			row[_from] = float(row[_from])
			row[_to] = float(row[_to])

		sorted_data = sorted(self._file_data[self._checking_table], key = lambda x: (x[_id], x[_from], x[_to]))

		cmp_id = "undefined"
		cmp_from = 0
		cmp_to = 0
		isCorrect = True
		for  row in sorted_data:
			if cmp_id == row[_id] and isCorrect:
				if cmp_to > float(row[_from]):
					report_row = [report_row_num, row[_id], "Пересечение интервалов"]
					report_row_num = report_row_num + 1
					self._report_data.append(report_row)
					ids_for_removing.append(row[_id])
					isCorrect = False

				if cmp_to < float(row[_from]):
					report_row = [report_row_num, row[_id], "Пропущенный интервал"]
					report_row_num = report_row_num + 1
					self._report_data.append(report_row)
					ids_for_removing.append(row[_id])
					isCorrect = False
			elif cmp_id != row[_id]:
				cmp_id = row[_id]
				isCorrect = True


			cmp_from = float(row[_from])
			cmp_to = float(row[_to])

		self.generate_row_idx_for_removing(set(ids_for_removing))

		if len(self._report_data) > 0:
			self._result = 1
 
	def generate_row_idx_for_removing(self, ids_for_removing):

		checking_table_id = self._file_headers[self._checking_table].index('id')
		checking_table_row_index = self._file_headers[self._checking_table].index('_row_index')

		for table in self._slave_tables:
			self._slave_tables[table]['idx_slave_id'] = self._file_headers[table].index('id')
			self._slave_tables[table]['idx_row_index']= self._file_headers[table].index('_row_index')
		

		self._row_indexes_to_delete[self._checking_table] = [ row[checking_table_row_index] for row in  self._file_data[self._checking_table] if row[checking_table_id] in ids_for_removing]

		for table in self._slave_tables:
			idx_slave_id = self._slave_tables[table]['idx_slave_id']
			idx_row_index = self._slave_tables[table]['idx_row_index']
			self._row_indexes_to_delete[table] = [ row[idx_row_index] for row in  self._file_data[table] if row[idx_slave_id] in ids_for_removing]
	@property
	def report_title(self):
		text = AssayOverlapCriterion.CriterionName() 
		behavior_descripton = ""
		if self._result == 1: 
			behavior_descripton = "(Некорректные данные пропущены)"
		elif self._result == 2: 
			behavior_descripton = "(Обновление отменено)"

		return text + " " + behavior_descripton

	@property
	def report_header(self):
		return self._report_header

	@property
	def report_data(self):
		return self._report_data

	@property
	def result(self):
		return self._result

	@property
	def row_indexes_to_delete(self):
		return self._row_indexes_to_delete

