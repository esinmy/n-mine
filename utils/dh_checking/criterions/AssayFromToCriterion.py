﻿from utils.dh_checking.ICheckCriterion import ICheckCriterion 
from utils.validation import is_blank

class AssayFromToCriterion(ICheckCriterion):
	@staticmethod
	def CriterionName():
		return "Проверка интервалов опробований"

	def __init__(self, headers, data):
		super(AssayFromToCriterion, self).__init__(headers, data)

	def _initVars(self):
		self._report_header = ['№', 'Скважина', 'Сообщение']
		self._checking_table = 'assay'
		self._slave_tables = {}

	def run_checking(self):
		id = self._file_headers[self._checking_table].index('id')
		_from = self._file_headers[self._checking_table].index('from')
		_to = self._file_headers[self._checking_table].index('to')

		ids_for_removing = []

		report_row_num = 1
		for row in self._file_data[self._checking_table]:
			if float(row[_from]) ==  float(row[_to]) :
				report_row = [report_row_num, row[id], "Файл опробований. Отметки интервалов От и До равны."]
				report_row_num = report_row_num + 1
				self._report_data.append(report_row)

			if float(row[_from]) >  float(row[_to]) :
				report_row = [report_row_num, row[id], "Файл опробований. Отметка От больше отметки До."]
				report_row_num = report_row_num + 1
				self._report_data.append(report_row)

		#self.generate_row_idx_for_removing(ids_for_removing)

		if len(self._report_data) > 0:
			self._result = 2
 
	def generate_row_idx_for_removing(self, ids_for_removing):

		checking_table_id = self._file_headers[self._checking_table].index('id')
		checking_table_row_index = self._file_headers[self._checking_table].index('_row_index')

		for table in self._slave_tables:
			self._slave_tables[table]['idx_slave_id'] = self._file_headers[table].index('id')
			self._slave_tables[table]['idx_row_index']= self._file_headers[table].index('_row_index')
		

		self._row_indexes_to_delete[self._checking_table] = [ row[checking_table_row_index] for row in  self._file_data[self._checking_table] if row[checking_table_id] in ids_for_removing]

		for table in self._slave_tables:
			idx_slave_id = self._slave_tables[table]['idx_slave_id']
			idx_row_index = self._slave_tables[table]['idx_row_index']
			self._row_indexes_to_delete[table] = [ row[idx_row_index] for row in  self._file_data[table] if row[idx_slave_id] in ids_for_removing]

	@property
	def report_title(self):
		text = AssayFromToCriterion.CriterionName()
		behavior_descripton = ""
		if self._result == 1: 
			behavior_descripton = "(Некорректные данные пропущены)"
		elif self._result == 2: 
			behavior_descripton = "(Обновление отменено)"

		return text + " " + behavior_descripton

	@property
	def report_header(self):
		return self._report_header

	@property
	def report_data(self):
		return self._report_data

	@property
	def result(self):
		return self._result

	@property
	def row_indexes_to_delete(self):
		return self._row_indexes_to_delete

