﻿from utils.dh_checking.ICheckCriterion import ICheckCriterion 
from utils.validation import is_blank
from itertools import groupby
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SurveyDepthCriterion(ICheckCriterion):
	@staticmethod
	def CriterionName():
		return "Проверка глубины инклинометрии скважин."

	def __init__(self, headers, data):
		super(SurveyDepthCriterion, self).__init__(headers, data)

	def _initVars(self):
		self._report_header = ['№', 'Скважина', 'Сообщение']
		self._checking_table = 'survey'
		self._slave_tables = {}

	def run_checking(self):
		id_survey = self._file_headers[self._checking_table].index('id')
		depth_survey = self._file_headers[self._checking_table].index('depth')

		id_collar = self._file_headers['collar'].index('id')
		depth_collar = self._file_headers['collar'].index('depth')

		survey_grouped  = groupby(sorted(self._file_data[self._checking_table], key = lambda x: x[id_survey]), key = lambda x: x[id_survey])

		survey_grouped_depth = [(key, list(zip(*group))[depth_survey])  for key, group in survey_grouped]
		survey_grouped_depth_float = [(key, map(float, group))  for key, group in survey_grouped_depth]
		survey_grouped_depth_float_max = [(key, max(group))  for key, group in survey_grouped_depth_float]

		collar_depth_dict = {}
		for row in self._file_data['collar']:
			collar_depth_dict[row[id_collar]] = float(row[depth_collar])

		report_row_num = 1

		for survey_row in survey_grouped_depth_float_max:
			survey_id = survey_row[0]
			if not survey_id in collar_depth_dict:
				continue

			if  survey_row[1] - collar_depth_dict[survey_id]  >  0.01:
				report_row = [report_row_num, survey_row[0], "Глубина инклинометрии больше глубины скважины"]
				report_row_num = report_row_num + 1
				self._report_data.append(report_row)

		for row in self._file_data[self._checking_table]:
			if is_blank(row[depth_survey]):
				report_row = [report_row_num, row[id_survey], "Глубина инклинометрии - пустое значение	"]
				report_row_num = report_row_num + 1
				self._report_data.append(report_row)
				continue

			if float(row[depth_survey]) < 0:
				report_row = [report_row_num, row[id_survey], "Глубина инклинометрии меньше 0"]
				report_row_num = report_row_num + 1
				self._report_data.append(report_row)




		ids_for_removing = []





		
		#self.generate_row_idx_for_removing(ids_for_removing)

		if len(self._report_data) > 0:
			self._result = 2
 
	def generate_row_idx_for_removing(self, ids_for_removing):

		checking_table_id = self._file_headers[self._checking_table].index('id')
		checking_table_row_index = self._file_headers[self._checking_table].index('_row_index')

		for table in self._slave_tables:
			self._slave_tables[table]['idx_slave_id'] = self._file_headers[table].index('id')
			self._slave_tables[table]['idx_row_index']= self._file_headers[table].index('_row_index')
		

		self._row_indexes_to_delete[self._checking_table] = [ row[checking_table_row_index] for row in  self._file_data[self._checking_table] if row[checking_table_id] in ids_for_removing]

		for table in self._slave_tables:
			idx_slave_id = self._slave_tables[table]['idx_slave_id']
			idx_row_index = self._slave_tables[table]['idx_row_index']
			self._row_indexes_to_delete[table] = [ row[idx_row_index] for row in  self._file_data[table] if row[idx_slave_id] in ids_for_removing]

	@property
	def report_title(self):
		text = SurveyDepthCriterion.CriterionName() 
		behavior_descripton = ""
		if self._result == 1: 
			behavior_descripton = "(Некорректные данные пропущены)"
		elif self._result == 2: 
			behavior_descripton = "(Обновление отменено)"

		return text + " " + behavior_descripton

	@property
	def report_header(self):
		return self._report_header

	@property
	def report_data(self):
		return self._report_data

	@property
	def result(self):
		return self._result

	@property
	def row_indexes_to_delete(self):
		return self._row_indexes_to_delete

