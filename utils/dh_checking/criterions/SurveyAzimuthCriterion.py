﻿from utils.dh_checking.ICheckCriterion import ICheckCriterion 
from utils.validation import is_blank
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SurveyAzimuthCriterion(ICheckCriterion):
	@staticmethod
	def CriterionName():
		return "Проверка азимутов и углов наклона инклинометрии скважин."

	def __init__(self, headers, data):
		super(SurveyAzimuthCriterion, self).__init__(headers, data)

	def _initVars(self):
		self._report_header = ['№', 'Скважина', 'Сообщение']
		self._checking_table = 'survey'
		self._slave_tables = {}

	def run_checking(self):
		id = self._file_headers[self._checking_table].index('id')
		azimuth = self._file_headers[self._checking_table].index('az')
		dip = self._file_headers[self._checking_table].index('dip')


		ids_for_removing = []


		surveys_have_az_empty = list(filter(lambda x: is_blank(x[azimuth]), self._file_data[self._checking_table]))
		surveys_have_az_empty_ids = []
		if surveys_have_az_empty:
			surveys_have_az_empty_ids = set(list(zip(*surveys_have_az_empty))[id])

		surveys_have_az_more_360 = list(filter(lambda x: float(x[azimuth]) > 360, self._file_data[self._checking_table]))
		surveys_have_az_more_360_ids = []
		if surveys_have_az_more_360:
			surveys_have_az_more_360_ids = set(list(zip(*surveys_have_az_more_360))[id])

		surveys_have_az_less_0 = list(filter(lambda x: float(x[azimuth]) < 0, self._file_data[self._checking_table]))
		surveys_have_az_less_0_ids = []
		if surveys_have_az_less_0:
			surveys_have_az_less_0_ids = set(list(zip(*surveys_have_az_less_0))[id])


		surveys_have_dip_empty = list(filter(lambda x: is_blank(x[dip]), self._file_data[self._checking_table]))
		surveys_have_dip_empty_ids = []
		if surveys_have_dip_empty:
			surveys_have_dip_empty_ids = set(list(zip(*surveys_have_dip_empty))[id])

		surveys_have_dip_more_90= list(filter(lambda x: float(x[dip]) > 90, self._file_data[self._checking_table]))
		surveys_have_dip_more_90_ids = []
		if surveys_have_dip_more_90:
			surveys_have_dip_more_90_ids = set(list(zip(*surveys_have_dip_more_90))[id])

		surveys_have_dip_less_minus_90 = list(filter(lambda x: float(x[dip]) < -90, self._file_data[self._checking_table]))
		surveys_have_dip_less_minus_90_ids = []
		if surveys_have_dip_less_minus_90:
			surveys_have_dip_less_minus_90_ids = set(list(zip(*surveys_have_dip_less_minus_90))[id])




		report_row_num = 1

		for id in surveys_have_az_empty_ids:
			report_row = [report_row_num, id, "Значение азимута = Пустое значение"]
			report_row_num = report_row_num + 1
			self._report_data.append(report_row)

		for id in surveys_have_az_more_360_ids:
			report_row = [report_row_num, id, "Значение азимута = Больше 360"]
			report_row_num = report_row_num + 1
			self._report_data.append(report_row)

		for id in surveys_have_az_less_0_ids:
			report_row = [report_row_num, id, "Значение азимута = Меньше 0"]
			report_row_num = report_row_num + 1
			self._report_data.append(report_row)

		for id in surveys_have_dip_empty_ids:
			report_row = [report_row_num, id, "Значение угла наклона = Пустое значение"]
			report_row_num = report_row_num + 1
			self._report_data.append(report_row)

		for id in surveys_have_dip_more_90_ids:
			report_row = [report_row_num, id, "Значение угла наклона = Больше 90 градусов"]
			report_row_num = report_row_num + 1
			self._report_data.append(report_row)

		for id in surveys_have_dip_less_minus_90_ids:
			report_row = [report_row_num, id, "Значение угла наклона = Меньше -90 градусов"]
			report_row_num = report_row_num + 1
			self._report_data.append(report_row)
		
		#self.generate_row_idx_for_removing(ids_for_removing)

		if len(self._report_data) > 0:
			self._result = 2
 
	def generate_row_idx_for_removing(self, ids_for_removing):

		checking_table_id = self._file_headers[self._checking_table].index('id')
		checking_table_row_index = self._file_headers[self._checking_table].index('_row_index')

		for table in self._slave_tables:
			self._slave_tables[table]['idx_slave_id'] = self._file_headers[table].index('id')
			self._slave_tables[table]['idx_row_index']= self._file_headers[table].index('_row_index')
		

		self._row_indexes_to_delete[self._checking_table] = [ row[checking_table_row_index] for row in  self._file_data[self._checking_table] if row[checking_table_id] in ids_for_removing]

		for table in self._slave_tables:
			idx_slave_id = self._slave_tables[table]['idx_slave_id']
			idx_row_index = self._slave_tables[table]['idx_row_index']
			self._row_indexes_to_delete[table] = [ row[idx_row_index] for row in  self._file_data[table] if row[idx_slave_id] in ids_for_removing]

	@property
	def report_title(self):
		text = SurveyAzimuthCriterion.CriterionName()
		behavior_descripton = ""
		if self._result == 1: 
			behavior_descripton = "(Некорректные данные пропущены)"
		elif self._result == 2: 
			behavior_descripton = "(Обновление отменено)"

		return text + " " + behavior_descripton

	@property
	def report_header(self):
		return self._report_header

	@property
	def report_data(self):
		return self._report_data

	@property
	def result(self):
		return self._result

	@property
	def row_indexes_to_delete(self):
		return self._row_indexes_to_delete

