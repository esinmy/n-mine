﻿from utils.dh_checking.ICheckCriterion import ICheckCriterion 
from utils.validation import is_blank
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class AssayIsCenterCoordsCriterion(ICheckCriterion):
	@staticmethod
	def CriterionName():
		return "Проверка существования центров проб."

	def __init__(self, headers, data):
		super(AssayIsCenterCoordsCriterion, self).__init__(headers, data)

	def _initVars(self):
		self._report_header = ['№', 'Сообщение']
		self._checking_table = 'assay'

	def run_checking(self):
		_xcollar = self._file_headers[self._checking_table].index('xcollar')
		_ycollar= self._file_headers[self._checking_table].index('ycollar')
		_zcollar= self._file_headers[self._checking_table].index('zcollar')

		_xcollar_list = list(zip(*self._file_data[self._checking_table]))[_xcollar]
		_ycollar_list = list(zip(*self._file_data[self._checking_table]))[_ycollar]
		_zcollar_list = list(zip(*self._file_data[self._checking_table]))[_zcollar]

		report_row_num = 1

		if all([is_blank(item) for item in _xcollar_list]):
			report_row = [report_row_num, "В файле опробований отсутствуют рассчитываемые координаты 'xcollar' цетров проб"]
			self._report_data.append(report_row)
			report_row_num = report_row_num + 1
			self._result = 2

		if all([is_blank(item) for item in _ycollar_list]):
			report_row = [report_row_num, "В файле опробований отсутствуют рассчитываемые координаты 'ycollar' цетров проб"]
			self._report_data.append(report_row)
			report_row_num = report_row_num + 1
			self._result = 2

		if all([is_blank(item) for item in _zcollar_list]):
			report_row = [report_row_num, "В файле опробований отсутствуют рассчитываемые координаты 'zcollar' цетров проб"]
			self._report_data.append(report_row)
			report_row_num = report_row_num + 1
			self._result = 2


	@property
	def report_title(self):
		text = AssayIsCenterCoordsCriterion.CriterionName() 
		behavior_descripton = ""
		if self._result == 1: 
			behavior_descripton = "(Некорректные данные пропущены)"
		elif self._result == 2: 
			behavior_descripton = "(Обновление отменено)"

		return text + " " + behavior_descripton

	@property
	def report_header(self):
		return self._report_header

	@property
	def report_data(self):
		return self._report_data

	@property
	def result(self):
		return self._result

	@property
	def row_indexes_to_delete(self):
		return self._row_indexes_to_delete

