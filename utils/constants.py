# ======================================================================
# ======================= DATABASE CONSTANTS ===========================
# ======================================================================

# Путь базы данных
SETTINGS_PATH = "C:\\ProgramData\\Micromine\\MICROMINE\\Settings.nmdb"
#SETTINGS_PATH = "C:\\N-Mine\\Settings.nmdb"
LOCK_EXTENSION = ".LCK"
DIR_ARCHIVE = "Архив"

PADDING = dict(padx = 3, pady = 2)

#
ID_USERGEO = "USERGEO"
ID_USERSVY = "USERSVY"
ID_USERDSN = "USERDSN"
ID_USERADMIN = "USERADMIN"
USER_ROLE_LIST = (ID_USERGEO, ID_USERSVY, ID_USERDSN, ID_USERADMIN)

#
ITEM_DIR = ".DIR"
ITEM_MODEL = ".MODEL"
ITEM_BM = ".BM"
ITEM_DATA = ".DAT"
ITEM_STRING = ".STR"
ITEM_TRIDB = ".TRIDB"
ITEM_WIREFRAME = ".WF"
ITEM_ANNOTATION = ".MMAXL"
ITEM_SECTIONPACK = ".SECPACK"
ITEM_SECTION = ".SECTION"
ITEM_STOCK = ".STOCK"
ITEM_STOCKSECTOR = ".STOCKSECTOR"
ITEM_TYPE_LIST = (ITEM_DIR, ITEM_MODEL, ITEM_BM, ITEM_DATA, ITEM_STRING, ITEM_TRIDB, ITEM_WIREFRAME, ITEM_ANNOTATION, ITEM_SECTIONPACK, ITEM_SECTION, ITEM_STOCK, ITEM_STOCKSECTOR)

# ИЗОБРАЖЕНИЯ
IMG_PATH = "gui/img"
IMG_ICONS_PATH = "/".join([IMG_PATH, "icons"])
ITEM_DIR_ICO_PATH = "/".join([IMG_ICONS_PATH, "folder.png"])
ITEM_FORMSET_ICO_PATH = "/".join([IMG_ICONS_PATH, "formset.png"])
ITEM_MODEL_ICO_PATH = "/".join([IMG_ICONS_PATH, "model.png"])
ITEM_BM_ICO_PATH = "/".join([IMG_ICONS_PATH, "blockmodel.png"])
ITEM_DATA_ICO_PATH = "/".join([IMG_ICONS_PATH, "data.png"])
ITEM_STRING_ICO_PATH = "/".join([IMG_ICONS_PATH, "string.png"])
ITEM_TRIDB_ICO_PATH = "/".join([IMG_ICONS_PATH, "tridb.png"])
ITEM_WIREFRAME_ICO_PATH = "/".join([IMG_ICONS_PATH, "wireframe.png"])
ITEM_ANNOTATION_ICO_PATH = "/".join([IMG_ICONS_PATH, "annotation.png"])
ITEM_SECTIONPACK_ICO_PATH = "/".join([IMG_ICONS_PATH, "sections.png"])
ITEM_SECTION_ICO_PATH = "/".join([IMG_ICONS_PATH, "section.png"])
ITEM_STOCK_ICO_PATH = "/".join([IMG_ICONS_PATH, "stock.png"])
ITEM_STOCKSECTOR_ICO_PATH = "/".join([IMG_ICONS_PATH, "stock.png"])

ITEM_ICO_DICT = {}
ITEM_ICO_DICT[ITEM_DIR] = ITEM_DIR_ICO_PATH
ITEM_ICO_DICT[ITEM_MODEL] = ITEM_MODEL_ICO_PATH
ITEM_ICO_DICT[ITEM_BM] = ITEM_BM_ICO_PATH
ITEM_ICO_DICT[ITEM_DATA] = ITEM_DATA_ICO_PATH
ITEM_ICO_DICT[ITEM_STRING] = ITEM_STRING_ICO_PATH
ITEM_ICO_DICT[ITEM_TRIDB] = ITEM_TRIDB_ICO_PATH
ITEM_ICO_DICT[ITEM_WIREFRAME] = ITEM_WIREFRAME_ICO_PATH
ITEM_ICO_DICT[ITEM_ANNOTATION] = ITEM_ANNOTATION_ICO_PATH
ITEM_ICO_DICT[ITEM_SECTIONPACK] = ITEM_SECTIONPACK_ICO_PATH
ITEM_ICO_DICT[ITEM_SECTION] = ITEM_SECTION_ICO_PATH
ITEM_ICO_DICT[ITEM_STOCK] = ITEM_STOCK_ICO_PATH
ITEM_ICO_DICT[ITEM_STOCKSECTOR] = ITEM_STOCKSECTOR_ICO_PATH

ITEM_CATEGORY_GEO = "ITEM_GEO"
ITEM_CATEGORY_SVY = "ITEM_SVY"
ITEM_CATEGORY_DSN = "ITEM_DSN"
ITEM_CATEGORY_USER = "ITEM_USER"
ITEM_CATEGORY_SECTIONS = "ITEM_SECTIONPACK"
ITEM_CATEGORY_LIST = (ITEM_CATEGORY_GEO, ITEM_CATEGORY_SVY, ITEM_CATEGORY_DSN, ITEM_CATEGORY_USER, ITEM_CATEGORY_SECTIONS)

ITEM_CATEGORY_GEO_NAME = "Геология"
ITEM_CATEGORY_SVY_NAME = "Маркшейдерия"
ITEM_CATEGORY_DSN_NAME = "Проектирование"
ITEM_CATEGORY_USER_NAME = "Проводник"
ITEM_CATEGORY_SECTIONS_NAME = "Разрезы"
ITEM_CATEGORY_DICT = {
				ITEM_CATEGORY_GEO: ITEM_CATEGORY_GEO_NAME, ITEM_CATEGORY_SVY: ITEM_CATEGORY_SVY_NAME,
				ITEM_CATEGORY_DSN: ITEM_CATEGORY_DSN_NAME, ITEM_CATEGORY_SECTIONS: ITEM_CATEGORY_SECTIONS_NAME}


ITEM_ROLE_CATEGORY_DICT = {ID_USERGEO: ITEM_CATEGORY_GEO, ID_USERSVY: ITEM_CATEGORY_SVY, ID_USERDSN: ITEM_CATEGORY_DSN, ID_USERADMIN: ITEM_CATEGORY_USER}

# режимы
MODE_DISPLAY_DATA = "DISPLAY_DATA"
MODE_GEO_WF_VALIDATION = "GEO_WF_VALIDATION"
MODE_GEO_UPDATE_BM = "GEO_UPDATE_BM"
MODE_GEO_MERGE_BM = "GEO_MERGE_BM"
MODE_GEO_TRANSFER_BM = "GEO_TRANSFER_BM"
MODE_GEO_BACKUP_BM = "GEO_BACKUP_BM"
MODE_SVY_BACKUP_WF = "SVY_BACKUP_WF"
MODE_SVY_WF_VALIDATION = "SVY_WF_VALIDATION"
MODE_SVY_UPDATE_BM = "SVY_UPDATE_BM"
MODE_SVY_REFRESH_BM = "SVY_REFRESH_BM"
MODE_SVY_REPORT_BM = "SVY_REPORT_BM"
MODE_SVY_UPDATE_GRAPHIC = "SVY_UPDATE_GRAPHIC"
MODE_SVY_RESTORE_GEOLOGY = "SVY_RESTORE_GEOLOGY"
MODE_SVY_STOCK_CALCPLOT = "SVY_STOCK_CALCPLOT"
MODE_DSN_WF_VALIDATION = "DSN_WF_VALIDATION"
MODE_DSN_UPDATE_BM = "DSN_UPDATE_BM"
MODE_DSN_REPORT_BM = "DSN_REPORT_BM"
MODE_CUSTOM_REPORT_BM = "CUSTOM_REPORT_BM"
MODE_AUTO_VIZEX_PLOT = "AUTO_VIZEX_PLOT"
MODE_GET_MIDDLE_LINE = "GET_MIDDLE_LINE"

MODES = (MODE_DISPLAY_DATA, MODE_GEO_WF_VALIDATION, MODE_GEO_UPDATE_BM, MODE_GEO_MERGE_BM, MODE_GEO_TRANSFER_BM, MODE_SVY_WF_VALIDATION, MODE_SVY_UPDATE_BM,
			MODE_SVY_REFRESH_BM, MODE_SVY_REPORT_BM, MODE_SVY_UPDATE_GRAPHIC, MODE_SVY_STOCK_CALCPLOT, MODE_CUSTOM_REPORT_BM,
			MODE_DSN_WF_VALIDATION, MODE_DSN_UPDATE_BM, MODE_DSN_REPORT_BM, MODE_AUTO_VIZEX_PLOT, MODE_SVY_RESTORE_GEOLOGY, MODE_GET_MIDDLE_LINE)

COMMAND_GEO_CREATE_UNIT = "GEO_CREATE_UNIT"
COMMAND_GEO_DEFINE_MODEL = "GEO_DEFINE_MODEL"
COMMAND_GEO_ADD_ELEMENT = "GEO_ADD_ELEMENT"
COMMAND_SVY_CREATE_UNIT = "SVY_CREATE_UNIT"
COMMAND_SVY_CREATE_SUBUNIT = "SVY_CREATE_SUBUNIT"
COMMAND_SVY_DEFINE_MODEL = "SVY_DEFINE_MODEL"
COMMAND_SVY_ADD_ELEMENT = "SVY_ADD_ELEMENT"
COMMAND_LOAD_DRILLHOLES = "LOAD_DRILLHOLES"
COMMAND_LOAD_TRENCHES = "LOAD_TRENCHES"
COMMAND_UPDATE_DHDB = "UPDATE_DHDB"
COMMAND_UPDATE_TRDB = "UPDATE_TRDB"
COMMAND_SURVEY_TO_DB_TRDB = "SURVEY_TO_DB_TRDB"
COMMAND_LOAD_SVY_POINTS = "LOAD_SVY_POINTS"
COMMAND_SVY_CREATE_POINT = "SVY_CREATE_POINT"
COMMAND_SVY_EDIT_POINT = "SVY_EDIT_POINT"
COMMAND_SVY_REMOVE_POINT = "SVY_REMOVE_POINT"
COMMAND_SVY_BROWSE_REGISTER = "SVY_BROWSE_REGISTER"
COMMAND_SVY_ADD_DIRECTION = "ADD_DIRECTION"
COMMAND_SVY_EDIT_DIRECTION = "EDIT_DIRECTION"
COMMAND_DSN_CREATE_UNIT = "DSN_CREATE_UNIT"
COMMAND_DSN_DEFINE_MODEL = "DSN_DEFINE_MODEL"
COMMAND_DSN_ADD_ELEMENT = "DSN_ADD_ELEMENT"
COMMAND_DSN_AUTO_CREATE_SOLID = "DSN_AUTO_CREATE_SOLID"
COMMAND_IMPORT_GSI = "IMPORT_GSI"
COMMAND_EXPORT_STRING_TO_SIM = 'COMMAND_EXPORT_STRING_TO_SIM'

COMMANDS = (COMMAND_GEO_CREATE_UNIT, COMMAND_GEO_DEFINE_MODEL, COMMAND_GEO_ADD_ELEMENT, COMMAND_SVY_CREATE_UNIT, COMMAND_SVY_DEFINE_MODEL, COMMAND_SVY_ADD_ELEMENT, COMMAND_LOAD_SVY_POINTS,
			COMMAND_SVY_CREATE_POINT, COMMAND_SVY_EDIT_POINT, COMMAND_SVY_ADD_DIRECTION, COMMAND_SVY_EDIT_DIRECTION, COMMAND_DSN_CREATE_UNIT, COMMAND_DSN_DEFINE_MODEL, COMMAND_DSN_ADD_ELEMENT,
			COMMAND_DSN_AUTO_CREATE_SOLID, COMMAND_IMPORT_GSI, COMMAND_EXPORT_STRING_TO_SIM)

# Идентификаторы полей

ID_EAST, ID_X = "EAST", "X"
ID_NORTH, ID_Y = "NORTH", "Y"
ID_RL, ID_Z = "RL", "Z"
ID_JOIN = "JOIN"

ID_GEO_MAINDIR = "GEO_MAINDIR"
ID_GEO_UPREFIX = "GEO_UPREFIX"
ID_GEO_UDSPNAME = "GEO_UDSPNAME"
ID_GEO_USUBDIRS = "GEO_USUBDIRS"
ID_GEO_UHIDEDIRS = "GEO_UHIDEDIRS"
ID_GEO_FORMSETS = "GEO_FORMSETS"
ID_SVY_MAINDIR = "SVY_MAINDIR"
ID_SVY_UPREFIX = "SVY_UPREFIX"
ID_SVY_UDSPNAME = "SVY_UDSPNAME"
ID_SVY_IS_MINETECHGRIDBIND = "SVY_IS_MINETECHGRIDBIND"
ID_SVY_IS_MINESUBUNITBIND = "SVY_IS_MINESUBUNITBIND"  
ID_SVY_MINEBINDATTRNAME = "SVY_MINEBINDATTRNAME"  
ID_SVY_USUBDIRS = "SVY_USUBDIRS"
ID_SVY_UHIDEDIRS = "SVY_UHIDEDIRS"
ID_SVY_FORMSETS = "SVY_FORMSETS"
ID_DSN_MAINDIR = "DSN_MAINDIR"
ID_DSN_UPREFIX = "DSN_UPREFIX"
ID_DSN_UDSPNAME = "DSN_UDSPNAME"
ID_DSN_USUBDIRS = "DSN_USUBDIRS"
ID_DSN_UHIDEDIRS = "DSN_UHIDEDIRS"
ID_DSN_FORMSETS = "DSN_FORMSETS"
ID_DSN_IS_MINENAMING1 = "DSN_IS_MINENAMING1"
ID_DSN_IS_MINENAMING2 = "DSN_IS_MINENAMING2"
ID_SIM_SET = "SIMSET"


ID_SRVNAME = "SRVNAME"
ID_DBGEO = "DBGEO"
ID_DBSVY = "DBSVY"
ID_MINELIST = "MINELIST"

ID_DBDIR = "DBDIR"
ID_EXPCOLLAR = "EXPCOLLAR"
ID_EXPASSAY = "EXPASSAY"
ID_EXPSVY = "EXPSVY"
ID_EXPLITH = "EXPLITH"
ID_DETCOLLAR = "DETCOLLAR"
ID_DETASSAY = "DETASSAY"
ID_DETSVY = "DETSVY"
ID_DETLITH = "DETLITH"
ID_TRCOLLAR = "TRCOLLAR"
ID_TRASSAY = "TRASSAY"
ID_TRSVY = "TRSVY"
ID_TRLITH = "TRLITH"

ID_MUNIT_MAINDIR = "MUNIT_MAINDIR"
ID_UPREFIX = "UPREFIX"
ID_UDSPNAME = "UDSPNAME"
ID_USUBDIRS = "USUBDIRS"

ID_USRFOLDERS = "USRFOLDERS"
ID_USRFORMSETS = "USRFORMSETS"

ID_IDXFLD = "IDXFLD"
ID_ROCKDENS = "ROCKDENS"
ID_GEOCODES = "GEOCODES"


ID_NETPATH = "NETPATH"
ID_PROJNAME = "PROJNAME"
ID_ALLOWUPD = "ALLOWUPD"
ID_PASS = "PASS"

ID_IDWELEM = "IDWELEM"
ID_IDWRADIUS = "IDWRADIUS"
ID_IDWSECTOR = "IDWSECTOR"
ID_IDWMINPTS = "IDWMINPTS"
ID_IDWMAXPTS = "IDWMAXPTS"
ID_IDWFACTOR1 = "IDWFACTOR1"
ID_IDWFACTOR2 = "IDWFACTOR2"
ID_IDWFACTOR3 = "IDWFACTOR3"

ID_MUDIR = "MUDIR"
ID_MUSUBDIR = "MUSUBDIR"

ID_SVYCAT = "SVYCAT"
ID_CONCCODE = "CONCCODE"
ID_CONCDENS = "CONCDENS"

ID_BMPATH = "BMPATH"
ID_XSIZE = "XSIZE"
ID_YSIZE = "YSIZE"
ID_ZSIZE = "ZSIZE"
ID_SUBXGEO = "SUBXGEO"
ID_SUBYGEO = "SUBYGEO"
ID_SUBZGEO = "SUBZGEO"
ID_SUBXSVY = "SUBXSVY"
ID_SUBYSVY = "SUBYSVY"
ID_SUBZSVY = "SUBZSVY"
ID_SUBXDSN = "SUBXDSN"
ID_SUBYDSN = "SUBYDSN"
ID_SUBZDSN = "SUBZDSN"
ID_ZOFFSET = "ZOFFSET"
ID_AUTOZ = "AUTOZ"

ID_GEO_BM_TEXT_HATCH = "GEO_BM_TEXT_HATCH"
ID_GEO_WIREFRAME_TEXT_COLOR = "GEO_WIREFRAME_TEXT_COLOR"
ID_GEO_STRING_TEXT_HATCH = "GEO_STRING_TEXT_HATCH"
ID_SVY_WIREFRAME_TEXT_HATCH = "SVY_WIREFRAME_TEXT_HATCH"
ID_SVY_STRING_TEXT_COLOR = "SVY_STRING_TEXT_COLOR"
ID_SVY_STRING_TEXT_HATCH = "SVY_STRING_TEXT_HATCH"
ID_DSN_WIREFRAME_TEXT_HATCH = "DSN_WIREFRAME_TEXT_HATCH"
ID_DSN_STRING_TEXT_COLOR = "DSN_STRING_TEXT_COLOR"
ID_DSN_STRING_TEXT_HATCH = "DSN_STRING_TEXT_HATCH"
ID_DSN_STRING_TEXT_LINE_SET = "DSN_STRING_TEXT_LINE_SET"

# Формы
ID_SETTINGS = "SETTINGS"
ID_FAVORITE = "FAVORITE"

# Геологические разности
GEO_VARIATIONS = ["Рудные породы", "Нерудные породы", "Вмещающие породы", "Прочее"]

# Настройка отчётов
ID_RPT_GEO_SVY_INDEXES = "RPT_GEO_SVY_INDEXES"
ID_RPT_DSN_INDEXES = "RPT_DSN_INDEXES"
ID_RPT_DSN_ELEMENTS	= "RPT_DSN_ELEMENTS"

# Коды классов маркшейдерских точек (для таблицы MP_CLASS)
ID_CLASS_CODE_CURRENT	= "CURRENT"
ID_CLASS_CODE_CTRL_HANG = "CTRL_HANG"
ID_CLASS_CODE_CLOSED	= "CLOSED"
ID_CLASS_CODE_EQUAL		= "EQUAL"
ID_CLASS_CODES = [ID_CLASS_CODE_CURRENT, ID_CLASS_CODE_CTRL_HANG, ID_CLASS_CODE_CLOSED, ID_CLASS_CODE_EQUAL]

# Принадлежность записи о выработке (Маркшейдеры, проектировщики и т.д). Для записи в поле MINE_OWNER таблицы VYRAB_P
ID_MINE_OF_SVY	= 1
ID_MINE_OF_DSN = 2


# ОБЯЗАТЕЛЬНЫЕ АТРИБУТЫ СТРИНГОВ МАРК ГРАФИКИ
SVY_STR_ATTR_TITLE = "Заголовок"
SVY_STR_ATTR_CODE = "Код"

# СТАНДАРТНЫЕ АТРИБУТЫ КАРКАСОВ
DEFAULT_ATTR_NAME = "Name"
DEFAULT_ATTR_TITLE = "Title"
DEFAULT_ATTR_CODE = "Code"

# АТРИБУТЫ ГЕОЛОГИЧЕСКИХ КАРКАСОВ
GEO_ATTR_LAYER = "ЗАЛЕЖЬ"
#GEO_ATTR_GEOUNIT = "ПАНЕЛЬ"			# Заменить везде на getValue(ID_GEO_UDSPNAME)
GEO_ATTR_MATERIAL = "МАТЕРИАЛ"
GEO_ATTR_DENSITY = "ОБЪЕМНЫЙ_ВЕС"
GEO_ATTR_DIR = "АЗИМУТ"
GEO_ATTR_DIP = "ПОГРУЖЕНИЕ"
GEO_ATTR_ROT = "ВРАЩЕНИЕ"
GEO_USER_ATTRIBUTES = [GEO_ATTR_LAYER, GEO_ATTR_MATERIAL, GEO_ATTR_DENSITY, GEO_ATTR_DIR, GEO_ATTR_DIP, GEO_ATTR_ROT]

# АТРИБУТЫ МАРКШЕЙДЕРСКИЙ КАРКАСОВ
SVY_ATTR_CATEGORY = "КАТЕГОРИЯ"
SVY_ATTR_LAYER = "ЗАЛЕЖЬ"
SVY_ATTR_BLOCK = "МАРК_БЛОК"
#SVY_ATTR_LONGWALL = "ЛЕНТА"		# Заменить везде на getValue(ID_SVY_MINEBINDATTRNAME)
SVY_ATTR_DATE = "ДАТА"
SVY_ATTR_MMYY = "ММ_ГГ"
SVY_ATTR_ACCURACY = "ТОЧНОСТЬ"
SVY_ATTR_LEGEND = "ЛЕГЕНДА"
SVY_USER_ATTRIBUTES = [SVY_ATTR_CATEGORY, SVY_ATTR_LAYER, SVY_ATTR_BLOCK, SVY_ATTR_DATE, SVY_ATTR_MMYY, SVY_ATTR_ACCURACY, SVY_ATTR_LEGEND]

# АТРИБУТЫ ПРОЕКТНЫХ КАРКАСОВ
DSN_ATTR_CATEGORY = "КАТЕГОРИЯ" 
DSN_ATTR_LAYER = "ЗАЛЕЖЬ"
DSN_ATTR_PROJECT = "ПРОЕКТ"
DSN_ATTR_STAGE = "СТАДИЯ"
DSN_ATTR_DATE = "ДАТА"
DSN_ATTR_SECTION = "СЕЧЕНИЕ"
DSN_USER_ATTRIBUTES = [DSN_ATTR_CATEGORY, DSN_ATTR_LAYER, DSN_ATTR_PROJECT, DSN_ATTR_STAGE, DSN_ATTR_DATE, DSN_ATTR_SECTION]

BMFLD_XSIZE = "_EAST"
BMFLD_YSIZE = "_NORTH"
BMFLD_ZSIZE = "_RL"

# ПОЛЯ БЛОЧНОЙ МОДЕЛИ
BMFLD_LAYER = "ЗАЛЕЖЬ"
# BMFLD_GEOUNIT = "ПАНЕЛЬ"							# Заменить везде на getValue(ID_GEO_UDSPNAME)
BMFLD_CURRMINED = "ДОБ_МЕСЯЦ"
BMFLD_TOTALMINED = "ДОБ_ВСЕГО"

BMFLD_GEOMATERIAL = "ГЕО_МАТЕРИАЛ"
BMFLD_GEOINDEX = "ГЕО_ИНДЕКС"
BMFLD_MATERIAL = "МАТЕРИАЛ"
BMFLD_INDEX = "ИНДЕКС"
BMFLD_DENSITY = "ОБЪЕМНЫЙ_ВЕС"

BMFLD_PROJECT_NAME = "ПРОЕКТ"
BMFLD_PROJECT_STAGE = "ОЧЕРЕДЬ"
BMFLD_PROJECT_MINENAME = "ПРОЕКТ_ВЫР-КА"
BMFLD_PROJECT_INTERVAL = "ПРОЕКТ_ИНТЕРВАЛ"
BMFLD_PROJECT_CATEGORY = "ПРОЕКТ_КАТЕГОРИЯ"
BMFLD_PROJECT_WORKTYPE = "ПРОЕКТ_РАБОТЫ"

BMFLD_SVYBLOCK = "МАРК_БЛОК"
#BMFLD_LONGWALL = "ЛЕНТА"	 # Заменить везде на getValue(ID_SVY_MINEBINDATTRNAME)
BMFLD_MINENAME = "ВЫРАБОТКА"
BMFLD_INTERVAL = "ИНТЕРВАЛ"
BMFLD_MINEWORK = "ВЫЕМ_РАБОТЫ"
BMFLD_FACTWORK = "ФАКТ_РАБОТЫ"
BMFLD_MINEDATE = "ВЫЕМ_ДАТА"
BMFLD_FACTDATE = "ФАКТ_ДАТА"
BMFLD_MINE_MINENAME = "ВЫЕМ_ВЫРАБОТКА"
BMFLD_MINE_INTERVAL = "ВЫЕМ_ИНТЕРВАЛ"

BMFIELDS_GEO = [BMFLD_GEOMATERIAL, BMFLD_GEOINDEX, BMFLD_MATERIAL, BMFLD_INDEX, BMFLD_DENSITY]
BMFIELDS_SVY = [BMFLD_SVYBLOCK, BMFLD_MINENAME, BMFLD_INTERVAL, BMFLD_MINEWORK, BMFLD_FACTWORK, BMFLD_MINEDATE, BMFLD_FACTDATE, BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL]
BMFIELDS_DSN = [BMFLD_PROJECT_NAME, BMFLD_PROJECT_STAGE, BMFLD_PROJECT_MINENAME, BMFLD_PROJECT_INTERVAL, BMFLD_PROJECT_CATEGORY, BMFLD_PROJECT_WORKTYPE]

BMFLD_SYS_FIELD = "SYS_FIELD"

BMFLD_SYS_MATERIAL = "SYS_%s" % BMFLD_MATERIAL
BMFLD_SYS_INDEX = "SYS_%s" % BMFLD_INDEX
BMFLD_SYS_DENSITY = "SYS_%s" % BMFLD_DENSITY
BMFLD_SYS_ROT = "SYS_ROT"
BMFLD_SYS_DIR = "SYS_DIR"
BMFLD_SYS_DIP = "SYS_DIP"

BMFLD_SYS_SVYBLOCK = "SYS_%s" % BMFLD_SVYBLOCK
#BMFLD_SYS_LONGWALL = "SYS_%s" % BMFLD_LONGWALL
BMFLD_SYS_MINENAME = "SYS_%s" % BMFLD_MINENAME
BMFLD_SYS_INTERVAL = "SYS_%s" % BMFLD_INTERVAL
BMFLD_SYS_MINEWORK = "SYS_%s" % BMFLD_MINEWORK
BMFLD_SYS_FACTWORK = "SYS_%s" % BMFLD_FACTWORK
BMFLD_SYS_FACTDATE = "SYS_%s" % BMFLD_FACTDATE

# необходимо дополнять		bmfld_sys_LONGWALL = "SYS_%s" % self.settings.getValue(ID_SVY_MINEBINDATTRNAME)
#									BMSYSFIELDS_SVY.append(bmfld_sys_LONGWALL)
BMSYSFIELDS_SVY = [BMFLD_SYS_SVYBLOCK,  BMFLD_SYS_MINENAME, BMFLD_SYS_INTERVAL, BMFLD_SYS_MINEWORK, BMFLD_SYS_FACTWORK, BMFLD_SYS_FACTDATE]

BMFLD_SYS_CURRMINED = "SYS_%s" % BMFLD_CURRMINED
BMFLD_SYS_TOTALMINED = "SYS_%s" % BMFLD_TOTALMINED

BMFLD_SYS_PROJECT_NAME = "SYS_%s" % BMFLD_PROJECT_NAME
BMFLD_SYS_PROJECT_STAGE = "SYS_%s" % BMFLD_PROJECT_STAGE
BMFLD_SYS_PROJECT_MINENAME = "SYS_%s" % BMFLD_PROJECT_MINENAME
BMFLD_SYS_PROJECT_INTERVAL = "SYS_%s" % BMFLD_PROJECT_INTERVAL
BMFLD_SYS_PROJECT_CATEGORY = "SYS_%s" % BMFLD_PROJECT_CATEGORY
BMFLD_SYS_PROJECT_WORKTYPE = "SYS_%s" % BMFLD_PROJECT_WORKTYPE

BMSYSFIELDS_DSN = [BMFLD_SYS_PROJECT_NAME, BMFLD_SYS_PROJECT_STAGE, BMFLD_SYS_PROJECT_MINENAME, BMFLD_SYS_PROJECT_INTERVAL, BMFLD_SYS_PROJECT_CATEGORY, BMFLD_SYS_PROJECT_WORKTYPE]

#

MMTYPE_BM = "Блочная модель"
MMTYPE_TRIDB = "Каркасы"
MMTYPE_DATA = "Данные"
MMTYPE_STRING = "Стринги"

MMTYPE_LIST = (MMTYPE_BM, MMTYPE_TRIDB, MMTYPE_DATA, MMTYPE_STRING)
MMTYPE_DICT = {ITEM_TRIDB: MMTYPE_TRIDB, ITEM_DATA: MMTYPE_DATA, ITEM_STRING: MMTYPE_STRING, ITEM_BM: MMTYPE_BM}
MMTYPE_COMMAND_ID_DICT = {MMTYPE_BM: 507, MMTYPE_TRIDB: 506, MMTYPE_DATA: 437, MMTYPE_STRING: 486}

#
ITEM_CATEGORY_FORMSET_DICT = {ITEM_CATEGORY_GEO: ID_GEO_FORMSETS, ITEM_CATEGORY_SVY: ID_SVY_FORMSETS, ITEM_CATEGORY_DSN: ID_DSN_FORMSETS}


RPTFLD_VOLUME = "ОБЪЕМ"
RPTFLD_FACT_TONNAGE = "ТОННАЖ"
RPTFLD_FACT_DENSITY = "ПЛОТНОСТЬ"
RPTFLD_MINE_TONNAGE = "ВЫЕМ_ТОННАЖ"
RPTFLD_MINE_DENSITY = "ВЫЕМ_ПЛОТНОСТЬ"

BMVW_ITEM_MATERIAL = "BMVW_ITEM_MATERIAL"
BMVW_ITEM_PURPOSE = "BMVW_ITEM_PURPOSE"
BMVW_ITEM_MINEWORK = "BMVW_ITEM_MINEWORK"
BMVW_ITEM_SUMMARY = "BMVW_ITEM_SUMMARY"
BMVW_ITEM_TYPES = (BMVW_ITEM_MATERIAL, BMVW_ITEM_PURPOSE, BMVW_ITEM_MINEWORK, BMVW_ITEM_SUMMARY)