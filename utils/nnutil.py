try:	import MMpy
except:	pass

import xml.etree.cElementTree as ElementTree
from datetime import datetime
from utils.osutil import *
from mm.mmutils import *

# проверка названий панелей
def is_valid_svypanel(v):
	if v[0].lower() != "п":		return False
	if len(v) == 1:				return False
	if v[1:].isdigit():			return True
	numbers = v[1:].split("-")
	if len(numbers) != 2:		return False
	if not all(map(lambda x: x and x.isdigit(), numbers)):
		return False
	return True

# проверка дат
def is_valid_year(v):
	if len(v) != 2:				return False
	if not v.isdigit():			return False
	return True
def is_valid_short_date(v):
	if len(v) != 5:					return False
	if v[2] != "_":					return False
	numbers = v.split("_")
	if len(numbers) != 2:			return False
	if len(numbers[0]) != 2:		return False
	if not numbers[0].isdigit():	return False
	if int(numbers[0]) > 12:		return False
	if not numbers[1].isdigit():	return False
	return True

def is_valid_date(v):
	if len(v) != 7:					return False
	if v[4] != "-":					return False
	numbers = v.split("-")
	if len(numbers) != 2:			return False
	if len(numbers[0]) != 4:		return False
	if not numbers[0].isdigit():	return False
	if not numbers[1].isdigit():	return False
	if int(numbers[1]) > 12:		return False
	if int(numbers[1]) < 1:			return False
	return True


def date_to_fieldname(date):
	return "%d-%s" % (date.year, str(date.month).zfill(2))

def fieldname_to_date(name):
	if not is_valid_date(name):
		raise Exception("Неверный формат даты '%s'" % name)
	year, month = map(int, name.split("-"))
	return datetime(year, month, 1)

# def prev_date(date):
# 	if not is_valid_date:
# 		raise ValueError("Invalid date format '%s'" % date)

# 	month, year = map(int, date.split("_"))
# 	prevmonth = 12 if month == 1 else month - 1
# 	if prevmonth == 12:		prevyear = year - 1 if year > 0 else 99
# 	else:									prevyear = year
# 	prevmonth, prevyear = map(lambda x: str(x).zfill(2), [prevmonth, prevyear])
# 	return "_".join([prevmonth, prevyear])
# вычисление следующей даты
# def next_date(date):
# 	if not is_valid_date:
# 		raise ValueError("Invalid date format '%s'" % date)

# 	month, year = map(int, date.split("_"))
# 	nextmonth = 1 if month == 12 else month + 1
# 	if nextmonth == 1:		nextyear = year + 1 if year < 99 else 0
# 	else:									nextyear = year
# 	nextmonth, nextyear = map(lambda x: str(x).zfill(2), [nextmonth, nextyear])
# 	return "_".join([nextmonth, nextyear])

# вычисление предыдущей даты
def prev_date(date, ret = "str"):
	if type(date) == str:			date = fieldname_to_date(date)
	elif type(date) != datetime:	raise TypeError("'%s' - неверный тип даты" % str(type(date)))
	#
	year, month = date.year, date.month
	if month == 1:	prevyear, prevmonth = year-1, 12
	else:			prevyear, prevmonth = year, month-1
	prevdate = datetime(prevyear, prevmonth, 1)
	if ret == "str":	return date_to_fieldname(prevdate)
	if ret == "date":	return prevdate

def next_date(date, ret = "str"):
	if type(date) == str:			date = fieldname_to_date(date)
	elif type(date) != datetime:	raise TypeError("'%s' - неверный тип даты" % str(type(date)))
	#
	year, month = date.year, date.month
	if month == 12:		nextyear, nextmonth = year + 1, 1
	else:				nextyear, nextmonth = year, month + 1
	nextdate = datetime(nextyear, nextmonth, 1)
	if ret == "str":	return date_to_fieldname(nextdate)
	if ret == "date":	return nextdate


# работа с XML
def xml_to_tuple(xmlstring):
	try:
		xml = ElementTree.fromstring(xmlstring)
		oretypes, indicies = [], []
		for node in xml.iter("oretype"):
			oretypes.append(node.attrib["v"])
			indicies.append([])
			for child in node.getchildren():
				indicies[-1].append(child.attrib["v"])
	except:
		oretypes, indicies = (), ()
	return tuple(zip(oretypes, indicies))

def tuple_to_xml(d):
	if not d:		return ""
	root = ElementTree.Element("xml", version = "1.0")
	for oretype, indicies in d:
		oretypeNode = ElementTree.SubElement(root, "oretype", v = oretype)
		for index in indicies:
			ElementTree.SubElement(oretypeNode, "index", v = index)
	return ElementTree.tostring(root, "unicode")

# экспорт в Excel
def export_to_excel(path, data, header = None):
	import xlsxwriter as xl
	
	wb = xl.Workbook(path)
	ws = wb.add_worksheet()
	bold = wb.add_format({"bold": True})

	if header:
		for icol, col in enumerate(header):
			ws.write(0, icol, col, bold)

	for irow, row in enumerate(data, start = 1):
		for icol, item in enumerate(row):
			ws.write(irow, icol, item)
	wb.close()
# отчет блочной по блочной модели в Excel
def bmreport_to_excel(path, data):
	path = "D:\\test_report.xlsx"
	import xlsxwriter as xl
	from xlsxwriter.utility import xl_range


	fdls_mapping = ["Б", "М", "В", "Р", "И", "СУХЗ", "ЗАКЛ"]
	wb = xl.Workbook(path)
	ws = wb.add_worksheet()
	ws.set_column("A:A", 40)
	merge_format = wb.add_format({"border": 2, "valign": "vcenter", "align": "center"})
	# создание шапки
	# первая строка
	ws.merge_range("A2:A4", "Наименование", merge_format)
	ws.merge_range("B2:K2", "ОБЪЕМ (КУБ.М)", merge_format)
	ws.merge_range("L2:T2", "МАССА (Т)", merge_format)
	ncols = 8
	header = ["СОДЕРЖАНИЯ Богатая тяжелая", "МАССА (Т) Богатая тяжелая", "СОДЕРЖАНИЯ Богатая легкая", "МАССА (Т) Богатая легкая"]
	header += ["СОДЕРЖАНИЯ Медистая руда", "МАССА (Т) Медистая руда", "СОДЕРЖАНИЯ Вкрапленная руда", "МАССА (Т) Вкрапленная руда"]
	startCol = 20
	fr, to = startCol, startCol + ncols
	for i in range(8):
		cell_range = xl_range(1, fr, 1, to)
		fr = startCol+ncols*(i+1)+i+1
		to = fr + ncols
		ws.merge_range(cell_range, header[i], merge_format)
	# вторая строка
	header = ["Богатая тяжелая", "Богатая легкая", "Медистая руда", "Вкрапленная руда", "Порода тяжелая", "Порода легкая", "Бетон", "Сухая закладка"]
	for i, col in enumerate(header):
		cell_range = xl_range(2, i+1, 2, i+1)
		ws.set_column(cell_range, 20)
		cell_range = xl_range(2, i+1, 3, i+1)
		ws.merge_range(cell_range, col, merge_format)
	for i, col in enumerate(header[:-1]):
		pass


	wb.close()

























