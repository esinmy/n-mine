import pyodbc, pymssql

from utils.logger import  NLogger
LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


def generate_connection_string(server, dbname):
	return "DRIVER={SQL Server};Server=%s;Database=%s" % (server, dbname)

class CursorFetcher:
	def __init__(self, cursor):
		self._cursor = cursor
		self._columns = [row[0] for row in self._cursor.description]
		self._data = [list(row) for row in self._cursor]
	@property
	def columns(self):	return tuple(self._columns)
	def get_column(self, colname):
		if not self._data:
			return None
		#
		lowname = colname.lower()
		lowcolumns = list(map(lambda x: x.lower(), self._columns))
		if not lowname in lowcolumns:
			self._cursor.connection.close()
			raise Exception("Столбец '%s' в запросе не найден" % colname)
		icolumn = lowcolumns.index(lowname)
		return list(zip(*self._data))[icolumn]
	def get_columns(self, header = False):
		data = list(map(list, zip(*self._data)))
		if not header:		return data
		else:				return dict(zip(self._columns, data))
	def get_row(self, index):
		nrows, ncols = len(self._data), len(self._columns)
		if index > nrows-1 or index < -nrows:	return
		elif ncols == 1:				return self._data[index][0]
		else:							return self._data[index]
	def get_rows(self):
		return self._data.copy()


class DbConnection(object):
	def __init__(self, cnxnstring):
		self._cnxn = pyodbc.connect(cnxnstring)
		self._cursor = self._cnxn.cursor()

		self._dbsvy = None
		self._dbgeo = None
		self._location = None
	def exec_query(self, cmd, *args):
		try:					self._cursor.execute(cmd, *args)
		except Exception as e:	raise Exception("Невозможно выполнить запрос:\n%s\nОшибка: %s" % (cmd, str(e)))

		if self._cursor.description is not None:
			return CursorFetcher(self._cursor)


	def exec_query_many(self, cmd, *args):
		try:					self._cursor.executemany(cmd, *args)
		except Exception as e:	raise Exception("Невозможно выполнить запрос:\n%s\nОшибка: %s" % (cmd, str(e)))

		if self._cursor.description is not None:
			return CursorFetcher(self._cursor)

	def cursor_description(self):
		return [row[0] for row in self._cursor.description]

	def rollback(self):
		self._cnxn.rollback()

	def commit(self):
		self._cnxn.commit()
	def close(self):
		self._cnxn.close()

	@property
	def dbsvy(self):				return self._dbsvy
	@dbsvy.setter
	def dbsvy(self, value):			self._dbsvy = value
	@property
	def dbgeo(self):				return self._dbgeo
	@dbgeo.setter
	def dbgeo(self, value):			self._dbgeo = value
	@property
	def location(self):				return self._location
	@location.setter
	def location(self, value):		self._location = value	

	# 
	#
	def getPanelId(self, panelName, layerId = None, layerName = None, locationName = None):
		if self._cnxn is None:						raise Exception("Отсутствует подключение к серверу базы данных")
		if self._dbsvy is None:						raise Exception("Не задана база данных маркшейдерии")
		#
		locationName = self._location if locationName is None else locationName
		#
		if panelName is None:
			return None
		if layerId is None:
			if not layerName or not locationName:	raise Exception("Отсутствуют необходимые параметры для извлечения уникального номера залежи")
			layerId = self.getLayerId(layerName, locationName)
		#
		if layerId is None:
			return None
		#
		cmd = "SELECT PANEL_ID FROM [{0}].[dbo].[PANEL]  WHERE PANEL_NAME = N'{1}' AND LAYER_ID = {2}".format(self.dbsvy, panelName, layerId)
		result = self.exec_query(cmd).get_rows()
		panelId = result[0][0] if result else None
		return panelId

	def getLayerId(self, layerName, locationName):
		if self._cnxn is None:						raise Exception("Отсутствует подключение к серверу базы данных")
		if self._dbsvy is None:						raise Exception("Не задана база данных маркшейдерии")
		#
		if layerName is None or locationName is None:
			return
		#
		cmd = "SELECT LAYER_ID FROM [{0}].[dbo].[LAYER]	WHERE LAYER_DESCRIPTION = N'{1}' AND LOCATION = N'{2}'".format(self.dbsvy, layerName, locationName)
		result = self.exec_query(cmd).get_rows()
		layerId = result[0][0] if result else None
		return layerId

	def getLocalProjectId(self, locProjectName, layerId = None, layerName = None, locationName = None):
		if self._cnxn is None:						raise Exception("Отсутствует подключение к серверу базы данных")
		if self._dbsvy is None:						raise Exception("Не задана база данных маркшейдерии")
		#
		locationName = self._location if locationName is None else locationName
		#
		if locProjectName is None:
			return None
		#
		if layerId is None:
			if not layerName or not locationName:	raise Exception("Отсутствуют необходимые параметры для извлечения уникального номера залежи")
			layerId = self.getLayerId(layerName, locationName)
		#
		if layerId is None:
			return None
		#
		cmd = "SELECT LOC_PROJECT_ID FROM [{0}].[dbo].[LOCAL_PROJECT] WHERE LOC_PROJECT_NAME = N'{1}' AND LAYER_ID = {2}".format(self.dbsvy, locProjectName, layerId)
		result = self.exec_query(cmd).get_rows()
		localProjectId = result[0][0] if result else None
		return localProjectId

	def getTechGridId(self, techGridName, locationName):
		if self._cnxn is None:						raise Exception("Отсутствует подключение к серверу базы данных")
		if self._dbsvy is None:						raise Exception("Не задана база данных маркшейдерии")
		#
		if techGridName is None or locationName is None:
			return None
		#
		cmd = "SELECT TECH_GRID_ID FROM [{0}].[dbo].[TECH_GRID] WHERE TECH_GRID_NAME = N'{1}' AND LOCATION = N'{2}'".format(self.dbsvy, techGridName, locationName)
		result = self.exec_query(cmd).get_rows()
		techGridId = result[0][0] if result else None
		return techGridId

	def getLongWallId(self, longWallName, techGridId = None, techGridName = None, locationName = None):
		if self._cnxn is None:						raise Exception("Отсутствует подключение к серверу базы данных")
		if self._dbsvy is None:						raise Exception("Не задана база данных маркшейдерии")
		#
		locationName = self._location if locationName is None else locationName
		#
		if longWallName is None:
			return None
		#
		if techGridId is None:
			if techGridName is None:				raise Exception("Отсутствуют необходимые параметры для извлечения уникального номера технологической сети")
			techGridId = self.getTechGridId(techGridName, locationName)
		if techGridId is None:
			return None
		#
		cmd = "SELECT LONGWALL_ID FROM [{0}].[dbo].[LONGWALL] WHERE LONGWALL_NAME = ? AND TECH_GRID_ID = ?".format(self.dbsvy)
		params = (longWallName, techGridId)
		result = self.exec_query(cmd, params).get_rows()
		longWallId = result[0][0] if result else None
		return longWallId

	def getWorkCategory(self, purpose):
		if self._cnxn is None:						raise Exception("Отсутствует подключение к серверу базы данных")
		if self._dbsvy is None:						raise Exception("Не задана база данных маркшейдерии")
		#
		if purpose is None:
			return None
		#
		cmd = "SELECT MINE_NAME_CATEGORY FROM [{0}].[dbo].[LKP_MINE_NAMES_CATEGORY] WHERE CATEGORY_DESCRIPTION = N'{1}'".format(self.dbsvy, purpose)
		result = self.exec_query(cmd).get_rows()
		category = result[0][0] if result else None
		return category

	def getGeoUnitNamesByLocalProject(self, locProjectName, layerId = None, layerName = None, locationName = None):
		if self._cnxn is None:						raise Exception("Отсутствует подключение к серверу базы данных")
		if self._dbsvy is None:						raise Exception("Не задана база данных маркшейдерии")
		#
		locationName = self._location if locationName is None else locationName
		#
		if locProjectName is None:
			return []
		#
		if layerId is None:
			if not layerName or not locationName:	raise Exception("Отсутствуют необходимые параметры для извлечения уникального номера залежи")
			layerId = self.getLayerId(layerName, locationName)

		if layerId is None:
			return []
		#
		locProjectId = self.getLocalProjectId(locProjectName, layerId)
		cmd = """
			SELECT PANEL_NAME FROM [{0}].[dbo].[PANEL]
			WHERE PANEL_ID in (
				SELECT PANEL_ID FROM [{0}].[dbo].[LOCAL_PROJECT_PANELS]
				WHERE LOC_PROJECT_ID = {1}
			)""".format(self.dbsvy, locProjectId)
		result = self.exec_query(cmd).get_rows()
		geoUnitNames = [row[0] for row in result] if result else []
		return geoUnitNames
