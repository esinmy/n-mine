try:	import MMpy
except:	pass

import os, uuid
import shutil
from datetime import datetime, timedelta
import win32security

# текстовые проверки
def is_blank(v):
	return v.replace(" ", "") == ""

# числовые проверки 
def is_number(v):
	try:		float(v);	return True
	except:		return False
def is_nonnegative(v):
	if is_number(v) and float(v) >= 0:		return True
	else:		return False
def is_positive(v):
	if is_number(v) and float(v) > 0:		return True
	else:		return False
def is_nonpositive(v):
	if is_number(v) and float(v) <= 0:		return True
	else:		return False
def is_negative(v):
	if is_number(v) and float(v) < 0:		return True
	else:		return False
def in_range(v, a, b, include_left = True, include_right = True):
	gt = v >= a if include_left else v > a
	lt = v <= b if include_right else v < b
	return gt and lt


def get_file_owner(filepath):
	f = win32security.GetFileSecurity(filepath, win32security.OWNER_SECURITY_INFORMATION)
	(username, domain, sid_name_use) =  win32security.LookupAccountSid(None, f.GetSecurityDescriptorOwner())
	ret = username if not domain else "\\".join([domain, username])
	return ret
def get_current_user():
	domain = os.environ["USERDOMAIN"]
	username = os.environ["USERNAME"]
	ret = username if not domain else "\\".join([domain, username])
	return ret
def get_current_time():
	return datetime.now().replace(microsecond = 0)

# генерирование префикса из даты и времени
def get_temporary_prefix():
	return str(datetime.now()).replace("-", "").replace(":", "").replace(" ", "").replace(".", "") + "_"

# Функции для работы с файловыми путями
def get_file_extension(path):
	ret = path[path.rfind("."):] if "." in path else ""
	return ret
def set_file_extension(path, extension):
	if not extension:
		return path
	path = path[:path.rfind(".")] if "." in path else path
	ret = path + extension
	return ret
def remove_file_extension(path):
	return path[:path.rfind(".")] if "." in path else path
def temporary_filepath(dirpath, extension = ""):
	basename = set_file_extention(uuid.uuid4(), extension)
	dirpath = dirpath.rstrip("\\").rstrip("\\")
	return "\\".join([dirpath, basenames])

def get_full_save_path(path, extension = "", root = ""):
	root = root.rstrip("\\").rstrip("//") if root else os.getcwd()
	path = set_file_extension(path, extension)

	dirpath, basename = os.path.dirname(path), os.path.basename(path)
	retdir = dirpath if os.path.exists(dirpath) else os.path.join(root, path)
	ret = os.path.join(retdir) if os.path.exists(retdir) else None
	return ret
def get_full_open_path(path, extension = "", root = ""):
	root = root.rstrip("//").rstrip("\\") if root else os.getcwd()
	path = set_file_extension(path, extension)

	ret = path if os.path.exists(path) else os.path.join(root, path)
	ret = ret if os.path.exists(ret) else None
	return ret
def get_full_path(path, extension = "", root = "", savepath = False):
	ret = get_full_save_path(path, extension, root) if savepath else get_full_open_path(path, extension, root)
	return ret

def get_short_save_path(path, extension = "", root = ""):
	root = root.rstrip("//").rstrip("\\") if root else os.getcwd()
	path = set_file_extension(path, extension)

	dirpath, basename = os.path.dirname(path), os.path.basename(path)
	retdir = dirpath if os.path.exists(dirpath) else os.path.join(root, dirpath)
	ret = os.path.join(retdir, basename) if os.path.exists(retdir) else None
	ret = ret if ret is None else ret[len(root)+1:] if root else ret
	return ret
def get_short_open_path(path, extension = "", root = ""):
	root = root.rstrip("//").rstrip("\\") if root else os.getcwd()
	path = set_file_extension(path, extension)

	ret = path if os.path.exists(path) else os.path.join(root, path)
	ret = ret if os.path.exists(ret) else None
	ret = ret if ret is None else ret[len(root)+1:] if root else ret
	return ret
def get_short_path(path, extension = "", root = "", savepath = False):
	ret = get_short_save_path(path, extension) if savepath else get_short_open_path(path, extension)
	return ret

def remove_file(path):
	if os.path.exists(path):
		os.remove(path)



def clear_temporary_folder ():
	try:
		_tmpfolder = MMpy.Project.path() + "TEMPORARY_SCRIPT_FOLDER"
	except:	
		_tmpfolder = "D:\\TEMPORARY_SCRIPT_FOLDER"

	if os.path.exists(_tmpfolder):
		for filename in os.listdir(_tmpfolder):
			path = os.path.join(_tmpfolder, filename)
			try:			os.remove(path)
			except:			pass

		folderMetadata = os.path.join( _tmpfolder, "METADATA")
		if os.path.exists(folderMetadata):
			for filename in os.listdir(folderMetadata):
				path = os.path.join(folderMetadata, filename)
				try:			os.remove(path)
				except:			pass

# DsnUnitEdition.Galkin.Insert.Begin

def isDirTreeContentsFiles(path):
	if not os.path.exists(path):
		raise Exception("Папка не существует")
	allFiles = []
	for path, subdirs, files in os.walk(path):
		allFiles.extend(files)

	if len(allFiles) > 0:
		return True
	else:
		return False

# DsnUnitEdition.Galkin.Insert.End 