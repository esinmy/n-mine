import os, sqlite3, pickle, shutil
from uuid import uuid4
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from utils.constants import *
from utils.osutil import *

from __init__ import __version__  as nm_version, __settings_version__ as nm_settings_version

sqlite3.register_converter("OBJECT", pickle.loads)

from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class FormSet:
	def __init__(self, processName, SETTINGS_PATH = SETTINGS_PATH):
		if processName == ID_SETTINGS:		cmdId = 1
		elif processName == ID_FAVORITE:	cmdId = 2
		else:								cmdId = None
		self._cmdId = cmdId
		self._params = {}
		self._rootId = -2147483648

		if not os.path.exists(SETTINGS_PATH):
			SettingsDb.create(SETTINGS_PATH)
		else:
			# validate settings db
			settings = SettingsDb()
			settings.connect(SETTINGS_PATH)
			settings.close()


		cnxn = sqlite3.connect(SETTINGS_PATH, detect_types = sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
		cnxn.row_factory = sqlite3.Row
		self._cursor = cnxn.cursor()

		self._createRootFolder()
	def exec_query(self, cmd, *args, **kw):
		try:
			self._cursor.execute(cmd, *args, **kw)
			self._cursor.connection.commit()
		except Exception as e:
			self.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))

	def _createRootFolder(self):
		rootId = self.getFolderId("")
		if rootId is None:
			cmd = """
			insert into Folders (cmdId, folderId, parentId, name, path, expanded, sortOrder)
			values				({0}, 0, {1}, '', '', 0, 0)"""
			cmd = cmd.format(self._cmdId, self._rootId)
			self.exec_query(cmd)

	#
	def close(self):
		self._cursor.connection.close()
	def open(self, setId):

		cmd = "select prmName, prmValue from FLDVALUE where cmdId = %d and setId = %d" % (self._cmdId, setId)
		self.exec_query(cmd)

		data = self._cursor.fetchall()
		try:
			self._params = {} if not data else dict(data)	#{prmName: pickle.loads(prmValue) for prmName, prmValue in data}
		except Exception as e:
			raise Exception("Невозможно считать форму. %s" % str(e))
		# print (self._params)
	def getValue(self, prmName):
		return self._params.get(prmName)
	def setValue(self, prmName, prmValue):
		self._params[prmName] = prmValue

	def getSetId(self, setName):
		cmd = "select setId from PRMSETS where title = '%s' and cmdId = %d" % (setName, self._cmdId)
		self.exec_query(cmd)
		setId = self._cursor.fetchone()
		setId = setId["setId"] if setId else None
		return setId

	def deleteSetId(self, setId):
		cmd = "delete from FLDVALUE where cmdId = %d and setId = %d" % (self._cmdId, setId)
		self.exec_query(cmd)
		cmd = "delete from PRMSETS where cmdId = %d and setId = %d" % (self._cmdId, setId)
		self.exec_query(cmd)

	def folderHasChildren(self, folderId):
		cmd = "select folderId from Folders where parentId = %d" % folderId
		self.exec_query(cmd)
		hasChildren = self._cursor.fetchone()
		return hasChildren is not None

	def deleteFolderId(self, folderId):
		if self.folderHasChildren(folderId):
			return
		# delete from FLDVALUE
		cmd = """
		delete from FLDVALUE
		where cmdId = {0} and setId in 
			(select setId from PRMSETS P where P.cmdId = {0} and P.folderId = {1})"""
		self.exec_query(cmd.format(self._cmdId, folderId))
		# delete from PRMSETS
		cmd = "delete from PRMSETS where cmdId = {0} and folderId = {1}"
		self.exec_query(cmd.format(self._cmdId, folderId))
		# delete from Folders
		cmd = "delete from Folders where cmdId = {0} and FolderId = {1}"
		self.exec_query(cmd.format(self._cmdId, folderId))

	def deleteFolderTree(self, folderPath):
		if folderPath != "":
			cmd = "select folderId from Folders where cmdId = %d and path like '%s'" % (self._cmdId, folderPath) + " || '%' order by path DESC"
		else:
			cmd = "select folderId from Folders where cmdId = %d and path != '' order by path DESC" % (self._cmdId)

		self.exec_query(cmd)
		folderIds = self._cursor.fetchall()
		if folderPath == []:
			return

		for i, row in enumerate(folderIds):
			self.deleteFolderId(row["folderId"])

	def setExists(self, setName):
		setId = self.getSetId(setName)
		return setId is not None

	def getFolderId(self, folderPath):
		cmd = "select folderId from Folders where path = '%s'" % folderPath
		self.exec_query(cmd)
		folderId = self._cursor.fetchone()
		folderId = folderId["folderId"] if folderId else None
		return folderId

	def createFolder(self, folderPath):
		parentPath = "|".join(folderPath.split("|")[:-1])
		parentId = self.getFolderId(parentPath)
		if parentId is None:
			parentId = self._rootId

		folderId = self.getFolderId(folderPath)
		if folderId is None:	folderId = self.getNextFolderId()
		else:					return

		cmd = """
		insert into Folders (cmdId, folderId, parentId, name, path, expanded, sortOrder)
		values				({0}, {1}, {2}, "{3}", "{4}", {5}, {6})"""

		name = folderPath.split("|")[-1]
		cmd = cmd.format(self._cmdId, folderId, parentId, name, folderPath, 0, 0)
		self.exec_query(cmd)

	def createFolderTree(self, folderPath):
		folders = folderPath.split("|")
		currentPath = ""
		for i, folderName in enumerate(folders):
			currentPath = "|".join([currentPath, folderName]) if currentPath else folderName
			self.createFolder(currentPath)

	def getNextSetId(self):
		cmd = "select max(setId) setId from PRMSETS where cmdId = %d" % (self._cmdId)
		self.exec_query(cmd)
		maxId = self._cursor.fetchone()
		maxId = maxId["setId"] if maxId else None
		setId = maxId + 1 if maxId is not None else 1
		return setId

	def getNextFolderId(self):
		cmd = "select max(folderId) folderId from Folders where cmdId = %d" % (self._cmdId)
		self.exec_query(cmd)
		maxId = self._cursor.fetchone()
		maxId = maxId["folderId"] if maxId else None
		folderId = maxId + 1 if maxId is not None else 1
		return folderId

	def save(self, setPath = None, setId = None):
		if setPath is None and setId is None:
			return
		if not setPath is None: 
			path = setPath.split("|")
			folderPath, setName = "|".join(path[:-1]), path[-1]

			# check if set exists
			setExists = self.setExists(setName)
			#
			if setId is None:
				if setExists:		setId = self.getSetId(setName)
				else:				setId = self.getNextSetId()
			elif setExists:
				oldSetId = self.getSetId(setName)
				if oldSetId is not None and oldSetId != setId:
					self.deleteSetId(oldSetId)
			else:
				self.deleteSetId(setId)
		

			if not setExists:
				cmd = """
				insert into PRMSETS		(cmdId, setId, title, timestamp, lCreateDateTime, szCreateAuthor, GUID, folderId, version)
				values					({0}, {1}, "{2}", {3}, "{4}", "{5}", "{6}", {7}, {8})"""
			else:
				cmd = """
				update PRMSETS set
					title = "{2}",
					timestamp = {3},
					lEditDateTime = {4},
					szEditAuthor = "{5}",
					GUID = "{6}",
					folderId = {7},
					version = {8}
				where
					cmdId = {0} and	setId = {1}
				"""

			folderId = self.getFolderId(folderPath)
			if folderId is None:
				self.createFolderTree(folderPath)
				folderId = self.getFolderId(folderPath)
		else:
			if not self.isSetExistsById(setId):
				return
			cmd = """
				update PRMSETS set
					timestamp = {3},
					lEditDateTime = {4},
					szEditAuthor = "{5}",
					version = {8}
				where
					cmdId = {0} and	setId = {1}
				"""
			setName = ""
			folderId = 0
		currentUser = get_current_user()
		currentTime = int(get_current_time().timestamp())
		cmd = cmd.format(self._cmdId, setId, setName, currentTime, currentTime, currentUser, uuid.uuid4(), folderId, nm_settings_version)
		# insert into PRMSETS
		self.exec_query(cmd)

		# insert into FLDVALUE
		cmd = """delete from FLDVALUE where cmdId = %d and setId = %d""" % (self._cmdId, setId)
		self.exec_query(cmd)
		cmd = """insert into FLDVALUE (cmdId, setId, prmName, prmValue) values (%d, %d, ?, ?)""" % (self._cmdId, setId)
		for prmName, prmValue in self._params.items():
			# print (prmName, prmValue)
			prmValue = pickle.dumps(prmValue)
			self.exec_query(cmd, (prmName, prmValue))

		return setId

	def values(self):
		return self._params.copy()


	def MarkCurrentSettingsOriginal(self):
		""" 
			Если изменяем настройки, значит считаем, что они уже не эквивалентны сохраненным настройкам.
			И очищаем поле Title настроек по умолчанию.
		"""
		cmd = """
		update PRMSETS set
			title = ""
		where	cmdId = {0} and setId = 0
		"""
		cmd = cmd.format(self._cmdId)
		self.exec_query(cmd)
	
	def IsCurrentSettingsInStore(self):
		""" 
			Проверяем на равенство поля Title настроек по умолчанию полю Title одного из сохраненных ранее набора настроек
		"""
		cmd = """
			select title, path from  PRMSETS
			left join FOLDERS
			on FOLDERS.folderId = PRMSETS.folderId
			where PRMSETS.cmdId = {0} and setId !=0 and 
			'_@current-' || setId = (select title from PRMSETS where cmdId = {0} and setId =0)
		"""
		cmd = cmd.format(self._cmdId)
		self.exec_query(cmd)
		row = self._cursor.fetchone()	
		if row:
			return "%s (%s)" % (row["title"], row["path"])
		else:
			return None

	def GetSets(self, FolderId):
		cmd = """
		select cmdId, setId, title, szCreateAuthor, lCreateDateTime as "lCreateDateTime [lDateTime]",
				  szEditAuthor, lEditDateTime as "lEditDateTime [lDateTime]", szGeneralNotes, GUID, folderId from  PRMSETS
		where	cmdId = {0} and setId != 0 	and folderId  = {1}
		"""
		cmd = cmd.format(self._cmdId, FolderId)
		self.exec_query(cmd)
		lstResult = []
		for row in self._cursor:
			lstResult.append(dict(row))

		return lstResult

	def GetFoldersByParent(self, parentFolderId):
		cmd = """
		select cmdId, folderId, parentId, name, path,
				  expanded, sortOrder  from  FOLDERS
		where	cmdId = {0} and parentId = {1} 
		"""
		cmd = cmd.format(self._cmdId, parentFolderId)
		self.exec_query(cmd)
		lstResult = []
		for row in self._cursor:
			lstResult.append(dict(row))

		return lstResult
	
	def isSetExistsById(self, setId):
		cmd = "select setId from PRMSETS where setId = %d and cmdId = %d" % (setId, self._cmdId)
		self.exec_query(cmd)
		data = self._cursor.fetchone()
		return not data is None
	
	def isSetExistsByFullPath(self, path, title):
		cmd = """
		select setId from FOLDERS
			inner join PRMSETS
			on PRMSETS.folderId = FOLDERS.folderId and FOLDERS.path = "{0}"
			where FOLDERS.path || PRMSETS.title = "{0}" || "{1}" and  PRMSETS.cmdId = {2}

	    
		"""
		cmd = cmd.format(path.strip(),title.strip(), self._cmdId) 

		
		self.exec_query(cmd)
		setId = self._cursor.fetchone()
		setId = setId["setId"] if setId else None
		return setId

	def getSetVersion(self, setId):
		cmd = "select version from PRMSETS where setId = %d and cmdId = %d" % (setId, self._cmdId)
		self.exec_query(cmd)
		data = self._cursor.fetchone()
		version = data["version"] if data else None
		return  version		
	
	def CopySettings(self, SourceSetId,  Title = "",Notes="", Folder="Общие"):
		""" 
			Копируем настройки из одного набора в другой, если набор не существует, то создаём новый (если источник SourceSetId == 0)
		"""


		if self.isSetExistsByFullPath (Folder, Title):
			raise Exception("Набор настроек с таким именем уже существует.")


		if self.isSetExistsById(SourceSetId) == None:
			raise Exception ("Указан несуществующий набор.")

		_UPDATE_CURRENT = 1
		_INSERT_NEW = 2

		_mode = None
		if SourceSetId == 0:
			DesinationSetId = self.getNextSetId() 
			_mode = _INSERT_NEW
			
		elif SourceSetId > 0:
			DesinationSetId = 0
			_mode = _UPDATE_CURRENT
		else: 
			raise Exception ("Непредусмотренные аргументы.")


		version = self.getSetVersion(SourceSetId)


		if _mode == _INSERT_NEW:

			folderId = self.getFolderId(Folder)
			if(not folderId):
				self.createFolder(Folder)
				folderId = self.getFolderId(Folder)

			cmd1 = """
			insert into PRMSETS (cmdId, setId, title, timeStamp, szCreateAuthor, lCreateDateTime, szGeneralNotes, GUID, folderId, version)
				values( {0}, {1}, "{2}", {3}, "{4}", {5}, "{6}", "{7}", {8}, {9} ) 

			"""

			currentUser = get_current_user()
			currentTime = int(get_current_time().timestamp())
			cmd1 = cmd1.format(self._cmdId, DesinationSetId, Title, currentTime, currentUser, currentTime, Notes, uuid.uuid4(), folderId, version)

			cmd2 = """
			delete from  FLDVALUE 
			where	cmdId = {0} and setId = {1}
			"""
			cmd2 = cmd2.format(self._cmdId, DesinationSetId)


			cmd3 = """
			insert into FLDVALUE 
				select {0}, {2}, prmName, prmValue from FLDVALUE where cmdId == {0} and setId == {1} 
			"""
			cmd3 = cmd3.format(self._cmdId, SourceSetId, DesinationSetId)



		elif _mode == _UPDATE_CURRENT:

			cmd1 = """
			update PRMSETS set
				title = '_@current-' || {5},
				szEditAuthor = "{3}",
				lEditDateTime = {4}	,
				version = {6}
			where	cmdId = {0} and setId = {1}
			"""

			currentUser = get_current_user()
			currentTime = int(get_current_time().timestamp())
			cmd1 = cmd1.format(self._cmdId, DesinationSetId, Title, currentUser, currentTime, SourceSetId, version)

			cmd2 = """
			delete from  FLDVALUE 
			where	cmdId = {0} and setId = {1}
			"""
			cmd2 = cmd2.format(self._cmdId, DesinationSetId)


			cmd3 = """
			insert into FLDVALUE 
				select {0}, {2}, prmName, prmValue from FLDVALUE where cmdId == {0} and setId == {1} 
			"""
			cmd3 = cmd3.format(self._cmdId, SourceSetId, DesinationSetId)


		try:
			self._cursor.execute(cmd1)
			self._cursor.execute(cmd2)
			self._cursor.execute(cmd3)
			self._cursor.connection.commit()
		except Exception as e:
			self.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))

	def DeleteStoredSettings (self, SetId):

		if SetId == 0 or SetId == None:
			raise Exception ("Недопустимый аргумент.")
		if not self.isSetExistsById(SetId): 
			raise Exception ("Набора с данным Id не существует.")

		cmd1 = """
		delete from  PRMSETS 
		where	cmdId = {0} and setId = {1}
		"""
		cmd1 = cmd1.format(self._cmdId, SetId)


		cmd2 = """
		delete from  FLDVALUE 
		where	cmdId = {0} and setId = {1}
		"""
		cmd2 = cmd2.format(self._cmdId, SetId)

		try:
			self._cursor.execute(cmd1)
			self._cursor.execute(cmd2)
			self._cursor.connection.commit()
		except Exception as e:
			self.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))

class SettingsDb:
	valid_schema = []
	valid_schema.append("CREATE TABLE FLDVALUE (cmdId INTEGER,setId INTEGER,prmName TEXT,prmValue OBJECT,PRIMARY KEY (cmdId,setId,prmName))")
	valid_schema.append("CREATE TABLE Folders (cmdId INTEGER,folderId INTEGER,parentId INTEGER,name TEXT,path TEXT,expanded BOOLEAN,sortOrder INTEGER,PRIMARY KEY (cmdId,folderId),UNIQUE (cmdId,path),UNIQUE (cmdId,parentId,name))")
	valid_schema.append("CREATE TABLE PRMSETS (cmdId INTEGER,setId INTEGER,title TEXT,timeStamp INTEGER,lCreateDateTime INTEGER,lEditDateTime INTEGER,szCreateAuthor TEXT,szEditAuthor TEXT,szGeneralNotes TEXT,GUID TEXT(16),locked BOOLEAN,folderId INTEGER DEFAULT 0,lockedDateTime INTEGER,lockedAuthor TEXT,lockedReason TEXT,version TEXT,PRIMARY KEY (cmdId,setId),UNIQUE (cmdId,folderId,title),UNIQUE (GUID))")
	valid_schema.append("CREATE TABLE VersionUpgrade (Version TEXT,Date TEXT,Comment TEXT)")
	


	def __init__(self):
		self._cursor = None
		self._dbpath = None

	def connect(self, dbpath):
		if not os.path.exists(dbpath):
			raise FileNotFoundError("Файл '%s' не существует" % dbpath)
		self._dbpath = dbpath
		cnxn = sqlite3.connect(dbpath)
		# validating schema
		schema = [row[0].replace("\t", "").replace("\n", "").replace("\"", "").replace("`", " ").replace("( ", "(").replace(" )", ")").replace(", ", ",").upper() for row in cnxn.execute("select sql from sqlite_master where type = 'table'")]

		#if not all(x.upper() == y.upper() for x, y in zip(schema, self.valid_schema)):
		#	cnxn.close()
		#	raise Exception("Неверная схема базы данных настроек")

		cnxn.row_factory = sqlite3.Row
		self._cursor = cnxn.cursor()
	def close(self):
		self._cursor.connection.close()

	@staticmethod
	def create(dbpath):
		if os.path.exists(dbpath):
			raise Exception("Файл '%s' уже существует")
		srcpath = os.path.join(os.getcwd(), "files", "Settings.nmdb")
		if not os.path.exists(srcpath):
			raise Exception("Файл шаблона настроек не найден")
			
		try:					shutil.copy(srcpath, dbpath)
		except Exception as e:	raise Exception("Невозможно создать файл настроек. %s" % str(e))
		#cnxn = sqlite3.connect(dbpath)
		## field values
		#for cmd in SettingsDb.valid_schema:
		#	cnxn.execute(cmd)
		#cnxn.commit()


class SettingConverter:
	def __init__(self):
		pass

	@staticmethod
	def check_update():
		message = None
		formset = FormSet(ID_SETTINGS)
		version = None
		cmd = "select version from PRMSETS where cmdId = %d and setId = %d" % (1, 0)
		try:
			formset.exec_query(cmd)
			data = formset._cursor.fetchall()
			if not data:
				formset.close()
				return
			version = data[0][0] if data[0][0] else None
		except:
			pass

		converter = None
		cnt_update = 0
		while nm_settings_version != version and cnt_update < 6:
		#while cnt_update < 1:


			if version is None:
				converter = "converters.converter_1_1"
			if version == '1.1':
				converter = "converters.converter_1_3"

			if converter is None:
				raise Exception("Не обнаружен конвертер настроек из версии '{0}'  в версию '{1}'".format(version, nm_settings_version))
 
			SettingConverter.update(converter, nm_settings_version, version)

			formset = FormSet(ID_SETTINGS)
			cmd = "select version from PRMSETS where cmdId = %d and setId = %d" % (1, 0)
			
			formset.exec_query(cmd)
			data = formset._cursor.fetchall()
			version = data[0][0] if data[0][0] else None
			cnt_update = cnt_update + 1
		
		formset.close()
				
		if cnt_update > 0 and cnt_update < 5:
			message = 'Настройки были обновлены.'
			logger.info (message) 
			print(message)


		if cnt_update > 5:
			message = 'Не удалось обновить текущие настройки. Попробуйте восстановить сохраненные настройки или отредактировать текущие вручную.'
			logger.info (message) 
			print (message)

		return message

	def update(converter, nm_settings_version, version):
		try:
			import importlib
			converter_module = importlib.import_module(converter) 
			method_to_start_converter = getattr(converter_module, 'start')

			splash_pix = QPixmap(os.path.join(os.getcwd(), "gui\\img\\splash_sets.png"))
			splash = MySplashScreen(splash_pix,  Qt.WindowStaysOnTopHint)
			splash.setMask(splash_pix.mask())
			splash.show()
				
			method_to_start_converter(splash)

			fakeWidget = QWidget()
			splash.finish(fakeWidget)
		except Exception as e:
			raise Exception("""В настоящее время используются настройки версии:{0} Требуются настройки версии:{1}.
			Не удалось загрузить ковертер {2}. {3}""".format(version, nm_settings_version, converter, str(e) ))



class MySplashScreen (QSplashScreen):
	def __init__(self, *args, **kw):
		super(MySplashScreen, self).__init__(*args, **kw)

	def showMessage(self, str, alignment = Qt.AlignLeft, color = Qt.white):

		super(MySplashScreen, self).showMessage(str, alignment, color)
		QApplication.processEvents()


							

				




# if os.path.exists(SETTINGS_PATH):
# 	os.remove(SETTINGS_PATH)
# SettingsDb.create(SETTINGS_PATH)

# fs = FormSet(ID_SETTINGS)
# fs.open(2)
# print (fs.values())
# fs.setValue("NETPATH", "D:\\")
# fs.setValue("DISPGEO", 1)
# fs.setValue("DISPSVY", 1)
# fs.setValue("DISPDSN", [12,3,4])

# fs.save("FOLDER2|FOLDER1-2|HOOK2", 2)
# fs.save("FOLDER2|FOLDER1-2|FOLDER1-2-3|HOOK3", 3)
# fs.save("FOLDER2|FOLDER1-2|FOLDER1-2-3|HOOK4")
# fs.save("FOLDER2|FOLDER1-2|FOLDER1-2-3|HOOK5")
# fs.save("FOLDER2|FOLDER1-2|FOLDER1-2-3|HOOK6")
# fs.save("HOOK7")
# fs.deleteFolderTree("FOLDER2")

# fs.close()


