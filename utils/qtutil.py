from PyQt4 import QtCore, QtGui

def toScreenCenter(wgt, mode = "minimum"):
	if mode == "minimum":	size = wgt.minimumSize()
	else:					mode = wgt.sizeHint()

	wroot, hroot = size.width(), size.height()

	screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
	resolution = QtGui.QApplication.desktop().screenGeometry(screen)

	wscreen, hscreen = resolution.width(), resolution.height()
	wgt.setGeometry(QtCore.QRect((wscreen - wroot) // 2, (hscreen - hroot) // 2, size.width(), size.height()))

def getWidgetValue(wgt):
	wt = type(wgt)
	if hasattr(wgt, "getValue"):
		return wgt.getValue()
	else:
		if wt == QtGui.QLineEdit or QtGui.QLineEdit in wt.__bases__:			return wgt.text()
		if wt == QtGui.QComboBox:												return wgt.currentIndex()
		if wt == QtGui.QCheckBox:												return 1 if wgt.isChecked() else 0
		if wt == QtGui.QRadioButton:											return 1 if wgt.isChecked() else 0
		if wt == QtGui.QDateEdit:												return wgt.text()
	return None

def getDefaultWidgetValue(wgt):
	wt = type(wgt)
	if wt == QtGui.QLineEdit or QtGui.QLineEdit in wt.__bases__:			return ""
	if wt == QtGui.QComboBox:												return 0
	if wt == QtGui.QCheckBox:												return 0
	if wt == QtGui.QRadioButton:											return 0

def setWidgetValue(wgt, val):
	wt = type(wgt)
	if hasattr(wgt, "setValue"):
		wgt.setValue(val)
	else:
		if wt == QtGui.QLineEdit or QtGui.QLineEdit in wt.__bases__:			wgt.setText(str(val))
		if wt == QtGui.QComboBox:												wgt.setCurrentIndex(int(val)) if type(val) == str else wgt.setCurrentIndex(val)
		if wt == QtGui.QCheckBox:												wgt.setChecked(True) if val else wgt.setChecked(False)
		if wt == QtGui.QRadioButton:											wgt.setChecked(True) if val else wgt.setChecked(False)

def setDefaultWidgetValue(wgt):
	val = getDefaultWidgetValue(wgt)
	setWidgetValue(wgt, val)

def iterWidgetChildren(parent, with_names = True):
	children = list(filter(lambda w: type(w) != QtCore.QObject, parent.children()))
	if not children:	return children

	ret = []
	for child in children:
		ret.append(child)
		ret += iterWidgetChildren(child)
	return list(filter(lambda w: hasattr(w, "accessibleName") and w.accessibleName() != "", ret)) if with_names else ret

def iterWidgetParents(wgt):
	ret = []
	parent = wgt.parent()
	if parent is not None:
		ret.append(parent)
		ret += iterWidgetParents(parent)
	return ret

def getIdByName(parent, name):
	for i, w in enumerate(iterWidgetChildren(parent), start = 1):
		if hasattr(w, "accessibleName") and w.accessibleName() == name:
			return i
	return None

def getWidgetByName(parent, name):
	for i, w in enumerate(iterWidgetChildren(parent), start = 1):
		if hasattr(w, "accessibleName") and w.accessibleName() == name:
			return i
	return None

def getWidgetById(parent, seqId):
	children = iterWidgetChildren(parent)
	return children[seqId-1] if seqId-1 in range(len(children)) else None

def getIdByWidget(parent, wgt):
	children = iterWidgetChildren(parent)
	return children.index(wgt) if wgt in children else None