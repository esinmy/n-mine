import os, pip
os.chdir(r"D:\Workspace\Micromine\Projects\Норильский Никель\N-Mine")

# pip.main(["uninstall", "D:\\NN\\N-Mine\\libs\\pyqt\\PyQt4-4.11.4-cp35-none-win_amd64.whl", "-y"])

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as tkmsgbox

try:
	from PyQt4 import QtGui, QtCore
except Exception as e:
	from gui.tk.libinstaller import LibInstaller
	# запуск установщика
	root = tk.Tk()
	try:	
		app = LibInstaller(root)
		root.mainloop()
	except Exception as e:
		tkmsgbox.showerror("Ошибка", str(e), parent = root)
		root.destroy()
	os.sys.exit()

from documentation.assistant import HelpAssistant

from utils.qtutil import toScreenCenter
from utils.osutil import  clear_temporary_folder
from utils.fsettings import FormSet	, SettingConverter
from utils.constants import *
from utils.dbconnection import generate_connection_string, DbConnection

import gui.qt.dialogs.messagebox as qtmsgbox

from gui.qt.Favorites import FavoritesManager
from gui.qt.SettingsWindow import SettingsWindow
from gui.qt.SettingsManager import SettingsManager
from gui.qt.MainWindow import MainWidget

from gui.qt.modes.AutoVizexPlot import AutoVizexPlot
from gui.qt.modes.DisplayData import DisplayData
from gui.qt.modes.WireframeAttributeValidation import GeoWireframeAttributeValidation, SvyWireframeAttributeValidation, DsnWireframeAttributeValidation
from gui.qt.modes.GetMiddleLine import GetMiddleLine

from gui.qt.modes.geo.GeoMergeBm import GeoMergeBm
from gui.qt.modes.geo.GeoTransferBm import GeoTransferBm
from gui.qt.modes.geo.GeoBackupBm import GeoBackupBm
from gui.qt.modes.geo.GeoUpdateBm import GeoUpdateBm

from gui.qt.modes.svy.SvyBackupWf import SvyBackupWf
from gui.qt.modes.svy.SvyRefreshBm import SvyRefreshBm
from gui.qt.modes.svy.SvyReportBm import SvyReportBm, CustomReportBm
from gui.qt.modes.svy.SvyUpdateBm import SvyUpdateBm
from gui.qt.modes.svy.SvyUpdateGraphic import SvyUpdateGraphic
from gui.qt.modes.svy.SvyCalcStock import SvyCalcStock
from gui.qt.modes.svy.SvyRestoreGeology import SvyRestoreGeology

from gui.qt.modes.dsn.DsnReportBm import DsnReportBm

from gui.qt.commands.geo.GeoAddElement import GeoAddElement
from gui.qt.commands.geo.GeoCreateUnit import GeoCreateUnit
from gui.qt.commands.geo.GeoDefineModel import GeoDefineModel
from gui.qt.commands.geo.GeoUpdateDrillholesDb import GeoUpdateDrillholesDb
from gui.qt.commands.geo.GeoUpdateTrenchesDb import GeoUpdateTrenchesDb
from gui.qt.commands.geo.GeoAddSurveyToDb import GeoAddSurveyToDb
																		 
from gui.qt.commands.svy.SvyAddElement import SvyAddElement
from gui.qt.commands.svy.SvyCreateUnit import SvyCreateUnit
from gui.qt.commands.svy.SvyCreateSubUnit import SvyCreateSubUnit
from gui.qt.commands.svy.SvyDefineModel import SvyDefineModel
from gui.qt.commands.svy.SvyCreatePoint import SvyCreatePoint, SvyEditPoint, SvyRemovePoint
from gui.qt.commands.svy.SvyRegister import SvyBrowseRegister

from gui.qt.commands.svy.SvyDirections import SvyAddDirection, SvyEditDirection

from gui.qt.commands.dsn.DsnAddElement import DsnAddElement
from gui.qt.commands.dsn.DsnAutoCreateSolid import DsnAutoCreateSolid
from gui.qt.commands.dsn.DsnCreateUnit import DsnCreateUnit
from gui.qt.commands.dsn.DsnDefineModelVer1 import DsnDefineModelVer1
from gui.qt.commands.dsn.DsnDefineModelVer2 import DsnDefineModelVer2

from gui.qt.commands.LoadSvyPoints import LoadSvyPoints
from gui.qt.commands.LoadDrillholes import LoadDrillholes
from gui.qt.commands.LoadDetailedTrenches import LoadDetailedTrenches

from gui.qt.commands.ImportLeicaGsiDialog import ImportLeicaGsiDialog
from gui.qt.dialogs.RequestPassword  import  RequestPassword
from gui.qt.commands.ExportStringToSim import ExportStringToSim

from services.utils.api_manager import  ApiManager
from services.api.interfaces.IApi import IApi
from services.utils.flask_server import FlaskServer
from flask import copy_current_request_context
import requests
import ssl
try:
	import ctypes
	ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID('N-Mine')
except:
	pass

from utils.logger import  NLogger , register_logging

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class N_MINE(QtGui.QMainWindow):
	def __init__(self, *args, **kw):
		super(N_MINE, self).__init__(*args, **kw)
		self.setWindowTitle("N-Mine")

		self._createVariables()
		self._createWidgets()

		timer = QtCore.QTimer()
		timer.singleShot(1, self.setParams)

		self._createBindings()
		self._gridWidgets()


		self.thread = None
		self.startThread()


	def _createMenu(self):
		self.menuBar = self.menuBar()

		self.menuFile = self.menuBar.addMenu("&Файл")
		self.menuFile.addAction("Выход", self.closeApplication)
		self.menuSettings = self.menuBar.addMenu("&Настройки")
		self.menuSettings.addAction("Избранное", self.manageFavorite)
		self.menuSettings.addAction("Менеджер настроек", self.openSettingsManager)
		self.menuUtilities = self.menuBar.addMenu("&Утилиты")
		self.menuUtilities.addAction("Сохранить каркасы в набор") #self.mainWidget.selectionExplorer.saveWireframesToSet)
		self.menuUtilities.addAction("Импорт GSI файла", self.importLeicaGsi)
		self.menuUtilities.addAction("Экспорт стрингов в СИМ", self.export_string_to_sim)
		self.menuHelp = self.menuBar.addMenu("Справка")
		helpAction = self.menuHelp.addAction("Справка")
		helpAction.setShortcut(QtGui.QKeySequence.HelpContents)
		helpAction.triggered.connect(self.showHelp)
	def _createVariables(self):
		# подключения к базам данных геологии и маркшейдерии
		self._srvcnxn = None
		#
		self._settings = None
		self.__helpAssistant = HelpAssistant(self)
		self._currentMode = MODE_DISPLAY_DATA
		self._current_process = None

		self.readSettings()

		self._mainDir = {}
		self._ignoreFolders = {}
		self.isExitCorrect = False


	def _createWidgets(self):
		self.mainWidget = MainWidget(self)

		self._createToolBars()
		self._createMenu()
	def _createBindings(self):
		self.mainWidget.pushButtonCancel.clicked.connect(self.closeApplication)
		self.mainWidget.pushButtonOk.clicked.connect(self.run)

	def _gridWidgets(self):
		self.setCentralWidget(self.mainWidget)
	
	def _createModeToolBar(self):
		self.toolBarMode = self.addToolBar("Режимы")
		self.modeGroupButtons = QtGui.QButtonGroup(self)

		self.toolButtonDisplayData = QtGui.QToolButton(self)
		self.toolButtonDisplayData.clicked.connect(lambda: self.modeClicked(MODE_DISPLAY_DATA))
		self.toolButtonGeoValidate = QtGui.QToolButton(self)
		self.toolButtonGeoValidate.clicked.connect(lambda: self.modeClicked(MODE_GEO_WF_VALIDATION))
		self.toolButtonSvyValidate = QtGui.QToolButton(self)
		self.toolButtonSvyValidate.clicked.connect(lambda: self.modeClicked(MODE_SVY_WF_VALIDATION))
		self.toolButtonDsnValidate = QtGui.QToolButton(self)
		self.toolButtonDsnValidate.clicked.connect(lambda: self.modeClicked(MODE_DSN_WF_VALIDATION))
		self.toolButtonGeoUpdateBm = QtGui.QToolButton(self)
		self.toolButtonGeoUpdateBm.clicked.connect(lambda: self.modeClicked(MODE_GEO_UPDATE_BM))
		self.toolButtonSvyUpdateBm = QtGui.QToolButton(self)
		self.toolButtonSvyUpdateBm.clicked.connect(lambda: self.modeClicked(MODE_SVY_UPDATE_BM))
		self.toolButtonSvyRefreshBm = QtGui.QToolButton(self)
		self.toolButtonSvyRefreshBm.clicked.connect(lambda: self.modeClicked(MODE_SVY_REFRESH_BM))
		self.toolButtonSvyRestoreGeology = QtGui.QToolButton(self)
		self.toolButtonSvyRestoreGeology.clicked.connect(lambda: self.modeClicked(MODE_SVY_RESTORE_GEOLOGY))
		self.toolButtonSvyReportBm = QtGui.QToolButton(self)
		self.toolButtonSvyReportBm.clicked.connect(lambda: self.modeClicked(MODE_SVY_REPORT_BM))
		self.toolButtonDsnReportBm = QtGui.QToolButton(self)
		self.toolButtonDsnReportBm.clicked.connect(lambda: self.modeClicked(MODE_DSN_REPORT_BM))
		self.toolButtonCustomReportBm = QtGui.QToolButton(self)
		self.toolButtonCustomReportBm.clicked.connect(lambda: self.modeClicked(MODE_CUSTOM_REPORT_BM))
		self.toolButtonGeoMergeBm = QtGui.QToolButton(self)
		self.toolButtonGeoMergeBm.clicked.connect(lambda: self.modeClicked(MODE_GEO_MERGE_BM))

		self.toolButtonTransferBm = QtGui.QToolButton(self)
		self.toolButtonTransferBm.setPopupMode(self.toolButtonTransferBm.InstantPopup)
		self.toolButtonTransferBmUnitMenu = QtGui.QMenu()
		self.toolButtonTransferBmUnitMenu.addAction("&Перевод блочных моделей",
		                                            lambda: self.modeClicked(MODE_GEO_TRANSFER_BM))
		self.toolButtonTransferBmUnitMenu.addAction("&Фиксация блочных моделей",
		                                            lambda: self.modeClicked(MODE_GEO_BACKUP_BM))
		self.toolButtonTransferBmUnitMenu.addAction("&Фиксация маркшейдерских каркасов",
		                                            lambda: self.modeClicked(MODE_SVY_BACKUP_WF))
		self.toolButtonTransferBm.setMenu(self.toolButtonTransferBmUnitMenu)

		self.toolButtonSvyUpdateGraphic = QtGui.QToolButton(self)
		self.toolButtonSvyUpdateGraphic.clicked.connect(lambda: self.modeClicked(MODE_SVY_UPDATE_GRAPHIC))
		self.toolButtonStock = QtGui.QToolButton(self)
		self.toolButtonStock.clicked.connect(lambda: self.modeClicked(MODE_SVY_STOCK_CALCPLOT))
		self.toolButtonPlot = QtGui.QToolButton(self)
		self.toolButtonPlot.clicked.connect(lambda: self.modeClicked(MODE_AUTO_VIZEX_PLOT))
		self.toolButtonMiddleLine = QtGui.QToolButton(self)
		self.toolButtonMiddleLine.clicked.connect(lambda: self.modeClicked(MODE_GET_MIDDLE_LINE))

		buttons = (self.toolButtonDisplayData, self.toolButtonGeoValidate, self.toolButtonSvyValidate, self.toolButtonDsnValidate, self.toolButtonGeoUpdateBm,
					self.toolButtonSvyUpdateBm, self.toolButtonSvyRefreshBm, self.toolButtonSvyRestoreGeology, self.toolButtonSvyReportBm, self.toolButtonDsnReportBm,
					self.toolButtonCustomReportBm, self.toolButtonGeoMergeBm, self.toolButtonTransferBm, self.toolButtonSvyUpdateGraphic, self.toolButtonStock,
				   self.toolButtonPlot, self.toolButtonMiddleLine)

		root = "gui/img/modes"
		icons = (
					"displayData", 
					"geoValidateWireframes", "svyValidateWireframes", "dsnValidateWireframes", 
					"geoUpdateBm", "svyUpdateBm", "svyRefreshBm", "svyRestoreGeology",
					"bmReportFact", "bmReportProject", "bmCustomReport", 
					"geoMergeBm", "transferModel", 
					"svyUpdateGraphics", "stock", "autoVizexPlot", "middle_line"
				)

		icons = tuple(map(lambda x: "/".join([root, "%s.png" % x]), icons))

		hints = (
					"Загрузить данные", 
					"Проверка геологических каркасов", "Проверка маркшейдерских каркасов", "Проверка проектных каркасов", 
					"Создать/Обновить блочную модель", "Обновить блочную модель", "Пополнить блочную модель", "Восстановить геологию",
					"Отчет по блочной модели", "Отчет по блочной модели", "Отчет по каркасной модели", 
					"Объединить блочные модели", "Управление моделями",
					"Пополнение маркшейдерской графики", "Рассчитать склад", "Печать", "Получение осевой линии"
				)

		self._buildToolBar(self.toolBarMode, self.modeGroupButtons, buttons, icons, hints, checkable = True)

		self.toolButtonDisplayData.click()
	def _createCommandToolBar(self):
		self.toolBarCommand = self.addToolBar("Команды")
		self.commandGroupButtons = QtGui.QButtonGroup(self)

		self.toolButtonGeoCreateUnit = QtGui.QToolButton(self)
		self.toolButtonGeoCreateUnit.clicked.connect(lambda: self.commandClicked(COMMAND_GEO_CREATE_UNIT))

		self.toolButtonSvyCreateUnit = QtGui.QToolButton(self)
		self.toolButtonSvyCreateUnit.setPopupMode(self.toolButtonSvyCreateUnit.InstantPopup)
		self.svyUnitMenu = QtGui.QMenu()
		self.svyUnitMenu.addAction("&Создание маркшейдерского юнита", lambda: self.commandClicked(COMMAND_SVY_CREATE_UNIT))
		self.svyUnitMenu.addAction("&Создание маркшейдерского подюнита", lambda: self.commandClicked(COMMAND_SVY_CREATE_SUBUNIT))
		self.toolButtonSvyCreateUnit.setMenu(self.svyUnitMenu)
		#self.toolButtonSvyCreateUnit = QtGui.QToolButton(self)
		#self.toolButtonSvyCreateUnit.clicked.connect(lambda: self.commandClicked(COMMAND_SVY_CREATE_UNIT))
		#self.toolButtonSvyCreateSubUnit = QtGui.QToolButton(self)
		#self.toolButtonSvyCreateSubUnit.clicked.connect(lambda: self.commandClicked(COMMAND_SVY_CREATE_SUBUNIT))
		self.toolButtonDsnCreateUnit = QtGui.QToolButton(self)
		self.toolButtonDsnCreateUnit.clicked.connect(lambda: self.commandClicked(COMMAND_DSN_CREATE_UNIT))
		self.toolButtonGeoDefineModel = QtGui.QToolButton(self)
		self.toolButtonGeoDefineModel.clicked.connect(lambda: self.commandClicked(COMMAND_GEO_DEFINE_MODEL))
		self.toolButtonSvyDefineModel = QtGui.QToolButton(self)
		self.toolButtonSvyDefineModel.clicked.connect(lambda: self.commandClicked(COMMAND_SVY_DEFINE_MODEL))
		self.toolButtonDsnDefineModel = QtGui.QToolButton(self)
		self.toolButtonDsnDefineModel.clicked.connect(lambda: self.commandClicked(COMMAND_DSN_DEFINE_MODEL))
		self.toolButtonAddGeoElement = QtGui.QToolButton(self)
		self.toolButtonAddGeoElement.clicked.connect(lambda: self.commandClicked(COMMAND_GEO_ADD_ELEMENT))
		self.toolButtonAddSvyElement = QtGui.QToolButton(self)
		self.toolButtonAddSvyElement.clicked.connect(lambda: self.commandClicked(COMMAND_SVY_ADD_ELEMENT))
		self.toolButtonAddDsnElement = QtGui.QToolButton(self)
		self.toolButtonAddDsnElement.clicked.connect(lambda: self.commandClicked(COMMAND_DSN_ADD_ELEMENT))
		self.toolButtonMagicMine = QtGui.QToolButton(self)
		self.toolButtonMagicMine.clicked.connect(lambda: self.commandClicked(COMMAND_DSN_AUTO_CREATE_SOLID))
		
		self.toolButtonDisplayDrillholes = QtGui.QToolButton(self)
		self.toolButtonDisplayDrillholes.setPopupMode(self.toolButtonDisplayDrillholes.InstantPopup)
		self.drillholesMenu = QtGui.QMenu()
		self.drillholesMenu.addAction("&Загрузка скважин", lambda: self.commandClicked(COMMAND_LOAD_DRILLHOLES))
		self.drillholesMenu.addAction("&Обновить базу данных скважин", lambda: self.commandClicked(COMMAND_UPDATE_DHDB))
		self.toolButtonDisplayDrillholes.setMenu(self.drillholesMenu)

		self.toolButtonDisplayTrenches = QtGui.QToolButton(self)
		self.toolButtonDisplayTrenches.setPopupMode(self.toolButtonDisplayTrenches.InstantPopup)
		self.trenchesMenu = QtGui.QMenu()
		self.trenchesMenu.addAction("&Загрузка борозд", lambda: self.commandClicked(COMMAND_LOAD_TRENCHES))
		self.trenchesMenu.addAction("&Обновить базу данных борозд", lambda: self.commandClicked(COMMAND_UPDATE_TRDB))
		self.trenchesMenu.addAction("&Добавить траекторию в БД", lambda: self.commandClicked(COMMAND_SURVEY_TO_DB_TRDB))
		self.toolButtonDisplayTrenches.setMenu(self.trenchesMenu)

		self.toolButtonSvyPoints = QtGui.QToolButton(self)
		self.toolButtonSvyPoints.setPopupMode(self.toolButtonSvyPoints.InstantPopup)
		self.svyPointsMenu = QtGui.QMenu()
		self.svyPointsMenu.addAction("&Загрузка точек", lambda: self.commandClicked(COMMAND_LOAD_SVY_POINTS))
		self.svyPointManageMenu = self.svyPointsMenu.addMenu("Управление точками")
		self.svyPointManageMenu.addAction("&Создать", lambda: self.commandClicked(COMMAND_SVY_CREATE_POINT))
		self.svyPointManageMenu.addAction("&Редактировать", lambda: self.commandClicked(COMMAND_SVY_EDIT_POINT))
		self.svyPointManageMenu.addAction("&Удалить", lambda: self.commandClicked(COMMAND_SVY_REMOVE_POINT))
		self.svyPointManageMenu.addSeparator()
		self.svyPointManageMenu.addAction("&Журналы", lambda: self.commandClicked(COMMAND_SVY_BROWSE_REGISTER))
		self.svyDirectionManageMenu = self.svyPointsMenu.addMenu("Расчет направлений")
		self.svyDirectionManageMenu.addAction("&Рассчитать направление", lambda: self.commandClicked(COMMAND_SVY_ADD_DIRECTION))
		self.svyDirectionManageMenu.addAction("&Редактировать направление", lambda: self.commandClicked(COMMAND_SVY_EDIT_DIRECTION))
		# self.svyDirectionManageMenu.addAction("&Удалить направление")
		# self.svyLookupMenu = self.svyPointsMenu.addMenu("Справочники")
		# self.svyLookupMenu.addAction("&Редактировать")
		self.toolButtonSvyPoints.setMenu(self.svyPointsMenu)

		buttons = (self.toolButtonGeoCreateUnit, self.toolButtonSvyCreateUnit, self.toolButtonDsnCreateUnit,
				 	self.toolButtonGeoDefineModel, self.toolButtonSvyDefineModel, self.toolButtonDsnDefineModel,
				 	self.toolButtonAddGeoElement, self.toolButtonAddSvyElement, self.toolButtonAddDsnElement,
					self.toolButtonMagicMine, self.toolButtonDisplayDrillholes, self.toolButtonDisplayTrenches,
					self.toolButtonSvyPoints)

		root = "gui/img/commands"
		icons = ("createUnit", "createUnit", "createUnit", "geoDefineModel", "svyDefineModel", "dsnDefineModel", "geoAddElement",
				"svyAddElement", "dsnAddElement", "magicMine", "displayDrillholes", "displayTrenches", "displaySvyPoints")
		icons = tuple(map(lambda x: "/".join([root, "%s.png" % x]), icons))

		hints = ("Создать геологический юнит", "Создать маркшейдерский юнит", "Создать проектный юнит",
				"Определить модель", "Создать файл выработки", "Создать проектную выработку",
				"Добавить элемент в модель", "Добавить элемент", "Добавить элемент", "Автоматическое построение выработки", "Работа с данными бурения",	"Работа с бороздовым опробованием",
				"Работа с маркшейдерскими точками")

		self._buildToolBar(self.toolBarCommand, self.commandGroupButtons, buttons, icons, hints, checkable = False)

	def _createToolBars(self):
		self._createModeToolBar()
		self._createCommandToolBar()
	def _buildToolBar(self, bar, group, buttons, icons, hints = [], checkable = False):
		for but, ico, hint in zip(buttons, icons, hints):
			but.setCheckable(checkable)
			but.setIcon(QtGui.QIcon(ico))
			but.setIconSize(QtCore.QSize(30, 30))
			but.setMinimumSize(QtCore.QSize(40, 40))
			but.setMaximumSize(QtCore.QSize(40, 40))
			but.setToolTip(hint)
			group.addButton(but)
			bar.addWidget(but)

	def closeEvent(self, event):
		if self.current_process:
			if qtmsgbox.show_question(self, "Внимание", "Прервать процесс?"):
				self.close_connections()
				event.accept()
			else:
				self.current_process = None
				event.ignore()
		else:
			self.close_connections()
			event.accept()

		if event.isAccepted():
			modifiers = QtGui.QApplication.queryKeyboardModifiers()
			if modifiers == (QtCore.Qt.ControlModifier | QtCore.Qt.AltModifier):
				return
			clear_temporary_folder()

		if self.__helpAssistant.isVisible():
			self.__helpAssistant.close()

		self.isExitCorrect = True

		logger.close()


	def closeApplication(self):
		self.close()

	def close_connections(self):
		if self.settings:	self.settings.close()
		if self.srvcnxn:
			self.srvcnxn.close()
			self._srvcnxn = None

	def connect_to_database(self):
		self.mainWidget.progressBar.setRange(0,0)
		self.mainWidget.progressBar.setText("Подключение к базе данных")

		#
		srvname = self.settings.getValue(ID_SRVNAME)
		dbname = "master"
		cnxnstring = generate_connection_string(srvname, dbname)
		try:
			self._srvcnxn = DbConnection(cnxnstring)
			self.srvcnxn.dbsvy = self.settings.getValue(ID_DBSVY)
			self.srvcnxn.dbgeo = self.settings.getValue(ID_DBGEO)
			self.srvcnxn.location = self.settings.getValue(ID_PROJNAME)
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Не удалось подключиться к базе данных. Проверьте права доступа. %s" % str(e))
			self._srvcnxn = None

		self.mainWidget.progressBar.setRange(0,100)
		self.mainWidget.progressBar.setText("")

		return self.srvcnxn

	def manageFavorite(self):
		favoritesWindow = FavoritesManager(self)
		toScreenCenter(favoritesWindow)
		favoritesWindow.exec_()

		modex = self.mainWidget.modelExplorer
		selex = self.mainWidget.selectionExplorer

		if favoritesWindow.needsUpdate:
			selex.clear()
			modex.updateUserModelsList()
			self.refreshUserModels()

	def openSettings(self):
		settingsWindow = SettingsWindow(self)
		toScreenCenter(settingsWindow)
		settingsWindow.exec_()

		if settingsWindow.needsUpdate:
			self.close_connections()
			self.readSettings()

			self.setParams()
			self.refreshStandardModels()
			self.refreshUserModels()

	def openSettingsManager(self):
		settingsManager = SettingsManager(self, self._settings)
		toScreenCenter(settingsManager)
		settingsManager.exec_()

		if settingsManager.needsUpdate:
			self.close_connections()
			self.readSettings()

			self.setParams()
			self.refreshStandardModels()
			self.refreshUserModels()

	def importLeicaGsi(self):
		self.currentMode = COMMAND_IMPORT_GSI
		self.run()

	def export_string_to_sim(self):
		self.currentMode = COMMAND_EXPORT_STRING_TO_SIM
		self.run()

	def setWindowTitle(self, title):
		__settings_mode = ""
		if hasattr(self,"_settings") and self._settings != None:
			__currentSettingsInStore = self._settings.IsCurrentSettingsInStore()
			if __currentSettingsInStore != None:
				__settings_mode  = 	" - Настройки: " +	__currentSettingsInStore
		super(N_MINE, self).setWindowTitle(title + __settings_mode)

	def readSettings(self):
		if self.settings:
			self.settings.close()
		try:					self._settings = FormSet(ID_SETTINGS)
		except Exception as e:	raise Exception("Невозможно считать настройки. %s" % str(e))
		self.settings.open(0)
		#
		if not self.settings.values():
			self.openSettings()

		self.check_authorization()


	def check_authorization(self):
		if self.settings.getValue(ID_USERADMIN) == 1:
			req_pass = RequestPassword(self)
			req_pass.exec_()
			if req_pass.GetPassword() != "фудзияма":
				self.settings.setValue(ID_USERADMIN, 0) 
				self.settings.setValue(ID_USERGEO, 1) 
				

	def clearExplorers(self):
		self.mainWidget.selectionExplorer.clear()
		self.mainWidget.stockExplorer.clear()
		self.mainWidget.sectionPlotExplorer.clear()

	#
	def commandClicked(self, command):
		self.currentMode = command
		self.run()

	def modeClicked(self, mode):
		self.clearExplorers()
		self.activateMode(mode)


	def refreshStandardModel(self, model):
		self.mainWidget.progressBar.setRange(0,0)
		self.mainWidget.progressBar.setText("Построение модели")

		model.update_root()

		self.mainWidget.progressBar.setRange(0, 100)
		self.mainWidget.progressBar.setText("")

	def refreshStandardModels(self):
		for model in self.mainWidget.modelExplorer.standard_models:
			self.refreshStandardModel(model)

	def refreshUserModel(self, model, root):
		model.update_root(root)

	def refreshUserModels(self):
		usrparams = FormSet(ID_FAVORITE)
		usrparams.open(0)
		for model, (tabname, root) in zip(self.mainWidget.modelExplorer.user_models, usrparams.getValue(ID_USRFOLDERS)):
			self.refreshUserModel(model, root)
		usrparams.close()

	def applyModelView(self):
		self.mainWidget.modelExplorer.applyModelView()

	def userRole(self):
		roles = [ID_USERADMIN, ID_USERGEO, ID_USERSVY, ID_USERDSN]
		flags = [self.settings.getValue(role) for role in roles]
		return roles[flags.index(1)]

	def setParams(self):
		if not self.settings.values():
			return
		#
		if self.settings.getValue(ID_USERADMIN) == 1:	self.setWindowTitle("N-Mine - %s" % "Администратор")
		if self.settings.getValue(ID_USERGEO) == 1:		self.setWindowTitle("N-Mine - %s" % ITEM_CATEGORY_GEO_NAME)
		if self.settings.getValue(ID_USERSVY) == 1:		self.setWindowTitle("N-Mine - %s" % ITEM_CATEGORY_SVY_NAME)
		if self.settings.getValue(ID_USERDSN) == 1:		self.setWindowTitle("N-Mine - %s" % ITEM_CATEGORY_DSN_NAME)
		#
		selex = self.mainWidget.selectionExplorer
		modex = self.mainWidget.modelExplorer
		#
		selex.clear()

		# настраиваем тулбары
		actions = self.toolBarMode.actions() + self.toolBarCommand.actions()
		buttons = self.modeGroupButtons.buttons() + self.commandGroupButtons.buttons()

		if self.settings.getValue(ID_USERADMIN) == 1:
			for action in actions:
				action.setVisible(True)
			# выпадающие меню
			for action in self.svyPointsMenu.actions() + self.svyPointManageMenu.actions() + self.drillholesMenu.actions() + self.trenchesMenu.actions() + self.toolButtonTransferBmUnitMenu.actions():
				action.setVisible(True)
		if self.settings.getValue(ID_USERGEO) == 1:
			for button, action in zip(buttons, actions):
				if button in (self.toolButtonGeoCreateUnit, self.toolButtonGeoDefineModel, self.toolButtonAddGeoElement,
							self.toolButtonDisplayDrillholes, self.toolButtonDisplayTrenches, self.toolButtonPlot, self.toolButtonDisplayData,
							self.toolButtonGeoValidate, self.toolButtonGeoUpdateBm, self.toolButtonTransferBm, self.toolButtonSvyReportBm, self.toolButtonGeoMergeBm,
							self.toolButtonSvyPoints, self.toolButtonDsnReportBm, self.toolButtonCustomReportBm, self.toolButtonMiddleLine):
					action.setVisible(True)
				else:
					action.setVisible(False)
			# выпадающие меню

			for i, action in enumerate(self.drillholesMenu.actions()):
				action.setVisible(True)
			for i, action in enumerate(self.trenchesMenu.actions()):
				action.setVisible(True)
			for i, action in enumerate(self.svyPointsMenu.actions()):
				if i > 0:	action.setVisible(False)
				else:		action.setVisible(True)
			for  i, action in enumerate(self.toolButtonTransferBmUnitMenu.actions()):
				if i > 1:   action.setVisible(False)
				else:   action.setVisible(True)
		if self.settings.getValue(ID_USERSVY) == 1:
			for button, action in zip(buttons, actions):
				if button in (self.toolButtonSvyCreateUnit, self.toolButtonSvyDefineModel, self.toolButtonAddSvyElement, self.toolButtonTransferBm,
							self.toolButtonDisplayDrillholes, self.toolButtonDisplayTrenches, self.toolButtonMagicMine, self.toolButtonStock, self.toolButtonPlot, self.toolButtonDisplayData,
							self.toolButtonSvyValidate, self.toolButtonSvyUpdateBm,	self.toolButtonSvyReportBm, self.toolButtonSvyRefreshBm, #self.toolButtonSvyRestoreGeology,
							self.toolButtonSvyPoints, self.toolButtonSvyUpdateGraphic, self.toolButtonDsnReportBm, self.toolButtonCustomReportBm, self.toolButtonMiddleLine):
					action.setVisible(True)
				else:
					action.setVisible(False)
			# выпадающие меню
			#for i, action in enumerate(self.svyUnitMenu.actions()):
			#	action.setVisible(True)
			for i, action in enumerate(self.drillholesMenu.actions()):
				if i > 0:	action.setVisible(False)
				else:		action.setVisible(True)
			for i, action in enumerate(self.trenchesMenu.actions()):
				if i > 0:	action.setVisible(False)
				else:		action.setVisible(True)
			for i, action in enumerate(self.svyPointsMenu.actions()):
				action.setVisible(True)
			for i, action in enumerate(self.svyPointManageMenu.actions()):
				action.setVisible(True)
			for  i, action in enumerate(self.toolButtonTransferBmUnitMenu.actions()):
				if i > 1:   action.setVisible(True)
				else:   action.setVisible(False)
		if self.settings.getValue(ID_USERDSN) == 1:
			for button, action in zip(buttons, actions):
				if button in (self.toolButtonDsnCreateUnit, self.toolButtonDsnDefineModel, self.toolButtonAddDsnElement,
							self.toolButtonDisplayDrillholes, self.toolButtonDisplayTrenches, self.toolButtonMagicMine, self.toolButtonPlot, self.toolButtonDisplayData, self.toolButtonGeoMergeBm,
							self.toolButtonDsnValidate,	self.toolButtonDsnReportBm, self.toolButtonSvyPoints, self.toolButtonCustomReportBm, self.toolButtonMiddleLine):
					action.setVisible(True)
				else:
					action.setVisible(False)
			# выпадающие меню

			for i, action in enumerate(self.drillholesMenu.actions()):
				if i > 0:	action.setVisible(False)
				else:		action.setVisible(True)
			for i, action in enumerate(self.trenchesMenu.actions()):
				if i > 0:	action.setVisible(False)
				else:		action.setVisible(True)
			for i, action in enumerate(self.svyPointsMenu.actions()):
				if i > 0:	action.setVisible(False)
				else:		action.setVisible(True)
		self.toolButtonDisplayData.click()
		#
		modex.updateUserModelsList()
		#
		self.refreshStandardModels()
		self.refreshUserModels()
	
	def showHelp(self):
		if not self.__helpAssistant.isVisible():
			self.__helpAssistant.show()

	@property
	def srvcnxn(self):		return self._srvcnxn
	@property
	def settings(self):		return self._settings
	@property
	def currentMode(self):				return self._currentMode
	@currentMode.setter
	def currentMode(self, mode):		self._currentMode = mode
	@property
	def current_process(self):			return self._current_process
	@current_process.setter
	def current_process(self, proc):	self._current_process = proc

	def activateMode(self, mode):
		modex = self.mainWidget.modelExplorer
		selex = self.mainWidget.selectionExplorer
		stockex = self.mainWidget.stockExplorer
		transex = self.mainWidget.transferExplorer
		bckpex = self.mainWidget.backupExplorer
		secpex = self.mainWidget.sectionPlotExplorer
		selexTree = selex.explorer

		selex.setFilterEnabled(False)

		self.currentMode = mode
		for i in range(modex.explorer.tabBar().count()):
			modex.explorer.setTabEnabled(i, True)
		for but in modex.explorerFilter.filterButtons():
			but.setChecked(True)
			but.setEnabled(True)
		modex.explorer.setCurrentIndex(0)

		# выбор необходимых обозревателей
		if mode in [MODE_DISPLAY_DATA, MODE_SVY_UPDATE_GRAPHIC]:
			self.mainWidget.labelProcessMessage.setText("Выберите объекты для загрузки")
			selex.setVisible(True)

		if mode in (MODE_GEO_WF_VALIDATION, MODE_GEO_UPDATE_BM, MODE_GEO_MERGE_BM, 
					MODE_SVY_WF_VALIDATION, MODE_SVY_UPDATE_BM, MODE_SVY_REFRESH_BM, MODE_SVY_REPORT_BM,
					MODE_CUSTOM_REPORT_BM, 
					MODE_DSN_WF_VALIDATION, MODE_DSN_REPORT_BM, MODE_GET_MIDDLE_LINE):
			selex.setVisible(True)

		if mode == MODE_GEO_TRANSFER_BM:
			self.toolButtonTransferBm.setChecked(True)
			self.mainWidget.labelProcessMessage.setText("Укажите дату для перевода блочных моделей")
			selex.setVisible(False)
			transex.setVisible(True)
		else:
			self.toolButtonTransferBm.setChecked(False)
			transex.setVisible(False)

		if mode == MODE_GEO_BACKUP_BM:
			self.toolButtonTransferBm.setChecked(True)
			self.mainWidget.labelProcessMessage.setText("Укажите, какие блочные модели необходимо зафиксировать")
			selex.setVisible(False)
			bckpex.setVisible(True)
		else:
			self.toolButtonTransferBm.setChecked(False)
			bckpex.setVisible(False)

		if mode == MODE_SVY_BACKUP_WF:
			self.toolButtonTransferBm.setChecked(True)
			self.mainWidget.labelProcessMessage.setText(
				"Укажите, какие маркшейдерские каркасы необходимо зафиксировать")
			selex.setVisible(True)
			for i in range(modex.explorer.tabBar().count()):
				if modex.explorer.widget(i) == modex.svyTreeModel:
					modex.explorer.setTabEnabled(i, True)
					modex.explorer.setCurrentIndex(i)
				else:
					modex.explorer.setTabEnabled(i, False)
			for but in modex.explorerFilter.filterButtons():
				if but == modex.explorerFilter.showTridbButton:
					but.setChecked(True)
					but.setEnabled(True)
				else:
					but.setChecked(False)
					but.setEnabled(False)
		else:
			self.toolButtonTransferBm.setChecked(False)

		if mode == MODE_AUTO_VIZEX_PLOT:
			self.mainWidget.labelProcessMessage.setText("Выберите объекты для загрузки (опционально)")
			selex.setVisible(True)
			secpex.setVisible(True)
		else:
			secpex.setVisible(False)

		#
		if mode in [MODE_SVY_UPDATE_GRAPHIC, MODE_AUTO_VIZEX_PLOT]:		modex.showSectionsTab()
		else:															modex.hideSectionsTab()
		#
		if mode in [MODE_SVY_STOCK_CALCPLOT]:
			self.mainWidget.layoutExplorerParams.setColumnStretch(0,1)
			self.mainWidget.layoutExplorerParams.setColumnStretch(1,1)
			stockex.setVisible(True)
			selex.setVisible(True)
		else:
			self.mainWidget.layoutExplorerParams.setColumnStretch(0,0)
			self.mainWidget.layoutExplorerParams.setColumnStretch(1,1)
			stockex.setVisible(False)

		#
		if mode in [MODE_DISPLAY_DATA, MODE_AUTO_VIZEX_PLOT, MODE_SVY_STOCK_CALCPLOT]:
			for but in modex.explorerFilter.filterButtons():
				but.setChecked(True)
				but.setEnabled(True)
			selex.setFilterEnabled(True)
			
			#
			for i in range(modex.explorer.tabBar().count()):
				modex.explorer.setTabEnabled(i, True)
			for i in range(selex.explorer.columnCount()):
				selexTree.showColumn(i)
			
			#
			if mode == MODE_DISPLAY_DATA:
				selex.displayParams.setVisible(True)
				modex.explorer.setCurrentIndex(0)
			#
			if mode == MODE_AUTO_VIZEX_PLOT:
				selex.displayParams.setVisible(False)
				modex.explorer.setCurrentIndex(3)
			if mode == MODE_SVY_STOCK_CALCPLOT:
				selex.displayParams.setVisible(True)
				modex.explorer.setCurrentIndex(1)
		else:
			for i in range(1, selexTree.columnCount()):
				selexTree.hideColumn(i)
			selex.displayParams.setVisible(False)

		# Хотелка (поиск: Василий Радзион) грузить все по умолчанию. Для этого прячем столбец 'Редактировать'
		selexTree.hideColumn(1)

		#
		if mode in [MODE_GEO_WF_VALIDATION, MODE_SVY_WF_VALIDATION, MODE_DSN_WF_VALIDATION]:
			self.mainWidget.labelProcessMessage.setText("Выберите каркасы для проверки")
			for but in modex.explorerFilter.filterButtons():
				if but == modex.explorerFilter.showTridbButton:
					but.setChecked(True)
					but.setEnabled(True)
				else:
					but.setChecked(False)
					but.setEnabled(False)
			#	
			if mode == MODE_GEO_WF_VALIDATION:
				for i in range(modex.explorer.tabBar().count()):
					tab = modex.explorer.widget(i)
					if tab == modex.geoTreeModel:
						modex.explorer.setTabEnabled(i, True)
						modex.explorer.setCurrentIndex(i)
					else:
						modex.explorer.setTabEnabled(i, False)
			if mode == MODE_SVY_WF_VALIDATION:
				for i in range(modex.explorer.tabBar().count()):
					if modex.explorer.widget(i) == modex.svyTreeModel:
						modex.explorer.setTabEnabled(i, True)
						modex.explorer.setCurrentIndex(i)
					else:
						modex.explorer.setTabEnabled(i, False)
			if mode == MODE_DSN_WF_VALIDATION:
				for i in range(modex.explorer.tabBar().count()):
					if modex.explorer.widget(i) == modex.dsnTreeModel:
						modex.explorer.setTabEnabled(i, True)
						modex.explorer.setCurrentIndex(i)
					else:
						modex.explorer.setTabEnabled(i, False)

		if mode == MODE_GET_MIDDLE_LINE:
			self.mainWidget.labelProcessMessage.setText("Выберите каркасы для построения осевых линий")
			for but in modex.explorerFilter.filterButtons():
				if but == modex.explorerFilter.showTridbButton:
					but.setChecked(True)
					but.setEnabled(True)
				else:
					but.setChecked(False)
					but.setEnabled(False)

		# геологическое обновление блочной модели и объединение блочных моделей
		if mode in [MODE_GEO_UPDATE_BM, MODE_GEO_MERGE_BM]:
			if mode == MODE_GEO_UPDATE_BM:
				self.mainWidget.labelProcessMessage.setText("Выберите блочные модели для обновления")
			if mode == MODE_GEO_MERGE_BM:
				self.mainWidget.labelProcessMessage.setText("Выберите блочные модели для объединения")
			#
			if mode == MODE_GEO_UPDATE_BM:
				for but in modex.explorerFilter.filterButtons():
					if but == modex.explorerFilter.showModelButton:
						but.setChecked(True)
						but.setEnabled(True)
					else:
						but.setChecked(False)
						but.setEnabled(False)
			if mode == MODE_GEO_MERGE_BM:
				for but in modex.explorerFilter.filterButtons():
					if but == modex.explorerFilter.showModelButton or but == modex.explorerFilter.showTridbButton:
						but.setChecked(True)
						but.setEnabled(True)
					else:
						but.setChecked(False)
						but.setEnabled(False)
			for i in range(modex.explorer.tabBar().count()):
				tab = modex.explorer.widget(i)
				if tab == modex.geoTreeModel:
					modex.explorer.setTabEnabled(i, True)
					modex.explorer.setCurrentIndex(i)
				else:
					modex.explorer.setTabEnabled(i, False)

		# маркшейдерское обновление блочных моделей и отчеты
		if mode in [MODE_SVY_UPDATE_BM, MODE_SVY_REFRESH_BM, MODE_SVY_REPORT_BM, MODE_CUSTOM_REPORT_BM, MODE_DSN_REPORT_BM, MODE_SVY_RESTORE_GEOLOGY, MODE_SVY_RESTORE_GEOLOGY]:
			self.mainWidget.labelProcessMessage.setText("Выберите блочные модели и каркасы")
			for but in modex.explorerFilter.filterButtons():
				if but in [modex.explorerFilter.showModelButton, modex.explorerFilter.showTridbButton]:
					but.setChecked(True)
					but.setEnabled(True)
				else:
					but.setChecked(False)
					but.setEnabled(False)
			for i in range(modex.explorer.tabBar().count()):
				tab = modex.explorer.widget(i)
				if mode in [MODE_SVY_UPDATE_BM, MODE_SVY_REFRESH_BM, MODE_SVY_REPORT_BM]:
					if tab in [modex.geoTreeModel, modex.svyTreeModel]:
						modex.explorer.setTabEnabled(i, True)
						modex.explorer.setCurrentIndex(i)
					else:
						modex.explorer.setTabEnabled(i, False)
				elif mode in [MODE_DSN_REPORT_BM]:
					if tab in [modex.geoTreeModel, modex.dsnTreeModel]:
						modex.explorer.setTabEnabled(i, True)
						modex.explorer.setCurrentIndex(i)
					else:
						modex.explorer.setTabEnabled(i, False)
				elif mode in [MODE_CUSTOM_REPORT_BM, MODE_SVY_RESTORE_GEOLOGY]:
					modex.explorer.setTabEnabled(i, True)
					modex.explorer.setCurrentIndex(0)

		# маркшейдерская графика
		if mode == MODE_SVY_UPDATE_GRAPHIC:
			msg = "Выберите каркасы, разрезы и пополняемый файл графики. Файл аннотаций может быть выбран опциально"
			self.mainWidget.labelProcessMessage.setText(msg)
			for but in modex.explorerFilter.filterButtons():
				if but in [modex.explorerFilter.showModelButton, modex.explorerFilter.showDataButton]:
					but.setChecked(False)
					but.setEnabled(False)
				else:
					but.setChecked(True)
					but.setEnabled(True)
			for i in range(modex.explorer.tabBar().count()):
				tab = modex.explorer.widget(i)
				if tab in [modex.geoTreeModel, modex.dsnTreeModel]:
					modex.explorer.setTabEnabled(i, False)
				else:
					modex.explorer.setTabEnabled(i, True)
				if tab == modex.svyTreeModel:
					modex.explorer.setCurrentIndex(i)

		#
		if mode == MODE_SVY_STOCK_CALCPLOT:
			msg = "Перетащите влево файлы границ складов и каркасы поверхностей. "
			msg += "Объекты для загрузки перетащите вправо (опционально)"
			self.mainWidget.labelProcessMessage.setText(msg)

	def run(self):
		selex = self.mainWidget.selectionExplorer
		selexTree = selex.explorer
		selexFilter = selex.tableWireframeFilter

		if self.current_process is not None:
			return

		#требуется ли подключение к базе данных	
		if self.currentMode in (MODE_GEO_UPDATE_BM, MODE_GEO_MERGE_BM, MODE_GEO_TRANSFER_BM, MODE_GEO_BACKUP_BM, MODE_SVY_UPDATE_BM,
								MODE_SVY_REFRESH_BM, MODE_SVY_REPORT_BM, MODE_SVY_RESTORE_GEOLOGY, MODE_CUSTOM_REPORT_BM,
								MODE_DSN_REPORT_BM,
								COMMAND_GEO_CREATE_UNIT, COMMAND_GEO_DEFINE_MODEL, COMMAND_GEO_ADD_ELEMENT,
								COMMAND_SVY_CREATE_UNIT, COMMAND_SVY_CREATE_SUBUNIT, COMMAND_SVY_DEFINE_MODEL, COMMAND_SVY_ADD_ELEMENT,
								COMMAND_UPDATE_DHDB, COMMAND_UPDATE_TRDB, COMMAND_SURVEY_TO_DB_TRDB,
								COMMAND_LOAD_SVY_POINTS, COMMAND_SVY_CREATE_POINT, COMMAND_SVY_EDIT_POINT,
								COMMAND_SVY_REMOVE_POINT, COMMAND_SVY_BROWSE_REGISTER,
								COMMAND_SVY_ADD_DIRECTION, COMMAND_SVY_EDIT_DIRECTION,
								COMMAND_DSN_CREATE_UNIT, COMMAND_DSN_DEFINE_MODEL, COMMAND_DSN_ADD_ELEMENT,
								COMMAND_IMPORT_GSI, COMMAND_EXPORT_STRING_TO_SIM):
			use_connection = True
		else:
			use_connection = False

		mine_naming_mode_is_1 = self.settings.getValue(ID_DSN_IS_MINENAMING1)
		if mine_naming_mode_is_1:
			module_DsnDefineModel = DsnDefineModelVer1
		else:
			module_DsnDefineModel = DsnDefineModelVer2

		modeDict = {
			MODE_DISPLAY_DATA: DisplayData,
			MODE_GEO_WF_VALIDATION: GeoWireframeAttributeValidation,
			MODE_GEO_UPDATE_BM: GeoUpdateBm,
			MODE_GEO_MERGE_BM: GeoMergeBm,
			MODE_GEO_TRANSFER_BM: GeoTransferBm,
			MODE_GEO_BACKUP_BM: GeoBackupBm,
			MODE_SVY_BACKUP_WF: SvyBackupWf,
			MODE_SVY_WF_VALIDATION: SvyWireframeAttributeValidation,
			MODE_SVY_UPDATE_BM: SvyUpdateBm,
			MODE_SVY_REFRESH_BM: SvyRefreshBm,
			MODE_SVY_REPORT_BM: SvyReportBm,
			MODE_SVY_RESTORE_GEOLOGY: SvyRestoreGeology,
			MODE_CUSTOM_REPORT_BM: CustomReportBm,
			MODE_SVY_UPDATE_GRAPHIC: SvyUpdateGraphic,
			MODE_SVY_STOCK_CALCPLOT: SvyCalcStock,
			MODE_AUTO_VIZEX_PLOT: AutoVizexPlot,
			MODE_DSN_WF_VALIDATION: DsnWireframeAttributeValidation,
			MODE_DSN_REPORT_BM: DsnReportBm,
			MODE_GET_MIDDLE_LINE: GetMiddleLine,
			COMMAND_GEO_CREATE_UNIT: GeoCreateUnit,
			COMMAND_GEO_DEFINE_MODEL: GeoDefineModel,
			COMMAND_GEO_ADD_ELEMENT: GeoAddElement,
			COMMAND_SVY_CREATE_UNIT: SvyCreateUnit,
			COMMAND_SVY_CREATE_SUBUNIT: SvyCreateSubUnit,
			COMMAND_SVY_DEFINE_MODEL: SvyDefineModel,
			COMMAND_SVY_ADD_ELEMENT: SvyAddElement,
			COMMAND_LOAD_DRILLHOLES: LoadDrillholes,
			COMMAND_LOAD_TRENCHES: LoadDetailedTrenches,
			COMMAND_UPDATE_DHDB: GeoUpdateDrillholesDb,
			COMMAND_UPDATE_TRDB: GeoUpdateTrenchesDb,
			COMMAND_SURVEY_TO_DB_TRDB: GeoAddSurveyToDb,
			COMMAND_LOAD_SVY_POINTS: LoadSvyPoints,
			COMMAND_SVY_CREATE_POINT: SvyCreatePoint,
			COMMAND_SVY_EDIT_POINT: SvyEditPoint,
			COMMAND_SVY_REMOVE_POINT: SvyRemovePoint,
			COMMAND_SVY_BROWSE_REGISTER: SvyBrowseRegister,
			COMMAND_SVY_ADD_DIRECTION: SvyAddDirection,
			COMMAND_SVY_EDIT_DIRECTION: SvyEditDirection,
			COMMAND_DSN_CREATE_UNIT: DsnCreateUnit,
			COMMAND_DSN_DEFINE_MODEL: module_DsnDefineModel,
			COMMAND_DSN_ADD_ELEMENT: DsnAddElement,
			COMMAND_DSN_AUTO_CREATE_SOLID: DsnAutoCreateSolid,
			COMMAND_IMPORT_GSI: ImportLeicaGsiDialog,
			COMMAND_EXPORT_STRING_TO_SIM: ExportStringToSim,
		}


		if use_connection and self.srvcnxn is None:
			self.connect_to_database()

		# запускаем процесс, если не требуется подключение или подключение было успешно
		if not use_connection or (use_connection and self.srvcnxn is not None):
			self.current_process = modeDict[self.currentMode](self, use_connection)
			# self.current_process.exec()
			try:					self.current_process.exec()
			except Exception as e:	qtmsgbox.show_error(self, "Ошибка", "Не удалось выполнить операцию. %s" % str(e))

			#
			# Galkin.Begin
			#if self.current_process.is_success and not self.current_process.is_cancelled:
			#	self.toolButtonDisplayData.click()
			# Galkin.End

		# восстановление текущего режима после выполнения команды
		# порядок важен, должен соответствовать кнопкам на тулбаре
		modes = (	MODE_DISPLAY_DATA,
					MODE_GEO_WF_VALIDATION, MODE_SVY_WF_VALIDATION, MODE_DSN_WF_VALIDATION,
					MODE_GEO_UPDATE_BM, MODE_SVY_UPDATE_BM, MODE_SVY_REFRESH_BM, MODE_SVY_RESTORE_GEOLOGY,
					MODE_SVY_REPORT_BM, MODE_DSN_REPORT_BM, MODE_CUSTOM_REPORT_BM, MODE_GEO_MERGE_BM, MODE_GEO_TRANSFER_BM,
					MODE_SVY_UPDATE_GRAPHIC, MODE_SVY_STOCK_CALCPLOT, MODE_AUTO_VIZEX_PLOT, MODE_GET_MIDDLE_LINE
				)
		for mode, action in zip(modes, self.modeGroupButtons.buttons()):
			if action.isChecked() and mode not in [MODE_GEO_TRANSFER_BM, MODE_GEO_BACKUP_BM, MODE_SVY_BACKUP_WF]:
				self.currentMode = mode
				break

		self.mainWidget.progressBar.setRange(0,100)
		self.mainWidget.progressBar.setText("")
		
		self.current_process = None
		self.setFocus()


	def handle_result(self, result):
		val = result.val
		print("got val {}".format(val))

	def startThread(self):
		self.thread = Thread(None, self.handle_result, self)
		self.thread.start()


class Thread(QtCore.QThread):
	finished = QtCore.pyqtSignal(object)

	def __init__(self, queue, callback, parent=None):
		QtCore.QThread.__init__(self, parent)      
		self.queue = queue
		self.finished.connect(callback)
		self.fs = None
		api_manager = ApiManager(self)
		self.plugins = api_manager.get_plygins_by_interface(IApi)
		logger.info("self.plugins")
		logger.info(self.plugins)



	def run(self):
		self.thread_process(1)  

	def thread_process(self, arg):
		self.fs = FlaskServer(self)

		ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
		mm_crt = os.path.join(os.getcwd(), "services", "cert", "mm.crt")
		mm_key = os.path.join(os.getcwd(), "services", "cert", "mm.key")
		ca_crt = os.path.join(os.getcwd(), "services", "cert", "ca.crt")

		ssl_ctx.load_cert_chain(mm_crt, mm_key)
		ssl_ctx.verify_mode = ssl.CERT_REQUIRED
		ssl_ctx.load_verify_locations(cafile = ca_crt)

		#self.fs.socketio.run(self.fs.app,  host="0.0.0.0", port = 5005, use_reloader = False, ssl_context=ssl_ctx)
		self.fs.socketio.run(self.fs.app,  host="0.0.0.0", port = 5005, use_reloader = False)
#
class tkApp:
	def __init__(self, root):
		self.root = root
		self.root.resizable(False, False)
		self.root.overrideredirect(1)

		self._create_widgets()
		self._create_bindings()
		self._grid_widgets()

		self.screen_center()

		self.root.after(1000, self.exec_app)
	def _create_widgets(self):
		image = tk.PhotoImage(file = os.path.join(os.getcwd(), "gui\\img\\splash_2.png"))
		self.splash = tk.Label(self.root, width = 440, height = 248, image = image)
		self.splash.image = image
	def _create_bindings(self):
		self.root.bind("<Escape>", self.root.destroy)
	def _grid_widgets(self):
		self.splash.pack(fill = "both")

	def screen_center(self):
		self.root.update()
		wscreen, hscreen = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
		wroot, hroot = tuple(int(_) for _ in self.root.geometry().split('+')[0].split('x'))
		self.root.geometry("+%d+%d" % (((wscreen - wroot) // 2, (hscreen - hroot) // 2)))

	def exec_app(self):
		self.root.withdraw()
		try: 
			register_logging()
			app = QtGui.QApplication(os.sys.argv)
			app.setWindowIcon(QtGui.QIcon(r'gui\\img\\icons\\ico.png'))
			message = SettingConverter.check_update() 
			if not message is None:
				tkmsgbox.showinfo("Внимание", message)
			nmineWindow = N_MINE()
			nmineWindow.setMinimumSize(QtCore.QSize(1300, 800))
			toScreenCenter(nmineWindow)
			nmineWindow.show()

			exit_result = app.exec_()

			if not nmineWindow.isExitCorrect:
				os.sys.exit(exit_result)

		except Exception as e:
			tkmsgbox.showerror("Ошибка", "Не удалось запустить приложение. %s" % str(e), parent = self.root)

		self.root.destroy()

root = tk.Tk()
tkApp(root)

root.mainloop()

