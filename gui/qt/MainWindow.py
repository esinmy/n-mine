from PyQt4 import QtGui, QtCore

from gui.qt.widgets.TreeExplorers.ModelExplorer import ModelExplorer
from gui.qt.widgets.TreeExplorers.SelectionExplorer import SelectionExplorer
from gui.qt.widgets.TreeExplorers.TransferExplorer import TransferExplorer
from gui.qt.widgets.TreeExplorers.BackupExplorer import BackupExplorer
from gui.qt.widgets.TreeExplorers.SectionExplorer import SectionPlotExplorer
from gui.qt.widgets.TreeExplorers.StockExplorer import StockExplorer

from gui.qt.widgets.CustomProgressBar import CustomProgressBar
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class MainWidget(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(MainWidget, self).__init__(*args, **kw)

		self._createWidgets()
		self._gridWidgets()

	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(2)
		self.layoutMain.setMargin(5)

		self.layoutWidgets = QtGui.QHBoxLayout()
		self.explorerSplitter = QtGui.QSplitter(QtCore.Qt.Horizontal, parent = self)
		self.explorerSplitter.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding))
	
		self.modelExplorer = ModelExplorer(self.explorerSplitter)

		#group of selection explorer, parameter frame and the process title
		self.workspace = QtGui.QWidget(self.explorerSplitter)
		self.layoutWorkspace = QtGui.QVBoxLayout()

		self.labelProcessMessage = QtGui.QLabel(self.workspace)
		self.labelProcessMessage.setMinimumSize(QtCore.QSize(0, 20))
		self.labelProcessMessage.setText("Выберите объекты для загрузки")
		self.labelProcessMessage.setWordWrap(True)

		# self.layoutExplorerParams = QtGui.QVBoxLayout()
		self.layoutExplorerParams = QtGui.QGridLayout()
		self.transferExplorer = TransferExplorer(self.workspace)
		self.backupExplorer = BackupExplorer(self.workspace)
		self.selectionExplorer = SelectionExplorer(self.workspace)
		self.sectionPlotExplorer = SectionPlotExplorer(self.workspace)
		self.stockExplorer = StockExplorer(self.workspace)

		# bottom widget with buttons and progress bar
		self.buttonBox = QtGui.QWidget(self)
		self.layoutButtonBox = QtGui.QHBoxLayout()
		self.layoutButtonBox.setSpacing(5)
		self.buttonGroup = QtGui.QButtonGroup(self.buttonBox)
		self.pushButtonOk = QtGui.QPushButton(self.buttonBox)
		self.pushButtonCancel = QtGui.QPushButton(self.buttonBox)
		# self.pushButtonForms = QtGui.QToolButton(self.buttonBox)
		# self.pushButtonForms.setPopupMode(self.pushButtonForms.MenuButtonPopup)

		# menu = QtGui.QMenu()
		# menu.addAction("&Открыть")
		# menu.addAction("&Сохранить")
		# self.pushButtonForms.setMenu(menu)

		self.progressBar = CustomProgressBar(self.buttonBox)
		self.progressBar.setAlignment(QtCore.Qt.AlignCenter)
		self.progressBar.setFormat("%.2f")

		buttons = (self.pushButtonOk, self.pushButtonCancel)#, self.pushButtonForms)
		names = ["OK", "Отмена"]#, "Формы"]
		for but, name in zip(buttons, names):
			but.setMinimumSize(QtCore.QSize(113, 0))
			but.setMaximumSize(QtCore.QSize(113, 25))
			but.setText(name)
			self.buttonGroup.addButton(but)
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.setSpacing(0)
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.layoutMain.addLayout(self.layoutWidgets)
		self.layoutWidgets.addWidget(self.modelExplorer)
		self.layoutWidgets.addWidget(self.explorerSplitter)
		self.layoutWidgets.addWidget(self.workspace)
		
		self.workspace.setLayout(self.layoutWorkspace)

		self.explorerSplitter.addWidget(self.modelExplorer)
		self.explorerSplitter.addWidget(self.workspace)

		self.explorerSplitter.setStretchFactor(0, 38.2)
		self.explorerSplitter.setStretchFactor(1, 61.8)

		self.layoutWorkspace.addWidget(self.labelProcessMessage)		
		self.layoutWorkspace.addLayout(self.layoutExplorerParams)

		# self.layoutExplorerParams.addWidget(self.transferExplorer)
		# self.layoutExplorerParams.addWidget(self.selectionExplorer)
		# self.layoutExplorerParams.addWidget(self.sectionPlotExplorer)
		# self.layoutExplorerParams.addWidget(self.stockExplorer)

		self.layoutExplorerParams.addWidget(self.transferExplorer, 0, 1, 1, 1)
		self.layoutExplorerParams.addWidget(self.selectionExplorer, 1, 1, 1, 1)
		self.layoutExplorerParams.addWidget(self.sectionPlotExplorer, 2, 1, 1, 1)
		self.layoutExplorerParams.addWidget(self.stockExplorer, 1, 0, 1, 1)
		# индексы
		self.layoutExplorerParams.addWidget(self.backupExplorer, 0, 1, 1, 1)

		self.buttonBox.setLayout(self.layoutButtonBox)
		self.layoutButtonBox.addWidget(self.progressBar)
		# self.layoutButtonBox.addWidget(self.pushButtonForms)
		self.layoutButtonBox.addWidget(self.pushButtonCancel)
		self.layoutButtonBox.addWidget(self.pushButtonOk)
		
		self.layoutMain.addWidget(self.buttonBox)
