from PyQt4 import QtCore, QtGui

from mm.mmutils import *

from utils.constants import *
from utils.osutil import *
from utils.qtutil import *
from utils.fsettings import FormSet

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
from gui.qt.dialogs.BaseDialog import BaseDialog

from gui.qt.frames.settings.GeneralSettingsFrame import GeneralSettingsFrame
from gui.qt.frames.settings.DatabaseSettingsFrame import DatabaseSettingsFrame
from gui.qt.frames.settings.DhdbSettingsFrame import DhdbSettingsFrame
from gui.qt.frames.settings.BlockModelSettings import BlockModelSettings
from gui.qt.frames.settings.MineUnitSettingsFrame import MineUnitSettingsFrame
from gui.qt.frames.settings.FoldersSettingsFrame import FoldersSettingsFrame
from gui.qt.frames.settings.InterpolationSettingsFrame import InterpolationSettingsFrame
from gui.qt.frames.settings.GeoCodeSettingsFrame import GeoCodeSettingsFrame
from gui.qt.frames.settings.SvyCodeSettingsFrame import SvyCodeSettingsFrame
from gui.qt.frames.settings.SvyReportsSettingsFrame import SvyReportsSettingsFrame
from gui.qt.frames.settings.SvyAdvancedSettingsFrame import SvyAdvancedSettingsFrame
from gui.qt.frames.settings.DsnReportsSettingsFrame import DsnReportsSettingsFrame
from gui.qt.frames.settings.DsnAdvancedSettingsFrame import DsnAdvancedSettingsFrame
from gui.qt.frames.settings.SimSettingsFrame import SimSettingsFrame
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class SettingsWindow(BaseDialog):
	def __init__(self, *args, **kw):
		super(SettingsWindow, self).__init__(*args, **kw)

		self.setWindowTitle("Настройки")

		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setModal(True)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.__setAccessibleNames()

		self.loadSettings()

		self.setFixedSize(self.sizeHint())

	def __createVariables(self):
		self._assayElements = []
		self._needsUpdate = False

		self._defaultGeoFolders = [("Блочная модель", ""), ("Каркасы", ""), ("Графика", ""), ("Контуры", ""), ("Растры", "")]
		self._defaultSvyFolders = [("Каркасы", ""), ("Графика", ""), ("Стринги", ""), ("Съемка", "")]
		self._defaultDsnFolders = [("Блочная модель", ""), ("Каркасы", ""), ("Стринги", ""), ("Графика", ""), ("БВР", ""), ("Таблицы", "")]
		self._defaultMineUnitFolders = [("Блочная модель", ""), ("Каркасы", ""), ("Отчеты", "")]

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.layoutSettings = QtGui.QHBoxLayout()
		self.layoutSettings.setSpacing(5)
		
		self.treeSettings = QtGui.QTreeWidget(self)
		self._build_tree_settings()
		
		# expand
		self.treeSettings.expandToDepth(0)
		#
		self.stackedWidgetSettings = QtGui.QStackedWidget(self)
		self.pageGeneral = GeneralSettingsFrame(self.stackedWidgetSettings)
		self.pageDb = DatabaseSettingsFrame(self.stackedWidgetSettings)
		self.pageDhdb = DhdbSettingsFrame(self.stackedWidgetSettings)
		self.pageBlockModel = BlockModelSettings(self.stackedWidgetSettings)
		self.pageMineUnit = MineUnitSettingsFrame(self.stackedWidgetSettings)
		self.pageGeoFolders = FoldersSettingsFrame(self.stackedWidgetSettings)
		self.pageGeoCodes = GeoCodeSettingsFrame(self.stackedWidgetSettings)
		self.pageInterpolation = InterpolationSettingsFrame(self.stackedWidgetSettings)
		self.pageSvyFolders = FoldersSettingsFrame(self.stackedWidgetSettings)
		self.pageSvyCodes = SvyCodeSettingsFrame(self.stackedWidgetSettings)
		self.pageSvyReports = SvyReportsSettingsFrame(self.stackedWidgetSettings)
		self.pageSvyAdvanced = SvyAdvancedSettingsFrame(self.stackedWidgetSettings)
		self.pageDsnFolders = FoldersSettingsFrame(self.stackedWidgetSettings)
		self.pageDsnReports = DsnReportsSettingsFrame(self.stackedWidgetSettings)
		self.pageDsnAdvanced = DsnAdvancedSettingsFrame(self.stackedWidgetSettings)
		self.pageSimSettings = SimSettingsFrame(self.stackedWidgetSettings)


		self.buttonBox = QtGui.QDialogButtonBox(self)
		buttons = QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.RestoreDefaults | QtGui.QDialogButtonBox.Reset | QtGui.QDialogButtonBox.Save
		self.buttonBox.setStandardButtons(buttons)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Save).setText("Сохранить как")
		self.buttonBox.button(QtGui.QDialogButtonBox.Save).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.RestoreDefaults).setText("Сбросить все")
		self.buttonBox.button(QtGui.QDialogButtonBox.RestoreDefaults).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Reset).setText("По умолчанию")
		self.buttonBox.button(QtGui.QDialogButtonBox.Reset).setMinimumWidth(100)

		# self.buttonBox.setFocusPolicy(QtCore.Qt.ClickFocus)

		self.stackedWidgetSettings.setCurrentIndex(1)

		self.insertAllDefaultData()

	def __createBindings(self):
		self.treeSettings.itemSelectionChanged.connect(self.select_frame)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.saveAndClose)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.saveAs)
		self.buttonBox.button(QtGui.QDialogButtonBox.RestoreDefaults).clicked.connect(self.restroreDefault)

		# bind browse functions
		self.pageGeneral.lineEditNetworkPath.mouseDoubleClickEvent = lambda e: self._browse_opendirectory(e, self.pageGeneral.lineEditNetworkPath)

		self.pageDhdb.lineEditDbDirectory.mouseDoubleClickEvent = lambda e: self._browse_opendirectory(e, self.pageDhdb.lineEditDbDirectory)
		self.pageDhdb.lineEditCollarsOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditCollarsOperExpl)
		self.pageDhdb.lineEditAssayOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditAssayOperExpl)
		self.pageDhdb.lineEditSurveyOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditSurveyOperExpl)
		self.pageDhdb.lineEditLithologyOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditLithologyOperExpl)
		self.pageDhdb.lineEditCollarsDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditCollarsDetailExpl)
		self.pageDhdb.lineEditAssayDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditAssayDetailExpl)
		self.pageDhdb.lineEditSurveyDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditSurveyDetailExpl)
		self.pageDhdb.lineEditLithologyDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditLithologyDetailExpl)
		self.pageDhdb.lineEditCollarsTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditCollarsTrenchExpl)
		self.pageDhdb.lineEditAssayTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditAssayTrenchExpl)
		self.pageDhdb.lineEditSurveyTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditSurveyTrenchExpl)
		self.pageDhdb.lineEditLithologyTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfilename(e, self.pageDhdb.lineEditLithologyTrenchExpl)

		self.pageMineUnit.lineEditParentDirectory.mouseDoubleClickEvent = lambda e: self._browse_opendirectory(e, self.pageMineUnit.lineEditParentDirectory)

		self.pageGeoFolders.lineEditParentDirectory.mouseDoubleClickEvent = lambda e: self._browse_opendirectory(e, self.pageGeoFolders.lineEditParentDirectory)
		self.pageSvyFolders.lineEditParentDirectory.mouseDoubleClickEvent = lambda e: self._browse_opendirectory(e, self.pageSvyFolders.lineEditParentDirectory)
		self.pageDsnFolders.lineEditParentDirectory.mouseDoubleClickEvent = lambda e: self._browse_opendirectory(e, self.pageDsnFolders.lineEditParentDirectory)

		self.pageGeoCodes.lineEditIndexField.mouseDoubleClickEvent = lambda e: self.askIndexField(e, self.pageGeoCodes.lineEditIndexField)

		self.pageGeoFolders.gridEditorHideDirs.toolBar.pushButtonRefresh.clicked.connect(lambda: self.setChildDirectories(self.pageGeoFolders))
		self.pageSvyFolders.gridEditorHideDirs.toolBar.pushButtonRefresh.clicked.connect(lambda: self.setChildDirectories(self.pageSvyFolders))
		self.pageDsnFolders.gridEditorHideDirs.toolBar.pushButtonRefresh.clicked.connect(lambda: self.setChildDirectories(self.pageDsnFolders))

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addLayout(self.layoutSettings)
		self.layoutSettings.addWidget(self.treeSettings)
		self.layoutSettings.addWidget(self.stackedWidgetSettings)

		self.stackedWidgetSettings.addWidget(self.pageGeneral)
		self.stackedWidgetSettings.addWidget(self.pageDb)
		self.stackedWidgetSettings.addWidget(self.pageDhdb)
		self.stackedWidgetSettings.addWidget(self.pageBlockModel)
		self.stackedWidgetSettings.addWidget(self.pageMineUnit)
		self.stackedWidgetSettings.addWidget(self.pageGeoFolders)
		self.stackedWidgetSettings.addWidget(self.pageGeoCodes)
		self.stackedWidgetSettings.addWidget(self.pageInterpolation)
		self.stackedWidgetSettings.addWidget(self.pageSvyFolders)
		self.stackedWidgetSettings.addWidget(self.pageSvyCodes)
		self.stackedWidgetSettings.addWidget(self.pageSvyReports)
		self.stackedWidgetSettings.addWidget(self.pageSvyAdvanced)
		self.stackedWidgetSettings.addWidget(self.pageDsnFolders)	
		self.stackedWidgetSettings.addWidget(self.pageDsnReports)
		self.stackedWidgetSettings.addWidget(self.pageDsnAdvanced)
		self.stackedWidgetSettings.addWidget(self.pageSimSettings)

		self.layoutMain.addWidget(self.buttonBox)

	def __setAccessibleNames(self):
		self.pageGeoFolders.lineEditParentDirectory.setAccessibleName(ID_GEO_MAINDIR)
		self.pageGeoFolders.lineEditUnitPrefix.setAccessibleName(ID_GEO_UPREFIX)
		self.pageGeoFolders.lineEditUnitDisplayName.setAccessibleName(ID_GEO_UDSPNAME)
		self.pageGeoFolders.gridEditorUnitSubdirs.setAccessibleName(ID_GEO_USUBDIRS)
		self.pageGeoFolders.gridEditorHideDirs.setAccessibleName(ID_GEO_UHIDEDIRS)
		self.pageGeoFolders.gridEditorDisplayParams.setAccessibleName(ID_GEO_FORMSETS)

		self.pageSvyFolders.lineEditParentDirectory.setAccessibleName(ID_SVY_MAINDIR)
		self.pageSvyFolders.lineEditUnitPrefix.setAccessibleName(ID_SVY_UPREFIX)
		self.pageSvyFolders.lineEditUnitDisplayName.setAccessibleName(ID_SVY_UDSPNAME)
		self.pageSvyFolders.gridEditorUnitSubdirs.setAccessibleName(ID_SVY_USUBDIRS)
		self.pageSvyFolders.gridEditorHideDirs.setAccessibleName(ID_SVY_UHIDEDIRS)
		self.pageSvyFolders.gridEditorDisplayParams.setAccessibleName(ID_SVY_FORMSETS)

		self.pageDsnFolders.lineEditParentDirectory.setAccessibleName(ID_DSN_MAINDIR)
		self.pageDsnFolders.lineEditUnitPrefix.setAccessibleName(ID_DSN_UPREFIX)
		self.pageDsnFolders.lineEditUnitDisplayName.setAccessibleName(ID_DSN_UDSPNAME)
		self.pageDsnFolders.gridEditorUnitSubdirs.setAccessibleName(ID_DSN_USUBDIRS)
		self.pageDsnFolders.gridEditorHideDirs.setAccessibleName(ID_DSN_UHIDEDIRS)
		self.pageDsnFolders.gridEditorDisplayParams.setAccessibleName(ID_DSN_FORMSETS)

	def _build_tree_settings(self):
		self.treeSettings.setMinimumWidth(300)
		self.treeSettings.setHeaderLabel("Настройки")
		stree = []
		stree.append(["Общие", ["База данных SQL", "База данных скважин", "Блочная модель", "Выемочные единицы", "Атрибуты осевых линий"]])
		stree.append(["Геология", ["Коды", "Параметры интерполяции"]])
		stree.append(["Маркшейдерия", ["Коды", "Настройка отчётов", "Дополнительно"]])
		stree.append(["Проектирование", ["Настройка отчётов", "Дополнительно"]])
		for parent, children in stree:
			item = QtGui.QTreeWidgetItem()
			item.setText(0, parent)
			self.treeSettings.addTopLevelItem(item)
			for child_name in children:
				child = QtGui.QTreeWidgetItem(item)
				child.setText(0, child_name)
				self.treeSettings.addTopLevelItem(child)

	def select_frame(self):
		selIndexes = self.treeSettings.selectedIndexes()
		if not selIndexes:
			return
			
		currIndex = selIndexes[0]
		data = currIndex.data()
		parent = currIndex.parent()
		if data == "Общие":														self.stackedWidgetSettings.setCurrentIndex(0)
		elif data == "База данных SQL":											self.stackedWidgetSettings.setCurrentIndex(1)
		elif data == "База данных скважин":										self.stackedWidgetSettings.setCurrentIndex(2)
		elif data == "Блочная модель":											self.stackedWidgetSettings.setCurrentIndex(3)
		elif data == "Выемочные единицы":										self.stackedWidgetSettings.setCurrentIndex(4)
		elif data == "Геология":												self.stackedWidgetSettings.setCurrentIndex(5)
		elif data == "Коды" and parent.data() == "Геология":					self.stackedWidgetSettings.setCurrentIndex(6)
		elif data == "Параметры интерполяции":									self.stackedWidgetSettings.setCurrentIndex(7)
		elif data == "Маркшейдерия":											self.stackedWidgetSettings.setCurrentIndex(8)
		elif data == "Коды" and parent.data() == "Маркшейдерия":				self.stackedWidgetSettings.setCurrentIndex(9)
		elif data == "Настройка отчётов" and parent.data() == "Маркшейдерия":	self.stackedWidgetSettings.setCurrentIndex(10)
		elif data == "Дополнительно" and parent.data() == "Маркшейдерия":		self.stackedWidgetSettings.setCurrentIndex(11)
		elif data == "Проектирование":											self.stackedWidgetSettings.setCurrentIndex(12)
		elif data == "Настройка отчётов" and parent.data() == "Проектирование":	self.stackedWidgetSettings.setCurrentIndex(13)
		elif data == "Дополнительно" and parent.data() == "Проектирование":		self.stackedWidgetSettings.setCurrentIndex(14)
		elif data == "Атрибуты осевых линий": self.stackedWidgetSettings.setCurrentIndex(15)

	# заполнение таблиц
	def insertAllDefaultData(self):
		self.pageMineUnit.gridEditorUnitSubdirs.insertDefaultData(self._defaultMineUnitFolders)
		self.pageGeoFolders.gridEditorUnitSubdirs.insertDefaultData(self._defaultGeoFolders)
		self.pageSvyFolders.gridEditorUnitSubdirs.insertDefaultData(self._defaultSvyFolders)
		self.pageDsnFolders.gridEditorUnitSubdirs.insertDefaultData(self._defaultDsnFolders)

	#
	def setChildDirectories(self, page):
		root = self.pageGeneral.lineEditNetworkPath.text()
		layer_dir = os.path.join(root, page.lineEditParentDirectory.text())
		if not os.path.exists(layer_dir):
			return
		#
		gridEditor = page.gridEditorHideDirs
		gridEditor.dataGrid.removeAll()
		for r, dirnames, filenames in os.walk(layer_dir):
			for dirname in dirnames:
				page.addChildDirectory(dirname)
			return

	#
	# возвращает стартовую директорию обзора для виджета
	def _get_initial_browse_directory(self, widget, root = ""):
		root = self.pageGeneral.lineEditNetworkPath.text() if not root else root

		initialdir = get_full_open_path(os.path.dirname(widget.text()), root = root)
		initialdir = root if not initialdir else initialdir
		return initialdir
	# проверка, находится ли выбранный путь внутри корневой директории
	def _is_valid_path_selected(self, path, root = ""):
		root = self.pageGeneral.lineEditNetworkPath.text() if not root else root
		if not path.lower().startswith(root.lower()) and path.lower() != root.lower():
			qtmsgbox.show_error(self, "Ошибка", "Выбран неверный путь")
			return
		return True
	# проверяет, выбран ли сетевой проект
	def _is_network_path_selected(self):
		path = self.pageGeneral.lineEditNetworkPath.text()
		errmsg = ""
		if is_blank(path):				errmsg = "Укажите директорию"
		elif not os.path.exists(path):	errmsg = "Путь '%s' не существует" % path
		if errmsg:
			qtmsgbox.show_error(self, "Ошибка", errmsg)
			self.stackedWidgetSettings.setCurrentIndex(0)
			self.pageGeneral.lineEditNetworkPath.setFocus()
			return False
		return True
	# проверяет, выбрана ли директория базы данных
	def _is_dbpath_selected(self):
		root = self.pageGeneral.lineEditNetworkPath.text()
		dbpath = self.pageDhdb.lineEditDbDirectory.text()
		errmsg = ""
		# if is_blank(dbpath):				errmsg = "Укажите директорию"
		dbpath = os.path.join(root, dbpath)
		if not os.path.exists(dbpath):	errmsg = "Путь '%s' не существует" % dbpath
		if errmsg:
			qtmsgbox.show_error(self, "Ошибка", errmsg)
			self.stackedWidgetSettings.setCurrentIndex(1)
			self.pageDhdb.lineEditDbDirectory.setFocus()
			return False
		return True

	# выбор существующей директории
	def _browse_opendirectory(self, event, widget):
		if widget != self.pageGeneral.lineEditNetworkPath and not self._is_network_path_selected():
			return

		root = self.pageGeneral.lineEditNetworkPath.text()

		initialdir = self._get_initial_browse_directory(widget)
		path = QtGui.QFileDialog.getExistingDirectory(self, "Выберите директорию", initialdir).replace("/", "\\")
		# ничего не выбрано
		if not path:
			return

		if widget == self.pageGeneral.lineEditNetworkPath:
			setWidgetValue(widget, path)
			return

		if not self._is_valid_path_selected(path, root = root):
			widget.setFocus()
			return

		short_path = get_short_open_path(path, root = root)
		setWidgetValue(widget, short_path)

		#
		if widget == self.pageGeoFolders.lineEditParentDirectory:		self.setChildDirectories(self.pageGeoFolders)
		if widget == self.pageSvyFolders.lineEditParentDirectory:		self.setChildDirectories(self.pageSvyFolders)
		if widget == self.pageDsnFolders.lineEditParentDirectory:		self.setChildDirectories(self.pageDsnFolders)
	# выбор существующего файла
	def _browse_openfilename(self, event, widget):
		if not self._is_network_path_selected():
			return

		# корневой каталог
		root = self.pageGeneral.lineEditNetworkPath.text()

		# проверка файлов база данных
		if widget in (	self.pageDhdb.lineEditCollarsOperExpl, self.pageDhdb.lineEditAssayOperExpl,
						self.pageDhdb.lineEditSurveyOperExpl, self.pageDhdb.lineEditLithologyOperExpl,
						self.pageDhdb.lineEditCollarsDetailExpl, self.pageDhdb.lineEditAssayDetailExpl,
						self.pageDhdb.lineEditSurveyDetailExpl, self.pageDhdb.lineEditLithologyDetailExpl,
						self.pageDhdb.lineEditCollarsTrenchExpl, self.pageDhdb.lineEditAssayTrenchExpl,
						self.pageDhdb.lineEditSurveyTrenchExpl, self.pageDhdb.lineEditLithologyTrenchExpl):
			root = os.path.join(root, self.pageDhdb.lineEditDbDirectory.text())
			if not self._is_dbpath_selected():
				return

		filetypes = "ДАННЫЕ (*.DAT);;СТРИНГ (*.STR)"
		initialdir = self._get_initial_browse_directory(widget, root = root)
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		# ничего не выбрано
		if not path:
			return

		if not self._is_valid_path_selected(path, root = root):
			widget.setFocus()
			return

		short_path = get_short_open_path(path, root = root)
		setWidgetValue(widget, short_path)

	def _getAssayIndexes(self):
		netPath = self.pageGeneral.lineEditNetworkPath.text()
		dhdbPath = self.pageDhdb.lineEditDbDirectory.text()
		assayPath = os.path.join(netPath, dhdbPath, self.pageDhdb.lineEditAssayOperExpl.text())
		if not os.path.exists(assayPath):
			qtmsgbox.show_error(self, "Ошибка", "Файл %s не существует" % assayPath)
			return
		# attemp to open
		f = MicromineFile()
		if not f.open(assayPath):
			qtmsgbox.show_error(self, "Ошибка", "Файл %s не существует" % assayPath)
			return
		header = f.header
		# check if field exists
		indexFld = self.pageGeoCodes.lineEditIndexField.text()
		if not indexFld in header:
			qtmsgbox.show_error(self, "Ошибка", "Поле %s не существует" % indexFld)
			f.close()
			return
		iFld = header.index(indexFld)
		# reading elements
		elements = set()
		for i in range(f.records_count):
			elements.add(f.get_str_field_value(iFld, i+1))
		f.close()
		#
		self._assayElements = sorted(list(elements))
		if "" in self._assayElements:
			self._assayElements.remove("")

	def askIndexField(self, event, widget):
		netPath = self.pageGeneral.lineEditNetworkPath.text()
		dhdbPath = self.pageDhdb.lineEditDbDirectory.text()
		assayPath = os.path.join(netPath, dhdbPath, self.pageDhdb.lineEditAssayOperExpl.text())
		if not os.path.exists(assayPath):
			qtmsgbox.show_error(self, "Ошибка", "Файл %s не существует" % assayPath)
			return

		f = MicromineFile()
		if not f.open(assayPath):
			qtmsgbox.show_error(self, "Ошибка", "Файл %s не существует" % assayPath)
			return
		header = f.header
		f.close()

		app = SelectFromListDialog(self)
		app.setMinimumSize(QtCore.QSize(200, 400))
		app.setWindowTitle("Выберите поле")
		app.labelTitle.setText("Выберите поле")
		app.setValues(header)
		app.exec()

		value = app.getValues()
		if value:
			widget.setText(value[0])
			# reading elements
			self._getAssayIndexes()

	def askAssayIndex(self, item):
		if item.column() != 0:
			return

		netPath = self.pageGeneral.lineEditNetworkPath.text()
		dhdbPath = self.pageDhdb.lineEditDbDirectory.text()
		assayPath = os.path.join(netPath, dhdbPath, self.pageDhdb.lineEditAssayOperExpl.text())
		if not os.path.exists(assayPath):
			qtmsgbox.show_error(self, "Ошибка", "Файл %s не существует" % assayPath)
			return

		if not self._assayElements:
			self._getAssayIndexes()

		app = SelectFromListDialog(self)
		app.setMinimumSize(QtCore.QSize(200, 400))
		app.setWindowTitle("Выберите поле")
		app.labelTitle.setText("Выберите поле")
		app.setValues(self._assayElements)
		app.exec()

		value = app.getValues()
		if value:
			item.setText(value[0])
				
	def loadSettings(self):
		try:
			for widget in iterWidgetChildren(self):
				name = widget.accessibleName()
				if name:
					value = self.main_app.settings.getValue(name)
					if name == ID_SVY_UDSPNAME:
						if not value:
							qtmsgbox.show_error(self, "Ошибка", "Имя маркшейдерского юнита в настройках не должно быть пустым. \n Создание и обработка БМ и каркасов будут не возможны.")
					if name == ID_GEO_UDSPNAME:
						if not value:
							qtmsgbox.show_error(self, "Ошибка", "Имя геологического юнита в настройках не должно быть пустым. \n Создание и обработка БМ и каркасов будут не возможны.")

					if value:
						setWidgetValue(widget, value)
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно загрузить настройки. %s" % str(e))

	def saveAs(self):
		pass

	def saveAndClose(self):
		try:
			for i, widget in enumerate(iterWidgetChildren(self)):
				name = widget.accessibleName()
				value = getWidgetValue(widget)
				if name:
					self.main_app.settings.setValue(name, getWidgetValue(widget))
			self.main_app.settings.save("", 0)
			self._needsUpdate = True
			self.close()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно сохранить настройки. %s" % str(e))

	def restroreDefault(self):
		pass

	def restoreAll(self):
		pass

	@property
	def needsUpdate(self):
		return self._needsUpdate

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
			event.ignore()
		else:
			super(SettingsWindow, self).keyPressEvent(event)


