from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.osutil import set_file_extension
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow


class RenameDatItem(DialogWindow):
    def __init__(self, parent, triDbItem, *args, **kw):
        super(RenameDatItem, self).__init__(parent, *args, **kw)
        self._main = parent
        self._triDbItem = triDbItem
        self.setWindowTitle("Переименовать DAT файл")

        self.__createVariables()
        self.__createWidgets()
        self.__createBindings()
        self.__gridWidgets()

    def __createVariables(self):
        self._newName = None

    def __createWidgets(self):
        self.layoutParams = QtGui.QFormLayout()

        self.labelOldName = QtGui.QLabel(self)
        self.labelOldName.setText("Старое имя")
        self.labelOldName.setMinimumWidth(100)
        self.labelOldName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.lineEditOldName = QtGui.QLineEdit(self)
        self.lineEditOldName.setMinimumWidth(200)
        self.lineEditOldName.setReadOnly(True)
        self.lineEditOldName.setText(self._triDbItem.name())
        self.labelNewName = QtGui.QLabel(self)
        self.labelNewName.setText("Новое имя")
        self.labelNewName.setMinimumWidth(100)
        self.labelNewName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.lineEditNewName = QtGui.QLineEdit(self)
        self.lineEditNewName.setMinimumWidth(200)

    def __createBindings(self):
        self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

    def __gridWidgets(self):
        self.layoutControls.addLayout(self.layoutParams)
        self.layoutParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOldName)
        self.layoutParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOldName)
        self.layoutParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelNewName)
        self.layoutParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditNewName)

    def check(self):
        if is_blank(self.lineEditNewName.text()):
            qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelNewName.text())
            return

        oldpath = self._triDbItem.filePath()
        dirpath = os.path.dirname(oldpath)
        newpath = os.path.join(dirpath, self._newName)

        if os.path.exists(newpath):
            qtmsgbox.show_error(self, "Ошибка", "Файл '%s' уже существует" % os.path.basename(newpath))
            return

        return True

    def prepare(self):
        super(RenameDatItem, self).prepare()

        self._newName = self.lineEditNewName.text()

        if not self.check():
            return

        try:
            self._is_running = True
            self.run()
            self._is_running = False
            self._is_success = True
        except Exception as e:
            self._is_running = False
            self._in_process = False
            qtmsgbox.show_error(self, "Ошибка", "Невозможно переименовать файл. %s" % str(e))

        self.after_run()
        if self._is_success:
            self.close()

    def run(self):

        def restore_filenames():
            nonlocal old_path, new_path
            if not os.path.exists(old_path):
                try:
                    os.rename(new_path, old_path)
                except:
                    pass


        old_path = self._triDbItem.filePath()
        #
        dirpath = os.path.dirname(old_path)
        new_path = os.path.join(dirpath, set_file_extension(self._newName, ITEM_DATA))
        set_file_extension(self._newName, ITEM_DATA)


        try:
            # переименовываем файлы
            try:
                os.rename(old_path, new_path)
            except Exception as e:
                raise Exception("Невозможно переименовать файл  '%s'" % str(e))

        except Exception as e:
            # возвращаем имена, если требуется
            restore_filenames()
            raise Exception(str(e))

        # переименовываем в интерфейсе
        try:
            self._triDbItem.setFilePath(new_path)
            self._main.refreshStandardModels()
            self._main.refreshUserModels()

        except Exception as e:
            raise Exception("Невозможно обновить дерево папок. Перезапустите скрипт. Ошибка: %s" % str(e))

        qtmsgbox.show_information(self, "Выполнено", "Файл успешно переименован")


