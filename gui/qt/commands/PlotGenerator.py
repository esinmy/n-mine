try:		import MMpy
except:		pass

from PyQt4 import QtGui, QtCore
import os

from datetime import datetime
from utils.constants import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog
from gui.qt.ProcessWindow import ProcessWindow

from mm.mmutils import MicromineFile
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class PlotParamsDialog(BaseDialog):
	def __init__(self, *args, **kw):
		super(PlotParamsDialog, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()

		self.layoutGroupBoxes = QtGui.QGridLayout()
		
		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Установки печати")
		self.layoutParams = QtGui.QFormLayout()


		self.groupBoxFragments = QtGui.QGroupBox()
		self.groupBoxFragments.setTitle("Фрагменты отчета")
		self.layoutFragments = QtGui.QFormLayout()
		
		self.groupBoxOutput = QtGui.QGroupBox()
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QFormLayout()

	def __createBindings(self):
		pass
	def __gridWidgets(self):
		pass

class PlotGenerator(ProcessWindow):
	def __init__(self, *args, **kw):
		super(PlotGenerator, self).__init__(*args, **kw)