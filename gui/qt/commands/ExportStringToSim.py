try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os
from mm.mmutils import MicromineFile

import gui.qt.dialogs.messagebox as qtmsgbox

from utils.constants import *
from utils.validation import is_blank
from gui.qt.ProcessWindow import DialogWindow

import xml.etree.ElementTree as ET

MIDDLE_LINE_FIELDS = ['EAST', 'NORTH', 'RL', 'JOIN', 'Имя tridb', 'Имя выработки',
                          'Использование ШАС', 'Время на закладочные работы, ч', 'Наличие крепи', 'Тип крепи',
                          'Среднее количество шпуров, шт', 'Среднее количество скважин в веере, шт',
                          'Средняя длина взрывной скважины, м', 'Количество вееров в стадии взрыва, шт', 'ЛНС, м']

class ExportStringToSim(DialogWindow):
    def __init__(self, *args, **kw):
        super(ExportStringToSim, self).__init__(*args, **kw)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
        self.setWindowTitle("Экспорт стрингов в СИМ")

        self.__createWidgets()
        self.__createBindings()
        self.__gridWidgets()

        self.setFixedSize(self.sizeHint())


    def __createWidgets(self):
        self.layoutMain = QtGui.QVBoxLayout()
        self.layoutMain.setMargin(5)

        self.layoutInput = QtGui.QHBoxLayout()

        self.groupBoxInput = QtGui.QGroupBox(self)
        self.groupBoxInput.setTitle("Входные данные")

        self.layoutInputParams = QtGui.QFormLayout()
        self.labelFileInput = QtGui.QLabel(self.groupBoxInput)
        self.labelFileInput.setText("Файл стрингов")
        self.labelFileInput.setMinimumWidth(100)
        self.labelFileInput.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.lineEditInputFile = QtGui.QLineEdit(self.groupBoxInput)
        self.lineEditInputFile.setMinimumWidth(200)
        self.lineEditInputFile.setReadOnly(True)

        self.groupBoxOutput = QtGui.QGroupBox(self)
        self.groupBoxOutput.setTitle("Выходные данные")

        self.layoutOutputParams = QtGui.QFormLayout()
        self.labelFileOutput = QtGui.QLabel(self.groupBoxOutput)
        self.labelFileOutput.setText("Файл xml")
        self.labelFileOutput.setMinimumWidth(100)
        self.labelFileOutput.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.lineEditOutputFile = QtGui.QLineEdit()
        self.lineEditOutputFile.setMinimumWidth(400)
        self.lineEditOutputFile.setReadOnly(True)

    def __createBindings(self):
        self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
        self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
        self.lineEditInputFile.mouseDoubleClickEvent = lambda e: self.select_input_file(e, self.lineEditInputFile)
        self.lineEditOutputFile.mouseDoubleClickEvent = lambda e: self.select_output_file(e, self.lineEditOutputFile)

    def __gridWidgets(self):
        self.layoutControls.addWidget(self.groupBoxInput)

        self.groupBoxInput.setLayout(self.layoutInputParams)
        self.layoutInputParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelFileInput)
        self.layoutInputParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditInputFile)


        self.layoutControls.addWidget(self.groupBoxOutput)
        self.groupBoxOutput.setLayout(self.layoutOutputParams)
        self.layoutOutputParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelFileOutput)
        self.layoutOutputParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutputFile)

    def select_input_file(self, e, widget):
        filetypes = "СТРИНГ (*.STR)"
        netpath = self.main_app.settings.getValue(ID_NETPATH)
        try:
            initialdir = netpath
        except:
            initialdir = os.getcwd()
        path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
        # ничего не выбрано
        if not path:
            return

        widget.setText(path)

    def select_output_file(self, e, widget):
        filetypes = "Файл вывода (*.XML)"
        netpath = self.main_app.settings.getValue(ID_NETPATH)
        try:
            initialdir = netpath
        except:
            initialdir = os.getcwd()
        path = QtGui.QFileDialog.getSaveFileName(self, "Сохранить как...", initialdir, filetypes).replace("/", "\\")

        # ничего не выбрано
        if not path:
            return

        widget.setText(path)

    def prepare(self):
        if not super(ExportStringToSim, self).prepare():
            return

        #
        # проверка заполненности полей
        labels = [self.labelFileInput, self.labelFileOutput]
        lineEdits = [self.lineEditInputFile, self.lineEditOutputFile]
        for label, lineEdit in zip(labels, lineEdits):
            if is_blank(lineEdit.text()):
                qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
                return

        # проверка директории имени
        if not os.path.exists(self.lineEditInputFile.text()):
            qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не найден" % self.lineEditInputFile.text())
            return
        if not os.path.exists(os.path.dirname(self.lineEditOutputFile.text())):
            qtmsgbox.show_error(self, "Ошибка",
                                "Путь '%s' не найден" % os.path.dirname(self.lineEditOutputFile.text()))
            return

        #
        try:
            self._is_running = True
            self.run()
            self._is_running = False
            self._is_success = True
        except Exception as e:
            if self._use_connection:
                self.main_app.srvcnxn.rollback()
            self._is_running = False
            self._in_process = False
            qtmsgbox.show_error(self, "Ошибка", "%s" % str(e))

        self.after_run()

    def run(self):
        if not super(ExportStringToSim, self).run():
            return

        output_file_path = self.lineEditOutputFile.text()
        input_file_path = self.lineEditInputFile.text()
        input_file = MicromineFile()
        if not input_file.open(input_file_path):
            raise Exception("Невозможно открыть файл '%s'" % input_file_path)

        header = input_file.header
        if not set(MIDDLE_LINE_FIELDS[:4]).issubset(set(header)):
            raise Exception("В файле стрингов должны быть 4 обязательных поля: EAST, NORTH, RL и JOIN.")

        data = input_file.read()

        # field_indices = {i : self.get_field_id_or_none(input_file, i) for i in MIDDLE_LINE_FIELDS}
        x_field_index = input_file.get_field_id('EAST')
        y_field_index = input_file.get_field_id('NORTH')
        z_field_index = input_file.get_field_id('RL')
        join_field_index = input_file.get_field_id('JOIN')
        tridb_name_field_index = input_file.get_field_id('Имя tridb')
        wrf_name_field_index = input_file.get_field_id('Имя выработки')
        truck_use_field_index = input_file.get_field_id('Использование ШАС')
        filling_time_field_index = input_file.get_field_id('Время на закладочные работы, ч')
        anchoring_type_field_index = input_file.get_field_id('Наличие крепи')
        anchoring_rulename_field_index = input_file.get_field_id('Тип крепи')
        millhole_field_index = input_file.get_field_id('Среднее количество шпуров, шт')
        stope_hole_count_field_index = input_file.get_field_id('Среднее количество скважин в веере, шт')
        stope_hole_length_field_index = input_file.get_field_id('Средняя длина взрывной скважины, м')
        stage_ring_num_field_index = input_file.get_field_id('Количество вееров в стадии взрыва, шт')
        lns_field_index = input_file.get_field_id('ЛНС, м')
        input_file.close()

        cur_join = data[0][join_field_index]
        root_xml = ET.Element("exported_data")
        cur_segment = ET.SubElement(root_xml, "segment", name=self.my_get_func(data[0], tridb_name_field_index) + ' ' + self.my_get_func(data[0], wrf_name_field_index))
        cur_attributes = ET.SubElement(cur_segment, "attributes")
        ET.SubElement(cur_attributes, 'truck_use', value=self.my_get_func(data[0], truck_use_field_index))
        ET.SubElement(cur_attributes, 'filling_time', value=self.my_get_func(data[0], filling_time_field_index))
        ET.SubElement(cur_attributes, 'anchoring', rulename=self.my_get_func(data[0], anchoring_rulename_field_index),
                      type=self.my_get_func(data[0], anchoring_type_field_index))
        ET.SubElement(cur_attributes, 'millhole', count=self.my_get_func(data[0], millhole_field_index))
        ET.SubElement(cur_attributes, 'stope_hole', count=self.my_get_func(data[0], stope_hole_count_field_index),
                      length=self.my_get_func(data[0], stope_hole_length_field_index))
        ET.SubElement(cur_attributes, 'stage_ring_num', value=self.my_get_func(data[0], stage_ring_num_field_index))
        ET.SubElement(cur_attributes, 'lns', value=self.my_get_func(data[0], lns_field_index))
        cur_points = ET.SubElement(cur_segment, "points")

        for row in data:
            if row[join_field_index] != cur_join:
                cur_join = row[join_field_index]
                cur_segment = ET.SubElement(root_xml, "segment", name=self.my_get_func(row,
                                                                                       tridb_name_field_index) + ' ' + self.my_get_func(
                    row, wrf_name_field_index))
                cur_attributes = ET.SubElement(cur_segment, "attributes")
                ET.SubElement(cur_attributes, 'truck_use', value=self.my_get_func(row, truck_use_field_index))
                ET.SubElement(cur_attributes, 'filling_time', value=self.my_get_func(row, filling_time_field_index))
                ET.SubElement(cur_attributes, 'anchoring',
                              rulename=self.my_get_func(row, anchoring_rulename_field_index),
                              type=self.my_get_func(row, anchoring_type_field_index))
                ET.SubElement(cur_attributes, 'millhole', count=self.my_get_func(row, millhole_field_index))
                ET.SubElement(cur_attributes, 'stope_hole',
                              count=self.my_get_func(row, stope_hole_count_field_index),
                              length=self.my_get_func(row, stope_hole_length_field_index))
                ET.SubElement(cur_attributes, 'stage_ring_num',
                              value=self.my_get_func(row, stage_ring_num_field_index))
                ET.SubElement(cur_attributes, 'lns', value=self.my_get_func(row, lns_field_index))
                cur_points = ET.SubElement(cur_segment, "points")
            cur_point = ET.SubElement(cur_points, "point")
            cur_point.set('z', str(row[z_field_index]))
            cur_point.set('y', str(row[y_field_index]))
            cur_point.set('x', str(row[x_field_index]))

        tree = ET.ElementTree(root_xml)
        with open(output_file_path, "wb") as fh:
            tree.write(fh, encoding='utf-8', xml_declaration=True)

        qtmsgbox.show_information(self, "Выполнено", "Экспорт файла стринга выполнен успешно!")

    def my_get_func(self, data, index):
        if index == -1: return 'Поле отсутствует'
        else: return str(data[index])
