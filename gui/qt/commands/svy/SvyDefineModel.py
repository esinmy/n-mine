from PyQt4 import QtGui, QtCore
import os
from datetime import datetime

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.osutil import get_current_time

from mm.mmutils import Tridb

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.widgets.DirectoryView import DirectoryView
from gui.qt.commands.dsn.DsnDefineModelVer1 import saveMineToDatabaseVer1
from gui.qt.commands.dsn.DsnDefineModelVer2 import saveMineToDatabaseVer2
from utils.logger import  NLogger

from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import CustomDataModel


LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

OTHER = "ПРОЧЕЕ"

class SvyDefineModel(DialogWindow):
	def __init__(self, *args, **kw):
		super(SvyDefineModel, self).__init__(*args, **kw)

		self.setWindowTitle("Создать файл выработки (tridb)")

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

		self.setActions()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.groupBoxLocation = QtGui.QGroupBox(self)
		self.groupBoxLocation.setTitle("Ввод")
		self.groupBoxLocation.setMaximumHeight(150)
		self.layoutLocation = QtGui.QFormLayout()

		self.labelLayer = QtGui.QLabel(self.groupBoxLocation)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLayer.setMinimumWidth(180)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxLocation)

		self.labelSvyUnit = QtGui.QLabel(self.groupBoxLocation)
		self.labelSvyUnit.setText(self.main_app.settings.getValue(ID_SVY_UDSPNAME))
		self.labelSvyUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSvyUnit.setMinimumWidth(180)
		self.comboBoxSvyUnit = QtGui.QComboBox(self.groupBoxLocation)

		self.labelLocalProject = QtGui.QLabel(self.groupBoxLocation)
		self.labelLocalProject.setText("Локальный проект")
		self.labelLocalProject.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLocalProject.setMinimumWidth(180)
		self.line_edit_local_project = QtGui.QLineEdit(self.groupBoxLocation)
		self.line_edit_local_project.setMaximumWidth(320)
		self.line_edit_local_project.setReadOnly(True)

#### start edit
		self.layoutExplorer = QtGui.QFormLayout()
		self.boxExplorer = QtGui.QGroupBox(self)
		self.boxExplorer.setTitle("Вывод")
		self.directoryView = DirectoryView(self.boxExplorer)
		sizePolicy = self.directoryView.sizePolicy()
		sizePolicy.setVerticalStretch(1)
		self.directoryView.setSizePolicy(sizePolicy)
		for ittype in ITEM_TYPE_LIST:
			if not ittype in [ITEM_DIR, ITEM_TRIDB]:
				self.directoryView.hideItemType(ittype)
			else:
				self.directoryView.showItemType(ittype)

		self.labelOutTriType = QtGui.QLabel(self.boxExplorer)
		self.labelOutTriType.setText("Выбор папки \n (Esc - снять выделение)")
		self.labelOutTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriType.setMinimumWidth(180)
		self.labelOutTriType.setMaximumWidth(180)

		self.labelDirChoosen = QtGui.QLabel(self.boxExplorer)
		self.labelDirChoosen.setText("Выбранная папка:")
		self.labelDirChoosen.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDirChoosen.setMinimumWidth(180)

		self.DirChoosen = QtGui.QLabel(self.boxExplorer)
		self.DirChoosen.setAlignment(QtCore.Qt.AlignLeft)
		self.DirChoosen.setMinimumWidth(300)
		self.DirChoosen.setText("не выбрана")
#### end edit

		self.groupBoxMineName = QtGui.QGroupBox(self)
		self.groupBoxMineName.setTitle("Наименование")
		self.layoutMineName = QtGui.QVBoxLayout()

		self.radioButtonBox = QtGui.QButtonGroup(self.groupBoxMineName)
		self.radioButtonSelect = QtGui.QRadioButton(self)
		self.radioButtonSelect.setText("Выбрать существующую")
		self.radioButtonSelect.setChecked(True)
		self.radioButtonCreate = QtGui.QRadioButton(self)
		self.radioButtonCreate.setText("Создать новую")
		#
		self.radioButtonBox.addButton(self.radioButtonSelect)
		self.radioButtonBox.addButton(self.radioButtonCreate)

		self.layoutMineNameFields = QtGui.QGridLayout()

		self.labelMineSelect = QtGui.QLabel(self.groupBoxMineName)
		self.labelMineSelect.setText("Выработка")
		self.labelMineSelect.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelMineSelect.setMinimumWidth(180)
		self.lineEditMineSelect = QtGui.QLineEdit(self.groupBoxMineName)
		self.lineEditMineSelect.setMinimumWidth(200)
		self.lineEditMineSelect.setReadOnly(True)

		self.labelCategory = QtGui.QLabel(self.groupBoxMineName)
		self.labelCategory.setText("Категория работ")
		self.labelCategory.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelCategory.setMinimumWidth(180)
		self.comboBoxCategory = QtGui.QComboBox(self.groupBoxMineName)

		self.labelGeoUnit = QtGui.QLabel(self.groupBoxMineName)
		self.labelGeoUnit.setText(self.main_app.settings.getValue(ID_GEO_UDSPNAME))
		self.labelGeoUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelGeoUnit.setMinimumWidth(180)
		self.lineEditGeoUnit = QtGui.QLineEdit(self.groupBoxMineName)
		self.lineEditGeoUnit.setMinimumWidth(200)
		self.lineEditGeoUnit.setReadOnly(True)

		self.labelProjectDate = QtGui.QLabel(self.groupBoxMineName)
		self.labelProjectDate.setText("Дата проектирования")
		self.labelProjectDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelProjectDate.setMinimumWidth(180)

		self.dateEditProjectDate = QtGui.QDateEdit(self.groupBoxMineName)
		self.dateEditProjectDate.setMaximumWidth(100)
		self.dateEditProjectDate.setMinimumWidth(100)
		self.dateEditProjectDate.setCalendarPopup(True)
		self.dateEditProjectDate.calendarWidget().setFirstDayOfWeek(QtCore.Qt.Monday)
		self.dateEditProjectDate.setDisplayFormat("yyyy-MM-dd")
		self.dateEditProjectDate.setDate(datetime.now())

		self.checkBoxCreateTridb = QtGui.QCheckBox(self.groupBoxMineName)
		self.checkBoxCreateTridb.setText("Создать файл tridb")

	def __createBindings(self):
		self.comboBoxLayer.currentIndexChanged.connect(self.layerChanged)
		self.comboBoxSvyUnit.currentIndexChanged.connect(self.svyUnitChanged)
		self.line_edit_local_project.mouseDoubleClickEvent = lambda e: self.select_local_project()
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

		self.lineEditGeoUnit.mouseDoubleClickEvent = lambda e: self.selectGeoUnit()
		self.lineEditMineSelect.mouseDoubleClickEvent = lambda e: self.selectMine()

		self.radioButtonSelect.clicked.connect(self.setActions)
		self.radioButtonCreate.clicked.connect(self.setActions)

######## start edit
		selmodel = self.directoryView.selectionModel()
		selmodel.selectionChanged.connect(self.treeAction)

	def treeAction(self, selected, deselected):

		layerName = self.comboBoxLayer.currentText()
		svyUnitName = self.comboBoxSvyUnit.currentText()
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svypath = netpath + "\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerpath = svypath + "\\" + layerName
		unitpath = layerpath + "\\" + svyUnitName
		tridir = unitpath + "\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]
		dir = "...\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR) + \
			  "\\" + layerName + "\\" + svyUnitName + \
			  "\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]

		if not self.directoryView.selectedItems():
			self.DirChoosen.setText("...\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1])
		else:
			for index in selected.indexes():
				item = self.directoryView.itemFromIndex(index)
				temp = self.directoryView.selectedItems()[0].filePath()
				param1, param2 = temp.split(self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1], 1)
				self.DirChoosen.setText("...\\" + \
										self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1] + \
										param2)
######## end edit
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxLocation)

		self.groupBoxLocation.setLayout(self.layoutLocation)
		self.layoutLocation.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.layoutLocation.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)
		self.layoutLocation.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSvyUnit)
		self.layoutLocation.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxSvyUnit)
		self.layoutLocation.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelLocalProject)
		self.layoutLocation.setWidget(2, QtGui.QFormLayout.FieldRole, self.line_edit_local_project)
		self.layoutLocation.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelMineSelect)
		self.layoutLocation.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditMineSelect)

		self.boxExplorer.setLayout(self.layoutExplorer)
		self.layoutExplorer.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTriType)
		self.layoutExplorer.setWidget(0, QtGui.QFormLayout.FieldRole, self.directoryView)
		self.layoutExplorer.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelDirChoosen)
		self.layoutExplorer.setWidget(1, QtGui.QFormLayout.FieldRole, self.DirChoosen)
		self.layoutControls.addWidget(self.boxExplorer)

		self.layoutControls.addWidget(self.groupBoxMineName)
		self.groupBoxMineName.setLayout(self.layoutMineName)
		self.layoutMineName.addWidget(self.radioButtonSelect)
		self.layoutMineName.addWidget(self.radioButtonCreate)

		self.layoutMineName.addLayout(self.layoutMineNameFields)
		self.layoutMineNameFields.addWidget(self.labelMineSelect, 0, 0)
		self.layoutMineNameFields.addWidget(self.lineEditMineSelect, 0, 1)
		self.layoutMineNameFields.addWidget(self.labelCategory, 1, 0)
		self.layoutMineNameFields.addWidget(self.comboBoxCategory, 1, 1)
		self.layoutMineNameFields.addWidget(self.labelGeoUnit, 2, 0)
		self.layoutMineNameFields.addWidget(self.lineEditGeoUnit, 2, 1)
		self.layoutMineNameFields.addWidget(self.labelProjectDate, 3, 0)
		self.layoutMineNameFields.addWidget(self.dateEditProjectDate, 3, 1)
		self.layoutMineNameFields.addWidget(self.checkBoxCreateTridb, 4, 1)

	def select_local_project(self):
		layerName = self.comboBoxLayer.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		if not layerName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите залежь")
			return

		try:					layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		except Exception as e:	return
		#
		if layerId is None:
			return

		cmd = """
			SELECT D.DepartId, P.PitName, D.DepartName, G.WorkName, N.LOC_PROJECT_NAME
			FROM [{0}].[dbo].[DEPARTS] D JOIN [{0}].[dbo].[PITS] P ON D.DepartPitId = P.PitId
			JOIN [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] T ON D.DepartId = T.DepartId
			JOIN [{0}].[dbo].[LOCAL_PROJECT] N ON N.LOC_PROJECT_ID = T.LOC_PROJECT_ID
			LEFT OUTER JOIN [{0}].[dbo].[LOCAL_PROJECT_WORK_TYPES] F ON F.LOC_PROJECT_ID = N.LOC_PROJECT_ID
			LEFT OUTER JOIN [{0}].[dbo].[WORKS] G ON F.WORK_TYPE_ID = G.WorkID
			WHERE N.LAYER_ID = ?
			ORDER BY N.LOC_PROJECT_NAME
		""".format(self.main_app.srvcnxn.dbsvy)

		params = (layerId)
		#
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		for i in data:
			if i[3] is None:
				i[3] = "Нет данных"
		locProjectNames = [""] + [row[0] for row in data]

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите участок")
		model = CustomDataModel(dialog.view, data, ["ID", "ШАХТА", "УЧАСТОК", "ВИД РАБОТ", "ЛОКАЛЬНЫЙ ПРОЕКТ"])
		dialog.view.setModel(model, combobox_filter=[1, 2, 3, 4])
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.view.hideColumn(0)
		dialog.resize(1000, 480)
		dialog.exec()

		result = dialog.getResult()
		if result:
			self.line_edit_local_project.setText(result[0][4])

		self.lineEditMineSelect.setText("")

	def buildCombos(self):
		self.buildComboBoxLayerList()
		self.buildComboBoxSvyUnitList()
		self.buildWorkTypeList()
	def buildComboBoxLayerList(self):
		self.comboBoxLayer.clear()
		#
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT LAYER_DESCRIPTION
			FROM [{0}].[dbo].[LAYER]
			WHERE LOCATION = ?
			ORDER BY LAYER_DESCRIPTION
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		layers = [""] + [row[0] for row in data]
		self.comboBoxLayer.addItems(layers)


	def buildComboBoxSvyUnitList(self):
		self.comboBoxSvyUnit.clear()

		layer = self.comboBoxLayer.currentText()
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svypath = netpath + "\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerpath = svypath + "\\" + layer

		if not layer:
			return
		if os.path.exists(layerpath):
			unitList = [""] + list(filter(lambda x: os.path.isdir(layerpath + "\\" + x), os.listdir(layerpath)))
			self.comboBoxSvyUnit.addItems(unitList)



	def buildWorkTypeList(self):
		self.comboBoxCategory.clear()
		cmd = "SELECT CATEGORY_DESCRIPTION FROM [%s].[dbo].[LKP_MINE_NAMES_CATEGORY] WHERE MINE_NAME_CATEGORY <> ? ORDER BY MINE_NAME_CATEGORY" % (self.main_app.srvcnxn.dbsvy)
		params = (OTHER)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		workTypes = [""] + [row[0] for row in data]
		self.comboBoxCategory.addItems(workTypes)
	#
	def setActions(self):
		selectWidgets = [self.labelMineSelect, self.lineEditMineSelect]
		createWidgets = [self.labelCategory, self.comboBoxCategory, self.labelGeoUnit, self.lineEditGeoUnit, self.labelProjectDate, self.dateEditProjectDate, self.checkBoxCreateTridb]
		if self.radioButtonSelect.isChecked():
			self.lineEditMineSelect.setReadOnly(True)
			for w in createWidgets:		w.setVisible(False)
			self.setFixedSize(QtCore.QSize(628, 515))
		else:
			self.lineEditMineSelect.setReadOnly(False)
			for w in createWidgets:		w.setVisible(True)
			self.setFixedSize(self.sizeHint())
	#
	def layerChanged(self, index):
		self.buildComboBoxSvyUnitList()
		self.line_edit_local_project.setText("")
		self.DirChoosen.setText("не выбрана")

	def svyUnitChanged(self, index):
		self.DirChoosen.setText("не выбрана")
		self.buildOutTypeList()
	def localProjectChanged(self):
		self.lineEditMineSelect.setText("")
		self.buildOutTypeList()

	def buildOutTypeList(self):
		self.directoryView.setFolderPath(None)

		layerName = self.comboBoxLayer.currentText()
		svyUnitName = self.comboBoxSvyUnit.currentText()


		if not all([layerName, svyUnitName]):
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svyDir = netpath + "\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerDir = svyDir + "\\" + layerName
		svyUnitDir = layerDir + "\\" + svyUnitName
		tridir = svyUnitDir + "\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]
		dir = "...\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]
		self.directoryView.setFolderPath(tridir)
		self.DirChoosen.setText(dir)

		for t in self.directoryView.iterChildren():
			if t.__class__.__name__ != 'TreeModelDirItem':
				t.setDisabled(True)

####### end edit

	def selectGeoUnit(self):
		layerName = self.comboBoxLayer.currentText()
		locProjectName = self.line_edit_local_project.text()
		if not layerName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите залежь")
			return
		if not locProjectName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите локальный проект")
			return

		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		try:					geoUnitNames = self.main_app.srvcnxn.getGeoUnitNamesByLocalProject(locProjectName, layerName = layerName, locationName = locationName)
		except Exception as e:	return
		#

		dialog = SelectFromListDialog(self)
		dialog.setWindowTitle("Выберите " + self.main_app.settings.getValue(ID_GEO_UDSPNAME).lower())
		dialog.labelTitle.setText("Выберите геологические юниты")
		dialog.listWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		dialog.setValues(geoUnitNames)
		dialog.exec()

		result = dialog.getValues()
		if result:
			self.lineEditGeoUnit.setText(", ".join(result))

	#
	def selectMine(self):
		layerName = self.comboBoxLayer.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		localProjectName = self.line_edit_local_project.text()
		#
		try:					localProjectId = self.main_app.srvcnxn.getLocalProjectId(localProjectName, layerName = layerName, locationName = locationName)
		except Exception as e:	return
		#
		if localProjectId is None:
			return
		#
		cmd = """
			SELECT Vyr
			FROM [{0}].[dbo].[VYRAB_P] AS V
			INNER JOIN [{0}].[dbo].[LOCAL_PROJECT_MINES] AS P_M
			ON P_M.MINE_ID = V.VyrId
			WHERE P_M.LOC_PROJECT_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (localProjectId)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		data.insert(0, [""])
		

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите значение")
		model = CustomDataModel(dialog.view, data, ["ВЫРАБОТКА"])
		dialog.view.setModel(model)
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.resize(500, 480)
		dialog.exec()

		result = dialog.getResult()
		if not result:
			return
		self.lineEditMineSelect.setText(result[0][0])

		

	#

	def prepare(self):
		if not super(SvyDefineModel, self).prepare():
			return

		layer = self.comboBoxLayer.currentText()
		svyUnitName = self.comboBoxSvyUnit.currentText()
		locProjectName = self.line_edit_local_project.text()
		mineName = self.lineEditMineSelect.text()
		geoUnitNames = self.lineEditGeoUnit.text()
		selectedItems = self.directoryView.selectedItems()

		labels = [self.labelLayer, self.labelSvyUnit, self.labelLocalProject, self.labelMineSelect]
		values = [layer, svyUnitName, locProjectName, mineName]
		if self.radioButtonCreate.isChecked():
			labels.extend([self.labelCategory, self.labelGeoUnit])
			values.extend([self.comboBoxCategory.currentText(), geoUnitNames])
		#
		for label, value in zip(labels, values):
			if not value:
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				return

		#
		if self.radioButtonCreate.isChecked() and self.checkBoxCreateTridb.isChecked():
			invalidSymbols = '\/:*?"<>|'
			if any(map(lambda x: x in mineName, invalidSymbols)):
				qtmsgbox.show_error(self, "Ошибка", "Недопустимый символ в имени выработки")
				return
		#
		#	Не требуется проверки на существование выработки так как далее проходим все этапы и прописываем все связи, которые не прописаны
		#	О всех связях, которые уже были прописаны уведомляем пользователя. 
		#if not self.radioButtonSelect.isChecked():
		#	cmd = "SELECT (SELECT LOC_PROJECT_NAME FROM [{0}].[dbo].[LOCAL_PROJECT] L WHERE L.LOC_PROJECT_ID = M.LOC_PROJECT_ID) + '||~||' + Vyr FROM [{0}].[dbo].[VYRAB_P] M"
		#	cmd = cmd.format(self.main_app.srvcnxn.dbsvy)
		#	data = [row[0] for row in self.main_app.srvcnxn.exec_query(cmd).get_rows()]
		#	if "%s||~||%s" % (locProjectName, mineName) in data:
		#		qtmsgbox.show_error(self, "Ошибка", "Выработка '%s' уже существует" % str(mineName))
		#		return

		#
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svypath = netpath + "\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerpath = svypath + "\\" + layer
		unitpath = layerpath + "\\" + svyUnitName
		tridir = unitpath + "\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]

#### start edit
		if not selectedItems:
#### end edit
			tripath = tridir + "\\%s.tridb" % mineName
#### start edit
		else:
			tripath = self.directoryView.selectedItems()[0].filePath() + "\\%s.tridb" % mineName
#### end edit
		if os.path.exists(tripath) and not qtmsgbox.show_question(self, "Внимание", "Файл '%s' уже существует. Заменить?" % mineName):
			return
		if self.directoryView.selectedItems():
			if self.directoryView.selectedItems()[0].filePath().lower().endswith('.tridb'):
							qtmsgbox.show_error(self, "Ошибка", "Необходимо выбрать папку, а не выработку")
							return
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать файл выработки. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(SvyDefineModel, self).run():
			return

		layerName = self.comboBoxLayer.currentText()
		svyUnitName = self.comboBoxSvyUnit.currentText()
		locProjectName = self.line_edit_local_project.text()
		mineName = self.lineEditMineSelect.text()
		purpose = self.comboBoxCategory.currentText()
		geoUnitNames = self.lineEditGeoUnit.text().split(", ")
		projectDate = datetime.strptime(self.dateEditProjectDate.text(), "%Y-%m-%d")

		locationName = self.main_app.settings.getValue(ID_PROJNAME)

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svypath = netpath + "\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerpath = svypath + "\\" + layerName
		unitpath = layerpath + "\\" + svyUnitName
		tridir = unitpath + "\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]
		strdir = unitpath + "\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[2][1]
		minedir = os.path.join(strdir, mineName)
		if not self.directoryView.selectedItems():
			tripath = tridir + "\\%s.tridb" % mineName
		#### start edit
		else:
			tripath = self.directoryView.selectedItems()[0].filePath() + "\\%s.tridb" % mineName
		#### end edit
		#tripath = tridir + "\\%s.tridb" % mineName
		if self.radioButtonSelect.isChecked() or (self.radioButtonCreate.isChecked() and self.checkBoxCreateTridb.isChecked()):
			if not os.path.exists(minedir):
				try:					os.mkdir(minedir)
				except Exception as e:	raise Exception("Невозможно создать директорию %s. %s" % (minedir, str(e)))
			svy_user_attributes = list(SVY_USER_ATTRIBUTES)
			svy_user_attributes.append(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
			try:					Tridb.create(tripath, svy_user_attributes)
			except Exception as e:	raise Exception("Невозможно создать файл '%s'" % tripath)
		#
		msg = []
		if self.radioButtonCreate.isChecked():
			try:
				mine_naming_mode_is_1 = self.main_app.settings.getValue(ID_DSN_IS_MINENAMING1)
				if mine_naming_mode_is_1:
					msg = saveMineToDatabaseVer1(self.main_app.srvcnxn, locationName, layerName, mineName, locProjectName, purpose, geoUnitNames, projectDate, mineOwner = ID_MINE_OF_SVY)
				else:
					saveMineToDatabaseVer2(self.main_app.srvcnxn, locationName, layerName, mineName, locProjectName, purpose, geoUnitNames, projectDate, mineOwner = ID_MINE_OF_SVY)

			except Exception as e:
				raise Exception(str(e))
			self.main_app.srvcnxn.commit()

		self.buildOutTypeList()
		qtmsgbox.show_information(self, "Выполнено", "Файл выработки '{0}' успешно создан.\n {1}".format (mineName, ",".join(msg)))
