try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os
from datetime import datetime

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.validation import is_blank

from mm.mmutils import Tridb
from mm.formsets import copy_wireframe

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.widgets.DirectoryView import DirectoryView
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyAddElement(DialogWindow):
	def __init__(self, *args, **kw):
		super(SvyAddElement, self).__init__(*args, **kw)

		self.setWindowTitle("Копировать выработку на сервер")
		self.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowMinimizeButtonHint | QtCore.Qt.WindowCloseButtonHint)
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.setMinimumWidth(self.sizeHint().width())
		self.setMaximumWidth(self.sizeHint().width())

		if self.main_app.srvcnxn is not None:
			self.buildCombos()

	def __createVariables(self):
		self.is_mine_techgrid_bind = self.main_app.settings.getValue(ID_SVY_IS_MINETECHGRIDBIND)
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.inputLayout = QtGui.QFormLayout()

		self.labelTriType = QtGui.QLabel(self.groupBoxInput)
		self.labelTriType.setText("Тип")
		self.labelTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelTriType.setMinimumWidth(120)
		self.lineEditTriType = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditTriType.setMinimumWidth(200)		

		self.labelTriName = QtGui.QLabel(self.groupBoxInput)
		self.labelTriName.setText("Имя")
		self.labelTriName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelTriName.setMinimumWidth(120)
		self.lineEditTriName = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditTriName.setMinimumWidth(150)


		self.groupBoxAttributes = QtGui.QGroupBox(self)
		self.groupBoxAttributes.setTitle("Атрибуты")
		self.attributesLayout = QtGui.QGridLayout()

		self.labelAttrLayer = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrLayer.setText("Залежь")
		self.labelAttrLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrLayer.setMinimumWidth(120)
		self.comboBoxAttrLayer = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrLayer.setMinimumWidth(150)

		self.labelAttrBlock = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrBlock.setText("Маркшейдерский блок")
		self.labelAttrBlock.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrBlock.setMinimumWidth(120)
		self.comboBoxAttrBlock = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrBlock.setMinimumWidth(150)

		self.labelAttrSvyUnit = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrSvyUnit.setText(self.main_app.settings.getValue(ID_SVY_UDSPNAME))
		self.labelAttrSvyUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrSvyUnit.setMinimumWidth(120)
		self.comboBoxAttrSvyUnit = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrSvyUnit.setMinimumWidth(150)

		
		self.labelAttrTechGrid = QtGui.QLabel(self.groupBoxInput)
		self.labelAttrTechGrid.setText("Технологическая сеть")
		self.labelAttrTechGrid.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrTechGrid.setMinimumWidth(120)
		self.comboBoxAttrTechGrid = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxAttrTechGrid.setMinimumWidth(150)
		if not self.is_mine_techgrid_bind:
			self.labelAttrTechGrid.setVisible(False)
			self.labelAttrTechGrid.setEnabled(False)
			self.comboBoxAttrTechGrid.setVisible(False)
			self.comboBoxAttrTechGrid.setEnabled(False)



		#self.labelAttrMineBind = QtGui.QLabel(self.groupBoxAttributes)
		#self.labelAttrMineBind.setText("Лента")
		#self.labelAttrMineBind.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		#self.labelAttrMineBind.setMinimumWidth(70)
		#self.comboBoxAttrMineBind = QtGui.QComboBox(self.groupBoxAttributes)
		#self.comboBoxAttrMineBind.setMinimumWidth(150)

		self.labelAttrMineBind= QtGui.QLabel(self.groupBoxAttributes)
		#if self.is_mine_techgrid_bind:
		#	self.labelAttrMineBind.setText("Лента")
		#else:
		#	self.labelAttrMineBind.setText("Подгоризонт")
		self.labelAttrMineBind.setText(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).lower().capitalize())

		self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).lower().capitalize()

		self.labelAttrMineBind.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrMineBind.setMinimumWidth(70)
		self.comboBoxAttrMineBind = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrMineBind.setMinimumWidth(150)

		self.labelAttrCategory = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrCategory.setText("Категория")
		self.labelAttrCategory.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrCategory.setMinimumWidth(70)
		self.comboBoxAttrCategory = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrCategory.setMinimumWidth(150)

		self.labelAttrCode = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrCode.setText("Код")
		self.labelAttrCode.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrCode.setMinimumWidth(70)
		self.comboBoxAttrCode = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrCode.setMinimumWidth(150)

		self.labelAttrDate = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrDate.setText("Дата")
		self.labelAttrDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrDate.setMinimumWidth(70)
		self.dateEditAttrDate = QtGui.QDateEdit(self.groupBoxAttributes)
		self.dateEditAttrDate.setMinimumWidth(150)
		self.dateEditAttrDate.setCalendarPopup(True)
		self.dateEditAttrDate.setCurrentSection(QtGui.QDateTimeEdit.MonthSection)
		currentTime = datetime.now()
		self.dateEditAttrDate.setDate(QtCore.QDate(currentTime.year, currentTime.month, currentTime.day))
		self.dateEditAttrDate.setDisplayFormat("yyyy-MM-dd")

		self.labelAttrAccuracy = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrAccuracy.setText("Съёмка недостоверна")
		self.labelAttrAccuracy.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrAccuracy.setMinimumWidth(70)
		self.checkBoxAttrAccuracy = QtGui.QCheckBox(self.groupBoxAttributes)
		self.checkBoxAttrAccuracy.setMinimumWidth(150)


		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.outputLayout = QtGui.QFormLayout()

		self.directoryView = DirectoryView(self.groupBoxOutput)
		sizePolicy = self.directoryView.sizePolicy()
		sizePolicy.setVerticalStretch(1)
		self.directoryView.setSizePolicy(sizePolicy)
		for ittype in ITEM_TYPE_LIST:
			if not ittype in [ITEM_DIR, ITEM_TRIDB]:
				self.directoryView.hideItemType(ittype)
			else:
				self.directoryView.showItemType(ittype)

		self.labelOutTriType = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTriType.setText("Выработка")
		self.labelOutTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriType.setMinimumWidth(120)

		self.labelOutTriName = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTriName.setText("Имя")
		self.labelOutTriName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriName.setMinimumWidth(120)
		self.lineEditOutTriName = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutTriName.setMinimumWidth(200)

	def __createBindings(self):
		self.lineEditTriType.mouseDoubleClickEvent = lambda e: self.select_tridb(e, self.lineEditTriType)
		self.lineEditTriName.mouseDoubleClickEvent = lambda e: self.select_wireframe(e, self.lineEditTriType, self.lineEditTriName)
		self.lineEditOutTriName.mouseDoubleClickEvent = lambda e: self.select_wireframe(e, self.directoryView, self.lineEditOutTriName)

		
		self.comboBoxAttrMineBind.mouseDoubleClickEvent = lambda e: self.selectLongwall()

		self.comboBoxAttrLayer.currentIndexChanged.connect(self.layerChanged)
		self.comboBoxAttrCategory.currentIndexChanged.connect(self.categoryChanged)

		if self.is_mine_techgrid_bind:
			self.comboBoxAttrTechGrid.currentIndexChanged.connect(self.techGridChanged)
		self.comboBoxAttrSvyUnit.currentIndexChanged.connect(self.svyUnitChanged)
		self.comboBoxAttrMineBind.currentIndexChanged.connect(self.mineBindChanged)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelTriType)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditTriType)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelTriName)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditTriName)

		self.layoutControls.addWidget(self.groupBoxAttributes)
		self.groupBoxAttributes.setLayout(self.attributesLayout)
		self.attributesLayout.addWidget(self.labelAttrLayer, 0, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrLayer, 0, 1)
		self.attributesLayout.addWidget(self.labelAttrBlock, 1, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrBlock, 1, 1)
		self.attributesLayout.addWidget(self.labelAttrSvyUnit, 2, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrSvyUnit, 2, 1)

		self.attributesLayout.addWidget(self.labelAttrTechGrid, 3, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrTechGrid, 3, 1)
		self.attributesLayout.addWidget(self.labelAttrAccuracy, 4, 0)
		self.attributesLayout.addWidget(self.checkBoxAttrAccuracy, 4, 1)

		self.attributesLayout.addWidget(self.labelAttrMineBind, 0, 2)
		self.attributesLayout.addWidget(self.comboBoxAttrMineBind, 0, 3)
		self.attributesLayout.addWidget(self.labelAttrCategory, 1, 2)
		self.attributesLayout.addWidget(self.comboBoxAttrCategory, 1, 3)
		self.attributesLayout.addWidget(self.labelAttrCode, 2, 2)
		self.attributesLayout.addWidget(self.comboBoxAttrCode, 2, 3)		
		self.attributesLayout.addWidget(self.labelAttrDate, 3, 2)
		self.attributesLayout.addWidget(self.dateEditAttrDate, 3, 3)


		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.outputLayout)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTriType)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.directoryView)
		self.outputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelOutTriName)
		self.outputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditOutTriName)

	#
	def buildCombos(self):
		self.buildLayerList()
		self.buildBlockList()
		self.buildSvyUnitList()
		self.buildTechGridList()
		self.buildCategoryList()
		self.buildCodeList()
		self.buildOutTypeList()

		self.checkBoxAttrAccuracy.setChecked(False)

	def buildLayerList(self):
		self.comboBoxAttrLayer.clear()
		try:
			locationName = self.main_app.settings.getValue(ID_PROJNAME)
			cmd = """
				SELECT LAYER_DESCRIPTION
				FROM [{0}].[dbo].[LAYER]
				WHERE LOCATION = ?
				ORDER BY LAYER_DESCRIPTION
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (locationName)
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			layers = [""] + [row[0] for row in data]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список залежей. %s" % str(e))
			return
		self.comboBoxAttrLayer.addItems(layers)
	def buildBlockList(self):
		self.comboBoxAttrBlock.clear()
		#
		layerName = self.comboBoxAttrLayer.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		#
		try:					layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		except Exception as e:	return
		#
		if layerId is None:
			return
		#
		try:
			cmd = """
				SELECT BlockPrim 
				FROM [{0}].[dbo].[BLOCK]  
				WHERE BlockKey IN (
					SELECT BlockKey
					FROM [{0}].[dbo].[BLOCK_PANELS] 
					WHERE PANEL_ID IN (
						SELECT PANEL_ID
						FROM [{0}].[dbo].[PANEL] 
						WHERE LAYER_ID = ?
					)
				)
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (layerId)
			#
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			blockNames = [""] + [row[0] for row in data]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список блоков. %s" % str(e))
			return
		self.comboBoxAttrBlock.addItems(blockNames)
	def buildSvyUnitList(self):
		self.comboBoxAttrSvyUnit.clear()

		layer = self.comboBoxAttrLayer.currentText()
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svypath = netpath + "\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerpath = svypath + "\\" + layer

		if not layer:
			return

		if not os.path.exists(layerpath):
			qtmsgbox.show_error(self, "Ошибка", "Путь '%s' не найден" % layerpath)
			return

		svyUnitNames = [""] + list(filter(lambda x: os.path.isdir(layerpath + "\\" + x), os.listdir(layerpath)))
		self.comboBoxAttrSvyUnit.addItems(svyUnitNames)
	def buildCategoryList(self):
		self.comboBoxAttrCategory.clear()
		categories = [""] + [row[0][0] for row in self.main_app.settings.getValue(ID_SVYCAT) if row[0][1]]
		self.comboBoxAttrCategory.addItems(categories)
	def buildCodeList(self):
		self.comboBoxAttrCode.clear()
		categoryIndex = self.comboBoxAttrCategory.currentIndex() - 1
		codes = [""] + [row[1] for row in self.main_app.settings.getValue(ID_SVYCAT)[categoryIndex][1] if row]
		self.comboBoxAttrCode.addItems(codes)
	def buildTechGridList(self):
		self.comboBoxAttrTechGrid.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		try:
			cmd = """
				SELECT TECH_GRID_NAME
				FROM [{0}].[dbo].[TECH_GRID]
				WHERE LOCATION = ?
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (locationName)
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			techGridNames = [""] + [row[0] for row in data]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список технологических сетей. %s" % str(e))
			return
		#
		self.comboBoxAttrTechGrid.addItems(techGridNames)
	def buildLongwallList(self):
		self.comboBoxAttrMineBind.clear()
		techGridName = self.comboBoxAttrTechGrid.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		if not techGridName:
			return
		#
		try:					techGridId = self.main_app.srvcnxn.getTechGridId(techGridName, locationName)
		except Exception as e:	return
		#
		if techGridId is None:
			return
		#
		try:
			cmd = """
				SELECT LONGWALL_NAME
				FROM [{0}].[dbo].[LONGWALL] 
				WHERE TECH_GRID_ID = ?
				ORDER BY LONGWALL_NAME
			""".format(self.main_app.srvcnxn.dbsvy)
			params  = (techGridId)
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			longwallNames = [""] + [row[0] for row in data]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список лент. %s" % str(e))
			return
		#
		self.comboBoxAttrMineBind.addItems(longwallNames)

	def buildSubunitList(self):
		self.comboBoxAttrMineBind.clear()
		svyUnitPrefixAndName = self.comboBoxAttrSvyUnit.currentText()
		svyUnitPrefix = self.main_app.settings.getValue(ID_SVY_UPREFIX)
		lenSvyUnitPrefix = len(svyUnitPrefix)
		svyUnitName	=  svyUnitPrefixAndName [lenSvyUnitPrefix:]
		layerName = self.comboBoxAttrLayer.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		if not svyUnitName:
			return
		#
		try:					svyUnitId = self.main_app.srvcnxn.getPanelId(svyUnitName,None,layerName, locationName)
		except Exception as e:	return
		#
		if svyUnitId is None:
			return
		#
		try:
			cmd = """
				SELECT NAME
				FROM [{0}].[dbo].[SUBUNIT] 
				WHERE UNIT = ?
				ORDER BY NAME
			""".format(self.main_app.srvcnxn.dbsvy)
			params  = (svyUnitId)
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			subunitNames = [""] + [row[0].rstrip() for row in data]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список подюнитов. %s" % str(e))
			return
		#
		self.comboBoxAttrMineBind.addItems(subunitNames)

	def buildOutTypeList(self):
		self.directoryView.setFolderPath(None)
			
		layerName = self.comboBoxAttrLayer.currentText()
		svyUnitName = self.comboBoxAttrSvyUnit.currentText()

		if not all([layerName, svyUnitName]):
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svyDir = netpath + "\\" + self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerDir = svyDir + "\\" + layerName
		svyUnitDir = layerDir + "\\" + svyUnitName
		tridir = svyUnitDir + "\\" + self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]
		#if not self.is_mine_techgrid_bind:
		#	subUnitName = self.comboBoxAttrMineBind.currentText()
		#	if not subUnitName:
		#		return
		#	tridir = tridir + "\\" + subUnitName


		self.directoryView.setFolderPath(tridir)

	#
	def layerChanged(self):
		self.buildBlockList()
		self.buildSvyUnitList()
	def categoryChanged(self):
		self.buildCodeList()
	def techGridChanged(self):
		self.buildLongwallList()
	def svyUnitChanged(self):
		if self.is_mine_techgrid_bind:
			self.buildOutTypeList()
		else:
			self.buildSubunitList()
	def mineBindChanged(self):
		if not self.is_mine_techgrid_bind:
			self.buildOutTypeList()


	#
	def select_tridb(self, e, widget):
		filetypes = "КАРКАСЫ (*.TRIDB)"
		try:		initialdir = MMpy.Project.path()
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		# ничего не выбрано
		if not path:
			return

		widget.setText(path)
	def select_wireframe(self, e, triwidget, widget):
		if triwidget == self.lineEditTriType:
			tripath = self.lineEditTriType.text()
		if triwidget == self.directoryView:
			selectedItems = self.directoryView.selectedItems()
			if not selectedItems or not selectedItems[0].isTridb():
				qtmsgbox.show_error(self, "Ошибка", "Выберите выработку")
				return
			tripath = selectedItems[0].filePath()
		try:
			tridb = Tridb(tripath)
			wireframe_names = tridb.wireframe_names
			tridb.close()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список каркасов. %s" % str(e))
			return

		dialog = SelectFromListDialog(self)
		dialog.setValues(wireframe_names)
		dialog.setWindowTitle("Выберите каркас")
		dialog.labelTitle.setText("Выберите каркас")
		dialog.exec()

		result = dialog.getValues()
		if result:
			widget.setText(result[0])
			self.lineEditOutTriName.setText(result[0])
	#
	def prepare(self):
		if not super(SvyAddElement, self).prepare():
			return

		reqlabels = (self.labelTriType, self.labelTriName, self.labelAttrLayer, self.labelAttrBlock, self.labelAttrSvyUnit,
					self.labelAttrCategory, self.labelAttrCode, self.labelOutTriName)
		reqfields = (self.lineEditTriType, self.lineEditTriName, self.comboBoxAttrLayer, self.comboBoxAttrBlock, self.comboBoxAttrSvyUnit,
					self.comboBoxAttrCategory, self.comboBoxAttrCode, self.lineEditOutTriName)
		for label, field in zip(reqlabels, reqfields):
			value = str(getWidgetValue(field))
			if is_blank(value):
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				field.setFocus()
				return
		selectedItems = self.directoryView.selectedItems()
		if not selectedItems or not selectedItems[0].isTridb():
			qtmsgbox.show_error(self, "Ошибка", "Выберите выработку")
			self.directoryView.setFocus()
			return
		#
		srcpath = self.lineEditTriType.text()
		srcname = self.lineEditTriName.text()
		#
		if not os.path.exists(srcpath):
			qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не существует")
			return
		#
		try:
			tridb = Tridb(srcpath)
			wireframe_names = tridb.wireframe_names
			tridb.close()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список каркасов")
			return
		#
		if not srcname in wireframe_names:
			qtmsgbox.show_error(self, "Ошибка", "Каркас '%s' не найден" % srcname)
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно скопировать каркас на сервер. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(SvyAddElement, self).run():
			return

		srcpath = self.lineEditTriType.text()
		srcname = self.lineEditTriName.text()

		layerName = self.comboBoxAttrLayer.currentText()
		blockName = self.comboBoxAttrBlock.currentText()
		svyUnitName = self.comboBoxAttrSvyUnit.currentText()
		techGridName = self.comboBoxAttrTechGrid.currentText()
		mineBindName = self.comboBoxAttrMineBind.currentText()
		categoryName = self.comboBoxAttrCategory.currentText()
		codeName = self.comboBoxAttrCode.currentText()
		date = datetime.strptime(self.dateEditAttrDate.text(), "%Y-%m-%d")
		accuracy = 'НД' if self.checkBoxAttrAccuracy.isChecked() else 'Д'
		dstpath = self.directoryView.selectedItems()[0].filePath()
		dsttype = self.directoryView.selectedItems()[0].name()
		dstname = self.lineEditOutTriName.text().rstrip()

		# копирование каркаса
		dsttri = Tridb(dstpath)
		if dstname in dsttri.wireframe_names:
			if not qtmsgbox.show_question(self, "", "Каркас '%s' уже существует. Заменить?" % dstname):
				return
		svy_user_attributes = list(SVY_USER_ATTRIBUTES)
		svy_user_attributes.append(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
		if not all(map(lambda x: x in dsttri.user_attributes, svy_user_attributes)):
			dsttri.close()
			raise ValueError("В каркасе отсутствует один или несколько обязательных пользовательских атрибутов")

		# копирование каркаса
		err = self.exec_proc(copy_wireframe, srcpath, srcname, dstpath, dstname, False)
		if err:
			raise Exception("Невозможно скопировать каркас. %s" % err)

		# задание атрибутов
		icategory, category = [(i, row[0][1]) for i, row in enumerate(self.main_app.settings.getValue(ID_SVYCAT)) if row[0][0] == categoryName][0]
		code = [row[0] for row in self.main_app.settings.getValue(ID_SVYCAT)[icategory][1] if row[1] == codeName][0]
		legend = '1:' + code + '_' + '2:' + accuracy
		svy_attr_MINEBINDATTRNAME = self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper()
		try:
			dsttri[dstname].set_default_attribute(DEFAULT_ATTR_TITLE, dsttype)
			dsttri[dstname].set_default_attribute(DEFAULT_ATTR_CODE, code)
			dsttri[dstname].set_user_attribute(SVY_ATTR_CATEGORY, category)
			dsttri[dstname].set_user_attribute(SVY_ATTR_LAYER, layerName)
			dsttri[dstname].set_user_attribute(SVY_ATTR_BLOCK, blockName)
			dsttri[dstname].set_user_attribute(svy_attr_MINEBINDATTRNAME, mineBindName)	 # либо ЛЕНТА либо ПОДГОРИЗОНТ. Для Кольской 
			dsttri[dstname].set_user_attribute(SVY_ATTR_DATE, date.strftime("%Y-%m-%d"))
			dsttri[dstname].set_user_attribute(SVY_ATTR_MMYY, "%s_%s" % (str(date.month).zfill(2), str(date.year)[-2:]))
			dsttri[dstname].set_user_attribute(SVY_ATTR_ACCURACY, accuracy)
			dsttri[dstname].set_user_attribute(SVY_ATTR_LEGEND, legend)
			
			dsttri.commit()
		except Exception as e:
			dsttri.close()
			raise Exception("Невозможно скопировать каркас. %s" % str(e))

		qtmsgbox.show_information(self, "Выполнено", "Каркас успешно скопирован на сервер")