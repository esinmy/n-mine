from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.osutil import set_file_extension
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.FileMapper import FileMapper
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import CustomDataModel


class SvyDeleteTriDb(DialogWindow):
	def __init__(self, parent, triDbDsnUnitItem, use_connection, *args, **kw):
		super(SvyDeleteTriDb, self).__init__(parent, use_connection, *args, **kw)
		self._triDbSvyUnitItem = triDbDsnUnitItem
		self._use_connection = use_connection
		self.setWindowTitle("Удалить выработку (TriDB)")
		


		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._newName = None
		
		
		#self.localProjectId = None

	def __createWidgets(self):
		self.layoutParams = QtGui.QVBoxLayout()

		self.labelMessage1 = QtGui.QLabel(self)
		self.labelMessage1.setText("Вы действительно хотите удалить выработку (TriDb):<br/><b>'%s'</b>?"  % (self._triDbSvyUnitItem.name()) )
		self.labelMessage1.setMinimumWidth(100)
		self.labelMessage1.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
		
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.addWidget(self.labelMessage1)


	def check(self):

		return True

	def prepare(self):
		super(SvyDeleteTriDb, self).prepare()

		if not self.check():
			return

		if self._use_connection and self.main_app.srvcnxn is None:
			self.main_app.connect_to_database()

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно удалить выработку (TriDb). %s" % str(e))

		self.after_run()
		if self._is_success:
			self.close()

	def run(self):



		def restore_filenames():
			nonlocal unitPath, unitTempPath
			if not os.path.exists(unitPath):
				try:		os.rename(unitTempPath, unitPath)
				except:		pass
		#
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		layerName = self._triDbSvyUnitItem.getLayerName()
		self.layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		#
		unitPath = self._triDbSvyUnitItem.filePath()
		dirpath = os.path.dirname(unitPath)
		unitTempPath = os.path.join(dirpath, set_file_extension(self._tmpprefix + self._triDbSvyUnitItem.name(), ITEM_TRIDB))	
	
		#
		try:

			if not self.__existTriDbUnitInDb ( self._triDbSvyUnitItem.name()):  # Если не существует Выработка  с таким именем
				if not qtmsgbox.show_question(self, "Внимание", "Выработка '{0}' не существует в залежи '{1}'. Удалить файл?".format( self._triDbSvyUnitItem.name(), layerName)):
					return

			else:
				mineId = self.__getMineId(self._triDbSvyUnitItem.name(), self.layerId )
				
				if not mineId:
					if not qtmsgbox.show_question(self, "Внимание", "Выработка '{0}' не определена. Удалить файл?".format( self._triDbSvyUnitItem.name())):
						return

				if mineId:
					if not self.__isMineOfDsn(mineId):
						cmdDel_binding ="""
							delete P_M from [{0}].[dbo].[MINE_PANELS]  as P_M
							where P_M.MINE_ID = ? 
						"""
						cmdDel_binding = cmdDel_binding.format(self.main_app.srvcnxn.dbsvy)
						params = (mineId) 
						self.main_app.srvcnxn.exec_query(cmdDel_binding, params)
				
						cmdDel1 ="""
						delete  from [{0}].[dbo].[MINE_LONGWALLS]  
							where  MINE_ID = ?
						"""
						params1 = (mineId)
						cmdDel1 = cmdDel1.format(self.main_app.srvcnxn.dbsvy)
						cmdDel2 ="""
						delete  from [{0}].[dbo].[LOCAL_PROJECT_MINES]  
							where  MINE_ID = ?
						"""
						params2 = (mineId)
						cmdDel2 = cmdDel2.format(self.main_app.srvcnxn.dbsvy)
						cmdDel3 ="""
						delete  from [{0}].[dbo].[VYRAB_P]  
							where  VyrId = ?
						"""
						params3 = (mineId)
						cmdDel3 = cmdDel3.format(self.main_app.srvcnxn.dbsvy)

				
						try:   
							self.main_app.srvcnxn.exec_query(cmdDel1, params1)
							self.main_app.srvcnxn.exec_query(cmdDel2, params2)
							self.main_app.srvcnxn.exec_query(cmdDel3, params3)
						except Exception as e:
							raise Exception("Невозможно удалить выработку из БД. %s" % str(e))

					elif self.__isMineOfSvy(mineId):
						cmdUpd ="""
						update  [{0}].[dbo].[VYRAB_P]  
						set MINE_OWNER = MINE_OWNER ^ ? 
							where  VyrId = ?
						""".format(self.main_app.srvcnxn.dbsvy)
						params = (ID_MINE_OF_SVY, mineId)
						try:   
							self.main_app.srvcnxn.exec_query(cmdUpd, params)
						except Exception as e:
							raise Exception("Невозможно обновить выработку в БД. %s" % str(e))

			# удаляем файлы
			try:
				os.rename(unitPath, unitTempPath)
				os.remove(unitTempPath)	
				
			except Exception as e:
				raise Exception("Невозможно удалить выработку '%s'" % str(e))
			
			self.main_app.srvcnxn.commit()
			
		except Exception as e:
			# возвращаем имена, если требуется
			restore_filenames()
			self.main_app.srvcnxn.rollback()
			raise Exception(str(e))

		#удаляем в интерфейсе
		try:
			parent = self._triDbSvyUnitItem.parent()
			parent.removeChild(self._triDbSvyUnitItem)
		except Exception as e:
			raise Exception("Невозможно обновить дерево папок. Перезапустите скрипт. Ошибка: %s" % str(e))

	
		qtmsgbox.show_information(self, "Выполнено", "Выработка успешно удалена")

	def __isMineOfDsn(self, mineId):
		cmd ="""
		select * from [{0}].[dbo].[VYRAB_P]  as VR
			where VR.VyrId = ?  
			and VR.MINE_OWNER & ? != 0
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (mineId, ID_MINE_OF_DSN)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		return True if not data is None else False

	def __isMineOfSvy(self, mineId):
		cmd ="""
		select * from [{0}].[dbo].[VYRAB_P]  as VR
			where VR.VyrId = ?  
			and VR.MINE_OWNER & ? != 0
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (mineId, ID_MINE_OF_SVY)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		return True if not data is None else False


	def __getMineId(self, triDbName, layerId):
		mineId = None
		if self.__countTriDbUnitInDb (triDbName) > 1:  # Если  существуют несколько Выработок  с таким именем
			cmd ="""
			select
			 VR.VyrId,
			 VR.Vyr,
			 SUBSTRING(
								(
									SELECT ',' + P.LOC_PROJECT_NAME AS 'data()'
										FROM [{0}].[dbo].[LOCAL_PROJECT_MINES]	AS M_P
										INNER JOIN 	[{0}].[dbo].[LOCAL_PROJECT]	AS P
										ON P.LOC_PROJECT_ID = M_P.LOC_PROJECT_ID
									    WHERE M_P.MINE_ID = VR.VyrId
										FOR XML PATH('')
									
								), 2 , 9999) As PROJECTS,
			 SUBSTRING(
								(
									SELECT ',' + P.PANEL_NAME AS 'data()'
										FROM [{0}].[dbo].[MINE_PANELS]	AS M_P
										INNER JOIN 	[{0}].[dbo].[PANEL]	AS P
										ON P.PANEL_ID = M_P.PANEL_ID
									    WHERE M_P.MINE_ID = VR.VyrId
										FOR XML PATH('')
								), 2 , 9999) As PANELS

			 from [{0}].[dbo].[VYRAB_P]  as VR
				where VR.VYR = ?  
				and exists (
								 select * from [{0}].[dbo].[LOCAL_PROJECT_MINES]  as LP_M
										inner join [{0}].[dbo].[LOCAL_PROJECT]  as LP
										on LP.LOC_PROJECT_ID = LP_M.LOC_PROJECT_ID	and LP_M.MINE_ID = VR.VyrId
										where  LP.LAYER_ID = ?
					)
			"""
			params = (triDbName, layerId )
			cmd = cmd.format(self.main_app.srvcnxn.dbsvy)
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()

			dialog = SelectFromTableDialog(self)
			dialog.setWindowTitle("Выберите выработку")
			model = CustomDataModel(dialog.view, data, ["ID", "ВЫРАБОТКА", "ЛОКАЛЬНЫЙ ПРОЕКТ", "ПАНЕЛИ"])
			dialog.view.setModel(model)
			dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
			dialog.view.hideColumn(0)
			dialog.resize(1000, 480)
			dialog.exec()

			result = dialog.getResult()
			if result:
				mineId = result[0][0]

		else:
					   
			# Узнаём Id выработки
			cmd ="""
				select VyrId from [{0}].[dbo].[VYRAB_P]  as VR
				inner join [{0}].[dbo].[LOCAL_PROJECT_MINES]  as LP_M
				on LP_M.MINE_ID = VR.VyrId
					inner join [{0}].[dbo].[LOCAL_PROJECT]  as LP
					on LP.LOC_PROJECT_ID = LP_M.LOC_PROJECT_ID
				where VR.VYR = ?  
				and LP.LAYER_ID = ?
			""" 
			cmd = cmd.format(self.main_app.srvcnxn.dbsvy)
			params = ( triDbName, layerId)
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			mineId = data[0][0]

		return mineId

		 
	def __existTriDbUnitInDb(self, triDbName):
		if triDbName == None:
			return False

		cmd ="""
		select * from [{0}].[dbo].[VYRAB_P]  as VR
			inner join [{0}].[dbo].[LOCAL_PROJECT_MINES]  as LP_M
			on LP_M.MINE_ID = VR.VyrId
				inner join [{0}].[dbo].[LOCAL_PROJECT]  as LP
				on LP.LOC_PROJECT_ID = LP_M.LOC_PROJECT_ID

			where VR.VYR = ?  
			and LP.LAYER_ID = ?
		"""
		params = (triDbName, self.layerId )
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			self.main_app.srvcnxn.rollback()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))
		if data:
			return True
		else:
			return False

	def __countTriDbUnitInDb(self, triDbName):
		if triDbName == None:
			return False

		cmd ="""
		select count(*) from [{0}].[dbo].[VYRAB_P]  as VR
			inner join [{0}].[dbo].[LOCAL_PROJECT_MINES]  as LP_M
			on LP_M.MINE_ID = VR.VyrId
				inner join [{0}].[dbo].[LOCAL_PROJECT]  as LP
				on LP.LOC_PROJECT_ID = LP_M.LOC_PROJECT_ID

			where VR.VYR = ?  
			and LP.LAYER_ID = ?
		"""
		params = (triDbName, self.layerId )
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			self.main_app.srvcnxn.rollback()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))
		if data:
			return data[0][0]


	#def __getDbTriDbNotMatchedFiles(self, dirpath):

	#	if dirpath == None:
	#		return False

	#	cmd ="""
	#	select V.VYR, V.USERNAME, V.UPDATEDATE
	#		 from [{0}].[dbo].[VYRAB_P] as V 
	#		 inner join  [{0}].[dbo].[MINE_PANELS] as P_M
	#		 on P_M.MINE_ID = V.VyrId
	#		 where P_M.PANEL_ID = ?
	#	"""
	#	params = (self.unitId)
	#	cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

	#	try:
	#		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
	#		_listVyrs = [{'vyr':row[0],
	#					 'unit_name': 'Unknown',#self.__getUnitName(),
	#					 'user_name':row[1] if row[1] != None else '',
	#					 'update_date':row[2].strftime("%Y-%m-%d") if row[2] != None else ''} 
	#			for row in data]
	#	except Exception as e:
	#		self.main_app.srvcnxn.rollback()
	#		raise Exception("Невозможно выполнить запрос. %s" % str(e))


	#	for w_path, w_dirs, w_files in os.walk(dirpath):
	#		_listFiles = [os.path.splitext(i)[0]  for i in w_files]
	#		#del w_dirs[:] # смотрим только верхний уровень

	#	 # Отбираем все строки в _listVyrs не существующие в _listFiles
	#	_listVyrsNotMatchedFiles = [i for i in _listVyrs if i['vyr'] not in _listFiles]

	#	return _listVyrsNotMatchedFiles

	#def __getUnitName(self):
	#	unit_name = None
	#	try:
	#		parents = self._triDbSvyUnitItem.iterParents()
	#		for i in range(0, len(parents)-1):
	#			if parents[i].isSvyUnit():
	#				unit = parents[i].name()
	#				if not is_blank(unit):
	#					unit_name = unit
	#					return unit_name
	#	except:
	#		return unit_name


						
		
