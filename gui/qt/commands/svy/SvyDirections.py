try:		import MMpy
except:		pass

import os, pyodbc, pymssql, math
from PyQt4 import QtGui, QtCore
import re

from utils.qtutil import *
from utils.osutil import *
from utils.constants import *
from utils.dbconnection import DbConnection

from mm.mmutils import MicromineFile

from directions.svydbapi import *
import geometry as geom

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.widgets.SurveyGrid import SvyControlGrid, SvyDirectionGrid, SvyGridStackWidget, N_HEADER_ROWS
from gui.qt.widgets.TableDataView import CustomDataModel
from gui.qt.frames.directions.CalculationFrame import ControlFrame, CalculationFrame
from gui.qt.frames.directions.SurveyInformationFrame import SurveyInformationFrame
from gui.qt.frames.directions.ProjectInformationFrame import ProjectInformationFrame
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def loadDirection(path, display_name):
	TextColset_WsLoadStrings_FormSet1_Nested0= MMpy.FormSet("TEXT_COLSET","16.1.70.0")
	DataGrid0 = MMpy.DataGrid(3,1)
	DataGrid0.set_column_info(0,4,-1)
	DataGrid0.set_column_info(1,5,1)
	DataGrid0.set_column_info(2,6,-1)
	DataGrid0.set_row(0, ["ПРОЕКТ_ВЫР-КА",MMpy.Colour(0,0,255).serialise(),"ПРОЕКТ_ВЫР-КА"])
	DataGrid0.set_row(1, ["ОСЬ_УСЛОВНАЯ",MMpy.Colour(255,0,0).serialise(),"ОСЬ_УСЛОВНАЯ"])
	DataGrid0.set_row(2, ["ВИСОК_МАРК",MMpy.Colour(255,0,0).serialise(),"ВИСОК_МАРК"])
	DataGrid0.set_row(3, ["ВИСОК_НАПР",MMpy.Colour(255,0,0).serialise(),"ВИСОК_НАПР"])
	DataGrid0.set_row(4, ["ОСЬ_ПОЧВА",MMpy.Colour(255,0,255).serialise(),"ОСЬ_ПОЧВА"])
	TextColset_WsLoadStrings_FormSet1_Nested0.set_field("GRID",DataGrid0.serialise())

	WsLoadStrings_FormSet1= MMpy.FormSet("WS_LOAD_STRINGS","16.1.70.0")
	WsLoadStrings_FormSet1.set_field("TRANSPARENCY","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FIELD_PREFIX"," ")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FIELD_DECIMALS","1")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ANGLE_FORMAT","1")
	WsLoadStrings_FormSet1.set_field("PLAN","0")
	WsLoadStrings_FormSet1.set_field("CHECK_ANGLE_FIELD","0")
	WsLoadStrings_FormSet1.set_field("DISPLAY","0")
	WsLoadStrings_FormSet1.set_field("POLYGON_COLOUR",MMpy.Colour(0,0,100).serialise())
	WsLoadStrings_FormSet1.set_field("FIELD_ANGLE","0")
	WsLoadStrings_FormSet1.set_field("CHECK","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FIELD","ANNOTATION")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIELD","1")
	WsLoadStrings_FormSet1.set_field("POINT_FIELD","CODE")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIELD","1")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT","0")
	WsLoadStrings_FormSet1.set_field("SINGLE_POINTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("HATCH",MMpy.Hatch(MMpy.Colour(0,0,0),MMpy.Colour(255,255,255),MMpy.Colour(0,0,0),False,MMpy.LineType.solid,13,0,"MM Hatch Patterns 1").serialise())
	WsLoadStrings_FormSet1.set_field("POLYGONS_BOOL","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ON","0")
	WsLoadStrings_FormSet1.set_field("BELOW","0")
	WsLoadStrings_FormSet1.set_field("ABOVE","1")
	WsLoadStrings_FormSet1.set_field("POINT_COLOUR",MMpy.Colour(255,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_COLOUR",MMpy.Colour(0,0,100).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("POINT_USE_CUSTOM_COLOUR","1")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("POINT_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("SEGMENT_LENGTH_DECIMALS","1")
	WsLoadStrings_FormSet1.set_field("INCLINATION_FORMAT","1")
	WsLoadStrings_FormSet1.set_field("BEARING_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INCLINATION","0")
	WsLoadStrings_FormSet1.set_field("BEARING","0")
	WsLoadStrings_FormSet1.set_field("CUM_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INT_ANGLE","0")
	WsLoadStrings_FormSet1.set_field("SEQ","0")
	WsLoadStrings_FormSet1.set_field("Z","0")
	WsLoadStrings_FormSet1.set_field("Y","0")
	WsLoadStrings_FormSet1.set_field("X","0")
	WsLoadStrings_FormSet1.set_field("SEGMENTS","1")
	WsLoadStrings_FormSet1.set_field("LABEL_POINTS","1")
	WsLoadStrings_FormSet1.set_field("SHOW_POINTS","1")
	WsLoadStrings_FormSet1.set_field("LINE_TYPE","0")
	WsLoadStrings_FormSet1.set_field("JOIN_VARB","JOIN")
	WsLoadStrings_FormSet1.set_field("THICK","1")
	WsLoadStrings_FormSet1.set_field("DEFAULT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("COLOUR_FILE",TextColset_WsLoadStrings_FormSet1_Nested0)
	WsLoadStrings_FormSet1.set_field("COLOUR_VARB","DESCRIPTION")
	WsLoadStrings_FormSet1.set_field("Z_VARB","RL")
	WsLoadStrings_FormSet1.set_field("Y_VARB","NORTH")
	WsLoadStrings_FormSet1.set_field("X_VARB","EAST")
	WsLoadStrings_FormSet1.set_field("FILTER","0")
	WsLoadStrings_FormSet1.set_field("FILE_TYPE","2")
	WsLoadStrings_FormSet1.set_field("WSSTR_FILE", path)
	WsLoadStrings_FormSet1.run(display_name)

class SvyDirections(DialogWindow):
	def __init__(self, *args, **kw):
		super(SvyDirections, self).__init__(*args, **kw)
		self.__createVariables()

	def exec(self):
		timer = QtCore.QTimer()
		self.connectDb()
		self._initGui()
		self.traversal_measure = DbTraversalMeasure(self.main_app.srvcnxn)
		super(SvyDirections, self).exec()

	def __createVariables(self):
		self.data = {}
		self._isEditingMode = False
		self._traversal_measure = None
		self._skipfunctions = {"SAVE": False, "LOAD": False, "EXPORT": False, "REMOVE": True}

	def _initGui(self):
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def isEditingMode(self):
		return self._isEditingMode

	def restoreSkipFunctions(self):
		self._skipfunctions = {"SAVE": False, "LOAD": False, "EXPORT": False, "REMOVE": True}
		
	def __createWidgets(self):
		self.tabWidget = QtGui.QTabWidget(self)

		self.frameInfo = SurveyInformationFrame(self.tabWidget)
		self.frameControl = ControlFrame(self.tabWidget)
		self.frameCalculation = CalculationFrame(self.tabWidget)
		self.frameProject = ProjectInformationFrame(self.tabWidget)

		self.tabWidget.addTab(self.frameInfo, "Информация")
		self.tabWidget.addTab(self.frameControl, "Контроль")
		self.tabWidget.addTab(self.frameCalculation, "Расчет хода")
		self.tabWidget.addTab(self.frameProject, "Направление")

		self.buttonBox.addButton(QtGui.QDialogButtonBox.Apply)
		self.buttonBox.addButton(QtGui.QDialogButtonBox.Reset)
		self.buttonBox.addButton(QtGui.QDialogButtonBox.RestoreDefaults)
		self.buttonBox.addButton(QtGui.QDialogButtonBox.Abort)
		self.buttonBox.button(QtGui.QDialogButtonBox.Abort).setEnabled(False)
		for button in self.buttonBox.buttons():
			button.setMinimumSize(QtCore.QSize(130, 25))
			button.setMaximumSize(QtCore.QSize(130, 25))
			button.setFocusPolicy(QtCore.Qt.NoFocus)
		titles = ["Удалить", "Сохранить в базу", "Загрузить в Визекс", "Экспорт отчета"]
		for button, title in zip(self.buttonBox.buttons()[2:], titles):
			button.setText(title)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setFocusPolicy(QtCore.Qt.NoFocus)
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
		self.buttonBox.button(QtGui.QDialogButtonBox.Apply).clicked.connect(self.prepare_export)
		self.buttonBox.button(QtGui.QDialogButtonBox.Reset).clicked.connect(self.prepare_save)
		self.buttonBox.button(QtGui.QDialogButtonBox.RestoreDefaults).clicked.connect(self.prepare_load)
		self.buttonBox.button(QtGui.QDialogButtonBox.Abort).clicked.connect(self.prepare_remove)
		self.frameControl.gridWidget.grids[0].itemChanged.connect(self.controlItemChanged)
		self.frameProject.project_nav_bar.load_project_vzx_button.clicked.connect(lambda: self.prepare_load(self.frameProject.project_stack.currentIndex()))
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.tabWidget)

	def connectDb(self):
		# caching
		tick = datetime.now()
		hasLocation = self.main_app.srvcnxn.location is not None
		#

		cmd = """
			SELECT  ID, NAME, [DATE]
			FROM [{0}].[dbo].[MP_REGISTER] 
			WHERE exists (
				SELECT * FROM fn_my_permissions ('[{0}].[dbo].'+[GROUP], 'TYPE')
			)
			ORDER BY [DATE] DESC
		""".format(self.main_app.srvcnxn.dbsvy)
		self.data["REGISTERS"] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["ID", "ЖУРНАЛ", "ДАТА"]]
		#
		cmd = """
			SELECT FIOID, SURNAME + ' ' + FIRSTNAME + ' ' + SUVMIDDLE 
			FROM [{0}].[dbo].[SURVEY_FIO]
		""".format(self.main_app.srvcnxn.dbsvy)
		self.data["SURVEYORS"] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["ID", "МАРКШЕЙДЕР"]]
		#
		params = []
		cmd = """
			SELECT V.VYRID, PN.PANEL_ID, L.LAYER_DESCRIPTION, PN.PANEL_DESCRIPTION, V.VYR 
			FROM [{0}].[dbo].[LAYER] AS L
			LEFT JOIN [{0}].[dbo].[PANEL] AS PN
			ON PN.LAYER_ID = L.LAYER_ID
				LEFT JOIN [{0}].[dbo].[MINE_PANELS] AS M_PN
				ON M_PN.PANEL_ID =  PN.PANEL_ID
					RIGHT JOIN 	[{0}].[dbo].[VYRAB_P] V
					ON V.VyrId =  M_PN.MINE_ID
						INNER JOIN [{0}].[dbo].[LOCAL_PROJECT_MINES] AS P_M
						ON P_M.MINE_ID = V.VyrId

			WHERE P_M.LOC_project_id IN (
				SELECT PD.LOC_project_id 
				FROM [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] PD
				WHERE PD.DepartID IN (
					SELECT D.DepartID 
					FROM [{0}].[dbo].[DEPARTS] D
					WHERE D.DepartPitID IN (
						SELECT P.PitID
						FROM [{0}].[dbo].[PITS] P
		""".format(self.main_app.srvcnxn.dbsvy)
		if self.main_app.srvcnxn.location is not None:
			cmd += """ WHERE P.LOCATION = ?"""
			params.append(self.main_app.srvcnxn.location)
		cmd += "))) ORDER BY UpdateDate DESC"

		geoUnitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper()
		self.data["UGMINES"] = [self.main_app.srvcnxn.exec_query(cmd, params).get_rows(), ["V_ID", "P_ID", "ЗАЛЕЖЬ", geoUnitDisplayName, "ВЫРАБОТКА"]]
		#
		cmd = """
			SELECT INSTRUMENT_ID, INSTNAME, INSTMODEL, INSTDESC 
			FROM [{0}].[dbo].[MP_LKP_INSTRUMENTS]
		""".format(self.main_app.srvcnxn.dbsvy)
		self.data["EQUIPMENT"] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["ID", "ОБОРУДОВАНИЕ", "МОДЕЛЬ", "ОПИСАНИЕ"]]
		#
		params = []
		cmd = """
			SELECT DISTINCT
				M.MEASURE_ID
				, M.MEASURENAME
				, """
		cmd += """(
					SELECT Vyr 
					FROM [{0}].[dbo].[VYRAB_P] E 
					WHERE E.VyrId = M.EXCAVATION_ID
		"""
		if hasLocation:
			params.append( self.main_app.srvcnxn.location)
			cmd += """
				AND VyrId IN (
				SELECT V.VYRID 
				FROM [{0}].[dbo].[VYRAB_P] V
				INNER JOIN [{0}].[dbo].[LOCAL_PROJECT_MINES] AS P_M
				ON P_M.MINE_ID = V.VyrId
				WHERE P_M.LOC_project_id IN (
					SELECT PD.LOC_project_id 
					FROM [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] PD
					WHERE PD.DepartID IN (
						SELECT D.DepartID 
						FROM [{0}].[dbo].[DEPARTS] D
						WHERE D.DepartPitID IN (
							SELECT P.PitID
							FROM [{0}].[dbo].[PITS] P
							WHERE P.LOCATION = ?
						)
					)
				)
			)
			"""
		#
		cmd += """) Vyr
				, MEASTIMESTAMP MEASURE_DATE
				, (
					SELECT FIRSTNAME + ' ' + SURNAME + ' ' + SUVMIDDLE 
					FROM [{0}].[dbo].[SURVEY_FIO] S 
					WHERE S.FIOID = M.EXECUTOR_ID
				) SURVEYOR
				, M.MEASDESC MEASURE_DESCRIPTION
				, R.NAME
			FROM [{0}].[dbo].[MP_STATIONMEASURES] S 
				JOIN [{0}].[dbo].[MP_MEASURES] M 
				ON S.MEASURE_ID = M.MEASURE_ID AND S.MEASURENAME = M.MEASURENAME
					INNER JOIN [{0}].[dbo].MP_REGISTER AS R
					ON R.ID = M.REGISTER_ID
			WHERE M.MEASURENAME <> 'UNKNOWN' AND M.MTYPE_ID = 1"""
		#
		if hasLocation:
			params.append( self.main_app.srvcnxn.location)
			cmd += """ AND M.LOCATION = ?"""
		cmd += """ ORDER BY MEASURE_DATE DESC"""
		#
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)
		#
		self.data["MEASURES"] = [self.main_app.srvcnxn.exec_query(cmd, params).get_rows(), ["ID", "СЪЕМКА", "ВЫРАБОТКА", "ДАТА", "ИСПОЛНИТЕЛЬ", "ОПИСАНИЕ", "ЖУРНАЛ"]]
		#
		cmd = """
			SELECT
					BASEPOINT_ID,
					CREATEMEASURE_ID,
					P.MEASURENAME,
					ISNULL(POINT_PREF,'') + POINTNAME + ISNULL(POINT_SUFF, ''),
					BASE_X,
					BASE_Y,
					BASE_Z,
					R.NAME,
					C.NAME
			FROM [{0}].[dbo].[MP_BASE_POINTS] AS P
				INNER JOIN [{0}].[dbo].[MP_MEASURES] M
				ON P.[CREATEMEASURE_ID] = M.[MEASURE_ID] 
					AND P.[MEASURENAME] = M.[MEASURENAME]
						INNER JOIN [{0}].[dbo].MP_REGISTER AS R
						ON R.ID = M.REGISTER_ID
							INNER JOIN [{0}].[dbo].MP_CLASS AS C
							ON C.ID = P.CLASS_ID
		    WHERE BASEREMOVED = 0
			""".format(self.main_app.srvcnxn.dbsvy)
		self.data["POINTS"] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["ID", "CREATEMEASURE_ID", "СЪЕМКА", "ИМЯ", "Y (ВОСТОК)", "X (СЕВЕР)", "Z (ОТМЕТКА)", "ЖУРНАЛ", "КЛАССИФИКАЦИЯ"]]
		cmd = """
			SELECT
					BASEPOINT_ID,
					CREATEMEASURE_ID,
					MEASURENAME,
					ISNULL(POINT_PREF,'') + POINTNAME + ISNULL(POINT_SUFF, ''),
					BASE_X,
					BASE_Y,
					BASE_Z
			FROM [{0}].[dbo].[MP_BASE_POINTS] WHERE BASEREMOVED = 1
			""".format(self.main_app.srvcnxn.dbsvy)
		self.data["POINTS_REMOVED"] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["ID", "CREATEMEASURE_ID", "СЪЕМКА", "ИМЯ", "Y (ВОСТОК)", "X (СЕВЕР)", "Z (ОТМЕТКА)"]]
		# print ("cached:", datetime.now() - tick)

	def getActualRegister(self):


		cmd = """
			SELECT TOP 1 NAME
			FROM [{0}].[dbo].[MP_REGISTER] 
			WHERE exists (
				SELECT * FROM fn_my_permissions ('[{0}].[dbo].'+[GROUP], 'TYPE')
			)
			ORDER BY [DATE] DESC
		""".format(self.main_app.srvcnxn.dbsvy)
		values = self.main_app.srvcnxn.exec_query(cmd).get_column("NAME")
		
		return values[0] if values else ""

	def controlItemChanged(self, item):
		irow, icol = item.row(), item.column()
		text = item.text()
		if irow == N_HEADER_ROWS + 1 and icol == 0:
			if not text:
				self.frameControl.gridWidget.grids[0].setStandPoint(SvyPoint(self.main_app.srvcnxn))
			self.frameCalculation.gridWidget.grids[0].setStandPoint(self.frameControl.gridWidget.grids[0].measure.standpoint)
		if irow == N_HEADER_ROWS and icol == 1:
			if not text:
				self.frameControl.gridWidget.grids[0].setBackPoint(SvyPoint(self.main_app.srvcnxn))
			self.frameCalculation.gridWidget.grids[0].setBackPoint(self.frameControl.gridWidget.grids[0].measure.backpoint)
		else:
			return
		self.frameCalculation.gridWidget.doCalculation(item)

	@property
	def traversal_measure(self):
		return self._traversal_measure
	@traversal_measure.setter
	def traversal_measure(self, measure):
		if type(measure) != DbTraversalMeasure:
			raise TypeError("%s - неверный тип. Ожидался TraversalMeasure")
		self._traversal_measure = measure

		# задаем общую информацию о съемке
		self.frameInfo.setParams()

		# задаем контрольное измерение
		self.frameControl.gridWidget.grids[0].measure = self._traversal_measure.control_measure.measure
		
		# задаем параметры для домеров
		self.frameProject.measure = self._traversal_measure.offsets_measure.measure

		# задаем маркшейдерский ход (задаем марк. ход после домеров, чтобы точка стояния не затерлась)
		self.frameCalculation.gridWidget.grids[0].measure = self._traversal_measure.station_measure.measures[0]
		for measure in self._traversal_measure.station_measure.measures[1:]:
			self.frameCalculation.addGrid()
			self.frameCalculation.gridWidget.grids[-1].measure = measure
		self.frameCalculation.setActions()

		self.frameCalculation.navibar.checkBoxIsDirection.setChecked(self._traversal_measure.station_measure.measures[-1].dirpoint.is_direction)
		

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return or  event.key() == QtCore.Qt.Key_Escape:
			event.ignore()
		else:
			super(SvyDirections, self).keyPressEvent(event)

	def isOkToLoad(self, project_id=False):
		frame = self.frameProject
		if frame.project_group_box.isChecked() and self.frameCalculation.gridWidget.grids[-1].measure.dirpoint.is_direction:
			measure = self._traversal_measure.offsets_measure.measure
			for i in range(len(measure)):
				if project_id is not False and i != project_id:
					continue
				frame.set_index_and_do_actions(i)
				if not measure[i].standpoint.name:
					qtmsgbox.show_error(self, "Ошибка", "Не задана маркшейдерская точка")
					self.tabWidget.setCurrentIndex(3)
					return
				if not measure[i].dirpoint.name:
					qtmsgbox.show_error(self, "Ошибка", "Не задана точка направления")
					self.tabWidget.setCurrentIndex(3)
					return

				labels = (
							frame.project_stack.currentWidget().init_east_label, frame.project_stack.currentWidget().init_north_label, frame.project_stack.currentWidget().init_z_label,
							frame.project_stack.currentWidget().end_east_label, frame.project_stack.currentWidget().end_north_label, frame.project_stack.currentWidget().end_z_label,
							frame.project_stack.currentWidget().bias_step_label, frame.project_stack.currentWidget().bias_tol_label, frame.project_stack.currentWidget().bias_max_dist_label,
							frame.project_stack.currentWidget().bias_up_label, frame.project_stack.currentWidget().bias_down_label, frame.project_stack.currentWidget().bias_width_label
						)
				fields = (
							frame.project_stack.currentWidget().init_east_eline, frame.project_stack.currentWidget().init_north_eline, frame.project_stack.currentWidget().init_z_eline,
							frame.project_stack.currentWidget().end_east_eline, frame.project_stack.currentWidget().end_north_eline, frame.project_stack.currentWidget().end_z_eline,
							frame.project_stack.currentWidget().bias_step_eline, frame.project_stack.currentWidget().bias_tol_eline, frame.project_stack.currentWidget().bias_max_dist_eline,
							frame.project_stack.currentWidget().bias_up_eline, frame.project_stack.currentWidget().bias_down_eline, frame.project_stack.currentWidget().bias_width_eline
						)
				for label, field in zip(labels, fields):
					if not field.text():
						qtmsgbox.show_error(self, "Ошибка", "Укажите %s" % label.text())
						self.tabWidget.setCurrentIndex(3)
						field.setFocus()
						return

			# проверяем, что направление и проект в одном направлении
			for i in range(len(self._traversal_measure.offsets_measure.measure)):
				if project_id is not False and i != project_id:
					continue
				frame.project_stack.setCurrentIndex(i)
				frame.set_index_and_do_actions(i)
				directionVector = measure[i].dirpoint - measure[i].standpoint
				projectVector = measure[i].endpoint - measure[i].startpoint if not measure[i].is_reverse else measure[i].startpoint - measure[i].endpoint
				if directionVector*projectVector <= 0:
					qtmsgbox.show_error(self, "Ошибка", "Направление и проект №{} направлены в разные стороны".format(i + 1))
					self.tabWidget.setCurrentIndex(3)
					return

		return True

	def isOkToExport(self):
		if self.frameProject.project_group_box.isChecked() and not self.isOkToLoad():
			return
		return True
	def isOkToSave(self):
		# проверка frameInfo
		frame = self.frameInfo
		labels = (frame.labelSurveyorName, frame.labelMineName, frame.labelEquipment, frame.labelRegister)
		fields = (frame.lineEditSurveyorName, frame.lineEditMineName, frame.lineEditEquipment, frame.lineEditRegister)
		variables = (self.traversal_measure.surveyor_id, self.traversal_measure.mine_id, self.traversal_measure.equipment_id)
		for label, field, variable in zip(labels, fields, variables):
			# пока что пропускаем поле выработки
			if field == frame.lineEditMineName:
				continue
			value = field.text()
			if not value:
				qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
				self.tabWidget.setCurrentIndex(0)
				field.setFocus()
				return
			elif variable is None:
				qtmsgbox.show_error(self, "Ошибка", "Неверное значение поля '%s'" % label.text())
				self.tabWidget.setCurrentIndex(0)
				field.setFocus()
				return
		register_name = frame.lineEditRegister.text() # для проверки наличия точки визирования в текущем журнале
		# проверка наличия контрольного замера
		frame = self.frameControl
		gw = frame.gridWidget
		measure = gw.grids[0].measure
		points = [measure.standpoint.name, measure.backpoint.name, measure.dirpoint.name]
		messages = ["Выберите точку стояния контрольного замера", "Выберите заднюю точку контрольного замера", "Выберите переднюю точку контрольного замера"]
		for pointname, message in zip(points, messages):
			if not pointname:
				qtmsgbox.show_error(self, "Ошибка", message)
				self.tabWidget.setCurrentIndex(1)
				return
		if not measure.has_all_parameters:
			qtmsgbox.show_error(self, "Ошибка", "Введите все параметры контрольного замера")
			self.tabWidget.setCurrentIndex(1)
			return
		message = "Погрешность при контрольном измерении превышает допустимую. Уверены, что хотите продолжить?"
		if not measure.satisfies and not qtmsgbox.show_question(self, "Внимание", message):
			self.tabWidget.setCurrentIndex(1)
			return

		# проверка хода
		frame = self.frameCalculation
		gw = frame.gridWidget
		for i, grid in enumerate(gw.grids):
			measure = grid.measure
			points = [measure.standpoint.name, measure.backpoint.name, measure.dirpoint.name]
			messages = ["Выберите точку стояния", "Выберите заднюю точку", "Выберите переднюю точку"]
			for pointname, message in zip(points, messages):
				if not pointname:
					qtmsgbox.show_error(self, "Ошибка", message)
					self.tabWidget.setCurrentIndex(2)
					gw.setCurrentIndex(i)
					frame.setActions()
					return
			if not measure.has_all_parameters:
				qtmsgbox.show_error(self, "Ошибка", "Введите все параметры замера")
				self.tabWidget.setCurrentIndex(2)
				gw.setCurrentIndex(i)
				frame.setActions()
				return
			if not measure.dirpoint.has_coords:
				qtmsgbox.show_error(self, "Ошибка", "Передняя точка не имеет координат")
				self.tabWidget.setCurrentIndex(2)
				gw.setCurrentIndex(i)
				frame.setActions()
				return
			if not self.isEditingMode() and not measure.dirpoint.is_removed and (measure.dirpoint.exists or measure.dirpoint.get_name_exists(register_name)) and not measure.dirpoint.isNeedOvercoordinate:
				qtmsgbox.show_error(self, "Ошибка", "Точка '%s' уже существует" % measure.dirpoint.name)
				self.tabWidget.setCurrentIndex(2)
				gw.setCurrentIndex(i)
				frame.setActions()
				return

		if not self.isOkToLoad():
			return

		return True

	def prepare_save(self):
		self._skipfunctions = {"SAVE": False, "LOAD": True, "EXPORT": True, "REMOVE": True}
		self.prepare()
	def save(self):
		# добавление маркшейдерского хода
		self._traversal_measure.station_measure.measures = [grid.measure for grid in self.frameCalculation.gridWidget.grids]

		# считывание данных для расчета домеров
		self._traversal_measure.needs_offsets = self.frameProject.project_group_box.isChecked()

		# сохранение замера
		if self._traversal_measure.needs_offsets and all(self._traversal_measure.offsets_measure.measure[i].calc_diraxis() for i in range(len(self._traversal_measure.offsets_measure.measure))) is None:
			if not qtmsgbox.show_question(self, "Внимание", "Направление находится вне проектной выработки. Продолжить?"):
				return
		self._traversal_measure.save()

		self.main_app.srvcnxn.commit()

		qtmsgbox.show_information(self, "Выполнение", "Направление успешно сохранено")

	def prepare_load(self, project_id=False):
		self._skipfunctions = {"SAVE": True, "LOAD": False, "EXPORT": True, "REMOVE": True}
		self.prepare(project_id)

	def load(self, project_id=False):
		for index in range(len(self.frameProject.measure)):
			if project_id is not False and index != project_id:
				continue
			self.frameProject.getParams(index)

		measure = self._traversal_measure.offsets_measure.measure
		for i in range(len(measure)):
			if project_id is not False and i != project_id:
				continue
			self.frameProject.set_index_and_do_actions(i)
			try:
				data = self._traversal_measure.offsets_measure.measure[i].calculate()
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Для проекта №{} не удалось загрузить данные. {}".format(i+1, str(e)))
				return

			path = os.path.join(self._tmpfolder, get_temporary_prefix() + "OFFSETS{}.STR".format(i))
			display_name = "%s (%s) Проект №%s" % (self.frameInfo.lineEditMineName.text(), self.frameCalculation.gridWidget.grids[-1].measure.standpoint.name, i+1)
			MicromineFile.write(path, data, ["EAST", "NORTH", "RL", "JOIN", "CODE", "ANNOTATION", "DESCRIPTION"])

			loadDirection(path, display_name)

	def prepare_export(self):
		self._skipfunctions = {"SAVE": True, "LOAD": True, "EXPORT": False, "REMOVE": True}
		self.prepare()
	def export(self):
		import xlsxwriter as xl
		from xlsxwriter.utility import xl_range, xl_col_to_name, xl_rowcol_to_cell, xl_cell_to_rowcol

		filetypes = "EXCEL (*.XLSX)"
		try:
			initialdir = MMpy.Project.path()
		except:
			initialdir = os.getcwd()
		path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		if not path:
			return

		measure = self._traversal_measure.offsets_measure.measure
		# считывание общей информации о съемке
		date = self._traversal_measure.timestamp.strftime("%d.%m.%Y") if self._traversal_measure.timestamp else ""
		# из-за отсутствия соответствия айдишников новой и старой БД
		# в отчете могут появляться значения None
		surveyor = self._traversal_measure.surveyor_name
		minename = self._traversal_measure.mine_name if self._traversal_measure.mine_name else ""
		equipment = self._traversal_measure.equipment_name
		survey_name = [val.standpoint.name for val in measure]

		tolerance = [val.tolerance if val.tolerance is not None else 1 for val in measure]

		uoffset = [round(val.uoffset, tolerance[i]) if val.uoffset is not None else "" for i, val in enumerate(measure)]
		doffset = [round(val.doffset, tolerance[i]) if val.doffset is not None else "" for i, val in enumerate(measure)]

		total_distance = []
		# Прежний расчет расстояния проходки (длина интервала)
		# Нужно уточнить, что это было неверно
		# for i, val in enumerate(measure):
		# 	dist = ((val.standpoint.x - val.endpoint.x) ** 2 + (val.standpoint.y - val.endpoint.y) ** 2) ** 0.5 \
		# 		if val.standpoint.name and val.endpoint else ""
		# 	total_distance.append(str(round(dist, 3)) if dist else "")

		inclination = [(val.endpoint - val.startpoint).inclination if val.endpoint and val.startpoint else "" for val in
		               measure]
		plumbs = [val.calc_plumbs() for val in measure]

		svyv, dirv = [], []
		for i, val in enumerate(plumbs):
			if val:
				svyPlumb, dirPlumb = val
				svyv.append(round((svyPlumb - measure[i].standpoint).norm, 3))
				dirv.append(round((dirPlumb - measure[i].dirpoint).norm, 3))
			else:
				svyv.append('')
				dirv.append('')

		# условная точка
		alamo = []
		for val in measure:
			try:
				offsets = val.calculate()
				alamoData = list(filter(lambda x: x[-1] == "ОСЬ_УСЛОВНАЯ", offsets))[-1][:3]
				alamo.append(geom.Point(*alamoData))
			except:
				alamo.append(None)

		for i, val in enumerate(measure):
			dist = ((val.standpoint.x - alamo[i].x) ** 2 + (val.standpoint.y - alamo[i].y) ** 2) ** 0.5 \
				if val.standpoint.name and alamo[i] else ""
			total_distance.append(str(round(dist, 3)) if dist else "")

		# alamo = measure.calc_alamo()
		# if alamo:
		# 	try:
		# 		# расчет координаты z (согласно методике на Октябрьском)
		# 		avgDist = round(self._traversal_measure.station_measure.measures[-1].calc_hdistAvg(), 3)
		# 		zv1 = round(measure.standpoint.z - svyv, 3)
		# 		zv2 = round(measure.dirpoint.z - dirv, 3)
		# 		dzv = round(zv2-zv1, 3) if not measure.is_reverse else round(zv1-zv2, 3)
		# 		dip = dzv / avgDist
		# 		if inclination < 0:
		# 			dip = -dip
		# 		dzAlamo = float(total_distance) * dip
		# 		alamo.z = measure.standpoint.z - svyv + dzAlamo
		# 	except Exception as e:
		# 		print ("Не удалось вычислить координату Z условной точки. %s" % str(e))
		# 		alamo = None

		# считывание контроля и перенос ячеек в соответствии с формой отчёта
		data_control = self.frameControl.gridWidget.to_list()
		data_control[0][7] = u"Δz=" + data_control[0][5]
		data_control[3][3] = data_control[3][5]

		# удаление ненужного столбца
		zipped_data_control = list(zip(*data_control))
		zipped_data_control.remove(zipped_data_control[5])
		data_control = list(zip(*zipped_data_control))

		# считывание подсчёта и перенос ячеек в соответствии с формой отчёта
		data_calculation = self.frameCalculation.gridWidget.to_list()

		for i, data in enumerate(data_calculation, start=0):
			if i % 4 == 3:
				data_calculation[i][3] = data_calculation[i][5]
			if data_calculation[i][5] == "Среднее":
				data_calculation[i + 2][4] = u"ΔZ=" + data_calculation[i + 1][5]
		# удаление ненужного столбца
		zipped_data_calculation = list(zip(*data_calculation))

		zipped_data_calculation.remove(zipped_data_calculation[5])
		data_calculation = list(zip(*zipped_data_calculation))

		# получаем таблицу для отчёта
		data = data_control + data_calculation

		# дополнение GRID колонкой наименования точек
		for i, row in enumerate(data):
			data[i] = list(data[i])
			if data[i][0] != "":
				data[i].append(data[i][0])
			elif data[i][1] != "":
				data[i].append(data[i][1])
			else:
				data[i].append("")

		# домеры
		offsets, noffsets = [], []
		for i, val in enumerate(measure):
			offsets.append(val.calc_offsets())
			noffsets.append(len(offsets[i]["DISTANCE"]))

		# экспорт
		wb = xl.Workbook(path)
		wsmain = wb.add_worksheet()
		wsmain.name = "Направление"
		wsmain.set_margins(0.35, 0.35, 0.35, 0.35)  # поля

		wstask = wb.add_worksheet()
		wstask.name = "Задание"
		wsvary = wb.add_worksheet()
		wsvary.name = "Переменные зад."

		# форматы
		fmtAlignLeft = wb.add_format({"valign": "vcenter", "align": "left", "size": 8})
		fmtAlignLeftBold = wb.add_format({"valign": "vcenter", "align": "left", "size": 8, "bold": True})
		fmtAlignLeft11 = wb.add_format({"valign": "vcenter", "align": "left", "size": 11})
		fmtAlignLeft11Bold = wb.add_format({"valign": "vcenter", "align": "left", "size": 11, "bold": True})
		fmtAlignRight = wb.add_format({"valign": "vcenter", "align": "right", "size": 8})
		fmtAlignCenter = wb.add_format({"valign": "vcenter", "align": "center", "size": 8})
		fmtAlignCenter11 = wb.add_format({"valign": "vcenter", "align": "center", "size": 11})
		fmtAlignCenter11Bold = wb.add_format({"valign": "vcenter", "align": "center", "size": 11, "bold": True})
		fmtAlignCenter12Bold = wb.add_format({"valign": "vcenter", "align": "center", "size": 12, "bold": True})

		fmtTableHeader = wb.add_format(
			{"border": 1, "valign": "vcenter", "align": "center", "size": 8, "text_wrap": True})
		fmtControlHeader = wb.add_format(
			{"border": 1, "valign": "vcenter", "align": "center", "size": 8, "text_wrap": True, "color": "red"})
		fmtControlHeader11 = wb.add_format(
			{"border": 1, "valign": "vcenter", "align": "center", "size": 11, "text_wrap": True, "color": "red"})

		fmtTableBody = wb.add_format({"border": 1, "valign": "vcenter", "align": "right", "size": 8})
		fmtTableBody11 = wb.add_format({"border": 1, "valign": "vcenter", "align": "right", "size": 11})
		lFmtTableBodyBlue = {"top": 2, "bottom": 2, "left": 2, "right": 2, "valign": "vcenter", "align": "right",
		                     "size": 8, "color": "blue"}

		fmtLastCell = wb.add_format(
			{"valign": "vcenter", "align": "right", "size": 8, "bottom": 2, "left": 1, "right": 1, "top": 1})

		fmtControl = wb.add_format({"border": 1, "valign": "vcenter", "align": "right", "size": 8, "color": "red"})

		# Делает нулевыми тонкие боковые рамки ячеек
		def del_slim_line(diFrm):
			brd_attrs = ["bottom", "top"]
			for i in brd_attrs:
				if i in diFrm and diFrm[i] == 1:
					diFrm[i] = 0

		# Ищет, изменяет и возвращает формат для ячейки
		def get_format_from_list(Range, Text):
			l = list(
				zip(*map(xl_cell_to_rowcol, Range.split(':'))))  # индексы строк транспонируются в список с индесом 0
			max_row_index = max(l[0])
			frm = []
			if max_row_index < 11:
				if Range in lFmtHeader_Border:
					frm = lFmtHeader_Border[Range].copy()
				elif Range[0] + "*" in lFmtHeader_Border:
					frm = lFmtHeader_Border[Range[0] + "*"].copy()
				else:
					frm = lFmtHeader_Border['Default'].copy()

			else:
				if Range in lFmtHeader_Border:
					frm = lFmtHeader_Border[Range].copy()
				elif Range[0] + "*" in lFmtHeader_Border:
					frm = lFmtHeader_Border[Range[0] + "*"].copy()
				else:
					frm = lFmtHeader_Border['Default'].copy()

			# Жирная черта каждые 4 строки
			if (max_row_index > 7) and (max_row_index % 4 == 2):
				frm["bottom"] = 2
			# Жирные координаты
			if (max_row_index > 11) and (max_row_index % 4 == 2) and Range[0] in ["J", "K", "L", "M"]:
				frm["bold"] = True
			# Жирные точки визирования
			if (max_row_index > 11) and (max_row_index % 8 == 6) and Range[0] in ["B"]:
				frm["bold"] = True
			# Жирные средние значения
			if (max_row_index > 14) and (max_row_index % 8 in [7, 0, 1]) and Range[0] in ["E"]:
				frm["bold"] = True

			if Range in {'E8', 'F8', 'G8', 'G12:G13'}:
				frm["color"] = "red"
				frm["bold"] = True
				frm["underline"] = 1

			if (max_row_index > 6) and Range[0] in {'B', 'C', 'E', 'F', 'G', 'J', 'K', 'L', 'H', 'I',
			                                        "M"} and Text == "":
				del_slim_line(frm)

			return frm

		# Обёртка для функций записи в ячейки Excel файла
		def ws_fill(*args):
			Ws = args[0]
			if len(args) == 4:
				Range = args[1]
				Text = args[2]
				format_list = args[3]
			elif len(args) == 5:
				Range = xl_rowcol_to_cell(args[1], args[2])
				Text = args[3]
				format_list = args[4]
			elif len(args) == 7:
				Range = xl_range(args[1], args[2], args[3], args[4])
				Text = args[5]
				format_list = args[6]

			if Range.find(":") > -1:
				Ws.merge_range(Range, Text, wb.add_format(get_format_from_list(Range, Text)))
			else:
				Ws.write(Range, Text, wb.add_format(get_format_from_list(Range, Text)))

		# создание заголовков
		wsmain.merge_range("A1:M1", "Расчет направления (%s) %s" % (minename, survey_name[0]), fmtAlignCenter12Bold)
		wstask.merge_range("A1:I1", "Дано направление для проходки (%s) %s" % (minename, survey_name[0]),
		                   fmtAlignLeft11Bold)
		wsvary.merge_range("A1:I1", "Дано направление для проходки (%s) %s" % (minename, survey_name[0]),
		                   fmtAlignLeft11Bold)

		# создание шапки таблицы
		colnames = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"]
		colwidths = [6.2, 6.2, 6.5, 5, 6, 7, 7, 5.5, 8, 8, 8, 7, 6.2]  # ширина столбцов
		for colname, colwidth in zip(colnames, colwidths):
			wsmain.set_column("%s:%s" % (colname, colname), colwidth)

		# Двумерный список для хранения форматов ячеек
		lFmtHeader_Border = {
			"Default": {"bottom": 1, "top": 1, "left": 1, "right": 1, "valign": "vcenter", "align": "right", "size": 8},
			"Last": {"bottom": 2, "top": 1, "valign": "vcenter", "align": "right", "size": 8},
			"A3:B4": {"top": 2, "bottom": 1, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"A5:A6": {"bottom": 2, "right": 1, "valign": "vcenter", "align": "center", "size": 8, "text_wrap": True},
			"B5:B6": {"bottom": 2, "right": 2, "valign": "vcenter", "align": "center", "size": 8, "text_wrap": True},
			"C3:C4": {"top": 2, "bottom": 1, "left": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"C5:C6": {"bottom": 2, "left": 2, "valign": "vcenter", "align": "center", "size": 8, "text_wrap": True},
			"D3": {"top": 2, "bottom": 1, "left": 1, "right": 1, "valign": "vcenter", "align": "center", "size": 8,
			       "text_wrap": True},
			"D6": {"border": 1, "valign": "vcenter", "align": "center", "size": 8, "text_wrap": True},
			"E3:E6": {"bottom": 2, "top": 2, "left": 1, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"F3:F6": {"bottom": 2, "top": 2, "left": 2, "right": 1, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"G3:G6": {"bottom": 2, "top": 2, "left": 1, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"H3:I4": {"bottom": 1, "top": 2, "left": 2, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"H5:H6": {"bottom": 2, "top": 2, "left": 2, "right": 1, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"I5:I6": {"bottom": 2, "top": 2, "left": 1, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"J3:L4": {"bottom": 1, "top": 2, "left": 2, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"J5:J6": {"bottom": 2, "top": 1, "left": 2, "right": 1, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"K5:K6": {"bottom": 2, "top": 1, "left": 1, "right": 1, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"L5:L6": {"bottom": 2, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"M3:M6": {"bottom": 2, "top": 2, "left": 1, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True},
			"A7:M7": {"bottom": 2, "top": 2, "left": 2, "right": 2, "valign": "vcenter", "align": "center", "size": 8,
			          "text_wrap": True, "underline": 1, "color": "red", "bold": True},

			"B9": {"bottom": 0, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"B10": {"bottom": 1, "top": 0, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"C9": {"bottom": 0, "right": 1, "valign": "vcenter", "align": "right", "size": 8},
			"C10": {"top": 0, "right": 1, "valign": "vcenter", "align": "right", "size": 8},
			"E9": {"bottom": 0, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"E10": {"bottom": 1, "top": 0, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"F9": {"bottom": 0, "valign": "vcenter", "align": "right", "size": 8},
			"F10": {"top": 0, "valign": "vcenter", "align": "right", "size": 8},
			"G9": {"bottom": 0, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"G10": {"bottom": 1, "top": 0, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"B*": {"bottom": 1, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"E*": {"bottom": 1, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"F*": {"bottom": 1, "top": 1, "left": 1, "right": 1, "valign": "vcenter", "align": "right", "size": 8},
			"G*": {"bottom": 1, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"I*": {"bottom": 0, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"L*": {"bottom": 1, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8},
			"H*": {"bottom": 0, "top": 1, "left": 2, "right": 1, "valign": "vcenter", "align": "right", "size": 8},
			"J*": {"bottom": 1, "top": 1, "right": 1, "valign": "vcenter", "align": "right", "size": 8},
			"M*": {"bottom": 1, "top": 1, "left": 1, "right": 2, "valign": "vcenter", "align": "right", "size": 8}
		}

		# Шапка таблицы
		ws_fill(wsmain, "A3:B4", "Точки", lFmtHeader_Border)
		ws_fill(wsmain, "A5:A6", "стояния", lFmtHeader_Border)
		ws_fill(wsmain, "B5:B6", "набл", lFmtHeader_Border)
		ws_fill(wsmain, "C3:C4", "Изм.\nдлина", lFmtHeader_Border)
		ws_fill(wsmain, "C5:C6", "Уг.накл.", lFmtHeader_Border)
		ws_fill(wsmain, "D3", "L*sin", lFmtHeader_Border)
		wsmain.write("D4", "I", fmtTableHeader)
		wsmain.write("D5", "v", fmtTableHeader)
		ws_fill(wsmain, "D6", "ΔZ", lFmtHeader_Border)
		ws_fill(wsmain, "E3:E6", "Гориз\nпролож.", lFmtHeader_Border)
		ws_fill(wsmain, "F3:F6", "Изм.\nгориз.\nугол", lFmtHeader_Border)
		ws_fill(wsmain, "G3:G6", "Дирек.\nугол", lFmtHeader_Border)
		ws_fill(wsmain, "H3:I4", "Приращение", lFmtHeader_Border)
		ws_fill(wsmain, "H5:H6", "Y", lFmtHeader_Border)
		ws_fill(wsmain, "I5:I6", "X", lFmtHeader_Border)
		ws_fill(wsmain, "J3:L4", "Координаты", lFmtHeader_Border)
		ws_fill(wsmain, "J5:J6", "Y", lFmtHeader_Border)
		ws_fill(wsmain, "K5:K6", "X", lFmtHeader_Border)
		ws_fill(wsmain, "L5:L6", "Z", lFmtHeader_Border)
		ws_fill(wsmain, "M3:M6", "Точка", lFmtHeader_Border)

		# заголовок контроля
		ws_fill(wsmain, "A7:M7", "Контроль", lFmtHeader_Border)
		# запись данных
		currow = 7
		for irow, row in enumerate(data, start=currow):
			fmt = fmtLastCell if (irow - currow) % 4 == 3 else fmtTableBody
			for icol, value in enumerate(row):
				value = int(value) if str(value).isnumeric() else float(value) if is_number(value) else value
				if is_number(value) and math.isnan(value):
					value = ""
				if (irow == currow and icol in [4, 5, 6]) or (irow == currow + 4 and icol == 7):
					ws_fill(wsmain, irow, icol, value, lFmtHeader_Border)
				else:
					if ((icol == 0 or (icol in [9, 10, 11, 12] and (irow % 4 == 0))) and value != ""):
						ws_fill(wsmain, irow, icol, irow + 1, icol, value, lFmtHeader_Border)

					elif (irow > 11) and (icol in [5] and (irow % 4 == 2) and value != ""):
						ws_fill(wsmain, irow, icol, irow - 3, icol, value, lFmtHeader_Border)

					elif (irow > 10) and (icol in [6] and (irow % 8 == 3) and value != ""):
						ws_fill(wsmain, irow, icol, irow + 1, icol, value, lFmtHeader_Border)

					elif (irow > 10) and (icol in [6] and (irow % 8 == 6) and value != ""):
						ws_fill(wsmain, irow, icol, irow - 1, icol, value, lFmtHeader_Border)

					else:
						ws_fill(wsmain, irow, icol, value, lFmtHeader_Border)

		# запись условной точки
		currow += len(data)
		for i, val in enumerate(alamo):
			wsmain.merge_range(xl_range(currow, 0, currow, 1), "Условная точка №{}".format(i + 1),
			                   wb.add_format(lFmtTableBodyBlue))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 1
			wsmain.write(currow, 2, "", wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 1
			copyLFmt["left"] = 1
			wsmain.write(currow, 3, "", wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 2
			copyLFmt["left"] = 1
			wsmain.write(currow, 4, total_distance[i], wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 1
			copyLFmt["left"] = 2
			wsmain.write(currow, 5, "", wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 2
			copyLFmt["left"] = 1
			wsmain.write(currow, 6, "", wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 1
			copyLFmt["left"] = 2
			wsmain.write(currow, 7, "", wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 2
			copyLFmt["left"] = 1
			wsmain.write(currow, 8, "", wb.add_format(copyLFmt))
			#
			if val:
				xc, yc, zc = round(val.x, 3), round(val.y, 3), round(val.z, 3)
			else:
				xc, yc, zc = "", "", ""
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 1
			copyLFmt["left"] = 1
			copyLFmt['num_format'] = '#0.000'
			wsmain.write(currow, 9, xc, wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 1
			copyLFmt["left"] = 1
			copyLFmt['num_format'] = '#0.000'
			wsmain.write(currow, 10, yc, wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 2
			copyLFmt["left"] = 1
			copyLFmt['num_format'] = '#0.000'
			wsmain.write(currow, 11, zc, wb.add_format(copyLFmt))
			#
			copyLFmt = lFmtTableBodyBlue.copy()
			copyLFmt["right"] = 2
			copyLFmt["left"] = 2
			wsmain.write(currow, 12, "Усл.т. №{}".format(i + 1), wb.add_format(copyLFmt))
			currow += 1
		else:
			currow -= 1

		# запись домеров и  висков
		task_currow = 2
		vary_currow = 2
		for i, val in enumerate(measure):
			currow += 2
			wsmain.merge_range(xl_range(currow, 0, currow, 1), "Проект №{}".format(i + 1), fmtAlignLeft11Bold)
			currow += 1
			wsmain.write(currow, 0, "Домеры", fmtAlignLeftBold)
			wsmain.write(currow + 1, 0, "Вверх", fmtTableBody)
			wsmain.write(currow + 1, 1, uoffset[i], fmtTableBody)
			wsmain.write(currow + 2, 0, "Вниз", fmtTableBody)
			wsmain.write(currow + 2, 1, doffset[i], fmtTableBody)

			wsmain.write(currow, 4, "Виски", fmtAlignLeftBold)
			wsmain.write(currow + 1, 4, "МТ", fmtTableBody)
			wsmain.write(currow + 1, 5, svyv[i], fmtTableBody)
			wsmain.write(currow + 2, 4, "НТ", fmtTableBody)
			wsmain.write(currow + 2, 5, dirv[i], fmtTableBody)

			wsmain.write(currow, 10, "Исх. ЖВК №", fmtAlignRight)
			wsmain.write(currow + 2, 10, "Пол.журн. №", fmtAlignRight)
			wsmain.write(currow + 4, 10, "Проект №", fmtAlignRight)

			wstask.merge_range(xl_range(task_currow, 0, task_currow, 1), "Проект №{}".format(i + 1), fmtAlignLeft11Bold)
			wstask.merge_range(xl_range(task_currow + 1, 0, task_currow + 2, 0), "Виски:", fmtControlHeader11)
			wstask.write(task_currow + 1, 1, "МТ", fmtTableBody11)
			wstask.write(task_currow + 1, 2, svyv[i], fmtTableBody11)
			wstask.write(task_currow + 1, 3, "", fmtTableBody11)
			wstask.write(task_currow + 2, 1, "НТ", fmtTableBody11)
			wstask.write(task_currow + 2, 2, dirv[i], fmtTableBody11)
			wstask.write(task_currow + 2, 3, "", fmtTableBody11)
			wstask.merge_range(xl_range(task_currow + 1, 4, task_currow + 2, 4), "Домеры:", fmtControlHeader11)
			if noffsets:
				wstask.write(task_currow + 1, 5, "Влево", fmtTableBody11)
				wstask.write(task_currow + 1, 6, offsets[i]["LEFT"][0], fmtTableBody11)
				wstask.write(task_currow + 2, 5, "Вправо", fmtTableBody11)
				wstask.write(task_currow + 2, 6, offsets[i]["RIGHT"][0], fmtTableBody11)
			wstask.write(task_currow + 1, 7, "Вверх", fmtTableBody11)
			wstask.write(task_currow + 1, 8, uoffset[i], fmtTableBody11)
			wstask.write(task_currow + 2, 7, "Вниз", fmtTableBody11)
			wstask.write(task_currow + 2, 8, doffset[i], fmtTableBody11)
			#
			wsvary.merge_range(xl_range(vary_currow, 0, vary_currow, 1), "Проект №{}".format(i + 1), fmtAlignLeft11Bold)
			wsvary.merge_range(xl_range(vary_currow + 1, 0, vary_currow + 2, 0), "Виски:", fmtControlHeader11)
			wsvary.write(vary_currow + 1, 1, "МТ", fmtTableBody11)
			wsvary.write(vary_currow + 1, 2, svyv[i], fmtTableBody11)
			wsvary.write(vary_currow + 1, 3, "", fmtTableBody11)
			wsvary.write(vary_currow + 2, 1, "НТ", fmtTableBody11)
			wsvary.write(vary_currow + 2, 2, dirv[i], fmtTableBody11)
			wsvary.write(vary_currow + 2, 3, "", fmtTableBody11)
			wsvary.merge_range(xl_range(vary_currow + 1, 4, vary_currow + 2, 4), "Домеры:", fmtControlHeader11)
			wsvary.write(vary_currow + 1, 5, "Вверх", fmtTableBody11)
			wsvary.write(vary_currow + 1, 6, uoffset[i], fmtTableBody11)
			wsvary.write(vary_currow + 2, 5, "Вниз", fmtTableBody11)
			wsvary.write(vary_currow + 2, 6, doffset[i], fmtTableBody11)
			#
			currow += 4
			wsmain.write(currow, 0, "Расст.", fmtTableBody)
			wsmain.write(currow + 1, 0, "Влево", fmtTableBody)
			wsmain.write(currow + 2, 0, "Вправо", fmtTableBody)
			#
			wsvary.merge_range(xl_range(vary_currow + 3, 0, vary_currow + 3, 2), "Переменные домеры:", fmtAlignLeft11)
			wsvary.write(vary_currow + 4, 0, "Расст.", fmtTableBody11)
			wsvary.write(vary_currow + 5, 0, "Влево", fmtTableBody11)
			wsvary.write(vary_currow + 6, 0, "Вправо", fmtTableBody11)

			# экспорт первого домера
			k = 1
			if noffsets[i] > 0:
				wsmain.write(currow, k, offsets[i]["DISTANCE"][0], fmtTableBody)
				wsmain.write(currow + 1, k, offsets[i]["LEFT"][0], fmtTableBody)
				wsmain.write(currow + 2, k, offsets[i]["RIGHT"][0], fmtTableBody)
				#
				wsvary.write(vary_currow + 4, k, offsets[i]["DISTANCE"][0], fmtTableBody11)
				wsvary.write(vary_currow + 5, k, offsets[i]["LEFT"][0], fmtTableBody11)
				wsvary.write(vary_currow + 6, k, offsets[i]["RIGHT"][0], fmtTableBody11)
			else:
				wsmain.write(currow, k, "", fmtTableBody)
				wsmain.write(currow + 1, k, "", fmtTableBody)
				wsmain.write(currow + 2, k, "", fmtTableBody)
				#
				wsvary.write(vary_currow + 4, k, "", fmtTableBody11)
				wsvary.write(vary_currow + 5, k, "", fmtTableBody11)
				wsvary.write(vary_currow + 6, k, "", fmtTableBody11)

			# экспорт остальных домеров (пропускаем те, что с одинаковыми значениями)
			k = 2
			# максимальное число домеров в отчете
			max_num_offsets = 9
			for j in range(1, noffsets[i]):
				if offsets[i]["LEFT"][j] == offsets[i]["LEFT"][j - 1]:
					continue
				if k < max_num_offsets + 1:
					wsmain.write(currow, k, offsets[i]["DISTANCE"][j], fmtTableBody)
					wsmain.write(currow + 1, k, offsets[i]["LEFT"][j], fmtTableBody)
					wsmain.write(currow + 2, k, offsets[i]["RIGHT"][j], fmtTableBody)
					#
					wsvary.write(vary_currow + 4, k, offsets[i]["DISTANCE"][j], fmtTableBody11)
					wsvary.write(vary_currow + 5, k, offsets[i]["LEFT"][j], fmtTableBody11)
					wsvary.write(vary_currow + 6, k, offsets[i]["RIGHT"][j], fmtTableBody11)
				k += 1

			# заполяем пустыми ячейками оставшиеся столбцы, если имеются
			for j in range(k, max_num_offsets + 1):
				wsmain.write(currow, j, "", fmtTableBody)
				wsmain.write(currow + 1, j, "", fmtTableBody)
				wsmain.write(currow + 2, j, "", fmtTableBody)
				#
				wsvary.write(vary_currow + 4, j, "", fmtTableBody11)
				wsvary.write(vary_currow + 5, j, "", fmtTableBody11)
				wsvary.write(vary_currow + 6, j, "", fmtTableBody11)
			#
			currow += 4

			wsmain.merge_range(xl_range(currow, 0, currow, 2), "Проходить от", fmtAlignLeftBold)
			wsmain.write(currow, 3, measure[i].standpoint.name, fmtAlignRight)
			wsmain.write(currow, 5, total_distance[i], fmtAlignRight)
			wsmain.write(currow, 6, "м.", fmtAlignLeft)
			wsmain.merge_range(xl_range(currow + 1, 0, currow + 1, 2), "Угол наклона:", fmtAlignLeftBold)

			if getattr(inclination[i], 'to_text', None):
				inclination_to_text = inclination[i].to_text()
			else:
				inclination_to_text = inclination[i]

			wsmain.write(currow + 1, 3, inclination_to_text, fmtAlignRight)

			# футер
			#
			_total_distance = "" if not total_distance[i] else "%.1f" % float(total_distance[i])
			wstask.merge_range(xl_range(task_currow + 3, 0, task_currow + 3, 8),
			                   "От маркшейдерской точки МТ %s пройти интервал %s" % (
			                   measure[i].standpoint.name, _total_distance), fmtAlignLeft11)
			#
			wsvary.merge_range(xl_range(vary_currow + 7, 0, vary_currow + 7, 8),
			                   "От маркшейдерской точки МТ %s пройти интервал %s" % (
			                   measure[i].standpoint.name, _total_distance), fmtAlignLeft11)
			wsvary.merge_range(xl_range(vary_currow + 8, 0, vary_currow + 8, 8),
			                   "после чего забой остановить для переноса направления", fmtAlignLeft11)
			task_currow += 6
			vary_currow += 11
			currow += 2
		else:
			task_currow -= 6
			vary_currow -= 11
			currow -= 2
		#
		task_currow += 5
		vary_currow += 10
		currow += 3
		wsmain.merge_range(xl_range(currow, 0, currow, 2), "Участковый маркшейдер:", fmtAlignLeft)
		wsmain.merge_range(xl_range(currow, 3, currow, 12), surveyor, fmtAlignLeft)
		#
		wstask.merge_range(xl_range(task_currow, 0, task_currow, 3), "Участковый маркшейдер:", fmtAlignLeft11)
		wstask.merge_range(xl_range(task_currow, 4, task_currow, 8), surveyor, fmtAlignLeft11)
		#
		wsvary.merge_range(xl_range(vary_currow, 0, vary_currow, 3), "Участковый маркшейдер:", fmtAlignLeft11)
		wsvary.merge_range(xl_range(vary_currow, 4, vary_currow, 8), surveyor, fmtAlignLeft11)

		currow += 1
		wsmain.merge_range(xl_range(currow, 0, currow, 2), "Дата:", fmtAlignLeft)
		wsmain.merge_range(xl_range(currow, 3, currow, 6), date, fmtAlignLeft)
		wsmain.merge_range(xl_range(currow + 1, 0, currow + 1, 2), "Проверено 'во вторую руку':", fmtAlignLeft)

		for irow in range(currow + 2):
			wsmain.set_row(irow, 12)

		wb.close()

		# запуск отчета
		os.startfile(path)

	def prepare_remove(self):
		self._skipfunctions = {"SAVE": True, "LOAD": True, "EXPORT": True, "REMOVE": False}
		self.prepare()
	def remove(self):
		if qtmsgbox.show_question(self, "Удаление", "Вы действительно хотите удалить направление?"):
			self._traversal_measure.remove()
			self.main_app.srvcnxn.commit()

	def prepare(self, project_id=False):
		if not super(SvyDirections, self).prepare():
			return
		try:
			self.frameInfo.getParams()
			for index in range(len(self.frameProject.measure)):
				if project_id is not False and index != project_id:
					continue
				self.frameProject.getParams(index)

			if canAccessToMultipleRegisterGroup(self.main_app.srvcnxn):
				qtmsgbox.show_error(self, "Ошибка", "Вы имеете доступ к более чем двум группам доступа журналов. Это не корректная ситуация для сохранения направлений.")
				return


			if not self._skipfunctions["SAVE"] and not self.isOkToSave():
				return
			if not self._skipfunctions["LOAD"] and not self.isOkToLoad(project_id):
				if not self.frameCalculation.gridWidget.grids[-1].measure.dirpoint.is_direction:
					qtmsgbox.show_error(self, "Ошибка", "Нет объектов для загрузки, так как не было рассчитано направление")
					return
				return
			if not self._skipfunctions["EXPORT"] and not self.isOkToExport():
				return


		# self.run()
		
			self._is_running = True
			self.run(project_id)
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", str(e))

		self.after_run()

	def run(self, project_id=False):
		super(SvyDirections, self).run()
		if not self._skipfunctions["SAVE"]:
			try:				self.save()
			except Exception as e:
				re_result = re.search("@.*@",str(e)) 
				if 	not re_result is None and  re_result.group(0)  == '@REGISTER_ACCESS_DENIED@':
					raise Exception ("Доступ к данному журналу запрещен.")
				elif 	not re_result is None and  re_result.group(0)  == '@ALREADY_EXISTS@':
					raise Exception ("В этом журнале уже существует не удаленная точка с таким именем.")
				elif 	not re_result is None and  re_result.group(0)  == '@NOT_ONLY_POINT_CLASS_IN_GROUP@':
					raise Exception ("Ошибка", "В группе должна быть актуальной только одна точка в одном классе.")
				else:
					raise Exception("Не удалось сохранить измерение. Ошибка: %s" % str(e))
		if not self._skipfunctions["LOAD"]:
			# self.load()
			try:					self.load(project_id)
			except Exception as e:	raise Exception("Не удалось загрузить проект. Ошибка: %s" % str(e))
		if not self._skipfunctions["EXPORT"]:
			# self.export()
			try:					self.export()
			except Exception as e:	raise Exception("Не удалось экспортировать измерения. Ошибка: %s" % str(e))
		if not self._skipfunctions["REMOVE"]:
			# self.export()
			try:					self.remove()
			except Exception as e:	
				re_result = re.search("@.*@",str(e)) 
				if 	not re_result is None and  re_result.group(0)  == '@REGISTER_ACCESS_DENIED@':
					raise Exception ("Доступ к данному журналу запрещен.")
				elif 	not re_result is None and  re_result.group(0)  == '@POINT_IN_OTHER_MEASURE@':
					raise Exception ("Точки сформированные данным рассчётом участвуют в рассчёте другого направления.")
				else:
					raise Exception("Не удалось удалить раччсёт направления. Ошибка: %s" % str(e))
			

	def after_run(self):
		super(SvyDirections, self).after_run()
		if not self._skipfunctions["REMOVE"]:
			self.close()
		self.restoreSkipFunctions()

class SvyAddDirection(SvyDirections):
	def __init__(self, *args, **kw):
		super(SvyAddDirection, self).__init__(*args, **kw)
		self.setWindowTitle("Рассчитать направление")
	def isOkToSave(self):
		if not super(SvyAddDirection, self).isOkToSave():
			return False

		# проверка новых точек на существование в базе данных
		register_name = self.frameInfo.lineEditRegister.text()
		gw = self.frameCalculation.gridWidget
		for i, grid in enumerate(gw.grids):
			measure = grid.measure
			if measure.dirpoint.get_name_exists(register_name) and not measure.dirpoint.is_removed and not measure.dirpoint.isNeedOvercoordinate:
				qtmsgbox.show_error(self, "Ошибка", "Точка '%s' уже существует" % measure.dirpoint.name)
				self.tabWidget.setCurrentIndex(2)
				gw.setCurrentIndex(i)
				self.frameCalculation.setActions()
				return

		return True

class SvyEditDirection(SvyDirections):
	def __init__(self, *args, **kw):
		super(SvyEditDirection, self).__init__(*args, **kw)
		self.setWindowTitle("Редактировать направление")

	def exec(self):
		timer = QtCore.QTimer()
		self.connectDb()

		self._isEditingMode = True
		self._initGui()
		self.buttonBox.button(QtGui.QDialogButtonBox.Abort).setEnabled(True)


		sel_measure = self.selectMeasure()
		if not sel_measure:
			self.close()
			return
		measure_id = sel_measure['cmid'] 
		measure_name = sel_measure['cmname'] 

		self.traversal_measure = DbTraversalMeasure(self.main_app.srvcnxn, measure_id = measure_id, measure_name = measure_name)
		super(SvyDirections, self).exec()
	def selectMeasure(self):
		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите съемку")
		model = CustomDataModel(dialog.view, *self.data["MEASURES"])
		dialog.view.setModel(model)
		dialog.view.hideColumn(0)
		# dialog.view.hideColumn(2)
		dialog.view.hideColumn(3)
		dialog.view.setColumnWidth(4, 270)
		dialog.resize(QtCore.QSize(900, 500))
		dialog.exec()
		#
		measure_id = dialog.getResult()
		if not measure_id:
			return

		return {'cmid':measure_id[0][0], 'cmname':measure_id[0][1]}




