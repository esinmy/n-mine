from PyQt4 import QtGui, QtCore
import os
from utils.constants import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyCreateUnit(DialogWindow):
	def __init__(self, *args, **kw):
		super(SvyCreateUnit, self).__init__(*args, **kw)

		self.setWindowTitle("Создать юнит")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createVariables(self):
		self._layers = {}
		self._units = []
		self.prefix = self.main_app.settings.getValue(ID_SVY_UPREFIX)
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")

		self.inputLayout = QtGui.QFormLayout()

		self.labelLayer = QtGui.QLabel(self.groupBoxInput)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setMinimumWidth(100)
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxLayer.setMinimumWidth(200)

		self.labelUnit = QtGui.QLabel(self.groupBoxInput)
		self.labelUnit.setMinimumWidth(100)
		self.labelUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelUnit.setText(self.main_app.settings.getValue(ID_SVY_UDSPNAME))
		self.lineEditUnit = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditUnit.setText(self.prefix)
		

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelUnit)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditUnit)

	def buildCombos(self):
		self.buildLayerList()

	def buildLayerList(self):
		# считываем залежи из БД
		self.comboBoxLayer.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		self._layers = {}
		#
		cmd = """
			SELECT LAYER_ID, LAYER_DESCRIPTION 
			FROM [{0}].[dbo].[LAYER]
			WHERE LOCATION = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		for layerId, layerName in data:
			self._layers[layerName] = layerId
		self.comboBoxLayer.addItems(sorted(list(self._layers.keys())))

	def createFolders(self):
		networkpath = self.main_app.settings.getValue(ID_NETPATH)
		svydir = self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerName = self.comboBoxLayer.currentText()
		unitDisplayName = self.main_app.settings.getValue(ID_SVY_UDSPNAME)
		unitName = self.lineEditUnit.text()

		# создание залежи, если отсутствует
		currentFolder = os.path.join(networkpath, svydir, layerName)
		if not os.path.exists(currentFolder):
			os.mkdir(currentFolder)
		
		# создание юнита, если отсутствует
		currentFolder = os.path.join(currentFolder, unitName)
		if os.path.exists(currentFolder):
			qtmsgbox.show_error(self, "Ошибка", "%s уже существует" % unitDisplayName)
			return

		try:
			os.mkdir(currentFolder)
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать директорию '%s'. %s" % (unitName, str(e)))
			return

		# создание вложенных директорий
		for folderPurpose, folderName in self.main_app.settings.getValue(ID_SVY_USUBDIRS):
			try:
				os.mkdir(currentFolder + "\\" + folderName)
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно создать директорию '%s'. %s" % (folderName, str(e)))
				return

		return True

	def prepare(self):
		if not super(SvyCreateUnit, self).prepare():
			return

		unitName = self.lineEditUnit.text()
		prefix = self.main_app.settings.getValue(ID_SVY_UPREFIX)
		if not unitName.startswith(prefix):
			qtmsgbox.show_error(self, "Ошибка", "Неверное значение. Ожидался префикс '%s'" % prefix)
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать маркшейдерский юнит")

		self.after_run()

	def run(self):
		if not super(SvyCreateUnit, self).run():
			return

		if not self.createFolders():
			return

		qtmsgbox.show_information(self, "Выполнено", "Юнит успешно создан")
		self.close()
