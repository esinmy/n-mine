import os, math
from PyQt4 import QtGui, QtCore
from datetime import datetime
import re

from directions.svydbapi import SvyPoint, DbKeyInMeasure

import gui.qt.dialogs.messagebox as qtmsgbox

from gui.qt.ProcessWindow import DialogWindow
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import CustomDataModel

from utils.validation import DoubleValidator, is_blank
from utils.osutil import get_temporary_prefix
from utils.constants import *


from mm.formsets import loadSurveyPoints
from mm.mmutils import MicromineFile
from utils.logger import  NLogger

from utils.osutil import get_current_time
from datetime import datetime

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class SvyCreatePoint(DialogWindow):
	def __init__(self, *args, **kw):
		super(SvyCreatePoint, self).__init__(*args, **kw)

		self.setWindowTitle("Создать маркшейдерскую точку")

		self.__createVariables()
		self.__createWigdets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildComboTypes()

		self.setPoint(SvyPoint(self.main_app.srvcnxn))

	def __createVariables(self):
		self._measure = DbKeyInMeasure(self.main_app.srvcnxn)
	def __createWigdets(self):
		self.groupBoxPoint = QtGui.QGroupBox(self)
		self.groupBoxPoint.setTitle("Точка")
		self.layoutPoint = QtGui.QFormLayout()

		self.labelPointName = QtGui.QLabel(self.groupBoxPoint)
		self.labelPointName.setText("Имя")
		self.labelPointName.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelPointName.setMinimumWidth(80)
		self.lineEditPointName = QtGui.QLineEdit(self.groupBoxPoint)
		self.labelPointType = QtGui.QLabel(self.groupBoxPoint)
		self.labelPointType.setText("Тип")
		self.labelPointType.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelPointType.setMinimumWidth(80)
		self.comboBoxPointType = QtGui.QComboBox(self.groupBoxPoint)
		self.labelPointDescription = QtGui.QLabel(self.groupBoxPoint)
		self.labelPointDescription.setText("Описание")
		self.labelPointDescription.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelPointDescription.setMinimumWidth(80)
		self.lineEditPointDescription = QtGui.QLineEdit(self.groupBoxPoint)
		self.checkBoxRemoved = QtGui.QCheckBox(self.groupBoxPoint)
		self.checkBoxRemoved.setText("Пометить удаленной")
		self.checkBoxRemoved.setVisible(False)

		self.groupBoxInformation = QtGui.QGroupBox(self)
		self.groupBoxInformation.setTitle("Общая информация")
		self.layoutInformation = QtGui.QFormLayout()
		self.labelRegister = QtGui.QLabel(self.groupBoxInformation)
		self.labelRegister.setText("Журнал")
		self.labelRegister.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelRegister.setMinimumWidth(80)
		self.lineEditRegister = QtGui.QLineEdit(self.groupBoxInformation)
		self.lineEditRegister.setReadOnly(True)

		self.labelSurveyor = QtGui.QLabel(self.groupBoxInformation)
		self.labelSurveyor.setText("Маркшейдер")
		self.labelSurveyor.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelSurveyor.setMinimumWidth(80)
		self.lineEditSurveyor = QtGui.QLineEdit(self.groupBoxInformation)

		self.labelMineName = QtGui.QLabel(self.groupBoxInformation)
		self.labelMineName.setText("Выработка")
		self.labelMineName.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelMineName.setMinimumWidth(80)
		self.lineEditMineName = QtGui.QLineEdit(self.groupBoxInformation)
		self.lineEditMineName.setReadOnly(True)

		self.labelLayer = QtGui.QLabel(self.groupBoxInformation)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelLayer.setMinimumWidth(80)
		self.lineEditLayer = QtGui.QLineEdit(self.groupBoxInformation)
		self.lineEditLayer.setEnabled(False)

		self.labelUnit = QtGui.QLabel(self.groupBoxInformation)
		geoUnitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME)
		self.labelUnit.setText(geoUnitDisplayName)
		self.labelUnit.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelUnit.setMinimumWidth(80)
		self.lineEditUnit = QtGui.QLineEdit(self.groupBoxInformation)
		self.lineEditUnit.setEnabled(False)

		self.labelEquipment = QtGui.QLabel(self.groupBoxInformation)
		self.labelEquipment.setText("Оборудование")
		self.labelEquipment.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelEquipment.setMinimumWidth(80)
		self.lineEditEquipment = QtGui.QLineEdit(self.groupBoxInformation)

		self.labelClass = QtGui.QLabel(self.groupBoxInformation)
		self.labelClass.setText("Классификация")
		self.labelClass.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelClass.setMinimumWidth(80)
		self.lineEditClass = QtGui.QLineEdit(self.groupBoxInformation)
		self.lineEditClass.setReadOnly(True)

		self.label_date = QtGui.QLabel(self.groupBoxInformation)
		self.label_date.setText("Дата создания")
		self.label_date.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.label_date.setMinimumWidth(80)
		self.label_date.setMaximumWidth(80)
		self.lineEdit_date = QtGui.QDateEdit(self.groupBoxInformation)
		self.lineEdit_date.setCalendarPopup(True)
		self.lineEdit_date.calendarWidget().setFirstDayOfWeek(QtCore.Qt.Monday)
		self.lineEdit_date.setDisplayFormat("yyyy-MM-dd")
		currentTime = get_current_time()
		self.lineEdit_date.setDate(currentTime)

		self.groupBoxCoordinates = QtGui.QGroupBox(self)
		self.groupBoxCoordinates.setTitle("Координаты")
		self.layoutCoordinates = QtGui.QGridLayout()
		self.labelX = QtGui.QLabel(self.groupBoxCoordinates)
		self.labelX.setText("Y (Восток)")
		self.labelX.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelX.setMinimumWidth(50)
		self.lineEditX = QtGui.QLineEdit(self.groupBoxCoordinates)
		self.lineEditX.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.lineEditX.setMaximumWidth(90)
		self.lineEditX.setValidator(DoubleValidator())
		self.labelY = QtGui.QLabel(self.groupBoxCoordinates)
		self.labelY.setText("X (Север)")
		self.labelY.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelY.setMinimumWidth(50)
		self.lineEditY = QtGui.QLineEdit(self.groupBoxCoordinates)
		self.lineEditY.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.lineEditY.setMaximumWidth(90)
		self.lineEditY.setValidator(DoubleValidator())
		self.labelZ = QtGui.QLabel(self.groupBoxCoordinates)
		self.labelZ.setText("Z (Отметка)")
		self.labelZ.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelZ.setMinimumWidth(50)
		self.lineEditZ = QtGui.QLineEdit(self.groupBoxCoordinates)
		self.lineEditZ.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.lineEditZ.setMaximumWidth(90)
		self.lineEditZ.setValidator(DoubleValidator())

		self.groupBoxParams = QtGui.QGroupBox(self)
		self.layoutParams = QtGui.QVBoxLayout()
		self.checkBoxAutoLoad = QtGui.QCheckBox(self.groupBoxParams)
		self.checkBoxAutoLoad.setText("Автозагрузка")

	def __createBindings(self):
		self.lineEditRegister.mouseDoubleClickEvent = lambda e: self.selectFromLookup(self.lineEditRegister)
		self.lineEditSurveyor.mouseDoubleClickEvent = lambda e: self.selectFromLookup(self.lineEditSurveyor)
		self.lineEditMineName.mouseDoubleClickEvent = lambda e: self.selectFromLookup(self.lineEditMineName)
		self.lineEditEquipment.mouseDoubleClickEvent = lambda e: self.selectFromLookup(self.lineEditEquipment)
		self.lineEditClass.mouseDoubleClickEvent = lambda e: self.selectFromLookup(self.lineEditClass)

		# self.checkBoxRemoved.stateChanged.connect(self.getParams)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.check)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxPoint)
		self.groupBoxPoint.setLayout(self.layoutPoint)
		
		self.layoutPoint.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelPointName)
		self.layoutPoint.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditPointName)
		self.layoutPoint.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelPointType)
		self.layoutPoint.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxPointType)
		self.layoutPoint.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelPointDescription)
		self.layoutPoint.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditPointDescription)
		self.layoutPoint.setWidget(3, QtGui.QFormLayout.FieldRole, self.checkBoxRemoved)

		self.layoutControls.addWidget(self.groupBoxInformation)
		self.groupBoxInformation.setLayout(self.layoutInformation)
		self.layoutInformation.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelRegister)
		self.layoutInformation.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditRegister)
		self.layoutInformation.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSurveyor)
		self.layoutInformation.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditSurveyor)
		self.layoutInformation.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelMineName)
		self.layoutInformation.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditMineName)
		self.layoutInformation.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.layoutInformation.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditLayer)
		self.layoutInformation.setWidget(4, QtGui.QFormLayout.LabelRole, self.labelUnit)
		self.layoutInformation.setWidget(4, QtGui.QFormLayout.FieldRole, self.lineEditUnit)
		self.layoutInformation.setWidget(5, QtGui.QFormLayout.LabelRole, self.labelEquipment)
		self.layoutInformation.setWidget(5, QtGui.QFormLayout.FieldRole, self.lineEditEquipment)
		self.layoutInformation.setWidget(6, QtGui.QFormLayout.LabelRole, self.labelClass)
		self.layoutInformation.setWidget(6, QtGui.QFormLayout.FieldRole, self.lineEditClass)
		self.layoutInformation.setWidget(7, QtGui.QFormLayout.LabelRole, self.label_date)
		self.layoutInformation.setWidget(7, QtGui.QFormLayout.FieldRole, self.lineEdit_date)

		self.layoutControls.addWidget(self.groupBoxCoordinates)
		self.groupBoxCoordinates.setLayout(self.layoutCoordinates)
		self.layoutCoordinates.addWidget(self.labelX, 0, 0, 1, 1, QtCore.Qt.AlignCenter)
		self.layoutCoordinates.addWidget(self.lineEditX, 1, 0, 1, 1)
		self.layoutCoordinates.addWidget(self.labelY, 0, 1, 1, 1, QtCore.Qt.AlignCenter)
		self.layoutCoordinates.addWidget(self.lineEditY, 1, 1, 1, 1)
		self.layoutCoordinates.addWidget(self.labelZ, 0, 2, 1, 1, QtCore.Qt.AlignCenter)
		self.layoutCoordinates.addWidget(self.lineEditZ, 1, 2, 1, 1)

		self.layoutControls.addWidget(self.groupBoxParams)
		self.groupBoxParams.setLayout(self.layoutParams)
		self.groupBoxParams.setTitle("Параметры")
		self.layoutParams.addWidget(self.checkBoxAutoLoad)


	def getActualRegister(self):


		cmd = """
			SELECT TOP 1 NAME
			FROM [{0}].[dbo].[MP_REGISTER] 
			WHERE exists (
				SELECT * FROM fn_my_permissions ('[{0}].[dbo].'+[GROUP], 'TYPE')
			)
			ORDER BY [DATE] DESC
		""".format(self.main_app.srvcnxn.dbsvy)
		values = self.main_app.srvcnxn.exec_query(cmd).get_column("NAME")
		
		return values[0] if values else ""

	def getCurrentClassName(self):


		cmd = """
			SELECT TOP 1 NAME
			FROM [{0}].[dbo].[MP_CLASS] 
			WHERE CODE = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (ID_CLASS_CODE_CURRENT)
		values = self.main_app.srvcnxn.exec_query(cmd, params).get_column("NAME")
		
		return values[0] if values else ""

	def buildComboTypes(self):
		cmd = """SELECT CASE WHEN PCODEDESC IS NULL OR PCODEDESC = '' THEN PCODENAME ELSE PCODEDESC END CODE_DESCRIPTION
					FROM [%s].[dbo].[MP_LKP_POINTCODES] WHERE POINTCODE_ID > 3""" % self.main_app.srvcnxn.dbsvy
		values = self.main_app.srvcnxn.exec_query(cmd).get_column("CODE_DESCRIPTION")
		self.comboBoxPointType.clear()
		for value in values:
			self.comboBoxPointType.addItem(value.upper())

	def selectFromLookup(self, wgt):
		data = {}

		cmd = """
			SELECT  NAME, [DATE]
			FROM [{0}].[dbo].[MP_REGISTER] 
			WHERE exists (
				SELECT * FROM fn_my_permissions ('[{0}].[dbo].'+[GROUP], 'TYPE')
			)
			ORDER BY [DATE] DESC
		""".format(self.main_app.srvcnxn.dbsvy)
		data[self.lineEditRegister] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["ЖУРНАЛ", "ДАТА"]]

		cmd = """
			SELECT SURNAME + ' ' + FIRSTNAME + ' ' + SUVMIDDLE 
			FROM [{0}].[dbo].[SURVEY_FIO]
		""".format(self.main_app.srvcnxn.dbsvy)
		data[self.lineEditSurveyor] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["МАРКШЕЙДЕР"]]
		#
		params = []
		cmd = """
			SELECT L.LAYER_DESCRIPTION, PN.PANEL_DESCRIPTION, V.VYR 
			FROM [{0}].[dbo].[LAYER] AS L
			LEFT JOIN [{0}].[dbo].[PANEL] AS PN
			ON PN.LAYER_ID = L.LAYER_ID
				LEFT JOIN [{0}].[dbo].[MINE_PANELS] AS M_PN
				ON M_PN.PANEL_ID =  PN.PANEL_ID
					RIGHT JOIN 	[{0}].[dbo].[VYRAB_P] V
					ON V.VyrId =  M_PN.MINE_ID
						INNER JOIN [{0}].[dbo].[LOCAL_PROJECT_MINES] AS P_M
						ON P_M.MINE_ID = V.VyrId

			WHERE P_M.LOC_PROJECT_ID IN (
				SELECT PD.LOC_PROJECT_ID 
				FROM [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] PD
				WHERE PD.DepartID IN (
					SELECT D.DepartID 
					FROM [{0}].[dbo].[DEPARTS] D
					WHERE D.DepartPitID IN (
						SELECT P.PitID
						FROM [{0}].[dbo].[PITS] P""".format(self.main_app.srvcnxn.dbsvy)
		if self.main_app.srvcnxn.location is not None:
			cmd += """ WHERE P.LOCATION = ?"""
			params.append(self.main_app.srvcnxn.location)
		cmd += "))) ORDER BY UpdateDate DESC"
		geoUnitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper()
		data[self.lineEditMineName] = [self.main_app.srvcnxn.exec_query(cmd, params).get_rows(), ["ЗАЛЕЖЬ", geoUnitDisplayName, "ВЫРАБОТКА"]]
		#
		cmd = """
			SELECT INSTNAME, INSTMODEL 
			FROM [{0}].[dbo].[MP_LKP_INSTRUMENTS]
		""".format(self.main_app.srvcnxn.dbsvy)
		data[self.lineEditEquipment] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["ОБОРУДОВАНИЕ", "МОДЕЛЬ"]]
		#

		cmd = """
			SELECT NAME 
			FROM [{0}].[dbo].[MP_CLASS]
		""".format(self.main_app.srvcnxn.dbsvy)
		data[self.lineEditClass] = [self.main_app.srvcnxn.exec_query(cmd).get_rows(), ["КЛАСС ТОЧКИ"]]
		#

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите значение")
		model = CustomDataModel(dialog.view, *data[wgt])
		dialog.view.setModel(model, combobox_filter=[i for i in range(len(data[wgt][1])-1)])
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.resize(500, 480)
		dialog.exec()

		result = dialog.getResult()
		if not result:
			return

		if wgt == self.lineEditMineName:
			wgt.setText(result[0][2])
			self.lineEditLayer.setText(result[0][0])
			self.lineEditUnit.setText(result[0][1])

		else:
			wgt.setText(result[0][0])

	def setPoint(self, pt):
		self._measure.point = pt
		self.setParams()

	def getParams(self):
		if not self._measure.point:
			return
		self._measure.point.name = self.lineEditPointName.text()
		self._measure.point.description = self.lineEditPointDescription.text()
		self._measure.point.x = "%f" % float(self.lineEditX.text()) if self.lineEditX.text() else math.nan
		self._measure.point.y = "%f" % float(self.lineEditY.text()) if self.lineEditY.text() else math.nan
		self._measure.point.z = "%f" % float(self.lineEditZ.text()) if self.lineEditZ.text() else math.nan
		#
		cmd = "SELECT POINTCODE_ID FROM [%s].[dbo].[MP_LKP_POINTCODES] WHERE PCODEDESC = ?" % (self.main_app.srvcnxn.dbsvy)
		params = (self.comboBoxPointType.currentText())
		code = int(self.main_app.srvcnxn.exec_query(cmd, params).get_row(0))
		self._measure.point.description = self.lineEditPointDescription.text()
		self._measure.point.code = code
		self._measure.point.is_removed = self.checkBoxRemoved.isChecked()
		cmd = "SELECT ID FROM [%s].[dbo].[MP_REGISTER] WHERE NAME = ?" % (self.main_app.srvcnxn.dbsvy)
		params = (self.lineEditRegister.text())
		#self._measure.point.register_id = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		self._measure.register_id = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		#
		cmd = "SELECT FIOID FROM [%s].[dbo].[SURVEY_FIO] WHERE SURNAME + ' ' + FIRSTNAME + ' ' + SUVMIDDLE = ?" % (self.main_app.srvcnxn.dbsvy)
		params = (self.lineEditSurveyor.text())
		self._measure.surveyor_id = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)

		params=[]
		cmd = """SELECT V.VyrId, PN.PANEL_ID 
			FROM [{0}].[dbo].[VYRAB_P]  AS V
			LEFT JOIN [{0}].[dbo].[MINE_PANELS] AS M_PN
			ON   M_PN.MINE_ID = V.VyrId 
				LEFT JOIN [{0}].[dbo].[PANEL] AS PN
				ON PN.PANEL_ID = M_PN.PANEL_ID 
					LEFT JOIN [{0}].[dbo].[LAYER] AS L
					ON L.LAYER_ID = PN.LAYER_ID 
			WHERE Vyr = ? 
			

			""".format(self.main_app.srvcnxn.dbsvy)
		params.append(self.lineEditMineName.text())

		if not is_blank(self.lineEditUnit.text()):
			cmd = cmd + """
			AND  PN.PANEL_DESCRIPTION = ?

			"""
			params.append(self.lineEditUnit.text())

		if not is_blank(self.lineEditUnit.text()):
			cmd = cmd + """
			AND L.LAYER_DESCRIPTION = ?

			"""
			params.append(self.lineEditLayer.text())

		row = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		if not row:
			raise Exception ("Не найдена выработка с аттрибутами: {0}".format(",".join(params)))
			 
		self._measure.mine_id = row[0]
		self._measure.unit_id = row[1]
		cmd = "SELECT INSTRUMENT_ID FROM [%s].[dbo].[MP_LKP_INSTRUMENTS] WHERE INSTNAME = ?" % (self.main_app.srvcnxn.dbsvy)
		params = (self.lineEditEquipment.text())
		self._measure.equipment_id = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		cmd = "SELECT ID FROM [%s].[dbo].[MP_CLASS] WHERE NAME = ?" % (self.main_app.srvcnxn.dbsvy)
		params = (self.lineEditClass.text())
		self._measure.point.class_id = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		self._measure.timestamp = datetime.strptime(self.lineEdit_date.text(), "%Y-%m-%d")
	def setParams(self):
		if self._measure.point.exists:
			self.lineEditPointName.setText(self._measure.point.name)
			if self._measure.point.code == 5:
				self.comboBoxPointType.setCurrentIndex(1)
			self.lineEditPointDescription.setText(self._measure.point.description)
			self.lineEditX.setText("%f" % self._measure.point.x)
			self.lineEditY.setText("%f" % self._measure.point.y)
			self.lineEditZ.setText("%f" % self._measure.point.z)
			self.checkBoxRemoved.setChecked(self._measure.point.is_removed)
			self.lineEditRegister.setText(self._measure.register_name)
			
			#
			self.lineEditSurveyor.setText(self._measure.surveyor_name if self._measure.surveyor_name is not None else "")
			self.lineEditMineName.setText(self._measure.mine_name if self._measure.mine_name is not None else "")
			self.lineEditEquipment.setText(self._measure.equipment_name if self._measure.equipment_name is not None else "")
			self.lineEditClass.setText(self._measure.point.class_name if self._measure.point.class_name is not None else "")
			#

			self.lineEditUnit.setText(self._measure.unit_name  if self._measure.unit_name  is not None else "")
			cmd = """
				SELECT LAYER_DESCRIPTION 
				FROM [{0}].[dbo].[LAYER] AS L
				INNER JOIN [{0}].[dbo].[PANEL] AS PN
				ON PN.LAYER_ID = L.LAYER_ID
				WHERE PANEL_ID = ?
			""".format (self.main_app.srvcnxn.dbsvy)
			params = (self._measure.unit_id)
			self.lineEditLayer.setText(self.main_app.srvcnxn.exec_query(cmd, params).get_row(0))
			self.lineEdit_date.setDate(self._measure.timestamp)

		else:
			self.lineEditPointName.setText("")
			self.lineEditPointDescription.setText("")
			self.lineEditX.setText("")
			self.lineEditY.setText("")
			self.lineEditZ.setText("")
			self.checkBoxRemoved.setChecked(False)
			self.lineEditRegister.setText("")
			#self.lineEditRegister.setText(self.getActualRegister())

			#
			self.lineEditSurveyor.setText("")
			self.lineEditMineName.setText("")
			self.lineEditEquipment.setText("")
			self.lineEditClass.setText(self.getCurrentClassName())
			self.lineEditUnit.setText("")
			self.lineEditLayer.setText("")

	def fieldsHaveData(self):
		labels = [self.labelPointName, self.labelRegister, self.labelSurveyor, self.labelMineName, self.labelEquipment, self.labelClass, self.labelX, self.labelY, self.labelZ, self.label_date]
		fields = [self.lineEditPointName, self.lineEditRegister, self.lineEditSurveyor, self.lineEditMineName, self.lineEditEquipment, self.lineEditClass, self.lineEditX, self.lineEditY, self.lineEditZ, self.lineEdit_date]

		for label, field in zip(labels, fields):
			value = field.text()
			if is_blank(value):
				qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
				field.setFocus()
				return False
		return True

	def check(self):
		if not super(SvyCreatePoint, self).check():
			return

		#
		if not self.fieldsHaveData():
			return

		self.getParams()

		if self._measure.point.get_name_exists(self.lineEditRegister.text()):
			qtmsgbox.show_error(self, "Ошибка", "Точка '%s' существует" % self._measure.point.name)
			self.lineEditPointName.setFocus()
			return

		message = "Значение '%s' не найдено"
		fields = [self.lineEditSurveyor, self.lineEditMineName, self.lineEditEquipment, self.lineEditRegister, self.lineEditClass, self.lineEdit_date]
		variables = [self._measure.surveyor_id, self._measure.mine_id, self._measure.equipment_id, self._measure.register_id, self._measure.point.class_id, self._measure.timestamp]
		for variable, field in zip(variables, fields):
			value = field.text()
			if variable is None:
				qtmsgbox.show_error(self, "Ошибка", message % value)
				field.setFocus()
				return

		self.prepare()
	def prepare(self):
		if not super(SvyCreatePoint, self).prepare():
			return
		
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			re_result = re.search("@.*@",str(e)) 
			if 	not re_result is None and  re_result.group(0)  == '@REGISTER_ACCESS_DENIED@':
					qtmsgbox.show_error(self, "Ошибка", "Доступ к данному журналу запрещен.")
			elif 	not re_result is None and  re_result.group(0)  == '@ALREADY_EXISTS@':
				qtmsgbox.show_error(self, "Ошибка", "В этом журнале уже существует не удаленная точка с таким именем.")
			elif 	not re_result is None and  re_result.group(0)  == '@NOT_ONLY_POINT_CLASS_IN_GROUP@':
				qtmsgbox.show_error(self, "Ошибка", "В группе должна быть актуальной только одна точка в одном классе.")
			else:
				qtmsgbox.show_error(self, "Ошибка", str(e))

		self.after_run()
	def run(self):
		if not super(SvyCreatePoint, self).run():
			return

		if not self._measure.exists:
			self._measure.name = self._measure.point.name
			self._measure.save()
		else:
			self._measure.save()

		self.main_app.srvcnxn.commit()
		
		qtmsgbox.show_information(self, "Выполнено", "Маркшейдерская точка успешно сохранена")

		if self.checkBoxAutoLoad.isChecked():
			ptspath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % self._measure.point.name)
			ptsheader = ["EAST", "NORTH", "RL", "ИМЯ"]
			ptsdata = [[self._measure.point.x, self._measure.point.y, self._measure.point.z, self._measure.point.name]]
			MicromineFile.write(ptspath, ptsdata, ptsheader)
			#
			loadSurveyPoints(ptspath, self._measure.point.name)

class SvyEditPoint(SvyCreatePoint):
	def __init__(self, *args, **kw):
		super(SvyEditPoint, self).__init__(*args, **kw)
		self.setWindowTitle("Редактировать маркшейдерскую точку")
		self.checkBoxRemoved.setVisible(True)

		self.buttonSaveAs = QtGui.QPushButton("Сохранить как\nновую точку в группе",self.buttonBox)
		self.buttonSaveAs.setMinimumHeight(35)
		self.buttonSaveAs.setMinimumWidth(130)
		self.buttonBox.addButton(self.buttonSaveAs, QtGui.QDialogButtonBox.NoRole)
		self.buttonBox.setContentsMargins(0, 5, 0, 0)
		self.buttonSaveAs.clicked.connect(self.on_clicked_buttonSaveAs)

	def on_clicked_buttonSaveAs(self):
		if not qtmsgbox.show_question(self, "Создание новой точки в группе", "Вы дейcтвительно хотите сохранить данную точку <b>как новую</b> в данной группе?"):
			return
		base_point_group_id = self._measure.point.base_point_group_id
		self._measure = DbKeyInMeasure(self.main_app.srvcnxn)
		self._measure.point = SvyPoint(self.main_app.srvcnxn)
		self._measure.point.base_point_group_id = base_point_group_id

		if not super(SvyEditPoint, self).check():
			return

	def exec(self):
		timer = QtCore.QTimer()
		sel_point =  self.selectPoint()
		if not sel_point:
			self.close()
			return
		pid = sel_point['pid']
		cmid = sel_point['cmid']
		cmname = sel_point['cmname']
		timestamp = sel_point['timestamp']

		pt = SvyPoint(self.main_app.srvcnxn, pid, cmid, cmname)
		self._measure = DbKeyInMeasure(self.main_app.srvcnxn, pt.measure.id, pt._type_name, pt.measure.measurename)
		self._measure.timestamp = datetime.strptime(timestamp, "%Y-%m-%d")
		# # если выбранная точка была создана вручную, то присваиваем
		# # текущему измерению измерение, с помощью которого была создана точка
		# if pt.measure.type_name == "KI":
		# 	self._measure = pt.measure
		# # в противном случае присваиваем текущему измерению общую информацию
		# else:
		# 	self._measure.mine_id = pt.measure.mine_id
		# 	self._measure.equipment_id = pt.measure.equipment_id
		# 	self._measure.surveyor_id = pt.measure.surveyor_id

		self.setPoint(pt)
		super(SvyEditPoint, self).exec()
	#
	def selectPoint(self):
		params = []
		cmd = """
			SELECT
				B.BASEPOINT_ID,
				B.CREATEMEASURE_ID,
				B.MEASURENAME,
				B.POINTNAME,
				B.BASE_X, B.BASE_Y, B.BASE_Z,
				(SELECT S.SURNAME + ' ' + S.FIRSTNAME + ' ' + S.SUVMIDDLE FROM [{0}].[dbo].[SURVEY_FIO] S WHERE S.FIOID = M.EXECUTOR_ID),
				ISNULL(V.Vyr, ''),
				M.MEASTIMESTAMP,
				CASE WHEN B.BASEREMOVED = 1 THEN 'ДА' ELSE 'НЕТ' END REMOVED,
				R.NAME,
				C.NAME
			FROM [{0}].[dbo].[MP_BASE_POINTS] B 
				INNER JOIN [{0}].[dbo].[MP_MEASURES] M
				ON B.CREATEMEASURE_ID = M.MEASURE_ID 
				AND M.MEASURENAME = B.MEASURENAME
				AND M.MTYPE_ID = (SELECT MTYPE_ID FROM [{0}].[dbo].[MP_LKP_MEASURETYPES] WHERE MTYPENAME = B.CREATEMEATYPE_ID)
					LEFT JOIN [{0}].[dbo].[VYRAB_P] AS V
					ON M.EXCAVATION_ID = V.VyrId
						INNER JOIN [{0}].[dbo].[MP_REGISTER] AS R
						ON R.ID = M.REGISTER_ID
							INNER JOIN [{0}].[dbo].[MP_CLASS] AS C
							ON C.ID = B.CLASS_ID
		""".format(self.main_app.srvcnxn.dbsvy)

		#
		
		if self.main_app.srvcnxn.location:
			params.append(self.main_app.srvcnxn.location)
			cmd += """ AND M.LOCATION = ?"""

		cmd += " ORDER BY M.MEASTIMESTAMP DESC"

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		for i, row in enumerate(data):
			for iCol in [4,5,6]:
				try:			data[i][iCol] = "%.3f" % float(data[i][iCol])
				except:			pass
		self._header = ["ID", "CREATEMEASURE_ID", "MEASURENAME", "ИМЯ", "Y (ВОСТОК)", "X (СЕВЕР)", "Z (ОТМЕТКА)", "МАРКШЕЙДЕР", "ВЫРАБОТКА", "ДАТА", "УДАЛЕНА", "ЖУРНАЛ", "КЛАССИФИКАЦИЯ"]

		self.dialog = SelectFromTableDialog(self)
		self.dialog.setWindowTitle("Выберите точку для редактирования")
		model = CustomDataModel(self.dialog.view, data, self._header)
		self.dialog.view.setModel(model)
		self.dialog.view.hideColumn(0)
		self.dialog.view.hideColumn(1)
		self.dialog.view.hideColumn(2)
		# dialog.view.hideColumn(3)
		# dialog.view.hideColumn(4)
		self.dialog.view.setColumnWidth(7, 300)
		self.dialog.resize(QtCore.QSize(1350, 500))


		self.dialog.view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.dialog.view.customContextMenuRequested.connect(self.displayMenu)

	

		self.dialog.exec()
		#
		pointdata = self.dialog.getResult()
		if not pointdata:
			return

		return {'pid':pointdata[0][0], 'cmid':pointdata[0][1], 'cmname':pointdata[0][2], 'timestamp':pointdata[0][9]}
	
	def displayMenu(self, position):
		#item = self.dialog.view.itemAt(position)
		indexes = self.dialog.view.selectedIndexes()

		model = self.dialog.view.model()
		if indexes:
			selectedRows = list(sorted(set([index.row() for index in indexes])))
			_result = []
			for iRow in selectedRows:
				_result.append([model.data(model.index(iRow, iCol)) for iCol in range(model.columnCount())])

		menu = QtGui.QMenu()
		action = QtGui.QAction("Связанные точки", menu) 
		action.triggered.connect(lambda: self.select_grouped_point(_result))
		menu.addAction(action)
		menu.exec_(self.dialog.view.viewport().mapToGlobal(position))

	def select_grouped_point(self, rows):
		row = rows[0]
		pid = row[0]
		cmid = row[1]

		cmd = """
			SELECT
				BASE_POINT_GROUP_ID
			FROM [{0}].[dbo].[MP_BASE_POINTS]  
			WHERE BASEPOINT_ID = ? 
			AND CREATEMEASURE_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (pid, cmid)
		groupid = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)

		params = []
		cmd = """
			SELECT
				B.BASEPOINT_ID,
				B.CREATEMEASURE_ID,
				B.MEASURENAME,
				B.POINTNAME,
				B.BASE_X, B.BASE_Y, B.BASE_Z,
				(SELECT S.SURNAME + ' ' + S.FIRSTNAME + ' ' + S.SUVMIDDLE FROM [{0}].[dbo].[SURVEY_FIO] S WHERE S.FIOID = M.EXECUTOR_ID),
				M.MEASTIMESTAMP,
				CASE WHEN B.BASEREMOVED = 1 THEN 'ДА' ELSE 'НЕТ' END REMOVED,
				R.NAME,
				C.NAME
			FROM [{0}].[dbo].[MP_BASE_POINTS] B 
				INNER JOIN [{0}].[dbo].[MP_MEASURES] M
				ON B.CREATEMEASURE_ID = M.MEASURE_ID 
				AND M.MEASURENAME = B.MEASURENAME
				AND M.MTYPE_ID = (SELECT MTYPE_ID FROM [{0}].[dbo].[MP_LKP_MEASURETYPES] WHERE MTYPENAME = B.CREATEMEATYPE_ID)
					INNER JOIN [{0}].[dbo].[MP_REGISTER] AS R
					ON R.ID = M.REGISTER_ID
						INNER JOIN [{0}].[dbo].[MP_CLASS] AS C
						ON C.ID = B.CLASS_ID
			---WHERE B.BASEREMOVED = 0
		""".format(self.main_app.srvcnxn.dbsvy)

		#
		if  groupid is not None:
			params.append(groupid)
			cmd += """ AND B.BASE_POINT_GROUP_ID = ?"""

		if self.main_app.srvcnxn.location:
			params.append(self.main_app.srvcnxn.location)
			cmd += """ AND M.LOCATION = ?"""

		cmd += " ORDER BY M.MEASTIMESTAMP DESC"

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		for i, row in enumerate(data):
			for iCol in [4,5,6]:
				try:			data[i][iCol] = "%.3f" % float(data[i][iCol])
				except:			pass

		model = CustomDataModel(self.dialog.view, data, self._header)
		self.dialog.view.updateModel(model)

	#
	def check(self):
		if not super(SvyCreatePoint, self).check():
			return

		#
		if not self.fieldsHaveData():
			return
		try:
			self.getParams()
			self.prepare()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", str(e))

class SvyRemovePoint(DialogWindow):
	def exec(self):
		timer = QtCore.QTimer()
		self._pid = self.selectPoint()
		if not self._pid:
			self.close()
			return
		#
		self.prepare()

	def selectPoint(self):
		params = []
		cmd = """
			SELECT
				[BASEPOINT_ID]
				, [POINTNAME]
				, [BASE_X]
				, [BASE_Y]
				, [BASE_Z]
				, ISNULL(V.Vyr, '')
				,(SELECT [SURNAME] + ' ' + [FIRSTNAME] + ' ' + [SUVMIDDLE] FROM [{0}].[dbo].[SURVEY_FIO] S WHERE S.[FIOID] = M.[EXECUTOR_ID]) [SURVEYOR]
				, M.MEASTIMESTAMP
				, R.NAME
				, C.NAME
			FROM [{0}].[dbo].[MP_BASE_POINTS] P 
				INNER JOIN [{0}].[dbo].[MP_MEASURES] M
				ON P.[CREATEMEASURE_ID] = M.[MEASURE_ID] 
					AND P.[MEASURENAME] = M.[MEASURENAME]
						INNER JOIN [{0}].[dbo].MP_REGISTER AS R
						ON R.ID = M.REGISTER_ID
							INNER JOIN [{0}].[dbo].[MP_CLASS] AS C
							ON C.ID = P.CLASS_ID
								LEFT JOIN [{0}].[dbo].[VYRAB_P] AS V
								ON M.EXCAVATION_ID = V.VyrId
			WHERE M.[MTYPE_ID] = (
				SELECT MTYPE_ID 
				FROM [{0}].[dbo].[MP_LKP_MEASURETYPES] 
				WHERE MTYPENAME = P.[CREATEMEATYPE_ID]
			) 
		""".format(self.main_app.srvcnxn.dbsvy)


		#
		if self.main_app.srvcnxn.location is not None:
			params.append(self.main_app.srvcnxn.location)
			cmd += """ AND M.LOCATION = ?"""
		#
		cmd += """
			AND [BASEPOINT_ID] not in (
				SELECT T.[POINT_ID] FROM
				(
					SELECT [MEASURE_ID], [STANDPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_STATIONMEASURES]
					UNION
					SELECT [MEASURE_ID], [BACKPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_STATIONMEASURES]
					UNION
					SELECT [MEASURE_ID], [NEWPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_STATIONMEASURES]
					UNION
					SELECT [MEASURE_ID], [CTRLBACKPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_TRAVERSALMEASURES]
					UNION
					SELECT [MEASURE_ID], [CTRLDIRPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_TRAVERSALMEASURES]
					UNION
					SELECT [MEASURE_ID], [CTRLSTANDPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_TRAVERSALMEASURES]
					UNION
					SELECT [MEASURE_ID], [STANDPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_TRAVERSALDRIVES]
					UNION
					SELECT [MEASURE_ID], [DIRPOINT_ID] AS [POINT_ID] FROM [{0}].[dbo].[MP_TRAVERSALDRIVES]					
				) T
				WHERE T.[POINT_ID] IS NOT NULL
			)
			ORDER BY [MEASTIMESTAMP] DESC
		""".format(self.main_app.srvcnxn.dbsvy)

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		for i, row in enumerate(data):
			for iCol in [2, 3, 4]:
				try:			data[i][iCol] = "%.3f" % float(data[i][iCol])
				except:			pass
		header = ["ID", "ИМЯ", "Y (ВОСТОК)", "X (СЕВЕР)", "Z (ОТМЕТКА)", "ВЫРАБОТКА", "МАРКШЕЙДЕР", "ДАТА", "ЖУРНАЛ", "КЛАССИФИКАЦИЯ"]

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите точку для удаления")
		model = CustomDataModel(dialog.view, data, header)
		dialog.view.setModel(model)
		dialog.view.hideColumn(0)
		dialog.view.setColumnWidth(5, 300)
		dialog.resize(QtCore.QSize(1350, 500))
		dialog.exec()
		#
		pointdata = dialog.getResult()
		if not pointdata:
			return

		return pointdata[0][0]

	#
	def prepare(self):
		if not super(SvyRemovePoint, self).prepare():
			return
	
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", str(e))

		self.after_run()
	#
	def run(self):
		if not super(SvyRemovePoint, self).run():
			return

		try:
			self.main_app.srvcnxn.exec_query("DELETE FROM [{0}].[dbo].[MP_BASE_POINTS] WHERE [BASEPOINT_ID] = ?".format(self.main_app.srvcnxn.dbsvy), (self._pid))
		except Exception as e:
			re_result = re.search("@.*@",str(e)) 
			if 	not re_result is None and  re_result.group(0)  == '@REGISTER_ACCESS_DENIED@':
				raise Exception ("Доступ к данному журналу запрещен.")
			elif 	not re_result is None and  re_result.group(0)  == '@ALREADY_EXISTS@':
				raise Exception ("В этом журнале уже существует не удаленная точка с таким именем.")
			elif 	not re_result is None and  re_result.group(0)  == '@NOT_ONLY_POINT_CLASS_IN_GROUP@':
				raise Exception ("Ошибка", "В группе должна быть актуальной только одна точка в одном классе.")
			else:
				raise Exception("Невозможно удалить точку. %s" % str(e))

		self.main_app.srvcnxn.commit()
		#
		qtmsgbox.show_information(self.main_app, "Выполнено", "Точка успешно удалена")

	
