from PyQt4 import QtGui, QtCore
import os
from utils.constants import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from utils.validation import is_blank
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyCreateSubUnit(DialogWindow):
	def __init__(self, *args, **kw):
		super(SvyCreateSubUnit, self).__init__(*args, **kw)

		self.setWindowTitle("Создать подюнит")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createVariables(self):
		self._layers = {}
		self._units = {}
		self._subunits = []
		self.prefix = self.main_app.settings.getValue(ID_SVY_UPREFIX)
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")

		self.inputLayout = QtGui.QFormLayout()

		self.labelLayer = QtGui.QLabel(self.groupBoxInput)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setMinimumWidth(100)
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxLayer.setMinimumWidth(200)

		self.labelUnit = QtGui.QLabel(self.groupBoxInput)
		self.labelUnit.setMinimumWidth(100)
		self.labelUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelUnit.setText(self.main_app.settings.getValue(ID_SVY_UDSPNAME))
		self.comboBoxUnit = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxUnit.setMinimumWidth(200)

		self.labelSubUnit = QtGui.QLabel(self.groupBoxInput)
		self.labelSubUnit.setMinimumWidth(100)
		self.labelSubUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSubUnit.setText(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME))	    # Аттрибут привязки выработки это сабюнит??
		self.lineEditSubUnit = QtGui.QLineEdit(self.groupBoxInput)

		

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
		self.comboBoxLayer.currentIndexChanged.connect(self.buildUnitList)

	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelUnit)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxUnit)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSubUnit)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditSubUnit)

	def buildCombos(self):
		self.buildLayerList()
		self.buildUnitList()

	def buildLayerList(self):
		# считываем залежи из БД
		self.comboBoxLayer.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		self._layers = {}
		#
		cmd = """
			SELECT LAYER_ID, LAYER_DESCRIPTION 
			FROM [{0}].[dbo].[LAYER]
			WHERE LOCATION = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		for layerId, layerName in data:
			self._layers[layerName] = layerId
		self.comboBoxLayer.addItems(sorted(list(self._layers.keys())))

	def buildUnitList(self):
		# считываем юниты из БД
		self.comboBoxUnit.clear()
		#locationName = self.main_app.settings.getValue(ID_PROJNAME)
		layer_name = self.comboBoxLayer.currentText()
		layer_id = self._layers[layer_name]
		self._unit = {}
		#
		cmd = """
			SELECT PANEL_ID, PANEL_NAME 
			FROM [{0}].[dbo].[PANEL]
			WHERE LAYER_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (layer_id)

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		for unitId, unitName in data:
			self._units[unitName] = unitId
		self.comboBoxUnit.addItems(sorted(list(self._units.keys())))

	def createFolders(self):
		networkpath = self.main_app.settings.getValue(ID_NETPATH)
		svydir = self.main_app.settings.getValue(ID_SVY_MAINDIR)
		layerName = self.comboBoxLayer.currentText()
		unitPrefix = self.main_app.settings.getValue(ID_SVY_UPREFIX)
		unitDisplayName = self.main_app.settings.getValue(ID_SVY_UDSPNAME)
		unitName = self.comboBoxUnit.currentText()
		subUnitName = self.lineEditSubUnit.text().strip()

		unitPrefixAndName = unitPrefix+unitName

		# создание залежи, если отсутствует
		currentFolder = os.path.join(networkpath, svydir, layerName)
		if not os.path.exists(currentFolder):
			os.mkdir(currentFolder)
		
		# создание юнита, если отсутствует
		currentFolder = os.path.join(currentFolder, unitPrefixAndName)
		if not os.path.exists(currentFolder):
			try:
				os.mkdir(currentFolder)

				# создание вложенных директорий
				for folderPurpose, folderName in self.main_app.settings.getValue(ID_SVY_USUBDIRS):
					try:
						os.mkdir(currentFolder + "\\" + folderName)
					except Exception as e:
						raise Exception("Ошибка", "Невозможно создать директорию '%s'. %s" % (folderName, str(e)))
						
			except:
				raise

		# создание подюнита, если отсутствует
		currentFolder = os.path.join(currentFolder, "КАРКАСЫ", subUnitName)
		if os.path.exists(currentFolder):
			#qtmsgbox.show_error(self, "Ошибка", "%s уже существует" % subUnitName)
			return	False

		try:
			os.mkdir(currentFolder)
		except Exception as e:
			raise Exception( "Ошибка", "Невозможно создать директорию '%s'. %s" % (subUnitName, str(e)))
			

	

		return True

	def createDbRecords(self):
		unit_name = self.comboBoxUnit.currentText()
		unit_id = self._units[unit_name]
		subunit_name = self.lineEditSubUnit.text().strip()

		cmd = """
			SELECT *
			FROM [{0}].[dbo].[SUBUNIT]
			WHERE UNIT = ? AND NAME = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (unit_id, subunit_name)

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if len(data) > 0:
			return False

		cmd = """
			INSERT INTO [{0}].[dbo].[SUBUNIT]
			(UNIT, NAME)
			values
			(?, ?)
		""".format(self.main_app.srvcnxn.dbsvy)
		params = ( unit_id, subunit_name)

		self.main_app.srvcnxn.exec_query(cmd, params)
		self.main_app.srvcnxn.commit()
		return True

	def prepare(self):
		if not super(SvyCreateSubUnit, self).prepare():
			return

		unitName = self.lineEditSubUnit.text()
		if is_blank(unitName):
			qtmsgbox.show_error(self, "Ошибка", "Неверное значение. Подюнит не может быть пустым значением")
			return
		#prefix = self.main_app.settings.getValue(ID_SVY_UPREFIX)
		#if not unitName.startswith(prefix):
		#	qtmsgbox.show_error(self, "Ошибка", "Неверное значение. Ожидался префикс '%s'" % prefix)
		#	return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать маркшейдерский подюнит")

		self.after_run()

	def run(self):
		if not super(SvyCreateSubUnit, self).run():
			return
		try:
			is_folder_created = self.createFolders()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать маркшейдерский подюнит. {0}".format(str(e)))
			raise
		try:
			is_dbrecords_created = self.createDbRecords()
		except:
			raise Exception("Невозможно добавить запись о подюните в базу данных")
		
		msg = ("Каталог уже существовал." if not is_folder_created else "") + (" Запись в БД уже существовала." if not is_dbrecords_created else "")

		qtmsgbox.show_information(self, "Выполнено", "Подюнит успешно создан. \n{0}".format(msg))
		self.close()
