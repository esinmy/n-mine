from PyQt4 import QtGui, QtCore
import os
from utils.constants import *

import pyodbc
import re
from collections import OrderedDict
from datetime import datetime
from utils.validation import  is_blank

from gui.qt.widgets.TableDataView import CustomDataView, CustomDataModel


import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyBrowseRegister(DialogWindow):
	ADD = 1
	EDIT = 2
	DELETE = 3
	def __init__(self, *args, **kw):
		super(SvyBrowseRegister, self).__init__(*args, **kw)

		self.setWindowTitle("Журналы маркшейдерских точек")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.refreshFromDb()

		self.resize(700, 300)

	def __createVariables(self): pass


	def __createWidgets(self):

		self.groupBoxTable = QtGui.QGroupBox(self)
		
		self.layoutTable = QtGui.QVBoxLayout()
		self.layoutTable.setSpacing(5)


		self.view = CustomDataView(self.groupBoxTable)
		header = ["ID", "Наименование", "Группа доступа", "Дата"]

		#self.model = CustomDataModel(self.view, [[""]], [""])
		self.model = CustomDataModel(self.view, [header], header)
		self.view.horizontalHeader().setDefaultSectionSize(70)


		self.view.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
		self.view.setModel(self.model)




		self.buttonBox.removeButton(self.buttonBox.button(QtGui.QDialogButtonBox.Cancel))

		self.buttonAdd = QtGui.QPushButton("Добавить",self.buttonBox)
		self.buttonEdit = QtGui.QPushButton("Редактировать",self.buttonBox)
		self.buttonDelete = QtGui.QPushButton("Удалить",self.buttonBox)

		self.buttonBox.addButton(self.buttonAdd, QtGui.QDialogButtonBox.NoRole)
		self.buttonBox.addButton(self.buttonEdit, QtGui.QDialogButtonBox.NoRole)
		self.buttonBox.addButton(self.buttonDelete, QtGui.QDialogButtonBox.NoRole)




		

	def __createBindings(self):	 
		
		#self.tvStoredSettings.selectionModel().selectionChanged.connect(self.on_selectionChanged_tvStoredSettings)
		#self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Open).clicked.connect(self.on_clicked_buttonBoxCurrentSettings_open_openSettings)
		#self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Save).clicked.connect(self.on_clicked_buttonBoxCurrentSettings_save_saveSettingsAs)
		#self.buttonApply.clicked.connect(self.on_clicked_buttonBoxStoredSettings_buttonApply)
		#self.buttonDelete.clicked.connect(self.on_clicked_buttonBoxStoredSettings_buttonDelete)
		#self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Close).clicked.connect(self.close)

		self.buttonAdd.clicked.connect(self.on_clicked_buttonAdd)
		self.buttonEdit.clicked.connect(self.on_clicked_buttonEdit)
		self.buttonDelete.clicked.connect(self.on_clicked_buttonDelete)
		self.view.doubleClicked.connect(self.on_mouseDoubleClickEvent_view)

	def __gridWidgets(self):
		
		self.layoutControls.addWidget(self.groupBoxTable)
		self.groupBoxTable.setLayout(self.layoutTable)
		self.layoutTable.addWidget(self.view)

	def refreshFromDb(self):
		cmd = """
			SELECT
				R.ID,
				R.NAME,
				R.[GROUP],
				R.[DATE]
				
			FROM [{0}].[dbo].[MP_REGISTER] AS R
		""".format(self.main_app.srvcnxn.dbsvy)
		#
		
		data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		
		#
		header = ["ID", "Наименование", "Группа доступа", "Дата"]
		if data:
			self.model = CustomDataModel(self.view, data, header)
			self.view.updateModel(self.model)
											   
	def _getCurrentRow(self):
		_result = []
		indexes = self.view.selectedIndexes()
		model = self.view.model() 
		if indexes:
			selectedRows = list(sorted(set([index.row() for index in indexes])))
			for iRow in selectedRows:
				_result.append([model.data(model.index(iRow, iCol)) for iCol in range(model.columnCount())])

		return _result



	
	
	# Обработка событий

	def on_clicked_buttonAdd(self):
		form = SvyCreateRegister(self, mode = SvyBrowseRegister.ADD)
		form.exec_()
		if form._is_cancelled:
			return
		params = form.getParams()
		self.save (params)
		self.refreshFromDb()

	def on_clicked_buttonEdit(self):
		form = SvyCreateRegister(self,  mode = SvyBrowseRegister.EDIT)
		rows = self._getCurrentRow()
		if rows is None or len(rows) == 0:
			qtmsgbox.show_error(self, "Внимание", "Не выбран журнал для редактирования.")
			return
		row = rows[0]
		values = {'ID': str(row[0]),
					'NAME': row[1],
					'GROUP': row[2],
					'DATE': row[3]}
		form.setParams(values)
		form.exec_()
		if form._is_cancelled:
			return
		params = form.getParams()
		self.save (params)
		self.refreshFromDb()

	def on_clicked_buttonDelete(self):
		form = SvyCreateRegister(self, mode = SvyBrowseRegister.DELETE)
		rows = self._getCurrentRow()
		if rows is None or len(rows) == 0:
			qtmsgbox.show_error(self, "Внимание", "Не выбран журнал для удаления.")
			return
		row = rows[0]
		values = {'ID': str(row[0]),
					'NAME': row[1],
					'GROUP': row[2],
					'DATE': row[3]}
		form.setParams(values)
		form.exec_()
		if form._is_cancelled:
			return
		params = form.getParams()
		if form._is_cancelled:
			return
		self.delete (params)
		self.refreshFromDb()

	def on_mouseDoubleClickEvent_view(self):
		self.buttonEdit.clicked.emit(True) 

	def save(self, data):
		
		if is_blank(data['ID']):


			cmd = """
				select TOP 1 NAME from [{0}].sys.types as t
				where is_user_defined = 1 and system_type_id = 127
				and exists(
					SELECT * FROM fn_my_permissions ('[{0}].[dbo].'+t.name, 'TYPE')
				)
			""".format(self.main_app.srvcnxn.dbsvy)
			#
		
			group_name = self.main_app.srvcnxn.exec_query(cmd).get_row(0)
			if group_name is None:
				qtmsgbox.show_error(self, "Ошибка", "Нет доступа к журналам.")
				return
			
			#
			name = data['NAME'] 
			group = group_name.strip() 
			date = datetime.strptime(data['DATE'] , "%d.%m.%Y")

			if is_blank(name):
				qtmsgbox.show_error(self, "Ошибка", "Не заполнено наименование журнала.")
				return


			#

			cmd = """
				INSERT INTO  [{0}].[dbo].[MP_REGISTER]
				(NAME, [GROUP], [DATE])
				VALUES (?, ?, ?)
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (name, group, date)

		else:
			id = int(data['ID']) 
			name = data['NAME'] 
			group = data['GROUP'] 
			date = datetime.strptime(data['DATE'] , "%d.%m.%Y")

			if is_blank(name):
				qtmsgbox.show_error(self, "Ошибка", "Не заполнено наименование журнала.")
				return

			cmd = """
				UPDATE [{0}].[dbo].[MP_REGISTER] 
				SET  NAME = ?,
			    [GROUP] = ?,
			    [DATE] = ?
				WHERE ID = ?
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (name, group, date, id)

			
		try:
			self.main_app.srvcnxn.exec_query(cmd, params)
			self.main_app.srvcnxn.commit()
		except Exception as e:
			
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
				re_result = re.search("@.*@",str(e)) 
				if 	not re_result is None and  re_result.group(0)  == '@REGISTER_ACCESS_DENIED@':
					qtmsgbox.show_error(self, "Ошибка", "Доступ к данному журналу запрещен.")
				else:
					qtmsgbox.show_error(self, "Ошибка", "Что-то пошло не так. {0}".format(str(e)))
					raise


	def delete(self, data):
		if is_blank(data['ID']):
			return
		cmd = """
			DELETE FROM [{0}].[dbo].[MP_REGISTER] 
			WHERE ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (data['ID'])


		try:
			self.main_app.srvcnxn.exec_query(cmd, params)
			self.main_app.srvcnxn.commit()
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
				re_result = re.search("@.*@",str(e)) 
				if 	not re_result is None and  re_result.group(0)  == '@REGISTER_ACCESS_DENIED@':
					qtmsgbox.show_error(self, "Ошибка", "Доступ запрещен к данному журналу запрещеню.")
				else:
					qtmsgbox.show_error(self, "Ошибка", "<b>Что-то пошло не так. Возможно данный журнал уже используется</b> <br> {0}".format(str(e)))
					raise			


	def prepare(self):
		if not super(SvyBrowseRegister, self).prepare():
			return

		
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Что-то пошло не так")

		self.after_run()



class SvyCreateRegister(DialogWindow):

	def __init__(self, parent, mode = SvyBrowseRegister.ADD,  *args, **kw):
		super(SvyCreateRegister, self).__init__(parent, *args, **kw)

		self._mode = mode
		if self._mode == SvyBrowseRegister.ADD:
			title = "Создать журнал"
		elif self._mode == SvyBrowseRegister.EDIT:
			title = "Редактировать журнал"
		elif self._mode == SvyBrowseRegister.DELETE:
			title = "Удалить журнал"

		self.setWindowTitle(title)
		
		self.__createVariables()
		self.__createWigdets()
		self.__createBindings()
		self.__gridWidgets()

		self.setMinimumWidth(500)

	def __createVariables(self):		pass
	def __createWigdets(self):
		self.groupBoxRegister = QtGui.QGroupBox(self)
		self.layoutRegister = QtGui.QFormLayout()

		self.labelRegisterId = QtGui.QLabel(self.groupBoxRegister)
		self.labelRegisterId.setText("ID")
		self.labelRegisterId.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelRegisterId.setMinimumWidth(80)
		self.lineEditRegisterId = QtGui.QLineEdit(self.groupBoxRegister)
		self.lineEditRegisterId.setEnabled(False)

		self.labelRegisterName = QtGui.QLabel(self.groupBoxRegister)
		self.labelRegisterName.setText("Наименование")
		self.labelRegisterName.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelRegisterName.setMinimumWidth(80)
		self.lineEditRegisterName = QtGui.QLineEdit(self.groupBoxRegister)
		self.lineEditRegisterName.setEnabled(self._mode != SvyBrowseRegister.DELETE)

		

		self.labelRegisterGroup = QtGui.QLabel(self.groupBoxRegister)
		self.labelRegisterGroup.setText("Группа доступа")
		self.labelRegisterGroup.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelRegisterGroup.setMinimumWidth(80)
		self.lineEditRegisterGroup = QtGui.QLineEdit(self.groupBoxRegister)
		self.lineEditRegisterGroup.setEnabled(False)

		self.labelRegisterDate = QtGui.QLabel(self.groupBoxRegister)
		self.labelRegisterDate.setText("Дата")
		self.labelRegisterDate.setMinimumWidth(80)
		self.labelRegisterDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditRegisterDate = QtGui.QDateEdit(self.groupBoxRegister)
		self.lineEditRegisterDate.setCalendarPopup(True)
		self.lineEditRegisterDate.calendarWidget().setFirstDayOfWeek(QtCore.Qt.Monday)
		now = datetime.now()
		self.lineEditRegisterDate.setDate(QtCore.QDate(now.year, now.month, now.day))
		self.lineEditRegisterDate.setEnabled(self._mode != SvyBrowseRegister.DELETE)


		
	def __createBindings(self):		 
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.check)

	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxRegister)
		self.groupBoxRegister.setLayout(self.layoutRegister)
		
		self.layoutRegister.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelRegisterId)
		self.layoutRegister.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditRegisterId)
		self.layoutRegister.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelRegisterName)
		self.layoutRegister.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditRegisterName)
		self.layoutRegister.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelRegisterDate)
		self.layoutRegister.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditRegisterDate)
		self.layoutRegister.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelRegisterGroup)
		self.layoutRegister.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditRegisterGroup)





	def getParams(self):
		_result = {'ID': self.lineEditRegisterId.text(),
					'NAME': self.lineEditRegisterName.text(),
					'GROUP': self.lineEditRegisterGroup.text(),
					'DATE': self.lineEditRegisterDate.text()}
		return _result
	def setParams(self, data = None):
		if data:
			self.lineEditRegisterId.setText(data['ID'] if data['ID'] else "")
			self.lineEditRegisterName.setText(data['NAME'] if data['NAME'] else "")
			self.lineEditRegisterDate.setDate( datetime.strptime(data['DATE'] , "%Y-%m-%d") )
			self.lineEditRegisterGroup.setText(data['GROUP'] if data['GROUP'] else "")

		else:
			self.lineEditRegisterId.setText("")
			self.lineEditRegisterName.setText("")
			self.lineEditRegisterDate.setDate(datetime.now())
			self.lineEditRegisterGroup.setText("")
			

	def fieldsHaveData(self):
		labels = [self.labelRegisterName, self.labelRegisterDate]
		fields = [self.lineEditRegisterName, self.lineEditRegisterDate]

		for label, field in zip(labels, fields):
			value = field.text()
			if is_blank(value):
				qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
				field.setFocus()
				return False
		return True

	def check(self):
		if not super(SvyCreateRegister, self).check():
			return

		#
		if not self.fieldsHaveData():
			return
		
		self.prepare()
	def prepare(self):
		if not super(SvyCreateRegister, self).prepare():
			return
		
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", str(e))

		self.after_run()
		self.close()
		self._is_cancelled = False


