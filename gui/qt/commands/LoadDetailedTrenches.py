try:		import MMpy
except:		pass

from PyQt4 import QtGui, QtCore
import os
from itertools import groupby

from datetime import datetime
from utils.constants import *
from utils.validation import is_blank
from utils.osutil import get_temporary_prefix

from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.DatabaseLoaderWindow import DatabaseLoaderWindow

from mm.formsets import loadSurveyPoints, loadDrillholesDb, loadDrillholesValue, loadDrillholesHatch
from mm.mmutils import MicromineFile, DrillholesDb, TrenchesDb
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class LoadDetailedTrenches(DatabaseLoaderWindow):
	ID_LOAD_MODE_SELECT		= 1
	ID_LOAD_MODE_DISPLAY	= 2

	@classmethod
	def get_empty_return_value(cls):
		return {'header': [] ,'data': []}

	def __init__(self, *args, **kw):
		super(LoadDetailedTrenches, self).__init__(*args, **kw)
		self.setWindowTitle("Выбрать борозды")

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
		#self.hideDateFilter()

	def __createVariables(self):
		geoUnitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper()
		self._headerCollarImport = ["TR_ID", "XCOLLAR", "YCOLLAR", "ZCOLLAR", "DEPTH", "DEPOSIT", "LAYER", "UNIT", "SECTION", "MINE_NAME", "START_DATE", "END_DATE"]  
		self._headerCollarExport = ["ID борозды", "XCOLLAR", "YCOLLAR", "ZCOLLAR", "DEPTH", "DEPOSIT", "LAYER", geoUnitDisplayName, "SECTION", "MINE_NAME", "Начало", "Окончание"]  
		self._headerSurveyImport = ["TR_ID", "XCOLLAR", "YCOLLAR", "ZCOLLAR", "JOIN", "POINT_ORDER", "DEPOSIT", "LAYER", "MINE_NAME"]   
		self._headerSurveyExport = ["HOLE_ID", "ВОСТОК", "СЕВЕР", "Z", "JOIN", "POINT_ORDER", "DEPOSIT", "LAYER", "MINE_NAME"]   
		self._headerAssayImport = ["TR_ID"	, "SAMPLE_ID", "FROM", "TO", "LENGTH",  "ROCK", "INDEX", "ORE_INDEX", "DEPOSIT", "CORE_REC", "XCOLLAR", "YCOLLAR", "ZCOLLAR"]
		self._headerAssayExport = ["HOLE_ID", "SAMPLE_ID", "FROM", "TO", "LENGTH",  "ROCK", "INDEX", "ORE_INDEX",  "DEPOSIT", "CORE_REC", "XCOLLAR", "YCOLLAR", "ZCOLLAR"]


		idw_elems = self.main_app.settings.getValue(ID_IDWELEM)
		elems = list(zip(*idw_elems))[0]

		self._headerAssayImport = self.__insertListInPosList(self._headerAssayImport, elems, 5)
		self._headerAssayExport = self.__insertListInPosList(self._headerAssayExport, elems, 5)
		


		self._work_mode = self.ID_LOAD_MODE_DISPLAY
		self._returnedData = []
		self._can_close = False

		netCollarPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRCOLLAR))

		f = MicromineFile()
		if not f.open(netCollarPath):
			raise Exception("Невозможно открыть файл '%s'" % netCollarPath)

		try:
			data = [row for row in f.read(columns = self._headerCollarImport, asstr = True)]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать файл. Обновите базу данных скважин. %s" % str(e))
			f.close()
		f.close()
		#region DataFile loading
		#f2 = open(r'c:\temp\collar_tr.txt', 'w')
		#for row in data:
		#	f2.write(','.join(row))
		#	f2.write('\n')
		#f2.close()


		#f2 = open(r'c:\temp\collar_tr.txt', 'r')
		#data = [line.strip('\n').split(',') for line in f2]
		#f2.close()
		#endregion
		try:
			data.sort(key = lambda x: x[0], reverse = True)
			self.setDataset(data, self._headerCollarExport, 10, 11)
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", str(e))
			raise
	def __createWidgets(self):
		self.layoutAddons = QtGui.QHBoxLayout()

		self.groupBoxDisplayAssay = QtGui.QGroupBox(self)
		self.groupBoxDisplayAssay.setTitle("Загрузить опробование")
		self.groupBoxDisplayAssay.setCheckable(True)
		self.groupBoxDisplayAssay.setChecked(False)

		self.layoutAssay = QtGui.QFormLayout()
		self.labelAssayForm = QtGui.QLabel(self.groupBoxDisplayAssay)
		self.labelAssayForm.setMinimumWidth(80)
		self.labelAssayForm.setText("Форма")
		self.labelAssayForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditAssayForm = QtGui.QLineEdit(self.groupBoxDisplayAssay)
		self.lineEditAssayForm.setMaximumWidth(100)

		self.groupBoxDisplayLitho = QtGui.QGroupBox(self)
		self.groupBoxDisplayLitho.setCheckable(True)
		self.groupBoxDisplayLitho.setChecked(False)
		self.groupBoxDisplayLitho.setTitle("Загрузить литологию")

		self.layoutLitho = QtGui.QFormLayout()
		self.labelLithoForm = QtGui.QLabel(self.groupBoxDisplayLitho)
		self.labelLithoForm.setMinimumWidth(80)
		self.labelLithoForm.setText("Форма")
		self.labelLithoForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditLithoForm = QtGui.QLineEdit(self.groupBoxDisplayLitho)
		self.lineEditLithoForm.setMaximumWidth(100)

		self.groupBoxDisplayIntervals = QtGui.QGroupBox(self)
		self.groupBoxDisplayIntervals.setCheckable(True)
		self.groupBoxDisplayIntervals.setChecked(False)
		self.groupBoxDisplayIntervals.setTitle("Загрузить метки интервалов")

		self.layoutIntervals = QtGui.QFormLayout()
		self.labelIntervalsForm = QtGui.QLabel(self.groupBoxDisplayIntervals)
		self.labelIntervalsForm.setMinimumWidth(80)
		self.labelIntervalsForm.setText("Форма")
		self.labelIntervalsForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditIntervalsForm = QtGui.QLineEdit(self.groupBoxDisplayIntervals)
		self.lineEditIntervalsForm.setMaximumWidth(100)	
	
	def __createBindings(self):
		self.dateRangeWidget.dateEditFrom.dateChanged.connect(self.on_dateRangeWidget_dateEditFrom_dateChanged)
		self.dateRangeWidget.dateEditTo.dateChanged.connect(self.on_dateRangeWidget_dateEditTo_dateChanged)
		self.shapeFilter.filterApplied.connect(self.on_shapeFilter_filterApplied)
		self.shapeFilter.filterDefaultRestored.connect(self.on_shapeFilter_filterDefaultRestored)
		self.lineEditAssayForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditAssayForm)
		self.lineEditLithoForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditLithoForm)
		self.lineEditIntervalsForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditIntervalsForm)

	def __gridWidgets(self):
		self.layoutParams.addLayout(self.layoutAddons)
		
		self.layoutAddons.addWidget(self.groupBoxDisplayAssay)
		self.groupBoxDisplayAssay.setLayout(self.layoutAssay)
		self.layoutAssay.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelAssayForm)
		self.layoutAssay.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditAssayForm)

		self.layoutAddons.addWidget(self.groupBoxDisplayLitho)
		self.groupBoxDisplayLitho.setLayout(self.layoutLitho)
		self.layoutLitho.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLithoForm)
		self.layoutLitho.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditLithoForm)

		self.layoutAddons.addWidget(self.groupBoxDisplayIntervals)
		self.groupBoxDisplayIntervals.setLayout(self.layoutIntervals)
		self.layoutIntervals.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelIntervalsForm)
		self.layoutIntervals.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditIntervalsForm)


	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
			self._set_shapefilter_params()
			event.ignore()
		else:
			super(LoadDetailedTrenches, self).keyPressEvent(event)

	def on_dateRangeWidget_dateEditFrom_dateChanged(self):
		self._set_shapefilter_params()


	def on_dateRangeWidget_dateEditTo_dateChanged(self):
		self._set_shapefilter_params()


	def on_shapeFilter_filterApplied(self, sender):
		self._set_shapefilter_params()


	def on_shapeFilter_filterDefaultRestored(self, sender):
		self._set_shapefilter_params()

	def _selectFormset(self, event, wgt):
		if not wgt.isEnabled():
			return

		cmdId = 512 if wgt == self.lineEditIntervalsForm else 509

		#
		try:
			tree = TreeMmFormsetBrowserDialog(self)
			tree.readFormsets(cmdId)
			tree.exec()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать наборы форм. %s" % str(e))
			return

		ret = tree.result
		if not ret:
			return
		wgt.setText(ret)

	def _set_shapefilter_params(self):
		extents={}
		extents['xmin'] = self.shapeFilter.xmin
		extents['xmax'] = self.shapeFilter.xmax
		extents['ymin'] = self.shapeFilter.ymin
		extents['ymax'] = self.shapeFilter.ymax
		extents['zmin'] = self.shapeFilter.zmin
		extents['zmax'] = self.shapeFilter.zmax
		self.model.applyFilter(10, 11, extents = extents, xcol = 1, ycol = 2, zcol = 3)	
		
	def set_work_mode(self, mode):
		if mode & (self.ID_LOAD_MODE_DISPLAY | self.ID_LOAD_MODE_SELECT):
			self._work_mode = mode
		if self._work_mode &  self.ID_LOAD_MODE_DISPLAY:
			self.groupBoxDisplayAssay.setVisible(True)
			self.groupBoxDisplayLitho.setVisible(True)
			self.groupBoxDisplayIntervals.setVisible(True)

		elif self._work_mode &  self.ID_LOAD_MODE_SELECT:
			self.groupBoxDisplayAssay.setVisible(False)
			self.groupBoxDisplayLitho.setVisible(False)
			self.groupBoxDisplayIntervals.setVisible(False)		

	def check(self):
		if not super(LoadDetailedTrenches, self).check():
			return

		if 	self._work_mode is None:
			return
		if self._work_mode == (self.ID_LOAD_MODE_SELECT | self.ID_LOAD_MODE_DISPLAY):
			return

		return True
		

	def prepare(self):
		if not super(LoadDetailedTrenches, self).prepare():
			return

		if not self.check():
			return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", str(e))

		self.after_run()
		if self._is_success and self._can_close:
			self.close()

	def run(self):
		if not super(LoadDetailedTrenches, self).run():
			return

		if 	self._work_mode & self.ID_LOAD_MODE_SELECT: 
			self.run_select()
			self._can_close = True
			return

		if 	self._work_mode & self.ID_LOAD_MODE_DISPLAY:
			self.run_display() 
			self._can_close = False
			return

	def run_select(self):
		self._dataload.sort(key = lambda x: x[0])

		netAssayPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRASSAY))
		f = MicromineFile()
		if not f.open(netAssayPath):
			raise Exception("Невозможно открыть файл '%s'" % netAssayPath)

		try:
			data = [row for row in f.read(columns = self._headerAssayImport, asstr = True)]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать файл. Обновите базу данных борозд. %s" % str(e))
			f.close()
			return
		f.close()

		lst_hole_id = list(zip(*self._dataload))[0]
		self._returnedData = list(filter(lambda x: x[0] in lst_hole_id ,data))

	def run_display(self):


		progress = self.main_app.mainWidget.progressBar
		progress.setRange(0,0)

		netAssayPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRASSAY))
		netCollarPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRCOLLAR))
		netSurveyPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRSVY))

		netFilePaths = [netAssayPath, netSurveyPath]

		locAssayPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netAssayPath))
		locCollarPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netCollarPath))
		locSurveyPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netSurveyPath))


		locFilePaths = [locAssayPath, locSurveyPath]

		# Survey
		f = MicromineFile()
		if not f.open(netSurveyPath):
			raise Exception("Невозможно открыть файл '%s'" % netSurveyPath)

		try:
			data = [row for row in f.read(columns = self._headerSurveyImport, asstr = True)]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать файл. Обновите базу данных борозд. %s" % str(e))
			f.close()
			return
		f.close()

		survey_data = []
		if data:
			lst_hole_id = list(zip(*self._dataload))[0]
			survey_data = list(filter(lambda x: x[0] in lst_hole_id ,data))
		# Assay
		f = MicromineFile()
		if not f.open(netAssayPath):
			raise Exception("Невозможно открыть файл '%s'" % netAssayPath)

		try:
			data = [row for row in f.read(columns = self._headerAssayImport, asstr = True)]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать файл. Обновите базу данных борозд. %s" % str(e))
			f.close()
			return
		f.close()

		assay_data = []
		if data:
			lst_hole_id = list(zip(*self._dataload))[0]
			assay_data = list(filter(lambda x: x[0] in lst_hole_id ,data))


		locDhdbPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "TRENCHES.DHDB")

		# создание БД
		try:					dhdb = TrenchesDb.create(locDhdbPath)
		except Exception as e:	raise Exception("Невозможно создать базу данных борозд. %s" % str(e))

		progress.setText("Создание файла траекторий")
		pos_east = self._headerSurveyExport.index("ВОСТОК")
		pos_north = self._headerSurveyExport.index("СЕВЕР")
		pos_rl = self._headerSurveyExport.index("Z")
		pos_point_order = self._headerSurveyExport.index("POINT_ORDER")
		
		for i, row in enumerate(survey_data):
			try:
				survey_data[i][pos_east] = float(survey_data[i][pos_east].replace(",", ".")) if survey_data[i][pos_east] else ""
				survey_data[i][pos_north] = float(survey_data[i][pos_north].replace(",", ".")) if survey_data[i][pos_north] else ""
				survey_data[i][pos_rl] = float(survey_data[i][pos_rl].replace(",", ".")) if survey_data[i][pos_rl] else ""
			except:
				pass

		survey_data.sort(key = lambda x: (x[0], int(x[pos_point_order])))
		MicromineFile.write(locSurveyPath, survey_data, self._headerSurveyExport)

		progress.setText("Создание файла интервалов")

		for i, row in enumerate(assay_data):
			try:
				for j in (2,3,4,5,6,7,8, 14, 15, 16):
					assay_data[i][j] = float(assay_data[i][j].replace(",", ".")) if assay_data[i][j] else None
			except:
				pass

		if len(assay_data) == 0 :
			assay_data.append( ['' for i in self._headerAssayExport])
		assay_data.sort(key = lambda x: x[0])
		MicromineFile.write(locAssayPath, assay_data, self._headerAssayExport)
		
		progress.setText("Создание базы данных борозд")
		
		required_survey_attributes = ["HOLE_ID", "ВОСТОК", "СЕВЕР", "Z", "ГЛУБИНА"]
		un_required_survey_attributes = [x for x in self._headerSurveyExport if x not in required_survey_attributes]  
		dhdb.set_trenches(locSurveyPath, "HOLE_ID", "ВОСТОК", "СЕВЕР", "Z", "", un_required_survey_attributes)
		dhdb.add_intervals(locAssayPath, "HOLE_ID", "FROM", "TO")

		dhdb.save()

		try:						dhdb.refresh()
		except Exception as e:		raise Exception("Невозможно рассчитать траектории скважин")

		dhdb.close()

		progress.setText("Загрузка")

		loadDrillholesDb(locDhdbPath, "Борозды")

		relDbPath = locDhdbPath[len(MMpy.Project.path()):]
		relAssayPath = locAssayPath[len(MMpy.Project.path()):]
		#
		if self.groupBoxDisplayAssay.isChecked():
			# загрузка опробования
			formsetId = self.lineEditAssayForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesHatch(relDbPath, relAssayPath, "Опробование", formsetId)
		#
		if self.groupBoxDisplayLitho.isChecked():
			# загрузка литологиих
			formsetId = self.lineEditLithoForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesHatch(relDbPath, relAssayPath, "Литология", formsetId)
		#
		if self.groupBoxDisplayIntervals.isChecked():
			formsetId = self.lineEditIntervalsForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesValue(relDbPath, relAssayPath, "Метки интервалов", formsetId)

		progress.setText("")
		progress.setRange(0,100)

	def getSelectedSamples(self):
		result = self.get_empty_return_value()
		if self._returnedData is not None:
			result['header'] = self._headerAssayExport
			result['data'] =  self._returnedData
		return result


	def __insertListInPosList(self, list_dest, list_source, position):
		result_list = []
		for i, f in enumerate(list_dest):
			if i == position:
				for e in list_source:
					result_list.append(e)	

			result_list.append(f)
		
		return result_list



