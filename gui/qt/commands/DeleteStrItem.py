from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.osutil import set_file_extension
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow


class DeleteStrItem(DialogWindow):
    def __init__(self, parent, triDbItem, *args, **kw):
        super(DeleteStrItem, self).__init__(parent, *args, **kw)
        self._main = parent
        self._triDbItem = triDbItem
        self.setWindowTitle("Удалить STR файл")

        self.__createVariables()
        self.__createWidgets()
        self.__createBindings()
        self.__gridWidgets()

    def __createVariables(self):
        self._newName = None

    def __createWidgets(self):
        self.layoutParams = QtGui.QVBoxLayout()

        self.labelMessage1 = QtGui.QLabel(self)
        self.labelMessage1.setText(
            "Вы действительно хотите удалить:<br/><b>'%s'</b>?" % (self._triDbItem.name()))
        self.labelMessage1.setMinimumWidth(100)
        self.labelMessage1.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)

    def __createBindings(self):
        self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

    def __gridWidgets(self):
        self.layoutControls.addLayout(self.layoutParams)
        self.layoutParams.addWidget(self.labelMessage1)

    def prepare(self):
        super(DeleteStrItem, self).prepare()

        try:
            self._is_running = True
            self.run()
            self._is_running = False
            self._is_success = True
        except Exception as e:
            self._is_running = False
            self._in_process = False
            qtmsgbox.show_error(self, "Ошибка", "Невозможно удалить файл. %s" % str(e))

        self.after_run()
        if self._is_success:
            self.close()

    def run(self):


        old_path = self._triDbItem.filePath()
        #
        dirpath = os.path.dirname(old_path)

        try:
            # переименовываем файлы
            try:
                os.remove(old_path)
            except Exception as e:
                raise Exception("Невозможно удалить файл '%s'" % str(e))

        except Exception as e:
            raise Exception(str(e))

        # переименовываем в интерфейсе
        try:
            self._main.refreshStandardModels()
            self._main.refreshUserModels()

        except Exception as e:
            raise Exception("Невозможно обновить дерево папок. Перезапустите скрипт. Ошибка: %s" % str(e))

        qtmsgbox.show_information(self, "Выполнено", "Файл успешно удален")
