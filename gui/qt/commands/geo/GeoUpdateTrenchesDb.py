from PyQt4 import QtGui, QtCore
import os
from collections import OrderedDict

from mm.formsets import *
from mm.mmutils import MicromineFile, TrenchesDb

from utils.constants import *
from utils.blockmodel import BlockModel
from utils.osutil import get_temporary_prefix

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import ProcessWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GeoUpdateTrenchesDb(ProcessWindow):
	def __init__(self, *args, **kw):
		super(GeoUpdateTrenchesDb, self).__init__(*args, **kw)

	def prepare(self):
		if not super(GeoUpdateTrenchesDb, self).prepare():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))

		self.after_run()

	def run(self):
		super(GeoUpdateTrenchesDb, self).run()

		progress = self.main_app.mainWidget.progressBar

		progress.setRange(0,0)
		
		netCollarPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRCOLLAR))
		netAssayPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRASSAY))
		netSurveyPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_TRSVY))
		netFilePaths = [netCollarPath, netSurveyPath, netAssayPath]

		dbdirpath = MMpy.Project.path() + "БД"
		dbfilespath = os.path.join(dbdirpath, "Файлы бороздового опробования")
		if not os.path.exists(dbdirpath):
			try:						os.mkdir(dbdirpath)
			except Exception as e:		raise Exception("Невозможно создать директорию '%s'" % dbdirpath)
		if not os.path.exists(dbfilespath):
			try:						os.mkdir(dbfilespath)
			except Exception as e:		raise Exception("Невозможно создать директорию '%s'" % dbfilespath)

		locCollarPath = os.path.join(dbfilespath, os.path.basename(netCollarPath))
		locAssayPath = os.path.join(dbfilespath, os.path.basename(netAssayPath))
		locSurveyPath = os.path.join(dbfilespath, os.path.basename(netSurveyPath))
		locFilePaths = [locCollarPath, locSurveyPath, locAssayPath]

		locDhdbPath = os.path.join(dbdirpath, "TRENCHES.DHDB")
		rptpath = os.path.join(self._tmpfolder, get_temporary_prefix() + "REPORT_CALCCOORDS.RPT")


		### =================================================================================== ###
		### ================================ СЧИТЫВАНИЕ УСТЬЕВ ================================ ###
		### =================================================================================== ###

		progress.setText("Считывание координат")
		cmd = """
			SELECT DISTINCT 
				[TR_ID], 
				CONVERT (FLOAT, [LOCATIONX]) [XCOLLAR], 
				CONVERT (FLOAT, [LOCATIONY]) [YCOLLAR], 
				CONVERT (FLOAT, [LOCATIONZ]) [ZCOLLAR], 
				CONVERT (FLOAT, [DEPTH]) [DEPTH],
				[DEPOSIT],
				[LAYER],
				[UNIT],
				[SECTION],
				[MINE_NAME],
				CONVERT (DATE, [START_DATE]) [START_DATE],
				CONVERT (DATE, [END_DATE]) [END_DATE]
			FROM [{0}].[dbo].[MM_TRENCH] ORDER BY [TR_ID]
		""".format(self.main_app.srvcnxn.dbgeo)
		collar_data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		collar_header = self.main_app.srvcnxn.cursor_description()
		collar_structure = ("TR_ID:C:50:0",  "XCOLLAR:R:0:2", "YCOLLAR:R:0:2", "ZCOLLAR:R:0:2", "DEPTH:R:0:2",
					   "DEPOSIT:C:50:0", "LAYER:C:50:0", "UNIT:C:50:0", "SECTION:C:50:0", "MINE_NAME:C:50:0", 
					  "START_DATE:C:20:0", "END_DATE:C:20:0")
		MicromineFile.write(locCollarPath, collar_data, collar_header, MicromineFile.to_filestruct(collar_structure))

		### =================================================================================== ###
		### ============================ СЧИТЫВАНИЕ ИНКЛИНОМЕТРИИ ============================= ###
		### =================================================================================== ###

		progress.setText("Считывание инклинометрии")
		cmd = """
			SELECT DISTINCT 
				[TR_ID], 
				CONVERT (INTEGER, [POINT_ORDER]) [POINT_ORDER],
				CONVERT (INTEGER, [JOIN]) [JOIN],
				CONVERT (FLOAT, [XCOLLAR]) [XCOLLAR], 
				CONVERT (FLOAT, [YCOLLAR]) [YCOLLAR], 
				CONVERT (FLOAT, [ZCOLLAR]) [ZCOLLAR], 
				[DEPOSIT],
				[LAYER],
				[MINE_NAME]
			FROM [{0}].[dbo].[MM_TRENCH_COORDS] ORDER BY [TR_ID]
		""".format(self.main_app.srvcnxn.dbgeo)
		survey_data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		survey_header = self.main_app.srvcnxn.cursor_description()
		survey_structure = ("TR_ID:C:50:0", "POINT_ORDER:N:6:0", "JOIN:N:6:0",  "XCOLLAR:R:0:2", "YCOLLAR:R:0:2", "ZCOLLAR:R:0:2",
					   "DEPOSIT:C:50:0", "LAYER:C:50:0", "MINE_NAME:C:50:0",)
		MicromineFile.write(locSurveyPath, survey_data, survey_header, MicromineFile.to_filestruct(survey_structure))


		### =================================================================================== ###
		### ============================== СЧИТЫВАНИЕ ОПРОБОВАНИЯ ============================= ###
		### =================================================================================== ###

		idw_elems = self.main_app.settings.getValue(ID_IDWELEM)
		elems = list(zip(*idw_elems))[0]
		lst_elems_sql_field = ["CONVERT (FLOAT, [{0}]) [{0}]".format(e) for e in elems]
		str_elems_sql_field = ",".join(lst_elems_sql_field)
		progress.setText("Считывание опробования")
		cmd = """
			SELECT DISTINCT 
				[TR_ID], 
				CONVERT (INTEGER, [SAMPLE_ID]) [SAMPLE_ID], 
				CONVERT (FLOAT, [FROM]) [FROM], 
				CONVERT (FLOAT, [TO]) [TO], 
				CONVERT (FLOAT, [LENGTH]) [LENGTH], 
				[ROCK], 
				[INDEX], 
				[ORE_INDEX], 
				CONVERT (FLOAT, [CORE_REC]) [CORE_REC],
				[DEPOSIT],
		"""
		cmd = cmd +  str_elems_sql_field

		cmd = cmd +	"""
			FROM [{0}].[dbo].[MM_ASSAY_TRENCH]
			ORDER BY TR_ID, [FROM]
		""".format(self.main_app.srvcnxn.dbgeo)

		assay_data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		assay_header = self.main_app.srvcnxn.cursor_description()
		assay_structure = OrderedDict([("TR_ID:C:50:0",0), ("SAMPLE_ID:L:0:0",0), ("FROM:R:0:1",0), ("TO:R:0:1",0), ("LENGTH:R:0:6",0), 
								("ROCK:C:10:0",0), ("INDEX:C:10:0",0), ("ORE_INDEX:C:30:0",0), ("CORE_REC:R:0:2",0),  ("DEPOSIT:C:50:0",0)])
		for e in elems:
			precision = 2
			if e == 'Co':
				precision = 3
			elif e == 'Os':
				precision = 0
			key = "{0}:R:0:{1}".format(e, precision)
			assay_structure[key] = 0
			assay_structure.move_to_end(key)
		
		assay_structure = assay_structure.keys()

		MicromineFile.write(locAssayPath, assay_data, assay_header, MicromineFile.to_filestruct(assay_structure))
		
		### =================================================================================== ###
		##№ ============================= ПРОВЕРКА ЦЕЛОСТНОСТИ БД ============================= ###
		### =================================================================================== ###

		trdb = TrenchesDb.create(locDhdbPath)
		trdb.set_trenches(locSurveyPath, "TR_ID", "XCOLLAR", "YCOLLAR", "ZCOLLAR", "")
		#trdb.set_survey(locSurveyPath, "TR_ID", "DEPTH", "AZ", "DIP")
		trdb.add_intervals(locAssayPath, "TR_ID", "FROM", "TO")

		trdb.save()

		progress.setText("Проверка целостности БД")
		try:
			trdb.refresh()
		except Exception as e:
			print ("Ошибка", str(e))

		has_traces = trdb.has_traces
		trdb.close()

		progress.setText("")
		progress.setRange(0, 100)

		if not has_traces:
			qtmsgbox.show_error(self.main_app, "Ошибка", "База не была обновлена, так как содержит ошибки")
		else:
			relDhdbPath = locDhdbPath[len(MMpy.Project.path()):]
			relAssayPath = locAssayPath[len(MMpy.Project.path()):]
			# расчет координат
			progress.setText("Расчет координат опробования")
			#
			P3dcoords_FormSet1= MMpy.FormSet("3DCOORDS","16.1.89.0")
			P3dcoords_FormSet1.set_field("SWITCH","0")
			P3dcoords_FormSet1.set_field("FIELDS","1")
			P3dcoords_FormSet1.set_field("TYPE","0")
			P3dcoords_FormSet1.set_field("RL","ZCOLLAR")
			P3dcoords_FormSet1.set_field("Y","YCOLLAR")
			P3dcoords_FormSet1.set_field("X","XCOLLAR")
			P3dcoords_FormSet1.set_field("T_FILE", relAssayPath)
			P3dcoords_FormSet1.set_field("BOOL","0")
			P3dcoords_FormSet1.set_field("3D_FILE", relDhdbPath)
			P3dcoords_FormSet1.set_field("INT","0")
			P3dcoords_FormSet1.set_field("REPFILE", rptpath, MMpy.append_flag.none)
			P3dcoords_FormSet1.run()

			# копирование файлов на сервер
			progress.setText("Копирование файлов на сервер")
			for locpath, netpath in zip(locFilePaths, netFilePaths):
				if locpath.lower() == netpath.lower():
					continue
				err = self.exec_proc(self.copy_file, locpath, netpath)
				if err:					raise Exception("Невозможно скопировать файл %s. %s" % (locpath, err))
			qtmsgbox.show_information(self.main_app, "Выполнено", "База данных ,борозд успешно обновлена")
