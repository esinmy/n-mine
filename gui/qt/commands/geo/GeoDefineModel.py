from PyQt4 import QtGui, QtCore
import os

from mm.mmutils import Tridb, MicromineFile

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.blockmodel import BlockModel

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GeoDefineModel(DialogWindow):
	def __init__(self, *args, **kw):
		super(GeoDefineModel, self).__init__(*args, **kw)

		self.setWindowTitle("Определить модель")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.inputLayout = QtGui.QFormLayout()

		self.labelLayer = QtGui.QLabel(self.groupBoxInput)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLayer.setMinimumWidth(100)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxLayer.setMinimumWidth(200)

		self.labelGeoUnit = QtGui.QLabel(self.groupBoxInput)
		self.labelGeoUnit.setText(self.main_app.settings.getValue(ID_GEO_UDSPNAME))
		self.labelGeoUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelGeoUnit.setMinimumWidth(100)
		self.comboBoxGeoUnit = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxGeoUnit.setMinimumWidth(200)

		self.labelSuffix = QtGui.QLabel(self.groupBoxInput)
		self.labelSuffix.setText("Суффикс")
		self.labelSuffix.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSuffix.setMinimumWidth(100)
		self.lineEditSuffix = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditSuffix.setMinimumWidth(200)

	def __createBindings(self):
		self.comboBoxLayer.currentIndexChanged.connect(self.layerChanged)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelGeoUnit)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxGeoUnit)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSuffix)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditSuffix)

	#
	def buildCombos(self):
		self.buildLayerList()
		self.buildGeoUnitList()

	def buildLayerList(self):
		self.comboBoxLayer.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT LAYER_DESCRIPTION 
			FROM [{0}].[dbo].[LAYER] 
			WHERE LOCATION = ?
			ORDER BY LAYER_DESCRIPTION
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		layers = [""] + [row[0] for row in data]
		self.comboBoxLayer.addItems(layers)
	def buildGeoUnitList(self):
		self.comboBoxGeoUnit.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		layerName = self.comboBoxLayer.currentText()
		cmd = """
				SELECT PANEL_NAME FROM [{0}].[dbo].[PANEL] 
				WHERE LAYER_ID = (
					SELECT LAYER_ID FROM [{0}].[dbo].[LAYER] 
					WHERE LAYER_DESCRIPTION = ?
					AND LOCATION = ?
				)""".format(self.main_app.srvcnxn.dbsvy)
		params = (layerName, locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		geoUnits = [""] + [row[0] for row in data]
		self.comboBoxGeoUnit.addItems(geoUnits)

	#
	def layerChanged(self):
		self.buildGeoUnitList()
	#
	def prepare(self):
		if not super(GeoDefineModel, self).prepare():
			return

		labels = [self.labelLayer, self.labelGeoUnit]
		fields = [self.comboBoxLayer, self.comboBoxGeoUnit]

		for label, field in zip(labels, fields):
			if not getWidgetValue(field):
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				return

		layerName = self.comboBoxLayer.currentText()
		geoUnitName = self.comboBoxGeoUnit.currentText()
		suffix = self.lineEditSuffix.text()
		prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		geodir = netpath + "\\" + self.main_app.settings.getValue(ID_GEO_MAINDIR)
		layerDir = geodir + "\\" + layerName
		geoUnitDir = layerDir + "\\" + prefix + geoUnitName
		bmdir = geoUnitDir + "\\" + self.main_app.settings.getValue(ID_GEO_USUBDIRS)[0][1]
		tridir = geoUnitDir + "\\" + self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1]

		filename = "_".join([layerName, prefix + geoUnitName])
		filename = filename + suffix if suffix else filename

		bmpath = bmdir + "\\%s.DAT" % filename
		str_bounds_path = bmdir + "\\%s.STR" % filename
		triGeoPath = tridir + "\\%s.TRIDB" % filename

		if os.path.exists(str_bounds_path) and os.path.exists(triGeoPath):
			if qtmsgbox.show_question(self, "Внимание", "Модель '%s' уже определена. Заменить?" % filename):
				os.remove(str_bounds_path)
				os.remove(triGeoPath)
			else: return

		if os.path.exists(triGeoPath):
			if qtmsgbox.show_question(self, "Внимание", "Каркасы '%s' уже существуют. Заменить?" % filename):
				os.remove(triGeoPath)
			else: return

		if os.path.exists(str_bounds_path):
			if qtmsgbox.show_question(self, "Внимание", "Стринги '%s' уже существуют. Заменить?" % filename):
				os.remove(str_bounds_path)
			else: return

		bm = BlockModel(bmpath, self.main_app)
		if bm.is_defined:
			if not qtmsgbox.show_question(self, "Внимание", "Блочная модель '%s' уже определена. Заменить имеющиеся данные?" % filename):
				return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно определить модель. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(GeoDefineModel, self).run():
			return

		layerName = self.comboBoxLayer.currentText()
		geoUnitName = self.comboBoxGeoUnit.currentText()
		suffix = self.lineEditSuffix.text()
		prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		geodir = netpath + "\\" + self.main_app.settings.getValue(ID_GEO_MAINDIR)
		layerDir = geodir + "\\" + layerName
		geoUnitDir = layerDir + "\\" + prefix + geoUnitName

		bmdir = geoUnitDir + "\\" + self.main_app.settings.getValue(ID_GEO_USUBDIRS)[0][1]
		tridir = geoUnitDir + "\\" + self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1]

		filename = "_".join([layerName, prefix + geoUnitName])
		filename = filename + suffix if suffix else filename



		structure = ["EAST:R:0:6", "NORTH:R:0:6", "RL:R:0:6", "JOIN:N:6:0"]
		str_bounds_path = bmdir + "\\%s.STR" % filename
		triGeoPath = tridir + "\\%s.TRIDB" % filename
		try:					MicromineFile.create(str_bounds_path, structure)
		except Exception as e:	raise Exception("Невозможно создать файл границ. %s" % str(e))
		#
		geo_user_attributes = list(GEO_USER_ATTRIBUTES)
		geo_user_attributes.insert(1, self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper())
		try:					Tridb.create(triGeoPath, geo_user_attributes)
		except Exception as e:	raise Exception("Невозможно создать файл геологических каркасов. %s" % str(e))

		self.main_app.refreshStandardModels()
		qtmsgbox.show_information(self, "Выполнено", "Модель '%s' успешно определена" % filename)
