from PyQt4 import QtGui, QtCore
import os

from mm.mmutils import Tridb, MicromineFile

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.blockmodel import BlockModel
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def show_twoanswer_question(parent, title, message):
	msgbox = QtGui.QMessageBox()
	msgbox.setMinimumSize(QtCore.QSize(200, 200))

	msgbox.setWindowTitle(title)
	msgbox.setText(message)

	msgbox.setIcon(QtGui.QMessageBox.Question)
	continue_ = msgbox.addButton("Продолжить", QtGui.QMessageBox.YesRole)
	use = msgbox.addButton("Использовать", QtGui.QMessageBox.NoRole)
	cancel = msgbox.addButton("Отмена", QtGui.QMessageBox.Cancel)
	msgbox.exec()

	if msgbox.clickedButton() == continue_:	return "continue"
	elif msgbox.clickedButton() == use:		return "use"
	else:									return None

class GeoRenameModel(DialogWindow):
	def __init__(self, *args, **kw):
		super(GeoRenameModel, self).__init__(*args, **kw)

		self.setWindowTitle("Переименовать модель")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._modelItem = None
	def __createWidgets(self):
		self.layoutParams = QtGui.QFormLayout()

		self.labelName = QtGui.QLabel(self)
		self.labelName.setText("Старое имя")
		self.labelName.setMinimumWidth(100)
		self.labelName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditName = QtGui.QLineEdit(self)
		self.lineEditName.setMinimumWidth(200)
		self.lineEditName.setReadOnly(True)
		self.labelSuffix = QtGui.QLabel(self)
		self.labelSuffix.setText("Новое имя")
		self.labelSuffix.setMinimumWidth(100)
		self.labelSuffix.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditSuffix = QtGui.QLineEdit(self)
		self.lineEditSuffix.setMinimumWidth(200)

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelName)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditName)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSuffix)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditSuffix)

	def setModelItem(self, bmItem):
		filepath = bmItem.filePath()
		#
		self._modelItem = bmItem
		blockmodel = BlockModel(filepath, self.main_app)

		displayName = "_".join([blockmodel.layer, blockmodel.unit])
		suffix = blockmodel.name[len(displayName):]

		self.lineEditName.setText(displayName)
		self.lineEditSuffix.setText(suffix)

	def check(self):
		if is_blank(self.lineEditSuffix.text()):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelSuffix.text())
			return

		name = self.lineEditName.text() + self.lineEditSuffix.text()
		path = self._modelItem.filePath()
		dirpath = os.path.dirname(path)
		newpath = os.path.join(dirpath, name + ITEM_DATA)

		if os.path.exists(newpath):
			qtmsgbox.show_error(self, "Ошибка", "Модель '%s' уже существует" % os.path.basename(newpath))
			return

		return True

	def prepare(self):
		super(GeoRenameModel, self).prepare()

		if not self.check():
			return

		# подключение к БД, если необходимо
		if self.main_app.srvcnxn is None:
			if self.main_app.connect_to_database() is None:
				return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно переименовать модель. %s" % str(e))

		self.after_run()
		if self._is_success:
			self.close()

	def run(self):
		def restore_filenames():
			nonlocal old_bm, new_bm
			if not os.path.exists(old_bm.path):
				try:		os.rename(new_bm.path, old_bm.path)
				except:		pass
			if not os.path.exists(old_bm.str_bounds_path):
				try:		os.rename(new_bm.str_bounds_path, old_bm.str_bounds_path)
				except:		pass
			if not os.path.exists(old_bm.tri_wireframes_path):
				try:		os.rename(new_bm.tri_wireframes_path, old_bm.tri_wireframes_path)
				except:		pass
		#
		oldBmPath = self._modelItem.filePath()
		old_bm = BlockModel(oldBmPath, self.main_app)
		#
		newBmName = self.lineEditName.text() + self.lineEditSuffix.text()
		newBmPath = os.path.join(os.path.dirname(oldBmPath), newBmName + ITEM_DATA)
		new_bm = BlockModel(newBmPath, self.main_app)
	
		if new_bm.has_metadata:
			message = "В базе данных найдена запись, соответствующая модели '%s'.\n"
			message += "По умолчанию метаданные будут удалены. Чтобы использовать их для выбранной модели, нажмите 'Использовать'."
			reply = show_twoanswer_question(self, "Внимание", message)
			#
			if not reply:				return
			if reply == "continue":		new_bm.remove()

		#
		try:
			# переименовываем файлы
			try:
				os.rename(oldBmPath, newBmPath)
				os.rename(old_bm.str_bounds_path, new_bm.str_bounds_path)
				os.rename(old_bm.tri_wireframes_path, new_bm.tri_wireframes_path)
			except Exception as e:
				raise Exception("Невозможно переименовать файл '%s'" % str(e))
			
			print (newBmName)
			print (old_bm.metadata)
			if old_bm.has_metadata:
				# переименовываем блочную модуль в метаданных
				try:
					old_bm.metadata.name = newBmName
					old_bm.metadata.path = newBmPath
				except Exception as e:
					raise Exception("Невозможно изменить метаданные. %s" % str(e))
				# сохраняем метаданные
				try:
					old_bm.save()
				except Exception as e:
					raise Exception("Невозможно сохранить метаданные")
		except Exception as e:
			# возвращаем имена, если требуется
			restore_filenames()
			raise Exception(str(e))

		# переименовываем в интерфейсе
		try:
			self._modelItem.setText(0, newBmName)
			self._modelItem.setFilePath(newBmPath)
			for child in self._modelItem.iterChildren():
				if child.isBlockModel() and child.filePath() == oldBmPath:
					child.setFilePath(newBmPath)
				elif child.isString() and child.filePath() == old_bm.str_bounds_path:
					child.setFilePath(new_bm.str_bounds_path)
				elif child.isTridb() and child.filePath() == old_bm.tri_wireframes_path:
					child.setFilePath(new_bm.tri_wireframes_path)
		except Exception as e:
			raise Exception("Невозможно обновить дерево моделей. Перезапустите скрипт. Ошибка: %s" % str(e))
	
		qtmsgbox.show_information(self, "Выполнено", "Модель успешно переименована")
