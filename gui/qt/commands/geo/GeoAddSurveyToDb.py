try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os

from mm.mmutils import Tridb
from mm.formsets import copy_wireframe

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.osutil import get_current_time
from utils.validation import DoubleValidator
from utils.validation import is_blank


import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import  CustomDataModel


from gui.qt.widgets.ShapeFilterWidget import StringsFile


from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class GeoAddSurveyToDb(DialogWindow):
	def __init__(self, *args, **kw):
		super(GeoAddSurveyToDb, self).__init__(*args, **kw)

		self.setWindowTitle("Копировать стринг в траектории борозды на сервер")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createVariables(self):
			self._selected_trench_id = None
			self._selected_trench_join = None
			self._selected_trench_num_surveys = None
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.inputLayout = QtGui.QFormLayout()

		self.labelStringFile = QtGui.QLabel(self.groupBoxInput)
		self.labelStringFile.setText("Файл линии")
		self.labelStringFile.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelStringFile.setMinimumWidth(70)
		self.lineEditStringFile = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditStringFile.setMinimumWidth(300)	
		self.lineEditStringFile.setReadOnly(True)
			

		self.labelStringObject = QtGui.QLabel(self.groupBoxInput)
		self.labelStringObject.setText("Линия")
		self.labelStringObject.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelStringObject.setMinimumWidth(70)
		self.lineEditStringObject = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditStringObject.setMinimumWidth(300)
		self.lineEditStringObject.setReadOnly(True)



		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.outputLayout = QtGui.QFormLayout()

		self.labelOutTrench = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTrench.setText("Борозда")
		self.labelOutTrench.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTrench.setMinimumWidth(70)
		self.labelOutTrench.setMaximumWidth(70)
		self.lineEditOutTrench = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutTrench.setMinimumWidth(300)
		self.lineEditOutTrench.setReadOnly(True)

	def __createBindings(self):

		self.lineEditStringFile.mouseDoubleClickEvent = lambda e: self.select_string(e, self.lineEditStringFile)
		self.lineEditStringObject.mouseDoubleClickEvent = lambda e: self.select_shape(e, self.lineEditStringFile, self.lineEditStringObject)
		self.lineEditOutTrench.mouseDoubleClickEvent = lambda e: self.selectTrench()
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelStringFile)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditStringFile)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelStringObject)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditStringObject)

		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.outputLayout)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTrench)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutTrench)


	#
	def buildCombos(self): pass

	def buildLayerList(self):
		self.comboBoxAttrLayer.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT LAYER_DESCRIPTION 
			FROM [{0}].[dbo].[LAYER] 
			WHERE LOCATION = ?
			ORDER BY LAYER_DESCRIPTION
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список залежей. %s" % str(e))
			return
		layers = [""] + [row[0] for row in data]
		self.comboBoxAttrLayer.addItems(layers)
	def buildGeoUnitList(self):
		self.comboBoxAttrGeoUnit.clear()
		#
		layerName = self.comboBoxAttrLayer.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		if not layerId:
			return

		cmd = """
			SELECT PANEL_NAME 
			FROM [{0}].[dbo].[PANEL] 
			WHERE LAYER_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (layerId)
		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список панелей. %s" % str(e))
			return
		geoUnitNames = [""] + [row[0] for row in data]
		self.comboBoxAttrGeoUnit.addItems(geoUnitNames)

	


	#
	def select_string(self, e, widget):
		filetypes = "ЛИНИИ (*.STR)"
		try:		initialdir = MMpy.Project.path()
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		# ничего не выбрано
		if not path:
			return

		widget.setText(path)

	def select_shape(self, e, filewidget, widget):

		filepath = filewidget.text()
		if not os.path.exists(filepath):
			qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не существует" % filepath)
			return

		if os.path.splitext(filepath)[1].upper() == ".STR":
			strfile = StringsFile(filepath)
			header = strfile.getDataHeader()
			must_be_fields = ["STRING", "EAST", "NORTH", "RL", "JOIN"]
			miss_fields = [ i for i in must_be_fields if i not in header]
			if len(miss_fields) > 0:
				qtmsgbox.show_error(self, "Ошибка", "Файл линий не содержит одно или несколько \n обязательных полей: %s" % ",".join(miss_fields))
				return



			strings = strfile.getStrings()
			dialog = SelectFromListDialog(self)
			dialog.setValues(strings)
			dialog.setWindowTitle("Выберите линию")
			dialog.labelTitle.setText("Комбинации полей (STRING:JOIN)")
			dialog.exec()
			result = dialog.getValues()
			if result:
				widget.setText(result[0])


	def selectTrench(self):
		cmd = """
			SELECT DISTINCT 
				[TR_ID], 
				CONVERT (FLOAT, [LOCATIONX]) [XCOLLAR], 
				CONVERT (FLOAT, [LOCATIONY]) [YCOLLAR], 
				CONVERT (FLOAT, [LOCATIONZ]) [ZCOLLAR], 
				CONVERT (FLOAT, [DEPTH]) [DEPTH],
				[DEPOSIT],
				[LAYER],
				[UNIT],
				[SECTION],
				[MINE_NAME],
				CONVERT (DATE, [START_DATE]) [START_DATE],
				CONVERT (DATE, [END_DATE]) [END_DATE],
				[JOIN],
				(SELECT COUNT(*) FROM [{0}].[dbo].[MM_TRENCH_COORDS] AS s WHERE s.TR_ID = c.TR_ID ) as NUM_SURVEYS

			FROM [{0}].[dbo].[MM_TRENCH] AS c ORDER BY [TR_ID]
		""".format(self.main_app.srvcnxn.dbgeo)

		trench_data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		trench_header = self.main_app.srvcnxn.cursor_description()

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Борозды")
		model = CustomDataModel(dialog.view, trench_data, trench_header)
		dialog.view.setModel(model)
		dialog.view.hideColumn(trench_header.index("JOIN"))
		dialog.view.hideColumn(trench_header.index("NUM_SURVEYS"))
		dialog.view.setColumnWidth(0, 200)
		dialog.resize(1000, 480)
		dialog.exec()

		result = dialog.getResult()
		if result:
			self._selected_trench_id = result[0][0]
			self._selected_trench_join = result[0][trench_header.index("JOIN")]
			self._selected_trench_num_surveys = result[0][trench_header.index("NUM_SURVEYS")]
			self.lineEditOutTrench.setText(self._selected_trench_id)

		
	#
	def prepare(self):
		if not super(GeoAddSurveyToDb, self).prepare():
			return

		try:

			_fileName = self.lineEditStringFile.text()
			_shapeName = self.lineEditStringObject.text()		

			if not os.path.exists(_fileName):
				qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не найден" % _fileName)
				return
			if is_blank(_shapeName):
				qtmsgbox.show_error(self, "Ошибка", "Не выбран объект линии из файла, загрузка всех линий из файла отключена для данной процедуры")
				return

			if self._selected_trench_id is None or self._selected_trench_join is None:
				qtmsgbox.show_error(self, "Ошибка", "Не выбранны данные борозды. Выполнение не возможно")
				return

		
			if self._selected_trench_num_surveys > 0:
				if  not qtmsgbox.show_question(self, "Существует траектория", "Переписать существующую в БД траекторию?"):
					return

		#
		
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно добавить траекторию борозды на сервер. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(GeoAddSurveyToDb, self).run():
			return
		_fileName = self.lineEditStringFile.text()
		_shapeName = self.lineEditStringObject.text()
		strfile = StringsFile(_fileName)
		strHeader = strfile.getDataHeader()
		strData = strfile.getData([_shapeName])

		if len(strData) == 0:
			raise Exception ("Файл линий не содержит данных соответствующих выбранному объекту")
			


		cmd_delete = """
			DELETE
			FROM [{0}].[dbo].[TrenchCoord] 
		    WHERE [TR_ID] = ?
		""".format(self.main_app.srvcnxn.dbgeo)
		params_delete = (self._selected_trench_id)

		cmd_insert = """
			INSERT INTO [{0}].[dbo].[TrenchCoord] (TR_ID, POINT_ORDER, EAST, NORTH, RL, [JOIN] )
		    VALUES (?, ?, ?, ?, ?, ?)
		""".format(self.main_app.srvcnxn.dbgeo)

		index_EAST = strHeader.index("EAST")
		index_NORTH = strHeader.index("NORTH")
		index_RL = strHeader.index("RL")
		params_insert = [[self._selected_trench_id, i, r[index_EAST], r[index_NORTH], r[index_RL], self._selected_trench_join] for i, r in enumerate(strData) ]

		self.main_app.srvcnxn.exec_query(cmd_delete, params_delete)
		self.main_app.srvcnxn.exec_query_many(cmd_insert, params_insert)
		self.main_app.srvcnxn.commit()
		qtmsgbox.show_information(self, "Выполнено", "В борозу {0} успешно добавлена {1} сегментная траектория".format(self._selected_trench_id, len(strData)-1))