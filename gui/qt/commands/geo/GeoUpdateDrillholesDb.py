from PyQt4 import QtGui, QtCore
import os
from collections import OrderedDict

from mm.formsets import *
from mm.mmutils import MicromineFile, DrillholesDb

from utils.constants import *
from utils.blockmodel import BlockModel
from utils.osutil import get_temporary_prefix

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import ProcessWindow

import xlsxwriter as xl
from xlsxwriter.utility import xl_range, xl_rowcol_to_cell, xl_col_to_name
from utils.dh_checking.DhChecking import DhChecking 

from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GeoUpdateDrillholesDb(ProcessWindow):
	def __init__(self, *args, **kw):
		super(GeoUpdateDrillholesDb, self).__init__(*args, **kw)

	def prepare(self):
		if not super(GeoUpdateDrillholesDb, self).prepare():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))

		self.after_run()

	def run(self):
		super(GeoUpdateDrillholesDb, self).run()

		progress = self.main_app.mainWidget.progressBar

		progress.setRange(0,0)
		
		netCollarPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPCOLLAR))
		netAssayPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPASSAY))
		netSurveyPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPSVY))
		netFilePaths = [netCollarPath, netAssayPath, netSurveyPath]

		dbdirpath = MMpy.Project.path() + "БД"
		dbfilespath = os.path.join(dbdirpath, "Файлы эксплоразведки")
		if not os.path.exists(dbdirpath):
			try:						os.mkdir(dbdirpath)
			except Exception as e:		raise Exception("Невозможно создать директорию '%s'" % dbdirpath)
		if not os.path.exists(dbfilespath):
			try:						os.mkdir(dbfilespath)
			except Exception as e:		raise Exception("Невозможно создать директорию '%s'" % dbfilespath)

		locCollarPath = os.path.join(dbfilespath, os.path.basename(netCollarPath))
		locAssayPath = os.path.join(dbfilespath, os.path.basename(netAssayPath))
		locSurveyPath = os.path.join(dbfilespath, os.path.basename(netSurveyPath))
		locFilePaths = [locCollarPath, locAssayPath, locSurveyPath]

		locDhdbPath = os.path.join(dbdirpath, "DRILLHOLES_EXPL.DHDB")
		rptpath = os.path.join(self._tmpfolder, get_temporary_prefix() + "REPORT_CALCCOORDS.RPT")


		### =================================================================================== ###
		### ================================ СЧИТЫВАНИЕ УСТЬЕВ ================================ ###
		### =================================================================================== ###

		progress.setText("Считывание координат")
		cmd = """
			SELECT DISTINCT 
				[HOLE_ID], 
				[DEPOSIT], 
				[LAYER],
				[SECTION],
				CONVERT (FLOAT, [XCOLLAR]) [XCOLLAR], 
				CONVERT (FLOAT, [YCOLLAR]) [YCOLLAR], 
				CONVERT (FLOAT, [ZCOLLAR]) [ZCOLLAR], 
				CONVERT (FLOAT, [DEPTH]) [DEPTH],
				[START_DATE], 
				[END_DATE], 
				[MINE_NAME], 
				CONVERT (FLOAT, [UNIT]) [UNIT]
			FROM [{0}].[dbo].[MM_COLLAR_EXPL] ORDER BY [HOLE_ID]
		""".format(self.main_app.srvcnxn.dbgeo)
		collar_data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		collar_header = self.main_app.srvcnxn.cursor_description()
		collar_structure = ("HOLE_ID:C:20:0", "DEPOSIT:C:20:0", "LAYER:C:22:0", "SECTION:C:15:0", "XCOLLAR:R:0:2", "YCOLLAR:R:0:2", "ZCOLLAR:R:0:2", "DEPTH:R:0:2",
								"START_DATE:C:20:0", "END_DATE:C:20:0",  "MINE_NAME:C:20:0", "UNIT:L:0:0")
		MicromineFile.write(locCollarPath, collar_data, collar_header, MicromineFile.to_filestruct(collar_structure))

		### =================================================================================== ###
		### ============================ СЧИТЫВАНИЕ ИНКЛИНОМЕТРИИ ============================= ###
		### =================================================================================== ###

		progress.setText("Считывание инклинометрии")
		cmd = """
			SELECT DISTINCT 
				[HOLE_ID], 
				[DEPOSIT], 
				CONVERT (FLOAT, [DEPTH]) [DEPTH], 
				CONVERT (FLOAT, [AZ]) [AZ], 
				CONVERT (FLOAT, [DIP]) [DIP]
			FROM [{0}].[dbo].[MM_SURVEY_EXPL]
			ORDER BY [HOLE_ID], [DEPTH]
		""".format(self.main_app.srvcnxn.dbgeo)
		survey_data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		survey_header = self.main_app.srvcnxn.cursor_description()
		survey_structure = ("HOLE_ID:C:20:0", "DEPOSIT:C:20:0", "DEPTH:R:0:6", "AZ:R:0:6", "DIP:R:0:6")
		MicromineFile.write(locSurveyPath, survey_data, survey_header, MicromineFile.to_filestruct(survey_structure))


		### =================================================================================== ###
		### ============================== СЧИТЫВАНИЕ ОПРОБОВАНИЯ ============================= ###
		### =================================================================================== ###

		idw_elems = self.main_app.settings.getValue(ID_IDWELEM)
		elems = list(zip(*idw_elems))[0]
		lst_elems_sql_field = ["CONVERT (FLOAT, [{0}]) [{0}]".format(e) for e in elems]
		str_elems_sql_field = ",".join(lst_elems_sql_field)
		progress.setText("Считывание опробования")
		cmd = """
			SELECT DISTINCT 
				[HOLE_ID], 
				CONVERT (INTEGER, [SAMPLE_ID]) [SAMPLE_ID], 
				CONVERT (FLOAT, [FROM]) [FROM], 
				CONVERT (FLOAT, [TO]) [TO], 
				CONVERT (FLOAT, [LENGTH]) [LENGTH], 
				[ROCK], 
				[INDEX], 
				[ORE_INDEX], 
				CONVERT (FLOAT, [CORE_REC]) [CORE_REC],
				[SORT],
				[DEPOSIT], 
				CONVERT (FLOAT, [USL_Ni]) [USL_Ni], 
		"""
		cmd = cmd +  str_elems_sql_field

		cmd = cmd +	"""
			FROM [{0}].[dbo].[MM_ASSAY_EXPL]
			ORDER BY HOLE_ID, [FROM]
		""".format(self.main_app.srvcnxn.dbgeo)

		assay_data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		assay_header = self.main_app.srvcnxn.cursor_description()
		assay_structure = OrderedDict([("HOLE_ID:C:20:0",0), ("SAMPLE_ID:L:0:0",0), ("FROM:R:0:2",0), ("TO:R:0:2",0), ("LENGTH:R:0:6",0), 
								("ROCK:C:10:0",0), ("INDEX:C:10:0",0), ("ORE_INDEX:C:30:0",0), ("CORE_REC:R:0:2",0), ("SORT:C:10:0",0), ("DEPOSIT:C:20:0",0), ("USL_Ni:R:0:2",0)])
		for e in elems:
			precision = 2
			if e == 'Co':
				precision = 3
			elif e == 'Os':
				precision = 0
			key = "{0}:R:0:{1}".format(e, precision)
			assay_structure[key] = 0
			assay_structure.move_to_end(key)
		
		assay_structure = assay_structure.keys()

		MicromineFile.write(locAssayPath, assay_data, assay_header, MicromineFile.to_filestruct(assay_structure))

		### =================================================================================== ###
		##№ ============================= ПРОВЕРКА ЦЕЛОСТНОСТИ БД ============================= ###
		### =================================================================================== ###

		dhdb = DrillholesDb.create(locDhdbPath)
		dhdb.set_collars(locCollarPath, "HOLE_ID", "XCOLLAR", "YCOLLAR", "ZCOLLAR", "DEPTH")
		dhdb.set_survey(locSurveyPath, "HOLE_ID", "DEPTH", "AZ", "DIP")
		dhdb.add_intervals(locAssayPath, "HOLE_ID", "FROM", "TO")

		dhdb.save()

		progress.setText("Проверка целостности БД")
		try:
			dhdb.refresh()
		except Exception as e:
			print ("Ошибка", str(e))

		has_traces = dhdb.has_traces
		dhdb.close()

		progress.setText("")
		progress.setRange(0, 100)

		if  has_traces:
			relDhdbPath = locDhdbPath[len(MMpy.Project.path()):]
			relAssayPath = locAssayPath[len(MMpy.Project.path()):]
			# расчет координат
			progress.setText("Расчет координат опробования")
			#
			P3dcoords_FormSet1= MMpy.FormSet("3DCOORDS","16.1.89.0")
			P3dcoords_FormSet1.set_field("SWITCH","0")
			P3dcoords_FormSet1.set_field("FIELDS","1")
			P3dcoords_FormSet1.set_field("TYPE","0")
			P3dcoords_FormSet1.set_field("RL","ZCOLLAR")
			P3dcoords_FormSet1.set_field("Y","YCOLLAR")
			P3dcoords_FormSet1.set_field("X","XCOLLAR")
			P3dcoords_FormSet1.set_field("T_FILE", relAssayPath)
			P3dcoords_FormSet1.set_field("BOOL","0")
			P3dcoords_FormSet1.set_field("3D_FILE", relDhdbPath)
			P3dcoords_FormSet1.set_field("INT","0")
			P3dcoords_FormSet1.set_field("REPFILE", rptpath, MMpy.append_flag.none)
			P3dcoords_FormSet1.run()



		check_module_params = {'collar' : {'path' : locCollarPath,
										'fields' : {'id' : "HOLE_ID",
													'east': "XCOLLAR", 'north' : "YCOLLAR", 'rl' : "ZCOLLAR", 'depth' : "DEPTH"
													},
										},
							'assay' : {'path' : locAssayPath,
										'fields' : {'id' : "HOLE_ID",
													'from' : "FROM", 'to' : "TO",  'length' : "LENGTH",
													'xcollar' : "XCOLLAR", 'ycollar' : "YCOLLAR", 'zcollar' : "ZCOLLAR"},
										'elements' : elems
										},
							'survey' : {'path' : locSurveyPath,
												'fields' : { 'id' : "HOLE_ID",
															'depth' : "DEPTH", 'az' : "AZ", 'dip' : "DIP"}
											}
						}
		
		check_module = DhChecking(self.main_app, check_module_params)
		check_module.next_check_calling.connect(lambda str: progress.setText("Контроль данных. {0}".format(str)))
		result = check_module.exec()
		check_module_message = ""
		if result > 0:
			check_report_path = os.path.join(self._tmpfolder, get_temporary_prefix() + "CHECK_REPORT.XLSX")

			workbook = xl.Workbook(check_report_path)

			worksheet = workbook.add_worksheet()
			worksheet.name = "Ошибки импорта скважин"
			fmtTitle = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "left", "valign": "vcenter", "border": 0, "bold": True})
			fmtHeader = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "left", "valign": "vcenter", "border": 1, "bold": True})
			fmtRptData = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "left", "valign": "vcenter", "border": 1})

			reports = check_module.reports

			worksheet_row = 0

			for report in reports:
				if report.result == 0:
					continue
						 
				title = report.report_title
				header = report.report_header
				data = report.report_data



				worksheet.write(worksheet_row + 1, 1, title, fmtTitle)
					
				for icol, colname in enumerate(header):
					worksheet.write(worksheet_row + 2, icol, colname, fmtHeader)

				for irow, row in enumerate(data, start = worksheet_row + 3):
					for icol, value in enumerate(row):
						worksheet.write(irow, icol, value, fmtRptData)
						worksheet_row = irow

				worksheet_row = worksheet_row + 1

			workbook.close()

			try:
				os.startfile(check_report_path)
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Не удалось открыть файл отчета")


			if result == 1:
				check_module_message =  "\n Некоторые данные были пропущены. Подробности смотрите в отчёте."
			elif result == 2:
				progress.setText("Откат обновления. Копирование файлов с сервера")
				for locpath, netpath in zip(locFilePaths, netFilePaths):
					if locpath.lower() == netpath.lower():
						continue
					err = self.exec_proc(self.copy_file, netpath, locpath)
					if err:					raise Exception("Невозможно скопировать файл %s. %s" % (netpath, err))

				qtmsgbox.show_error(self.main_app, "Ошибка", "База данных скважин не обновлена. \n Подробности смотрите в отчёте.")
				return

			



		# копирование файлов на сервер
		if not  has_traces:
			qtmsgbox.show_error(self.main_app, "Ошибка", "База не была обновлена, так как содержит ошибки")
		else:
			progress.setText("Копирование файлов на сервер")
			for locpath, netpath in zip(locFilePaths, netFilePaths):
				if locpath.lower() == netpath.lower():
					continue
				err = self.exec_proc(self.copy_file, locpath, netpath)
				if err:					raise Exception("Невозможно скопировать файл %s. %s" % (locpath, err))
			qtmsgbox.show_information(self.main_app, "Выполнено", "База данных скважин успешно обновлена" + check_module_message)





