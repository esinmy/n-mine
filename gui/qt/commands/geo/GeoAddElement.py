try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os

from mm.mmutils import Tridb
from mm.formsets import copy_wireframe

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.osutil import get_current_time
from utils.validation import DoubleValidator

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class GeoAddElement(DialogWindow):
	def __init__(self, *args, **kw):
		super(GeoAddElement, self).__init__(*args, **kw)

		self.setWindowTitle("Копировать каркас на сервер")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createVariables(self):
		oreCodes, self._oreDescriptions, oreIndexes = [], [], []
		for (oreCode, oreDescription, oreType), assayAndBmIndexes in self.main_app.settings.getValue(ID_GEOCODES):
			# if oreType == 2:
			# 	continue
			oreCodes.append(oreCode)
			self._oreDescriptions.append(oreDescription)
			oreIndexes.append(list(zip(*assayAndBmIndexes["BM"]))[0])

		self._oreIndexesDict = dict(zip(self._oreDescriptions, oreIndexes))
		self._oreCodeDict = dict(zip(self._oreDescriptions, oreCodes))
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.inputLayout = QtGui.QFormLayout()

		self.labelTriType = QtGui.QLabel(self.groupBoxInput)
		self.labelTriType.setText("Тип")
		self.labelTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelTriType.setMinimumWidth(70)
		self.labelTriType.setMaximumWidth(70)
		self.lineEditTriType = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditTriType.setMinimumWidth(200)		

		self.labelTriName = QtGui.QLabel(self.groupBoxInput)
		self.labelTriName.setText("Имя")
		self.labelTriName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelTriName.setMinimumWidth(70)
		self.labelTriName.setMaximumWidth(70)
		self.lineEditTriName = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditTriName.setMinimumWidth(200)


		self.groupBoxAttributes = QtGui.QGroupBox(self)
		self.groupBoxAttributes.setTitle("Атрибуты")
		self.attributesLayout = QtGui.QGridLayout()

		self.labelAttrLayer = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrLayer.setText("Залежь")
		self.labelAttrLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrLayer.setMinimumWidth(70)
		self.comboBoxAttrLayer = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrLayer.setMinimumWidth(150)

		self.labelAttrGeoUnit = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrGeoUnit.setText(self.main_app.settings.getValue(ID_GEO_UDSPNAME))
		self.labelAttrGeoUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrGeoUnit.setMinimumWidth(70)
		self.comboBoxAttrGeoUnit = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrGeoUnit.setMinimumWidth(150)

		self.labelAttrOreType = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrOreType.setText("Материал")
		self.labelAttrOreType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrOreType.setMinimumWidth(70)
		self.comboBoxAttrOreType = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrOreType.setMinimumWidth(150)

		self.labelAttrCode = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrCode.setText("Индекс")
		self.labelAttrCode.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrCode.setMinimumWidth(70)
		self.comboBoxAttrCode = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxAttrCode.setMinimumWidth(150)

		self.labelAttrDensity = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrDensity.setText("Объемный вес")
		self.labelAttrDensity.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrDensity.setMinimumWidth(70)
		self.lineEditAttrDensity = QtGui.QLineEdit(self.groupBoxAttributes)
		self.lineEditAttrDensity.setMinimumWidth(75)
		self.lineEditAttrDensity.setMaximumWidth(75)
		self.lineEditAttrDensity.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditAttrDensity.setValidator(DoubleValidator(0, 10, 3))

		self.labelAttrAzimuth = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrAzimuth.setText("Азимут")
		self.labelAttrAzimuth.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrAzimuth.setMinimumWidth(70)
		self.lineEditAttrAzimuth = QtGui.QLineEdit(self.groupBoxAttributes)
		self.lineEditAttrAzimuth.setMinimumWidth(75)
		self.lineEditAttrAzimuth.setMaximumWidth(75)
		self.lineEditAttrAzimuth.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditAttrAzimuth.setValidator(DoubleValidator(0, 360, 3))

		self.labelAttrInclination = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrInclination.setText("Погружение")
		self.labelAttrInclination.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrInclination.setMinimumWidth(70)
		self.lineEditAttrInclination = QtGui.QLineEdit(self.groupBoxAttributes)
		self.lineEditAttrInclination.setMinimumWidth(75)
		self.lineEditAttrInclination.setMaximumWidth(75)
		self.lineEditAttrInclination.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditAttrInclination.setValidator(DoubleValidator(-90, 90, 3))

		self.labelAttrRotation = QtGui.QLabel(self.groupBoxAttributes)
		self.labelAttrRotation.setText("Вращение")
		self.labelAttrRotation.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAttrRotation.setMinimumWidth(70)
		self.lineEditAttrRotation = QtGui.QLineEdit(self.groupBoxAttributes)
		self.lineEditAttrRotation.setMinimumWidth(75)
		self.lineEditAttrRotation.setMaximumWidth(75)
		self.lineEditAttrRotation.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditAttrRotation.setValidator(DoubleValidator(-90, 90, 3))


		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.outputLayout = QtGui.QFormLayout()

		self.labelOutTriType = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTriType.setText("Модель")
		self.labelOutTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriType.setMinimumWidth(70)
		self.labelOutTriType.setMaximumWidth(70)
		self.comboBoxOutTriType = QtGui.QComboBox(self.groupBoxOutput)
		self.comboBoxOutTriType.setMinimumWidth(200)

		self.labelOutTriName = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTriName.setText("Имя")
		self.labelOutTriName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriName.setMinimumWidth(70)
		self.labelOutTriName.setMaximumWidth(70)
		self.lineEditOutTriName = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutTriName.setMinimumWidth(200)

	def __createBindings(self):
		self.comboBoxAttrLayer.currentIndexChanged.connect(self.layerChanged)
		self.comboBoxAttrGeoUnit.currentIndexChanged.connect(self.geoUnitChanged)
		self.comboBoxAttrOreType.currentIndexChanged.connect(self.oreTypeChanged)
		self.comboBoxAttrCode.currentIndexChanged.connect(self.indexChanged)

		self.lineEditTriType.mouseDoubleClickEvent = lambda e: self.select_tridb(e, self.lineEditTriType)
		self.lineEditTriName.mouseDoubleClickEvent = lambda e: self.select_wireframe(e, self.lineEditTriType, self.lineEditTriName)
		self.lineEditOutTriName.mouseDoubleClickEvent = lambda e: self.select_wireframe(e, self.comboBoxOutTriType, self.lineEditOutTriName)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelTriType)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditTriType)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelTriName)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditTriName)

		self.layoutControls.addWidget(self.groupBoxAttributes)
		self.groupBoxAttributes.setLayout(self.attributesLayout)
		self.attributesLayout.addWidget(self.labelAttrLayer, 0, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrLayer, 0, 1)
		self.attributesLayout.addWidget(self.labelAttrGeoUnit, 1, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrGeoUnit, 1, 1)
		self.attributesLayout.addWidget(self.labelAttrOreType, 2, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrOreType, 2, 1)
		self.attributesLayout.addWidget(self.labelAttrCode, 3, 0)
		self.attributesLayout.addWidget(self.comboBoxAttrCode, 3, 1)
		self.attributesLayout.addWidget(self.labelAttrDensity, 0, 2)
		self.attributesLayout.addWidget(self.lineEditAttrDensity, 0, 3)
		self.attributesLayout.addWidget(self.labelAttrAzimuth, 1, 2)
		self.attributesLayout.addWidget(self.lineEditAttrAzimuth, 1, 3)
		self.attributesLayout.addWidget(self.labelAttrInclination, 2, 2)
		self.attributesLayout.addWidget(self.lineEditAttrInclination, 2, 3)
		self.attributesLayout.addWidget(self.labelAttrRotation, 3, 2)
		self.attributesLayout.addWidget(self.lineEditAttrRotation, 3, 3)

		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.outputLayout)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTriType)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxOutTriType)
		self.outputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelOutTriName)
		self.outputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditOutTriName)

	#
	def buildCombos(self):
		self.buildLayerList()
		self.buildGeoUnitList()
		self.buildOreTypeList()
		self.buildCodeList()
		self.buildOutTypeList()

	def buildLayerList(self):
		self.comboBoxAttrLayer.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT LAYER_DESCRIPTION 
			FROM [{0}].[dbo].[LAYER] 
			WHERE LOCATION = ?
			ORDER BY LAYER_DESCRIPTION
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список залежей. %s" % str(e))
			return
		layers = [""] + [row[0] for row in data]
		self.comboBoxAttrLayer.addItems(layers)
	def buildGeoUnitList(self):
		self.comboBoxAttrGeoUnit.clear()
		#
		layerName = self.comboBoxAttrLayer.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		if not layerId:
			return

		cmd = """
			SELECT PANEL_NAME 
			FROM [{0}].[dbo].[PANEL] 
			WHERE LAYER_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (layerId)
		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список панелей. %s" % str(e))
			return
		geoUnitNames = [""] + [row[0] for row in data]
		self.comboBoxAttrGeoUnit.addItems(geoUnitNames)
	def buildOreTypeList(self):
		self.comboBoxAttrOreType.clear()
		oretypes = [""] + self._oreDescriptions
		self.comboBoxAttrOreType.addItems(oretypes)
	def buildCodeList(self):
		self.comboBoxAttrCode.clear()
		oredesc = self.comboBoxAttrOreType.currentText()
		if not oredesc:
			return

		codes = ("",) + list(zip(*self._oreIndexesDict[oredesc]))[0]
		self.comboBoxAttrCode.addItems(codes)
	def buildOutTypeList(self):
		self.comboBoxOutTriType.clear()

		layerName = self.comboBoxAttrLayer.currentText()
		geoUnitName = self.comboBoxAttrGeoUnit.currentText()

		if not all([layerName, geoUnitName]):
			return

		prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		geodir = netpath + "\\" + self.main_app.settings.getValue(ID_GEO_MAINDIR)
		layerdir = geodir + "\\" + layerName
		geoUnitDir = layerdir + "\\" + prefix + geoUnitName
		tridir = geoUnitDir + "\\" + self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1]

		if not os.path.exists(tridir):
			return

		outtypes = [""] + list(map(lambda x: x[:x.rfind(".")], filter(lambda x: os.path.isfile(tridir + "\\" + x) and x.lower().endswith(".tridb"), os.listdir(tridir))))
		self.comboBoxOutTriType.addItems(outtypes)
	


	#
	def select_tridb(self, e, widget):
		filetypes = "КАРКАСЫ (*.TRIDB)"
		try:		initialdir = MMpy.Project.path()
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		# ничего не выбрано
		if not path:
			return

		widget.setText(path)
	def select_wireframe(self, e, triwidget, widget):
		if triwidget == self.lineEditTriType:
			tripath = triwidget.text()
		if triwidget == self.comboBoxOutTriType:
			layerName = self.comboBoxAttrLayer.currentText()
			geoUnitName = self.comboBoxAttrGeoUnit.currentText()

			if not all([layerName, geoUnitName]):
				return

			prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)

			netpath = self.main_app.settings.getValue(ID_NETPATH)
			geodir = netpath + "\\" + self.main_app.settings.getValue(ID_GEO_MAINDIR)
			layerdir = geodir + "\\" + layerName
			geoUnitDir = layerdir + "\\" + prefix + geoUnitName
			tridir = geoUnitDir + "\\" + self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1]
			triname = triwidget.currentText()
			if not triname:
				qtmsgbox.show_error(self, "Ошибка", "Выберите модель")
				return
			tripath = tridir + "\\%s.tridb" % triname

		try:
			tridb = Tridb(tripath)
			wireframe_names = tridb.wireframe_names
			tridb.close()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список каркасов. %s" % str(e))
			return

		dialog = SelectFromListDialog(self)
		dialog.setValues(wireframe_names)
		dialog.setWindowTitle("Выберите каркас")
		dialog.labelTitle.setText("Выберите каркас")
		dialog.exec()

		result = dialog.getValues()
		if result:
			widget.setText(result[0])
			self.lineEditOutTriName.setText(result[0])

	#
	def layerChanged(self):
		self.buildGeoUnitList()
	def geoUnitChanged(self):
		self.buildOutTypeList()
	def oreTypeChanged(self):
		self.buildCodeList()

		material = self.comboBoxAttrOreType.currentText()
		for row in list(zip(*self.main_app.settings.getValue(ID_GEOCODES)))[0]:
			if row[1] != material:
				continue
			#
			if row[2] != 0:
				self.lineEditAttrAzimuth.setDisabled(True)
				self.lineEditAttrRotation.setDisabled(True)
				self.lineEditAttrInclination.setDisabled(True)
			else:
				self.lineEditAttrAzimuth.setDisabled(False)
				self.lineEditAttrRotation.setDisabled(False)
				self.lineEditAttrInclination.setDisabled(False)
			#
			break
		self.lineEditAttrDensity.setText("")
	def indexChanged(self):
		index = self.comboBoxAttrCode.currentText()
		oredesc = self.comboBoxAttrOreType.currentText()
		if not index or not oredesc:
			self.lineEditAttrDensity.setText("")
		else:
			self.lineEditAttrDensity.setText(dict([[i,p] for i,p,pw in self._oreIndexesDict[oredesc]])[index])
	#
	def prepare(self):
		if not super(GeoAddElement, self).prepare():
			return
		
		inputPath = self.lineEditTriType.text()
		inputName = self.lineEditTriName.text()

		if not os.path.exists(inputPath):
			qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не найден" % inputPath)
			return
		#
		try:
			tridb = Tridb(inputPath)
			wireframe_names = tridb.wireframe_names
			tridb.close()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список каркасов. '%s'" % str(e))
			return
		#
		if not inputName in wireframe_names:
			qtmsgbox.show_error(self, "Ошибка", "Каркас '%s' не существует" % inputName)
			return

		#
		layerName = self.comboBoxAttrLayer.currentText()
		geoUnitName = self.comboBoxAttrGeoUnit.currentText()
		oreType = self.comboBoxAttrOreType.currentText()
		densityRequired = oreType in [row[1] for row in list(zip(*self.main_app.settings.getValue(ID_GEOCODES)))[0] if row[2] != 3]
		code = self.comboBoxAttrCode.currentText()
		
		density = self.lineEditAttrDensity.text()
		azimuth = self.lineEditAttrAzimuth.text()
		inclination = self.lineEditAttrInclination.text()
		rotation = self.lineEditAttrRotation.text()

		outtype = self.comboBoxOutTriType.currentText()
		outname = self.lineEditOutTriName.text()

		labels = (self.labelTriType, self.labelTriName, self.labelAttrLayer, self.labelAttrGeoUnit, self.labelAttrOreType, self.labelAttrCode,
					self.labelAttrDensity, self.labelAttrAzimuth, self.labelAttrInclination, self.labelAttrRotation, self.labelOutTriType, self.labelOutTriName)
		edits = (self.lineEditTriType, self.lineEditTriName, self.comboBoxAttrLayer, self.comboBoxAttrGeoUnit, self.comboBoxAttrOreType, self.comboBoxAttrCode,
					self.lineEditAttrDensity, self.lineEditAttrAzimuth,	self.lineEditAttrInclination, self.lineEditAttrRotation, self.comboBoxOutTriType, self.lineEditOutTriName)


		# values = [inputPath, inputName, layerName, geoUnitName, oreType, code, density, azimuth, inclination, rotation, outtype, outname]
		for label, edit in zip(labels, edits):
			if not edit.isEnabled():
				continue

			if edit == self.lineEditAttrDensity and not densityRequired:
				continue

			value = edit.currentText() if isinstance(edit, QtGui.QComboBox) else edit.text()
			if not value:
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно скопировать каркас на сервер. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(GeoAddElement, self).run():
			return

		srcpath = self.lineEditTriType.text()
		srcname = self.lineEditTriName.text()

		layerName = self.comboBoxAttrLayer.currentText()
		geoUnitName = self.comboBoxAttrGeoUnit.currentText()
		oreType = self._oreCodeDict[self.comboBoxAttrOreType.currentText()]
		code = self.comboBoxAttrCode.currentText()
		density = self.lineEditAttrDensity.text()
		azimuth = self.lineEditAttrAzimuth.text()
		inclination = self.lineEditAttrInclination.text()
		rotation = self.lineEditAttrRotation.text()

		dsttype = self.comboBoxOutTriType.currentText()
		dstname = self.lineEditOutTriName.text().rstrip()

		prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		geodir = netpath + "\\" + self.main_app.settings.getValue(ID_GEO_MAINDIR)
		layerdir = geodir + "\\" + layerName
		geoUnitDir = layerdir + "\\" + prefix + geoUnitName
		tridir = geoUnitDir + "\\" + self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1]
		dstpath = tridir + "\\%s.tridb" % dsttype

		# копирование каркаса
		dsttridb = Tridb(dstpath)
		if dstname in dsttridb.wireframe_names:
			if not qtmsgbox.show_question(self, "", "Каркас '%s' уже существует. Заменить?" % dstname):
				return
		geo_user_attributes = list(GEO_USER_ATTRIBUTES)
		geo_user_attributes.insert(1,self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper())
		if not all(map(lambda x: x in dsttridb.user_attributes, geo_user_attributes)):
			dsttridb.close()
			raise ValueError("В каркасе отсутствует один или несколько обязательных пользовательских атрибутов")
		dsttridb.close()

		# копирование каркаса
		err = self.exec_proc(copy_wireframe, srcpath, srcname, dstpath, dstname, False)
		if err:
			raise Exception("Невозможно скопировать каркас. %s" % err)

		# задание атрибутов
		dsttridb = Tridb(dstpath)
		dstwf = dsttridb[dstname]
		dstwf.set_default_attribute(DEFAULT_ATTR_CODE, code)
		dstwf.set_user_attribute(GEO_ATTR_LAYER, layerName)
		dstwf.set_user_attribute(self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper(), geoUnitName)
		dstwf.set_user_attribute(GEO_ATTR_MATERIAL, oreType)
		dstwf.set_user_attribute(GEO_ATTR_DENSITY, density)
		dstwf.set_user_attribute(GEO_ATTR_DIR, azimuth)
		dstwf.set_user_attribute(GEO_ATTR_DIP, inclination)
		dstwf.set_user_attribute(GEO_ATTR_ROT, rotation)
		dsttridb.commit()
		dsttridb.close()

		qtmsgbox.show_information(self, "Выполнено", "Каркас успешно скопирован на сервер")