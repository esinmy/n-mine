from PyQt4 import QtGui, QtCore
import os

from mm.mmutils import Tridb, MicromineFile

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.blockmodel import BlockModel
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GeoDeleteModel(DialogWindow):
	def __init__(self, *args, **kw):
		super(GeoDeleteModel, self).__init__(*args, **kw)

		self.setWindowTitle("Удалить модель")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._modelItem = None
	def __createWidgets(self):
		self.layoutParams = QtGui.QFormLayout()
		self.labelMessage1 = QtGui.QLabel(self)
		self.labelMessage1.setText("Вы действительно хотите удалить модель?")
		self.labelMessage1.setMinimumWidth(300)
		self.labelMessage1.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.addWidget(self.labelMessage1)

	def setModelItem(self, bmItem):
		filepath = bmItem.filePath()
		#
		self._modelItem = bmItem
		blockmodel = BlockModel(filepath, self.main_app)
		displayName = "_".join([blockmodel.layer, blockmodel.unit])
		suffix = blockmodel.name[len(displayName):]


	def prepare(self):
		super(GeoDeleteModel, self).prepare()

		# подключение к БД, если необходимо
		if self.main_app.srvcnxn is None:
			if self.main_app.connect_to_database() is None:
				return
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно удалить модель. %s" % str(e))

		self.after_run()
		if self._is_success:
			self.close()

	def run(self):
		#
		oldBmPath = self._modelItem.filePath()
		old_bm = BlockModel(oldBmPath, self.main_app)
		#
		#
		try:
			os.remove(old_bm.str_bounds_path)
			os.remove(old_bm.tri_wireframes_path)
			if not os.path.exists(oldBmPath):
				qtmsgbox.show_information(self, "Выполнено", "Модель успешно удалена.\n"
															"Файл блочной модели отсутствовал")
			else:
				os.remove(oldBmPath)
				if old_bm.has_metadata:
					old_bm.remove()
					self.main_app.srvcnxn.commit()
					qtmsgbox.show_information(self, 'Выполнено', "Модель и запись в БД успешно удалены.")
				else:
					qtmsgbox.show_information(self, "Выполнено", "Модель успешно удалена.\n"
																 "Запись в БД отсутствовала.")
			self.main_app.refreshStandardModels()
			self.main_app.refreshUserModels()

		except Exception as e:
			raise Exception("Невозможно удалить модель '%s'" % str(e))



