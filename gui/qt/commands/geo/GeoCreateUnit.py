from PyQt4 import QtGui, QtCore
import os

from utils.constants import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from utils.validation import is_blank

from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GeoCreateUnit(DialogWindow):
	def __init__(self, *args, **kw):
		super(GeoCreateUnit, self).__init__(*args, **kw)

		self.setWindowTitle("Создать юнит")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createVariables(self):
		self._layers = {}
		self._units = []
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")

		self.inputLayout = QtGui.QFormLayout()

		self.labelLayer = QtGui.QLabel(self.groupBoxInput)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setMinimumWidth(100)
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxLayer.setMinimumWidth(200)

		self.labelUnit = QtGui.QLabel(self.groupBoxInput)
		self.labelUnit.setMinimumWidth(100)
		self.labelUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelUnit.setText(self.main_app.settings.getValue(ID_GEO_UDSPNAME))
		self.comboBoxUnit = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxUnit.setInsertPolicy(QtGui.QComboBox.NoInsert)
		self.comboBoxUnit.setMinimumWidth(200)
		self.comboBoxUnit.setEditable(True)

		self.labelUnitDescription = QtGui.QLabel(self.groupBoxInput)
		self.labelUnitDescription.setText("Описание")
		self.labelUnitDescription.setMinimumWidth(100)
		self.labelUnitDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditUnitDescription = QtGui.QLineEdit(self.groupBoxInput)
	def __createBindings(self):
		self.comboBoxLayer.currentIndexChanged.connect(self.selectLayer)
		self.comboBoxUnit.currentIndexChanged.connect(self.selectUnit)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelUnit)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxUnit)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelUnitDescription)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditUnitDescription)

	def buildCombos(self):
		self.buildLayerList()
		self.buildUnitList()

	def buildLayerList(self):
		# считываем залежи из БД
		self.comboBoxLayer.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		self._layers = {}
		#
		cmd = """
			SELECT LAYER_ID, LAYER_DESCRIPTION 
			FROM [{0}].[dbo].[LAYER]
			WHERE LOCATION = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not data:
			return
		for layerId, layerName in data:
			self._layers[layerName] = layerId
		self.comboBoxLayer.addItems([""] + sorted(list(self._layers.keys())))
	def buildUnitList(self):
		# считываем залежи из БД
		self.comboBoxUnit.clear()
		self._units = []
		#
		layerName = self.comboBoxLayer.currentText()
		if not layerName in self._layers:
			return
		cmd = """
			SELECT PANEL_NAME, PANEL_DESCRIPTION FROM [{0}].[dbo].[PANEL]
			WHERE LAYER_ID = ?""".format(self.main_app.srvcnxn.dbsvy)
		params = (self._layers[layerName])
		self._units = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not self._units:
			return
		names, descriptions = zip(*self._units)
		names = [""] + list(names)
		descriptions = [""] + list(descriptions)
		self.comboBoxUnit.addItems(names)
		self.lineEditUnitDescription.setText(descriptions[0])

	def selectLayer(self, index):
		layer = self.comboBoxLayer.itemText(index)
		if layer and not layer in self._layers:
			raise KeyError("Не найдена залежь '%s'" % layer)
		self.buildUnitList()
	def selectUnit(self, index):
		self.lineEditUnitDescription.setText(self._units[index-1][1])

	def createFolders(self):
		networkpath = self.main_app.settings.getValue(ID_NETPATH)
		geodir = self.main_app.settings.getValue(ID_GEO_MAINDIR)
		layerName = self.comboBoxLayer.currentText()
		prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)
		unitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME)
		unitName = self.comboBoxUnit.currentText()

		# создание залежи, если отсутствует
		currentFolder = os.path.join(networkpath, geodir, layerName)
		if not os.path.exists(currentFolder):
			os.mkdir(currentFolder)
		
		# создание юнита, если отсутствует
		currentFolder = os.path.join(currentFolder, prefix + unitName)
		if os.path.exists(currentFolder):
			qtmsgbox.show_error(self, "Ошибка", "%s уже существует" % unitDisplayName)
			return

		try:
			os.mkdir(currentFolder)
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать директорию '%s'. %s" % (unitName, str(e)))
			return

		# создание вложенных директорий
		for folderPurpose, folderName in self.main_app.settings.getValue(ID_GEO_USUBDIRS):
			try:
				os.mkdir(currentFolder + "\\" + folderName)
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно создать директорию '%s'. %s" % (folderName, str(e)))
				return

		return True

	def prepare(self):
		if not super(GeoCreateUnit, self).prepare():
			return

		unitName = self.comboBoxUnit.currentText()
		prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)
		if unitName.startswith(prefix) and not is_blank(prefix):
			qtmsgbox.show_error(self, "Ошибка", "Возможно, вы хватили лишку... Введите правильно")
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать геологический юнит. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(GeoCreateUnit, self).run():
			return

		layerName = self.comboBoxLayer.currentText()
		unitName = self.comboBoxUnit.currentText()
		unitDescription = self.lineEditUnitDescription.text()

		cmd = "SELECT PANEL_NAME FROM [{0}].[dbo].[PANEL] WHERE LAYER_ID = ?".format(self.main_app.srvcnxn.dbsvy)
		params  = (self._layers[layerName])
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		# if not data:
		# 	return

		unitNames = [row[0] for row in data]
		if not unitName in unitNames:
			cmd = """
				INSERT INTO [{0}].[dbo].[PANEL] (PANEL_NAME, PANEL_DESCRIPTION, LAYER_ID)
				VALUES (?, ?, ?)""".format(self.main_app.srvcnxn.dbsvy)
			params = (unitName, unitDescription, self._layers[layerName])
			self.main_app.srvcnxn.exec_query(cmd, params)
			self.main_app.srvcnxn.commit()

		if not self.createFolders():
			return

		qtmsgbox.show_information(self, "Выполнено", "Юнит успешно создан")

