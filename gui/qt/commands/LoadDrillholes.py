try:		import MMpy
except:		pass

from PyQt4 import QtGui, QtCore
import os

from datetime import datetime
from utils.constants import *
from utils.validation import is_blank
from utils.osutil import get_temporary_prefix

from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.DatabaseLoaderWindow import DatabaseLoaderWindow

from mm.formsets import loadSurveyPoints, loadDrillholesDb, loadDrillholesValue, loadDrillholesHatch, create_composite_simple, create_composite_advanced, create_composite_gkz
from mm.mmutils import MicromineFile, DrillholesDb
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class LoadDrillholes(DatabaseLoaderWindow):
	def __init__(self, *args, **kw):
		super(LoadDrillholes, self).__init__(*args, **kw)
		self.setWindowTitle("Загрузить скважины")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self.__wgt_cmdId = {}
		netCollarPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPCOLLAR))
		f = MicromineFile()
		if not f.open(netCollarPath):
			raise Exception("Невозможно открыть файл '%s'" % netCollarPath)

		try:
			readcols = ["HOLE_ID", "DEPOSIT", "LAYER", "UNIT", "SECTION", "MINE_NAME", "XCOLLAR", "YCOLLAR", "ZCOLLAR", "DEPTH", "START_DATE", "END_DATE"]
			data = [row[:1] + [row[0][:-2]] + row[1:] for row in f.read(columns = readcols, asstr = True)]
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать файл. Обновите базу данных скважин. %s" % str(e))
			f.close()
			return
		f.close()

		#f2 = open(r'c:\temp\data.txt', 'w')
		#for row in data:
		#	f2.write(','.join(row))
		#	f2.write('\n')
		#f2.close()
		#f2 = open(r'c:\temp\data.txt', 'r')
		#data = [line.strip('\n').split(',') for line in f2]
		#f2.close()
		try:
			data.sort(key = lambda x: x[8], reverse = True)
			geoUnitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper()
			self._header = ["HOLE_ID", "СКВАЖИНА", "МЕСТОРОЖДЕНИЕ", "ЗАЛЕЖЬ", geoUnitDisplayName, "РАЗРЕЗ", "ШАХТА", "ВОСТОК", "СЕВЕР", "Z", "ГЛУБИНА", "ДАТА НАЧАЛА", "ДАТА ОКОНЧАНИЯ"]
			self.setDataset(data, self._header, 11, 12)
			self.view.horizontalHeader().controls[0].click()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", str(e))
			return
	def __createWidgets(self):
		self.layoutAddons = QtGui.QHBoxLayout()

		self.groupBoxDisplayTrace = QtGui.QGroupBox(self)
		self.groupBoxDisplayTrace.setTitle("Загрузить траектории")
		self.groupBoxDisplayTrace.setCheckable(False)


		self.layoutTrace = QtGui.QFormLayout()
		self.labelTraceForm = QtGui.QLabel(self.groupBoxDisplayTrace)
		self.labelTraceForm.setMinimumWidth(80)
		self.labelTraceForm.setText("Форма")
		self.labelTraceForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditTraceForm = QtGui.QLineEdit(self.groupBoxDisplayTrace)
		self.lineEditTraceForm.setMaximumWidth(100)

		self.groupBoxDisplayAssay = QtGui.QGroupBox(self)
		self.groupBoxDisplayAssay.setTitle("Загрузить опробование")
		self.groupBoxDisplayAssay.setCheckable(True)
		self.groupBoxDisplayAssay.setChecked(False)

		self.layoutAssay = QtGui.QFormLayout()
		self.labelAssayForm = QtGui.QLabel(self.groupBoxDisplayAssay)
		self.labelAssayForm.setMinimumWidth(80)
		self.labelAssayForm.setText("Форма")
		self.labelAssayForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditAssayForm = QtGui.QLineEdit(self.groupBoxDisplayAssay)
		self.lineEditAssayForm.setMaximumWidth(100)

		self.groupBoxDisplayLitho = QtGui.QGroupBox(self)
		self.groupBoxDisplayLitho.setCheckable(True)
		self.groupBoxDisplayLitho.setChecked(False)
		self.groupBoxDisplayLitho.setTitle("Загрузить литологию")

		self.layoutLitho = QtGui.QFormLayout()
		self.labelLithoForm = QtGui.QLabel(self.groupBoxDisplayLitho)
		self.labelLithoForm.setMinimumWidth(80)
		self.labelLithoForm.setText("Форма")
		self.labelLithoForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditLithoForm = QtGui.QLineEdit(self.groupBoxDisplayLitho)
		self.lineEditLithoForm.setMaximumWidth(100)

		self.groupBoxDisplayIntervals = QtGui.QGroupBox(self)
		self.groupBoxDisplayIntervals.setCheckable(True)
		self.groupBoxDisplayIntervals.setChecked(False)
		self.groupBoxDisplayIntervals.setTitle("Загрузить интервалы")

		self.layoutIntervals = QtGui.QFormLayout()
		self.labelIntervalsForm = QtGui.QLabel(self.groupBoxDisplayIntervals)
		self.labelIntervalsForm.setMinimumWidth(80)
		self.labelIntervalsForm.setText("Форма")
		self.labelIntervalsForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditIntervalsForm = QtGui.QLineEdit(self.groupBoxDisplayIntervals)
		self.lineEditIntervalsForm.setMaximumWidth(100)		

		self.groupBoxDisplayComposites = QtGui.QGroupBox(self)
		self.groupBoxDisplayComposites.setCheckable(True)
		self.groupBoxDisplayComposites.setChecked(False)
		self.groupBoxDisplayComposites.setTitle("Загрузить композиты")

		self.layoutComposites = QtGui.QGridLayout()
		self.labelCompositesEstiForm = QtGui.QLabel(self.groupBoxDisplayComposites)
		self.labelCompositesEstiForm.setMinimumWidth(95)
		self.labelCompositesEstiForm.setText("Форма. Расчёт.")
		self.labelCompositesEstiForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditCompositesEstiForm = QtGui.QLineEdit(self.groupBoxDisplayComposites)
		self.lineEditCompositesEstiForm.setMaximumWidth(100)	

		self.labelCompositesViewForm = QtGui.QLabel(self.groupBoxDisplayComposites)
		self.labelCompositesViewForm.setMinimumWidth(80)
		self.labelCompositesViewForm.setText("Форма. Вид.")
		self.labelCompositesViewForm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditCompositesViewForm = QtGui.QLineEdit(self.groupBoxDisplayComposites)
		self.lineEditCompositesViewForm.setMaximumWidth(100)	

	def __createBindings(self):
		self.dateRangeWidget.dateEditFrom.dateChanged.connect(self.on_dateRangeWidget_dateEditFrom_dateChanged)
		self.dateRangeWidget.dateEditTo.dateChanged.connect(self.on_dateRangeWidget_dateEditTo_dateChanged)
		self.shapeFilter.filterApplied.connect(self.on_shapeFilter_filterApplied)
		self.shapeFilter.filterDefaultRestored.connect(self.on_shapeFilter_filterDefaultRestored)
		self.lineEditTraceForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditTraceForm)
		self.lineEditAssayForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditAssayForm)
		self.lineEditLithoForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditLithoForm)
		self.lineEditIntervalsForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditIntervalsForm)
		self.lineEditCompositesEstiForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditCompositesEstiForm)
		self.lineEditCompositesViewForm.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditCompositesViewForm)

	def __gridWidgets(self):
		self.layoutParams.addLayout(self.layoutAddons)
		
		self.layoutAddons.addWidget(self.groupBoxDisplayTrace)
		self.groupBoxDisplayTrace.setLayout(self.layoutTrace)
		self.layoutTrace.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelTraceForm)
		self.layoutTrace.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditTraceForm)

		self.layoutAddons.addWidget(self.groupBoxDisplayAssay)
		self.groupBoxDisplayAssay.setLayout(self.layoutAssay)
		self.layoutAssay.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelAssayForm)
		self.layoutAssay.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditAssayForm)

		self.layoutAddons.addWidget(self.groupBoxDisplayLitho)
		self.groupBoxDisplayLitho.setLayout(self.layoutLitho)
		self.layoutLitho.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLithoForm)
		self.layoutLitho.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditLithoForm)

		self.layoutAddons.addWidget(self.groupBoxDisplayIntervals)
		self.groupBoxDisplayIntervals.setLayout(self.layoutIntervals)
		self.layoutIntervals.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelIntervalsForm)
		self.layoutIntervals.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditIntervalsForm)

		self.layoutAddons.addWidget(self.groupBoxDisplayComposites)
		self.groupBoxDisplayComposites.setLayout(self.layoutComposites)
		self.layoutComposites.addWidget(self.labelCompositesEstiForm,0, 0, QtCore.Qt.AlignRight)
		self.layoutComposites.addWidget(self.lineEditCompositesEstiForm,0,1, QtCore.Qt.AlignLeft)
		self.layoutComposites.addWidget(self.labelCompositesViewForm,0,2, QtCore.Qt.AlignRight)
		self.layoutComposites.addWidget(self.lineEditCompositesViewForm,0,3, QtCore.Qt.AlignLeft)


	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
			self._set_shapefilter_params()

			event.ignore()
		else:
			super(LoadDrillholes, self).keyPressEvent(event)

	def on_dateRangeWidget_dateEditFrom_dateChanged(self):
		self._set_shapefilter_params()

	def on_dateRangeWidget_dateEditTo_dateChanged(self):
		self._set_shapefilter_params()

	def on_shapeFilter_filterApplied(self, sender):
		self._set_shapefilter_params()

	def on_shapeFilter_filterDefaultRestored(self, sender):
		self._set_shapefilter_params()

	def _selectFormset(self, event, wgt):
		if not wgt.isEnabled():
			return

		if wgt == self.lineEditIntervalsForm:
			cmdId = [512]
		elif wgt == self.lineEditTraceForm:
			cmdId = [451]
		elif wgt == self.lineEditCompositesEstiForm:
			cmdId = [905, 444, 1020]
		else:
			cmdId = [509]

		 #if wgt == self.lineEditIntervalsForm else 509

		#
		try:
			tree = TreeMmFormsetBrowserDialog(self)
			for id in cmdId:
				tree.readFormsets(id)

			tree.exec()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать наборы форм. %s" % str(e))
			return

		ret = tree.result 
		ret_cmdId = tree.result_cmdId 
		if not ret:
			return
		wgt.setText(ret)
		self.__wgt_cmdId[wgt] = ret_cmdId

	def _set_shapefilter_params(self):
		extents={}
		extents['xmin'] = self.shapeFilter.xmin
		extents['xmax'] = self.shapeFilter.xmax
		extents['ymin'] = self.shapeFilter.ymin
		extents['ymax'] = self.shapeFilter.ymax
		extents['zmin'] = self.shapeFilter.zmin
		extents['zmax'] = self.shapeFilter.zmax
		self.model.applyFilter(11, 12, extents = extents, xcol = 8, ycol = 9, zcol = 10)

	def check(self):
		if not super(LoadDrillholes, self).check():
			return
		return True
		

	def prepare(self):
		if not super(LoadDrillholes, self).prepare():
			return

		if not self.check():
			return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", str(e))

		self.after_run()

	def run(self):
		if not super(LoadDrillholes, self).run():
			return

		progress = self.main_app.mainWidget.progressBar
		progress.setRange(0,0)

		netAssayPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPASSAY))
		netCollarPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPCOLLAR))
		netSurveyPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPSVY))
		netFilePaths = [netAssayPath, netSurveyPath]

		locAssayPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netAssayPath))
		locCollarPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netCollarPath))
		locSurveyPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netSurveyPath))
		locFilePaths = [locAssayPath, locSurveyPath]




		locDhdbPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "DrillholeDataBase.DHDB")

		progress.setText("Копирование файлов опробования и инклинометрии")
		for netpath, locpath in zip(netFilePaths, locFilePaths):
			err = self.exec_proc(self.copy_file, netpath, locpath)
			if err:					raise Exception("Невозможно скопировать файл %s. %s" % (netpath, err))
		# создание БД
		try:					dhdb = DrillholesDb.create(locDhdbPath)
		except Exception as e:	raise Exception("Невозможно создать базу данных скважин. %s" % str(e))

		progress.setText("Создание файла устьев")

		pos_east = self._header.index("ВОСТОК")
		pos_north = self._header.index("СЕВЕР")
		pos_rl = self._header.index("Z")
		pos_depth = self._header.index("ГЛУБИНА")
		for i, row in enumerate(self._dataload):
			try:  
				self._dataload[i][pos_east] = float(self._dataload[i][pos_east].replace(",", ".")) if self._dataload[i][pos_east] else ""
				self._dataload[i][pos_north] = float(self._dataload[i][pos_north].replace(",", ".")) if self._dataload[i][pos_north] else ""
				self._dataload[i][pos_rl] = float(self._dataload[i][pos_rl].replace(",", ".")) if self._dataload[i][pos_rl] else ""
				self._dataload[i][pos_depth] = float(self._dataload[i][pos_depth].replace(",", ".")) if self._dataload[i][pos_depth] else ""
			except:
				pass

		self._dataload.sort(key = lambda x: x[0])
		MicromineFile.write(locCollarPath, self._dataload, self._header)

		progress.setText("Создание базы данных скважин")
		geoUnitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper()
		attributes = ["СКВАЖИНА", "МЕСТОРОЖДЕНИЕ", "ДАТА НАЧАЛА", "ДАТА ОКОНЧАНИЯ", "ШАХТА", geoUnitDisplayName]
		dhdb.set_collars(locCollarPath, "HOLE_ID", "ВОСТОК", "СЕВЕР", "Z", "ГЛУБИНА", attributes)
		dhdb.set_survey(locSurveyPath, "HOLE_ID", "DEPTH", "AZ", "DIP")
		dhdb.add_intervals(locAssayPath, "HOLE_ID", "FROM", "TO")

		dhdb.save()

		try:						dhdb.refresh()
		except Exception as e:		raise Exception("Невозможно рассчитать траектории скважин")

		dhdb.close()

		progress.setText("Загрузка")


		relDbPath = locDhdbPath[len(MMpy.Project.path()):]
		relAssayPath = locAssayPath[len(MMpy.Project.path()):]
		#

		if True:		# Траектории грузятся всегда
			# загрузка траекторий
			formsetId = self.lineEditTraceForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesDb(locDhdbPath, "Скважины", formsetId)

		if self.groupBoxDisplayAssay.isChecked():
			# загрузка опробования
			formsetId = self.lineEditAssayForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesHatch(relDbPath, relAssayPath, "Опробование", formsetId)
		#
		if self.groupBoxDisplayLitho.isChecked():
			# загрузка литологиих
			formsetId = self.lineEditLithoForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesHatch(relDbPath, relAssayPath, "Литология", formsetId)

		if self.groupBoxDisplayIntervals.isChecked():
			formsetId = self.lineEditIntervalsForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesValue(relDbPath, relAssayPath, "Метки интервалов", formsetId)

		if self.groupBoxDisplayComposites.isChecked():

			progress.setText("Расчёт композитов")

			formsetId = self.lineEditCompositesEstiForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			if formsetId is None:
				return

			cmdId = int(self.__wgt_cmdId[self.lineEditCompositesEstiForm])

			locCompositePath = os.path.join(self._tmpfolder, get_temporary_prefix() + "COMPOSITES.DAT")
			if cmdId == 905: 
				title = "Композиты. Простые."
				create_composite_simple( locAssayPath, locCompositePath, title, formsetId)
			elif cmdId == 444:
				title = "Композиты. Расширенные."
				create_composite_advanced( locAssayPath, locCompositePath, title, formsetId)
			elif cmdId == 1020:
				title = "Композиты. ГКЗ."
				create_composite_gkz( locAssayPath, locCompositePath, title, formsetId)

			if not os.path.exists(locCompositePath):
				raise FileNotFoundError("Файл '%s' не существует" % locCompositePath)

			dhdb = DrillholesDb(locDhdbPath)
			dhdb.add_intervals(locCompositePath, "HOLE_ID", "FROM", "TO")
			dhdb.save()

			try:						dhdb.refresh()
			except Exception as e:		raise Exception("Невозможно рассчитать траектории скважин")

			dhdb.close()

			progress.setText("Загрузка")

			relCompositePath = locCompositePath[len(MMpy.Project.path()):]
			formsetId = self.lineEditCompositesViewForm.text()
			formsetId = None if is_blank(formsetId) else int(formsetId)
			loadDrillholesHatch(relDbPath, relCompositePath, title, formsetId)

				


			
		progress.setText("")
		progress.setRange(0,100)

	