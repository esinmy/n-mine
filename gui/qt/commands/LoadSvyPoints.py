from PyQt4 import QtGui, QtCore
import os

from datetime import datetime

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
from gui.qt.DatabaseLoaderWindow import DatabaseLoaderWindow

from mm.formsets import loadSurveyPoints, loadPoints
from mm.mmutils import MicromineFile

from utils.validation import is_blank
from utils.constants import *
from utils.osutil import get_temporary_prefix
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class LoadSvyPoints(DatabaseLoaderWindow):
	def __init__(self, *args, **kw):
		super(LoadSvyPoints, self).__init__(*args, **kw)

		self.setWindowTitle("Загрузить маркшейдерские точки")
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		cmd = """
			SELECT
				P.POINTNAME,
				P.BASE_X, P.BASE_Y, P.BASE_Z,
				(SELECT PCODENAME FROM [{0}].[dbo].[MP_LKP_POINTCODES] L WHERE L.POINTCODE_ID = P.POINTCODE_ID),
				(
					SELECT FIRSTNAME + ' ' + SURNAME + ' ' + SUVMIDDLE
					FROM [{0}].[dbo].[SURVEY_FIO]
					WHERE
						FIOID = M.EXECUTOR_ID
				),
				ISNULL(L.LAYER_DESCRIPTION + ' : ' + PN.PANEL_DESCRIPTION, '')  AS UNIT,
				ISNULL(V.Vyr, ''),
				R.NAME,
				C.NAME,
				M.MEASTIMESTAMP

			FROM [{0}].[dbo].[MP_BASE_POINTS] P LEFT JOIN [{0}].[dbo].[MP_MEASURES] M
				ON P.CREATEMEASURE_ID = M.MEASURE_ID 
				AND M.MEASURENAME = P.MEASURENAME
				AND M.MTYPE_ID = (SELECT MTYPE_ID FROM [{0}].[dbo].[MP_LKP_MEASURETYPES] WHERE MTYPENAME = P.CREATEMEATYPE_ID)
					LEFT JOIN [{0}].[dbo].[PANEL] AS PN
					ON PN.PANEL_ID = M.UNIT_ID
						LEFT JOIN  [{0}].[dbo].[LAYER] AS L
						ON L.LAYER_ID = PN.LAYER_ID
							LEFT JOIN [{0}].[dbo].[VYRAB_P] AS V
							ON M.EXCAVATION_ID = V.VyrId
								INNER JOIN [{0}].[dbo].[MP_REGISTER] AS R
								ON R.ID = M.REGISTER_ID
									INNER JOIN [{0}].[dbo].[MP_CLASS] AS C
									ON C.ID = P.CLASS_ID
			WHERE BASEREMOVED = 0 and M.MEASREMOVED = 0
		""".format(self.main_app.srvcnxn.dbsvy)
		#
		if self.main_app.srvcnxn.location is not None:
			cmd += """ AND M.LOCATION = N'{0}'""".format(self.main_app.srvcnxn.location)
		cmd += """ ORDER BY MEASTIMESTAMP DESC"""

		data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		for i, row in enumerate(data):
			data[i] = list(map(str, row))
			for iCol in [1,2,3]:
				try:		data[i][iCol] = "%.3f" % float(data[i][iCol])
				except:		pass
		#
		geoUnitDisplayName = self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper()
		header = ["ИМЯ", "Y (ВОСТОК)", "X (СЕВЕР)", "Z (ОТМЕТКА)", "ТИП", "МАРКШЕЙДЕР", geoUnitDisplayName, "ВЫРАБОТКА", "ЖУРНАЛ", "КЛАССИФИКАЦИЯ", "ДАТА"]
		if data:
			self.setDataset(data, header, -1, -1)
		self.view.setColumnWidth(6, 300)
	def __createWidgets(self):
		self.groupBoxUserFormset = QtGui.QGroupBox(self)
		self.groupBoxUserFormset.setTitle("Использовать пользовательскую форму")
		self.groupBoxUserFormset.setCheckable(True)
		self.groupBoxUserFormset.setChecked(True)

		self.layoutUserFormset = QtGui.QFormLayout()

		self.labelFormset = QtGui.QLabel(self.groupBoxUserFormset)
		self.labelFormset.setText("Форма")
		self.labelFormset.setMinimumWidth(100)
		self.labelFormset.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditFormset = QtGui.QLineEdit(self.groupBoxUserFormset)
		self.lineEditFormset.setText("1")
		self.lineEditFormset.setMaximumWidth(100)
		self.lineEditFormset.setAlignment(QtCore.Qt.AlignRight)
		self.lineEditFormset.setValidator(QtGui.QIntValidator())

	def __createBindings(self):
		self.dateRangeWidget.dateEditFrom.dateChanged.connect(self.on_dateRangeWidget_dateEditFrom_dateChanged)
		self.dateRangeWidget.dateEditTo.dateChanged.connect(self.on_dateRangeWidget_dateEditTo_dateChanged)

		self.lineEditFormset.mouseDoubleClickEvent = lambda e: self._selectFormset(e, self.lineEditFormset)

		self.shapeFilter.filterApplied.connect(self.on_shapeFilter_filterApplied)
		self.shapeFilter.filterDefaultRestored.connect(self.on_shapeFilter_filterDefaultRestored)
	def __gridWidgets(self):
		self.layoutParams.addWidget(self.groupBoxUserFormset)
		self.groupBoxUserFormset.setLayout(self.layoutUserFormset)

		self.layoutUserFormset.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelFormset)
		self.layoutUserFormset.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditFormset)

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
			extents={}
			extents['xmin'] = self.shapeFilter.xmin
			extents['xmax'] = self.shapeFilter.xmax
			extents['ymin'] = self.shapeFilter.ymin
			extents['ymax'] = self.shapeFilter.ymax
			extents['zmin'] = self.shapeFilter.zmin
			extents['zmax'] = self.shapeFilter.zmax
			self.model.applyFilter(-1, -1, extents = extents, xcol = 2, ycol=3, zcol=4)
			event.ignore()
		else:
			super(LoadSvyPoints, self).keyPressEvent(event)
	
	def on_dateRangeWidget_dateEditFrom_dateChanged(self):
		extents={}
		extents['xmin'] = self.shapeFilter.xmin
		extents['xmax'] = self.shapeFilter.xmax
		extents['ymin'] = self.shapeFilter.ymin
		extents['ymax'] = self.shapeFilter.ymax
		extents['zmin'] = self.shapeFilter.zmin
		extents['zmax'] = self.shapeFilter.zmax
		self.model.applyFilter(-1, -1, extents = extents, xcol = 2, ycol=3, zcol=4)


	def on_dateRangeWidget_dateEditTo_dateChanged(self):
		extents={}
		extents['xmin'] = self.shapeFilter.xmin
		extents['xmax'] = self.shapeFilter.xmax
		extents['ymin'] = self.shapeFilter.ymin
		extents['ymax'] = self.shapeFilter.ymax
		extents['zmin'] = self.shapeFilter.zmin
		extents['zmax'] = self.shapeFilter.zmax
		self.model.applyFilter(-1, -1, extents = extents, xcol = 2, ycol=3, zcol=4)

	def on_shapeFilter_filterApplied(self, sender):
		extents={}
		extents['xmin'] = sender.xmin
		extents['xmax'] = sender.xmax
		extents['ymin'] = sender.ymin
		extents['ymax'] = sender.ymax
		extents['zmin'] = sender.zmin
		extents['zmax'] = sender.zmax

		self.model.applyFilter(-1, -1, extents = extents, xcol = 2, ycol=3, zcol=4)

	def on_shapeFilter_filterDefaultRestored(self, sender):
		extents={}
		extents['xmin'] = sender.xmin
		extents['xmax'] = sender.xmax
		extents['ymin'] = sender.ymin
		extents['ymax'] = sender.ymax
		extents['zmin'] = sender.zmin
		extents['zmax'] = sender.zmax

		self.model.applyFilter(-1, -1, extents = extents, xcol = 2, ycol=3, zcol=4)

	def _selectFormset(self, event, wgt):
		if not wgt.isEnabled():
			return

		#
		try:
			tree = TreeMmFormsetBrowserDialog(self)
			tree.readFormsets(MMTYPE_COMMAND_ID_DICT[MMTYPE_DATA])
			tree.exec()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать наборы форм. %s" % str(e))
			return

		ret = tree.result
		if not ret:
			return
		wgt.setText(ret)

	def check(self):
		if not super(LoadSvyPoints, self).check():
			return

		if self.groupBoxUserFormset.isChecked() and is_blank(self.lineEditFormset.text()):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelFormset.text())
			self.lineEditFormset.setFocus()
			return

		return True

	def prepare(self):
		if not super(LoadSvyPoints, self).prepare():
			return

		if not self.check():
			return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", str(e))

		self.after_run()

	def run(self):
		if not super(LoadSvyPoints, self).run():
			return

		# запись в файл
		path = os.path.join(self._tmpfolder, get_temporary_prefix() + "SURVEY_POINTS.DAT")
		header = ["ИМЯ", "EAST", "NORTH", "RL", "ТИП", "МАРКШЕЙДЕР", "ДАТА"]
		struct = ["ИМЯ:C:20:0", "EAST:R:0:3", "NORTH:R:0:3", "RL:R:0:3", "ТИП:C:5:0", "МАРКШЕЙДЕР:C:50:0", "ДАТА:C:10:0"]
		MicromineFile.write(path, self._dataload, header, struct)

		# загрузка
		display_name = "Маркшейдерские точки"
		if self.groupBoxUserFormset.isChecked():
			setId = int(self.lineEditFormset.text())
			loadPoints(path, display_name, setId)
		else:
			loadSurveyPoints(path, display_name)


