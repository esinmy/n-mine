try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os
from mm.mmutils import MicromineFile

import gui.qt.dialogs.messagebox as qtmsgbox

from utils.osutil import get_current_time
from utils.constants import *
from utils.validation import is_blank
from gui.qt.ProcessWindow import DialogWindow
from mm.formsets import  importLeicaGsi	, loadStrings

class ImportLeicaGsiDialog(DialogWindow):
	def __init__(self, *args, **kw):
		super(ImportLeicaGsiDialog, self).__init__(*args, **kw)
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setWindowTitle("Импорт GSI файла")

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.setFixedSize(self.sizeHint())

		self.buildCombos()

	def __createVariables(self): 
	    pass 
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(5)

		self.layoutInput = QtGui.QHBoxLayout()

		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Входные параметры")

		self.layoutInputParams = QtGui.QFormLayout()
		self.labelFileInput = QtGui.QLabel(self.groupBoxInput)
		self.labelFileInput.setText("Файл GSI")
		self.labelFileInput.setMinimumWidth(100)
		self.labelFileInput.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.lineEditInputFile = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditInputFile.setMinimumWidth(200)

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Параметры файла вывода")

		self.layoutOutputParams = QtGui.QFormLayout()
		self.labelFileOutput = QtGui.QLabel(self.groupBoxOutput)
		self.labelFileOutput.setText("Файл вывода")
		self.labelFileOutput.setMinimumWidth(100)
		self.labelFileOutput.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutputFile = QtGui.QLineEdit()
		self.lineEditOutputFile.setMinimumWidth(400)

		self.labelExecutor = QtGui.QLabel(self.groupBoxOutput)
		self.labelExecutor.setText("Исполнитель")
		self.labelExecutor.setMinimumWidth(100)
		self.labelExecutor.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxExecutor = QtGui.QComboBox()

		self.labelDate = QtGui.QLabel(self.groupBoxOutput)
		self.labelDate.setText("Дата")
		self.labelDate.setMinimumWidth(100)
		self.labelDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.dateEditDate = QtGui.QDateEdit(self.groupBoxOutput)
		self.dateEditDate.setCalendarPopup(True)
		self.dateEditDate.calendarWidget().setFirstDayOfWeek(QtCore.Qt.Monday)
		self.dateEditDate.setDisplayFormat("yyyy-MM-dd")
		currentTime = get_current_time()
		self.dateEditDate.setDate(QtCore.QDate(currentTime.year, currentTime.month, currentTime.day))


	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
		self.lineEditInputFile.mouseDoubleClickEvent = lambda e: self.select_input_file(e, self.lineEditInputFile)
		self.lineEditOutputFile.mouseDoubleClickEvent = lambda e: self.select_output_file(e, self.lineEditOutputFile)
	def __gridWidgets(self):

		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.layoutInputParams)
		self.layoutInputParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelFileInput)
		self.layoutInputParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditInputFile)
		
		
		self.layoutControls.addWidget(self.groupBoxOutput)

		self.groupBoxOutput.setLayout(self.layoutOutputParams)
		self.layoutOutputParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelFileOutput)
		self.layoutOutputParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutputFile)
		self.layoutOutputParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelExecutor)
		self.layoutOutputParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxExecutor)
		self.layoutOutputParams.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelDate)
		self.layoutOutputParams.setWidget(2, QtGui.QFormLayout.FieldRole, self.dateEditDate)

	def buildCombos(self):
		self.buildcomboBoxExecutor()

	def buildcomboBoxExecutor(self):
		self.comboBoxExecutor.clear()
		surveyors = {}
		surveyors[''] = 0
		#
		cmd = """
			SELECT FIOid, Surname + ' ' + Firstname + ' ' + Suvmiddle as FIO
			FROM [{0}].[dbo].[SURVEY_FIO]
			ORDER BY FIO
		""".format(self.main_app.srvcnxn.dbsvy)
		try:
			data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Ошибка при выполнении запроса имени маркшейдера." + str(e))
			raise 
		if not data:
			return
		for FIOid, FIO in data:
			surveyors[FIO] = FIOid
		self.comboBoxExecutor.addItems(sorted(list(surveyors.keys())))


	def prepare(self):
		if not super(ImportLeicaGsiDialog, self).prepare():
			return

		#
		# проверка заполненности полей

		labels = [self.labelFileInput, self.labelFileOutput]
		lineEdits = [self.lineEditInputFile, self.lineEditOutputFile]
		for label, lineEdit in zip(labels, lineEdits):
			if is_blank(lineEdit.text()):
				qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
				return

		labels = [self.labelExecutor]
		combos = [self.comboBoxExecutor]
		for label, combo in zip(labels, combos):
			if is_blank(combo.currentText()):
				qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
				return


		# проверка директории имени

		if not os.path.exists(self.lineEditInputFile.text()):
			qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не найден" % self.lineEditInputFile.text())
			return
		if not os.path.exists(os.path.dirname(self.lineEditOutputFile.text())):
			qtmsgbox.show_error(self, "Ошибка", "Путь '%s' не найден" % os.path.basename(self.lineEditOutputFile.text()))
			return
						
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "%s" % str(e))

		self.after_run()
		self.close()

	def run(self):
		if not super(ImportLeicaGsiDialog, self).run():
			return

		importLeicaGsi(self.lineEditInputFile.text(), self.lineEditOutputFile.text(),False,'Импорт GSI файла')
		self.__updateStructure()

		if (os.path.exists(self.lineEditOutputFile.text())):
			loadStrings(self.lineEditOutputFile.text(), 'Импорт GSI файла ' + os.path.basename(self.lineEditOutputFile.text()))

		qtmsgbox.show_information(self, "Выполнено", "Файл съёмки создан")

	def select_input_file(self, e, widget):
		filetypes = "Файл съёмки (*.GSI)"
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		try:		initialdir = netpath
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		# ничего не выбрано
		if not path:
			return

		widget.setText(path)

	def select_output_file(self, e, widget):
		filetypes = "Файл полилиний (*.STR)"
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		try:		initialdir = netpath
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getSaveFileName(self, "Сохранить как...", initialdir, filetypes).replace("/", "\\")

		# ничего не выбрано
		if not path:
			return

		widget.setText(path)

	def __updateStructure(self):
		strFile = 	MicromineFile()
		if not strFile.open(self.lineEditOutputFile.text()):
				raise Exception("Невозможно открыть файл %s" % self.lineEditOutputFile.text())

		strStructure = strFile.structure
		strHeader = strFile.header
		if not 'SURVEYOR' in strHeader:
			strStructure.add_field('SURVEYOR', MMpy.FieldType.character, 100, 0)
		if not 'SUR_DATE' in strHeader:
			strStructure.add_field('SUR_DATE', MMpy.FieldType.character, 10, 0)

		strFile.structure = strStructure
		
		strHeader = strFile.header
		try:
			idxSurveyorField = strHeader.index('SURVEYOR')
			idxSurdateField = strHeader.index('SUR_DATE')
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Не найдено необходимое поле." + str(e))
			strFile.close()
			raise Exception (e)
	   
		for irow in range(strFile.records_count):
				strFile.set_field_value(idxSurveyorField, irow+1, self.comboBoxExecutor.currentText())
				strFile.set_field_value(idxSurdateField, irow+1, self.dateEditDate.text())

		strFile.close()



