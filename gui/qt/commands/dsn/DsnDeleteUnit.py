from PyQt4 import QtGui, QtCore
import os, shutil


from utils.constants import *
from utils.osutil import isDirTreeContentsFiles
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow



class DsnDeleteUnit(DialogWindow):
	def __init__(self, parent, dsnUnitItem, use_connection, *args, **kw):
		super(DsnDeleteUnit, self).__init__(parent, use_connection, *args, **kw)
		self._dsnUnitItem = dsnUnitItem
		self._use_connection = use_connection
		self.setWindowTitle("Удалить локальный проект")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._newName = None
	def __createWidgets(self):
		self.layoutParams = QtGui.QVBoxLayout()

		self.labelMessage1 = QtGui.QLabel(self)
		self.labelMessage1.setText("Вы действительно хотите удалить локальный проект:<br/><b>'%s'</b>?"  % (self._dsnUnitItem.name()) )
		self.labelMessage1.setMinimumWidth(100)
		self.labelMessage1.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
		
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.addWidget(self.labelMessage1)



	def check(self):
		return True

	def prepare(self):
		super(DsnDeleteUnit, self).prepare()

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно удалить юнит. %s" % str(e))

		self.after_run()
		if self._is_success:
			self.close()

	def run(self):

		if self._use_connection and self.main_app.srvcnxn is None:
			self.main_app.connect_to_database()

		def restore_filenames():
			nonlocal unitPath, unitTempPath
			if not os.path.exists(unitPath):
				try:		os.rename(unitTempPath, unitPath)
				except:		pass
		#
		unitPath = self._dsnUnitItem.filePath()
		#
		dirpath = os.path.dirname(unitPath)
		unitTempPath = os.path.join(dirpath, self._tmpprefix + self._dsnUnitItem.name() )
	
		departmentName = os.path.split(dirpath)[1]

		if not self.__existLocProjInDb(self._dsnUnitItem.name()):
			qtmsgbox.show_information(self, "Внимание", "Текущий проект отсутствует в БД. Будет удалён только каталог проекта.")

		#
		try:

			cmdSel ="""
			select  LOC_PROJECT_ID  from [{0}].[dbo].[LOCAL_PROJECT]  
				where  LOC_PROJECT_NAME = ?
			""" 
			params = (self._dsnUnitItem.name())
			cmdSel = cmdSel.format(self.main_app.srvcnxn.dbsvy)

			data = self.main_app.srvcnxn.exec_query(cmdSel, params).get_rows()
			if len(data) > 0:
				loc_project_id = data[0][0]

				cmdDel1 ="""
				delete  from [{0}].[dbo].[LOCAL_PROJECT_PANELS]  
					where  LOC_PROJECT_ID = ?
				"""
				params1 = ( loc_project_id)
				cmdDel1 = cmdDel1.format(self.main_app.srvcnxn.dbsvy,)

				cmdDel2 ="""
				delete  from [{0}].[dbo].[LOCAL_PROJECT_DEPARTS]  
					where  LOC_PROJECT_ID = ?
				"""
				params2 = (loc_project_id)
				cmdDel2 = cmdDel2.format(self.main_app.srvcnxn.dbsvy)

				cmdDel2_2 ="""
				delete  from [{0}].[dbo].[LOCAL_PROJECT_WORK_TYPES]  
					where  LOC_PROJECT_ID = ?
				"""
				params2_2 = (loc_project_id)
				cmdDel2_2 = cmdDel2_2.format(self.main_app.srvcnxn.dbsvy)

				cmdDel3 ="""
				delete  from [{0}].[dbo].[LOCAL_PROJECT]  
					where  LOC_PROJECT_ID = ?
				"""
				params3 = (loc_project_id)
				cmdDel3 = cmdDel3.format(self.main_app.srvcnxn.dbsvy)


				try:   
					self.main_app.srvcnxn.exec_query(cmdDel1, params1)
					self.main_app.srvcnxn.exec_query(cmdDel2, params2)
					self.main_app.srvcnxn.exec_query(cmdDel2_2, params2_2)
					self.main_app.srvcnxn.exec_query(cmdDel3, params3)
				except Exception as e:
					raise Exception("Невозможно удалить локадьный проект из БД. %s" % str(e))

			# переименовываем файлы
			try:
				if isDirTreeContentsFiles(unitPath):
					raise Exception("Папка локального проекта содержит файлы. Удаляются только пустые папки.")
				os.rename(unitPath, unitTempPath)
				shutil.rmtree(unitTempPath)	
				
			except Exception as e:
				raise Exception("Невозможно удалить папку локального проекта '%s'" % str(e))
			
			self.main_app.srvcnxn.commit()
			
		except Exception as e:
			# возвращаем имена, если требуется
			restore_filenames()
			self.main_app.srvcnxn.rollback()
			raise Exception(str(e))

		#переименовываем в интерфейсе
		try:
			parent = self._dsnUnitItem.parent()
			parent.removeChild(self._dsnUnitItem)
		except Exception as e:
			raise Exception("Невозможно обновить дерево папок. Перезапустите скрипт. Ошибка: %s" % str(e))

	
		qtmsgbox.show_information(self, "Выполнено", "Юнит успешно удалён")

	def __existLocProjInDb(self, locProjectName):
		cmd ="""
		select * from [{0}].[dbo].[LOCAL_PROJECT]  as LP
			where LP.LOC_PROJECT_NAME = ?
		"""
		params = (locProjectName)
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			self.main_app.srvcnxn.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))
		if data:
			return True
		else:
			return False
		
