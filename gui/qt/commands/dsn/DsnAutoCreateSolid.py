try:			import MMpy
except:			pass

from PyQt4 import QtCore, QtGui
import os

from geometry import  *

from mm.mmutils import Tridb, MicromineFile

from utils.constants import *
from utils.validation import is_blank, in_range, IntValidator, DoubleValidator
from utils.osutil import get_temporary_prefix
from mm.mmutils import get_micromine_version

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def create_solid(strpath, tripath, triname, secpath):
	ExtrudeShape_ExtrudeSolid_FormSet1_Nested0= MMpy.FormSet("EXTRUDE_SHAPE","16.0.912.4")
	ExtrudeShape_ExtrudeSolid_FormSet1_Nested0.set_field("FILTER","0")
	ExtrudeShape_ExtrudeSolid_FormSet1_Nested0.set_field("JOIN_FIELD","JOIN")
	ExtrudeShape_ExtrudeSolid_FormSet1_Nested0.set_field("Y_FIELD","NORTH")
	ExtrudeShape_ExtrudeSolid_FormSet1_Nested0.set_field("X_FIELD","EAST")
	ExtrudeShape_ExtrudeSolid_FormSet1_Nested0.set_field("TYPE","2")
	ExtrudeShape_ExtrudeSolid_FormSet1_Nested0.set_field("FILE", secpath)

	ExtrudeSolid_FormSet1= MMpy.FormSet("EXTRUDE_SOLID","16.0.912.4")
	ExtrudeSolid_FormSet1.set_field("COLOUR2",MMpy.Colour(128,128,128).serialise())
	ExtrudeSolid_FormSet1.set_field("NAME2", triname)
	ExtrudeSolid_FormSet1.set_field("TYPE2", tripath)
	ExtrudeSolid_FormSet1.set_field("ACCURACY","0")
	DataGrid0 = MMpy.DataGrid(3,1)
	DataGrid0.set_column_info(0,1,4)
	DataGrid0.set_column_info(1,2,-1)
	DataGrid0.set_column_info(2,0,8)
	DataGrid0.set_row(0, ["","",""])
	ExtrudeSolid_FormSet1.set_field("GRID",DataGrid0.serialise())
	ExtrudeSolid_FormSet1.set_field("SAVE_TYPE", tripath)
	ExtrudeSolid_FormSet1.set_field("SAVE_COLOUR",MMpy.Colour(128,128,128).serialise())
	ExtrudeSolid_FormSet1.set_field("NAME", triname)
	ExtrudeSolid_FormSet1.set_field("R_FIELD","RIGHT")
	ExtrudeSolid_FormSet1.set_field("L_FIELD","LEFT")
	ExtrudeSolid_FormSet1.set_field("DEFAULT_WIDTH","5")
	ExtrudeSolid_FormSet1.set_field("INDEPENDENT","1")
	ExtrudeSolid_FormSet1.set_field("USE_OFFSET","1")
	ExtrudeSolid_FormSet1.set_field("USE_WIDTH","0")
	ExtrudeSolid_FormSet1.set_field("SEGM","0")
	ExtrudeSolid_FormSet1.set_field("LENGTH","0")
	ExtrudeSolid_FormSet1.set_field("PARTS","0")
	ExtrudeSolid_FormSet1.set_field("AUTO","0")
	ExtrudeSolid_FormSet1.set_field("HORIZ","0")
	ExtrudeSolid_FormSet1.set_field("VERTL","1")
	ExtrudeSolid_FormSet1.set_field("PERP","0")
	ExtrudeSolid_FormSet1.set_field("POLY","1")
	ExtrudeSolid_FormSet1.set_field("WF","0")
	ExtrudeSolid_FormSet1.set_field("VIZEX","0")
	ExtrudeSolid_FormSet1.set_field("ENDS","0")
	ExtrudeSolid_FormSet1.set_field("USER_SHAPE",ExtrudeShape_ExtrudeSolid_FormSet1_Nested0)
	ExtrudeSolid_FormSet1.set_field("HEIGHT_FIELD","HEIGHT")
	ExtrudeSolid_FormSet1.set_field("WIDTH_OFFSET","-1")
	ExtrudeSolid_FormSet1.set_field("OTHER_SWITCH","0")
	ExtrudeSolid_FormSet1.set_field("OTHER","0.6667")
	ExtrudeSolid_FormSet1.set_field("HEIGHT","5")
	ExtrudeSolid_FormSet1.set_field("RECT_WIDTH","2")
	ExtrudeSolid_FormSet1.set_field("SHAPE_SWITCH","6")
	ExtrudeSolid_FormSet1.set_field("JOIN_FIELD","JOIN")
	ExtrudeSolid_FormSet1.set_field("Z_FIELD","RL")
	ExtrudeSolid_FormSet1.set_field("NORTH_FIELD","NORTH")
	ExtrudeSolid_FormSet1.set_field("EAST_FIELD","EAST")
	ExtrudeSolid_FormSet1.set_field("FILTER","0")
	ExtrudeSolid_FormSet1.set_field("EXSOLIDS_TYPE","2")
	ExtrudeSolid_FormSet1.set_field("EXSOLIDS_FILE", strpath)
	ExtrudeSolid_FormSet1.run()
def load_solid(tripath, triname, display_name):

	def defaultLoadTriangulation_2016(tripath, triname, display_name):
		WsLoadTriangulation_FormSet1= MMpy.FormSet("WS_LOAD_TRIANGULATION","16.0.912.4")
		WsLoadTriangulation_FormSet1.set_field("REPEAT_BOOL","0")
		WsLoadTriangulation_FormSet1.set_field("SLICE","0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE","0")
		WsLoadTriangulation_FormSet1.set_field("PLAN","0")
		WsLoadTriangulation_FormSet1.set_field("FONT","0=0=0=0=3=2=1=34TFFF8=0=2147483647=2147483647=15000804=0=0=TT=(0,0,-1)(0,1,0)=TT=30=2=3 Arial")
		WsLoadTriangulation_FormSet1.set_field("FIELD1","Name")
		WsLoadTriangulation_FormSet1.set_field("POLYGON_COLOUR",MMpy.Colour(255,40,40).serialise())
		WsLoadTriangulation_FormSet1.set_field("CHECK","0")
		WsLoadTriangulation_FormSet1.set_field("TYPE","0")
		WsLoadTriangulation_FormSet1.set_field("DEFCOLOUR",MMpy.Colour(0,0,0).serialise())
		WsLoadTriangulation_FormSet1.set_field("GRID_BOOL","0")
		WsLoadTriangulation_FormSet1.set_field("NODRAPE_BOOL","1")
		WsLoadTriangulation_FormSet1.set_field("LINE_COLOUR",MMpy.Colour(0,0,0).serialise())
		WsLoadTriangulation_FormSet1.set_field("THICK","0")
		WsLoadTriangulation_FormSet1.set_field("VALUE","0")
		WsLoadTriangulation_FormSet1.set_field("EDGE_ANGLE_SLIDER","0")
		WsLoadTriangulation_FormSet1.set_field("COLOUR_SET_BOOL","0")
		WsLoadTriangulation_FormSet1.set_field("3DCAPPED","0")
		WsLoadTriangulation_FormSet1.set_field("AUTO","1")
		WsLoadTriangulation_FormSet1.set_field("SET","ОЧЕРЕДЬ")
		WsLoadTriangulation_FormSet1.set_field("SHADING_ANGLE_SLIDER","0")
		WsLoadTriangulation_FormSet1.set_field("GEOREFERENCE_BOOL","1")
		WsLoadTriangulation_FormSet1.set_field("STRETCH_BOOL","0")
		WsLoadTriangulation_FormSet1.set_field("TRANSPARENCY_SLIDER","0")
		WsLoadTriangulation_FormSet1.set_field("IMAGE_BOOL","0")
		WsLoadTriangulation_FormSet1.set_field("3DPOINTS","0")
		WsLoadTriangulation_FormSet1.set_field("WFSET","")
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_SET_BOOL","0")
		WsLoadTriangulation_FormSet1.set_field("SINGLE_BOOL","1")
		WsLoadTriangulation_FormSet1.set_field("HATCH",MMpy.Hatch(MMpy.Colour(0,0,0),MMpy.Colour(255,255,255),MMpy.Colour(0,0,100),False,MMpy.LineType.solid,13,0,"MM Hatch Patterns 1").serialise())
		WsLoadTriangulation_FormSet1.set_field("2DSLICE","0")
		WsLoadTriangulation_FormSet1.set_field("LINES","0")
		WsLoadTriangulation_FormSet1.set_field("3DSHADE","0")
		WsLoadTriangulation_FormSet1.set_field("COLOUR_BOOL","0")
		WsLoadTriangulation_FormSet1.set_field("WFTYPE", tripath)
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_COLOUR",MMpy.Colour(255,0,0).serialise())
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_FILE", triname)
		WsLoadTriangulation_FormSet1.run(display_name)

	def defaultLoadTriangulation_2018(tripath, triname, display_name):
		WsLoadTriangulation_FormSet1 = MMpy.FormSet("WS_LOAD_TRIANGULATION", "18.0.598.0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_CLR_BACK_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_CLR_BACK_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_CLR_FORE_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_CLR_FORE_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_CLR_BORDER_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_CLR_BORDER_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_BORDER_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_BORDER_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_FILL_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_FILL_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("CLR", "0")
		WsLoadTriangulation_FormSet1.set_field("SILHOUETTE_ADV_DFLT_PAT",
											   MMpy.Hatch(MMpy.Colour(0, 0, 0), MMpy.Colour(255, 255, 255),
														  MMpy.Colour(0, 0, 0), False, MMpy.LineType.solid, 13, 0,
														  "MM Hatch Patterns 1").serialise())
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_CLR_BACK_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_CLR_BACK_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_CLR_FORE_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_CLR_FORE_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_CLR_BORDER_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_CLR_BORDER_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_BORDER_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_BORDER_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_FILL_SET_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE_ADV_FILL_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("USER", MMpy.Colour(0, 0, 100).serialise())
		WsLoadTriangulation_FormSet1.set_field("LINECOLOUR", "0")
		WsLoadTriangulation_FormSet1.set_field("LINEWIDTH", "0")
		WsLoadTriangulation_FormSet1.set_field("LINETYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("FOREGROUND", "0")
		WsLoadTriangulation_FormSet1.set_field("DESCENDING", "0")
		WsLoadTriangulation_FormSet1.set_field("ASCENDING", "1")
		WsLoadTriangulation_FormSet1.set_field("SORT_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("POSITION", "1")
		WsLoadTriangulation_FormSet1.set_field("ENDCAP", "0")
		WsLoadTriangulation_FormSet1.set_field("ADVANCED_HATCH_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("WF_COLOUR", "0")
		WsLoadTriangulation_FormSet1.set_field("DEFAULT_HATCH_BOOL", "1")
		WsLoadTriangulation_FormSet1.set_field("PATTERN", MMpy.Hatch(MMpy.Colour(0, 0, 0), MMpy.Colour(255, 255, 255),
																	 MMpy.Colour(0, 0, 0), False, MMpy.LineType.solid,
																	 13, 0, "MM Hatch Patterns 1").serialise())
		WsLoadTriangulation_FormSet1.set_field("HATCH_COLOUR", MMpy.Colour(0, 0, 100).serialise())
		WsLoadTriangulation_FormSet1.set_field("LINE_COLOUR_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("WIDTH", "0")
		WsLoadTriangulation_FormSet1.set_field("LINE_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SIMPLE_HATCH_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_COLOUR_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICESILHOUETTE", "0")
		WsLoadTriangulation_FormSet1.set_field("COLOUR_SET_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("LABEL_COLOUR_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("DS_SILHOUETTE", "0")
		WsLoadTriangulation_FormSet1.set_field("REPEAT_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SLICE", "0")
		WsLoadTriangulation_FormSet1.set_field("AREA_SILHOUETTE", "0")
		WsLoadTriangulation_FormSet1.set_field("PLAN", "0")
		WsLoadTriangulation_FormSet1.set_field("FONT",
											   "0=0=0=0=0=0=0=0FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
		WsLoadTriangulation_FormSet1.set_field("POLYGON_COLOUR", MMpy.Colour(0, 0, 100).serialise())
		WsLoadTriangulation_FormSet1.set_field("CHECK", "0")
		WsLoadTriangulation_FormSet1.set_field("GEOREFERENCE_TYPE", "0")
		WsLoadTriangulation_FormSet1.set_field("DEFCOLOUR", MMpy.Colour(0, 0, 100).serialise())
		WsLoadTriangulation_FormSet1.set_field("GRID_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("NODRAPE_BOOL", "1")
		WsLoadTriangulation_FormSet1.set_field("LINE_COLOUR", MMpy.Colour(0, 0, 100).serialise())
		WsLoadTriangulation_FormSet1.set_field("THICK", "0")
		WsLoadTriangulation_FormSet1.set_field("VALUE", "0")
		WsLoadTriangulation_FormSet1.set_field("EDGE_ANGLE_SLIDER", "0")
		WsLoadTriangulation_FormSet1.set_field("3DCAPPED", "0")
		WsLoadTriangulation_FormSet1.set_field("AUTO", "0")
		WsLoadTriangulation_FormSet1.set_field("SHADING_ANGLE_SLIDER", "0")
		WsLoadTriangulation_FormSet1.set_field("GEOREFERENCE_BOOL", "1")
		WsLoadTriangulation_FormSet1.set_field("STRETCH_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("BLUE_BAND", "0")
		WsLoadTriangulation_FormSet1.set_field("GREEN_BAND", "1")
		WsLoadTriangulation_FormSet1.set_field("RED_BAND", "2")
		WsLoadTriangulation_FormSet1.set_field("TRANSPARENCY_SLIDER", "0")
		WsLoadTriangulation_FormSet1.set_field("IMAGE_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("3DPOINTS", "0")
		WsLoadTriangulation_FormSet1.set_field("WFSET", "[S]408_5")
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_SET_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("SINGLE_BOOL", "1")
		WsLoadTriangulation_FormSet1.set_field("2DSLICE", "0")
		WsLoadTriangulation_FormSet1.set_field("LINES", "0")
		WsLoadTriangulation_FormSet1.set_field("3DSHADE", "1")
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_COLOUR_BOOL", "0")
		WsLoadTriangulation_FormSet1.set_field("WFTYPE", tripath)
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_COLOUR", MMpy.Colour(0, 0, 100).serialise())
		WsLoadTriangulation_FormSet1.set_field("TRIANGULATION_FILE", triname)

		WsLoadTriangulation_FormSet1.run(display_name)

	major, minor, servicepack, build = get_micromine_version()
	if major > 16:
		defaultLoadTriangulation_2018(tripath, triname, display_name)
	else:
		defaultLoadTriangulation_2016(tripath, triname, display_name)

def load_sections(strpath, display_name):
	WsLoadStrings_FormSet1= MMpy.FormSet("WS_LOAD_STRINGS","16.0.912.4")
	WsLoadStrings_FormSet1.set_field("TRANSPARENCY","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ANGLE_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("PLAN","0")
	WsLoadStrings_FormSet1.set_field("CHECK_ANGLE_FIELD","0")
	WsLoadStrings_FormSet1.set_field("DISPLAY","0")
	WsLoadStrings_FormSet1.set_field("POLYGON_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("CHECK","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT","0")
	WsLoadStrings_FormSet1.set_field("SINGLE_POINTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("HATCH",MMpy.Hatch(MMpy.Colour(0,0,0),MMpy.Colour(255,255,255),MMpy.Colour(0,0,0),False,MMpy.LineType.solid,13,0,"MM Hatch Patterns 1").serialise())
	WsLoadStrings_FormSet1.set_field("POLYGONS_BOOL","0")
	WsLoadStrings_FormSet1.set_field("HATCH_SET","[S]2")
	WsLoadStrings_FormSet1.set_field("HATCH_FIELD","HATCH")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ON","0")
	WsLoadStrings_FormSet1.set_field("BELOW","0")
	WsLoadStrings_FormSet1.set_field("ABOVE","1")
	WsLoadStrings_FormSet1.set_field("POINT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("POINT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("POINT_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("INCLINATION_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("BEARING_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INCLINATION","0")
	WsLoadStrings_FormSet1.set_field("BEARING","0")
	WsLoadStrings_FormSet1.set_field("CUM_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INT_ANGLE","0")
	WsLoadStrings_FormSet1.set_field("SEQ","0")
	WsLoadStrings_FormSet1.set_field("Z","0")
	WsLoadStrings_FormSet1.set_field("Y","0")
	WsLoadStrings_FormSet1.set_field("X","0")
	WsLoadStrings_FormSet1.set_field("SEGMENTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_POINTS","0")
	WsLoadStrings_FormSet1.set_field("SHOW_POINTS","0")
	WsLoadStrings_FormSet1.set_field("LINE_TYPE","1")
	WsLoadStrings_FormSet1.set_field("JOIN_VARB","JOIN")
	WsLoadStrings_FormSet1.set_field("THICK","0")
	WsLoadStrings_FormSet1.set_field("DEFAULT_COLOUR",MMpy.Colour(0,0,255).serialise())
	WsLoadStrings_FormSet1.set_field("Z_VARB","RL")
	WsLoadStrings_FormSet1.set_field("Y_VARB","NORTH")
	WsLoadStrings_FormSet1.set_field("X_VARB","EAST")
	WsLoadStrings_FormSet1.set_field("FLTNO","0")
	WsLoadStrings_FormSet1.set_field("FILTER","0")
	WsLoadStrings_FormSet1.set_field("FILE_TYPE","2")
	WsLoadStrings_FormSet1.set_field("WSSTR_FILE", strpath)
	WsLoadStrings_FormSet1.run(display_name)
def load_axis(strpath, display_name):
	WsLoadStrings_FormSet1= MMpy.FormSet("WS_LOAD_STRINGS","16.0.912.4")
	WsLoadStrings_FormSet1.set_field("TRANSPARENCY","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ANGLE_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("PLAN","0")
	WsLoadStrings_FormSet1.set_field("CHECK_ANGLE_FIELD","0")
	WsLoadStrings_FormSet1.set_field("DISPLAY","0")
	WsLoadStrings_FormSet1.set_field("POLYGON_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("CHECK","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT","0")
	WsLoadStrings_FormSet1.set_field("SINGLE_POINTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("HATCH",MMpy.Hatch(MMpy.Colour(0,0,0),MMpy.Colour(255,255,255),MMpy.Colour(0,0,0),False,MMpy.LineType.solid,13,0,"MM Hatch Patterns 1").serialise())
	WsLoadStrings_FormSet1.set_field("POLYGONS_BOOL","0")
	WsLoadStrings_FormSet1.set_field("HATCH_SET","[S]2")
	WsLoadStrings_FormSet1.set_field("HATCH_FIELD","HATCH")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ON","0")
	WsLoadStrings_FormSet1.set_field("BELOW","0")
	WsLoadStrings_FormSet1.set_field("ABOVE","1")
	WsLoadStrings_FormSet1.set_field("POINT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("POINT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("POINT_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("INCLINATION_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("BEARING_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INCLINATION","0")
	WsLoadStrings_FormSet1.set_field("BEARING","0")
	WsLoadStrings_FormSet1.set_field("CUM_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INT_ANGLE","0")
	WsLoadStrings_FormSet1.set_field("SEQ","0")
	WsLoadStrings_FormSet1.set_field("Z","0")
	WsLoadStrings_FormSet1.set_field("Y","0")
	WsLoadStrings_FormSet1.set_field("X","0")
	WsLoadStrings_FormSet1.set_field("SEGMENTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_POINTS","0")
	WsLoadStrings_FormSet1.set_field("SHOW_POINTS","1")
	WsLoadStrings_FormSet1.set_field("LINE_TYPE","0")
	WsLoadStrings_FormSet1.set_field("JOIN_VARB","JOIN")
	WsLoadStrings_FormSet1.set_field("THICK","1")
	WsLoadStrings_FormSet1.set_field("DEFAULT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("Z_VARB","RL")
	WsLoadStrings_FormSet1.set_field("Y_VARB","NORTH")
	WsLoadStrings_FormSet1.set_field("X_VARB","EAST")
	WsLoadStrings_FormSet1.set_field("FLTNO","0")
	WsLoadStrings_FormSet1.set_field("FILTER","0")
	WsLoadStrings_FormSet1.set_field("FILE_TYPE","2")
	WsLoadStrings_FormSet1.set_field("WSSTR_FILE", strpath)
	WsLoadStrings_FormSet1.run(display_name)
def load_intersections(ptspath, display_name):
	WsLoadPoints_FormSet1= MMpy.FormSet("WS_LOAD_POINTS","16.0.912.4")
	DataGrid0 = MMpy.DataGrid(9,1)
	DataGrid0.set_column_info(0,0,14)
	DataGrid0.set_column_info(1,1,4)
	DataGrid0.set_column_info(2,2,-1)
	DataGrid0.set_column_info(3,3,-1)
	DataGrid0.set_column_info(4,4,-1)
	DataGrid0.set_column_info(5,5,-1)
	DataGrid0.set_column_info(6,6,1)
	DataGrid0.set_column_info(7,7,4)
	DataGrid0.set_column_info(8,8,15)
	DataGrid0.set_row(0, ["1","","","","","",MMpy.Colour(0,0,100).serialise(),"",""])
	WsLoadPoints_FormSet1.set_field("GRID",DataGrid0.serialise())
	WsLoadPoints_FormSet1.set_field("USE_POSFIELD","0")
	WsLoadPoints_FormSet1.set_field("LABELS","0")
	WsLoadPoints_FormSet1.set_field("USE_ANGLEFIELD","1")
	WsLoadPoints_FormSet1.set_field("ANNOTATION_ANGLEFIELD","ANGLE")
	WsLoadPoints_FormSet1.set_field("MIP","0")
	WsLoadPoints_FormSet1.set_field("FONT","0=0=0=1=3=2=1=34FFFF1.5=1=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FT=5=2=3 Arial")
	WsLoadPoints_FormSet1.set_field("LOG_BOOL","0")
	WsLoadPoints_FormSet1.set_field("RANGE_BOOL","0")
	WsLoadPoints_FormSet1.set_field("FACTOR_BOOL","1")
	WsLoadPoints_FormSet1.set_field("SYMBOLS_BOOL","1")
	WsLoadPoints_FormSet1.set_field("UP","0")
	WsLoadPoints_FormSet1.set_field("SYMBOLS","0")
	WsLoadPoints_FormSet1.set_field("POINTS","1")
	WsLoadPoints_FormSet1.set_field("DEFAULT","1.5")
	WsLoadPoints_FormSet1.set_field("SYMBOL",MMpy.Symbol("Mm Symbols",123,12,"",False,False,0,MMpy.BorderType.no_border,MMpy.Colour(0,0,0),False).serialise())
	WsLoadPoints_FormSet1.set_field("COLOUR",MMpy.Colour(255,0,0).serialise())
	WsLoadPoints_FormSet1.set_field("Z_VARB","RL")
	WsLoadPoints_FormSet1.set_field("Y_VARB","NORTH")
	WsLoadPoints_FormSet1.set_field("X_VARB","EAST")
	WsLoadPoints_FormSet1.set_field("FLTNO","0")
	WsLoadPoints_FormSet1.set_field("FILTER","0")
	WsLoadPoints_FormSet1.set_field("TYPE","0")
	WsLoadPoints_FormSet1.set_field("WSPTS_FILE", ptspath)
	WsLoadPoints_FormSet1.run(display_name)
def load_string(strpath, display_name):
	WsLoadStrings_FormSet1= MMpy.FormSet("WS_LOAD_STRINGS","16.0.922.4")
	WsLoadStrings_FormSet1.set_field("TRANSPARENCY","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ANGLE_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("PLAN","0")
	WsLoadStrings_FormSet1.set_field("CHECK_ANGLE_FIELD","0")
	WsLoadStrings_FormSet1.set_field("DISPLAY","0")
	WsLoadStrings_FormSet1.set_field("POLYGON_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("FIELD_ANGLE","0")
	WsLoadStrings_FormSet1.set_field("CHECK","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT","0")
	WsLoadStrings_FormSet1.set_field("SINGLE_POINTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("HATCH",MMpy.Hatch(MMpy.Colour(0,0,0),MMpy.Colour(255,255,255),MMpy.Colour(0,0,0),False,MMpy.LineType.solid,13,0,"MM Hatch Patterns 1").serialise())
	WsLoadStrings_FormSet1.set_field("POLYGONS_BOOL","0")
	WsLoadStrings_FormSet1.set_field("HATCH_SET","[S]2")
	WsLoadStrings_FormSet1.set_field("HATCH_FIELD","HATCH")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ON","0")
	WsLoadStrings_FormSet1.set_field("BELOW","0")
	WsLoadStrings_FormSet1.set_field("ABOVE","1")
	WsLoadStrings_FormSet1.set_field("POINT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("POINT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("POINT_FONT","0=0=0=1=3=2=1=34FFFF8.000=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("INCLINATION_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("BEARING_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INCLINATION","0")
	WsLoadStrings_FormSet1.set_field("BEARING","0")
	WsLoadStrings_FormSet1.set_field("CUM_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INT_ANGLE","0")
	WsLoadStrings_FormSet1.set_field("SEQ","0")
	WsLoadStrings_FormSet1.set_field("Z","0")
	WsLoadStrings_FormSet1.set_field("Y","0")
	WsLoadStrings_FormSet1.set_field("X","0")
	WsLoadStrings_FormSet1.set_field("SEGMENTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_POINTS","0")
	WsLoadStrings_FormSet1.set_field("SHOW_POINTS","1")
	WsLoadStrings_FormSet1.set_field("LINE_TYPE","0")
	WsLoadStrings_FormSet1.set_field("JOIN_VARB","")
	WsLoadStrings_FormSet1.set_field("STRING_VARB","")
	WsLoadStrings_FormSet1.set_field("THICK","1")
	WsLoadStrings_FormSet1.set_field("DEFAULT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("Z_VARB","")
	WsLoadStrings_FormSet1.set_field("Y_VARB","NORTH")
	WsLoadStrings_FormSet1.set_field("X_VARB","EAST")
	WsLoadStrings_FormSet1.set_field("FILTER","0")
	WsLoadStrings_FormSet1.set_field("FILE_TYPE","2")
	WsLoadStrings_FormSet1.set_field("WSSTR_FILE", strpath)
	WsLoadStrings_FormSet1.run(display_name)

class DsnAutoCreateSolid(DialogWindow):
	def __init__(self, *args, **kw):
		super(DsnAutoCreateSolid, self).__init__(*args, **kw)

		self.setWindowTitle("Автоматическое построение выработки")
		# self.setFixedSize(QtCore.QSize(500, 390))

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(5)

		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.layoutInput = QtGui.QFormLayout()

		self.labelStrings = QtGui.QLabel(self.groupBoxInput)
		self.labelStrings.setMinimumSize(QtCore.QSize(110, 20))
		self.labelStrings.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelStrings.setText("Файл стрингов")
		self.lineEditStrings = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditStrings.setMinimumWidth(300)
		
		self.labelSection = QtGui.QLabel(self.groupBoxInput)
		self.labelSection.setMinimumSize(QtCore.QSize(110, 20))
		self.labelSection.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSection.setText("Файл сечения")
		self.lineEditSection = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditSection.setMinimumWidth(300)
		
		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры")
		self.layoutParams = QtGui.QFormLayout()

		self.labelNumPoints = QtGui.QLabel(self.groupBoxParams)
		self.labelNumPoints.setMinimumSize(QtCore.QSize(110, 20))
		self.labelNumPoints.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelNumPoints.setText("Число точек")
		self.lineEditNumPoints = QtGui.QLineEdit(self.groupBoxParams)
		self.lineEditNumPoints.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditNumPoints.setMaximumWidth(120)
		self.lineEditNumPoints.setMinimumWidth(120)
		self.lineEditNumPoints.setValidator(IntValidator(2, 100))
		self.lineEditNumPoints.setText("20")
		
		self.labelMinDistance = QtGui.QLabel(self.groupBoxParams)
		self.labelMinDistance.setMinimumSize(QtCore.QSize(110, 20))
		self.labelMinDistance.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelMinDistance.setText("Мин. расстояние")
		self.lineEditMinDistance = QtGui.QLineEdit(self.groupBoxParams)
		self.lineEditMinDistance.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditMinDistance.setMaximumWidth(120)
		self.lineEditMinDistance.setMinimumWidth(120)
		self.lineEditMinDistance.setValidator(DoubleValidator(0, 1000, 3))
		self.lineEditMinDistance.setText("0.5")
		
		self.labelBasisLines = QtGui.QLabel(self.groupBoxParams)
		self.labelBasisLines.setMinimumSize(QtCore.QSize(110, 20))
		self.labelBasisLines.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelBasisLines.setText("Опорные линии")
		self.comboBoxBasisStrings = QtGui.QComboBox(self.groupBoxParams)
		self.comboBoxBasisStrings.setMaximumWidth(120)
		self.comboBoxBasisStrings.setMinimumWidth(120)
		self.comboBoxBasisStrings.addItem("Кровля/Подошва")
		self.comboBoxBasisStrings.addItem("Борт")
		
		self.checkBoxReverseAxis = QtGui.QCheckBox(self.groupBoxParams)
		self.checkBoxReverseAxis.setText("Развернуть ось")


		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QVBoxLayout()
		
		self.groupBoxOutIntersection = QtGui.QGroupBox(self.groupBoxOutput)
		self.groupBoxOutIntersection.setCheckable(True)
		self.groupBoxOutIntersection.setChecked(False)
		self.groupBoxOutIntersection.setTitle("Создать файл точек пересечений")
		self.layoutOutIntersection = QtGui.QFormLayout()
		self.labelOutIntersection = QtGui.QLabel(self.groupBoxOutIntersection)
		self.labelOutIntersection.setMinimumWidth(100)
		self.labelOutIntersection.setText("Файл")
		self.labelOutIntersection.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutIntersection = QtGui.QLineEdit(self.groupBoxOutIntersection)
		self.lineEditOutIntersection.setMinimumWidth(300)

		self.groupBoxOutSections = QtGui.QGroupBox(self.groupBoxOutput)
		self.groupBoxOutSections.setCheckable(True)
		self.groupBoxOutSections.setChecked(False)
		self.groupBoxOutSections.setTitle("Создать файл сечений")
		self.layoutOutSections = QtGui.QFormLayout()
		self.labelOutSections = QtGui.QLabel(self.groupBoxOutSections)
		self.labelOutSections.setMinimumWidth(100)
		self.labelOutSections.setText("Файл")
		self.labelOutSections.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutSections = QtGui.QLineEdit(self.groupBoxOutSections)
		self.lineEditOutSections.setMinimumWidth(300)

		self.groupBoxOutAxis = QtGui.QGroupBox(self.groupBoxOutput)
		self.groupBoxOutAxis.setCheckable(True)
		self.groupBoxOutAxis.setChecked(False)
		self.groupBoxOutAxis.setTitle("Создать файл осевой линии")
		self.layoutOutAxis = QtGui.QFormLayout()
		self.labelOutAxis = QtGui.QLabel(self.groupBoxOutAxis)
		self.labelOutAxis.setMinimumWidth(100)
		self.labelOutAxis.setText("Файл")
		self.labelOutAxis.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutAxis = QtGui.QLineEdit(self.groupBoxOutAxis)
		self.lineEditOutAxis.setMinimumWidth(300)

		self.groupBoxOutWireframe = QtGui.QGroupBox(self.groupBoxOutput)
		self.groupBoxOutWireframe.setCheckable(True)
		self.groupBoxOutWireframe.setChecked(True)
		self.groupBoxOutWireframe.setTitle("Создать каркас выработки")
		self.layoutOutWireframe = QtGui.QFormLayout()	
		self.labelOutTridb = QtGui.QLabel(self.groupBoxOutWireframe)
		self.labelOutTridb.setMinimumWidth(100)
		self.labelOutTridb.setText("Тип")
		self.labelOutTridb.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutTridb = QtGui.QLineEdit(self.groupBoxOutWireframe)
		self.lineEditOutTridb.setMinimumWidth(300)
		self.labelOutWireframe = QtGui.QLabel(self.groupBoxOutWireframe)
		self.labelOutWireframe.setMinimumWidth(100)
		self.labelOutWireframe.setText("Имя")
		self.labelOutWireframe.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutWireframe = QtGui.QLineEdit(self.groupBoxOutWireframe)
		self.lineEditOutWireframe.setMinimumWidth(300)

		self.checkBoxOutputAutoload = QtGui.QCheckBox(self.groupBoxParams)
		self.checkBoxOutputAutoload.setText("Автозагрузка")
		self.checkBoxOutputAutoload.setChecked(True)
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

		self.lineEditStrings.mouseDoubleClickEvent = lambda e: self._browse_openfilename(self.lineEditStrings)
		self.lineEditSection.mouseDoubleClickEvent = lambda e: self._browse_openfilename(self.lineEditSection)
		self.lineEditOutIntersection.mouseDoubleClickEvent = lambda e: self._browse_savefilename(self.lineEditOutIntersection)
		self.lineEditOutSections.mouseDoubleClickEvent = lambda e: self._browse_savefilename(self.lineEditOutSections)
		self.lineEditOutAxis.mouseDoubleClickEvent = lambda e: self._browse_savefilename(self.lineEditOutAxis)
		self.lineEditOutTridb.mouseDoubleClickEvent = lambda e: self._browse_openfilename(self.lineEditOutTridb)
		self.lineEditOutWireframe.mouseDoubleClickEvent = lambda e: self._select_wireframe()
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)
		self.groupBoxInput.setLayout(self.layoutInput)
		self.layoutInput.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelStrings)
		self.layoutInput.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditStrings)
		self.layoutInput.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSection)
		self.layoutInput.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditSection)

		self.layoutControls.addWidget(self.groupBoxParams)
		self.groupBoxParams.setLayout(self.layoutParams)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelNumPoints)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditNumPoints)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelMinDistance)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditMinDistance)
		self.layoutParams.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelBasisLines)
		self.layoutParams.setWidget(2, QtGui.QFormLayout.FieldRole, self.comboBoxBasisStrings)
		self.layoutParams.setWidget(3, QtGui.QFormLayout.FieldRole, self.checkBoxReverseAxis)

		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)
		
		self.layoutOutput.addWidget(self.groupBoxOutIntersection)
		self.groupBoxOutIntersection.setLayout(self.layoutOutIntersection)
		self.layoutOutIntersection.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutIntersection)
		self.layoutOutIntersection.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutIntersection)

		self.layoutOutput.addWidget(self.groupBoxOutSections)
		self.groupBoxOutSections.setLayout(self.layoutOutSections)
		self.layoutOutSections.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutSections)
		self.layoutOutSections.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutSections)

		self.layoutOutput.addWidget(self.groupBoxOutAxis)
		self.groupBoxOutAxis.setLayout(self.layoutOutAxis)
		self.layoutOutAxis.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutAxis)
		self.layoutOutAxis.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutAxis)

		self.layoutOutput.addWidget(self.groupBoxOutWireframe)
		self.groupBoxOutWireframe.setLayout(self.layoutOutWireframe)
		self.layoutOutWireframe.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTridb)
		self.layoutOutWireframe.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutTridb)
		self.layoutOutWireframe.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelOutWireframe)
		self.layoutOutWireframe.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditOutWireframe)

		self.layoutOutput.addWidget(self.checkBoxOutputAutoload)

	def _browse_openfilename(self, widget):
		if not widget.isEnabled():
			return

		if widget == self.lineEditOutTridb:		filetypes = "КАРКАС (*.TRIDB)"
		else:									filetypes = "СТРИНГ (*.STR);;ДАННЫЕ (*.DAT)"
		initialdir = os.path.dirname(widget.text())
		try:		initialdir = MMpy.Project.path() if not initialdir else initialdir
		except:		initialdir = os.getcwd() if not initialdir else initialdir
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		if not path:
			return
		widget.setText(path)

	def _browse_savefilename(self, widget):
		if not widget.isEnabled():
			return

		if widget == self.lineEditOutIntersection:	filetypes = "ДАННЫЕ (*.DAT);;СТРИНГ (*.STR)"
		else:										filetypes = "СТРИНГ (*.STR);;ДАННЫЕ (*.DAT)"
		initialdir = os.path.dirname(widget.text())
		try:		initialdir = MMpy.Project.path() if not initialdir else initialdir
		except:		initialdir = os.getcwd() if not initialdir else initialdir
		path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		if not path:
			return
		widget.setText(path)

	def _select_wireframe(self):
		tridbPath = self.lineEditOutTridb.text()
		if not os.path.exists(tridbPath):
			qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не найден" % tridbPath)
			self.lineEditOutTridb.setFocus()
			return
		tridb = Tridb(tridbPath)
		wireframe_names = tridb.wireframe_names
		#
		dialog = SelectFromListDialog(self)
		dialog.setWindowTitle("Выберите каркас")
		dialog.labelTitle.setText("Выберите каркас")
		dialog.setValues(tridb.wireframe_names)
		dialog.exec()

		result = dialog.getValues()
		if not result:
			return

		self.lineEditOutWireframe.setText(result[0])


	def prepare(self):
		if not super(DsnAutoCreateSolid, self).prepare():
			return

		reqlabels = (self.labelStrings, self.labelSection, self.labelNumPoints, self.labelMinDistance)
		reqfields = (self.lineEditStrings, self.lineEditSection, self.lineEditNumPoints, self.lineEditMinDistance)
		for label, field in zip(reqlabels, reqfields):
			if is_blank(field.text()):
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				field.setFocus()
				return

		#
		srcpath = self.lineEditStrings.text()
		if not os.path.exists(srcpath):
			qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не существует" % srcpath)
			self.lineEditStrings.setFocus()
			return
		secpath = self.lineEditSection.text()
		if not os.path.exists(secpath):
			qtmsgbox.show_error(self, "Ошибка", "Сечение '%s' не найдено" % secpath)
			self.lineEditSection.setFocus()
			return
		#
		numpts = int(self.lineEditNumPoints.text())
		if not in_range(numpts, 2, 100):
			qtmsgbox.show_error(self, "Ошибка", "Неверное число точек")
			self.lineEditNumPoints.setFocus()
			return
		mindist = float(self.lineEditMinDistance.text())
		if not in_range(mindist, 0, 1000):
			qtmsgbox.show_error(self, "Ошибка", "Неверное минимальное расстояние")
			self.lineEditMinDistance.setFocus()
			return

		#
		groupBoxes = [self.groupBoxOutIntersection, self.groupBoxOutSections, self.groupBoxOutAxis, self.groupBoxOutWireframe]
		fields = [self.lineEditOutIntersection, self.lineEditOutSections, self.lineEditOutAxis, self.lineEditOutTridb]
		labels = [self.labelOutIntersection, self.labelOutSections, self.labelOutAxis, self.labelOutTridb]
		for groupBox, label, field in zip(groupBoxes, labels, fields):
			if groupBox.isChecked():
				path = field.text()
				dirname = os.path.dirname(path)
				if not dirname or not os.path.exists(dirname):
					msg = "Неверное значение '%s'. Директория '%s' не найдена" % (label.text(), dirname)
					qtmsgbox.show_error(self, "Ошибка", msg)
					field.setFocus()
					return
				elif field == self.lineEditOutTridb:
					wfName = self.lineEditOutWireframe.text()
					tridb = Tridb(path)
					wireframe_names = tridb.wireframe_names
					tridb.close()
					#
					if not wfName:
						qtmsgbox.show_error(self, "Ошибка", "Каркас '%s' не найден" % wfName)
						self.lineEditOutWireframe.setFocus()
						return
					elif wfName in wireframe_names and not qtmsgbox.show_question(self, "Внимание", "Каркас '%s' уже существует. Заменить?" % wfName):
						return

		#
		self.main_app.mainWidget.progressBar.setRange(0,0)

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Не удалось построить выработку. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(DsnAutoCreateSolid, self).run():
			return

		srcpath = self.lineEditStrings.text()
		display_name = os.path.basename(srcpath)
		display_name = display_name[:display_name.rfind(".")] if "." in display_name else display_name
		npoints = int(self.lineEditNumPoints.text())
		secdist = float(self.lineEditMinDistance.text())

		# способ аппроксимации осевой линии
		axmode = "horizontal"  if self.comboBoxBasisStrings.currentText() == "Борт" else "vertical"

		srcpath = srcpath + ".STR" if not srcpath.lower().endswith(".str") else srcpath
		secpath = self.lineEditSection.text()
		secname = os.path.basename(secpath)

		reverse = self.checkBoxReverseAxis.isChecked()

		outIntersectionsPath = self.lineEditOutIntersection.text()
		outSectionPath = self.lineEditOutSections.text()
		outSectionPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "СЕЧЕНИЯ.STR") if not outSectionPath else outSectionPath
		outAxisPath = self.lineEditOutAxis.text()
		outAxisPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "ОСЕВАЯ.STR") if not outAxisPath else outAxisPath
		outTridbPath = self.lineEditOutTridb.text()

		progress = self.main_app.mainWidget.progressBar

				### =================================================================================== ###
		### ================================== ЧТЕНИЕ ФАЙЛОВ ================================== ###
		### =================================================================================== ###

		progress.setText("Чтение файлов")

		# файл сечений
		f = MicromineFile()
		if not f.open(secpath):				raise Exception("Не удалось открыть файл %s" % secpath)
		try:								section = LineString([row + [0] for row in f.read(columns = ["EAST", "NORTH"])])
		except Exception as e:				raise Exception("Не удалось считать файл %s. %s" % (secpath, str(e)))
		f.close()
		# файл съемки
		f = MicromineFile()
		if not f.open(srcpath):				raise Exception("Невозможно открыть файл %s" % srcpath)
		try:								source = f.read(columns = ["EAST", "NORTH", "RL", "JOIN"], asstr = True)
		except Exception as e:				raise Exception("Невозможно считать файл %s. %s" % (srcpath, str(e)))
		f.close()

		### =================================================================================== ###
		### ============================== ПРОВЕРКА ДАННЫХ СЪЕМКИ ============================= ###
		### =================================================================================== ###

		progress.setText("Проверка данных съемки")

		xx, yy, zz, jj = zip(*source)
		joins = list(set(jj))
		if len(joins) != 4:
			raise Exception("Стринг содержит неверное число линий")

		strings = {join: [] for join in joins}
		for row in source:
			x, y, z, join = row
			if "nan" in [x,y,z]:
				raise Exception("Стринг содержит незаполненные координаты")
			try:							strings[join].append([float(x), float(y), float(z)])
			except Exception as e:			raise Exception("Невозможно конвертировать данные в число. %s" % str(e))

		# конвертируем в LineString
		strings = {join: LineString(strings[join]) for join in joins}

		### =================================================================================== ###
		### ================== РАЗВОРАЧИВАЕМ ВСЕ СТРИНГИ В ОДНОМ НАПРАВЛЕНИИ ================== ###
		### =================================================================================== ###

		progress.setText("Разворот линий в одном направлении")
		n = len(strings)
		pt = strings[joins[0]][0]
		for i in range(1, n):
			d1 = pt.distance(strings[joins[i]][0])
			d2 = pt.distance(strings[joins[i]][-1])
			if d1 > d2:
				strings[joins[i]].reverse(inplace = True)

		### =================================================================================== ###
		### =================== РАЗДЕЛЕНИЕ СЪЕМКИ НА БОРТА И КРОВЛЮ/ПОДОШВУ =================== ###
		### =================================================================================== ###

		progress.setText("Разделение стрингов")

		_strings = {}
		n = 20
		for join in joins:
			step = strings[join].length / (n+1)
			_strings[join] = LineString([strings[join].interpolate(i*step) for i in range(n+2)])

		# определение кровли/подошвы
		tmpax = LineString(list(map(lambda p1, p2: 0.5*(p1.to_vector()+p2.to_vector()), _strings[joins[0]], _strings[joins[1]])))
		for i, segment in enumerate(tmpax.segments):
			vector = segment.vector.copy()
			vector.z = 0
			plane = Plane(vector, tmpax[i])
			intersections = []
			for join in joins:
				intersection = plane.intersection(_strings[join])
				if intersection:
					intersections.append([intersection, join])
			if len(intersections) == 4:
				break

		zvalues = sorted([[pt.z, join, pt] for pt, join in intersections])
		roof_join = zvalues[-1][1]
		floor_join = zvalues[0][1]
		rbjoins = [roof_join, floor_join]	

		# поля join для кровли/подошвы
		vside1, vside2 = strings[rbjoins[0]], strings[rbjoins[1]]
		# поля join для бортов
		rljoins = [join for join in joins if not join in rbjoins]
		hside1, hside2 = strings[rljoins[0]], strings[rljoins[1]]

		print (roof_join, floor_join)
		print (rljoins)

		del _strings

		# вставляем точки для аппроксимации осевой линии
		stepH1 = hside1.length / (npoints+1)
		stepH2 = hside2.length / (npoints+1)
		stepV1 = vside1.length / (npoints+1)
		stepV2 = vside2.length / (npoints+1)
		_hside1, _hside2 = [], []
		_vside1, _vside2 = [], []
		for i in range(npoints+2):
			_hside1.append(hside1.interpolate(i*stepH1))
			_hside2.append(hside2.interpolate(i*stepH2))
			_vside1.append(vside1.interpolate(i*stepV1))
			_vside2.append(vside2.interpolate(i*stepV2))
		hside1, hside2 = LineString(_hside1), LineString(_hside2)
		vside1, vside2 = LineString(_vside1), LineString(_vside2)

		del _hside1, _hside2, _vside1, _vside2, stepH1, stepH2, stepV1, stepV2

		#
		if axmode == "horizontal":		side1, side2 = hside1.copy(), hside2.copy()
		elif axmode == "vertical":		side1, side2 = vside1.copy(), vside2.copy()

		### =================================================================================== ###
		### ============================= ВЫЧИСЛЕНИЕ ОСЕВОЙ ЛИНИИ ============================= ###
		### =================================================================================== ###

		progress.setText("Вычисление осевой линии")
		# осевая линия
		axis = LineString(list(map(lambda p1, p2: 0.5*(p1.to_vector()+p2.to_vector()), side1, side2)))
		if reverse:
			axis.reverse(inplace = True)

		### =================================================================================== ###
		### =============================== ОПРЕДЕЛЕНИЕ СТОРОН ================================ ###
		### =================================================================================== ###

		progress.setText("Определение сторон")
		sides = {}
		for join in rljoins:
			v = axis.segments[0].vector**(strings[join][0].to_vector() - axis[0].to_vector())
			if v.z < 0:			sides[join] = "RIGHT"
			elif v.z > 0:		sides[join] = "LEFT"
			else:						raise Exception("Неверное расположение стрингов")
		for join in rbjoins:
			if sum(map(lambda v1, v2: v1.z - v2.z, vside1, axis)) < 0:		sides[rbjoins[0]], sides[rbjoins[1]] = "BOTTOM", "ROOF"
			elif sum(map(lambda v1, v2: v1.z - v2.z, vside1, axis)) > 0:	sides[rbjoins[0]], sides[rbjoins[1]] = "ROOF", "BOTTOM"
			else:						raise Exception("Неверное расположение стрингов")


		### =================================================================================== ###
		### ======================== РАСЧЕТ ПРОЕКЦИЙ НА ОСЕВУЮ ЛИНИЮ ========================== ###
		### =================================================================================== ###

		progress.setText("Расчет проекций")
		projections = []
		for join in strings:
			v = axis.segments[0].vector**(strings[join][0].to_vector() - axis[0].to_vector())
			vabs = list(map(abs, v.coords))
			imaxcoord = vabs.index(max(vabs))	
			if v.coords[imaxcoord] == 0:
				raise Exception("Неверное расположение стрингов")
			# поиск проекций
			for i, pt in enumerate(strings[join]):
				proj = axis.projection(pt, mode = "vertical")
				if proj:		projections.append(proj)

		# осевая линия, состоящая только из точек проекций
		newaxisData = [projections[pt[1]] for pt in sorted([[axis.project(proj), i] for i, proj in enumerate(projections)])]
		newaxis = LineString(newaxisData)
		del newaxisData
		#MicromineFile.write(MMpy.Project.path() + "PROJECTIONS.DAT", newaxis.tolist(), ["EAST", "NORTH", "RL", "JOIN"])

		# прореживание сечений
		i = len(newaxis)-1
		pt1 = newaxis[i]
		for i in range(len(newaxis)-2, 1, -1):
			pt2 = newaxis[i-1]
			d = pt1.distance(pt2)
			if d < secdist:	newaxis.remove(i-1)
			else:						pt1 = newaxis[i-1]

		### =================================================================================== ###
		### =============================== РАСЧЕТ СЕЧЕНИЙ ==================================== ###
		### =================================================================================== ###

		progress.setText("Расчет сечений")
		# определение исходных размеров сечения
		srcx, srcy, srcz = section.x, section.y, section.z
		xmax, xmin = max(srcx), min(srcx)
		ymax, ymin = max(srcy), min(srcy)
		zmax, zmin = max(srcz), min(srcz)
		srcw = xmax - xmin
		srch = ymax - ymin

		#
		toremove = []
		sections, intersections = [], []
		biases = []
		jj = 1

		segments = list(newaxis.segments)
		segments.append(segments[-1])

		for i, seg in enumerate(segments):
			axp = newaxis[i].copy()
			segvector = seg.vector
			axp.z, segvector.z = 0, 0
			# поиск пересечений c линиями съемки
			isec = {"RIGHT": "", "LEFT": "", "ROOF": "", "BOTTOM": ""}
			for join in strings:
				for k, s in enumerate(strings[join].segments):
					sp = strings[join][k]
					divider = segvector*s.vector
					if divider == 0:
						continue
					t = (segvector*(axp.to_vector()-sp.to_vector())) / divider
					if t < -1e-6 or t > 1+1e-6:
						continue
					intersection = sp.to_vector() + t*s.vector
					isec[sides[join]] = intersection
					break
			if "" in isec.values():
				toremove.append(i)
				continue

			# добавляем в список точки пересечения
			for p in isec.values():
				intersections.append(p)

			# определение центра в плане
			centerh = 0.5*(isec["RIGHT"] + isec["LEFT"])
			# определение размером сечения
			r, l = isec["RIGHT"].copy(), isec["LEFT"].copy()
			r.z, l.z = 0, 0
			dstw = (r - l).norm
			dsth = isec["ROOF"].z - isec["BOTTOM"].z

			# 
			newaxis[i].z = isec["BOTTOM"].z

			biases.append([(r.to_vector() - axp.to_vector()).norm, (l.to_vector() - axp.to_vector()).norm, dsth])

			xfactor = dstw / srcw
			yfactor = dsth / srch
			# масштабирование сечения
			s = section.scale(xfactor, yfactor)

			# расчет геометрического центра сечения
			srcx, srcy, srcz = s.x, s.y, s.z
			xmax, xmin = max(srcx), min(srcx)
			ymax, ymin = max(srcy), min(srcy)
			zmax, zmin = max(srcz), min(srcz)
			section_center = Vector(0.5*(xmax+xmin), 0.5*(ymax + ymin), 0.5*(zmax+zmin))

			# задание ориентации сечения
			s = s.set_orientation(-segvector, center = section_center)
			# перенос в точку проекции (в плане)
			s = s + centerh - section_center
			
			zoffset = isec["BOTTOM"].z - min(s.z)
			for k, p in enumerate(s):
				s[k].z += zoffset

			# добавляем сечение к списку сечений
			xx, yy, zz = zip(*s.coords)
			n = len(xx)
			sections += list(zip(xx, yy, zz, [jj] * n))
			jj += 1
			# _section = list(zip(xx, yy, zz, [jj]*n))
			# if sections[0] != sections[-1]:
			# 	sections.append(sections[0])
			# sections += _section
			# jj += 1

		# удаление точек из осевой линии, для которых нет всех пересечений с исходными данным
		for p in reversed(toremove):
			newaxis.remove(p)

		# переопределяем линию, чтобы удалить близкие точки
		newaxis = LineString(newaxis.coords)
		axisdata = [[p.x, p.y, p.z, 1, *biases[i]] for i, p in enumerate(newaxis)]

		### =================================================================================== ###
		### ===================================== ВЫВОД ======================================= ###
		### =================================================================================== ###

		if not sections:		raise Exception("Сечения не были созданы")
		else:					MicromineFile.write(outSectionPath, sections, ["EAST", "NORTH", "RL", "JOIN"])

		if self.groupBoxOutWireframe.isChecked():
			if not os.path.exists(outTridbPath):
				try:					Tridb.create(outTridbPath);	print ("Создан"); return
				except Exception as e:	raise Exception("Невозможно создать файл %s. %s" % (outTridbPath, str(e)))
			# создание файла осевой линии
			progress.setText("Создание осевой линии")
			if not axisdata:			qtmsgbox.show_error(self, "Ошибка", "Файл осевой линии пустой")
			else:					MicromineFile.write(outAxisPath, axisdata, ["EAST", "NORTH", "RL", "JOIN", "RIGHT", "LEFT", "HEIGHT"])
			# создание каркаса
			progress.setText("Создание каркаса")
			err = self.exec_mmproc(create_solid, outAxisPath, outTridbPath, display_name, secpath)
			if err:
				raise Exception("Невозможно создать каркас выработки. %s" % err)			
			# удаление файла осевой линии
			if not self.groupBoxOutAxis.isChecked():
				try:			os.remove(outAxisPath)
				except:			pass		

		if self.groupBoxOutIntersection.isChecked():
			progress.setText("Создание точек пересечения")
			data = [p.coords for p in intersections]
			if not data:			qtmsgbox.show_error(self, "Ошибка", "Файл точек пересечений пустой")
			else:					MicromineFile.write(outIntersectionsPath, data, ["EAST", "NORTH", "RL"])

		# вывод проекций на осевую линию
		# data = [p.tolist() for p in projections]
		# if data:
		# 	MicromineFile.write(MMpy.Project.path() + "Проекции.DAT", data, ["EAST", "NORTH", "RL"])

		if self.is_cancelled:
			return

		### =================================================================================== ###
		### ================================= ЗАГРУЗКА В ВИЗЕКС =============================== ###
		### =================================================================================== ###

		if self.checkBoxOutputAutoload.isChecked():
			progress.setText("Загрузка в Визекс")
			# загрузка сечений
			err = self.exec_mmproc(load_sections, outSectionPath, "%s_Сечения" % display_name)
			if err:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно загрузить файл сечений. %s" % err)
			# загрузка осевой линии
			if self.groupBoxOutAxis.isChecked():
				err = self.exec_mmproc(load_axis, outAxisPath, os.path.basename(outAxisPath)[len(get_temporary_prefix()):])
				if err:
					qtmsgbox.show_error(self, "Ошибка", "Невозможно загрузить файл осевой линии. %s" % err)
			# загрузка пересечений
			if self.groupBoxOutIntersection.isChecked():
				err = self.exec_mmproc(load_intersections, outIntersectionsPath, os.path.basename(outIntersectionsPath)[len(get_temporary_prefix()):])
				if err:
					qtmsgbox.show_error(self, "Ошибка", "Невозможно загрузить файл точек пересечений. %s" % err)
			# загрузка каркаса выработки
			if self.groupBoxOutWireframe.isChecked():
				err = self.exec_mmproc(load_solid, outTridbPath, display_name, display_name)
				if err:
					qtmsgbox.show_error(self, "Ошибка", "Невозможно загрузить каркас выработки. %s" % err)

		qtmsgbox.show_information(self, "Выполнено", "Выработка '%s' успешно построена" % display_name)
