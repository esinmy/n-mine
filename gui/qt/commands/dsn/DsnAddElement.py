try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os
from datetime import datetime

from mm.mmutils import Tridb
from mm.formsets import copy_wireframe

from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.osutil import get_current_time

#from gui.qt.commands.dsn.DsnDefineModel import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.widgets.DirectoryView import DirectoryView
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class DsnAddElement(DialogWindow):
	def __init__(self, *args, **kw):
		super(DsnAddElement, self).__init__(*args, **kw)

		self.setWindowTitle("Копировать проектный каркас выработки на сервер")
		
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.inputLayout = QtGui.QFormLayout()

		self.labelTriType = QtGui.QLabel(self.groupBoxInput)
		self.labelTriType.setText("Тип")
		self.labelTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelTriType.setMinimumWidth(80)
		self.lineEditTriType = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditTriType.setMinimumWidth(200)		

		self.labelTriName = QtGui.QLabel(self.groupBoxInput)
		self.labelTriName.setText("Имя")
		self.labelTriName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelTriName.setMinimumWidth(80)
		self.lineEditTriName = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditTriName.setMinimumWidth(200)


		self.groupBoxAttributes = QtGui.QGroupBox(self)
		self.groupBoxAttributes.setTitle("Атрибуты")
		self.layoutAttributes = QtGui.QGridLayout()

		self.labelLayer = QtGui.QLabel(self.groupBoxAttributes)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLayer.setMinimumWidth(80)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxLayer.setMinimumWidth(150)

		self.labelMineSite = QtGui.QLabel(self.groupBoxAttributes)
		self.labelMineSite.setText("Участок")
		self.labelMineSite.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelMineSite.setMinimumWidth(80)
		self.comboBoxMineSite = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxMineSite.setMinimumWidth(150)

		self.labelLocProject = QtGui.QLabel(self.groupBoxAttributes)
		self.labelLocProject.setText("Проект")
		self.labelLocProject.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLocProject.setMinimumWidth(80)
		self.comboBoxLocalProject = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxLocalProject.setMinimumWidth(150)

		self.labelWorkTypeDescription = QtGui.QLabel(self.groupBoxAttributes)
		self.labelWorkTypeDescription.setText("Тип работ")
		self.labelWorkTypeDescription.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelWorkTypeDescription.setMinimumWidth(70)
		self.comboBoxWorkTypeDescription = QtGui.QComboBox(self.groupBoxAttributes)
		self.comboBoxWorkTypeDescription.setMinimumWidth(150)

		self.labelDate = QtGui.QLabel(self.groupBoxAttributes)
		self.labelDate.setText("Дата")
		self.labelDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDate.setMinimumWidth(70)
		self.dateEditDate = QtGui.QDateEdit(self.groupBoxAttributes)
		self.dateEditDate.setMinimumWidth(180)
		self.dateEditDate.setMaximumWidth(180)
		self.dateEditDate.setCalendarPopup(True)
		self.dateEditDate.setCurrentSection(QtGui.QDateTimeEdit.MonthSection)
		currentTime = get_current_time()
		self.dateEditDate.setDate(QtCore.QDate(currentTime.year, currentTime.month, currentTime.day))
		self.dateEditDate.setDisplayFormat("yyyy-MM-dd")

		self.labelStage = QtGui.QLabel(self.groupBoxAttributes)
		self.labelStage.setText("Стадия")
		self.labelStage.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelStage.setMinimumWidth(70)
		self.spinBoxStage = QtGui.QSpinBox(self.groupBoxAttributes)
		self.spinBoxStage.setButtonSymbols(QtGui.QSpinBox.NoButtons)
		self.spinBoxStage.setValue(1)
		self.spinBoxStage.setMinimum(1)
		self.spinBoxStage.setMinimumWidth(150)

		self.labelSection = QtGui.QLabel(self.groupBoxAttributes)
		self.labelSection.setText("Сечение")
		self.labelSection.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSection.setMinimumWidth(70)
		self.lineEditSection = QtGui.QLineEdit(self.groupBoxAttributes)
		self.lineEditSection.setMinimumWidth(180)
		self.lineEditSection.setMinimumWidth(180)


		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.outputLayout = QtGui.QFormLayout()
######## start edit

		self.directoryView = DirectoryView(self.groupBoxOutput)
		sizePolicy = self.directoryView.sizePolicy()
		sizePolicy.setVerticalStretch(1)
		self.directoryView.setSizePolicy(sizePolicy)
		for ittype in ITEM_TYPE_LIST:
			if not ittype in [ITEM_DIR, ITEM_TRIDB]:
				self.directoryView.hideItemType(ittype)
			else:
				self.directoryView.showItemType(ittype)

		self.labelOutTriType = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTriType.setText("Выработка")
		self.labelOutTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriType.setMinimumWidth(80)
######## end edit

		self.labelOutTriName = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTriName.setText("Имя")
		self.labelOutTriName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriName.setMinimumWidth(80)
		self.lineEditOutTriName = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutTriName.setMinimumWidth(200)
	def __createBindings(self):
		self.comboBoxLayer.currentIndexChanged.connect(self.layerChanged)
		self.comboBoxMineSite.currentIndexChanged.connect(self.mineSiteChanged)
		self.comboBoxLocalProject.currentIndexChanged.connect(self.localProjectChanged)

		self.lineEditTriType.mouseDoubleClickEvent = lambda e: self.select_tridb(e, self.lineEditTriType)
		self.lineEditTriName.mouseDoubleClickEvent = lambda e: self.select_wireframe(e, self.lineEditTriType, self.lineEditTriName)
		self.lineEditOutTriName.mouseDoubleClickEvent = lambda e: self.select_wireframe(e, self.directoryView,
																						self.lineEditOutTriName)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
######## start edit
		selmodel = self.directoryView.selectionModel()
		selmodel.selectionChanged.connect(self.treeAction)

	def treeAction(self, selected, deselected):

		if not self.directoryView.selectedItems():
			pass
		else:
			for index in selected.indexes():
				self.mine__item = self.directoryView.itemFromIndex(index)
				self.mine__Name = self.mine__item.text(0)

		self.comboBoxWorkTypeDescription.clear()
		layerName = self.comboBoxLayer.currentText()
		localProjectName = self.comboBoxLocalProject.currentText()

		category = self.getCategoryCode(self.mine__Name, localProjectName, layerName)
		if not category:
			return

		codeList = []
		for item in self.main_app.settings.getValue(ID_SVYCAT):
			if item[0][1] == category:
				codeList.extend([row[1] for row in item[1]])
		codeList = [""] + list(sorted(set(codeList)))
		self.comboBoxWorkTypeDescription.addItems(codeList)



	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelTriType)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditTriType)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelTriName)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditTriName)

		self.layoutControls.addWidget(self.groupBoxAttributes)
		self.groupBoxAttributes.setLayout(self.layoutAttributes)
		self.layoutAttributes.addWidget(self.labelLayer, 0, 0)
		self.layoutAttributes.addWidget(self.comboBoxLayer, 0, 1)
		self.layoutAttributes.addWidget(self.labelMineSite, 1, 0)
		self.layoutAttributes.addWidget(self.comboBoxMineSite, 1, 1)
		self.layoutAttributes.addWidget(self.labelLocProject, 2, 0)
		self.layoutAttributes.addWidget(self.comboBoxLocalProject, 2, 1)


		self.layoutAttributes.addWidget(self.labelWorkTypeDescription, 0, 2)
		self.layoutAttributes.addWidget(self.comboBoxWorkTypeDescription, 0, 3)
		self.layoutAttributes.addWidget(self.labelDate, 1, 2)
		self.layoutAttributes.addWidget(self.dateEditDate, 1, 3)
		self.layoutAttributes.addWidget(self.labelStage, 2, 2)
		self.layoutAttributes.addWidget(self.spinBoxStage, 2, 3)
		self.layoutAttributes.addWidget(self.labelSection, 3, 2)
		self.layoutAttributes.addWidget(self.lineEditSection, 3, 3)

		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.outputLayout)
#### start edit
		self.outputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTriType)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.directoryView)
#### end edit
		self.outputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelOutTriName)
		self.outputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditOutTriName)

	# запросы
	def getCategoryCode(self, mineName, localProjectName, layerName):
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		try:						localProjectId = self.main_app.srvcnxn.getLocalProjectId(localProjectName, layerName = layerName, locationName = locationName)
		except Exception as e:		return
		#
		if localProjectId is None:
			return
		#
		cmd = """
			SELECT V.WORK_TYPE
			FROM [{0}].[dbo].[VYRAB_P] AS V
			INNER JOIN [{0}].[dbo].[LOCAL_PROJECT_MINES] AS P_M
			ON P_M.MINE_ID = V.VyrId
			WHERE V.Vyr = ? AND P_M.LOC_PROJECT_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (mineName, localProjectId)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		categories = [row[0] for row in data]
		if categories:
			return categories[0]
	def getWorkTypeCode(self, categoryCode, workTypeDescription):
		for item in self.main_app.settings.getValue(ID_SVYCAT):
			codeDescritptions = list(zip(*item[1]))[1]
			if item[0][1] == categoryCode and workTypeDescription in codeDescritptions:
				return item[1][codeDescritptions.index(workTypeDescription)][0]
	def getMineNames(self, localProjectName, layerName):
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		try:					localProjectId = self.main_app.srvcnxn.getLocalProjectId(localProjectName, layerName = layerName, locationName = locationName)
		except Exception as e:	return
		#
		if localProjectId is None:
			return
		#
		cmd = """
			SELECT V.Vyr
			FROM [{0}].[dbo].[VYRAB_P] AS V
			INNER JOIN [{0}].[dbo].[LOCAL_PROJECT_MINES] AS P_M
			ON P_M.MINE_ID = V.VyrId
			WHERE P_M.LOC_PROJECT_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (localProjectId)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		return [row[0] for row in data]
	def getLayers(self):
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT LAYER_DESCRIPTION
			FROM [{0}].[dbo].[LAYER]
			WHERE LOCATION = ?
			ORDER BY LAYER_DESCRIPTION
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		return [row[0] for row in data]
	def getProjectNames(self, layerName):
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		try:					layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		except Exception as e:	return
		#
		if layerId is None:
			return
		#
		cmd = """
			SELECT LOC_PROJECT_NAME
			FROM [{0}].[dbo].[LOCAL_PROJECT]
			WHERE LAYER_ID = ?
			ORDER BY LOC_PROJECT_NAME
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (layerId)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		return [row[0] for row in data]

	# построение выпадающих списков
	def buildCombos(self):
		self.buildLayerList()
	def buildLayerList(self):
		self.comboBoxLayer.clear()
		layers = [""] + self.getLayers()
		self.comboBoxLayer.addItems(layers)
	def buildLocalProjectList(self):
		self.comboBoxLocalProject.clear()

		layerName = self.comboBoxLayer.currentText()
		mineSiteName = self.comboBoxMineSite.currentText()

		if not layerName or not mineSiteName:
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		dsndir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerdir = dsndir + "\\" + layerName
		mineSiteDir = layerdir + "\\" + mineSiteName
		prefix = self.main_app.settings.getValue(ID_DSN_UPREFIX)

		locProjectNames = [""] + list(filter(lambda x: x.lower() in map(lambda f: f.lower(), os.listdir(mineSiteDir)), self.getProjectNames(layerName)))
		self.comboBoxLocalProject.addItems(locProjectNames)
	def buildMineSiteList(self):
		self.comboBoxMineSite.clear()
		layerName = self.comboBoxLayer.currentText()
		if not layerName:
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		dsndir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerdir = dsndir + "\\" + layerName
		mineSiteNames = [""] + list(filter(lambda x: os.path.isdir(layerdir + "\\" + x), os.listdir(layerdir)))
		self.comboBoxMineSite.addItems(mineSiteNames)
	# события
	def layerChanged(self):
		self.buildMineSiteList()
	def mineSiteChanged(self):
		self.buildLocalProjectList()

######## start edit
	def localProjectChanged(self):
		 # self.buildMineNameList()
		self.buildOutTypeList()
	#	self.buildCodeList()
	# def mineNameChanged(self):
	# 	self.buildCodeList()


	def buildOutTypeList(self):
		self.directoryView.setFolderPath(None)

		layerName = self.comboBoxLayer.currentText()
		minesiteName = self.comboBoxMineSite.currentText()
		localprojectName = self.comboBoxLocalProject.currentText()

		if not all([layerName, minesiteName, localprojectName]):
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svyDir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerDir = svyDir + "\\" + layerName
		minesiteDir = layerDir + "\\" + minesiteName
		tridir = minesiteDir + "\\" + localprojectName + '\\' +\
				 self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1]

		self.directoryView.setFolderPath(tridir)

####### end edit
	# диалоги
	def select_tridb(self, e, widget):
		filetypes = "КАРКАСЫ (*.TRIDB)"
		try:		initialdir = MMpy.Project.path()
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		# ничего не выбрано
		if not path:
			return

		widget.setText(path)


	def select_wireframe(self, e, triwidget, widget):
		if triwidget == self.lineEditTriType:
			tripath = self.lineEditTriType.text()
		if triwidget == self.directoryView:
			selectedItems = self.directoryView.selectedItems()
			if not selectedItems or not selectedItems[0].isTridb():
				qtmsgbox.show_error(self, "Ошибка", "Выберите выработку")
				return
			tripath = selectedItems[0].filePath()
		try:
			tridb = Tridb(tripath)
			wireframe_names = tridb.wireframe_names
			tridb.close()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список каркасов. %s" % str(e))
			return

		dialog = SelectFromListDialog(self)
		dialog.setValues(wireframe_names)
		dialog.setWindowTitle("Выберите каркас")
		dialog.labelTitle.setText("Выберите каркас")
		dialog.exec()

		result = dialog.getValues()
		if result:
			widget.setText(result[0])
			self.lineEditOutTriName.setText(result[0])

	# проверка
	def prepare(self):
		if not super(DsnAddElement, self).prepare():
			return
		
		inputTriPath = self.lineEditTriType.text()
		inputTriName = self.lineEditTriName.text()

		layerName = self.comboBoxLayer.currentText()
		mineSiteName = self.comboBoxMineSite.currentText()
		localProjectName = self.comboBoxLocalProject.currentText()
		mineName = self.mine__Name
		workTypeDescription = self.comboBoxWorkTypeDescription.currentText()
		stage = self.spinBoxStage.text()

		outname = self.lineEditOutTriName.text()

		labels = [self.labelTriType, self.labelTriName, self.labelLayer, self.labelMineSite, self.labelLocProject,
				  self.labelWorkTypeDescription, self.labelStage, self.labelOutTriName]
		values = [inputTriPath, inputTriName, layerName, mineSiteName, localProjectName, workTypeDescription, stage, mineName, outname]
		for label, value in zip(labels, values):
			if not value:
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно скопировать каркас на сервер. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(DsnAddElement, self).run():
			return

		srcpath = self.lineEditTriType.text()
		srcname = self.lineEditTriName.text()

		layerName = self.comboBoxLayer.currentText()
		mineSiteName = self.comboBoxMineSite.currentText()
		localProjectName = self.comboBoxLocalProject.currentText()
		# mineName = self.comboBoxMineName.currentText()
		mineName = self.mine__Name

		categoryCode = self.getCategoryCode(mineName, localProjectName, layerName)
		workTypeDescription = self.comboBoxWorkTypeDescription.currentText()
		code = self.getWorkTypeCode(categoryCode, workTypeDescription)

		date = datetime.strptime(self.dateEditDate.text(), "%Y-%m-%d")
		stage = self.spinBoxStage.text()
		section = self.lineEditSection.text()

		dstname = self.lineEditOutTriName.text().rstrip()
		dstpath = self.directoryView.selectedItems()[0].filePath()

		#
		if not os.path.exists(dstpath):
			if not qtmsgbox.show_question(self, "Внимание", "Файл выработки '%s' не существует. Создать?" % mineName):
				return
			else:
				err = self.exec_proc(Tridb.create, dstpath, DSN_USER_ATTRIBUTES)
				if err:
					raise Exception("Невозможно создать файл '%s'. %s" % (dstpath, err))

		# копирование каркаса
		dsttridb = Tridb(dstpath)
		if dstname in dsttridb.wireframe_names:
			if not qtmsgbox.show_question(self, "Внимание", "Каркас '%s' уже существует. Заменить?" % dstname):
				return

		if not all(map(lambda x: x in dsttridb.user_attributes, DSN_USER_ATTRIBUTES)):
			dsttridb.close()
			raise ValueError("В каркасе отсутствует один или несколько обязательных пользовательских атрибутов")
		dsttridb.close()

		# копирование каркаса
		err = self.exec_proc(copy_wireframe, srcpath, srcname, dstpath, dstname, False)
		if err:
			raise Exception("Невозможно скопировать каркас. %s" % err)


		# задание атрибутов
		dsttridb = Tridb(dstpath)
		dstwf = dsttridb[dstname]
		dstwf.set_default_attribute(DEFAULT_ATTR_TITLE, mineName)
		dstwf.set_default_attribute(DEFAULT_ATTR_CODE, code)
		dstwf.set_user_attribute(DSN_ATTR_CATEGORY, categoryCode)
		dstwf.set_user_attribute(DSN_ATTR_LAYER, layerName)
		dstwf.set_user_attribute(DSN_ATTR_PROJECT, localProjectName)
		dstwf.set_user_attribute(DSN_ATTR_DATE, date.strftime("%Y-%m-%d"))
		dstwf.set_user_attribute(DSN_ATTR_STAGE, stage)
		dstwf.set_user_attribute(DSN_ATTR_SECTION, section)

		dsttridb.commit()
		dsttridb.close()

		qtmsgbox.show_information(self, "Выполнено", "Каркас успешно скопирован на сервер")
		
