from PyQt4 import QtGui, QtCore
import os
from datetime import datetime

from utils.validation import is_blank
from utils.constants import *
from utils.osutil import get_current_time
from utils.qtutil import iterWidgetChildren, getWidgetValue

import gui.qt.dialogs.messagebox as qtmsgbox

from gui.qt.dialogs.messagebox import *
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.DirectoryView import DirectoryView
from gui.qt.widgets.TableDataView import CustomDataModel
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DsnCreateUnit(DialogWindow):
	def __init__(self, *args, **kw):
		super(DsnCreateUnit, self).__init__(*args, **kw)

		self.setWindowTitle("Создать юнит")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		if self.main_app.srvcnxn is not None:
			self.buildCombos()

	def __createVariables(self):
		self._layers = {"": ""}
		self._minesites_ids = []
		self._geounits_ids = []
		self._units = []
		self._loc_work_type_ids = []
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.inputLayout = QtGui.QFormLayout()

		self.labelLayer = QtGui.QLabel(self.groupBoxInput)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setMinimumWidth(180)
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxLayer.setMinimumWidth(200)

		self.labelGeoUnit = QtGui.QLabel(self.groupBoxInput)
		self.labelGeoUnit.setMinimumWidth(180)
		self.labelGeoUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelGeoUnit.setText(self.main_app.settings.getValue(ID_GEO_UDSPNAME))
		self.lineEditGeoUnit = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditGeoUnit.setMinimumWidth(200)
		self.lineEditGeoUnit.setReadOnly(True)

		self.labelTechProject = QtGui.QLabel(self.groupBoxInput)
		self.labelTechProject.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelTechProject.setMinimumWidth(180)
		self.labelTechProject.setText("Технический проект")
		self.comboBoxTechProject = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxTechProject.setMinimumWidth(200)

		self.labelMinesite = QtGui.QLabel(self.groupBoxInput)
		self.labelMinesite.setMinimumWidth(180)
		self.labelMinesite.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelMinesite.setText("Участок")
		self.lineEditMinesite = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditMinesite.setMinimumWidth(200)
		self.lineEditMinesite.setReadOnly(True)

		self.labelAuthor = QtGui.QLabel(self.groupBoxInput)
		self.labelAuthor.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelAuthor.setMinimumWidth(180)
		self.labelAuthor.setText("Автор")
		self.comboBoxAuthor = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxAuthor.setMinimumWidth(200)

		self.labelDate = QtGui.QLabel(self.groupBoxInput)
		self.labelDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDate.setMinimumWidth(180)
		self.labelDate.setText("Дата")
		self.dateEditProjectDate = QtGui.QDateEdit(self.groupBoxInput)
		self.dateEditProjectDate.setCalendarPopup(True)
		self.dateEditProjectDate.calendarWidget().setFirstDayOfWeek(QtCore.Qt.Monday)
		self.dateEditProjectDate.setDisplayFormat("yyyy-MM-dd")
		currentTime = get_current_time()
		self.dateEditProjectDate.setDate(QtCore.QDate(currentTime.year, currentTime.month, currentTime.day))

		self.labelLocProject = QtGui.QLabel(self.groupBoxInput)
		self.labelLocProject.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLocProject.setMinimumWidth(180)
		self.labelLocProject.setText("Локальный проект")
		self.lineEditLocProject = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditLocProject.setMinimumWidth(200)

		self.label_loc_work_type = QtGui.QLabel(self.groupBoxInput)
		self.label_loc_work_type.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.label_loc_work_type.setMinimumWidth(180)
		self.label_loc_work_type.setText("Вид работ")
		self.loc_work_type_box = QtGui.QLineEdit(self.groupBoxInput)
		self.loc_work_type_box.setReadOnly(True)

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.outputLayout = QtGui.QFormLayout()

		self.labelOutTriType = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutTriType.setText("Выбор папки \n (Esc - снять выделение)")
		self.labelOutTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriType.setMinimumWidth(180)
		self.labelOutTriType.setMaximumWidth(180)

		self.directoryView = DirectoryView(self.groupBoxOutput)
		sizePolicy = self.directoryView.sizePolicy()
		sizePolicy.setVerticalStretch(1)
		self.directoryView.setSizePolicy(sizePolicy)
		for ittype in ITEM_TYPE_LIST:
			if not ittype in [ITEM_DIR, ITEM_TRIDB]:
				self.directoryView.hideItemType(ittype)
			else:
				self.directoryView.showItemType(ittype)

		self.labelDirChoosen = QtGui.QLabel(self.groupBoxOutput)
		self.labelDirChoosen.setText("Выбранная папка:")
		self.labelDirChoosen.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTop)
		self.labelDirChoosen.setMinimumWidth(180)

		self.DirChoosen = QtGui.QLabel(self.groupBoxOutput)
		self.DirChoosen.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
		self.DirChoosen.setMinimumWidth(300)
		self.DirChoosen.setText("не выбрана")


	def __createBindings(self):
		self.comboBoxLayer.currentIndexChanged.connect(self.layerChanged)
		self.lineEditGeoUnit.mouseDoubleClickEvent = lambda e: self.selectGeoUnit()
		self.lineEditMinesite.mouseDoubleClickEvent = lambda e: self.selectMinesite()
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
		self.loc_work_type_box.mouseDoubleClickEvent = lambda e: self.select_work_type()
		selmodel = self.directoryView.selectionModel()
		selmodel.selectionChanged.connect(self.treeAction)


	def treeAction(self, selected, deselected):
		layerName = self.comboBoxLayer.currentText()
		minesiteName = self.lineEditMinesite.text()
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		dsnDir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerDir = dsnDir + "\\" + layerName
		minesiteDir = layerDir + "\\" + minesiteName
		shown_dir = "...\\" + layerName + "\\" + minesiteName
		shown_dir_multi_minesite = "...\\" + layerName

		if ',' not in minesiteName:
			if not self.directoryView.selectedItems():
				self.DirChoosen.setText(shown_dir)
			else:
				temp = self.directoryView.selectedItems()[0].filePath()
				param1, param2 = temp.split(minesiteName, 1)
				self.DirChoosen.setText(shown_dir + param2)
		else:
			if not self.directoryView.selectedItems():
				self.DirChoosen.setText(shown_dir_multi_minesite)
			else:
				temp = self.directoryView.selectedItems()[0].filePath()
				param1, param2 = temp.split(layerName, 1)
				self.DirChoosen.setText(shown_dir_multi_minesite + param2)

	def select_work_type(self):
		cmd = "SELECT WorkID, WorkName FROM [{0}].[dbo].[WORKS]".format(self.main_app.srvcnxn.dbsvy)
		data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		if not data:
			return
		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите вид работ")
		model = CustomDataModel(dialog.view, data, ["ID", "ВИД РАБОТ"])
		dialog.view.setSelectionMode(QtGui.QTableView.ExtendedSelection)
		dialog.view.setModel(model)
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.view.hideColumn(0)
		dialog.resize(400, 480)
		dialog.exec()

		result = dialog.getResult()
		if result:
			self._loc_work_type_ids = [row[0] for row in result]
			self.loc_work_type_box.setText(", ".join([row[1] for row in result]))
			self.loc_work_type_box.setCursorPosition(0)

	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelGeoUnit)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditGeoUnit)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelTechProject)
		self.inputLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.comboBoxTechProject)

		self.inputLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelMinesite)
		self.inputLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditMinesite)
		self.inputLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.labelAuthor)
		self.inputLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.comboBoxAuthor)
		self.inputLayout.setWidget(5, QtGui.QFormLayout.LabelRole, self.labelDate)
		self.inputLayout.setWidget(5, QtGui.QFormLayout.FieldRole, self.dateEditProjectDate)
		self.inputLayout.setWidget(7, QtGui.QFormLayout.LabelRole, self.labelLocProject)
		self.inputLayout.setWidget(7, QtGui.QFormLayout.FieldRole, self.lineEditLocProject)
		self.inputLayout.setWidget(6, QtGui.QFormLayout.LabelRole, self.label_loc_work_type)
		self.inputLayout.setWidget(6, QtGui.QFormLayout.FieldRole, self.loc_work_type_box)

		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.outputLayout)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTriType)
		self.outputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.directoryView)
		self.outputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelDirChoosen)
		self.outputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.DirChoosen)


	def buildCombos(self):
		self.buildLayerList()
		self.buildTechProjectList()
		self.buildAuthorList()
		# self.build_loc_work_type()

	def buildLayerList(self):
		# считываем залежи из БД
		self.comboBoxLayer.clear()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		self._layers = {}
		#
		cmd = """
			SELECT LAYER_ID, LAYER_DESCRIPTION 
			FROM [{0}].[dbo].[LAYER]
			WHERE LOCATION = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not data:
			return
		for layerId, layerName in data:
			self._layers[layerName] = layerId
		self.comboBoxLayer.addItems(sorted(list(self._layers.keys())))
	def buildTechProjectList(self):
		self.comboBoxTechProject.clear()
		cmd = "SELECT ProjectNum FROM [{0}].[dbo].[Projects]".format(self.main_app.srvcnxn.dbsvy)
		data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		if not data:
			return
		techProjects = [""] + [row[0] for row in data]
		self.comboBoxTechProject.addItems(techProjects)
	def buildAuthorList(self):
		self.comboBoxAuthor.clear()
		cmd = "SELECT Surname + ' ' + Firstname + ' ' + Suvmiddle FROM [{0}].[dbo].[SURVEY_FIO]".format(self.main_app.srvcnxn.dbsvy)
		data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		authors = [""] + [row[0] for row in data]
		if not authors:
			return
		self.comboBoxAuthor.addItems(authors)
	#
	def layerChanged(self, indexs):
		self.lineEditGeoUnit.setText("")
		self.buildOutTypeList()

	def selectGeoUnit(self):
		layerName = self.comboBoxLayer.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		geoUnitName = self.main_app.settings.getValue(ID_GEO_UDSPNAME)
		if not layerName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите залежь")
			return

		#
		cmd = """
			SELECT PANEL_ID, PANEL_NAME FROM [{0}].[dbo].[PANEL]
			WHERE LAYER_ID = (
				SELECT LAYER_ID FROM [{0}].[dbo].[LAYER]
				WHERE LAYER_DESCRIPTION = ? AND LOCATION = ?
			) ORDER BY PANEL_NAME""".format(self.main_app.srvcnxn.dbsvy)
		params = (layerName, locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not data:
			return

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите " + geoUnitName.lower())
		model = CustomDataModel(dialog.view, data, ["ID", geoUnitName])
		dialog.view.setModel(model)
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.view.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		dialog.view.hideColumn(0)
		dialog.resize(400, 480)
		dialog.exec()

		result = dialog.getResult()
		if result:
			self._geounits_ids = [row[0] for row in result]
			self.lineEditGeoUnit.setText(", ".join([row[1] for row in result]))

	def selectMinesite(self):
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT D.DepartId, D.DepartName, P.PitName
			FROM [{0}].[dbo].[DEPARTS] D JOIN [{0}].[dbo].[PITS] P ON D.DepartPitId = P.PitId
			WHERE P.LOCATION = ?
			ORDER BY DepartName
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not data:
			return

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите участок")
		model = CustomDataModel(dialog.view, data, ["ID", "УЧАСТОК", "ШАХТА"])
		dialog.view.setModel(model, combobox_filter=[2])
		# dialog.view.horizontalHeader().set_comboboxfilter_editable(True)
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.view.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		dialog.view.hideColumn(0)
		dialog.resize(400, 480)
		dialog.exec()

		result = dialog.getResult()
		if result:
			self._minesites_ids = [row[0] for row in result]
			self.lineEditMinesite.setText(", ".join([row[1] for row in result]))
		self.buildOutTypeList()


	def buildOutTypeList(self):
		self.directoryView.setFolderPath(None)

		layerName = self.comboBoxLayer.currentText()
		minesiteName = self.lineEditMinesite.text()

		if not all([layerName, minesiteName]):
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		dsnDir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerDir = dsnDir + "\\" + layerName
		minesiteDir = layerDir + "\\" + minesiteName
		shown_dir = "...\\" + layerName + "\\" + minesiteName
		shown_layerDir = "...\\" + layerName


		dirWf = tuple(map(lambda x: x[1], self.main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]
		if ',' in minesiteName:
			self.DirChoosen.setText("{}".format(shown_layerDir))
			self.directoryView.setFolderPath(layerDir)
			for t in self.directoryView.iterChildren():
				if t.__class__.__name__ != 'TreeModelDirItem':
					t.setHidden(True)
				if t.name() == dirWf:
					t.parent().setDisabled(True)
				if t.name() == DIR_ARCHIVE:
					t.setDisabled(True)
			return

		self.directoryView.setFolderPath(minesiteDir)
		self.DirChoosen.setText(shown_dir)

		for t in self.directoryView.iterChildren():
			if t.__class__.__name__ != 'TreeModelDirItem':
				t.setDisabled(True)
			if t.name() == dirWf:
				t.parent().setDisabled(True)
			if t.name() == DIR_ARCHIVE:
				t.setDisabled(True)


	def createFolders(self):
		networkpath = self.main_app.settings.getValue(ID_NETPATH)
		dsndir = self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerName = self.comboBoxLayer.currentText()
		minesiteNames = self.lineEditMinesite.text().split(", ")
		locProjectName = self.lineEditLocProject.text()
		selectedItems = self.directoryView.selectedItems()
		if not self.directoryView.selectedItems():
			# создание залежи, если отсутствует
			currentFolder = os.path.join(networkpath, dsndir, layerName)
			if not os.path.exists(currentFolder):
				os.mkdir(currentFolder)

			minesite = minesiteNames[0]
			if len(minesiteNames) > 1:
				minesiteFolder = currentFolder
			else:
				minesiteFolder = currentFolder + "\\" + minesite
			if not os.path.exists(minesiteFolder):
				try:
					os.mkdir(minesiteFolder)
				except Exception as e:
					qtmsgbox.show_error(self, "Ошибка", "Невозможно создать директорию '%s'. %s" % (minesiteFolder, str(e)))
					return

			# создание директории проекта, если не существует
			projectFolder = minesiteFolder + "\\" + locProjectName
			if not self.create_loc_project_folder(projectFolder):
				return

		else:
			projectFolder = self.directoryView.selectedItems()[0].filePath() + "\\" + locProjectName
			if not self.create_loc_project_folder(projectFolder):
				return
		return True


	def create_loc_project_folder(self, projectFolder):
		if os.path.exists(projectFolder):
			qtmsgbox.show_error(self, "Ошибка", "Локальный проект уже существует")
			return
		try:
			os.mkdir(projectFolder)
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно создать директорию '%s'. %s" % (projectFolder, str(e)))
			return

		# создание вложенных директорий
		for folderPurpose, folderName in self.main_app.settings.getValue(ID_DSN_USUBDIRS):
			try:
				os.mkdir(projectFolder + "\\" + folderName)
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно создать директорию '%s'. %s" % (
				projectFolder + "\\" + folderName, str(e)))
		return True


	def prepare(self):

		if not super(DsnCreateUnit, self).prepare():
			return

		layerName = self.comboBoxLayer.currentText()
		geoUnitNames = self.lineEditGeoUnit.text()
		techProjectName = self.comboBoxTechProject.currentText()
		minesiteNames = self.lineEditMinesite.text()
		author = self.comboBoxAuthor.currentText()
		projectDate = self.dateEditProjectDate.text()
		locProjectName = self.lineEditLocProject.text()
		loc_work_type = self.loc_work_type_box.text()
		
		#
		labels = (self.labelLayer, self.labelGeoUnit, self.labelTechProject, self.labelMinesite, self.labelAuthor, self.labelDate, self.labelLocProject, self.label_loc_work_type)
		values = (layerName, geoUnitNames, techProjectName, minesiteNames, author, projectDate, locProjectName, loc_work_type)
		for label, value in zip(labels, values):
			if is_blank(value):
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Не удалось создать проектный юнит. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(DsnCreateUnit, self).run():
			return

		layerName = self.comboBoxLayer.currentText()
		geoUnitNames = self.lineEditGeoUnit.text().split(", ")
		techProjectName = self.comboBoxTechProject.currentText()
		minesiteNames = self.lineEditMinesite.text().split(", ")
		author = self.comboBoxAuthor.currentText()
		locProjectName = self.lineEditLocProject.text()
		projectDate = datetime.strptime(self.dateEditProjectDate.text(), "%Y-%m-%d")
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		loc_work_type_names = self.loc_work_type_box.text().split(", ")
		selectedItems = self.directoryView.selectedItems()

		#
		cmd = "SELECT ProjectId FROM [%s].[dbo].[Projects] WHERE ProjectNum = ?" % (self.main_app.srvcnxn.dbsvy)
		params = (techProjectName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not data:
			return
		techProjectId = data[0][0]

		cmd = """
			SELECT LAYER_ID FROM [{0}].[dbo].[LAYER]
			WHERE LAYER_DESCRIPTION = ? AND LOCATION = ?
		""".format(self.main_app.srvcnxn.dbsvy, locProjectName)
		params = (layerName, locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		layerId = data[0][0] if data else None
		if layerId is None:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно определить уникальный номер залежи")
			return

		#
		cmd = """
			SELECT LOC_PROJECT_ID FROM [{0}].[dbo].[LOCAL_PROJECT]
			WHERE LOC_PROJECT_NAME = ? AND LAYER_ID = ?
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locProjectName, layerId)

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		locProjectId = data[0][0] if data else None
		name, surname, fname = author.split()

		# если локальный проект не существует
		if not locProjectId:
			# запись в локального проекта в БД
			cmd = """
			INSERT INTO [{0}].[dbo].[LOCAL_PROJECT] (LOC_PROJECT_NAME, ProjectId, UNAME, LAYER_ID, DATE_PROJECTED)
			SELECT
				?
				, ?
				, (SELECT FIOId FROM [{0}].[dbo].[SURVEY_FIO] WHERE Surname = ? AND Firstname = ? AND Suvmiddle = ?)
				, ?
				, CONVERT(smalldatetime, ?, 121)
			""".format(self.main_app.srvcnxn.dbsvy)
			params = ( locProjectName, techProjectId, name, surname, fname, layerId, projectDate.strftime("%Y-%m-%d"))
			self.main_app.srvcnxn.exec_query(cmd, params)

			# извлекаем ID локального проекта
			cmd = """
				SELECT LOC_PROJECT_ID FROM [{0}].[dbo].[LOCAL_PROJECT]
				WHERE LOC_PROJECT_NAME = ? AND LAYER_ID = ?
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (locProjectName, layerId)

			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			locProjectId = data[0][0] if data else None
			if not data:
				return

			# запись связи локального проекта и вида работ
			cmd = "INSERT INTO [{0}].[dbo].[LOCAL_PROJECT_WORK_TYPES] (LOC_PROJECT_ID, WORK_TYPE_ID) SELECT ?, ?"
			for loc_work_type in self._loc_work_type_ids:
				params = (locProjectId, loc_work_type)
				self.main_app.srvcnxn.exec_query(cmd.format(self.main_app.srvcnxn.dbsvy), params)

			# запись связи локального проекта и участков
			cmd = "INSERT INTO [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] (LOC_PROJECT_ID, DepartId) SELECT ?, ?"
			for i, minesite_id in enumerate(self._minesites_ids):
				params = ( locProjectId, minesite_id)
				self.main_app.srvcnxn.exec_query(cmd.format(self.main_app.srvcnxn.dbsvy), params)

			# запись связи локального проекта и геологических юнитов
			cmd = """
				INSERT INTO [{0}].[dbo].[LOCAL_PROJECT_PANELS] (LOC_PROJECT_ID, PANEL_ID)
				SELECT ?, (SELECT  PANEL_ID FROM [{0}].[dbo].[PANEL] WHERE PANEL_NAME = ? and LAYER_ID = ?)
			""" 
			for geoUnit in geoUnitNames:
				params = ( locProjectId, geoUnit, layerId)
				self.main_app.srvcnxn.exec_query(cmd.format(self.main_app.srvcnxn.dbsvy), params)

			# сохраняем
			self.main_app.srvcnxn.commit()
		elif not qtmsgbox.show_question(self, "Внимание", "Локальный проект уже существует в базе данных. Создать директории?"):
			return

		# создание директорий
		if not self.createFolders():
			return

		self.buildOutTypeList()
		qtmsgbox.show_information(self, "Выполнено", "Локальный проект успешно создан")
