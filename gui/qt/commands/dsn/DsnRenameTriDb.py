from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.osutil import set_file_extension
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow

from gui.qt.FileMapper import FileMapper

class DsnRenameTriDb(DialogWindow):
	def __init__(self, parent, triDbDsnUnitItem, use_connection, *args, **kw):
		super(DsnRenameTriDb, self).__init__(parent, use_connection, *args, **kw)
		self._triDbDsnUnitItem = triDbDsnUnitItem
		self._use_connection = use_connection
		self.setWindowTitle("Переименовать выработку (TriDb)")



		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._newName = None
		
		self.localProjectId = None

	def __createWidgets(self):
		self.layoutParams = QtGui.QFormLayout()

		self.labelOldName = QtGui.QLabel(self)
		self.labelOldName.setText("Старое имя")
		self.labelOldName.setMinimumWidth(100)
		self.labelOldName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOldName = QtGui.QLineEdit(self)
		self.lineEditOldName.setMinimumWidth(200)
		self.lineEditOldName.setReadOnly(True)
		self.lineEditOldName.setText(self._triDbDsnUnitItem.name())
		self.labelNewName = QtGui.QLabel(self)
		self.labelNewName.setText("Новое имя")
		self.labelNewName.setMinimumWidth(100)
		self.labelNewName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditNewName = QtGui.QLineEdit(self)
		self.lineEditNewName.setMinimumWidth(200)

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOldName)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOldName)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelNewName)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditNewName)


	def check(self):
		if is_blank(self.lineEditNewName.text()):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelNewName.text())
			return

		oldpath = self._triDbDsnUnitItem.filePath()
		dirpath = os.path.dirname(oldpath)
		newpath = os.path.join(dirpath, self._newName)

		if os.path.exists(newpath):
			qtmsgbox.show_error(self, "Ошибка", "Выработка (TriDb) '%s' уже существует" % os.path.basename(newpath))
			return

		return True

	def prepare(self):
		super(DsnRenameTriDb, self).prepare()

		self._newName = self.lineEditNewName.text()

		if not self.check():
			return

		if self._use_connection and self.main_app.srvcnxn is None:
			self.main_app.connect_to_database()

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно переименовать выработку (TriDb). %s" % str(e))

		self.after_run()
		if self._is_success:
			self.close()


	
	def run(self):

		if self._use_connection and self.main_app.srvcnxn is None:
			self.main_app.connect_to_database()

		def restore_filenames():
			nonlocal old_path, new_path
			if not os.path.exists(old_path):
				try:		os.rename(new_path, old_path)
				except:		pass

		#
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		layerName = self._triDbDsnUnitItem.getLayerName()
		layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		self.localProjectId = self.main_app.srvcnxn.getLocalProjectId(self.__getLocalProjectName(), layerId)
		#
		old_path = self._triDbDsnUnitItem.filePath()
		#
		dirpath = os.path.dirname(old_path)
		new_path = os.path.join(dirpath, set_file_extension(self._newName , ITEM_TRIDB) )
		set_file_extension(self._newName , ITEM_TRIDB)

		if  self.__existTriDbLocProjInDb(self._newName):
			raise Exception("Невозможно изменить имя. Выработка  с именем '%s' уже есть в БД. " % (self._newName))

		# Проверяем есть ли запись в БД соответствующая данному каркасу, если нет, 
		# то проверяем есть ли записи не соответствующие ни одному файлу
		# Если есть то показываем диалог предлагающий провести сопоставление с переименованием файла
		# Если сопоставление не проведено, то удаляется только файл
		if not self.__existTriDbLocProjInDb(self._triDbDsnUnitItem.name()):

			#Определяем корневую папку каркасов
			split_self_path = dirpath.upper().split('\\')
			index_tridb_path = split_self_path.index("КАРКАСЫ")

			tridb_path = ""
			for i in split_self_path[:index_tridb_path]:
				tridb_path = os.path.join(tridb_path,i)
			tridb_path = os.path.join(tridb_path, 'КАРКАСЫ' ) 
			tridb_path = tridb_path.replace(":", ":\\")

			#

			_listDbTriDbNotMatchedFiles = self.__getDbTriDbNotMatchedFiles(tridb_path) 
				
			if _listDbTriDbNotMatchedFiles != None and len(_listDbTriDbNotMatchedFiles) > 0:
				
				_fileMapper = FileMapper(self)
				_fileMapper.setMessage("В БД не найдена соответствующая запись для выбранного файла. Необходимо сопоставить файлу каркаса запись в базе данных.")
				_fileMapper.setFiles([self._triDbDsnUnitItem.filePath()])
				_dbObjects	=[]
				for i in _listDbTriDbNotMatchedFiles:
					_dbObjectName = i.pop('vyr')
					_dbObjectInformation = '<br/>'.join([ e[0]+' : '+e[1] for e in list(i.items())])
					_dbObjects.append((_dbObjectName,_dbObjectInformation))

				_fileMapper.setDbObjects (_dbObjects) # arg = [('objectName','Information'),<...>]
				_fileMapper.exec_()
				_model = _fileMapper.getModel()	  # returns [{'path': value, 'file': value, 'object':value}, <...>]
				for i in _model:
					if len (i['object'].strip()) > 0:
						ext = os.path.splitext(i['file'])[1]
						newName = i['object']+ext
						newPath = os.path.join(dirpath,newName)
						try:
							os.rename(i['path'], newPath)
						except:
							raise Exception ('Не удалось переименовать файл при сопоставлении')

						self._triDbDsnUnitItem.setName(i['object'])
						self._triDbDsnUnitItem.setFilePath (newPath)
						old_path = self._triDbDsnUnitItem.filePath()

			 # Проверяем еще раз, сопоставили файл записи в БД или нет
			if not self.__existTriDbLocProjInDb(self._triDbDsnUnitItem.name()):
				qtmsgbox.show_information(self, "Внимание", "Выработка  отсутствует в БД. Будет переименован только файл.")
		
			
			


		#
		try:

			# Узнаём Id выработки
			cmd ="""
				select VyrId from [{0}].[dbo].[VYRAB_P]  as VR
				inner join [{0}].[dbo].[LOCAL_PROJECT_MINES]  as P_M
				on P_M.MINE_ID = VR.VyrId
				where VR.VYR = ?  
				and P_M.LOC_PROJECT_ID = ?
			""" 
			cmd = cmd.format(self.main_app.srvcnxn.dbsvy)
			params = ( self._triDbDsnUnitItem.name(), self.localProjectId)
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			triDbLocProjectId = data[0][0]

			cmd ="""
			update  [{0}].[dbo].[VYRAB_P]  
				set [Vyr] = ?
				where  [VyrId] = ?
			"""
			params = (self._newName , triDbLocProjectId)
			cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

			try:
				self.main_app.srvcnxn.exec_query(cmd, params)
			except Exception as e:
				raise Exception("Невозможно изменить имя выработки в БД. %s" % str(e))
			# переименовываем файлы
			try:
				os.rename(old_path, new_path)
			except Exception as e:
				raise Exception("Невозможно переименовать выработку  '%s'" % str(e))
			
			self.main_app.srvcnxn.commit()
			
		except Exception as e:
			# возвращаем имена, если требуется
			restore_filenames()
			raise Exception(str(e))

		# переименовываем в интерфейсе
		try:
			self._triDbDsnUnitItem.setName(self._newName)
			self._triDbDsnUnitItem.setFilePath (new_path)
		except Exception as e:
			raise Exception("Невозможно обновить дерево папок. Перезапустите скрипт. Ошибка: %s" % str(e))

	
		qtmsgbox.show_information(self, "Выполнено", "Выработка успешно переименована")

	def __existTriDbLocProjInDb(self, triDbName):
		if triDbName == None:
			return False

		cmd ="""
		select * from [{0}].[dbo].[VYRAB_P]  as VR
			inner join [{0}].[dbo].[LOCAL_PROJECT_MINES]  as P_M
			on P_M.MINE_ID = VR.VyrId
			where VR.VYR = ? 
		    and P_M.LOC_PROJECT_ID = ?
		"""
		params = (triDbName, self.localProjectId)
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			self.main_app.srvcnxn.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))
		if data:
			return True
		else:
			return False

	def __getDbTriDbNotMatchedFiles(self, dirpath):

		if dirpath == None:
			return False

		cmd ="""
		select V.VYR, V.USERNAME, V.UPDATEDATE
			 from [{0}].[dbo].[VYRAB_P] as V 
			 inner join  [{0}].[dbo].[LOCAL_PROJECT_MINES] as P_M
			 on P_M.MINE_ID = V.VyrId
			 where P_M.LOC_PROJECT_ID = ?
		"""
		params = (self.localProjectId)
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			_listVyrs = [{'vyr':row[0],
						 'project_name':self.__getLocalProjectName(),
						 'user_name':row[1] if row[1] != None else '',
						 'update_date':row[2].strftime("%Y-%m-%d") if row[2] != None else ''} 
				for row in data]
		except Exception as e:
			self.main_app.srvcnxn.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))


		for w_path, w_dirs, w_files in os.walk(dirpath):
			_listFiles = [os.path.splitext(i)[0]  for i in w_files]
			#del w_dirs[:] # смотрим только верхний уровень

		 # Отбираем все строки в _listVyrs не существующие в _listFiles
		_listVyrsNotMatchedFiles = [i for i in _listVyrs if i['vyr'] not in _listFiles]

		return _listVyrsNotMatchedFiles

	def __getLocalProjectName(self):
		local_project_name = None
		try:
			parents = self._triDbDsnUnitItem.iterParents()
			for i in range(0, len(parents)):
				if parents[i].isDsnUnit():
					if i < len(parents) - 1:
						local_project = parents[i-1].name()
						if not is_blank(local_project):
							local_project_name = local_project
							return local_project_name
		except:
			return local_project_name
						
		
