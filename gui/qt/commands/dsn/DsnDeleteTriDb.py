from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.osutil import set_file_extension
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.FileMapper import FileMapper


class DsnDeleteTriDb(DialogWindow):
	def __init__(self, parent, triDbDsnUnitItem, use_connection, *args, **kw):
		super(DsnDeleteTriDb, self).__init__(parent, use_connection, *args, **kw)
		self._triDbDsnUnitItem = triDbDsnUnitItem
		self._use_connection = use_connection
		self.setWindowTitle("Удалить выработку (TriDB)")
		


		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._newName = None
		
		
		self.localProjectId = None

	def __createWidgets(self):
		self.layoutParams = QtGui.QVBoxLayout()

		self.labelMessage1 = QtGui.QLabel(self)
		self.labelMessage1.setText("Вы действительно хотите удалить выработку (TriDb):<br/><b>'%s'</b>?"  % (self._triDbDsnUnitItem.name()) )
		self.labelMessage1.setMinimumWidth(100)
		self.labelMessage1.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
		
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.addWidget(self.labelMessage1)


	def check(self):

		return True

	def prepare(self):
		super(DsnDeleteTriDb, self).prepare()

		if not self.check():
			return

		if self._use_connection and self.main_app.srvcnxn is None:
			self.main_app.connect_to_database()

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно удалить выработку (TriDb). %s" % str(e))

		self.after_run()
		if self._is_success:
			self.close()

	def run(self):



		def restore_filenames():
			nonlocal unitPath, unitTempPath
			if not os.path.exists(unitPath):
				try:		os.rename(unitTempPath, unitPath)
				except:		pass
		#
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		layerName = self._triDbDsnUnitItem.getLayerName()
		layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		self.localProjectId = self.main_app.srvcnxn.getLocalProjectId(self.__getLocalProjectName(), layerId)
		#
		unitPath = self._triDbDsnUnitItem.filePath()
		dirpath = os.path.dirname(unitPath)
		unitTempPath = os.path.join(dirpath, set_file_extension(self._tmpprefix + self._triDbDsnUnitItem.name(), ITEM_TRIDB))	
	
		# Проверяем есть ли запись в БД соответствующая данному каркасу, если нет, 
		# то проверяем есть ли записи не соответствующие ни одному файлу
		# Если есть то показываем диалог предлагающий провести сопоставление с переименованием файла
		# Если сопоставление не проведено, то удаляется только файл
		if not self.__existTriDbLocProjInDb(self._triDbDsnUnitItem.name()):

			#Определяем корневую папку каркасов
			split_self_path = dirpath.upper().split('\\')
			index_tridb_path = split_self_path.index("КАРКАСЫ")

			tridb_path = ""
			for i in split_self_path[:index_tridb_path]:
				tridb_path = os.path.join(tridb_path,i)
			tridb_path = os.path.join(tridb_path, 'КАРКАСЫ' ) 
			tridb_path = tridb_path.replace(":", ":\\")

			#

			_listDbTriDbNotMatchedFiles = self.__getDbTriDbNotMatchedFiles(tridb_path) 
				
			if _listDbTriDbNotMatchedFiles != None and len(_listDbTriDbNotMatchedFiles) > 0:
				
				_fileMapper = FileMapper(self)
				_fileMapper.setMessage("В БД не найдена соответствующая запись для выбранного файла. Необходимо сопоставить файлу каркаса запись в базе данных.")
				_fileMapper.setFiles([self._triDbDsnUnitItem.filePath()])
				_dbObjects	=[]
				for i in _listDbTriDbNotMatchedFiles:
					_dbObjectName = i.pop('vyr')
					_dbObjectInformation = '<br/>'.join([ e[0]+' : '+e[1] for e in list(i.items())])
					_dbObjects.append((_dbObjectName,_dbObjectInformation))

				_fileMapper.setDbObjects (_dbObjects) # arg = [('objectName','Information'),<...>]
				_fileMapper.exec_()
				_model = _fileMapper.getModel()	  # returns [{'path': value, 'file': value, 'object':value}, <...>]
				for i in _model:
					if len (i['object'].strip()) > 0:
						ext = os.path.splitext(i['file'])[1]
						newName = i['object']+ext
						newPath = os.path.join(dirpath,newName)
						try:
							os.rename(i['path'], newPath)
						except:
							raise Exception ('Не удалось переименовать файл при сопоставлении')

						self._triDbDsnUnitItem.setName(i['object'])
						self._triDbDsnUnitItem.setFilePath (newPath)
						unitPath = self._triDbDsnUnitItem.filePath()
						unitTempPath = os.path.join(dirpath, set_file_extension(self._tmpprefix + self._triDbDsnUnitItem.name(), ITEM_TRIDB))

			 # Проверяем еще раз, сопоставили файл записи в БД или нет
			if not self.__existTriDbLocProjInDb(self._triDbDsnUnitItem.name()):
				qtmsgbox.show_information(self, "Внимание", "Выработка  отсутствует в БД. Будет удалён только файл.")


		#
		try:



			if self.__existTriDbLocProjInDb ( self._triDbDsnUnitItem.name()):  # Если существует Выработка  с таким именем

				# Узнаём Id выработки
				cmdSel1 ="""
					select VyrId from [{0}].[dbo].[VYRAB_P]  as VR
					inner join [{0}].[dbo].[LOCAL_PROJECT_MINES]  as P_M
					on P_M.MINE_ID = VR.VyrId
					where VR.VYR = ?  
					and P_M.LOC_PROJECT_ID = ?
				""" 
				cmdSel1 = cmdSel1.format(self.main_app.srvcnxn.dbsvy)
				params = ( self._triDbDsnUnitItem.name(), self.localProjectId)
				data = self.main_app.srvcnxn.exec_query(cmdSel1, params).get_rows()
				triDbLocProjectId = data[0][0]

				# Удаляем связку выработки с локальным проектом
				cmdDel_binding ="""
					delete P_M from [{0}].[dbo].[LOCAL_PROJECT_MINES]  as P_M
					where P_M.MINE_ID = ? 
				    and P_M.LOC_PROJECT_ID= ?
				"""
				cmdDel_binding = cmdDel_binding.format(self.main_app.srvcnxn.dbsvy)
				params = (triDbLocProjectId, self.localProjectId) 
				self.main_app.srvcnxn.exec_query(cmdDel_binding, params)


				# Узнаём привязана ли выработка с нашим Id к другим проектам, если да, то больше в БД ничего не трогаем
				cmdSel1 ="""
					select * from [{0}].[dbo].[LOCAL_PROJECT_MINES]  as P_M
					where P_M.MINE_ID = ?  
				""" 
				cmdSel1 = cmdSel1.format(self.main_app.srvcnxn.dbsvy)
				params = ( triDbLocProjectId)
				data = self.main_app.srvcnxn.exec_query(cmdSel1, params).get_rows()
				if not data:

					cmdSel2 ="""
					select  [MINE_ID] from [{0}].[dbo].[MINE_INTERVAL]
						where  [MINE_ID] = ?
					""" 
					cmdSel2 = cmdSel2.format(self.main_app.srvcnxn.dbsvy)
					params = (triDbLocProjectId)
					data = self.main_app.srvcnxn.exec_query(cmdSel2, params).get_rows()

					if len(data) > 0:	
						raise Exception ("На данный каркас ссылаются интервалы в таблице MINE_INTERVAL. Удаление не возможно.")

					cmdDel1 ="""
					delete  from [{0}].[dbo].[MINE_LONGWALLS]  
						where  MINE_ID = ?
					"""
					params1 = (triDbLocProjectId)
					cmdDel1 = cmdDel1.format(self.main_app.srvcnxn.dbsvy)
					cmdDel2 ="""
					delete  from [{0}].[dbo].[MINE_PANELS]  
						where  MINE_ID = ?
					"""
					params2 = (triDbLocProjectId)
					cmdDel2 = cmdDel2.format(self.main_app.srvcnxn.dbsvy)
					cmdDel3 ="""
					delete  from [{0}].[dbo].[VYRAB_P]  
						where  VyrId = ?
					"""
					params3 = (triDbLocProjectId)
					cmdDel3 = cmdDel3.format(self.main_app.srvcnxn.dbsvy)

				
					try:   
						self.main_app.srvcnxn.exec_query(cmdDel1, params1)
						self.main_app.srvcnxn.exec_query(cmdDel2, params2)
						self.main_app.srvcnxn.exec_query(cmdDel3, params3)
					except Exception as e:
						raise Exception("Невозможно удалить выработку из БД. %s" % str(e))

			# переименовываем файлы
			try:
				os.rename(unitPath, unitTempPath)
				os.remove(unitTempPath)	
				
			except Exception as e:
				raise Exception("Невозможно удалить выработку '%s'" % str(e))
			
			self.main_app.srvcnxn.commit()
			
		except Exception as e:
			# возвращаем имена, если требуется
			restore_filenames()
			self.main_app.srvcnxn.rollback()
			raise Exception(str(e))

		#переименовываем в интерфейсе
		try:
			parent = self._triDbDsnUnitItem.parent()
			parent.removeChild(self._triDbDsnUnitItem)
		except Exception as e:
			raise Exception("Невозможно обновить дерево папок. Перезапустите скрипт. Ошибка: %s" % str(e))

	
		qtmsgbox.show_information(self, "Выполнено", "Выработка успешно удалена")

	def __existTriDbLocProjInDb(self, triDbName):
		if triDbName == None:
			return False



		cmd ="""
		select * from [{0}].[dbo].[VYRAB_P]  as VR
			inner join [{0}].[dbo].[LOCAL_PROJECT_MINES]  as P_M
			on P_M.MINE_ID = VR.VyrId
			where VR.VYR = ? 
		    and P_M.LOC_PROJECT_ID = ?
		"""
		params = (triDbName, self.localProjectId)
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			self.main_app.srvcnxn.rollback()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))
		if data:
			return True
		else:
			return False

	def __getDbTriDbNotMatchedFiles(self, dirpath):

		if dirpath == None:
			return False

		cmd ="""
		select V.VYR, V.USERNAME, V.UPDATEDATE
			 from [{0}].[dbo].[VYRAB_P] as V 
			 inner join  [{0}].[dbo].[LOCAL_PROJECT_MINES] as P_M
			 on P_M.MINE_ID = V.VyrId
			 where P_M.LOC_PROJECT_ID = ?
		"""
		params = (self.localProjectId)
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
			_listVyrs = [{'vyr':row[0],
						 'project_name':self.__getLocalProjectName(),
						 'user_name':row[1] if row[1] != None else '',
						 'update_date':row[2].strftime("%Y-%m-%d") if row[2] != None else ''} 
				for row in data]
		except Exception as e:
			self.main_app.srvcnxn.rollback()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))


		for w_path, w_dirs, w_files in os.walk(dirpath):
			_listFiles = [os.path.splitext(i)[0]  for i in w_files]
			#del w_dirs[:] # смотрим только верхний уровень

		 # Отбираем все строки в _listVyrs не существующие в _listFiles
		_listVyrsNotMatchedFiles = [i for i in _listVyrs if i['vyr'] not in _listFiles]

		return _listVyrsNotMatchedFiles

	def __getLocalProjectName(self):
		local_project_name = None
		try:
			parents = self._triDbDsnUnitItem.iterParents()
			for i in range(0, len(parents)-1):
				if parents[i].isDsnUnit():
					if i < len(parents) - 1:
						local_project = parents[i-1].name()
						if not is_blank(local_project):
							local_project_name = local_project
							return local_project_name
		except:
			return local_project_name


						
		
