from PyQt4 import QtGui, QtCore
import os
from  uuid import uuid4
from datetime import datetime

from mm.mmutils import Tridb, MicromineFile
from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.osutil import get_current_time
from utils.validation import MineSuffixValidator, DistanceValidator, DoubleValidator , IntValidator

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import CustomDataModel
from gui.qt.widgets.DirectoryView import DirectoryView
from gui.qt.ProcessWindow import DialogWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

OTHER = "ПРОЧЕЕ"

def saveMineToDatabaseVer2(cnxn, locationName, layerName, mineName, locProjectName, purpose, geoUnitNames, projectDate, techGridNames = [], longWallNames = [], levelName = None, levelValue = None, mineOwner = None):
	# извлекаем id залежи
	layerId = cnxn.getLayerId(layerName, locationName)
	if layerId is None:
		raise Exception("Невозможно определить уникальный номер залежи '%s'" % str(layerName))
	#
	# извлекаем id локального проекта
	locProjectId = cnxn.getLocalProjectId(locProjectName, layerId)
	if locProjectId is None:
		raise Exception("Невозможно определить уникальный номер локального проекта '%s'" % str(locProjectName))

	# извлекаем категорию работ
	category = cnxn.getWorkCategory(purpose)
	if category is None:
		raise Exception("Невозможно определить категорию работ для '%s'" % purpose)
	# извлекаем id уровня
	if levelName is not None:
		cmd = "SELECT LEVEL_NAME_ID FROM [{0}].[dbo].[LKP_LEVEL_NAME] WHERE LEVEL_NAME = ?".format(cnxn.dbsvy)
		params = ( levelName)
		data = cnxn.exec_query(cmd, levelName).get_rows()
		levelId = data[0][0] if data else None
	else:
		levelId = None
	# значение уровня
	levelValue = levelValue if levelValue else None
	#

	# извлекаем id  выработки с нужным именем
	placeholders = ','.join('?' for i in range(len(geoUnitNames))) 
	cmd = """ 
		SELECT V.VyrId FROM [{0}].[dbo].[VYRAB_P]  AS V
		INNER JOIN [{0}].[dbo].[MINE_PANELS]  AS M_P
		ON M_P.MINE_ID = V.VyrId 
			INNER JOIN [{0}].[dbo].[PANEL] AS P
			ON P.PANEL_ID = M_P.PANEL_ID
		WHERE  V.Vyr = ? 
				AND P.PANEL_NAME in ({1})
				AND P.LAYER_ID = ?
		""".format(cnxn.dbsvy, placeholders)


	params = [mineName]
	params.extend(geoUnitNames)
	params.append(layerId)	

	data = cnxn.exec_query(cmd, params).get_rows()

	mineId = data[0][0] if data else None

	msg = []
	if not mineId  is None:
		msg.append("Выработка {0} уже существовала.\n".format(mineName))
		# Добавить к владельцам выработки
		cmd = """
			UPDATE [{0}].[dbo].[VYRAB_P]
			SET MINE_OWNER = MINE_OWNER | ?
			WHERE VyrId = ?
		""".format(cnxn.dbsvy)
		params = ( mineOwner, mineId)
		cnxn.exec_query(cmd, params)

	if mineId is None:
		# добавляем новую выработку
		guid = 	uuid4()
		cmd = "INSERT INTO [{0}].[dbo].[VYRAB_P] ( Vyr, LEVEL_NAME_ID, LEVEL_VALUE, WORK_TYPE, DATE_PROJECTED, MINE_OWNER) VALUES ( '{1}' ".format(cnxn.dbsvy, guid)
		if levelId is None:			cmd += ", NULL"
		else:						cmd += ", {0} ".format(levelId) 
		if levelValue is None:		cmd += ", NULL"
		else:						cmd += ", {0} ".format(levelValue) 
		cmd += ", '{0}', CONVERT(smalldatetime, '{1}', 121), {2}) ".format(category, projectDate.strftime("%Y-%m-%d"), mineOwner)
		print (cmd)

		cnxn.exec_query(cmd)
	


		# извлекаем id добавленной выработки
		#cmd = """
		#	select SCOPE_IDENTITY() 
		#"""	
		#data = cnxn.exec_query(cmd).get_rows()
		cmd = """
			SELECT VyrId FROM  [{0}].[dbo].[VYRAB_P] 
			WHERE Vyr = ? 
		""".format(cnxn.dbsvy)			
		params = (str(guid))

		data = cnxn.exec_query(cmd, params).get_rows()


		mineId = data[0][0] if data else None
		if mineId is None:
			raise Exception("Невозможно определить уникальный номер выработки '%s'" % str(mineName))

		cmd = """
	    UPDATE  [{0}].[dbo].[VYRAB_P] 
		SET Vyr = ? 
		WHERE VyrId = ? 	
		""".format(cnxn.dbsvy)
		params = [mineName, mineId]

		cnxn.exec_query(cmd, params)

	# добавляем связь проекта и выработки
	locProjectId = cnxn.getLocalProjectId(locProjectName, layerId)
	if locProjectId is None:
		raise Exception("Невозможно определить уникальный номер локального проекта '%s'" % str(locProjectName))

	cmd = "INSERT INTO [{0}].[dbo].[LOCAL_PROJECT_MINES] (MINE_ID, LOC_PROJECT_ID) VALUES (? , ?)".format(cnxn.dbsvy)
	params = ( mineId, locProjectId)
	try:
		cnxn.exec_query(cmd, params)
	except Exception as e:
		msg.append("Выработка {0} уже была привязана к проекту {1}.\n".format(mineName, locProjectName) )

		# связь выработки и профилей
	lst_techGridNames_longWallNames = set(zip(longWallNames, techGridNames))
	for longWallName , techGridName in lst_techGridNames_longWallNames:
		# извлекаем id технологической сети
		techGridId = cnxn.getTechGridId(techGridName, locationName)
		if techGridId is None:
			raise Exception("Невозможно определить уникальный номер тех сети '%s'" % str(techGridName))
		longWallId = cnxn.getLongWallId(longWallName, techGridId)
		if longWallId is None:
			raise Exception("Невозможно определить уникальный номер профиля '%s'" % str(longWallName))

		cmd = "INSERT INTO [{0}].[dbo].[MINE_LONGWALLS] (MINE_ID, LONGWALL_ID) VALUES ( ?, ?)".format(cnxn.dbsvy)
		params = ( mineId, longWallId)
		try:
			cnxn.exec_query(cmd, params)
		except Exception as e:
			msg.append("Выработка {0} уже была привязана к ленте {1}.\n".format(mineName, longWallName) )

		# связь выработки и панелей		
	for geoUnitName in geoUnitNames:
		panelId = cnxn.getPanelId(geoUnitName, layerId)
		if panelId is None:
			raise Exception("Невозможно определить уникальный номер панели '%s'" % str(geoUnitName))
		cmd = "INSERT INTO [{0}].[dbo].[MINE_PANELS] (MINE_ID, PANEL_ID) VALUES (?, ?)".format(cnxn.dbsvy)
		params = ( mineId, panelId)
		try:
			cnxn.exec_query(cmd, params)
		except Exception as e:
			msg.append("Выработка {0} уже была привязана к панели {1}.\n".format(mineName, geoUnitName) )


	return msg

class DsnDefineModelVer2(DialogWindow):
	def __init__(self, *args, **kw):
		super(DsnDefineModelVer2, self).__init__(*args, **kw)

		self._default_title = "Создать проектную выработку"
		self.setWindowTitle(self._default_title)
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.buildCombos()

	def __createVariables(self):
		self._minesites_id = None
	def __createWidgets(self):
		self.centralWidget = QtGui.QWidget(self)

		self.layoutCentral = QtGui.QGridLayout()
		self.layoutCentral.setMargin(0)
		self.layoutCentral.setSpacing(2)

		self.groupBoxProjectMine = QtGui.QGroupBox(self)
		self.groupBoxProjectMine.setTitle("Проектная выработка")
		self.layoutProjectMine = QtGui.QFormLayout()

		self.labelCategory = QtGui.QLabel(self.groupBoxProjectMine)
		self.labelCategory.setText("Категория работ")
		self.labelCategory.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelCategory.setMaximumWidth(130)
		self.labelCategory.setMinimumWidth(130)
		self.comboBoxCategory = QtGui.QComboBox(self.groupBoxProjectMine)
		self.comboBoxCategory.setMaximumWidth(180)
		self.comboBoxCategory.setMinimumWidth(250)

		self.labelName = QtGui.QLabel(self.groupBoxProjectMine)
		self.labelName.setText("Наименование")
		self.labelName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelName.setMaximumWidth(130)
		self.labelName.setMinimumWidth(130)
		self.comboBoxName = QtGui.QComboBox(self.groupBoxProjectMine)
		self.comboBoxName.setMaximumWidth(180)
		self.comboBoxName.setMinimumWidth(250)
		
		self.labelDate = QtGui.QLabel(self.groupBoxProjectMine)
		self.labelDate.setText("Дата проектирования")
		self.labelDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDate.setMaximumWidth(130)
		self.labelDate.setMinimumWidth(130)
		self.dateEditProjectDate = QtGui.QDateEdit(self.groupBoxProjectMine)
		self.dateEditProjectDate.setMaximumWidth(120)
		self.dateEditProjectDate.setMinimumWidth(120)
		self.dateEditProjectDate.setCalendarPopup(True)
		self.dateEditProjectDate.calendarWidget().setFirstDayOfWeek(QtCore.Qt.Monday)
		self.dateEditProjectDate.setDisplayFormat("yyyy-MM-dd")
		currentTime = get_current_time()
		self.dateEditProjectDate.setDate(currentTime)

		#
		self.groupBoxLocationProject = QtGui.QGroupBox(self)
		self.groupBoxLocationProject.setTitle("Привязка выработки")
		self.layoutLocationProject = QtGui.QFormLayout()
		self.layoutLocationProject.setAlignment(QtCore.Qt.AlignTop)

		self.labelLayer = QtGui.QLabel(self.groupBoxLocationProject)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLayer.setMaximumWidth(150)
		self.labelLayer.setMinimumWidth(150)
		self.comboBoxLayer = QtGui.QComboBox(self.groupBoxLocationProject)
		self.comboBoxLayer.setMaximumWidth(280)
		self.comboBoxLayer.setMinimumWidth(280)

		self.label_local_project = QtGui.QLabel(self.groupBoxLocationProject)
		self.label_local_project.setText("Локальный проект")
		self.label_local_project.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.label_local_project.setMaximumWidth(150)
		self.label_local_project.setMinimumWidth(150)
		self.line_edit_local_project = QtGui.QLineEdit(self.groupBoxLocationProject)
		self.line_edit_local_project.setMaximumWidth(280)
		self.line_edit_local_project.setReadOnly(True)

		self.labelminesitechoosen = QtGui.QLabel(self.groupBoxLocationProject)
		self.labelminesitechoosen.setText("Участок:")
		self.labelminesitechoosen.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelminesitechoosen.setMinimumWidth(150)

		self.minesitechoosen = QtGui.QLabel(self.groupBoxLocationProject)
		self.minesitechoosen.setAlignment(QtCore.Qt.AlignLeft)
		self.minesitechoosen.setMinimumWidth(280)
		self.minesitechoosen.setText("не выбран")


		self.groupBoxLocationSubunit = QtGui.QGroupBox(self)
		self.groupBoxLocationSubunit.setTitle("Расположение выработки. Горизонты.")
		self.layoutLocationSubunit = QtGui.QFormLayout()
		self.layoutLocationSubunit.setAlignment(QtCore.Qt.AlignTop)

		self.labelGeoUnit = QtGui.QLabel(self.groupBoxLocationSubunit)
		self.labelGeoUnit.setText(self.main_app.settings.getValue(ID_GEO_UDSPNAME))
		self.labelGeoUnit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelGeoUnit.setMaximumWidth(120)
		self.labelGeoUnit.setMinimumWidth(120)
		self.lineEditGeoUnit = QtGui.QLineEdit(self.groupBoxLocationSubunit)
		self.lineEditGeoUnit.setMaximumWidth(180)
		self.lineEditGeoUnit.setMinimumWidth(180)
		self.lineEditGeoUnit.setReadOnly(True)

		self.labelSubUnitFrom = QtGui.QLabel(self.groupBoxLocationSubunit)
		self.labelSubUnitFrom.setText("От подгоризонта")
		self.labelSubUnitFrom.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSubUnitFrom.setMaximumWidth(120)
		self.labelSubUnitFrom.setMinimumWidth(120)
		self.comboBoxSubUnitFrom = QtGui.QComboBox(self.groupBoxLocationSubunit)
		self.comboBoxSubUnitFrom.setMaximumWidth(180)
		self.comboBoxSubUnitFrom.setMinimumWidth(180)

		self.labelSubUnitTo = QtGui.QLabel(self.groupBoxLocationSubunit)
		self.labelSubUnitTo.setText("До подгоризонта")
		self.labelSubUnitTo.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSubUnitTo.setMaximumWidth(120)
		self.labelSubUnitTo.setMinimumWidth(120)
		self.comboBoxSubUnitTo = QtGui.QComboBox(self.groupBoxLocationSubunit)
		self.comboBoxSubUnitTo.setMaximumWidth(180)
		self.comboBoxSubUnitTo.setMinimumWidth(180)


		self.groupBoxAdvanced = QtGui.QGroupBox(self)
		self.groupBoxAdvanced.setTitle("Дополнительная информация")
		self.layoutAdvanced = QtGui.QGridLayout()
		self.layoutAdvanced.setAlignment(QtCore.Qt.AlignTop)

		self.labelNumberValue = QtGui.QLabel(self.groupBoxAdvanced)
		self.labelNumberValue.setText("Номер")
		self.labelNumberValue.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelNumberValue.setMaximumWidth(130)
		self.lineEditNumberValue = QtGui.QLineEdit(self.groupBoxAdvanced)
		self.lineEditNumberValue.setMaximumWidth(250)
		self.lineEditNumberValue.setMinimumWidth(250)
		validator = IntValidator()
		validator.setBottom(0)
		self.lineEditNumberValue.setValidator(validator)


		self.labelSuffix = QtGui.QLabel(self.groupBoxAdvanced)
		self.labelSuffix.setText("Суффикс")
		self.labelSuffix.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelSuffix.setMaximumWidth(130)
		self.labelSuffix.setMinimumWidth(130)
		self.lineEditSuffix = QtGui.QLineEdit(self.groupBoxAdvanced)
		self.lineEditSuffix.setMaximumWidth(250)
		self.lineEditSuffix.setMinimumWidth(250)
		self.lineEditSuffix.setValidator(MineSuffixValidator())


		self.layoutExplorer = QtGui.QFormLayout()
		self.boxExplorer = QtGui.QGroupBox(self)
		self.boxExplorer.setTitle("Вывод")
		self.directoryView = DirectoryView(self.boxExplorer)
		# self.directoryView.setMaximumHeight(100)
		self.directoryView.setMaximumWidth(280)
		sizePolicy = self.directoryView.sizePolicy()
		sizePolicy.setVerticalStretch(1)
		self.directoryView.setSizePolicy(sizePolicy)
		for ittype in ITEM_TYPE_LIST:
			if not ittype in [ITEM_DIR, ITEM_TRIDB]:
				self.directoryView.hideItemType(ittype)
			else:
				self.directoryView.showItemType(ittype)

		self.labelOutTriType = QtGui.QLabel(self.boxExplorer)
		self.labelOutTriType.setText("Выбор папки \n (Esc - снять выделение)")
		self.labelOutTriType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutTriType.setMinimumWidth(150)
		self.labelOutTriType.setMaximumWidth(150)

		self.labelDirChoosen = QtGui.QLabel(self.boxExplorer)
		self.labelDirChoosen.setText("Выбранная папка:")
		self.labelDirChoosen.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDirChoosen.setMinimumWidth(150)


		self.DirChoosen = QtGui.QLabel(self.boxExplorer)
		self.DirChoosen.setAlignment(QtCore.Qt.AlignLeft)
		self.DirChoosen.setMinimumWidth(300)
		self.DirChoosen.setText("не выбрана")


		self.groupBoxLocationProfile = QtGui.QGroupBox(self)
		self.groupBoxLocationProfile.setTitle("Расположение выработки. Профили")
		self.layoutLocationProfile = QtGui.QGridLayout()
		self.layoutLocationProfile.setAlignment(QtCore.Qt.AlignTop)

		self.labelProfileFrom = QtGui.QLabel(self.groupBoxLocationProfile)
		self.labelProfileFrom.setText("От")
		self.labelProfileFrom.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelProfileFrom.setMaximumWidth(40)
		self.labelProfileFrom.setMinimumWidth(40)

		self.labelProfileTo = QtGui.QLabel(self.groupBoxLocationProfile)
		self.labelProfileTo.setText("До")
		self.labelProfileTo.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelProfileTo.setMaximumWidth(40)
		self.labelProfileTo.setMinimumWidth(40)

		self.labelTechGridCol = QtGui.QLabel(self.groupBoxLocationProfile)
		self.labelTechGridCol.setText("Технологическая сеть")
		self.labelTechGridCol.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
		self.labelTechGridCol.setMaximumWidth(135)
		self.labelTechGridCol.setMinimumWidth(135)

		self.labelProfileCol = QtGui.QLabel(self.groupBoxLocationProfile)
		self.labelProfileCol.setText("Профиль")
		self.labelProfileCol.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelProfileCol.setMaximumWidth(120)
		self.labelProfileCol.setMinimumWidth(120)

		self.labelDistanceCol = QtGui.QLabel(self.groupBoxLocationProfile)
		self.labelDistanceCol.setText("Расстояние")
		self.labelDistanceCol.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDistanceCol.setMaximumWidth(120)
		self.labelDistanceCol.setMinimumWidth(120)


		self.comboBoxTechGridFrom = QtGui.QComboBox(self.groupBoxLocationProfile)
		self.comboBoxTechGridFrom.setMaximumWidth(120)
		self.comboBoxTechGridFrom.setMinimumWidth(120)


		self.comboBoxTechGridTo = QtGui.QComboBox(self.groupBoxLocationProfile)
		self.comboBoxTechGridTo.setMaximumWidth(120)
		self.comboBoxTechGridTo.setMinimumWidth(120)

		self.lineEditProfileFrom = QtGui.QLineEdit(self.groupBoxLocationProfile)
		self.lineEditProfileFrom.setMaximumWidth(120)
		self.lineEditProfileFrom.setMinimumWidth(120)
		self.lineEditProfileFrom.setReadOnly(True)

		self.lineEditProfileTo = QtGui.QLineEdit(self.groupBoxLocationProfile)
		self.lineEditProfileTo.setMaximumWidth(120)
		self.lineEditProfileTo.setMinimumWidth(120)
		self.lineEditProfileTo.setReadOnly(True)

		self.lineEditDistanceFrom= QtGui.QLineEdit(self.groupBoxLocationProfile)
		self.lineEditDistanceFrom.setMaximumWidth(120)								  
		self.lineEditDistanceFrom.setMaximumWidth(120)
		self.lineEditDistanceFrom.setValidator(DistanceValidator())
		self.lineEditDistanceFrom.setEnabled(False)
		
		self.lineEditDistanceTo= QtGui.QLineEdit(self.groupBoxLocationProfile)
		self.lineEditDistanceTo.setMaximumWidth(120)
		self.lineEditDistanceTo.setMaximumWidth(120)
		self.lineEditDistanceTo.setValidator(DistanceValidator())
		self.lineEditDistanceTo.setEnabled(False)



		self.checkBoxExclude = QtGui.QCheckBox(self.groupBoxLocationProfile)
		self.checkBoxExclude.setText("Исключить "+self.main_app.settings.getValue(ID_GEO_UDSPNAME).lower()+" и ленту из наименования")
		self.checkBoxExclude.setEnabled(False)


	def __createBindings(self):

		self.comboBoxLayer.currentIndexChanged.connect(self.layerChanged)
		self.lineEditGeoUnit.textChanged.connect(self.geoUnitChanged)

		self.comboBoxTechGridFrom.currentIndexChanged.connect(self.techGridFromChanged)
		self.comboBoxTechGridTo.currentIndexChanged.connect(self.techGridToChanged)
		self.comboBoxCategory.currentIndexChanged.connect(self.workTypeChanged)	 
		self.comboBoxSubUnitFrom.currentIndexChanged.connect(lambda e: self.subUnitChanged(self.comboBoxSubUnitFrom, self.comboBoxSubUnitTo))	 
		self.comboBoxSubUnitTo.currentIndexChanged.connect(lambda e: self.subUnitChanged( self.comboBoxSubUnitTo, self.comboBoxSubUnitFrom))
		self.lineEditGeoUnit.mouseDoubleClickEvent = lambda e: self.selectGeoUnit()

		self.lineEditProfileFrom.mouseDoubleClickEvent = lambda e: self.selectLongwall(self.lineEditProfileFrom, self.comboBoxTechGridFrom, self.lineEditProfileTo)
		self.lineEditProfileTo.mouseDoubleClickEvent = lambda e: self.selectLongwall(self.lineEditProfileTo, self.comboBoxTechGridTo, self.lineEditProfileFrom)

		self.lineEditProfileFrom.textChanged.connect(self.profileFromChanged)
		self.lineEditProfileTo.textChanged.connect(self.profileToChanged)

		self.comboBoxCategory.currentIndexChanged.connect(self._updateWindowTitle)
		self.comboBoxLayer.currentIndexChanged.connect(self._updateWindowTitle)
		self.lineEditGeoUnit.textChanged.connect(self._updateWindowTitle)

		self.comboBoxName.currentIndexChanged.connect(self._updateWindowTitle)
		self.lineEditNumberValue.textChanged.connect(self._updateWindowTitle)
		self.lineEditProfileFrom.textChanged.connect(self._updateWindowTitle)
		self.lineEditProfileTo.textChanged.connect(self._updateWindowTitle)
		self.lineEditDistanceFrom.textChanged.connect(self._updateWindowTitle)
		self.lineEditDistanceTo.textChanged.connect(self._updateWindowTitle)
		self.lineEditDistanceFrom.textChanged.connect(self._updateWindowTitle)
		self.lineEditDistanceTo.textChanged.connect(self._updateWindowTitle)
		self.comboBoxSubUnitFrom.currentIndexChanged.connect(self._updateWindowTitle)
		self.comboBoxSubUnitTo.currentIndexChanged.connect(self._updateWindowTitle)
		self.lineEditSuffix.textChanged.connect(self._updateWindowTitle)

		self.checkBoxExclude.stateChanged.connect(self._updateWindowTitle)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

		self.line_edit_local_project.mouseDoubleClickEvent = lambda e: self.select_local_project()
		self.line_edit_local_project.textChanged.connect(self.local_project_changed)

		selmodel = self.directoryView.selectionModel()
		selmodel.selectionChanged.connect(self.treeAction)

	def treeAction(self, selected, deselected):

		if not self.directoryView.selectedItems():
			pass

		if not self.directoryView.selectedItems():
			self.DirChoosen.setText("...\\" + self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1])
		else:
			for index in selected.indexes():
				item = self.directoryView.itemFromIndex(index)
				temp = self.directoryView.selectedItems()[0].filePath()
				param1, param2 = temp.split(self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1], 1)
				self.DirChoosen.setText("...\\" + \
										self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1] + \
										param2)
######## end edit
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.centralWidget)
		self.centralWidget.setLayout(self.layoutCentral)

		self.layoutCentral.addWidget(self.groupBoxProjectMine, 0, 0, 1, 1)
		self.groupBoxProjectMine.setLayout(self.layoutProjectMine)
		self.layoutProjectMine.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelCategory)
		self.layoutProjectMine.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxCategory)
		self.layoutProjectMine.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelName)
		self.layoutProjectMine.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxName)
		self.layoutProjectMine.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelDate)
		self.layoutProjectMine.setWidget(2, QtGui.QFormLayout.FieldRole, self.dateEditProjectDate)

		self.layoutCentral.addWidget(self.groupBoxLocationProject, 0, 1, 1, 1)
		self.groupBoxLocationProject.setLayout(self.layoutLocationProject)
		self.layoutLocationProject.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.layoutLocationProject.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)
		self.layoutLocationProject.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_local_project)
		self.layoutLocationProject.setWidget(1, QtGui.QFormLayout.FieldRole, self.line_edit_local_project)
		self.layoutLocationProject.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelminesitechoosen)
		self.layoutLocationProject.setWidget(2, QtGui.QFormLayout.FieldRole, self.minesitechoosen)

		self.layoutCentral.addWidget(self.groupBoxLocationSubunit, 0, 2, 1, 1)
		self.groupBoxLocationSubunit.setLayout(self.layoutLocationSubunit)
		self.layoutLocationSubunit.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelGeoUnit)
		self.layoutLocationSubunit.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditGeoUnit)
		self.layoutLocationSubunit.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSubUnitFrom)
		self.layoutLocationSubunit.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxSubUnitFrom)
		self.layoutLocationSubunit.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSubUnitTo)
		self.layoutLocationSubunit.setWidget(2, QtGui.QFormLayout.FieldRole, self.comboBoxSubUnitTo)

		self.layoutCentral.addWidget(self.groupBoxAdvanced, 1, 0, 1, 1)
		self.groupBoxAdvanced.setLayout(self.layoutAdvanced)

		self.layoutAdvanced.addWidget(self.labelNumberValue, 0, 0, 1, 1)
		self.layoutAdvanced.addWidget(self.lineEditNumberValue, 0, 1, 1, 1)
		self.layoutAdvanced.addWidget(self.labelSuffix, 1, 0, 1, 1)
		self.layoutAdvanced.addWidget(self.lineEditSuffix, 1, 1, 1, 1)

		
	################ start edit
		self.layoutCentral.addWidget(self.boxExplorer, 1, 1, 1, 1)
		self.boxExplorer.setLayout(self.layoutExplorer)
		self.layoutExplorer.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutTriType)
		self.layoutExplorer.setWidget(0, QtGui.QFormLayout.FieldRole, self.directoryView)
		self.layoutExplorer.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelDirChoosen)
		self.layoutExplorer.setWidget(1, QtGui.QFormLayout.FieldRole, self.DirChoosen)

	################ end edit

		self.layoutCentral.addWidget(self.groupBoxLocationProfile, 1, 2, 1, 1)
		self.groupBoxLocationProfile.setLayout(self.layoutLocationProfile)
		self.layoutLocationProfile.addWidget(self.labelProfileFrom,1,0)
		self.layoutLocationProfile.addWidget(self.labelProfileTo,2,0)
		self.layoutLocationProfile.addWidget(self.labelTechGridCol,0,1)
		self.layoutLocationProfile.addWidget(self.labelProfileCol,0,2)
		self.layoutLocationProfile.addWidget(self.labelDistanceCol,0,3)
		self.layoutLocationProfile.addWidget(self.comboBoxTechGridFrom,1,1)
		self.layoutLocationProfile.addWidget(self.comboBoxTechGridTo,2,1)	 
		self.layoutLocationProfile.addWidget(self.lineEditProfileFrom,1,2)
		self.layoutLocationProfile.addWidget(self.lineEditProfileTo,2,2)
		self.layoutLocationProfile.addWidget(self.lineEditDistanceFrom,1,3)
		self.layoutLocationProfile.addWidget(self.lineEditDistanceTo,2,3)

		self.layoutControls.addWidget(self.checkBoxExclude)

	#
	def buildCombos(self): 
		self.buildLayerList()
		self.buildTechGridList()
		self.buildWorkTypeList()
		self.buildNameList()


	def buildLayerList(self):
		self.comboBoxLayer.clear()
		#
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT LAYER_DESCRIPTION
			FROM [{0}].[dbo].[LAYER]
			WHERE LOCATION = ?
			ORDER BY LAYER_DESCRIPTION
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		layers = [""] + [row[0] for row in data]
		self.comboBoxLayer.addItems(layers)

	def select_local_project(self):
		layerName = self.comboBoxLayer.currentText()
		if not layerName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите залежь")
			return

		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT D.DepartId, P.PitName, D.DepartName, G.WorkName, N.LOC_PROJECT_NAME
			FROM [{0}].[dbo].[DEPARTS] D JOIN [{0}].[dbo].[PITS] P ON D.DepartPitId = P.PitId
			JOIN [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] T ON D.DepartId = T.DepartId
			JOIN [{0}].[dbo].[LOCAL_PROJECT] N ON N.LOC_PROJECT_ID = T.LOC_PROJECT_ID
			LEFT OUTER JOIN [{0}].[dbo].[LOCAL_PROJECT_WORK_TYPES] F ON F.LOC_PROJECT_ID = N.LOC_PROJECT_ID
			LEFT OUTER JOIN [{0}].[dbo].[WORKS] G ON F.WORK_TYPE_ID = G.WorkID
			WHERE P.LOCATION = ?
			ORDER BY D.DepartName
		""".format(self.main_app.srvcnxn.dbsvy)

		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not data:
			return

		for i in data:
			if i[3] is None:
				i[3] = "Нет данных"

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите участок")
		model = CustomDataModel(dialog.view, data, ["ID", "ШАХТА", "УЧАСТОК", "ВИД РАБОТ", "ЛОКАЛЬНЫЙ ПРОЕКТ"])
		dialog.view.setModel(model, combobox_filter=[1, 2, 3, 4])
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.view.hideColumn(0)
		dialog.resize(1000, 480)
		dialog.exec()

		result = dialog.getResult()
		if result:
			self._minesites_id = result[0][0]
			self.line_edit_local_project.setText(result[0][4])
			self.localProjectNAME = result[0][4]
			self.minesitechoosen.setText(result[0][2])

		self.buildOutTypeList()

	def selectMineSite(self):
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT D.DepartId, D.DepartName, P.PitName
			FROM [{0}].[dbo].[DEPARTS] D JOIN [{0}].[dbo].[PITS] P ON D.DepartPitId = P.PitId
			WHERE P.LOCATION = ?
			ORDER BY D.DepartName
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		if not data:
			return

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите участок")
		model = CustomDataModel(dialog.view, data, ["ID", "УЧАСТОК", "ШАХТА"])
		dialog.view.setModel(model)
		dialog.view.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		dialog.view.hideColumn(0)
		dialog.resize(400, 480)
		dialog.exec()

		result = dialog.getResult()
		if result:
			self._minesites_id = result[0][0]
			self.lineEditMineSite.setText(result[0][1])

		self.MineSiteNAME = result[0][1]


	def mineSiteChanged(self):
		self.comboBoxLocalProject.clear()

		layerName = self.comboBoxLayer.currentText()
		mineSiteName = self.minesitechoosen.text()

		if not layerName or not mineSiteName:
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		dsndir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerdir = dsndir + "\\" + layerName
		mineSiteDir = layerdir + "\\" + mineSiteName
		prefix = self.main_app.settings.getValue(ID_DSN_UPREFIX)

		locProjectNames = [""] + list(filter(lambda x: x.lower() in map(lambda f: f.lower(), os.listdir(mineSiteDir)),
											 self.getProjectNames(layerName)))
		self.comboBoxLocalProject.addItems(locProjectNames)

	def getProjectNames(self, layerName):
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		try:					layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
		except Exception as e:	return
		#
		if layerId is None:
			return
		#
		cmd = """
			SELECT LOC_PROJECT_NAME
			FROM [{0}].[dbo].[LOCAL_PROJECT]
			WHERE LAYER_ID = ?
			ORDER BY LOC_PROJECT_NAME
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (layerId)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		return [row[0] for row in data]


	def buildOutTypeList(self):
		self.directoryView.setFolderPath(None)

		layerName = self.comboBoxLayer.currentText()
		minesiteName = self.minesitechoosen.text()
		localprojectName = self.line_edit_local_project.text()

		if not all([layerName, minesiteName, localprojectName]):
			return

		netpath = self.main_app.settings.getValue(ID_NETPATH)
		svyDir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerDir = svyDir + "\\" + layerName
		minesiteDir = layerDir + "\\" + minesiteName
		tridir = minesiteDir + "\\" + localprojectName + '\\' +\
				 self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1]
		dir = "...\\" + self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1]
		self.directoryView.setFolderPath(tridir)
		self.DirChoosen.setText(dir)

		for t in self.directoryView.iterChildren():
			if t.__class__.__name__ != 'TreeModelDirItem':
				t.setDisabled(True)


	def buildTechGridList(self):
		self.comboBoxTechGridFrom.clear()
		self.comboBoxTechGridTo.clear()
		#
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = "SELECT TECH_GRID_NAME FROM [{0}].[dbo].[TECH_GRID] WHERE LOCATION = ?".format(self.main_app.srvcnxn.dbsvy)
		params = (locationName)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		techGridNames = [""] + [row[0] for row in data]
		self.comboBoxTechGridFrom.addItems(techGridNames)
		self.comboBoxTechGridTo.addItems(techGridNames)
	def buildLevelNameList(self):
		self.comboBoxLevelName.clear()
		cmd = "SELECT LEVEL_NAME FROM [%s].[dbo].[LKP_LEVEL_NAME]" % self.main_app.srvcnxn.dbsvy
		data = self.main_app.srvcnxn.exec_query(cmd).get_rows()
		levelNames = [""] + [row[0] for row in data]
		self.comboBoxLevelName.addItems(levelNames)
	def buildWorkTypeList(self):
		self.comboBoxCategory.clear()
		cmd = "SELECT CATEGORY_DESCRIPTION FROM [%s].[dbo].[LKP_MINE_NAMES_CATEGORY] WHERE MINE_NAME_CATEGORY <> ? ORDER BY MINE_NAME_CATEGORY" % (self.main_app.srvcnxn.dbsvy)
		params = (OTHER)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		workTypes = [""] + [row[0] for row in data]
		self.comboBoxCategory.addItems(workTypes)
	def buildNameList(self):
		category = self.comboBoxCategory.currentText()
		self.comboBoxName.clear()
		cmd = """
			SELECT FULL_NAME
			FROM [{0}].[dbo].[LKP_MINE_NAMES]
			WHERE MINE_NAME_CATEGORY = (
				SELECT MINE_NAME_CATEGORY FROM [{0}].[dbo].[LKP_MINE_NAMES_CATEGORY]
				WHERE CATEGORY_DESCRIPTION = ?
			)
			ORDER BY FULL_NAME""".format(self.main_app.srvcnxn.dbsvy)
		params = (category)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		names = [""] + [row[0] for row in data]
		self.comboBoxName.addItems(names)

	def buildSubUnitList(self):
		self.comboBoxSubUnitFrom.blockSignals(True)
		self.comboBoxSubUnitTo.blockSignals(True)
		geoUnitNames = self.lineEditGeoUnit.text().split(", ")
		geoUnitNamesForQuery = ",".join(["'"+item+"'" for item in geoUnitNames])
		placeholders = ",".join("?" * len(geoUnitNames))

		self.comboBoxSubUnitFrom.clear()
		self.comboBoxSubUnitTo.clear()

		cmd = """
			SELECT NAME
			FROM [{0}].[dbo].[SUBUNIT]
			WHERE UNIT in (
				SELECT PANEL_ID FROM [{0}].[dbo].[PANEL]
				WHERE PANEL_NAME in ({1})
			)
			ORDER BY NAME""".format(self.main_app.srvcnxn.dbsvy, placeholders)

		params = (geoUnitNames)
		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		names = [""] + [row[0] for row in data]
		self.comboBoxSubUnitFrom.addItems(names)
		self.comboBoxSubUnitTo.addItems(names)
		self.comboBoxSubUnitFrom.blockSignals(False)
		self.comboBoxSubUnitTo.blockSignals(False)

	def subUnitChanged(self,  sender, opponent):
		for index in range(opponent.count()):
			opponent.model().item(index).setEnabled(True)
		currentIndex = sender.currentIndex()
		if currentIndex == 0:
			return
		if opponent.currentIndex() == currentIndex:
			opponent.setCurrentIndex(0)
		opponent.model().item(currentIndex).setEnabled(False)

	#															   
	def layerChanged(self, index):
		self.lineEditGeoUnit.setText("")
		self.line_edit_local_project.setText("")
		self.minesitechoosen.setText("не выбран")
		self.DirChoosen.setText("не выбрана")

	def local_project_changed(self):
		self._updateWindowTitle()
		self.lineEditGeoUnit.setText("")
		self.buildOutTypeList()

	def localProjectChanged(self, index):
		self.lineEditGeoUnit.setText("")
		self.buildOutTypeList()
	def techGridChanged(self, index):
		self.lineEditLongwall.setText("")	 
	def techGridFromChanged(self, index):
		self.comboBoxTechGridTo.setCurrentIndex(self.comboBoxTechGridFrom.currentIndex())
		self.lineEditProfileFrom.setText("")
	def techGridToChanged(self, index):
		self.lineEditProfileTo.setText("")
	def workTypeChanged(self, index):
		self.buildNameList()
	def geoUnitChanged(self, index):
		self.buildSubUnitList()
	def  profileFromChanged(self, index):
		if self.lineEditProfileFrom.text() == '':
			self.lineEditDistanceFrom.setText('')
			self.lineEditDistanceFrom.setEnabled(False)
		else:
			self.lineEditDistanceFrom.setEnabled(True)

	def  profileToChanged(self, index):
		if self.lineEditProfileTo.text() == '':
			self.lineEditDistanceTo.setText('')
			self.lineEditDistanceTo.setEnabled(False)
		else:
			self.lineEditDistanceTo.setEnabled(True)

									   
	def _updateWindowTitle(self):
		title = self._default_title + " " + self.generate_mine_name()
		self.setWindowTitle(title)

	#
	def selectGeoUnit(self):
		layerName = self.comboBoxLayer.currentText()
		locProjectName = self.line_edit_local_project.text()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		#
		if not layerName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите залежь")
			return
		if not locProjectName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите локальный проект")
			return

		try:
			layerId = self.main_app.srvcnxn.getLayerId(layerName, locationName)
			geoUnitNames = tuple(self.main_app.srvcnxn.getGeoUnitNamesByLocalProject(locProjectName, layerId = layerId))
			if not geoUnitNames:
				return
		except Exception as e:
			return

		dialog = SelectFromListDialog(self)
		dialog.setWindowTitle("Выберите "+ self.main_app.settings.getValue(ID_GEO_UDSPNAME).lower())
		dialog.labelTitle.setText("Выберите геологические юниты")
		dialog.listWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		dialog.setValues(geoUnitNames)
		dialog.exec()

		result = dialog.getValues()
		if result:
			self.lineEditGeoUnit.setText(", ".join(result))
	def selectLongwall(self, lineEditLongwall, comboBoxTechGrid, opponent):
		techGridName = comboBoxTechGrid.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		#
		if not techGridName:
			qtmsgbox.show_error(self, "Ошибка", "Выберите технологическую сеть")
			return

		try:					techGridId = self.main_app.srvcnxn.getTechGridId(techGridName, locationName)
		except Exception as e:	return

		#
		cmd = """
			SELECT LONGWALL_NAME 
			FROM [{0}].[dbo].[LONGWALL] 
			WHERE TECH_GRID_ID = ?
			ORDER BY LONGWALL_ID, LONGWALL_NAME
		""".format(self.main_app.srvcnxn.dbsvy)
		params = (techGridId)

		data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		longWallNames = [row[0] for row in data]

		dialog = SelectFromListDialog(self)
		dialog.setWindowTitle("Профили")
		dialog.labelTitle.setText("Выберите профиль")
		dialog.setValues(longWallNames)
		dialog.exec()

		result = dialog.getValues()
		if result:
			newVal = ", ".join(result)
			lineEditLongwall.setText(newVal)

	def generate_mine_name(self):

		number = self.lineEditNumberValue.text()
		profileFrom = self.lineEditProfileFrom.text()
		profileTo = self.lineEditProfileTo.text()
		distanceFrom = self.lineEditDistanceFrom.text()
		distanceTo = self.lineEditDistanceTo.text()
		subUnitFrom = self.comboBoxSubUnitFrom.currentText()
		subUnitTo = self.comboBoxSubUnitTo.currentText()


		name = self.comboBoxName.currentText()
		cmd = "SELECT MINE_NAME FROM [{0}].[dbo].[LKP_MINE_NAMES] WHERE FULL_NAME = ?".format(self.main_app.srvcnxn.dbsvy)
		params = (name)
		name = self.main_app.srvcnxn.exec_query(cmd, params).get_row(0)
		suffix = self.lineEditSuffix.text()

		number = number.strip()
		profileFrom = profileFrom.strip()
		profileTo = profileTo.strip()	
		distanceFrom = distanceFrom.strip()
		distanceTo = distanceTo.strip()	
		subUnitFrom = subUnitFrom.strip()
		subUnitTo = subUnitTo.strip()	

		if name is None:
			return ""

		mineName = name
		if not self.checkBoxExclude.isChecked():
			mineName += "-%s" % number if number else ""
			mineName += "_%s" % profileFrom if profileFrom else ""
			mineName += "%s" % distanceFrom if distanceFrom else ""
			if profileTo != profileFrom:
				mineName += "_%s" % profileTo if profileTo else ""
			mineName += "%s" % distanceTo if distanceTo else ""
			mineName += "_%s" % subUnitFrom if subUnitFrom else ""
			mineName += "_%s" % subUnitTo if subUnitTo else ""


		mineName += "-%s" % suffix if suffix else ""

		mineName = mineName[1:] if mineName.startswith("-") else mineName
		mineName = mineName[:-1] if mineName.endswith("-") else mineName

		return mineName.upper()

	#
	def prepare(self):
		if not super(DsnDefineModelVer2, self).prepare():
			return

		labels = (self.labelLayer, self.label_local_project, self.labelGeoUnit, self.labelName, self.labelDate)
		widgets = (self.comboBoxLayer, self.line_edit_local_project, self.lineEditGeoUnit, self.comboBoxName, self.dateEditProjectDate)
		for i, (label, widget) in enumerate(zip(labels, widgets)):
			text = getWidgetValue(widget)
			if not text:
				qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % label.text())
				return


		#
		locProjectName = self.line_edit_local_project.text()
		mineName = self.generate_mine_name()


		#	Не требуется проверки на существование выработки так как далее проходим все этапы и прописываем все связи, которые не прописаны
		#	О всех связях, которые уже были прописаны уведомляем пользователя. 
		#cmd = "SELECT (SELECT LOC_PROJECT_NAME FROM [{0}].[dbo].[LOCAL_PROJECT] L WHERE L.LOC_PROJECT_ID = M.LOC_PROJECT_ID) + '||~||' + Vyr FROM [{0}].[dbo].[VYRAB_P] M"
		#cmd = cmd.format(self.main_app.srvcnxn.dbsvy)
		#data = [row[0] for row in self.main_app.srvcnxn.exec_query(cmd).get_rows()]
		#if "%s||~||%s" % (locProjectName, mineName) in data:
		#	qtmsgbox.show_error(self, "Ошибка", "Выработка уже существует")
		#	return

		if self.directoryView.selectedItems():
			if self.directoryView.selectedItems()[0].filePath().lower().endswith('.tridb'):
				qtmsgbox.show_error(self, "Ошибка", "Необходимо выбрать папку, а не выработку")
				return
		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Не удалось создать выработку. %s" % str(e))

		self.after_run()

	def run(self):
		if not super(DsnDefineModelVer2, self).run():
			return
		
		layerName = self.comboBoxLayer.currentText()
		mineSiteName = self.minesitechoosen.text()
		locProjectName = self.line_edit_local_project.text()
		geoUnitNames = self.lineEditGeoUnit.text().split(", ")
		techGridNameFrom = self.comboBoxTechGridFrom.currentText()
		techGridNameTo = self.comboBoxTechGridTo.currentText()
		levelName = "Номер"
		levelValue = self.lineEditNumberValue.text()
		purpose = self.comboBoxCategory.currentText()
		locationName = self.main_app.settings.getValue(ID_PROJNAME)
		profileFrom = self.lineEditProfileFrom.text()
		profileTo = self.lineEditProfileTo.text()



		name = self.comboBoxName.currentText()
		suffix = self.lineEditSuffix.text()
		projectDate = datetime.strptime(self.dateEditProjectDate.text(), "%Y-%m-%d")

		category = self.main_app.srvcnxn.getWorkCategory(purpose)

		mineName = self.generate_mine_name()

		techGridNames = [i for i in [techGridNameFrom, techGridNameTo] if i !=""]
		longWallNames = [i for i in [profileFrom, profileTo] if i !=""]

		try:
			msg = saveMineToDatabaseVer2(self.main_app.srvcnxn, locationName, layerName, mineName, locProjectName, purpose, geoUnitNames, projectDate, techGridNames, longWallNames, levelName, levelValue, mineOwner = ID_MINE_OF_DSN)
		except Exception as e:
			raise Exception(str(e))

		# создание файла выработки
		netpath = self.main_app.settings.getValue(ID_NETPATH)
		dsndir = netpath + "\\" + self.main_app.settings.getValue(ID_DSN_MAINDIR)
		layerdir = dsndir + "\\" + layerName
		mineSiteDir = layerdir + "\\" + mineSiteName
		localProjectDir = mineSiteDir + "\\" + locProjectName
		tridir = localProjectDir + "\\" + self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1]
		strdir = localProjectDir + "\\" + self.main_app.settings.getValue(ID_DSN_USUBDIRS)[2][1]

		#tripath = tridir + "\\%s.tridb" % mineName
		######## start edit
		if not self.directoryView.selectedItems():
			tripath = tridir + "\\%s.tridb" % mineName
		#### start edit
		else:
			tripath = self.directoryView.selectedItems()[0].filePath() + "\\%s.tridb" % mineName
		######## end edit
		if os.path.exists(tripath):
			msg.append("Файл выработки уже существовал.\n")
		else:
			try:					Tridb.create(tripath, DSN_USER_ATTRIBUTES)
			except Exception as e:	raise Exception("Не удалось создать файл выработки. %s" % str(e))

		if category in [self.main_app.settings.getValue(ID_SVYCAT)[0][0][1], self.main_app.settings.getValue(ID_SVYCAT)[2][0][1]]:
			strpath = os.path.join(strdir, "%s.STR" % mineName)
			structure = ["EAST:R:0:6", "NORTH:R:0:6", "RL:R:0:6", "STRING:C:20:0", "JOIN:R:0:0"]
			if os.path.exists(strpath):
				msg.append("Файл стрингов уже существовал.\n")
			else:
				try:					MicromineFile.create(strpath, structure)
				except Exception as e:	raise Exception("Не удалось создать файл стрингов. %s" % str(e))

		# сохраняем
		self.main_app.srvcnxn.commit()
		#
		self.buildOutTypeList()
		qtmsgbox.show_information(self, "Выполнено", "Действие выполнено успешно.\n {0}".format("".join(msg)))

