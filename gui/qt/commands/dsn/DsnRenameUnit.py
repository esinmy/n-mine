from PyQt4 import QtGui, QtCore
import os


from utils.constants import *
from utils.qtutil import getWidgetValue
from utils.blockmodel import BlockModel
from utils.validation import is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow


class DsnRenameUnit(DialogWindow):
	def __init__(self, parent, dsnUnitItem, use_connection, *args, **kw):
		super(DsnRenameUnit, self).__init__(parent, use_connection, *args, **kw)
		self._dsnUnitItem = dsnUnitItem
		self._use_connection = use_connection
		self.setWindowTitle("Переименовать локальный проект")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._newName = None
	def __createWidgets(self):
		self.layoutParams = QtGui.QFormLayout()

		self.labelOldName = QtGui.QLabel(self)
		self.labelOldName.setText("Старое имя")
		self.labelOldName.setMinimumWidth(100)
		self.labelOldName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOldName = QtGui.QLineEdit(self)
		self.lineEditOldName.setMinimumWidth(200)
		self.lineEditOldName.setReadOnly(True)
		self.lineEditOldName.setText(self._dsnUnitItem.name())
		self.labelNewName = QtGui.QLabel(self)
		self.labelNewName.setText("Новое имя")
		self.labelNewName.setMinimumWidth(100)
		self.labelNewName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditNewName = QtGui.QLineEdit(self)
		self.lineEditNewName.setMinimumWidth(200)

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOldName)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOldName)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelNewName)
		self.layoutParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditNewName)


	def check(self):
		if is_blank(self.lineEditNewName.text()):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelNewName.text())
			return

		oldpath = self._dsnUnitItem.filePath()
		dirpath = os.path.dirname(oldpath)
		newpath = os.path.join(dirpath, self._newName)

		if os.path.exists(newpath):
			qtmsgbox.show_error(self, "Ошибка", "Локальный проект '%s' уже существует" % os.path.basename(newpath))
			return

		return True

	def prepare(self):
		super(DsnRenameUnit, self).prepare()

		self._newName = self.lineEditNewName.text()

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self, "Ошибка", "Невозможно переименовать юнит. %s" % str(e))

		self.after_run()
		if self._is_success:
			self.close()

	def run(self):

		if self._use_connection and self.main_app.srvcnxn is None:
			self.main_app.connect_to_database()

		def restore_filenames():
			nonlocal old_path, new_path
			if not os.path.exists(old_path):
				try:		os.rename(new_path, old_path)
				except:		pass
		#
		old_path = self._dsnUnitItem.filePath()
		#
		dirpath = os.path.dirname(old_path)
		new_path = os.path.join(dirpath, self._newName )
	
		departmentName = os.path.split(dirpath)[1]

		if not self.__existLocProjInDb(self._dsnUnitItem.name(), departmentName):
			qtmsgbox.show_information(self, "Внимание", "Текущий проект отсутствует в БД. Будет переименован только каталог проекта.")
		if  self.__existLocProjInDb(self._newName, departmentName):
			raise Exception("Невозможно изменить имя. Проект с именем '%s' уже есть в БД. " % (self._newName))

		#
		try:

			cmd ="""
			update  [{0}].[dbo].[LOCAL_PROJECT]  
				set [LOC_PROJECT_NAME] = ?
				where  LOC_PROJECT_NAME = ?
			"""
			params = (self._newName, self._dsnUnitItem.name())
			cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

			try:
				self.main_app.srvcnxn.exec_query(cmd, params)
			except Exception as e:
				raise Exception("Невозможно изменить имя локального проекта в БД. %s" % str(e))
			# переименовываем файлы
			try:
				os.rename(old_path, new_path)
			except Exception as e:
				raise Exception("Невозможно переименовать папку локального проекта '%s'" % str(e))
			
			self.main_app.srvcnxn.commit()
			
		except Exception as e:
			# возвращаем имена, если требуется
			restore_filenames()
			raise Exception(str(e))

		# переименовываем в интерфейсе
		try:
			self._dsnUnitItem.setName(self._newName)
			self._dsnUnitItem.setFilePath (new_path)
		except Exception as e:
			raise Exception("Невозможно обновить дерево папок. Перезапустите скрипт. Ошибка: %s" % str(e))

	
		qtmsgbox.show_information(self, "Выполнено", "Юнит успешно переименован")

	def __existLocProjInDb(self, locProjectName, departmentName):
		cmd ="""
		select * from [{0}].[dbo].[LOCAL_PROJECT]  as LP
			where LP.LOC_PROJECT_NAME = ?
		"""
		params = (locProjectName)
		cmd = cmd.format(self.main_app.srvcnxn.dbsvy)

		try:
			data = self.main_app.srvcnxn.exec_query(cmd, params).get_rows()
		except Exception as e:
			self.main_app.srvcnxn.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))
		if data:
			return True
		else:
			return False
		
