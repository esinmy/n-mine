from PyQt4 import QtCore, QtGui	
from collections import OrderedDict
from mm.mmutils import *
from utils.constants import *
from utils.osutil import *
from utils.qtutil import *
from utils.fsettings import FormSet	
from gui.qt.SettingsWindow import SettingsWindow
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


def lDateTime_converter (d):
	return datetime.fromtimestamp(int(d)).strftime("%d/%m/%Y")

sqlite3.register_converter("lDateTime", lDateTime_converter)

# Менеджер еаборов настроек
class SettingsManager(BaseDialog):

	def __init__(self, parent,  *args, **kwargs):
		super(SettingsManager,self).__init__(parent)
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setWindowTitle("Менеджер настроек")
		self.setModal(True)
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
		self.__loadStoredSettings()
		self.setFixedSize(self.sizeHint())

	def __createVariables(self):

		self.headerStoredSettings = OrderedDict ([
												( "title",{"header":"Наименование",
														 "modeSize":QtGui.QHeaderView.ResizeToContents,
														 "hidden":False}),
												( "szCreateAuthor", {"header":"Автор",
																	 "modeSize":QtGui.QHeaderView.ResizeToContents,
																	 "hidden":False}),
												( "lCreateDateTime" , {"header":"Дата создания",
																	 "modeSize":QtGui.QHeaderView.ResizeToContents,
																	 "hidden":False}),
												("szGeneralNotes",{"header":"Комментарий",
																	 "modeSize":QtGui.QHeaderView.Stretch,
																	 "hidden":False}) ,
												("cmdId",{"header":"",
																	"modeSize":QtGui.QHeaderView.ResizeToContents,
																	"hidden":True}),
												( "setId",{"header":"",
																	 "modeSize":QtGui.QHeaderView.ResizeToContents,
																	 "hidden":True}),
																	 ])
		self._needsUpdate = False

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(5)

		# Текущие настройки	

		self.groupBoxCurrentSettings = QtGui.QGroupBox(self)
		self.groupBoxCurrentSettings.setTitle("Текущие настройки")
		self.groupBoxCurrentSettings.setMaximumHeight(150)

		self.layoutCurrent = QtGui.QHBoxLayout()
		self.layoutCurrent.setSpacing(5)

		self.layoutCurrentState = QtGui.QFormLayout()
		self.layoutCurrentState.setLabelAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.layoutCurrentState.setSpacing(5)

		self.labelCurrentСaption = QtGui.QLabel()
		self.labelCurrentСaption.setText("Состояние: ")
		self.labelCurrentСaption.setMinimumWidth(100)
		self.labelCurrentСaption.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.labelCurrentState = QtGui.QLabel()
		self.__formatLabelBySettingsState(self.labelCurrentState)
		self.labelCurrentState.setMinimumWidth(300)
		self.labelCurrentState.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

		self.layoutCurrentButton = QtGui.QVBoxLayout()
		self.layoutCurrentButton.setSpacing(5)
		self.buttonBoxCurrentSettings = QtGui.QDialogButtonBox(self)
		self.buttonBoxCurrentSettings.setOrientation(QtCore.Qt.Vertical)
		self.buttonBoxCurrentSettings.addButton(QtGui.QDialogButtonBox.Open)
		self.buttonBoxCurrentSettings.addButton(QtGui.QDialogButtonBox.Save)
		self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Open).setMinimumWidth(150)
		self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Open).setText("Редактировать")
		self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Save).setMinimumWidth(150)
		self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Save).setText("Сохранить как")

		
		# Сохраненные настройки

		self.tvStoredSettings = QtGui.QTreeView(self)
		self.tvStoredSettings.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
		self.tvStoredSettings.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
		self.tvStoredSettings.setMinimumWidth(800)
		self.tvStoredSettings.setMinimumHeight(300)
		self.tvStoredSettings.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.tvStoredSettings.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
		self.tvStoredSettings.setAlternatingRowColors(True)

		self.modelStoredSettings = QtGui.QStandardItemModel(self)
		self.tvStoredSettings.setModel(self.modelStoredSettings)

		self.labelStoredSettings = QtGui.QLabel("Сохраненные настройки", self)
		#self.groupBoxStoredSettings = QtGui.QGroupBox(self)
		#self.groupBoxStoredSettings.setTitle("Сохраненные настройки")
		#self.groupBoxStoredSettings.setMaximumHeight(250)

		self.layoutStored = QtGui.QHBoxLayout()
		self.layoutStored.setSpacing(5)

		self.layoutStoredTable = QtGui.QVBoxLayout()
		self.layoutStoredTable.setSpacing(5)

		self.layoutStoredButton = QtGui.QVBoxLayout()
		self.layoutStoredButton.setSpacing(5)
		self.layoutStoredButton.setContentsMargins(0,20,0,0)

		self.buttonBoxStoredSettings = QtGui.QDialogButtonBox(self)
		self.buttonBoxStoredSettings.setOrientation(QtCore.Qt.Vertical)
		
		self.buttonApply = QtGui.QPushButton("Сделать текущими",self)
		self.buttonDelete = QtGui.QPushButton("Удалить",self)


		self.buttonBoxStoredSettings.addButton(self.buttonApply,QtGui.QDialogButtonBox.NoRole)
		self.buttonBoxStoredSettings.addButton(self.buttonDelete,QtGui.QDialogButtonBox.NoRole)

		self.buttonApply.setMinimumWidth(150)
		self.buttonDelete.setMinimumWidth(150)

		self.buttonApply.setDisabled(True)
		self.buttonDelete.setDisabled(True)

		#Общие кнопки диалогового окна
		self.buttonBoxGeneral = QtGui.QDialogButtonBox(self)
		buttonsGeneral = QtGui.QDialogButtonBox.Close 
		self.buttonBoxGeneral.setStandardButtons(buttonsGeneral)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Close).setMinimumWidth(150)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Close).setText("Применить")
		

	def __createBindings(self):
		
		self.tvStoredSettings.selectionModel().selectionChanged.connect(self.on_selectionChanged_tvStoredSettings)
		self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Open).clicked.connect(self.on_clicked_buttonBoxCurrentSettings_open_openSettings)
		self.buttonBoxCurrentSettings.button(QtGui.QDialogButtonBox.Save).clicked.connect(self.on_clicked_buttonBoxCurrentSettings_save_saveSettingsAs)
		self.buttonApply.clicked.connect(self.on_clicked_buttonBoxStoredSettings_buttonApply)
		self.buttonDelete.clicked.connect(self.on_clicked_buttonBoxStoredSettings_buttonDelete)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Close).clicked.connect(self.close)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addLayout (self.layoutCurrent)
		self.layoutCurrent.addWidget (self.groupBoxCurrentSettings)
		self.groupBoxCurrentSettings.setLayout(self.layoutCurrentState)
		self.layoutCurrentState.setWidget(0,QtGui.QFormLayout.LabelRole,self.labelCurrentСaption)
		self.layoutCurrentState.setWidget(0,QtGui.QFormLayout.FieldRole,self.labelCurrentState)
		self.layoutCurrent.addLayout(self.layoutCurrentButton )
		self.layoutCurrentButton.addWidget(self.buttonBoxCurrentSettings)


		self.layoutMain.addLayout(self.layoutStored)
		#self.layoutStored.addWidget(self.groupBoxStoredSettings)

		#self.groupBoxStoredSettings.setLayout (self.layoutStoredTable)
		self.layoutStored.addLayout(self.layoutStoredTable)
		self.layoutStoredTable.addWidget(self.labelStoredSettings)
		self.layoutStoredTable.addWidget(self.tvStoredSettings)

		self.layoutStored.addLayout( self.layoutStoredButton)
		self.layoutStoredButton.addWidget(self.buttonBoxStoredSettings)

		self.layoutMain.addWidget(self.buttonBoxGeneral)
	
	# Обработка событий
	def on_selectionChanged_tvStoredSettings(self,sele,desele):
		rows = self.tvStoredSettings.selectionModel().selectedRows()	
		if rows:
			self.buttonApply.setDisabled(False)
			self.buttonDelete.setDisabled(False)

	def on_clicked_buttonBoxCurrentSettings_open_openSettings(self):
		settingsWindow = SettingsWindow(self)
		toScreenCenter(settingsWindow)
		settingsWindow.exec_()

		self._needsUpdate = self._needsUpdate | settingsWindow.needsUpdate

		if self._needsUpdate:
			try:
				self.main_app.settings.MarkCurrentSettingsOriginal()
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно поменять статус текущих настроек. %s" % str(e))
			self.__formatLabelBySettingsState(self.labelCurrentState)

	def on_clicked_buttonBoxCurrentSettings_save_saveSettingsAs(self):
		
			settingsManager_Save = SettingsManager_Save(self, self.main_app.settings)
			toScreenCenter(settingsManager_Save)
			if not settingsManager_Save.exec_():
				return

			try:
				self.main_app.settings.CopySettings (0,settingsManager_Save.GetName(), settingsManager_Save.GetComment(), settingsManager_Save.GetFolder())
				self.__loadStoredSettings()
		
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно скопировать настройки. %s" % str(e))

	def on_clicked_buttonBoxStoredSettings_buttonApply(self):

		if not qtmsgbox.show_question(self,"Восстановление набора настроек","Вы действительно хотите восстановить настройки?"):
			return

		index = self.tvStoredSettings.selectedIndexes()[0]
		userData = index.model().itemFromIndex(index).data()

		if  userData.get("path") :
			qtmsgbox.show_information(self, "Внимание", "Нельзя использовать папку как настройку")
			return
		else:
			try:
				_setId = int(userData["setId"])
				self.main_app.settings.CopySettings (_setId)

			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно скопировать настройки. %s" % str(e))

			self.readSettings()
			self.__loadStoredSettings()									
			self.__formatLabelBySettingsState(self.labelCurrentState)
			self._needsUpdate = True

	def on_clicked_buttonBoxStoredSettings_buttonDelete	(self):

		if not qtmsgbox.show_question(self,"Удаление набора настроек","Вы действительно хотите удалить?"):
			return

		index = self.tvStoredSettings.selectedIndexes()[0]
		userData = index.model().itemFromIndex(index).data()

		if  userData.get("path") :
			folderId = self.main_app.settings.getFolderId(userData["path"])
			try:
				self.main_app.settings.deleteFolderId(folderId)
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно удалить папку. %s" % str(e))
		else:

			_setId = int(userData["setId"])
			try:
				self.main_app.settings.DeleteStoredSettings(_setId)
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно удалить настройки. %s" % str(e))
		
		self.__loadStoredSettings()

	def readSettings(self):
		if self.main_app.settings:
			self.main_app.settings.close()

		try:							self.main_app._settings = FormSet(ID_SETTINGS)
		except Exception as e:			raise Exception("Невозможно считать настройки. %s" % str(e))
		self.main_app.settings.open(0)


	def	 __loadStoredSettings(self):

		# Рекурсивный обход дерева
		def getFoldersTree(parentId, parentName, path, hasLeafs =True):

			_parentFolderItem = QtGui.QStandardItem(parentName)
			_parentFolderItem.setData({"path":path})
			_parentFolderItem.setIcon(QtGui.QIcon("gui/img/icons/folder.png"))
		
			for folder in self.main_app.settings.GetFoldersByParent(parentId) :	 
				_childFolderItem = getFoldersTree (folder["folderId"],folder["name"],folder["path"], hasLeafs)
				_parentFolderItem.appendRow(_childFolderItem)

			if hasLeafs:
				for row in self.main_app.settings.GetSets(parentId):
					modelRow = []
					for field in self.headerStoredSettings:
				
						child_node = QtGui.QStandardItem(str(row[field]))
						child_node.setData({"setId":row["setId"]})
						modelRow.append(child_node)
					_parentFolderItem.appendRow(modelRow)

			return 	_parentFolderItem

		self.modelStoredSettings.clear()
		self.modelStoredSettings.setHorizontalHeaderLabels([item["header"] for item in self.headerStoredSettings.values()])
		for folder in self.main_app.settings.GetFoldersByParent(0) :	
			_childFolderRow = getFoldersTree (folder["folderId"], folder["name"], folder["path"])
			self.modelStoredSettings.appendRow(_childFolderRow)

		self.tvStoredSettings.setFirstColumnSpanned(1, self.tvStoredSettings.rootIndex(), True)

		for i in range(0,len(self.headerStoredSettings)):
			self.tvStoredSettings.resizeColumnToContents(i)

		header = self.tvStoredSettings.header()
		for i, key in enumerate(self.headerStoredSettings):
			header.setResizeMode(i, self.headerStoredSettings[key]["modeSize"])
			if self.headerStoredSettings[key]["hidden"]:  
				header.hideSection(i)	

	@property
	def needsUpdate(self):
		return self._needsUpdate

	@property
	def settings(self):		return self.main_app.settings

	def __formatLabelBySettingsState(self, label):
		_settingsSetTitle = self.main_app.settings.IsCurrentSettingsInStore()
		if _settingsSetTitle:
			label.setText("Восстановлено из " + _settingsSetTitle.replace("_@current-",""))
			label.setStyleSheet('color: green')
		else:
			label.setText("Оригинальные настройки")
			label.setStyleSheet('color: red')

# Диалог сохранения текущих настроек		
class SettingsManager_Save(BaseDialog):
	def __init__(self, parent, Settings, *args, **kwargs):
		super(SettingsManager_Save,self).__init__(parent, *args, **kwargs)
		
		self._settings = Settings

		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setWindowTitle("Сохранить настройки как...")
		self.setModal(True)
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
		self.__loadStoredSettings()
		self.setFixedSize(self.sizeHint())

	def __createVariables(self):

		# Переменная для хранения пути корневой папки при добавлении новой папки
		self.__folderPath = None

		self.headerStoredSettings = OrderedDict ([
												( "title",{"header":"Наименование папки",
														 "modeSize":QtGui.QHeaderView.ResizeToContents,
														 "hidden":False})
																	 ])


	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.layoutMain.setSpacing(5)

		# Параметры сохранения	

		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры набора настроек")
		self.groupBoxParams.setMaximumHeight(150)

		self.layoutParams = QtGui.QGridLayout()
		self.layoutParams.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.layoutParams.setSpacing(5)

		self.labelParamsName = QtGui.QLabel(self)
		self.labelParamsName.setText("Наименование: ")
		self.labelParamsName.setMinimumWidth(100)
		self.labelParamsName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.leParamsName = QtGui.QLineEdit(self)
		self.leParamsName.setMinimumWidth(300)
		self.leParamsName.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

		self.labelParamsComment = QtGui.QLabel(self)
		self.labelParamsComment.setText("Комментарий: ")
		self.labelParamsComment.setMinimumWidth(100)
		self.labelParamsComment.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.leParamsComment = QtGui.QLineEdit(self)
		self.leParamsComment.setMinimumWidth(300)
		self.leParamsComment.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

		self.labelParamsFolder = QtGui.QLabel(self)
		self.labelParamsFolder.setText("Папка: ")
		self.labelParamsFolder.setMinimumWidth(100)
		self.labelParamsFolder.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.leParamsFolder = QtGui.QLineEdit(self)
		self.leParamsFolder.setMinimumWidth(300)
		self.leParamsFolder.setReadOnly(True)
		self.leParamsFolder.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

		self.buttonFolder = QtGui.QPushButton("Создать", self)
		self.buttonFolder.setMinimumWidth(100)
		self.buttonFolder.setMinimumWidth(100)


		# Выбор каталога

		self.tvFolders = QtGui.QTreeView(self)
		self.tvFolders.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
		self.tvFolders.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
		self.tvFolders.setMinimumWidth(800)
		self.tvFolders.setMinimumHeight(300)
		self.tvFolders.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.tvFolders.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
		self.tvFolders.setAlternatingRowColors(True)

		self.modelFolders = QtGui.QStandardItemModel(self)
		self.tvFolders.setModel(self.modelFolders)

		#Общие кнопки диалогового окна
		self.buttonBoxGeneral = QtGui.QDialogButtonBox(self)
		self.buttonBoxGeneral.setOrientation(QtCore.Qt.Horizontal)
		buttonsGeneral = QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel 
		self.buttonBoxGeneral.setStandardButtons(buttonsGeneral)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Cancel).setText("Закрыть")
		

	def __createBindings(self):
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.myaccept)	
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.reject)
		self.tvFolders.selectionModel().selectionChanged.connect(self.on_selectionChanged_tvFolders)
		self.buttonFolder.clicked.connect(self.on_clicked_buttonFolder)
	
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxParams)

		self.groupBoxParams.setLayout(self.layoutParams)

		self.layoutParams.addWidget(self.labelParamsName,0,0)
		self.layoutParams.addWidget(self.leParamsName,0,1)

		self.layoutParams.addWidget(self.labelParamsComment,1,0)
		self.layoutParams.addWidget(self.leParamsComment,1,1)

		self.layoutParams.addWidget(self.labelParamsFolder,2,0)
		self.layoutParams.addWidget(self.leParamsFolder,2,1)
		self.layoutParams.addWidget(self.buttonFolder,2,2)

		
		self.layoutMain.addWidget(self.tvFolders)

		self.layoutMain.addWidget(self.buttonBoxGeneral)

	def GetName(self):
		return self.leParamsName.text()

	def GetComment(self):
		return self.leParamsComment.text()

	def GetFolder(self):
		return self.leParamsFolder.text()


	def on_selectionChanged_tvFolders(self,sele,desele):
		__index = self.tvFolders.selectedIndexes()[0]
		__folderName = __index.model().itemFromIndex(__index).text()
		__userData = __index.model().itemFromIndex(__index).data()
		self.__folderPath  = __userData["path"]
		self.leParamsFolder.setText(self.__folderPath)

	def on_clicked_buttonFolder(self):

		__settingsManager_GetFolderName = SettingsManager_GetFolderName(self)
		toScreenCenter(__settingsManager_GetFolderName)

		if not __settingsManager_GetFolderName.exec_():
			return

		try:
			if self.__folderPath == None or len(self.__folderPath.strip()) == 0 :
				self.leParamsFolder.setText(__settingsManager_GetFolderName.GetFolderName())
			else:
				self.leParamsFolder.setText(self.__folderPath + "|" + __settingsManager_GetFolderName.GetFolderName())

		
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно задать имя папки. %s" % str(e))

	def myaccept(self):
		if len(self.leParamsName.text().strip()) == 0 or len(self.leParamsFolder.text().strip()) == 0:
			return	False
		else:
			return self.accept()
		
	# Загрузка данных в QTreeView
	def	 __loadStoredSettings(self):

		# Рекурсивный обход дерева
		def getFoldersTree(parentId, parentName, path,  hasLeafs = False):

			_parentFolderItem = QtGui.QStandardItem(parentName)
			_parentFolderItem.setData({"path":path})
			_parentFolderItem.setIcon(QtGui.QIcon("gui/img/icons/folder.png"))
		
			for folder in self._settings.GetFoldersByParent(parentId) :	 
				_childFolderItem= getFoldersTree (folder["folderId"],folder["name"],folder["path"], hasLeafs)
				_parentFolderItem.appendRow(_childFolderItem)

			if hasLeafs:
				for row in self._settings.GetSets(parentId):
					modelRow = []
					for field in self.headerStoredSettings:
				
						child_node = QtGui.QStandardItem(str(row[field]))
						child_node.setData({"setId":row["setId"]})
						modelRow.append(child_node)
					_parentFolderItem.appendRow(modelRow)

			return 	_parentFolderItem

		self.modelFolders.clear()
		self.modelFolders.setHorizontalHeaderLabels([item["header"] for item in self.headerStoredSettings.values()])
		for folder in self._settings.GetFoldersByParent(0) :	
			_childFolderRow = getFoldersTree (folder["folderId"],folder["name"],folder["path"])
			self.modelFolders.appendRow(_childFolderRow)

		self.tvFolders.setFirstColumnSpanned(1, self.tvFolders.rootIndex(), True)

		for i in range(0,len(self.headerStoredSettings)):
			self.tvFolders.resizeColumnToContents(i)

		header = self.tvFolders.header()
		for i, key in enumerate(self.headerStoredSettings):
			header.setResizeMode(i, self.headerStoredSettings[key]["modeSize"])
			if self.headerStoredSettings[key]["hidden"]:  
				header.hideSection(i)	

# Диалог ввода имени папки
class SettingsManager_GetFolderName(BaseDialog):
	def __init__(self, parent, *args, **kwargs):
		super(SettingsManager_GetFolderName,self).__init__(parent, *args, **kwargs)

		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setWindowTitle("Задать имя папки")
		self.setModal(True)
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
		self.setFixedSize(self.sizeHint())

	def __createVariables(self):
		pass

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.layoutMain.setSpacing(5)

		# Параметры сохранения	

		self.groupBoxFolderName = QtGui.QGroupBox(self)
		self.groupBoxFolderName.setTitle("Папка")
		self.groupBoxFolderName.setMaximumHeight(150)

		self.layoutFolderName = QtGui.QGridLayout()
		self.layoutFolderName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.layoutFolderName.setSpacing(5)

		self.labelFolderName = QtGui.QLabel(self)
		self.labelFolderName.setText("Наименование: ")
		self.labelFolderName.setMinimumWidth(100)
		self.labelFolderName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.leFolderName = QtGui.QLineEdit(self)
		self.leFolderName.setMinimumWidth(300)
		self.leFolderName.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

		#Общие кнопки диалогового окна
		self.buttonBoxGeneral = QtGui.QDialogButtonBox(self)
		self.buttonBoxGeneral.setOrientation(QtCore.Qt.Horizontal)
		buttonsGeneral = QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel 
		self.buttonBoxGeneral.setStandardButtons(buttonsGeneral)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Cancel).setText("Закрыть")
		

	def __createBindings(self):
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.accept)	
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.reject)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxFolderName)

		self.groupBoxFolderName.setLayout(self.layoutFolderName)

		self.layoutFolderName.addWidget(self.labelFolderName,0,0)
		self.layoutFolderName.addWidget(self.leFolderName,0,1)

		self.layoutMain.addWidget(self.buttonBoxGeneral)

	def GetFolderName(self):
		return self.leFolderName.text()



	

