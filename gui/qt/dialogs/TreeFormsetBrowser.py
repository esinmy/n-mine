from PyQt4 import QtGui, QtCore
import os, sqlite3
from datetime import datetime

from utils.constants import *
from mm.mmutils import get_fldval_path
from utils.fsettings import FormSet

from gui.qt.dialogs.messagebox import *
from gui.qt.dialogs.BaseDialog import BaseDialog

__all__ = ["TreeMmFormBrowser", "TreeFormMmBrowserDialog"]

class TreeDirectoryItem(QtGui.QTreeWidgetItem):
	def __init__(self, parent):
		super(TreeDirectoryItem, self).__init__(parent)

		self._isDirectory = True
	def isDiretory(self):		return self._isDirectory
	def isFormset(self):		return not self._isDirectory
	def name(self):				return self.text(0)

class TreeFormsetItem(TreeDirectoryItem):
	def __init__(self, parent, data):
		super(TreeFormsetItem, self).__init__(parent, data)
		self._data = data
	def formsetId(self):		return self._data[0]

class TreeMmFormBrowser(QtGui.QTreeWidget):
	def __init__(self, *args, **kw):
		super(TreeMmFormBrowser, self).__init__(*args, **kw)
		self.__create_variables()

	def __create_variables(self):
		self._items = {"": None}

	def setColumns(self, columns):
		for i, colname in enumerate(columns):
			self.headerItem().setText(i, colname)
	def setValuesFromList(self, values, splitter = "\\", icon_path = "", icon_dict = {}, hide_extension = True):
		# icon_dict is a dictionary of (extension: icon_path)
		# icon_path is used for all items that are not in icon_dict
		for k, row in enumerate(values):
			path, data = row[0].split(splitter), row[1:]
			n = len(path)
			for i, parent in enumerate(path):
				key = splitter.join(path[:i+1])

				if not key in self._items:
					if i == 0:	self._items[key] = QtGui.QTreeWidgetItem()
					else:		self._items[key] = QtGui.QTreeWidgetItem(self._items[key[:key.rfind(splitter)]])

					# set text
					if hide_extension:		value = parent[:parent.rfind(".")] if "." in parent else ""
					else:					value = parent
					self._items[key].setText(0, parent)
					# set icon if necessary
					extension = parent[parent.rfind("."):] if "." in parent else ""
					if extension in icon_dict:
						self._items[key].setIcon(0, QtGui.QIcon(icon_dict[extension]))
					elif icon_path:
						self._items[key].setIcon(0, QtGui.QIcon(icon_path))
					# set attributes
					if i == n-1:
						for j, val in enumerate(data, start = 1):
							self._items[key].setText(j, str(val))
					self.addTopLevelItem(self._items[key])

	def connect(self, dbpath):
		if not os.path.exists(dbpath):
			raise FileNotFoundError("Файл '%s' не существует" % dbpath)
		cnxn = sqlite3.connect(dbpath)
		self._cursor = cnxn.cursor()

	def disconnect(self):
		self._cursor.connection.close()

	def load(self, cmdId = None):
		if cmdId is None:
			# работает неправильно
			dirCmd = """
			select
				CASE WHEN F.path = ''
					THEN F.cmdId
					ELSE F.cmdId || '|' || F.path
				END Заголовок,
				NULL ID,
				NULL [Изменен],
				NULL [Дата изменения],
				NULL [Создан],
				NULL [Дата создания]
			from FOLDERS F
			"""

			setCmd = """
			select
				CASE WHEN F.path = ''
					THEN F.cmdId || '|' || P.title
					ELSE F.cmdId || '|' || F.path || '|' || P.title
				END Заголовок,
				P.setId ID,
				P.szEditAuthor [Изменен],
				P.lEditDateTime [Дата изменения],
				P.szCreateAuthor [Создан],
				P.lCreateDateTime [Дата создания],
				P.cmdId [cmdId]
			from PRMSETS P join FOLDERS F on P.cmdId = F.cmdId and P.folderId = F.folderId
			where P.setId != 0
			order by P.setId
			"""
		else:
			dirCmd = """
			select
				F.path Заголовок,
				NULL ID,
				NULL [Изменен],
				NULL [Дата изменения],
				NULL [Создан],
				NULL [Дата создания]
			from FOLDERS F where cmdId = %d and F.path != ''
			""" % cmdId
			# setCmd = """
			# select
			# 	F.path || '|' || P.title Заголовок,
			# 	P.setId ID,
			# 	P.szEditAuthor [Изменен],
			# 	P.lEditDateTime [Дата изменения],
			# 	P.szCreateAuthor [Создан],
			# 	P.lCreateDateTime [Дата создания]
			# from PRMSETS P join FOLDERS F on P.cmdId = F.cmdId and P.folderId = F.folderId
			# where P.cmdId = %d and P.setId != 0
			# order by P.setId
			# """ % cmdId
			setCmd = """
			select
				CASE
					WHEN F.path = '' or F.path is NULL
					THEN P.title
					ELSE F.path || '|' || P.title
				END Заголовок,
				P.setId ID,
				P.szEditAuthor [Изменен],
				P.lEditDateTime [Дата изменения],
				P.szCreateAuthor [Создан],
				P.lCreateDateTime [Дата создания],
				P.cmdId [cmdId]
			from PRMSETS P left join FOLDERS F on P.cmdId = F.cmdId and P.folderId = F.folderId
			where P.cmdId = %d and P.setId != 0
			order by P.setId
			""" % cmdId
	
		#
		folders = []
		self._cursor.execute(dirCmd)
		for row in self._cursor:
			folder = str(row[0])
			if folder:
				folders.append([folder])
		folders.sort()

		#
		formsets = []
		self._cursor.execute(setCmd)
		for row in self._cursor:
			row = list(row)
			path = str(row[0])
			row[2] = row[2] if row[2] is not None else ""
			row[3] = datetime.fromtimestamp(row[3]) if row[3] is not None else ""
			row[5] = datetime.fromtimestamp(row[5]) if row[5] is not None else ""
			formsets.append(row)

		columns = [row[0] for row in self._cursor.description]
		self.setColumns(columns)
		self.setValuesFromList(folders, splitter = "|", icon_path = "gui/img/icons/folder.png")
		self.setValuesFromList(formsets, splitter = "|", icon_path = "gui/img/icons/formset.png")

		self.setColumnWidth(0, 250)
		self.setColumnWidth(1, 50)
		self.setColumnHidden(6, True) # cmdId field
		for i in range(2, len(columns)):
			self.setColumnWidth(i, 120)

class TreeMmFormsetBrowserDialog(BaseDialog):
	def __init__(self, *args, **kw):
		super(TreeMmFormsetBrowserDialog, self).__init__(*args, **kw)

		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setModal(True)

		self.setWindowTitle("Выберите форму")

		self._createVariables()
		self._createWidgets()
		self._createBindings()
		self._gridWidgets()

	def _createVariables(self):
		self._result = None
		self._result_cmdId = None
	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.treeFormset = TreeMmFormBrowser(self)
		self.treeFormset.itemDoubleClicked.connect(self._getSelected)
		self.dialogButtonBox = QtGui.QDialogButtonBox(self)
		self.dialogButtonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
		self.dialogButtonBox.setOrientation(QtCore.Qt.Horizontal)

		self.dialogButtonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumSize(QtCore.QSize(100, 0))
		self.dialogButtonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.dialogButtonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
		self.dialogButtonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumSize(QtCore.QSize(100, 0))
		self.dialogButtonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._getSelected)
	def _createBindings(self):
		pass
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.treeFormset)
		self.layoutMain.addWidget(self.dialogButtonBox)

	def readFormsets(self, cmdId = None):
		self.treeFormset.setMinimumSize(QtCore.QSize(800, 400))
		try:
			self.treeFormset.connect(get_fldval_path())
		except Exception as e:
			show_error(self, "Ошибка", "Невозможно подключиться к базе данных форм. %s" % str(e))
			return
		self.treeFormset.load(cmdId)
		self.treeFormset.disconnect()

		return True

	def _getSelected(self, e):
		selectedItems = self.treeFormset.selectedItems()
		if not selectedItems:
			show_error(self, "Ошибка", "Выберите форму")
			return

		self._result = selectedItems[0].text(1)
		self._result_cmdId = selectedItems[0].text(6)
		if self._result:
			self.close()

	@property
	def result(self):
		return self._result
	@property
	def result_cmdId(self):
		return self._result_cmdId

class SaveMmFormsetDialog(BaseDialog):
	def __init__(self, parent, commandId):
		super(SaveMmFormsetDialog, self).__init__(parent)
		self._parent = parent
		self._commandId = commandId

		self.setWindowTitle("Сохранить форму")
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setModal(True)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setMargin(5)

		self.layoutControls = QtGui.QVBoxLayout()
		
		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.setOrientation(QtCore.Qt.Vertical)
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._saveAndClose)
	def __gridWidgets(self):
		pass

	def _saveAndClose(self):
		if not self.check():
			return

	def check(self):
		return True


# app = QtGui.QApplication([])
# tree = TreeMmFormsetBrowserDialog()
# tree.readFormsets(479)
# tree.show()
# app.exec_()