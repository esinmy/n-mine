from PyQt4 import QtGui, QtCore

from gui.qt.widgets.TableDataView import CustomDataView, CustomDataModel
from gui.qt.dialogs.BaseDialog import BaseDialog

class SelectFromTableDialog(BaseDialog):
	def __init__(self, parent):
		super(SelectFromTableDialog, self).__init__(parent)
		self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowMaximizeButtonHint)

		self._createVariables()
		self._createWidgets()
		self._gridWidgets()
		self._createBindings()

	def _createVariables(self):
		self._result = None
	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.view = CustomDataView(self)
		self.view.doubleClicked.connect(self._getResult)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.view)
		self.layoutMain.addWidget(self.buttonBox)
	def _createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._getResult)
	def _getResult(self):
		indexes = self.view.selectedIndexes()
		model = self.view.model()
		if indexes:
			selectedRows = list(sorted(set([index.row() for index in indexes])))
			self._result = []
			for iRow in selectedRows:
				self._result.append([model.data(model.index(iRow, iCol)) for iCol in range(model.columnCount())])
		self.close()
	def getResult(self):
		# пока так, потом надо изменить во всех скриптах считывание результата
		return self._result