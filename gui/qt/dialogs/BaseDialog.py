from PyQt4 import QtGui, QtCore
from utils.qtutil import iterWidgetParents

class BaseDialog(QtGui.QDialog):
	helpCalled = QtCore.pyqtSignal()
	def __init__(self, parent):
		super().__init__(parent)
		self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowMaximizeButtonHint)
		self.__createBindings()
	def __createBindings(self):
		self.helpCalled.connect(self.main_app.showHelp)
	def keyPressEvent(self, event):
		super().keyPressEvent(event)
		if event.key() == QtCore.Qt.Key_F1:
			self.helpCalled.emit()
	@property
	def main_app(self):
		return iterWidgetParents(self)[-1]

