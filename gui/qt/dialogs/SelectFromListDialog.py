from PyQt4 import QtCore, QtGui
from gui.qt.dialogs.BaseDialog import BaseDialog

class SelectFromListDialog(BaseDialog):
	def __init__(self, *args, **kw):
		super(SelectFromListDialog, self).__init__(*args, **kw)

		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setModal(True)

		self.__create_variables()
		self.__create_widgets()
		self.__create_bindings()
		self.__grid_widgets()
	def __create_variables(self):
		self._result = []
	def __create_widgets(self):
		self.gridLayout = QtGui.QGridLayout()
		self.gridLayout.setMargin(5)
		self.gridLayout.setSpacing(5)

		self.labelTitle = QtGui.QLabel(self)
		self.labelTitle.setMargin(5)
		self.labelTitle.setWordWrap(True)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		# sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		# sizePolicy.setHorizontalStretch(0)
		# sizePolicy.setVerticalStretch(0)
		# sizePolicy.setHeightForWidth(self.buttonBox.sizePolicy().hasHeightForWidth())
		# self.buttonBox.setSizePolicy(sizePolicy)
		self.buttonBox.setMinimumSize(QtCore.QSize(90, 0))
		self.buttonBox.setOrientation(QtCore.Qt.Vertical)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._select)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)

		self.listWidget = QtGui.QListWidget(self)

	def __create_bindings(self):
		self.listWidget.mouseDoubleClickEvent = lambda e: self._select(e)
	def __grid_widgets(self):
		self.setLayout(self.gridLayout)
		self.gridLayout.addWidget(self.labelTitle, 0, 0, 1, 1)
		self.gridLayout.addWidget(self.buttonBox, 0, 1, 2, 1)
		self.gridLayout.addWidget(self.listWidget, 1, 0, 1, 1)

	def _select(self, event):
		selected = self.listWidget.selectedItems()
		if not selected:
			return
		self._result = [item.text() for item in selected]
		self.close()

	def getValues(self):
		return self._result
	def setValues(self, values):
		self.listWidget.clear()
		for val in values:
			self.listWidget.addItem(val)

# import os
# app = QtGui.QApplication(os.sys.argv)
# win = SelectFromListDialog(self)
# win.setFixedSize(win.sizeHint())
# win.show()
#
## Instantiate stderr handler class
# std_err_handler = StdErrHandler()
## Connect stderr to the handler
# os.sys.stderr = std_err_handler
#
# os.sys.exit(app.exec_())