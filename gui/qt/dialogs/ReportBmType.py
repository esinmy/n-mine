try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore

import os

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import CustomDataModel
from gui.qt.widgets.DateRangeWidget import DateRangeWidget
from gui.qt.dialogs.BaseDialog import BaseDialog

from utils.constants import *

class ReportBmParameters(BaseDialog):
	def __init__(self, parent):
		super(ReportBmParameters, self).__init__(parent)

		self._parent = parent

		self.setWindowTitle("Отчет по блочной модели")
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint | QtCore.Qt.MSWindowsFixedSizeDialogHint)
		self.resize(QtCore.QSize(300, 100))

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._cancelled = True
		self._depart_id = None
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxDepart = QtGui.QGroupBox(self)
		self.groupBoxDepart.setTitle("Участок")
		self.layoutDepart = QtGui.QFormLayout()
		self.labelDepart = QtGui.QLabel(self.groupBoxDepart)
		self.labelDepart.setText("Участок")
		self.labelDepart.setMinimumWidth(70)
		self.labelDepart.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditDepart = QtGui.QLineEdit(self.groupBoxDepart)
		self.lineEditDepart.setMinimumWidth(100)

		self.layoutReportStyle = QtGui.QHBoxLayout()
		self.groupBoxReportType = QtGui.QGroupBox(self)
		self.groupBoxReportType.setTitle("Тип отчета")

		self.layoutReportType = QtGui.QVBoxLayout()
		self.radioTypeButtonBox = QtGui.QButtonGroup()
		self.radioButtonMonthly = QtGui.QRadioButton(self.groupBoxReportType)
		self.radioButtonMonthly.setChecked(True)
		self.radioButtonMonthly.setText("Отчет по месяцу")
		self.radioButtonByMine = QtGui.QRadioButton(self.groupBoxReportType)
		self.radioButtonByMine.setText("Отчет по выработке")

		self.radioTypeButtonBox.addButton(self.radioButtonMonthly)
		self.radioTypeButtonBox.addButton(self.radioButtonByMine)

		self.groupBoxReportMode = QtGui.QGroupBox(self)
		self.groupBoxReportMode.setTitle("Режим отчета")
		self.layoutReportMode = QtGui.QVBoxLayout()

		self.radioModeButtonBox = QtGui.QButtonGroup()
		self.radioButtonAuto = QtGui.QRadioButton(self.groupBoxReportMode)
		self.radioButtonAuto.setText("Автоматический")
		self.radioButtonAuto.setChecked(True)
		self.radioButtonSemiAuto = QtGui.QRadioButton(self.groupBoxReportMode)
		self.radioButtonSemiAuto.setText("Полуавтоматический")

		self.radioModeButtonBox.addButton(self.radioButtonAuto)
		self.radioModeButtonBox.addButton(self.radioButtonSemiAuto)

		self.groupBoxReportParams = QtGui.QGroupBox(self)
		self.groupBoxReportParams.setTitle("Параметры")

		self.layoutParams = QtGui.QVBoxLayout()
		self.checkBoxSaveToDb = QtGui.QCheckBox(self.groupBoxReportParams)
		self.checkBoxSaveToDb.setText("Сохранить отчет в базу данных")

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QFormLayout()
		self.labelOutPath = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutPath.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutPath.setMinimumWidth(70)
		self.labelOutPath.setText("Файл отчета")
		self.lineEditOutPath = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutPath.setMinimumWidth(300)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._prepare)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self._cancel)
	def __createBindings(self):
		self.lineEditDepart.mouseDoubleClickEvent = lambda e: self._selectDepart(e)
		self.lineEditOutPath.mouseDoubleClickEvent = lambda e: self._browseSaveFile(e)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.setContentsMargins(5,5,5,5)

		self.layoutMain.addWidget(self.groupBoxDepart)

		self.groupBoxDepart.setLayout(self.layoutDepart)
		self.layoutDepart.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelDepart)
		self.layoutDepart.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditDepart)

		self.layoutMain.addLayout(self.layoutReportStyle)

		self.layoutReportStyle.addWidget(self.groupBoxReportType)
		self.groupBoxReportType.setLayout(self.layoutReportType)
		self.layoutReportType.addWidget(self.radioButtonMonthly)
		self.layoutReportType.addWidget(self.radioButtonByMine)
		self.layoutReportStyle.addWidget(self.groupBoxReportMode)
		self.groupBoxReportMode.setLayout(self.layoutReportMode)
		self.layoutReportMode.addWidget(self.radioButtonAuto)
		self.layoutReportMode.addWidget(self.radioButtonSemiAuto)

		self.layoutMain.addWidget(self.groupBoxReportParams)
		self.groupBoxReportParams.setLayout(self.layoutParams)
		self.layoutParams.addWidget(self.checkBoxSaveToDb)

		self.layoutMain.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutPath)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutPath)

		self.layoutMain.addWidget(self.buttonBox)

	def _browseSaveFile(self, e):
		filetypes = "EXCEL (*.XLSX)"
		try:			initialdir = MMpy.Project.path()
		except:			initialdir = os.getcwd()
		path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		if not path:
			return
		self.lineEditOutPath.setText(path)

	def _selectDepart(self, e):
		projname = self._parent.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT DepartId, DepartName, PitName
			FROM [{0}].[dbo].[DEPARTS] D LEFT JOIN [{0}].[dbo].[PITS] P ON D.DepartPitID = P.PitID
			WHERE P.LOCATION = N'{1}'""".format(self._parent.main_app.srvcnxn.dbsvy, projname)

		data = self._parent.main_app.srvcnxn.exec_query(cmd).get_rows()

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите участок")
		model = CustomDataModel(dialog.view, data, ["ID", "УЧАСТОК", "ШАХТА"])
		dialog.view.setModel(model)
		dialog.view.hideColumn(0)
		dialog.view.setColumnWidth(1, 250)
		dialog.resize(QtCore.QSize(500, 500))
		dialog.exec()
		#
		depid = dialog.getResult()
		if not depid:
			return

		self.lineEditDepart.setText(depid[0][1])
		self._depart_id = depid[0][0]

	@property
	def depart_id(self):		return self._depart_id

	def _prepare(self):
		depname = self.lineEditDepart.text()
		if self.lineEditDepart.isVisible() and not self.depart_id:
			qtmsgbox.show_error(self, "Ошибка", "Участок '%s' не найден" % depname)
			return
		self._continue()

	def _cancel(self):
		self._cancelled = True
		self.close()
	def _continue(self):
		self._cancelled = False
		self.close()

	@property
	def is_cancelled(self):
		return self._cancelled

class ReportBmByDateDialog(BaseDialog):
	def __init__(self, parent):
		super(ReportBmByDateDialog, self).__init__(parent)
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint | QtCore.Qt.MSWindowsFixedSizeDialogHint)
		self.setWindowTitle("Отчет по блочной модели")
		#
		self._parent = parent

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._cancelled = True
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры")
		self.layoutParams = QtGui.QVBoxLayout()

		self.layoutDateRange = QtGui.QFormLayout()
		self.labelDateRange = QtGui.QLabel(self.groupBoxParams)
		self.labelDateRange.setText("Временной интервал")
		self.labelDateRange.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDateRange.setMinimumWidth(120)
		self.dateRangeEdit = DateRangeWidget(self.groupBoxParams)
		
		self.groupBoxReportMode = QtGui.QGroupBox(self.groupBoxParams)
		self.groupBoxReportMode.setTitle("Режим отчета")
		self.layoutReportMode = QtGui.QVBoxLayout()
		self.radioModeButtonBox = QtGui.QButtonGroup()
		self.radioButtonCurrentInfo = QtGui.QRadioButton(self.groupBoxReportMode)
		self.radioButtonCurrentInfo.setText("Текущий статус")
		self.radioButtonCurrentInfo.setChecked(True)
		self.radioButtonHistoryInfo = QtGui.QRadioButton(self.groupBoxReportMode)
		self.radioButtonHistoryInfo.setText("Выемочные работы")
		self.radioModeButtonBox.addButton(self.radioButtonCurrentInfo)
		self.radioModeButtonBox.addButton(self.radioButtonHistoryInfo)

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QFormLayout()
		self.labelOutPath = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutPath.setText("Файл")
		self.labelOutPath.setMinimumWidth(50)
		self.labelOutPath.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutPath = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutPath.setMinimumWidth(300)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._prepare)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self._cancel)
	def __createBindings(self):
		self.lineEditOutPath.mouseDoubleClickEvent = lambda e: self._browseSaveFile(e)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.setContentsMargins(5,5,5,5)

		self.layoutMain.addWidget(self.groupBoxParams)

		self.groupBoxParams.setLayout(self.layoutParams)
		self.layoutParams.addLayout(self.layoutDateRange)
		self.layoutDateRange.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelDateRange)
		self.layoutDateRange.setWidget(0, QtGui.QFormLayout.FieldRole, self.dateRangeEdit)

		self.layoutParams.addWidget(self.groupBoxReportMode)

		self.groupBoxReportMode.setLayout(self.layoutReportMode)

		self.layoutReportMode.addWidget(self.radioButtonCurrentInfo)
		self.layoutReportMode.addWidget(self.radioButtonHistoryInfo)
	
		self.layoutMain.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutPath)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutPath)

		self.layoutMain.addWidget(self.buttonBox)

	def _browseSaveFile(self, e):
		filetypes = "EXCEL (*.XLSX)"
		try:			initialdir = MMpy.Project.path()
		except:			initialdir = os.getcwd()
		path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		if not path:
			return
		self.lineEditOutPath.setText(path)

	def _prepare(self):
		self._continue()

	def _cancel(self):
		self._cancelled = True
		self.close()
	def _continue(self):
		self._cancelled = False
		self.close()

	@property
	def is_cancelled(self):
		return self._cancelled

class AskSaveFileDialog(BaseDialog):
	def __init__(self, parent):
		super(AskSaveFileDialog, self).__init__(parent)
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint | QtCore.Qt.MSWindowsFixedSizeDialogHint)
		self.setWindowTitle("Выберите файл вывода")

		self._parent = parent

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._cancelled = True
		self._fileTypes = []
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QFormLayout()

		self.labelOutPath = QtGui.QLabel(self)
		self.labelOutPath.setText("Файл")
		self.labelOutPath.setMinimumWidth(50)

		self.lineEditOutPath = QtGui.QLineEdit(self)
		self.lineEditOutPath.setMinimumWidth(300)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._prepare)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self._cancel)
	def __createBindings(self):
		self.lineEditOutPath.mouseDoubleClickEvent = lambda e: self._browseSaveFile(e)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		#
		self.layoutMain.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutPath)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutPath)
		#
		self.layoutMain.addWidget(self.buttonBox)

	def _browseSaveFile(self, e):
		try:			initialdir = self._parent.settings.getValue(ID_NETPATH)
		except:			initialdir = os.getcwd()
		if self._fileTypes:
			path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir, self._fileTypes).replace("/", "\\")
		else:
			path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir).replace("/", "\\")
		if not path:
			return
		self.lineEditOutPath.setText(path)

	def setFileTypes(self, value):
		self._fileTypes = value

	def _prepare(self):
		self._continue()
	def _cancel(self):
		self._cancelled = True
		self.close()
	def _continue(self):
		self._cancelled = False
		self.close()

	@property
	def is_cancelled(self):
		return self._cancelled