from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.validation import is_blank, DoubleValidator, IntValidator, is_number
from utils.qtutil import getWidgetValue, setWidgetValue
from mm.mmutils import Tridb 

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog

from gui.qt.frames.settings.InterpolationSettingsFrame import InterpolationSearchGroupBox, InterpolationAxesGroupBox, RaduisGridEditor
from gui.qt.widgets.ListTreeItem import createListTreeItem
from gui.qt.widgets.ListTreeItemWidget import ListTreeItemWidget
from gui.qt.widgets.TopCutGridWidget import TopCutGridWidget
from gui.qt.widgets.XenolithTree import XenolithTreeWidget

from gui.qt.commands.LoadDetailedDrillholes import LoadDetailedDrillholes 
from gui.qt.commands.LoadDetailedTrenches import LoadDetailedTrenches

from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()
class InterpolationParamsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(InterpolationParamsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QGridLayout()
		self.layoutMain.setMargin(0)

		self.groupBoxInterpolationSearch = InterpolationSearchGroupBox(self)
		self.groupBoxInterpolationAxes = InterpolationAxesGroupBox(self)
		#
		self.groupBoxInterpolationRadius = QtGui.QGroupBox(self)
		self.groupBoxInterpolationRadius.setMaximumWidth(160)
		self.groupBoxInterpolationRadius.setTitle("Радиус поиска")
		self.layoutInterpolationRadius = QtGui.QVBoxLayout()
		self.gridEditorRadius = RaduisGridEditor(self.groupBoxInterpolationRadius)
		self.gridEditorRadius.toolBar.setVisible(False)
	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.groupBoxInterpolationSearch, 1, 1, 1, 1)
		self.layoutMain.addWidget(self.groupBoxInterpolationAxes, 2, 1, 1, 1)
		self.layoutMain.addWidget(self.groupBoxInterpolationRadius, 1, 2, 2, 1)
		self.groupBoxInterpolationRadius.setLayout(self.layoutInterpolationRadius)
		self.layoutInterpolationRadius.addWidget(self.gridEditorRadius)

	def inputFields(self):
		fields = (
			self.groupBoxInterpolationSearch.comboBoxSearchParamsSectors,
			self.groupBoxInterpolationSearch.lineEditSearchParamsMaxPoints,
			self.groupBoxInterpolationSearch.lineEditSearchParamsMinPoints,
			self.groupBoxInterpolationAxes.lineEditSearchAxesFactor1,
			self.groupBoxInterpolationAxes.lineEditSearchAxesFactor2,
			self.groupBoxInterpolationAxes.lineEditSearchAxesFactor3
		)
		return fields

class InterpolationFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(InterpolationFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QGridLayout()

		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setText("Выберите модель и введите параметры интерполяции")

		self.listModels = ListTreeItemWidget(self)
		self.listModels.setMaximumWidth(250)

		self.stackedWidget = QtGui.QStackedWidget(self)
		
	def __createBindings(self):
		self.listModels.itemSelectionChanged.connect(self.modelSelectionChanged)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.labelHint, 0, 0, 1, 2)
		self.layoutMain.addWidget(self.listModels, 1, 0, 1, 1)
		self.layoutMain.addWidget(self.stackedWidget, 1, 1, 1, 1)
		
	def check(self):
		for i in range(self.stackedWidget.count()):
			frame = self.stackedWidget.widget(i)
			#
			labels = (
				frame.groupBoxInterpolationSearch.labelSearchParamsMaxPoints,
				frame.groupBoxInterpolationSearch.labelSearchParamsMinPoints,
				frame.groupBoxInterpolationAxes.labelSearchAxesFactor1,
				frame.groupBoxInterpolationAxes.labelSearchAxesFactor2,
				frame.groupBoxInterpolationAxes.labelSearchAxesFactor3
			)
			fields = (
				frame.groupBoxInterpolationSearch.lineEditSearchParamsMaxPoints,
				frame.groupBoxInterpolationSearch.lineEditSearchParamsMinPoints,
				frame.groupBoxInterpolationAxes.lineEditSearchAxesFactor1,
				frame.groupBoxInterpolationAxes.lineEditSearchAxesFactor2,
				frame.groupBoxInterpolationAxes.lineEditSearchAxesFactor3
			)
			#
			for label, field in zip(labels, fields):
				if is_blank(field.text()):
					qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
					field.setFocus()
					self.listModels.setItemSelected(self.listModels.item(i), True)
					return
			#
			if not frame.gridEditorRadius.hasCorrectInput():
				qtmsgbox.show_error(self, "Ошибка", "Неверно заданы радиусы поиска")
				frame.gridEditorRadius.setFocus()
				self.listModels.setItemSelected(self.listModels.item(i), True)
				return

		return True

	def modelSelectionChanged(self):
		selectedIndexes = self.listModels.selectedIndexes()
		if not selectedIndexes:
			return
		selectedRow = selectedIndexes[0].row()
		self.stackedWidget.setCurrentIndex(selectedRow)

	def setModels(self, modelItems):
		for modelItem in modelItems:
			self.listModels.addItem(modelItem)
			self.stackedWidget.addWidget(InterpolationParamsFrame(self.stackedWidget))
	def modelParams(self, modelItem):
		index = [self.listModels.item(i) for i in range(self.listModels.count())].index(modelItem)
		frame = self.stackedWidget.widget(index)
		
		idwParamIds = (ID_IDWSECTOR, ID_IDWMINPTS, ID_IDWMAXPTS, ID_IDWFACTOR1, ID_IDWFACTOR2, ID_IDWFACTOR3)
		ret = {param: getWidgetValue(field) for field, param in zip(frame.inputFields(), idwParamIds)}
		ret[ID_IDWRADIUS] = frame.gridEditorRadius.dataGrid.to_list()
		return ret
	def setModelParams(self, modelItem, params):
		index = [self.listModels.item(i) for i in range(self.listModels.count())].index(modelItem)
		frame = self.stackedWidget.widget(index)

		idwParamIds = (ID_IDWSECTOR, ID_IDWMINPTS, ID_IDWMAXPTS, ID_IDWFACTOR1, ID_IDWFACTOR2, ID_IDWFACTOR3)
		for field, param in zip(frame.inputFields(), idwParamIds):
			setWidgetValue(field, params[param])

		frame.gridEditorRadius.dataGrid.from_list(params[ID_IDWRADIUS])

class TopCutGradeFrame(QtGui.QWidget):
	def __init__(self, parent):
		super(TopCutGradeFrame, self).__init__(parent)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setWordWrap(True)
		hint = "Выберите или введите пороговые значения для каждого элемента. "
		hint += "Для выбора выделите одну или несколько ячеек с необходимым пороговым значением и нажмите Применить. "
		hint += "Пустое значение в ячейке означает, что никаких ограничений применено не будет."
		self.labelHint.setText(hint)
		self.gridTopCutGrade = TopCutGridWidget(self)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Apply|QtGui.QDialogButtonBox.Discard)
		self.buttonBox.button(QtGui.QDialogButtonBox.Apply).setMinimumSize(QtCore.QSize(100, 25))
		self.buttonBox.button(QtGui.QDialogButtonBox.Apply).setText("Применить")
		self.buttonBox.button(QtGui.QDialogButtonBox.Discard).setMinimumSize(QtCore.QSize(100, 25))
		self.buttonBox.button(QtGui.QDialogButtonBox.Discard).setText("По умолчанию")
		self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Apply).clicked.connect(self.apply)
		self.buttonBox.button(QtGui.QDialogButtonBox.Discard).clicked.connect(self.applyDefault)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.labelHint)
		self.layoutMain.addWidget(self.gridTopCutGrade)
		self.layoutMain.addWidget(self.buttonBox)

	def check(self):
		return True

	def apply(self):
		grid = self.gridTopCutGrade
		nRows, nCols = grid.rowCount(), grid.columnCount()
		for iCol in range(1, nCols):
			selectedRows = [iRow for iRow in range(nRows-1) if grid.item(iRow, iCol).isSelected()]
			if not selectedRows:
				continue
			lastSelectedItem = grid.item(sorted(selectedRows)[-1], iCol)
			grid.item(nRows-1, iCol).setText(lastSelectedItem.text())
		grid.setFocus()
	def applyDefault(self):
		grid = self.gridTopCutGrade
		defaultRowIndex = grid.defaultRowIndex()
		nRows, nCols = grid.rowCount(), grid.columnCount()
		for iCol in range(1, nCols):
			grid.item(nRows-1, iCol).setText(grid.item(defaultRowIndex, iCol).text())
		grid.setFocus()

class GeologyParamsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(GeologyParamsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxEmptyRockCode = QtGui.QGroupBox(self)
		self.groupBoxEmptyRockCode.setTitle("Вмещающие породы")
		self.layoutEmptyRockCode = QtGui.QFormLayout(self.groupBoxEmptyRockCode)
		self.labelEmptyRockCode = QtGui.QLabel(self.groupBoxEmptyRockCode)
		self.labelEmptyRockCode.setText("Код")
		self.labelEmptyRockCode.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelEmptyRockCode.setMinimumWidth(50)
		self.comboBoxEmptyRockCode = QtGui.QComboBox(self.groupBoxEmptyRockCode)
		self.comboBoxEmptyRockCode.setMinimumWidth(200)
		self.comboBoxEmptyRockCode.setMaximumWidth(200)

		self.groupBoxXenolith = QtGui.QGroupBox(self)
		self.groupBoxXenolith.setTitle("Добавить ксенолиты")
		self.groupBoxXenolith.setCheckable(True)
		self.groupBoxXenolith.setChecked(False)
		self.layoutXenoliths = QtGui.QVBoxLayout(self.groupBoxXenolith)
		self.xenolithTree = XenolithTreeWidget(self.groupBoxXenolith)
	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxEmptyRockCode)

		self.layoutEmptyRockCode.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelEmptyRockCode)
		self.layoutEmptyRockCode.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxEmptyRockCode)

		self.layoutMain.addWidget(self.groupBoxXenolith)
		self.layoutXenoliths.addWidget(self.xenolithTree)

	def setEmptyRockCodeValues(self, values, currentValue = None):
		self.comboBoxEmptyRockCode.clear()
		self.comboBoxEmptyRockCode.addItems(values)
		if currentValue and currentValue in values:
			self.comboBoxEmptyRockCode.setCurrentIndex(values.index(currentValue))
	def getEmptyRockCode(self):
		return self.comboBoxEmptyRockCode.currentText()
	def check(self):
		return True


class DetailedDhdbFrame(QtGui.QWidget):
	def __init__(self, parent, mainapp):
		super(DetailedDhdbFrame, self).__init__(parent)
		self._mainapp = mainapp
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
		self.__setSamplesStatus()

	def __createVariables(self):
		self.__selectedDetailDrillholeSamples	= LoadDetailedDrillholes.get_empty_return_value() 
		self.__selectedTrenchSamples =  LoadDetailedTrenches.get_empty_return_value() 
		LoadDetailedDrillholes.empty_return_value  = 1

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.groupBoxDrillholes = QtGui.QGroupBox(self)
		self.groupBoxDrillholes.setTitle("Добавить данные опробования")
		self.groupBoxDrillholes.setCheckable(True)
		self.groupBoxDrillholes.setChecked(False)
		self.layoutDrillholes = QtGui.QHBoxLayout()
		self.layoutList = QtGui.QHBoxLayout()
		self.textEditDrillholes = QtGui.QTextEdit(self.groupBoxDrillholes)
		self.layoutButtons = QtGui.QGridLayout()
		self.layoutButtons.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft);
		self.buttonAddDetailDh = QtGui.QPushButton()
		self.buttonAddDetailDh.setText("Скважины")
		self.buttonAddDetailDh.setMaximumSize(QtCore.QSize(200, 25))

		self.buttonClearDetailDh = QtGui.QPushButton()
		self.buttonClearDetailDh.setIcon(QtGui.QIcon("/".join([IMG_ICONS_PATH, "delete.png"])))
		self.buttonClearDetailDh.setMaximumSize(QtCore.QSize(100, 25))

		self.buttonAddTrench = QtGui.QPushButton()
		self.buttonAddTrench.setText("Борозды")
		self.buttonAddTrench.setMaximumSize(QtCore.QSize(200, 25))

		self.buttonClearTrench = QtGui.QPushButton()
		self.buttonClearTrench.setIcon(QtGui.QIcon("/".join([IMG_ICONS_PATH, "delete.png"])))
		self.buttonClearTrench.setMaximumSize(QtCore.QSize(100, 25))

	def __createBindings(self):
		self.buttonAddDetailDh.clicked.connect(self.on_clicked_buttonAddDetailDh)
		self.buttonAddTrench.clicked.connect(self.on_clicked_buttonAddTrench)
		self.buttonClearDetailDh.clicked.connect(self.on_clicked_buttonClearDetailDh)
		self.buttonClearTrench.clicked.connect(self.on_clicked_buttonClearTrench)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.groupBoxDrillholes)
		self.groupBoxDrillholes.setLayout(self.layoutDrillholes)
		self.layoutDrillholes.addLayout(self.layoutList)
		self.layoutList.addWidget(self.textEditDrillholes)
		self.layoutDrillholes.addLayout(self.layoutButtons)
		self.layoutButtons.addWidget(self.buttonAddDetailDh,0,0)
		self.layoutButtons.addWidget(self.buttonClearDetailDh,0,1)
		self.layoutButtons.addWidget(self.buttonAddTrench,1,0)
		self.layoutButtons.addWidget(self.buttonClearTrench,1,1)


	def getSelectedDetailedDrillholeSamples(self):
		return self.__selectedDetailDrillholeSamples

	def getSelectedTrenchSamples(self):
		return self.__selectedTrenchSamples

	def check(self):
		return True

	def on_clicked_buttonAddDetailDh(self):
		use_connection = True
		if self._mainapp.srvcnxn is None:
			raise Exception("Не доступно подключение к БД")
		if  self._mainapp.srvcnxn is not None:
			loadDetailedDrillholes = LoadDetailedDrillholes(self._mainapp, use_connection)
			loadDetailedDrillholes.set_work_mode (LoadDetailedDrillholes.ID_LOAD_MODE_SELECT )
			loadDetailedDrillholes.exec()
			result = loadDetailedDrillholes.getSelectedSamples()
			if result != LoadDetailedDrillholes.get_empty_return_value():
				self.__selectedDetailDrillholeSamples = result
				self.__setSamplesStatus()

	def on_clicked_buttonAddTrench(self):
		use_connection = True
		if self._mainapp.srvcnxn is None:
			raise Exception("Не доступно подключение к БД")
		if self._mainapp.srvcnxn is not None:
			loadDetailedTrenches = LoadDetailedTrenches(self._mainapp, use_connection)
			loadDetailedTrenches.set_work_mode (loadDetailedTrenches.ID_LOAD_MODE_SELECT )
			loadDetailedTrenches.exec()
			result = loadDetailedTrenches.getSelectedSamples()
			if result != LoadDetailedTrenches.get_empty_return_value():
				self.__selectedTrenchSamples = result
				self.__setSamplesStatus()

	def on_clicked_buttonClearDetailDh (self):
		self.__selectedDetailDrillholeSamples	= LoadDetailedDrillholes.get_empty_return_value()
		self.__setSamplesStatus()

	def on_clicked_buttonClearTrench (self):
		self.__selectedTrenchSamples =  LoadDetailedTrenches.get_empty_return_value()
		self.__setSamplesStatus()

	def __setSamplesStatus(self):
		message = ""
		if not self.__selectedDetailDrillholeSamples['data'] is None:
			message = "Добавлено проб детальной разведки: {0} ".format(len(self.__selectedDetailDrillholeSamples['data']))
			message = message + '\n'
		if not self.__selectedTrenchSamples['data'] is None:
			message = message + "Добавлено бороздовых проб: {0} ".format(len(self.__selectedTrenchSamples['data']))
		self.textEditDrillholes.setText(message)


class OldNewValueCheckLineEdit (QtGui.QLineEdit):
	textModified = QtCore.pyqtSignal(str, str, str) 
	def __init__(self, parent,  **kwargs):
		super(OldNewValueCheckLineEdit, self).__init__(parent, **kwargs)
		self.editingFinished.connect(self.on_editingFinished)
		self.textChanged.connect(self.on_textChanged)
		self._init = None
		self._before = ''
	def on_textChanged(self, text):
		if not self.hasFocus():
			self._before = text
			if self._init is None:
			   self._init = self._before

	def on_editingFinished(self):
		before, after = self._before, self.text()
		if before != after:
			self._before = after
			self.textModified.emit(before, after, self._init)

class BoundariesParamsFrame(QtGui.QWidget):
	"""
	Предоставляет компоненты для ввода настроек высотных отметок в разрезе каждой БМ
	"""
	def __init__(self, *args, **kw):
		super(BoundariesParamsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(0)

		

		self.groupBoxBoundariesParams = QtGui.QGroupBox(self)
		self.groupBoxBoundariesParams.setTitle("Вертикальные границы")

		self.layoutBoundariesParams = QtGui.QFormLayout(self.groupBoxBoundariesParams)

		self.labelZMax = QtGui.QLabel(self.groupBoxBoundariesParams)
		self.labelZMax.setText("Максимум по Z")
		self.labelZMax.setMinimumWidth(100)
		self.labelZMax.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditZMax = OldNewValueCheckLineEdit(self.groupBoxBoundariesParams)
		self.lineEditZMax.setValidator(DoubleValidator(-1500, 300, 3))
		self.lineEditZMax.setMaximumWidth(100)
		self.lineEditZMax.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.labelZMin = QtGui.QLabel(self.groupBoxBoundariesParams)
		self.labelZMin.setText("Минимум по Z")
		self.labelZMin.setMinimumWidth(100)
		self.labelZMin.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditZMin = OldNewValueCheckLineEdit(self.groupBoxBoundariesParams)
		self.lineEditZMin.setValidator(DoubleValidator(-1500, 300, 3))
		self.lineEditZMin.setMaximumWidth(100)
		self.lineEditZMin.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)


	def __createBindings(self):
		self.lineEditZMin.textModified.connect(self.on_lineEditZMin_textModified)
		self.lineEditZMax.textModified.connect(self.on_lineEditZMax_textModified)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.groupBoxBoundariesParams)
		self.layoutBoundariesParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelZMax)
		self.layoutBoundariesParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditZMax)
		self.layoutBoundariesParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelZMin)
		self.layoutBoundariesParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditZMin)



	def inputFields(self):
		fields = {
			'zmin' : self.lineEditZMin,
			'zmax' : self.lineEditZMax
		}
		return fields
	def on_lineEditZMin_textModified(self, before, after, init):
		try:
			init_value = float(init)
			after_value = float(after)
		except:	
			msg =  "Вы ввели нижнюю границу БМ '%s' , есть вероятность потери данных,  если они содержатся в отсекаемых областях." %  (after )
			qtmsgbox.show_information(self, "Внимание", msg )
			return
		
		if after_value > init_value:
			msg =  "Вы ввели нижнюю границу БМ '%s' большую чем ранее '%s', есть вероятность потери данных,  если они содержатся в отсекаемых областях." %  (after, init )
			qtmsgbox.show_information(self, "Внимание", msg )
	def on_lineEditZMax_textModified(self, before, after, init):
		try: 
			init_value = float(init)
			after_value = float(after)
		except:	
			msg =  "Вы ввели верхнюю границу БМ '%s' , есть вероятность потери данных,  если они содержатся в отсекаемых областях." %  (after )
			qtmsgbox.show_information(self, "Внимание", msg )
			return
		
		if after_value < init_value:
			msg =  "Вы ввели верхнюю границу БМ '%s' меньшую чем ранее '%s', есть вероятность потери данных, если они содержатся в отсекаемых областях." %  (after, init )
			qtmsgbox.show_information(self, "Внимание", msg )
class ExpansionParamsFrame(QtGui.QWidget):
	"""
	Предоставляет компоненты для ввода настроек расширения области вмещающей породы вокруг области полезного компонента в БМ
	"""
	def __init__(self, *args, **kw):
		super(ExpansionParamsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(0)

		

		self.groupBoxExpansionParams = QtGui.QGroupBox(self)
		self.groupBoxExpansionParams.setTitle("Вмещающие породы")

		self.layoutExpansionParams = QtGui.QFormLayout(self.groupBoxExpansionParams)

		self.labelExpansion = QtGui.QLabel(self.groupBoxExpansionParams)
		self.labelExpansion.setText("Расширение")
		self.labelExpansion.setMinimumWidth(100)
		self.labelExpansion.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditExpansion = QtGui.QLineEdit(self.groupBoxExpansionParams)
		self.lineEditExpansion.setValidator(IntValidator(0, 200))
		self.lineEditExpansion.setMaximumWidth(100)
		self.lineEditExpansion.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)


	def __createBindings(self):
		pass

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.groupBoxExpansionParams)
		self.layoutExpansionParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelExpansion)
		self.layoutExpansionParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditExpansion)




	def inputFields(self):
		fields = {
			'expansion' : self.lineEditExpansion
		}
		return fields
	


class IsUseDynamicDensityParamsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(IsUseDynamicDensityParamsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(0)

		

		self.groupBoxDynamicDensityParams = QtGui.QGroupBox(self)
		self.groupBoxDynamicDensityParams.setTitle("Режим рассчета объёмного веса")

		self.layoutDynamicDensityParams = QtGui.QVBoxLayout(self.groupBoxDynamicDensityParams)

		self.radioOptionsMode = QtGui.QRadioButton("По справочнику", self.groupBoxDynamicDensityParams)
		self.radioOptionsMode.setToolTip("Настройки - Геология - Коды - Индексы в БМ - ρ")
		self.radioDynamicMode = QtGui.QRadioButton("Динамический рассчет",self.groupBoxDynamicDensityParams)
		self.radioDynamicMode.setToolTip(
			"Настройки - Геология - Коды - Индексы в БМ - ρ породы" "\n"
			", а также элементы и коэфициенты в зависимом справочнике."  "\n"
			"P = Pпороды + K1*ЭЛ1 + К2*ЭЛ2 + … + Кn*ЭЛn," "\n"
			"где Pпороды – плотность породы, K1…Kn – коэффициенты для химического элемента, ЭЛ1…ЭЛn – содержания химических элементов."	
			)

		self.radioOptionsMode.setChecked(True)

	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.groupBoxDynamicDensityParams)
		self.layoutDynamicDensityParams.addWidget(self.radioOptionsMode)
		self.layoutDynamicDensityParams.addWidget( self.radioDynamicMode)




	def inputFields(self):
		fields = {
			'isUseDynamicDensity' : self.radioDynamicMode
		}
		return fields

class StackParamsFrame(QtGui.QWidget):
	def __init__(self,parent, *args, **kw):
		super(StackParamsFrame, self).__init__(parent, *args, **kw)
		self._parent = parent
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setMargin(0)

		self.boundariesParamsFrame = BoundariesParamsFrame(self._parent)
		self.expansionParamsFrame = ExpansionParamsFrame(self._parent)	 
		self.isUseDynamicDensityParamsFrame = IsUseDynamicDensityParamsFrame(self._parent)	 


	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.boundariesParamsFrame)
		self.layoutMain.addWidget(self.expansionParamsFrame)
		self.layoutMain.addWidget(self.isUseDynamicDensityParamsFrame)




class OptionsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(OptionsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		#self.groupBoxParams = QtGui.QGroupBox(self)
		#self.groupBoxParams.setTitle("Параметры")
		#self.layoutParams = QtGui.QFormLayout()
		#self.labelBuffer = QtGui.QLabel(self.groupBoxParams)
		#self.labelBuffer.setText("Отступ по Z")
		#self.labelBuffer.setMinimumWidth(100)
		#self.labelBuffer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		#self.lineEditBuffer = QtGui.QLineEdit(self.groupBoxParams)
		#self.lineEditBuffer.setValidator(DoubleValidator(0, 100, 3))
		#self.lineEditBuffer.setMaximumWidth(100)
		#self.lineEditBuffer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.layoutZBoundaries = QtGui.QGridLayout()

		self.groupBoxZBoundaries = QtGui.QGroupBox(self)
		self.groupBoxZBoundaries.setTitle("Параметры БМ")
		self.groupBoxZBoundaries.setCheckable(True)
		self.groupBoxZBoundaries.setChecked(False)

		self.listModels = ListTreeItemWidget(self)
		#self.listModels.setMaximumWidth(300)

		self.stackedWidget = QtGui.QStackedWidget(self)

		
		


		#
	def __createBindings(self):
		self.listModels.itemSelectionChanged.connect(self.modelSelectionChanged)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxZBoundaries)
		self.groupBoxZBoundaries.setLayout(self.layoutZBoundaries)

		self.layoutZBoundaries.addWidget(self.listModels, 1, 1, 1, 3)
		self.layoutZBoundaries.addWidget(self.stackedWidget, 2, 1, 1, 1)


	def setParams(self, params):
		pass

	def check(self):
		#labels = (self.labelBuffer,)
		#fields = (self.lineEditBuffer,)
		##
		#for label, field in zip(labels, fields):
		#	if is_blank(field.text()):
		#		qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
		#		field.setFocus()
		#		return
		return True
	def modelSelectionChanged(self):
		selectedIndexes = self.listModels.selectedIndexes()
		if not selectedIndexes:
			return
		selectedRow = selectedIndexes[0].row()
		self.stackedWidget.setCurrentIndex(selectedRow)

	def setModels(self, modelItems):
		for modelItem in modelItems:
			self.listModels.addItem(modelItem)
			
			self.stackedWidget.addWidget(StackParamsFrame(self.stackedWidget))

	def modelParams(self, modelItem):
		index = [self.listModels.item(i) for i in range(self.listModels.count())].index(modelItem)
		frame = self.stackedWidget.widget(index)
		inputFields = frame.boundariesParamsFrame.inputFields()
		ret = { param: getWidgetValue(inputFields[param]) for param in ('zmin','zmax')}

		inputFields = frame.expansionParamsFrame.inputFields()
		ret.update({ param: getWidgetValue(inputFields[param]) for param in {'expansion'}})

		inputFields = frame.isUseDynamicDensityParamsFrame.inputFields()
		ret.update({ param: getWidgetValue(inputFields[param]) for param in {'isUseDynamicDensity'}})
		return ret

	def setModelParams(self, modelItem, params):
		index = [self.listModels.item(i) for i in range(self.listModels.count())].index(modelItem)
		frame = self.stackedWidget.widget(index)
		inputFields = frame.boundariesParamsFrame.inputFields()
		for param in ('zmin','zmax'):
			setWidgetValue(inputFields[param], params[param])

		inputFields = frame.expansionParamsFrame.inputFields()
		for param in {'expansion'}:
			setWidgetValue(inputFields[param], params[param])
					
		inputFields = frame.isUseDynamicDensityParamsFrame.inputFields()
		for param in {'isUseDynamicDensity'}:
			setWidgetValue(inputFields[param], params[param])

	@property
	def isParamsZChecked(self):
		return self.groupBoxZBoundaries.isChecked()


class GeoUpdateBmDialog(BaseDialog):
	def __init__(self, parent):
		super(GeoUpdateBmDialog, self).__init__(parent)
		self._parent = parent
		self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowMaximizeButtonHint)
		self.setWindowTitle("Параметры обновления блочных моделей")
		self.setFixedSize(QtCore.QSize(865, 300))

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._is_cancelled = True

	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()

		self.tabWidget = QtGui.QTabWidget(self)

		self.frameInteroplation = InterpolationFrame(self.tabWidget)
		self.frameTopCutGrade = TopCutGradeFrame(self.tabWidget)
		self.frameGeologyParams = GeologyParamsFrame(self.tabWidget)
		self.frameDetailedDhdb = DetailedDhdbFrame(self.tabWidget, self._parent)
		self.frameOptions = OptionsFrame(self.tabWidget)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumSize(QtCore.QSize(100, 25))
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._continue)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumSize(QtCore.QSize(100, 25))
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self._cancel)
		self.buttonBox.setOrientation(QtCore.Qt.Vertical)

	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.tabWidget)

		tabs = [self.frameGeologyParams, self.frameInteroplation, self.frameTopCutGrade, self.frameDetailedDhdb, self.frameOptions]
		names = ["Параметры геологи", "Параметры интерполяции", "Ограничение ураганов", "Детальная разведка", "Дополнительные параметры"]
		for tab, name in zip(tabs, names):
			self.tabWidget.addTab(tab, name)
		self.layoutMain.addWidget(self.buttonBox)

	def check(self):
		tabs = (self.frameInteroplation, self.frameTopCutGrade, self.frameGeologyParams, self.frameDetailedDhdb, self.frameOptions)
		for i, tab in enumerate(tabs):
			if not tab.check():
				self.tabWidget.setCurrentIndex(i)
				return
		#
		return True

	def _continue(self):
		if not self.check():
			return
		self._is_cancelled = False
		self.close()
	def _cancel(self):
		self._is_cancelled = True
		self.close()
	@property
	def is_cancelled(self):
		return self._is_cancelled

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
			event.ignore()
		else:
			super(GeoUpdateBmDialog, self).keyPressEvent(event)




