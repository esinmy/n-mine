try:		import MMpy
except:		pass

from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.validation import DoubleValidator, is_number, is_blank
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog

from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class AutoVizexPlotDialog(BaseDialog):
	def __init__(self, parent):
		super(AutoVizexPlotDialog, self).__init__(parent)

		self._parent = parent

		self.setWindowTitle("Печать")
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint | QtCore.Qt.MSWindowsFixedSizeDialogHint)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.getPlotTypes()

	def __createVariables(self):
		self._template_path = os.path.join(os.getcwd(), "files", "plot_templates")
		self._cancelled = True
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры чертежа")
		self.layoutParams = QtGui.QVBoxLayout()

		self.layoutGeneralParams = QtGui.QFormLayout()
		self.labelPlotType = QtGui.QLabel(self.groupBoxParams)
		self.labelPlotType.setMinimumWidth(100)
		self.labelPlotType.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelPlotType.setText("Тип чертежа")
		self.comboBoxPlotType = QtGui.QComboBox(self.groupBoxParams)
		self.comboBoxPlotType.setMaximumWidth(150)
		self.labelLevelZ = QtGui.QLabel(self.groupBoxParams)
		self.labelLevelZ.setMinimumWidth(100)
		self.labelLevelZ.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelLevelZ.setText("Отметка Z")
		self.lineEditLevelZ = QtGui.QLineEdit(self.groupBoxParams)
		self.lineEditLevelZ.setMaximumWidth(150)
		self.lineEditLevelZ.setAlignment(QtCore.Qt.AlignRight)
		self.lineEditLevelZ.setValidator(DoubleValidator())
		self.lineEditLevelZ.setText("0")
		self.checkBoxReverse = QtGui.QCheckBox(self.groupBoxParams)
		self.checkBoxReverse.setText("Печать поперечных разрезов в обратном порядке")

		self.layoutPartParams = QtGui.QHBoxLayout()
		self.groupBoxPlotHeaderParams = QtGui.QGroupBox(self.groupBoxParams)
		self.groupBoxPlotHeaderParams.setTitle("Продольные разрезы и планы")
		self.groupBoxPlotHeaderParams.setCheckable(True)
		self.groupBoxPlotHeaderParams.setChecked(True)
		self.layoutPlotHeaderParams = QtGui.QFormLayout()
		self.labelPlotHeaderSheetFormat = QtGui.QLabel(self.groupBoxPlotHeaderParams)
		self.labelPlotHeaderSheetFormat.setMinimumWidth(90)
		self.labelPlotHeaderSheetFormat.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelPlotHeaderSheetFormat.setText("Формат листа")
		self.comboBoxPlotHeaderSheetFormat = QtGui.QComboBox(self.groupBoxPlotHeaderParams)
		self.comboBoxPlotHeaderSheetFormat.setMinimumWidth(150)
		self.labelPlotHeaderGroupStyle = QtGui.QLabel(self.groupBoxPlotHeaderParams)
		self.labelPlotHeaderGroupStyle.setMinimumWidth(90)
		self.labelPlotHeaderGroupStyle.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelPlotHeaderGroupStyle.setText("Компоновка")
		self.comboBoxPlotHeaderGroupStyle = QtGui.QComboBox(self.groupBoxPlotHeaderParams)
		self.comboBoxPlotHeaderGroupStyle.setMinimumWidth(150)

		self.groupBoxPlotBodyParams = QtGui.QGroupBox(self.groupBoxParams)
		self.groupBoxPlotBodyParams.setTitle("Поперечные разрезы")
		self.groupBoxPlotBodyParams.setCheckable(True)
		self.groupBoxPlotBodyParams.setChecked(True)
		self.layoutPlotBodyParams = QtGui.QFormLayout()
		self.labelPlotBodySheetFormat = QtGui.QLabel(self.groupBoxPlotBodyParams)
		self.labelPlotBodySheetFormat.setMinimumWidth(100)
		self.labelPlotBodySheetFormat.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelPlotBodySheetFormat.setText("Формат листа")
		self.comboBoxPlotBodySheetFormat = QtGui.QComboBox(self.groupBoxPlotBodyParams)
		self.comboBoxPlotBodySheetFormat.setMinimumWidth(150)
		self.labelPlotBodyFragmentsCount = QtGui.QLabel(self.groupBoxPlotBodyParams)
		self.labelPlotBodyFragmentsCount.setMinimumWidth(100)
		self.labelPlotBodyFragmentsCount.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelPlotBodyFragmentsCount.setText("Число фрагментов")
		self.comboBoxPlotBodyFragmentsCount = QtGui.QComboBox(self.groupBoxPlotBodyParams)
		self.comboBoxPlotBodyFragmentsCount.setMinimumWidth(150)

		self.groupBoxPrefixes = QtGui.QGroupBox(self.groupBoxParams)
		self.groupBoxPrefixes.setTitle("Префиксы разрезов")
		self.layoutPrefixes = QtGui.QGridLayout()
		self.labelLongPrefix = QtGui.QLabel(self.groupBoxPrefixes)
		self.labelLongPrefix.setText("Продольный")
		self.labelLongPrefix.setMinimumWidth(90)
		self.labelLongPrefix.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditLongPrefix = QtGui.QLineEdit(self.groupBoxPrefixes)
		self.lineEditLongPrefix.setMaximumWidth(150)
		self.labelCrossPrefix = QtGui.QLabel(self.groupBoxPrefixes)
		self.labelCrossPrefix.setText("Поперечный")
		self.labelCrossPrefix.setMinimumWidth(120)
		self.labelCrossPrefix.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditCrossPrefix = QtGui.QLineEdit(self.groupBoxPrefixes)
		self.lineEditCrossPrefix.setMaximumWidth(150)

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QFormLayout()
		self.labelOutputDirectory = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutputDirectory.setMinimumWidth(100)
		self.labelOutputDirectory.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutputDirectory.setText("Директория")
		self.lineEditOutputDirectory = QtGui.QLineEdit(self.groupBoxOutput)
		self.labelOutputName = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutputName.setMinimumWidth(100)
		self.labelOutputName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutputName.setText("Название")
		self.lineEditOutputName = QtGui.QLineEdit(self.groupBoxOutput)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._prepare)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self._cancel)
	def __createBindings(self):
		self.comboBoxPlotType.currentIndexChanged.connect(self.plotTypeChanged)
		self.comboBoxPlotHeaderSheetFormat.currentIndexChanged.connect(self.plotHeaderSheetFormatChanged)
		self.comboBoxPlotBodySheetFormat.currentIndexChanged.connect(self.plotBodySheetFormatChanged)
		#
		self.lineEditOutputDirectory.mouseDoubleClickEvent = lambda e: self.browseOutputDirectory(e)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxParams)
		self.groupBoxParams.setLayout(self.layoutParams)

		self.layoutParams.addLayout(self.layoutGeneralParams)
		self.layoutGeneralParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelPlotType)
		self.layoutGeneralParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxPlotType)
		self.layoutGeneralParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelLevelZ)
		self.layoutGeneralParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditLevelZ)
		self.layoutGeneralParams.setWidget(2, QtGui.QFormLayout.FieldRole, self.checkBoxReverse)

		self.layoutParams.addLayout(self.layoutPartParams)
		self.layoutPartParams.addWidget(self.groupBoxPlotHeaderParams)
		self.groupBoxPlotHeaderParams.setLayout(self.layoutPlotHeaderParams)
		self.layoutPlotHeaderParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelPlotHeaderSheetFormat)
		self.layoutPlotHeaderParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxPlotHeaderSheetFormat)
		self.layoutPlotHeaderParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelPlotHeaderGroupStyle)
		self.layoutPlotHeaderParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxPlotHeaderGroupStyle)

		self.layoutPartParams.addWidget(self.groupBoxPlotBodyParams)
		self.groupBoxPlotBodyParams.setLayout(self.layoutPlotBodyParams)
		self.layoutPlotBodyParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelPlotBodySheetFormat)
		self.layoutPlotBodyParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxPlotBodySheetFormat)
		self.layoutPlotBodyParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelPlotBodyFragmentsCount)
		self.layoutPlotBodyParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxPlotBodyFragmentsCount)

		self.layoutParams.addWidget(self.groupBoxPrefixes)
		self.groupBoxPrefixes.setLayout(self.layoutPrefixes)
		self.layoutPrefixes.addWidget(self.labelLongPrefix, 0, 0, 1, 1)
		self.layoutPrefixes.addWidget(self.lineEditLongPrefix, 0, 1, 1, 1)
		self.layoutPrefixes.addWidget(self.labelCrossPrefix, 0, 2, 1, 1)
		self.layoutPrefixes.addWidget(self.lineEditCrossPrefix, 0, 3, 1, 1)

		self.layoutMain.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutputDirectory)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutputDirectory)
		self.layoutOutput.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelOutputName)
		self.layoutOutput.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditOutputName)

		self.layoutMain.addWidget(self.buttonBox)
	def getPlotTypes(self):
		self.comboBoxPlotType.clear()
		for root, folders, filenames in os.walk(self._template_path):
			# хардкодинг для Коньшина, чтобы геологи и технологи не могли распечатать паспорт БВР 
			if self._parent.main_app.userRole() in [ID_USERGEO, ID_USERDSN]:
				folders = list(filter(lambda folderName: "ПАСПОРТ БВР" not in folderName.upper(), folders))
			self.comboBoxPlotType.addItems([""] + list(folders))
			break
	#
	def getPlotHeaderSheetFormats(self):
		self.comboBoxPlotHeaderSheetFormat.clear()
		#
		plotType = self.comboBoxPlotType.currentText()
		if not plotType:
			return
		#
		plotTypePath = os.path.join(self._template_path, plotType, "title")
		for root, folders, filenames in os.walk(plotTypePath):
			self.comboBoxPlotHeaderSheetFormat.addItems([""] + list(folders))
			break
	def getPlotHeaderGroupStyles(self):
		self.comboBoxPlotHeaderGroupStyle.clear()
		#
		plotType = self.comboBoxPlotType.currentText()
		sheetFormat = self.comboBoxPlotHeaderSheetFormat.currentText()
		if not all([plotType, sheetFormat]):
			return
		#
		groupStylesPath = os.path.join(self._template_path, plotType, "title", sheetFormat)
		for root, folders, filenames in os.walk(groupStylesPath):
			self.comboBoxPlotHeaderGroupStyle.addItems([""] + list(folders))
			break
	def getPlotHeaderRoot(self):
		plotType = self.comboBoxPlotType.currentText()
		sheetFormat = self.comboBoxPlotHeaderSheetFormat.currentText()
		groupStyle = self.comboBoxPlotHeaderGroupStyle.currentText()
		if not all([plotType, sheetFormat, groupStyle]):
			return ""
		return os.path.join(self._template_path, plotType, "title", sheetFormat, groupStyle)
	#
	def getPlotBodySheetFormats(self):
		self.comboBoxPlotBodySheetFormat.clear()
		#
		plotType = self.comboBoxPlotType.currentText()
		if not plotType:
			return
		#
		plotTypePath = os.path.join(self._template_path, plotType, "body")
		for root, folders, filenames in os.walk(plotTypePath):
			self.comboBoxPlotBodySheetFormat.addItems([""] + list(folders))
			break
	def getPlotBodyFragmentsCount(self):
		self.comboBoxPlotBodyFragmentsCount.clear()
		#
		plotType = self.comboBoxPlotType.currentText()
		sheetFormat = self.comboBoxPlotBodySheetFormat.currentText()
		if not all([plotType, sheetFormat]):
			return
		#
		groupStylesPath = os.path.join(self._template_path, plotType, "body", sheetFormat)
		for root, folders, filenames in os.walk(groupStylesPath):
			self.comboBoxPlotBodyFragmentsCount.addItems([""] + list(folders))
			break
	def getPlotBodyRoot(self):
		plotType = self.comboBoxPlotType.currentText()
		sheetFormat = self.comboBoxPlotBodySheetFormat.currentText()
		fragsCount = self.comboBoxPlotBodyFragmentsCount.currentText()
		if not all([plotType, sheetFormat, fragsCount]):
			return ""
		return os.path.join(self._template_path, plotType, "body", sheetFormat, fragsCount)

	#
	def plotTypeChanged(self):
		self.getPlotHeaderSheetFormats()
		self.getPlotBodySheetFormats()
	def plotHeaderSheetFormatChanged(self):
		self.getPlotHeaderGroupStyles()
	def plotBodySheetFormatChanged(self):
		self.getPlotBodyFragmentsCount()

	#
	def browseOutputDirectory(self, event):
		try:		initialdir = MMpy.Project.path()
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getExistingDirectory(self, "Выберите директорию", initialdir).replace("/", "\\")
		if not path:
			return
		self.lineEditOutputDirectory.setText(path)

	def _prepare(self):
		if not self.comboBoxPlotType.currentText():
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelPlotType.text())
			self.comboBoxPlotType.setFocus()
			return
		if not is_number(self.lineEditLevelZ.text()):
			qtmsgbox.show_error(self, "Ошибка", "Неверное значение поля '%s'" % self.labelLevelZ.text())
			self.lineEditLevelZ.setFocus()
			return

		if not self.groupBoxPlotHeaderParams.isChecked() and not self.groupBoxPlotBodyParams.isChecked():
			qtmsgbox.show_error(self, "Ошибка", "Нет выбранных объектов для печати")
			return

		#
		if self.groupBoxPlotHeaderParams.isChecked():
			labels = [self.labelPlotHeaderSheetFormat, self.labelPlotHeaderGroupStyle]
			fields = [self.comboBoxPlotHeaderSheetFormat, self.comboBoxPlotHeaderGroupStyle]
			for label, field in zip(labels, fields):
				if not field.currentText():
					qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
					field.setFocus()
					return
		if self.groupBoxPlotBodyParams.isChecked():
			labels = [self.labelPlotBodySheetFormat, self.labelPlotBodyFragmentsCount]
			fields = [self.comboBoxPlotBodySheetFormat, self.comboBoxPlotBodyFragmentsCount]
			for label, field in zip(labels, fields):
				if not field.currentText():
					qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
					field.setFocus()
					return
		#
		outputDirectory = self.lineEditOutputDirectory.text()
		if is_blank(outputDirectory):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelOutputDirectory.text())
			self.lineEditOutputDirectory.setFocus()
			return
		if not os.path.exists(outputDirectory):
			qtmsgbox.show_error(self, "Ошибка", "Путь '%s' не найден" % outputDirectory)
			self.lineEditOutputDirectory.setFocus()
			return
		#
		outputName = self.lineEditOutputName.text() 
		if is_blank(outputName):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelOutputName.text())
			self.lineEditOutputName.setFocus()
			return
		for invalidSymbol in "\/:*?<>|":
			if invalidSymbol in outputName:
				qtmsgbox.show_error(self, "Ошибка", "Недопустимый символ '%s'" % invalidSymbol)
				return
		if os.path.exists(os.path.join(outputDirectory, outputName)):
			qtmsgbox.show_error(self, "Ошибка", "Печать '%s' уже существует" % outputName)
			self.lineEditOutputName.setFocus()
			return

		#
		self._continue()

	def _cancel(self):
		self._cancelled = True
		self.close()
	def _continue(self):
		self._cancelled = False
		self.close()
		
	@property
	def is_cancelled(self):
		return self._cancelled
