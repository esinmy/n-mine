try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog
from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog

from utils.constants import *
from utils.validation import is_blank

class CalcStockDialog(BaseDialog):
	def __init__(self, parent):
		super(CalcStockDialog, self).__init__(parent)
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setWindowTitle("Рассчитать склад")

		self._parent = parent

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.setFixedSize(self.sizeHint())

		self.buildCombos()

	def __createVariables(self):
		self._cancelled = True
		self._templatesRootPath = os.path.join(os.getcwd(), "files", "stock_plot_templates", "body")
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(5)

		self.layoutInput = QtGui.QHBoxLayout()

		self.groupBoxPlotParams = QtGui.QGroupBox(self)
		self.groupBoxPlotParams.setTitle("Параметры чертежа")
		self.layoutPlotParams = QtGui.QFormLayout()
		self.labelSheetFormat = QtGui.QLabel(self.groupBoxPlotParams)
		self.labelSheetFormat.setText("Формат листа")
		self.labelSheetFormat.setMinimumWidth(100)
		self.labelSheetFormat.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxSheetFormat = QtGui.QComboBox(self.groupBoxPlotParams)
		self.comboBoxSheetFormat.setMinimumWidth(150)
		self.labelScale = QtGui.QLabel(self.groupBoxPlotParams)
		self.labelScale.setText("Масштаб")
		self.labelScale.setMinimumWidth(100)
		self.labelScale.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxScale = QtGui.QComboBox(self.groupBoxPlotParams)
		self.comboBoxScale.setMinimumWidth(150)
		self.labelOrientation = QtGui.QLabel(self.groupBoxPlotParams)
		self.labelOrientation.setText("Ориентация")
		self.labelOrientation.setMinimumWidth(100)
		self.labelOrientation.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxOrientation = QtGui.QComboBox(self.groupBoxPlotParams)
		self.comboBoxOrientation.setMinimumWidth(150)

		self.groupBoxDisplayParams = QtGui.QGroupBox(self)
		self.groupBoxDisplayParams.setTitle("Настройки отображения")
		self.layoutDisplayParams = QtGui.QFormLayout()
		self.labelIsoLine = QtGui.QLabel(self.groupBoxDisplayParams)
		self.labelIsoLine.setText("Форма изолиний")
		self.labelIsoLine.setMinimumWidth(100)
		self.labelIsoLine.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditIsoLine = QtGui.QLineEdit(self.groupBoxDisplayParams)
		self.lineEditIsoLine.setAlignment(QtCore.Qt.AlignRight)
		self.lineEditIsoLine.setMaximumWidth(150)

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QFormLayout()
		self.labelOutputDirectory = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutputDirectory.setText("Директория")
		self.labelOutputDirectory.setMinimumWidth(100)
		self.labelOutputDirectory.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutputDirectory = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutputDirectory.setMinimumWidth(200)
		self.labelOutputName = QtGui.QLabel(self.groupBoxOutput)
		self.labelOutputName.setText("Название")
		self.labelOutputName.setMinimumWidth(100)
		self.labelOutputName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditOutputName = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEditOutputName.setMinimumWidth(200)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._saveAndClose)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
		self.lineEditOutputDirectory.mouseDoubleClickEvent = lambda e: self.browseOutputDirectory(e)
		self.lineEditIsoLine.mouseDoubleClickEvent = lambda e: self.browseFormset(e, self.lineEditIsoLine)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addLayout(self.layoutInput)

		self.layoutInput.addWidget(self.groupBoxPlotParams)
		self.groupBoxPlotParams.setLayout(self.layoutPlotParams)
		self.layoutPlotParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelSheetFormat)
		self.layoutPlotParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxSheetFormat)
		self.layoutPlotParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelScale)
		self.layoutPlotParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxScale)
		self.layoutPlotParams.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelOrientation)
		self.layoutPlotParams.setWidget(2, QtGui.QFormLayout.FieldRole, self.comboBoxOrientation)
		
		self.layoutInput.addWidget(self.groupBoxDisplayParams)
		self.groupBoxDisplayParams.setLayout(self.layoutDisplayParams)
		self.layoutDisplayParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelIsoLine)
		self.layoutDisplayParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditIsoLine)

		self.layoutMain.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOutputDirectory)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOutputDirectory)
		self.layoutOutput.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelOutputName)
		self.layoutOutput.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditOutputName)

		self.layoutMain.addWidget(self.buttonBox)

	def buildCombos(self):
		self.buildComboSheetFormat()
		self.buildComboScale()
		self.buildComboOrientation()
	def buildComboSheetFormat(self):
		self.comboBoxSheetFormat.clear()
		if not os.path.exists(self._templatesRootPath):
			return
		for root, folders, filenames in os.walk(self._templatesRootPath):
			self.comboBoxSheetFormat.addItems([""] + folders)
			break
	def buildComboScale(self):
		self.comboBoxScale.clear()
		self.comboBoxScale.addItems(["1:200", "1:500", "1:2000", "1:5000"])
	def buildComboOrientation(self):
		self.comboBoxOrientation.clear()
		self.comboBoxOrientation.addItems(["Альбомная", "Портретная"])

	def check(self):
		# проверка заполненности полей
		labels = [self.labelSheetFormat, self.labelScale, self.labelOrientation]
		combos = [self.comboBoxSheetFormat, self.comboBoxScale, self.comboBoxOrientation]
		for label, combo in zip(labels, combos):
			if is_blank(combo.currentText()):
				qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % label.text())
				return

		# проверка существования шаблона
		ptxPath = self.getTemplatePath()
		if not os.path.exists(ptxPath):
			qtmsgbox.show_error(self, "Ошика", "Не найден необходимый шаблон печати")
			return

		# проверка директории имени
		outputDirectory = self.lineEditOutputDirectory.text()
		if is_blank(outputDirectory):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelOutputDirectory.text())
			return
		if not os.path.exists(outputDirectory):
			qtmsgbox.show_error(self, "Ошибка", "Путь '%s' не найден" % outputDirectory)
			return

		# проверка имени вывода
		outputName = self.lineEditOutputName.text()
		if is_blank(outputName):
			qtmsgbox.show_error(self, "Ошибка", "Укажите поле '%s'" % self.labelOutputName.text())
			return
		for invalidSymbol in "\/:*?<>|":
			if invalidSymbol in outputName:
				qtmsgbox.show_error(self, "Ошибка", "Недопустимый символ '%s'" % invalidSymbol)
				return
		if os.path.exists(os.path.join(outputDirectory, outputName)):
			qtmsgbox.show_error(self, "Ошибка", "Печать '%s' уже существует" % outputName)
			self.lineEditOutputName.setFocus()
			return

		return True

	def _saveAndClose(self):
		if not self.check():
			return
		self._cancelled = False
		self.close()

	def browseOutputDirectory(self, item):
		try:		initialdir = MMpy.Project.path()
		except:		initialdir = os.getcwd()
		path = QtGui.QFileDialog.getExistingDirectory(self, "Выберите директорию", initialdir).replace("/", "\\")
		if not path:
			return
		self.lineEditOutputDirectory.setText(path)
	def browseFormset(self, event, wgt):
		if not wgt.isEnabled():
			return
		#
		try:
			tree = TreeMmFormsetBrowserDialog(self)
			tree.readFormsets(617)
			tree.exec()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать наборы форм. %s" % str(e))
			return

		ret = tree.result
		if not ret:
			return
		wgt.setText(ret)

	def getTemplatePath(self):
		sheetFormat = self.comboBoxSheetFormat.currentText()
		orientation = "landscape" if self.comboBoxOrientation.currentIndex() == 0 else "portrait"
		numStocks = len(list(filter(lambda it: it.isStock(), self._parent.mainWidget.stockExplorer.explorer.iterChildren())))
		if not all([sheetFormat, orientation, numStocks]):
			return ""
		return os.path.join(self._templatesRootPath, sheetFormat, "%s_%s.PTX" % (orientation, str(numStocks).zfill(2)))

	def getTemplateBodyRoot(self):
		sheetFormat = self.comboBoxSheetFormat.currentText()
		if not sheetFormat:
			return ""
		return os.path.join(self._templatesRootPath, sheetFormat, "fragments")

	@property
	def cancelled(self):		return self._cancelled
