from PyQt4 import QtCore, QtGui
import os, math, csv
from copy import deepcopy
from itertools import groupby

# nMineDir = "D:\\NN\\N-Mine"
# os.chdir(nMineDir)
# os.sys.path.append(nMineDir)

from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.dialogs.BaseDialog import BaseDialog
import gui.qt.dialogs.messagebox as qtmsgbox

from gui.qt.modes.geo.GeoUpdateBm import getDynamicDensity

from gui.qt.widgets.ListTreeItem import ListTreeBmItem, ListTreeWireframeItem
from gui.qt.widgets.TableDataView import CustomDataView, CustomDataModel
from gui.qt.widgets.TreeModel import TreeWidget
from utils.constants import *
from utils.qtutil import iterWidgetParents
from utils.validation import DoubleValidator, is_blank, is_number
from mm.mmutils import MicromineFile
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

ROCK_AND_ORE = "Руда/Порода"
BACKFILL = "Бетон"

def createBmPreviewListTreeItem(value, itemType, itemCategory):
	itDict = {}

	itDict[ITEM_BM] = BmPreviewListTreeBmItem
	itDict[ITEM_WIREFRAME] = BmPreviewListTreeWireframeItem
	item = itDict[itemType](value, itemCategory)

	return item

class BmPreviewListTreeBmItem(ListTreeBmItem):
	def __init__(self, *args, **kw):
		super(BmPreviewListTreeBmItem, self).__init__(*args, **kw)
		self._key = tuple()
	def key(self):					return self._key
	def setKey(self, values):		self._key = tuple(deepcopy(values))

class BmPreviewListTreeWireframeItem(ListTreeWireframeItem):
	def __init__(self, *args, **kw):
		super(BmPreviewListTreeWireframeItem, self).__init__(*args, **kw)
		self._key = tuple()
	def key(self):					return self._key
	def setKey(self, values):		self._key = tuple(deepcopy(values))

class CheckBoxTreeItem(QtGui.QWidget):
	def __init__(self, item, column):
		super(CheckBoxTreeItem, self).__init__()

		self._item = item
		self._item.setTextAlignment(column, QtCore.Qt.AlignCenter)
		self._column = column

		self._checkBox = QtGui.QCheckBox()
		self._checkBox.stateChanged.connect(self._stateChanged)

		layout = QtGui.QHBoxLayout()
		layout.addWidget(self._checkBox)
		layout.setAlignment(QtCore.Qt.AlignCenter)
		layout.setContentsMargins(0,0,0,0)
		layout.setSpacing(0)
		layout.setMargin(0)

		self.setLayout(layout)
	def checkBox(self):		return self._checkBox
	def item(self):		return self._item
	def _stateChanged(self, state):
		tree = self._item.treeWidget()
		self._item.setText(self._column, str(state))
		selectedItems = tree.selectedItems()
		if not self._item.isSelected() or not selectedItems or len(selectedItems) == 1:
			tree.clearSelection()
			tree.setItemSelected(self._item, True)
		tree.setFocus()

class ComboBoxTreeItem(QtGui.QComboBox):
	def __init__(self, item, column):
		super(ComboBoxTreeItem, self).__init__()
		self._item = item
		self._column = column

		self.__createBindings()

	def __createBindings(self):
		self.editTextChanged.connect(self._textChanged)
		self.currentIndexChanged.connect(self._indexChanged)

	def _indexChanged(self, index):
		self._item.treeWidget().blockSignals(True)
		self._item.setText(self._column, self.currentText())
		self._item.treeWidget().blockSignals(False)
	def _textChanged(self, text):
		self._item.treeWidget().blockSignals(True)
		self._item.setText(self._column, text)
		self._item.treeWidget().blockSignals(False)
	def column(self):
		return self._column

class CustomTreeWidgetItem(QtGui.QTreeWidgetItem):
	def __init__(self, *args, **kw):
		super(CustomTreeWidgetItem, self).__init__(*args, **kw)
		self._itemWidgets = {}

	def setCheckBoxToColumn(self, idxColumn):
		checkBoxItemWidget = CheckBoxTreeItem(self, idxColumn)
		self.treeWidget().setItemWidget(self, idxColumn, checkBoxItemWidget)
		return checkBoxItemWidget.checkBox()
	def checkBox(self, idxColumn):
		return self.treeWidget().itemWidget(self, idxColumn).checkBox()

	def setComboBoxToColumn(self, idxColumn):
		comboBox = ComboBoxTreeItem(self, idxColumn)	
		self.treeWidget().setItemWidget(self, idxColumn, comboBox)
	def comboBox(self, idxColumn):
		return self.treeWidget().itemWidget(self, idxColumn)

	def iterParents(self):
		ret = [self.parent()]
		if ret[-1]:
			ret.extend(ret[-1].iterParents())
		return ret
	def iterChildren(self):
		ret = []
		for i in range(self.childCount()):
			child = self.child(i)
			ret.append(child)
			if child.childCount():
				ret.extend(child.iterChildren())
		return ret

	def removeAllChildren(self):
		for i in range(self.childCount()):
			self.removeChild(self.child(0))


class BmViewItem(CustomTreeWidgetItem):
	#
	def __init__(self, parent, itemType, category):
		super(BmViewItem, self).__init__(parent)
		if not itemType in BMVW_ITEM_TYPES:
			raise Exception("%s - неверный тип элемента дерева" % itemType)
		#
		self._font = QtGui.QFont()
		self._itemType = itemType
		self._category = category
		#
		self._key = ()					# список ключевых значений, по которым произошла группировка
		self._reportData = []
		#
		self._volume = None
		self._tonnage = None
		#
		self._buildOptionColumns()
		self._setSummaryColumnProperties()

	def _formatSummary(self, number, decimals = 1):
		number = round(number, decimals)
		return "{:,}".format(number).replace(",", " ")
	def _buildOptionColumns(self):
		includeInReportColumnIndex = self.treeWidget().includeInReportColumnIndex()
		restoreMiningWorkColumnIndex = self.treeWidget().restoreMiningWorkColumnIndex()
		restoreGeologyColumnIndex = self.treeWidget().restoreGeologyColumnIndex()
		simulateConcreteColumnIndex = self.treeWidget().simulateConcreteColumnIndex()


		includeInReportCheckBox = self.setCheckBoxToColumn(includeInReportColumnIndex)
		restoreMiningCheckBox = self.setCheckBoxToColumn(restoreMiningWorkColumnIndex)
		restoreMiningCheckBox.setDisabled(True)
		restoreGeologyCheckBox = self.setCheckBoxToColumn(restoreGeologyColumnIndex)
		restoreGeologyCheckBox.setDisabled(True)
		simulateConcreteCheckBox = self.setCheckBoxToColumn(simulateConcreteColumnIndex)
		simulateConcreteCheckBox.setDisabled(True)

		includeInReportCheckBox.stateChanged.connect(lambda: self.optionCheckBoxStateChanged(includeInReportColumnIndex))
		restoreMiningCheckBox.stateChanged.connect(lambda: self.optionCheckBoxStateChanged(restoreMiningWorkColumnIndex))
		restoreGeologyCheckBox.stateChanged.connect(lambda: self.optionCheckBoxStateChanged(restoreGeologyColumnIndex))
		simulateConcreteCheckBox.stateChanged.connect(lambda: self.optionCheckBoxStateChanged(simulateConcreteColumnIndex))


		dialog = iterWidgetParents(self.treeWidget())[-3]
		if self.category() not in dialog.parent().getMineWorkCategories():
			restoreGeologyCheckBox.setVisible(False)

		if not self.category() == dialog.parent().getBackfillCategory():
			restoreMiningCheckBox.setVisible(False)

		if (self.category() not in dialog.parent().getMineWorkCategories()) or self.category() == dialog.parent().getBackfillCategory():
			simulateConcreteCheckBox.setVisible(False)

	def _setSummaryColumnProperties(self):
		for summaryColumnIndex in self.treeWidget().summaryColumnIndexes():
			self.setTextAlignment(summaryColumnIndex, QtCore.Qt.AlignRight)
	#
	def isPurposeItem(self):		return self._itemType == BMVW_ITEM_PURPOSE
	def isMineWorkItem(self):		return self._itemType == BMVW_ITEM_MINEWORK
	def isMaterialItem(self):		return self._itemType == BMVW_ITEM_MATERIAL
	def isSummaryItem(self):		return self._itemType == BMVW_ITEM_SUMMARY
	#
	def itemType(self):				return self._itemType
	#
	def isIncluded(self):
		includeInReportColumnIndex = self.treeWidget().includeInReportColumnIndex()
		return self.checkBox(includeInReportColumnIndex).isChecked()
	def setIsIncluded(self, flag):
		includeInReportColumnIndex = self.treeWidget().includeInReportColumnIndex()
		checkBox = self.checkBox(includeInReportColumnIndex)
		checkBox.setChecked(bool(flag))
	def isExcluded(self):
		includeInReportColumnIndex = self.treeWidget().includeInReportColumnIndex()
		return not self.checkBox(includeInReportColumnIndex).isChecked()
	def isRestoreMiningWork(self):
		restoreMiningWorkColumnIndex = self.treeWidget().restoreMiningWorkColumnIndex()
		return self.checkBox(restoreMiningWorkColumnIndex).isChecked()
	def isRestoreGeology(self):
		restoreGeologyColumnIndex = self.treeWidget().restoreGeologyColumnIndex()
		return self.checkBox(restoreGeologyColumnIndex).isChecked()
	def isSimulateConcrete(self):
		simulateConcretColumnIndex = self.treeWidget().simulateConcreteColumnIndex()	  
		return self.checkBox(simulateConcretColumnIndex).isChecked()

	def isGeology(self):
		tree = self.treeWidget()
		dialog = iterWidgetParents(tree)[-3]
		idxCurrDate = self.treeWidget().reportHeader().index(self.treeWidget().currDateField())
		ret = all(map(lambda row: row[idxCurrDate] == ROCK_AND_ORE, self._reportData))
		return ret
	def isBackfill(self):
		tree = self.treeWidget()
		dialog = iterWidgetParents(tree)[-3]
		backfillCategory = dialog.parent().getBackfillCategory()
		idxCurrDate = self.treeWidget().reportHeader().index(self.treeWidget().currDateField())
		ret = all(map(lambda row: row[idxCurrDate] == backfillCategory, self._reportData))
		return ret

	#
	def category(self):
		return self._category
	
	def _updateKey(self):
		return
	#
	def _populate(self, report):
		self.removeAllChildren()

	def setText(self, iColumn, text):
		super(BmViewItem, self).setText(iColumn, text)
		self.setFont(iColumn, self._font)

	def setReport(self, key, report):
		self._key = deepcopy(key)
		self._reportData = deepcopy(report)
		self._updateKey()
		self._populate(report)
		#
		self.updateSummary()

	def volume(self):
		tree = self.treeWidget()
		childCount = self.childCount()
		vol = 0
		if childCount == 0:
			idxVolume = self.treeWidget().reportHeader().index(RPTFLD_VOLUME)
			vol = sum(row[idxVolume] for row in self._reportData)
		else:
			if self.isPurposeItem():
				for materialItem in filter(lambda it: it.isMaterialItem(), self.iterChildren()):
					childCheckBox = materialItem.checkBox(tree.includeInReportColumnIndex())
					isChecked = childCheckBox.checkState() in [QtCore.Qt.Checked]
					if isChecked:
						vol += materialItem.volume()
			else:
				vol = sum(self.child(i).volume() for i in range(childCount))
		return vol
	def tonnage(self):
		tree = self.treeWidget()
		childCount = self.childCount()
		if childCount == 0:
			restoreTonnage = self.checkBox(tree.restoreMiningWorkColumnIndex()).isChecked() or self.checkBox(tree.restoreGeologyColumnIndex()).isChecked()
			checkBox = self.checkBox(tree.restoreMiningWorkColumnIndex())
			if restoreTonnage:	idxTonnage = tree.reportHeader().index(RPTFLD_MINE_TONNAGE)
			else:				idxTonnage = tree.reportHeader().index(RPTFLD_FACT_TONNAGE)
			#
			ton = sum(row[idxTonnage] for row in self._reportData)
		else:
			ton = 0
			if self.isPurposeItem():
				for materialItem in filter(lambda it: it.isMaterialItem(), self.iterChildren()):
					childCheckBox = materialItem.checkBox(tree.includeInReportColumnIndex())
					isChecked = childCheckBox.checkState() in [QtCore.Qt.Checked]
					if isChecked:
						ton += materialItem.tonnage()
			else:
				ton = sum(self.child(i).tonnage() for i in range(childCount))

		return ton

	def updateVolume(self):
		volume = self.volume()
		fmtValue = self._formatSummary(volume)
		self.setText(self.treeWidget().volumeColumnIndex(), fmtValue)
	def updateTonnage(self):
		tonnage = self.tonnage()
		fmtValue = self._formatSummary(tonnage)
		self.setText(self.treeWidget().tonnageColumnIndex(), fmtValue)
	def updateSummary(self):
		self.updateVolume()
		self.updateTonnage()

	def reportData(self):
		childCount = self.childCount()
		ret = []
		if childCount == 0:		ret = deepcopy(self._reportData)
		else:
			for i in range(childCount):
				ret.extend(self.child(i).reportData())
		return ret

	def setDefaultCheckBoxStates(self, column):
		tree = self.treeWidget()
		checkBox = self.checkBox(column)
		isChecked = checkBox.checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]

		checkBox.blockSignals(True)
		checkBox.setChecked(isChecked)
		checkBox.blockSignals(False)

		# enable/disable other checkboxes for current item
		# if column == tree.includeInReportColumnIndex():
		includeCheckBox = self.checkBox(tree.optionColumnIndexes()[0])
		isIncluded = includeCheckBox.isChecked()
		restoreMiningWorkCheckBox = self.checkBox(tree.optionColumnIndexes()[1])
		restoreGeologyCheckBox = self.checkBox(tree.optionColumnIndexes()[2])
		simulateConcreteCheckBox = self.checkBox(tree.optionColumnIndexes()[3])
		restoreMiningWorkCheckBox.setDisabled(not isIncluded)
		restoreGeologyCheckBox.setDisabled(not isIncluded)
		simulateConcreteCheckBox.setDisabled(not isIncluded)
		if isIncluded:
			restoreMiningWorkCheckBox.setDisabled(restoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
			if restoreGeologyCheckBox.checkState() == QtCore.Qt.Checked and restoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked:
				restoreMiningWorkCheckBox.setCheckState(QtCore.Qt.Unchecked)
			#
			restoreGeologyCheckBox.setDisabled(restoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked)
			if restoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and restoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
				restoreGeologyCheckBox.setCheckState(QtCore.Qt.Unchecked)

			simulateConcreteCheckBox.setDisabled(restoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked 
												or restoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
			if (restoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked or restoreGeologyCheckBox.checkState() == QtCore.Qt.Checked) and simulateConcreteCheckBox.checkState() == QtCore.Qt.Checked:
				simulateConcreteCheckBox.setCheckState(QtCore.Qt.Unchecked)

		# setting child checkbox state
		for childItem in self.iterChildren():
			childCheckBox = childItem.checkBox(column)
			#
			childCheckBox.blockSignals(True)
			childCheckBox.setChecked(isChecked)
			childCheckBox.blockSignals(False)
			# enable/disable other checkboxes for child item
			isChildIncluded = childItem.checkBox(tree.includeInReportColumnIndex()).isChecked()
			isChildChecked = childCheckBox.checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]
			childRestoreMiningWorkCheckBox = childItem.checkBox(tree.restoreMiningWorkColumnIndex())
			childRestoreGeologyCheckBox = childItem.checkBox(tree.restoreGeologyColumnIndex())
			childSimulateConcreteCheckBox = childItem.checkBox(tree.simulateConcreteColumnIndex())

			if column == tree.includeInReportColumnIndex():
				childRestoreMiningWorkCheckBox.setDisabled(not isChildChecked or childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
				if childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					childRestoreMiningWorkCheckBox.setCheckState(QtCore.Qt.Unchecked)
				#
				childRestoreGeologyCheckBox.setDisabled(not isChildChecked or childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked)
				if childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					childRestoreGeologyCheckBox.setCheckState(QtCore.Qt.Unchecked)
				#
				childSimulateConcreteCheckBox.setDisabled(not isChildChecked or childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked or childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
				if (childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked or childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked) and childSimulateConcreteCheckBox.checkState() == QtCore.Qt.Checked:
					childSimulateConcreteCheckBox.setCheckState(QtCore.Qt.Unchecked)
			elif column == tree.restoreMiningWorkColumnIndex() and isChildIncluded:
				childRestoreGeologyCheckBox.setDisabled(childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked)
				if childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					childRestoreGeologyCheckBox.setCheckState(QtCore.Qt.Unchecked)
			elif column == tree.restoreGeologyColumnIndex() and isChildIncluded:
				childRestoreMiningWorkCheckBox.setDisabled(childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
				if childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					childRestoreMiningWorkCheckBox.setCheckState(QtCore.Qt.Unchecked)
			elif column == tree.simulateConcreteColumnIndex() and isChildIncluded:
				childRestoreMiningWorkCheckBox.setDisabled(childSimulateConcreteCheckBox.checkState() == QtCore.Qt.Checked)
				childRestoreGeologyCheckBox.setDisabled(childSimulateConcreteCheckBox.checkState() == QtCore.Qt.Checked)
				if (childRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked or childRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked) and childSimulateConcreteCheckBox.checkState() == QtCore.Qt.Checked:
					childSimulateConcreteCheckBox.setCheckState(QtCore.Qt.Unchecked)

		# setting parent checkbox state
		for parentItem in self.iterParents()[:-1]:
			parentCheckBox = parentItem.checkBox(column)
			#
			childrenAreChecked = [it.checkBox(column).isChecked() for it in parentItem.iterChildren()]
			parentCheckBox.blockSignals(True)
			if all(childrenAreChecked):		parentCheckBox.setCheckState(QtCore.Qt.Checked)
			elif any(childrenAreChecked):	parentCheckBox.setCheckState(QtCore.Qt.PartiallyChecked)
			else:							parentCheckBox.setCheckState(QtCore.Qt.Unchecked)
			parentCheckBox.blockSignals(False)
			# enable/disable other checkboxes for parent item
			isParentIncluded = parentItem.checkBox(tree.includeInReportColumnIndex()).isChecked()
			isParentChecked = parentCheckBox.checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]
			parentRestoreMiningWorkCheckBox = parentItem.checkBox(tree.optionColumnIndexes()[1])
			parentRestoreGeologyCheckBox = parentItem.checkBox(tree.optionColumnIndexes()[2])
			parentSimulateConcreteCheckBox = parentItem.checkBox(tree.optionColumnIndexes()[3])
			if column == tree.includeInReportColumnIndex():
				parentRestoreMiningWorkCheckBox.setDisabled(not isParentChecked or parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
				if parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					parentRestoreMiningWorkCheckBox.setCheckState(QtCore.Qt.Unchecked)
				#
				parentRestoreGeologyCheckBox.setDisabled(not isParentChecked or parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked)
				if parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					parentRestoreGeologyCheckBox.setCheckState(QtCore.Qt.Unchecked)
				#
				parentSimulateConcreteCheckBox.setDisabled(not isParentChecked or parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked
															or  parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
				if (parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked
					or parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)  and parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					parentRestoreGeologyCheckBox.setCheckState(QtCore.Qt.Unchecked)
			elif column == tree.restoreMiningWorkColumnIndex():
				# parentRestoreMiningWorkCheckBox.setDisabled(not isParentIncluded and not isParentChecked)
				parentRestoreGeologyCheckBox.setDisabled(not isParentIncluded or parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked)
				if parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					parentRestoreGeologyCheckBox.setCheckState(QtCore.Qt.Unchecked)
			elif column == tree.restoreGeologyColumnIndex():
				# parentRestoreGeologyCheckBox.setDisabled(not isParentIncluded and not isParentChecked)
				parentRestoreMiningWorkCheckBox.setDisabled(not isParentIncluded or parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked)
				if parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked and parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked:
					parentRestoreMiningWorkCheckBox.setCheckState(QtCore.Qt.Unchecked)
			elif column == tree.simulateConcreteColumnIndex():
				# parentRestoreGeologyCheckBox.setDisabled(not isParentIncluded and not isParentChecked)
				parentRestoreMiningWorkCheckBox.setDisabled(not isParentIncluded or parentSimulateConcreteCheckBox.checkState() == QtCore.Qt.Checked)
				parentRestoreGeologyCheckBox.setDisabled(not isParentIncluded or parentSimulateConcreteCheckBox.checkState() == QtCore.Qt.Checked)
				if (parentRestoreMiningWorkCheckBox.checkState() == QtCore.Qt.Checked 
					or parentRestoreGeologyCheckBox.checkState() == QtCore.Qt.Checked) and parentSimulateConcreteCheckBox.checkState() == QtCore.Qt.Checked:
					parentSimulateConcreteCheckBox.setCheckState(QtCore.Qt.Unchecked)
		# updating purpose summary
		purposeItem = self if self.isPurposeItem() else self.iterParents()[-2]
		purposeItem.updateSummary()
		# updating total summary
		summaryItem = tree.summaryItem()
		summaryItem.updateSummary()
	#
	def setSelectedCheckBoxStates(self, column):
		tree = self.treeWidget()
		selectedItems = tree.selectedItems()
		#
		isChecked = self.checkBox(column).checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]
		itemType = self.itemType()
		#
		items = list(filter(lambda it: it.itemType() == itemType, selectedItems))
		for item in items:
			checkBox = item.checkBox(column)
			#
			checkBox.blockSignals(True)
			checkBox.setChecked(isChecked)
			checkBox.blockSignals(False)
			#
			item.setDefaultCheckBoxStates(column)

	def optionCheckBoxStateChanged(self, column):
		tree = self.treeWidget()
		selectedItems = tree.selectedItems()
		#
		if len(selectedItems) == 1:		self.setDefaultCheckBoxStates(column)
		else:							self.setSelectedCheckBoxStates(column)

class PurposeTreeWidgetItem(BmViewItem):
	def __init__(self, parent, category):
		self._category = category
		super(PurposeTreeWidgetItem, self).__init__(parent, BMVW_ITEM_PURPOSE, category)
		self._font.setBold(True)
	def setReport(self, key, report):
		self._category = key
		super(PurposeTreeWidgetItem, self).setReport(key, report)
	def _populate(self, report):
		super(PurposeTreeWidgetItem, self)._populate(report)
		dialog = iterWidgetParents(self.treeWidget())[-3]
		#
		keyIndexes = tuple(self.treeWidget().reportHeader().index(field) for field in self.groupFields())
		for key, values in groupby(report, lambda x: tuple(x[keyIndex] for keyIndex in keyIndexes)):
			values = list(values)
			#
			if self.category() in dialog.parent().getMineWorkCategories():
				mineWorkItem = MineWorkTreeWidgetItem(self, self.category())
				mineWorkItem.setReport(key, values)
			else:
				mineWorkGroupFields = self.treeWidget().mineWorkGroupFields()
				keyIndexes = tuple(self.treeWidget().reportHeader().index(field) for field in mineWorkGroupFields)
				for materialKey, materialValues in groupby(values, lambda x: tuple(x[keyIndex] for keyIndex in keyIndexes)):
					materialItem = MaterialTreeWidgetItem(self, self.category())
					materialItem.setReport(materialKey, tuple(materialValues))
		self.setExpanded(True)
	def _updateKey(self):
		category = self._key
		purpose = self.treeWidget().catPurposeDict().get(category)
		displayKey = purpose if purpose else category
		self.setText(0, displayKey)
	def groupFields(self):
		return self.treeWidget().purposeGroupFields()
		
class MineWorkTreeWidgetItem(BmViewItem):
	def __init__(self, parent, category):
		super(MineWorkTreeWidgetItem, self).__init__(parent, BMVW_ITEM_MINEWORK, category)
	
	def category(self):
		return self.parent().category()
	def _populate(self, report):
		super(MineWorkTreeWidgetItem, self)._populate(report)
		#
		keyIndexes = tuple(self.treeWidget().reportHeader().index(field) for field in self.groupFields())
		for key, values in groupby(report, lambda x: tuple(x[keyIndex] for keyIndex in keyIndexes)):
			materialItem = MaterialTreeWidgetItem(self, self.category())
			materialItem.setReport(key, tuple(values))
	def _updateKey(self):
		dialog = iterWidgetParents(self.treeWidget())[-3]
		#
		idxCategory = self.treeWidget().reportHeader().index(self.treeWidget().currDateField())
		idxFactWork = self.treeWidget().reportHeader().index(BMFLD_FACTWORK)

		category, code = self._key[0], self._key[-1]
		codeDescription = self.treeWidget().catCodeDescriptionDict().get((category, code))
		displayCodeDescription = codeDescription if codeDescription else code
		displayKeys = tuple(self._key)[1:5] + (displayCodeDescription,)
		#
		for i, value in enumerate(displayKeys):
			if i == 3 and self.category() != dialog.parent().getBackfillCategory():
				continue
			self.setText(i, value)
	def groupFields(self):
		return self.treeWidget().mineWorkGroupFields()

class MaterialTreeWidgetItem(BmViewItem):
	def __init__(self, parent, category):
		super(MaterialTreeWidgetItem, self).__init__(parent, BMVW_ITEM_MATERIAL, category)
		
		#
		self._font.setItalic(True)
	def category(self):
		return self.parent().category()
	def key(self):
		tree = self.treeWidget()
		columnIndexes = [tree.reportHeader().index(columnName) for columnName in tree.mineWorkGroupFields()]
		return tuple(self._reportData[0][columnIndex] for columnIndex in columnIndexes)
	def isIncluded(self):
		checkBox = self.checkBox(self.treeWidget().includeInReportColumnIndex())
		return checkBox.checkState() == QtCore.Qt.Checked
	def isExcluded(self):
		checkBox = self.checkBox(self.treeWidget().includeInReportColumnIndex())
		return checkBox.checkState() == QtCore.Qt.Unchecked
	def setText(self, iColumn, text):
		super(BmViewItem, self).setText(iColumn, text)
		if iColumn == self.treeWidget().volumeColumnIndex()-1:
			self.setTextAlignment(iColumn, QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.setFont(iColumn, self._font)
	def setReport(self, key, report):
		dialog = iterWidgetParents(self.treeWidget())[-3]
		# заменяем Руда/Порода на исходную категорию
		idxCategory = self.treeWidget().reportHeader().index(self.treeWidget().currDateField())
		idxMaterial = self.treeWidget().reportHeader().index(BMFLD_MATERIAL)
		report = [list(row) for row in report]
		for i, row in enumerate(report):
			material = row[idxMaterial]
			category = row[idxCategory]
			if not category in dialog.parent().getMineWorkCategories() and category != material:
				report[i][idxCategory] = material
		#
		super(MaterialTreeWidgetItem, self).setReport(key, report)
	def _updateKey(self):
		material = self._key[-1]
		materialDescription = self.treeWidget().materialDescriptionDict().get(material)
		displayKey = materialDescription if materialDescription else material
		self.setText(len(self._key)-3, displayKey)

class SummaryTreeWidgetItem(BmViewItem):
	def __init__(self, parent):
		super(SummaryTreeWidgetItem, self).__init__(parent, BMVW_ITEM_SUMMARY, None)
		self._font.setBold(True)
		self._font.setItalic(True)
	
		tree = self.treeWidget()
		for optionColumnIndex in tree.optionColumnIndexes():
			self.checkBox(optionColumnIndex).setVisible(False)
		#
		self.setTextAlignment(tree.volumeColumnIndex()-1, QtCore.Qt.AlignRight)
		self.setText(tree.volumeColumnIndex()-1, "Итого")
		self.updateSummary()

	def updateVolume(self):
		tree = self.treeWidget()
		volume = sum(purposeItem.volume() for purposeItem in filter(lambda it: it.isPurposeItem(), tree.iterChildren()))
		fmtValue = self._formatSummary(volume)
		self.setText(self.treeWidget().volumeColumnIndex(), fmtValue)

	def updateTonnage(self):
		tree = self.treeWidget()
		tonnage = sum(purposeItem.tonnage() for purposeItem in filter(lambda it: it.isPurposeItem(), tree.iterChildren()))
		fmtValue = self._formatSummary(tonnage)
		self.setText(self.treeWidget().tonnageColumnIndex(), fmtValue)
#

def bmDataPreviewReport(bmData, bmHeader, fieldCurrDate, concDensity, elemFields, elemUnits, geoCodes, svyCatsAndCodes):
	if len(elemFields) != len(elemUnits):
		raise Exception("Указаны разное количество элементов и единиц измерений")
	#
	materialIndexDensityDict = {}
	for (material, materialDesc, materialType), indexDict in geoCodes: 
		for bmIndex, indexDensity, indexDensityWaste in list(zip(*indexDict["BM"]))[0]:
			try:		density = float(indexDensity)
			except:		density = math.nan
			materialIndexDensityDict[(material, bmIndex)] = density
	materialIndexDensityDict[(svyCatsAndCodes[6][0][1], svyCatsAndCodes[6][1][0][0])] = concDensity
	materialIndexDensityDict[(svyCatsAndCodes[6][0][1], svyCatsAndCodes[6][1][0][1])] = concDensity

	# процент: elemUnit = 0
	# г/т: elemUnit = 1
	coeff = None
	elemCoeffDict = {0: 100, 1: 1000}
	summFields = (RPTFLD_VOLUME, RPTFLD_FACT_TONNAGE, RPTFLD_MINE_TONNAGE, RPTFLD_FACT_DENSITY)
	groupFields = (fieldCurrDate, BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL, BMFLD_MINEDATE, BMFLD_FACTDATE, BMFLD_FACTWORK, BMFLD_MATERIAL, BMFLD_INDEX)
	elemCoeffs = [elemCoeffDict.get(elemUnit) for elemUnit in elemUnits]
	elemCoeffUnknown = [elemCoeff is not None for elemCoeff in elemCoeffs]
	if not all(elemCoeffs):
		raise Exception("Неизвестная единица измерения '%s'" % elemFields[elemCoeffUnknown.index(False)])

	#
	blockFldX, blockFldY, blockFldZ = "_EAST", "_NORTH", "_RL"
	idxBlockFldX, idxBlockFldY, idxBlockFldZ  = bmHeader.index(blockFldX), bmHeader.index(blockFldY), bmHeader.index(blockFldZ)

	# индекс поля признака динамической плотности
	idxIsUseDynamicDensity = bmHeader.index('isUseDynamicDensity')


	# индекс поля плотности
	idxDensity = bmHeader.index(BMFLD_DENSITY)
	idxGeoMaterial = bmHeader.index(BMFLD_GEOMATERIAL)
	idxGeoIndex = bmHeader.index(BMFLD_GEOINDEX)

	# проверка на наличие полей
	allFields = groupFields + elemFields
	allFieldsInHeader = list(map(lambda field: field in bmHeader, allFields))
	if not allFieldsInHeader:
		raise Exception("Поле '%s' отсутствует в блочной модели" % allFields[allFieldsInHeader.index(False)])

	# индексы полей
	groupIndexes = [bmHeader.index(field) for field in groupFields]
	elemIndexes = [bmHeader.index(field) for field in elemFields]
	#

	# elements кортеж хим элементов из справочника геоиндексов (для рассчёта динамической плотности)
	elements = []
	materials_indexes = list(zip(*geoCodes))[1]
	materials_bm_indexes = [i['BM'] for i in materials_indexes]
			
	for material_bm_indexes in materials_bm_indexes:
		for bm_index in material_bm_indexes:
			for factor in bm_index[1]['FACTOR']:
				if factor[0] in elemFields:
					elements.append(factor[0])
	elements = set(elements)

	# Словарь индексов граф с химическими элементами в блочной модели
	idxElements = {}
	for element in elements:
		idx = bmHeader.index(element)
		idxElements[element] = idx

	# расчет объемов/тоннажа
	dictionary = {}
	for k, row in enumerate(bmData):
		density = 0 if math.isnan(row[idxDensity]) else row[idxDensity] 
		#
		isUseDynamicDensity = row[idxIsUseDynamicDensity]
		if isUseDynamicDensity:

			geoDensity = getDynamicDensity(bmHeader, row, geoCodes, idxElements, idxGeoMaterial, idxGeoIndex)
		else:
			geoDensity = materialIndexDensityDict.get((row[idxGeoMaterial], row[idxGeoIndex]))
		geoDensity = 0 if geoDensity is None else geoDensity
		#
		volume = row[idxBlockFldX]*row[idxBlockFldY]*row[idxBlockFldZ]
		tonnage = density*volume
		mineTonnage = geoDensity*volume
		#
		elemTonnage = [tonnage*row[elemIndex] / elemCoeffs[i] if not math.isnan(row[elemIndex]) else 0 for i, elemIndex in enumerate(elemIndexes)]
		#
		key = tuple(row[index] for index in groupIndexes)
		if not key in dictionary:
			dictionary[key] = [volume, tonnage, mineTonnage] + elemTonnage
		else:
			for iElem, value in enumerate([volume, tonnage, mineTonnage] + elemTonnage):
				dictionary[key][iElem] += value
	#
	# переход к содержаниям
	for key, values in dictionary.items():
		volume, tonnage, mineTonnage, elemTonnages = values[0], values[1], values[2], values[3:]
		density = tonnage / volume if volume != 0 else 0
		#
		if tonnage == 0:		elemGrades = tuple(0 for i, elemTonnage in enumerate(elemTonnages))
		else:					elemGrades = tuple(elemCoeffs[i]*elemTonnage / tonnage for i, elemTonnage in enumerate(elemTonnages))
		dictionary[key] = (volume, tonnage, mineTonnage, density) + elemGrades
	#
	rptData = [list(key) + list(dictionary[key]) for key in sorted(dictionary)]
	rptHeader = groupFields + summFields + elemFields
	return rptData, rptHeader

class BlockModelView(TreeWidget):
	def __init__(self, parent):
		super(BlockModelView, self).__init__(parent)

		self.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.setSortingEnabled(True)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()

		self._buildHeader()

	def __createVariables(self):
		self._volumeColumnIndex, self._tonnageColumnIndex, self._densityColumnIndex = 5, 6, 7
		self._includeInReportColumnIndex, self._restoreMiningWorkColumnIndex, self._restoreGeologyColumnIndex, self._simulateConcreteColumnIndex = 8, 9, 10, 11
		#
		self._catPurposeDict = {}
		self._catCodeDescriptionDict = {}
		self._materialDescriptionDict = {}
		self._materialIndexDensityDict = {}
		#
		self._currDateField = None
		self._summaryItem = None
		#
		self._originReport = []
		self._previewReport = []
		self._reportHeader = []
		self._elementHeader = []
		self._elementUnits = []
	def __createWidgets(self):
		pass

	def __createBindings(self):
		pass

	def selectedReportData(self):
		dialog = iterWidgetParents(self)[-3]
		#
		selectedItems = self.selectedItems()
		if not selectedItems:
			return []
		# выводим сводную информацию только по однотипным строкам
		materialItems = []
		for selectedItem in filter(lambda it: it.category() != dialog.parent().getBackfillCategory(), selectedItems):
			if selectedItem.isMaterialItem():
				materialItems.append(selectedItem)
			else:
				materialItems.extend(list(filter(lambda it: it.isMaterialItem() and it not in materialItems, selectedItem.iterChildren())))
		#
		idxMaterial = self.reportHeader().index(BMFLD_MATERIAL)
		reportData = []
		for materialItem in materialItems:
			reportData.extend(list(filter(lambda row: row[idxMaterial] != dialog.parent().getBackfillCategory(), materialItem.reportData())))
		return reportData

	def includedItems(self):
		return tuple(filter(lambda item: item.isMaterialItem() and item.isIncluded(), self.iterChildren()))
	def restoredMiningWorkItems(self):
		return tuple(filter(lambda item: item.isMaterialItem() and item.isIncluded() and item.isRestoreMiningWork(), self.iterChildren()))
	def restoredGeologyItems(self):
		return tuple(filter(lambda item: item.isMaterialItem() and item.isIncluded() and item.isRestoreGeology(), self.iterChildren()))
	def simulateConcreteItems(self):
		return tuple(filter(lambda item: item.isMaterialItem() and item.isIncluded() and item.isSimulateConcrete(), self.iterChildren()))
				 
	def includeInReportAll(self):
		for item in filter(lambda it: it.isPurposeItem(), self.iterChildren()):
			item.checkBox(self.includeInReportColumnIndex()).setChecked(True)

	def gradeReport(self, reportData):
		idxIndex = self.reportHeader().index(BMFLD_INDEX)
		idxVolume = self.reportHeader().index(RPTFLD_VOLUME)
		idxTonnage = self.reportHeader().index(RPTFLD_FACT_TONNAGE)
		#
		reportData.sort(key = lambda x: x[idxIndex])


		elemUnits = self.elementUnits()
		elemIndexes = [self.reportHeader().index(element) for element in self.elementHeader()]
		elemCoeffDict = {0: 100, 1: 1000}
		elemCoeffs = [elemCoeffDict.get(elemUnit) for elemUnit in elemUnits]

		dictionary = {}
		for row in reportData:
			volume = row[idxVolume]
			tonnage = row[idxTonnage]

			elemTonnage = [tonnage*row[elemIndex] / elemCoeffs[i] if not math.isnan(row[elemIndex]) else 0 for i, elemIndex in enumerate(elemIndexes)]
			#
			key = (row[idxIndex],)
			if not key in dictionary:
				dictionary[key] = [tonnage] + elemTonnage
			else:
				for iElem, value in enumerate([tonnage] + elemTonnage):
					dictionary[key][iElem] += value
		#
		# переход к содержаниям
		for key, values in dictionary.items():
			tonnage, elemTonnages = values[0], values[1:]
			#
			if tonnage == 0:		elemGrades = tuple(0 for i, elemTonnage in enumerate(elemTonnages))
			else:					elemGrades = tuple(elemCoeffs[i]*elemTonnage / tonnage for i, elemTonnage in enumerate(elemTonnages))
			dictionary[key] = (tonnage,) + elemGrades

		report = [list(key) + list(dictionary[key])[1:] for key in sorted(dictionary)]
		return report

	def setMaterialDescriptionDict(self, d):	self._materialDescriptionDict = deepcopy(d)
	def materialDescriptionDict(self):			return self._materialDescriptionDict
	#
	def setMaterialIndexDensityDict(self, d):	self._materialIndexDensityDict = deepcopy(d)
	def materialIndexDensityDict(self):			return self._materialIndexDensityDict
	#
	def setCatPurposeDict(self, d):				self._catPurposeDict = deepcopy(d)
	def catPurposeDict(self):					return self._catPurposeDict
	#
	def setCatCodeDescriptionDict(self, d):		self._catCodeDescriptionDict = deepcopy(d)
	def catCodeDescriptionDict(self):			return self._catCodeDescriptionDict

	#
	def volumeColumnIndex(self):				return self._volumeColumnIndex
	def tonnageColumnIndex(self):				return self._tonnageColumnIndex
	def densityColumnIndex(self):				return self._densityColumnIndex

	def includeInReportColumnIndex(self):		return self._includeInReportColumnIndex
	def restoreMiningWorkColumnIndex(self):		return self._restoreMiningWorkColumnIndex
	def restoreGeologyColumnIndex(self):		return self._restoreGeologyColumnIndex
	def simulateConcreteColumnIndex(self):		return self._simulateConcreteColumnIndex
	

	def summaryColumnIndexes(self):				return (self.volumeColumnIndex(), self.tonnageColumnIndex(), self.densityColumnIndex())
	def optionColumnIndexes(self):				return (self._includeInReportColumnIndex, self._restoreMiningWorkColumnIndex, self._restoreGeologyColumnIndex
														, self._simulateConcreteColumnIndex)

	def currDateField(self):					return self._currDateField

	def purposeGroupFields(self):				return (self.currDateField(), BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL, BMFLD_MINEDATE, BMFLD_FACTDATE, BMFLD_FACTWORK)
	def mineWorkGroupFields(self):				return self.purposeGroupFields() + (BMFLD_MATERIAL,)
	def materialGroupFields(self):				return self.mineWorkGroupFields() + (BMFLD_INDEX,)

	def _buildHeader(self):
		headerItem = self.headerItem()
		columnNames = ("Выработка", "Интервал", "Дата выемки", "Дата закладки", "Вид работ", "Объем, м3", "Тоннаж, т", "Плотность",
						"Включить\nв отчет", "Восстановить\nвыемку", "Восстановить\nгеологию", "Проектный\nбетон")
		columnWidths = (220, 130, 90, 90, 170, 80, 80, 80, 85, 85, 85, 85)
		for idxColumn, (columnName, columnWidth) in enumerate(zip(columnNames, columnWidths)):
			headerItem.setText(idxColumn, columnName)
			headerItem.setTextAlignment(idxColumn, QtCore.Qt.AlignCenter)
			self.setColumnWidth(idxColumn, columnWidth)
		self.hideDensityColumn()

	def reportHeader(self):						return self._reportHeader
	def elementHeader(self):					return self._elementHeader
	def elementUnits(self):						return self._elementUnits

	def setReport(self, rptData, rptHeader, elemFields, elemUnits, currDateField = None):
		if not all((self._catPurposeDict, self._catCodeDescriptionDict, self._materialDescriptionDict, self._materialIndexDensityDict)):
			raise Exception("Не заданы словари для расшифровки кодов")
		if not currDateField in rptHeader:
			raise Exception("Поле '%s' отсутствует" % str(currDateField))

		self._originReport = deepcopy(rptData)
		self._previewReport = deepcopy(rptData)
		self._reportHeader = deepcopy(rptHeader)
		self._elementHeader = deepcopy(elemFields)
		self._elementUnits = deepcopy(elemUnits)

		self._currDateField = currDateField

		self._prepare()
		self._populate()

	def _prepare(self):
		# сортируем по назначению
		dialog = self.parent().parent().parent()
		idxCurrDate = self.reportHeader().index(self.currDateField())
		for i, row in enumerate(self._previewReport):
			category = row[idxCurrDate]
			if not category in dialog.parent().getMineWorkCategories():
				self._previewReport[i][idxCurrDate] = self.catPurposeDict()[category]

		groupFields = [self.currDateField(), BMFLD_MINE_MINENAME, BMFLD_MINEDATE, BMFLD_MINE_INTERVAL, BMFLD_FACTWORK, BMFLD_MATERIAL]
		sortIndexes = [self.reportHeader().index(field) for field in groupFields]
		self._previewReport.sort(key = lambda x: tuple(x[index] for index in sortIndexes), reverse = True)
		
	def _populate(self):
		# вставляем отчет
		self.blockSignals(True)
		self.clear()
		#
		for purposeGroup, purposeGroupValues in groupby(self._previewReport, key = lambda x: x[0]):
			purposeGroupValues = tuple(purposeGroupValues)
			#
			purposeItem = PurposeTreeWidgetItem(self, purposeGroup)
			purposeItem.setReport(purposeGroup, purposeGroupValues)
		self._summaryItem = SummaryTreeWidgetItem(self)
		#
		self.blockSignals(False)

	def _includeIntoReportStateChanged(self, item):
		isChecked = item.checkBox(self._includeInReportColumnIndex).isChecked()
		for checkBoxColumnIndex in self.optionColumnIndexes()[1:]:
			item.checkBox(checkBoxColumnIndex).setDisabled(not isChecked)

	def _itemChanged(self, item, column):
		if not column in self.optionColumnIndexes():
			return
		self.setFocus()

	def summaryItem(self):
		return self._summaryItem

	def hideIncludeIntoReportColumn(self):		self.hideColumn(self._includeInReportColumnIndex)
	def showIncludeIntoReportColumn(self):		self.showColumn(self._includeInReportColumnIndex)
	def hideRestoreMiningWorkColumn(self):		self.hideColumn(self._restoreMiningWorkColumnIndex)
	def showRestoreMiningWorkColumn(self):		self.showColumn(self._restoreMiningWorkColumnIndex)
	def hideRestoreGeologyColumn(self):			self.hideColumn(self._restoreGeologyColumnIndex)
	def showRestoreGeologyColumn(self):			self.showColumn(self._restoreGeologyColumnIndex)
	def hideDensityColumn(self):				self.hideColumn(self._densityColumnIndex)
	def showDensityColumn(self):				self.showColumn(self._densityColumnIndex)
	def hideSimulateConcreteColumn(self):		self.hideColumn(self._simulateConcreteColumnIndex)
	def showSimulateConcreteColumn(self):		self.showColumn(self._simulateConcreteColumnIndex)

class BlockModelGradesWidget(QtGui.QSplitter):
	def __init__(self, parent):
		super(BlockModelGradesWidget, self).__init__(QtCore.Qt.Vertical, parent = parent)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._canSeeGrades = True
	def __createBindings(self):
		self.blockModelView.itemSelectionChanged.connect(self.showSelectedGrades)
	def __createWidgets(self):
		self.blockModelView = BlockModelView(self)
		self.blockModelGrades = CustomDataView(self)
		self.blockModelGrades.setVisible(False)
	def __gridWidgets(self):
		self.addWidget(self.blockModelView)
		self.addWidget(self.blockModelGrades)

		self.setStretchFactor(0, 70)
		self.setStretchFactor(1, 30)

	def showSelectedGrades(self):
		if not self._canSeeGrades:
			return
		#
		selectedData = self.blockModelView.selectedReportData()
		isGradeReportVisible = bool(selectedData)
		self.blockModelGrades.setVisible(isGradeReportVisible)
		if not isGradeReportVisible:
			return
		#
		gradeReport = self.blockModelView.gradeReport(selectedData)
		gradeHeader = (BMFLD_INDEX,) + tuple(self.blockModelView.elementHeader())
		gradeModel = CustomDataModel(self.blockModelGrades, gradeReport, gradeHeader)
		self.blockModelGrades.setModel(gradeModel)
		self.blockModelGrades.horizontalHeader().removeFilter()
		self.blockModelGrades.horizontalHeader().setVisible(True)
		self.blockModelGrades.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		self.blockModelGrades.horizontalHeader().setStretchLastSection(False)
		self.blockModelGrades.horizontalHeader().setDefaultSectionSize(70)

	def canSeeGrades(self):					return self._canSeeGrades
	def setCanSeeGrades(self, value):		self._canSeeGrades = bool(value)

class BmPreviewOptionsFrame(QtGui.QWidget):
	def __init__(self, parent):
		super(BmPreviewOptionsFrame, self).__init__(parent)

		self.setMaximumWidth(250)

		self.__createVariables()
		self.__initUi()
		self.__createBindings()

	def __initUi(self):
		self.__createWidgets()
		self.__gridWidgets()

	def __createVariables(self):
		self._mineSiteId = None
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(0)

		self.groupBoxDilution = QtGui.QGroupBox(self)
		self.groupBoxDilution.setTitle("Разубоживание")
		self.layoutDilution = QtGui.QFormLayout()
		self.labelDilutionCoef = QtGui.QLabel(self.groupBoxDilution)
		self.labelDilutionCoef.setText("Коэффициент")
		self.labelDilutionCoef.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelDilutionCoef.setMinimumWidth(100)
		self.lineEditDilutionCoef = QtGui.QLineEdit(self.groupBoxDilution)
		self.lineEditDilutionCoef.setMaximumWidth(50)
		self.lineEditDilutionCoef.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditDilutionCoef.setValidator(DoubleValidator(0, 1000, 3))

		self.groupBoxReportType = QtGui.QGroupBox(self)
		self.groupBoxReportType.setTitle("Тип отчета")
		self.layoutReportType = QtGui.QVBoxLayout()
		self.reportTypeButtonGroup = QtGui.QButtonGroup()
		self.radioButtonMonthly = QtGui.QRadioButton(self.groupBoxReportType)
		self.radioButtonMonthly.setText("По месяцу")
		self.radioButtonMonthly.setChecked(True)
		self.radioButtonByMine = QtGui.QRadioButton(self.groupBoxReportType)
		self.radioButtonByMine.setText("По выработке")
		self.reportTypeButtonGroup.addButton(self.radioButtonMonthly)
		self.reportTypeButtonGroup.addButton(self.radioButtonByMine)

		self.groupBoxSaveToDataBase = QtGui.QGroupBox(self)
		self.groupBoxSaveToDataBase.setTitle("Сохранить в базу данных")
		self.groupBoxSaveToDataBase.setCheckable(True)
		self.groupBoxSaveToDataBase.setChecked(False)
		self.layoutSaveToDataBase = QtGui.QFormLayout()
		self.labelMineSiteName = QtGui.QLabel(self.groupBoxSaveToDataBase)
		self.labelMineSiteName.setText("Участок")
		self.labelMineSiteName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelMineSiteName.setMinimumWidth(100)
		self.lineEditMineSiteName = QtGui.QLineEdit(self.groupBoxSaveToDataBase)
		self.lineEditMineSiteName.setReadOnly(True)
	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxDilution)
		self.groupBoxDilution.setLayout(self.layoutDilution)
		self.layoutDilution.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelDilutionCoef)
		self.layoutDilution.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditDilutionCoef)

		self.layoutMain.addWidget(self.groupBoxReportType)
		self.groupBoxReportType.setLayout(self.layoutReportType)
		self.layoutReportType.addWidget(self.radioButtonMonthly)
		self.layoutReportType.addWidget(self.radioButtonByMine)

		self.layoutMain.addWidget(self.groupBoxSaveToDataBase)
		self.groupBoxSaveToDataBase.setLayout(self.layoutSaveToDataBase)
		self.layoutSaveToDataBase.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelMineSiteName)
		self.layoutSaveToDataBase.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditMineSiteName)

	def dilutionCoef(self):
		coef = self.lineEditDilutionCoef.text()
		return 0 if is_blank(coef) or not is_number(coef) else float(coef)

class BlockModelPreviewDialog(BaseDialog):
	def __init__(self, *args, **kw):
		super(BlockModelPreviewDialog, self).__init__(*args, **kw)

		self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowMaximizeButtonHint)
		self.setWindowTitle("Выбор данных")
		self.resize(QtCore.QSize(1340, 600))

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._is_cancelled = True
		self._needsIncludeInReport = True
		self._needsRestoreMiningWork = True
		self._needsRestoreGeology = False
		self._needsSimulateConcrete = False
		self._groupFields = None
		self._fldCurrdate = None
		self._itemsAndReports = []
		self._canSeeGrades = True
		self._mineSiteId = None

		#
		concDensity = self.main_app.settings.getValue(ID_CONCDENS)
		geoCodes = self.main_app.settings.getValue(ID_GEOCODES)
		svyCatsAndCodes = self.main_app.settings.getValue(ID_SVYCAT)
		#
		self._materialIndexDensityDict = {}
		for (material, materialDesc, materialType), indexDict in geoCodes:
			for bmIndex, indexDensity, indexDensityWaste in list(zip(*indexDict["BM"]))[0]:	
				try:		density = float(indexDensity)
				except:		density = math.nan
				self._materialIndexDensityDict[(material, bmIndex)] = density
		self._materialIndexDensityDict[(svyCatsAndCodes[6][0][1], svyCatsAndCodes[6][1][0][0])] = concDensity
		self._materialIndexDensityDict[(svyCatsAndCodes[6][0][1], svyCatsAndCodes[6][1][0][1])] = concDensity

		#
		self._catPurposeDict = {}
		self._catCodeDescriptionDict = {}
		for (purpose, category), codesAndDescriptions in svyCatsAndCodes:
			self._catPurposeDict[category] = purpose
			for code, description in codesAndDescriptions:
				self._catCodeDescriptionDict[(category, code)] = description
		for material, index in self._materialIndexDensityDict.keys():
			if material in self.parent().getMineWorkCategories():
				continue
			self._catPurposeDict[material] = ROCK_AND_ORE
			self._catCodeDescriptionDict[(material, "")] = ""

		#
		self._materialDescriptionDict = {geoCode: geoDescription for (geoCode, geoDescription, other), indexes in geoCodes}
		self._materialDescriptionDict[self.parent().getBackfillCategory()] = BACKFILL

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.layoutControls = QtGui.QHBoxLayout()

		self.layoutLeftPanel = QtGui.QVBoxLayout()
		self.objectList = QtGui.QListWidget(self)
		self.objectList.setMaximumWidth(250)
		# self.objectList.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.paramsFrame = BmPreviewOptionsFrame(self)
		#
		self.bmPreviewStacked = QtGui.QStackedWidget(self)
		#
		self.layoutButtons = QtGui.QHBoxLayout()
		self.layoutFunctionButtons = QtGui.QHBoxLayout()
		self.layoutFunctionButtons.setAlignment(QtCore.Qt.AlignLeft)
		self.buttonExport = QtGui.QPushButton()
		self.buttonExport.setMinimumWidth(130)
		self.buttonExport.setText("Экспорт отчета")
		self.buttonSave = QtGui.QPushButton()
		self.buttonSave.setMinimumWidth(130)
		self.buttonSave.setText("Сохранить в базу")
		self.buttonSave.setDisabled(True)
		self.buttonUpdate = QtGui.QPushButton()
		self.buttonUpdate.setMinimumWidth(130)
		self.buttonUpdate.setText("Обновить")
		self.buttonLoad = QtGui.QPushButton()
		self.buttonLoad.setMinimumWidth(130)
		self.buttonLoad.setText("Загрузить в Визекс")

		self.layoutProcessButtons = QtGui.QHBoxLayout()
		self.layoutProcessButtons.setAlignment(QtCore.Qt.AlignRight)
		self.buttonContinue = QtGui.QPushButton()
		self.buttonContinue.setMinimumWidth(130)
		self.buttonContinue.setText("Продолжить")
		self.buttonCancel = QtGui.QPushButton()
		self.buttonCancel.setMinimumWidth(130)
		self.buttonCancel.setText("Отмена")
		
	def __createBindings(self):
		self.objectList.itemSelectionChanged.connect(self.listItemSelected)
		self.paramsFrame.groupBoxSaveToDataBase.clicked.connect(self.saveToDbStateChanged)
		self.paramsFrame.lineEditMineSiteName.mouseDoubleClickEvent = lambda e: self.selectMineSiteName()
		self.buttonCancel.clicked.connect(self._cancell)
		self.buttonContinue.clicked.connect(self._continue)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addLayout(self.layoutControls)

		self.layoutControls.addLayout(self.layoutLeftPanel)
		self.layoutLeftPanel.addWidget(self.objectList)
		self.layoutLeftPanel.addWidget(self.paramsFrame)
		#
		self.layoutControls.addWidget(self.bmPreviewStacked)
		#
		self.layoutMain.addLayout(self.layoutButtons)
		self.layoutButtons.addLayout(self.layoutFunctionButtons)
		self.layoutFunctionButtons.addWidget(self.buttonExport)
		self.layoutFunctionButtons.addWidget(self.buttonSave)
		self.layoutFunctionButtons.addWidget(self.buttonUpdate)
		self.layoutFunctionButtons.addWidget(self.buttonLoad)
		self.layoutButtons.addLayout(self.layoutProcessButtons)
		self.layoutProcessButtons.addWidget(self.buttonContinue)
		self.layoutProcessButtons.addWidget(self.buttonCancel)

	def _addPreviewPage(self, reportData, reportHeader):
		elemFields = self.parent().getIdwElements()
		elemUnits = self.parent().getIdwUnits()
		#
		bmPreviewWidget = BlockModelGradesWidget(self.bmPreviewStacked)
		bmPreviewWidget.setCanSeeGrades(self.canSeeGrades())
		#
		if not self._needsIncludeInReport:		bmPreviewWidget.blockModelView.hideIncludeIntoReportColumn()
		if not self._needsRestoreGeology:		bmPreviewWidget.blockModelView.hideRestoreGeologyColumn()
		if not self._needsRestoreMiningWork:	bmPreviewWidget.blockModelView.hideRestoreMiningWorkColumn()
		if not self._needsSimulateConcrete:		bmPreviewWidget.blockModelView.hideSimulateConcreteColumn()
		#
		bmPreviewWidget.blockModelView.setCatPurposeDict(self._catPurposeDict)
		bmPreviewWidget.blockModelView.setCatCodeDescriptionDict(self._catCodeDescriptionDict)
		bmPreviewWidget.blockModelView.setMaterialIndexDensityDict(self._materialIndexDensityDict)
		bmPreviewWidget.blockModelView.setMaterialDescriptionDict(self._materialDescriptionDict)
		bmPreviewWidget.blockModelView.setReport(reportData, reportHeader, elemFields, elemUnits, self._fldCurrdate)
		#
		self.bmPreviewStacked.addWidget(bmPreviewWidget)

	def addBlockModelPreview(self, listItem, reportData, reportHeader):
		self._itemsAndReports.append((listItem, (reportData, reportHeader)))
		#
		self.objectList.addItem(listItem)
		self._addPreviewPage(reportData, reportHeader)

	def _removeMultiPreviewPage(self):
		stackedCount = self.bmPreviewStacked.count()
		itemsCount = self.objectList.count()
		if stackedCount == itemsCount:
			return
		#
		self.bmPreviewStacked.removeWidget(self.bmPreviewStacked.widget(stackedCount-1))

	def listItemSelected(self):
		selectedItems = self.objectList.selectedItems()
		selectedItemsCount = len(selectedItems)
		if selectedItemsCount == 0:
			self.bmPreviewStacked.setCurrentIndex(0)
			return
		#
		self._removeMultiPreviewPage()
		#
		userItems, reports = tuple(zip(*self._itemsAndReports))
		if selectedItemsCount == 1:
			selectedItemIndex = userItems.index(selectedItems[0])
			self.bmPreviewStacked.setCurrentIndex(selectedItemIndex)
		else:
			multiReportData = []
			for i in range(self.objectList.count()):
				listItem = self.objectList.item(i)
				if not listItem.isSelected():
					continue
				#
				reportData, reportHeader = self._itemsAndReports[i][1]
				multiReportData.extend(reportData)
			#
			self._addPreviewPage(multiReportData, reportHeader)
			self.bmPreviewStacked.setCurrentIndex(self.bmPreviewStacked.count()-1)

	def saveToDbStateChanged(self):
		disabled = not self.paramsFrame.groupBoxSaveToDataBase.isChecked()
		self.buttonSave.setDisabled(disabled)

	def includedItemKeys(self):
		keys = []
		for i in range(self.objectList.count()):
			listItem = self.objectList.item(i)
			includedItems = self.bmPreviewStacked.widget(i).blockModelView.includedItems()
			for includedItem in includedItems:
				keys.append(listItem.key() + includedItem.key())
		return keys
	def restoredMiningWorkItemKeys(self):
		keys = []
		for i in range(self.objectList.count()):
			listItem = self.objectList.item(i)
			restoredMiningWorkItems = self.bmPreviewStacked.widget(i).blockModelView.restoredMiningWorkItems()
			for restoredMiningWorkItem in restoredMiningWorkItems:
				keys.append(listItem.key() + restoredMiningWorkItem.key())
		return keys
	def restoredGeologyItemKeys(self):
		keys = []
		for i in range(self.objectList.count()):
			listItem = self.objectList.item(i)
			restoredGeologyItems = self.bmPreviewStacked.widget(i).blockModelView.restoredGeologyItems()
			for restoredGeologyItem in restoredGeologyItems:
				keys.append(listItem.key() + restoredGeologyItem.key())
		return keys

	def simulateConcreteItemKeys(self):
		keys = []
		for i in range(self.objectList.count()):
			listItem = self.objectList.item(i)
			simulateConcreteItems = self.bmPreviewStacked.widget(i).blockModelView.simulateConcreteItems()
			for simulateConcreteItem in simulateConcreteItems:
				keys.append(listItem.key() + simulateConcreteItem.key())
		return keys

	def selectedReportData(self):
		reportData = []
		for i in range(self.objectList.count()):
			listItem = self.objectList.item(i)
			includedItems = self.bmPreviewStacked.widget(i).blockModelView.includedItems()
			for includedItem in includedItems:
				for reportRow in includedItem.reportData():
					reportData.append(listItem.key() + tuple(reportRow))
		reportHeader = list(self.bmPreviewStacked.widget(i).blockModelView.reportData())
		if RPTFLD_MINE_TONNAGE in reportHeader:
			mineTonnageIndex = reportHeader.index(RPTFLD_MINE_TONNAGE)
			reportHeader.remove(reportHeader[mineTonnageIndex])
			zippedReportData = list(zip(*reportData))
			zippedReportData.remove(zippedReportData[mineTonnageIndex])
			reportData = tuple(zip(*zippedReportData))
		return reportData, reportHeader

	def canSeeGrades(self):						return self._canSeeGrades
	def setCanSeeGrades(self, value):
		flag = bool(value)
		self._canSeeGrades = flag
		for i in range(self.bmPreviewStacked.count()):
			self.bmPreviewStacked.widget(i).setCanSeeGrades(flag)

	def needsIncludeInReport(self):				return self._needsIncludeInReport
	def setNeedsIncludeInReport(self, value):	self._needsIncludeInReport = bool(value)
	def needsRestoreGeology(self):				return self._needsRestoreGeology
	def setNeedsRestoreGeology(self, value):	self._needsRestoreGeology = bool(value)
	def needsRestoreMiningWork(self):			return self._needsRestoreMiningWork
	def setNeedsRestoreMiningWork(self, value):	self._needsRestoreMiningWork = bool(value)
	def needsSimulateConcrete(self):			return self._needsSimulateConcrete
	def setNeedsSimulateConcrete(self, value):	self._needsSimulateConcrete = bool(value)
	def fldCurrdate(self):						return self._fldCurrdate
	def setFldCurrdate(self, value):			self._fldCurrdate = value

	def setGeologySelected(self, flag, index = None):
		flag = bool(flag)
		if index is None:
			for i in range(self.objectList.count()):
				for purposeItem in filter(lambda item: item.isPurposeItem() and item.isGeology(), self.bmPreviewStacked.widget(i).blockModelView.iterChildren()):
					purposeItem.setIsIncluded(flag)
		else:
			for purposeItem in filter(lambda item: item.isPurposeItem() and item.isGeology(), self.bmPreviewStacked.widget(index).blockModelView.iterChildren()):
				purposeItem.setIsIncluded(flag)

	def setMaterialSelected(self, flag, index = None):
		flag = bool(flag)
		if index is None:
			for i in range(self.objectList.count()):
				for purposeItem in filter(lambda item: item.isPurposeItem() and (item.isBackfill() or item.isGeology()), self.bmPreviewStacked.widget(i).blockModelView.iterChildren()):
					purposeItem.setIsIncluded(flag)
		else:
			for purposeItem in filter(lambda item: item.isPurposeItem() and (item.isBackfill() or item.isGeology()), self.bmPreviewStacked.widget(index).blockModelView.iterChildren()):
				purposeItem.setIsIncluded(flag)

	def selectMineSiteName(self):
		if not self.paramsFrame.lineEditMineSiteName.isEnabled():
			return

		projname = self.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT DepartId, DepartName, PitName
			FROM [{0}].[dbo].[DEPARTS] D LEFT JOIN [{0}].[dbo].[PITS] P ON D.DepartPitID = P.PitID
			WHERE P.LOCATION = N'{1}'""".format(self.main_app.srvcnxn.dbsvy, projname)
		data = self.main_app.srvcnxn.exec_query(cmd).get_rows()

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите участок")
		model = CustomDataModel(dialog.view, data, ["ID", "УЧАСТОК", "ШАХТА"])
		dialog.view.setModel(model)
		dialog.view.hideColumn(0)
		dialog.view.setColumnWidth(1, 250)
		dialog.resize(QtCore.QSize(500, 500))
		dialog.exec()
		#
		mineSiteId = dialog.getResult()
		if not mineSiteId:
			return

		self.paramsFrame.lineEditMineSiteName.setText(mineSiteId[0][1])
		self._mineSiteId = mineSiteId[0][0]

	def groupFields(self):					return self._groupFields
	def setGroupFields(self, fields):		self._groupFields = tuple(fields)

	def mineSiteId(self):
		return self._mineSiteId

	def _continue(self):
		self._is_cancelled = False
		self.close()

	def _cancell(self):
		self.close()

	@property
	def is_cancelled(self):
		return self._is_cancelled

	@property
	def is_empty(self):
		return len(self._itemsAndReports) == 0

# BMFLD_CURRDATE = "2017-03"
# groupFields = (BMFLD_CURRDATE, BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL, BMFLD_MINEDATE, BMFLD_FACTDATE, BMFLD_FACTWORK, BMFLD_MATERIAL, BMFLD_INDEX)
# elemFields = ("Ni", "Cu", "Co", "S", "Pt", "Pd", "Rh", "Os", "Ir", "Ru", "Au", "Ag", "Te", "Se")
# elemUnits = (   0,    0,    0,   0,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)

# rptParams = dict(
# 	bmData = bmData, 
# 	bmHeader = bmHeader,
# 	groupFields = groupFields,
# 	fldDensity = "ОБЪЕМНЫЙ_ВЕС",
# 	elemFields = elemFields,
# 	elemUnits = elemUnits
# )

# largeReportData, largeReportHeader = bmDataPreviewReport(**rptParams)
# smallReportData, smallReportHeader = deepcopy(largeReportData)[:30], deepcopy(largeReportHeader)

# #

# app = QtGui.QApplication(os.sys.argv)
# #
# bmPreviewDialog = BlockModelPreviewDialog(None)
# bmPreviewDialog.resize(QtCore.QSize(1340, 600))
# #
# bmPreviewDialog.setNeedsRestoreGeology(False)
# bmPreviewDialog.setFldCurrdate(BMFLD_CURRDATE)
# #
# listItemLarge = createBmPreviewListTreeItem("LARGE", ITEM_BM, ITEM_CATEGORY_GEO)
# bmPreviewDialog.addBlockModelPreview(listItemLarge, largeReportData, largeReportHeader)
# listItemSmall = createBmPreviewListTreeItem("SMALL", ITEM_BM, ITEM_CATEGORY_GEO)
# bmPreviewDialog.addBlockModelPreview(listItemSmall, smallReportData, smallReportHeader)
# #
# bmPreviewDialog.exec()

# #
# app.exec_()
