try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os
from itertools import groupby

# os.chdir("D:\\NN\\N-MIne")
# os.sys.path.append("D:\\NN\\N-Mine")

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog
from gui.qt.widgets.TreeModel import TreeModelWidget, CheckBoxItem
from gui.qt.widgets.TreeModelItem import createTreeModelItem, TreeModelWireframeItem
from utils.constants import *

class AssignedDataDialog(BaseDialog):
	def __init__(self, parent):
		super(AssignedDataDialog, self).__init__(parent)
		self._parent = parent

		self.setWindowFlags(QtCore.Qt.WindowMaximizeButtonHint)
		self.setWindowTitle("Выбор данных")

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.resize(QtCore.QSize(630, 600))

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setText("Выберите выработки для включения в подсчет")

		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры")
		self.layoutParams = QtGui.QVBoxLayout()
		self.checkBoxSelectAll = QtGui.QCheckBox(self.groupBoxParams)
		self.checkBoxSelectAll.setText("Включить все горные работы в отчет")
		self.checkBoxExclConcreteAll = QtGui.QCheckBox(self.groupBoxParams)
		self.checkBoxExclConcreteAll.setText("Исключить бетон для всех выработок")

		self.tree = TreeModelWidget(self)
		columnNames = ["Выработка", "Интервал", "Работы", "Дата", "Включить", "Бетон"]
		columnWidths = [150, 150, 70, 70, 70, 70]
		for iCol, (columnName, columnWidth) in enumerate(zip(columnNames, columnWidths)):
			self.tree.headerItem().setText(iCol, columnName)
			self.tree.setColumnWidth(iCol, columnWidth)
	
		self.buttonBox = QtGui.QDialogButtonBox()
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
	def __createBindings(self):
		self.checkBoxSelectAll.stateChanged.connect(self.checkBoxSelectAllStateChanged)
		self.checkBoxExclConcreteAll.stateChanged.connect(self.setExcludeAllConcrete)
		self.tree.itemChanged.connect(self.itemSelected)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.close)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.labelHint)
		self.layoutMain.addWidget(self.groupBoxParams)
		#
		self.groupBoxParams.setLayout(self.layoutParams)
		self.layoutParams.addWidget(self.checkBoxSelectAll)
		self.layoutParams.addWidget(self.checkBoxExclConcreteAll)
		#
		self.layoutMain.addWidget(self.tree)
		self.layoutMain.addWidget(self.buttonBox)

	# 
	def itemSelected(self, item, column):
		if not column in [4, 5]:									return

		if column == 4:
			if not hasattr(item, "includeCheckBox"):
				return
			#
			isChecked = item.includeCheckBox.isChecked()
			if hasattr(item, "concreteCheckBox"):
				item.concreteCheckBox.setDisabled(not isChecked)
			if not isChecked:
				allItemsChecked = False
			else:
				allItemsChecked = True
				for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
					if not item.includeCheckBox.isChecked():
						allItemsChecked = False
						break
			#
			self.checkBoxSelectAll.blockSignals(True)
			self.checkBoxSelectAll.setChecked(allItemsChecked)
			self.checkBoxSelectAll.blockSignals(False)
		#
		if column == 5:
			if not hasattr(item, "concreteCheckBox"):
				return
			#
			isChecked = item.concreteCheckBox.isChecked()
			if isChecked:
				allItemsUnchecked = False
			else:
				allItemsUnchecked = True
				for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
					if item.concreteCheckBox.isChecked():
						allItemsUnchecked = False
						break
			#
			self.checkBoxExclConcreteAll.blockSignals(True)
			self.checkBoxExclConcreteAll.setChecked(allItemsUnchecked)
			self.checkBoxExclConcreteAll.blockSignals(False)

	def checkBoxSelectAllStateChanged(self, state):
		isChecked = state == QtCore.Qt.Checked
		self.setAllRowsSelected(isChecked)
	def checkBoxExclConcreteAllStateChanged(self, state):
		isChecked = state == QtCore.Qt.Checked
		self.setExcludeAllConcrete(isChecked)

	#
	def setValues(self, values, concCategory):
		groupIndexes = [0, 1]
		values = sorted(values, key = lambda x: tuple(x[k] for k in groupIndexes))
		#
		boldFont = QtGui.QFont()
		boldFont.setBold(True)
		for group, groupValues in groupby(values, key = lambda x: tuple(x[k] for k in groupIndexes)):
			wfItem = createTreeModelItem(self.tree, "", ITEM_WIREFRAME, ITEM_CATEGORY_SVY)
			for i, g in enumerate(group):
				wfItem.setText(i, g)
				wfItem.setFont(i, boldFont)
			#
			groupValues = sorted(set(tuple(row[len(group):]) for row in groupValues))
			mineMaterialDict = {}
			for row in groupValues:
				minename, interval, factwork, factdate, material = row
				key = (minename, interval, factwork, factdate)
				#
				if not key in mineMaterialDict:
					mineMaterialDict[key] = set()
				mineMaterialDict[key].add(material)
			#
			for key in sorted(mineMaterialDict):
				item = QtGui.QTreeWidgetItem(wfItem)
				for i, value in enumerate(key):
					item.setText(i, value)

				item.includeCheckBox = CheckBoxItem(self.tree, item, len(key))
				if concCategory in mineMaterialDict[key]:
					item.concreteCheckBox = CheckBoxItem(self.tree, item, len(key)+1)
					item.concreteCheckBox.setChecked(True)
					item.concreteCheckBox.setDisabled(True)
			wfItem.setExpanded(True)

		self.setAllRowsSelected(False)
	def values(self):
		vals = []
		nCols = self.tree.columnCount()
		for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
			if item.includeCheckBox.isChecked():
				vals.append([item.text(iCol) for iCol in range(nCols-2)] + [True if hasattr(item, "concreteCheckBox") and item.concreteCheckBox.isChecked() else False])
		return vals
	#
	def setAllRowsSelected(self, flag):
		flag = bool(flag)
		for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
			item.includeCheckBox.setChecked(flag)
	def setExcludeAllConcrete(self, flag):
		flag = bool(flag)
		for item in filter(lambda x: type(x) != TreeModelWireframeItem and hasattr(x, "concreteCheckBox"), self.tree.iterChildren()):
			item.concreteCheckBox.setChecked(not flag)
	def setSelectColumnIsHidden(self, flag):
		flag = bool(flag)
		if flag:	self.tree.hideColumn(4)
		else:		self.tree.showColumn(4)
	def setConcreteColumnIsHidden(self, flag):
		flag = bool(flag)
		if flag:	self.tree.hideColumn(5)
		else:		self.tree.showColumn(5)

class ConcreteByMineDialog(AssignedDataDialog):
	def __init__(self, parent):
		super(ConcreteByMineDialog, self).__init__(parent)
		#
		self.setSelectColumnIsHidden(True)

		self.checkBoxSelectAll.setVisible(False)

		self.resize(QtCore.QSize(550, 600))

	def setValues(self, *args, **kw):
		super(ConcreteByMineDialog, self).setValues(*args, **kw)
		self.setAllRowsSelected(True)

class AssignedAllDataDialog(BaseDialog):
	def __init__(self, parent):
		super(AssignedAllDataDialog, self).__init__(parent)
		self._parent = parent

		self.setWindowFlags(QtCore.Qt.WindowMaximizeButtonHint)
		self.setWindowTitle("Выбор данных")

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.resize(QtCore.QSize(630, 600))

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setText("Выберите выработки для включения в подсчет")

		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры")
		self.layoutParams = QtGui.QVBoxLayout()
		self.checkBoxSelectAll = QtGui.QCheckBox(self.groupBoxParams)
		self.checkBoxSelectAll.setText("Включить все горные работы в отчет")

		self.tree = TreeModelWidget(self)
		columnNames = ["Выработка", "Интервал", "Работы", "Дата", "Материал", "Включить"]
		columnWidths = [150, 150, 70, 70, 70, 70]
		for iCol, (columnName, columnWidth) in enumerate(zip(columnNames, columnWidths)):
			self.tree.headerItem().setText(iCol, columnName)
			self.tree.setColumnWidth(iCol, columnWidth)
	
		self.buttonBox = QtGui.QDialogButtonBox()
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
	def __createBindings(self):
		self.checkBoxSelectAll.stateChanged.connect(self.checkBoxSelectAllStateChanged)
		self.tree.itemChanged.connect(self.itemSelected)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.close)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.labelHint)
		self.layoutMain.addWidget(self.groupBoxParams)
		#
		self.groupBoxParams.setLayout(self.layoutParams)
		self.layoutParams.addWidget(self.checkBoxSelectAll)
		#
		self.layoutMain.addWidget(self.tree)
		self.layoutMain.addWidget(self.buttonBox)
	# 
	def itemSelected(self, item, column):
		if not column in [4, 5]:									return

		if column == 4:
			if not hasattr(item, "includeCheckBox"):
				return
			#
			isChecked = item.includeCheckBox.isChecked()
			if hasattr(item, "concreteCheckBox"):
				item.concreteCheckBox.setDisabled(not isChecked)
			if not isChecked:
				allItemsChecked = False
			else:
				allItemsChecked = True
				for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
					if not item.includeCheckBox.isChecked():
						allItemsChecked = False
						break
			#
			self.checkBoxSelectAll.blockSignals(True)
			self.checkBoxSelectAll.setChecked(allItemsChecked)
			self.checkBoxSelectAll.blockSignals(False)
		#
		if column == 5:
			if not hasattr(item, "concreteCheckBox"):
				return
			#
			isChecked = item.concreteCheckBox.isChecked()
			if isChecked:
				allItemsUnchecked = False
			else:
				allItemsUnchecked = True
				for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
					if item.concreteCheckBox.isChecked():
						allItemsUnchecked = False
						break
			#
			self.checkBoxExclConcreteAll.blockSignals(True)
			self.checkBoxExclConcreteAll.setChecked(allItemsUnchecked)
			self.checkBoxExclConcreteAll.blockSignals(False)

	def checkBoxSelectAllStateChanged(self, state):
		isChecked = state == QtCore.Qt.Checked
		self.setAllRowsSelected(isChecked)
	def checkBoxExclConcreteAllStateChanged(self, state):
		isChecked = state == QtCore.Qt.Checked
		self.setExcludeAllConcrete(isChecked)

	#
	def setValues(self, values):
		groupIndexes = [0, 1]
		values = sorted(values, key = lambda x: tuple(x[k] for k in groupIndexes))
		#
		boldFont = QtGui.QFont()
		boldFont.setBold(True)
		for group, groupValues in groupby(values, key = lambda x: tuple(x[k] for k in groupIndexes)):
			wfItem = createTreeModelItem(self.tree, "", ITEM_WIREFRAME, ITEM_CATEGORY_SVY)
			for i, g in enumerate(group):
				wfItem.setText(i, g)
				wfItem.setFont(i, boldFont)
			#
			groupValues = sorted(set(tuple(row[len(group):]) for row in groupValues))
			mineMaterialDict = {}
			for row in groupValues:
				item = QtGui.QTreeWidgetItem(wfItem)
				for i, value in enumerate(row):
					item.setText(i, value)
				item.includeCheckBox = CheckBoxItem(self.tree, item, len(row))
			wfItem.setExpanded(True)

		self.setAllRowsSelected(False)
	def values(self):
		vals = []
		nCols = self.tree.columnCount()
		for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
			if item.includeCheckBox.isChecked():
				vals.append(tuple([item.text(iCol) for iCol in range(nCols-1)]))
		return tuple(vals)
	#
	def setAllRowsSelected(self, flag):
		flag = bool(flag)
		for item in filter(lambda x: type(x) != TreeModelWireframeItem, self.tree.iterChildren()):
			item.includeCheckBox.setChecked(flag)

class BmMineNamesDialog(BaseDialog):
	def __init__(self, parent):
		super(BmMineNamesDialog, self).__init__(parent)
		self.setWindowFlags(QtCore.Qt.WindowMaximizeButtonHint)
		self.setWindowTitle("Отчет по блочной модели")
		self._parent = parent

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.resize(QtCore.QSize(300, 500))

	def __createVariables(self):
		self._cancelled = True
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setText("Выберите выработки, по котором необходимо создать отчет")
		self.listMines = QtGui.QListWidget(self)
		self.listMines.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.checkBoxSelectAll = QtGui.QCheckBox(self)
		self.checkBoxSelectAll.setText("Выбрать все")

		self.buttonBox = QtGui.QDialogButtonBox()
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
		self.checkBoxSelectAll.stateChanged.connect(self.checkBoxSelectAllStateChanged)
		self.listMines.itemChanged.connect(self.itemChanged)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.labelHint)
		self.layoutMain.addWidget(self.listMines)
		self.layoutMain.addWidget(self.checkBoxSelectAll)
		self.layoutMain.addWidget(self.buttonBox)

	def selectedValues(self):
		values = []
		for i in range(self.listMines.count()):
			item = self.listMines.item(i)
			if item.checkState() == QtCore.Qt.Checked:
				values.append(item.text())
		return values
	def setValues(self, values):
		self.listMines.clear()
		for value in values:
			item = QtGui.QListWidgetItem(value)
			self.listMines.addItem(item)
			item.setCheckState(QtCore.Qt.Checked)

	def check(self):
		if not self.selectedValues():
			qtmsgbox.show_error(self, "Ошибка", "Выберите хотя бы одну выработку")
			return
		return True
	def prepare(self):
		if not self.check():
			return
		self._continue()
	def _continue(self):
		self._cancelled = False
		self.close()

	@property
	def is_cancelled(self):
		return self._cancelled

	def checkBoxSelectAllStateChanged(self, state):
		flag = state == QtCore.Qt.Checked
		self.setAllRowsSelected(flag)
	def setAllRowsSelected(self, flag):
		for i in range(self.listMines.count()):
			item = self.listMines.item(i)
			item.setCheckState(QtCore.Qt.Checked if flag else QtCore.Qt.Unchecked)
	def itemChanged(self, item):
		self.checkBoxSelectAll.blockSignals(True)
		self.checkBoxSelectAll.setChecked(all([self.listMines.item(i).checkState() == QtCore.Qt.Checked for i in range(self.listMines.count())]))
		self.checkBoxSelectAll.blockSignals(False)
		#
		selectedItems = self.listMines.selectedItems()
		if item.isSelected():
			checkState = QtCore.Qt.Unchecked if item.checkState() == QtCore.Qt.Unchecked else QtCore.Qt.Checked
			for i in range(self.listMines.count()):
				it = self.listMines.item(i)
				if it == item or not it.isSelected():
					continue
				it.setCheckState(checkState)

# BMFLD_SYS_MINENAME = "SYS_%s" % BMFLD_MINENAME
# BMFLD_SYS_INTERVAL = "SYS_%s" % BMFLD_INTERVAL

# testHeader = [BMFLD_GEOMATERIAL, BMFLD_GEOINDEX, BMFLD_MATERIAL, BMFLD_INDEX, BMFLD_MINENAME, BMFLD_INTERVAL, BMFLD_SYS_MINENAME, BMFLD_SYS_INTERVAL]
# testData = []
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА1", "ИНТЕРВАЛ1", "СИСВЫРКА1", "СИСИНТЕРВАЛ1"])
# testData.append(["Б", "X", "ЗАКЛ", "ПРОХ", "ВЫРКА1", "ИНТЕРВАЛ1", "СИСВЫРКА1", "СИСИНТЕРВАЛ1"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА1", "ИНТЕРВАЛ1", "СИСВЫРКА1", "СИСИНТЕРВАЛ1"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА1", "ИНТЕРВАЛ1", "СИСВЫРКА1", "СИСИНТЕРВАЛ2"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА1", "ИНТЕРВАЛ1", "СИСВЫРКА1", "СИСИНТЕРВАЛ2"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА2", "ИНТЕРВАЛ1", "СИСВЫРКА2", "СИСИНТЕРВАЛ1"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА2", "ИНТЕРВАЛ1", "СИСВЫРКА2", "СИСИНТЕРВАЛ1"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА2", "ИНТЕРВАЛ1", "СИСВЫРКА2", "СИСИНТЕРВАЛ1"])
# testData.append(["ЗАКЛ", "ЗАКЛ", "ЗАКЛ", "ЗАКЛ", "ВЫРКА2", "ИНТЕРВАЛ1", "СИСВЫРКА2", "СИСИНТЕРВАЛ2"])
# testData.append(["ЗАКЛ", "ЗАКЛ", "ЗАКЛ", "ЗАКЛ", "ВЫРКА2", "ИНТЕРВАЛ1", "СИСВЫРКА2", "СИСИНТЕРВАЛ2"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА1", "ИНТЕРВАЛ1", "СИСВЫРКА2", "СИСИНТЕРВАЛ2"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА1", "ИНТЕРВАЛ1", "СИСВЫРКА3", "СИСИНТЕРВАЛ1"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА3", "ИНТЕРВАЛ1", "СИСВЫРКА3", "СИСИНТЕРВАЛ1"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА3", "ИНТЕРВАЛ1", "СИСВЫРКА3", "СИСИНТЕРВАЛ2"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА3", "ИНТЕРВАЛ1", "СИСВЫРКА3", "СИСИНТЕРВАЛ2"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА3", "ИНТЕРВАЛ1", "СИСВЫРКА3", "СИСИНТЕРВАЛ3"])
# testData.append(["Б", "X", "ЗАКЛ", "ЗАКЛ", "ВЫРКА3", "ИНТЕРВАЛ1", "СИСВЫРКА3", "СИСИНТЕРВАЛ3"])

# app = QtGui.QApplication(os.sys.argv)
# widget = AssignedAllDataDialog(app)

# concData = [row[-2:] + row[-4:-2] + row[:-5] for row in testData]
# widget.setValues(concData, concCategory = "ЗАКЛ")
# # widget.checkBoxExclConcreteAll.setChecked(True)
# # widget.setConcreteColumnIsHidden(True)
# widget.exec()

# replaceIndexes = widget.values()

# for row in replaceIndexes:
# 	print (row)

# app.exec_()