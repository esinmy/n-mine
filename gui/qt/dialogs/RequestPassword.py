from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.osutil import set_file_extension
from utils.validation import is_blank
from gui.qt.dialogs.BaseDialog import BaseDialog
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow


class RequestPassword(DialogWindow):
	def __init__(self, parent):
		super(RequestPassword, self).__init__(parent)
		self.setWindowTitle("Запрос авторизации")



		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):	pass
		


	def __createWidgets(self):
		self.layoutParams = QtGui.QFormLayout()

		self.labelPassword = QtGui.QLabel(self)
		self.labelPassword.setText("Пароль")
		self.labelPassword.setMinimumWidth(100)
		self.labelPassword.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditPassword = QtGui.QLineEdit(self)
		self.lineEditPassword.setMinimumWidth(200)
		self.lineEditPassword.setEchoMode(QtGui.QLineEdit.Password)
		

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.close)
	def __gridWidgets(self):
		self.layoutControls.addLayout(self.layoutParams)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelPassword)
		self.layoutParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditPassword)



	def GetPassword(self):
		return self.lineEditPassword.text();

	
	