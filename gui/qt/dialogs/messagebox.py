from PyQt4 import QtGui, QtCore
import os

def show_error(parent, title, message):
	return QtGui.QMessageBox.critical(parent, title, message)
def show_information(parent, title, message):
	return QtGui.QMessageBox.information(parent, title, message)
def show_question(parent, title, message):
	msgbox = QtGui.QMessageBox()
	msgbox.setMinimumSize(QtCore.QSize(200, 200))

	msgbox.setWindowTitle(title)
	msgbox.setText(message)

	msgbox.setIcon(QtGui.QMessageBox.Question)
	yes = msgbox.addButton("Да", QtGui.QMessageBox.YesRole)
	no = msgbox.addButton("Нет", QtGui.QMessageBox.NoRole)
	msgbox.exec()

	if msgbox.clickedButton() == yes:		return True
	elif msgbox.clickedButton() == no:		return False
	else:									return None

def show_question_triDB(parent, title, message):
	msgbox = QtGui.QMessageBox()

	msgbox.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowTitleHint)

	msgbox.setWindowTitle(title)
	msgbox.setText(message)

	msgbox.setIcon(QtGui.QMessageBox.Question)
	yes = msgbox.addButton("Перезаписать весь файл", 6)
	add_wfr = msgbox.addButton("Добавить недостающие каркасы", 5)
	no = msgbox.addButton("Отмена", 4)
	msgbox.exec()

	if msgbox.clickedButton() == yes:		return 1
	if msgbox.clickedButton() == add_wfr:	return 2
	elif msgbox.clickedButton() == no:		return 0
	else:									return 0