from PyQt4 import QtGui, QtCore
import os

from datetime import datetime

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.widgets.TableDataCheckView import CustomDataCheckView, CustomDataCheckModel
from gui.qt.widgets.DateRangeWidget import DateRangeWidget
from gui.qt.widgets.ShapeFilterWidget import	DropDownWidget, ShapeFilterWidget

from mm.formsets import loadSurveyPoints
from mm.mmutils import MicromineFile
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DatabaseLoaderWindow(DialogWindow):
	def __init__(self, *args, **kw):
		super(DatabaseLoaderWindow, self).__init__(*args, **kw)
		self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.resize(1000, 700)

	def __createVariables(self):
		self._hasDateFilter = True
		self._dataload = []
		self._total_count_objects = 0
		self._filtred_count_objects = 0
		self._checked_count_objects = 0
	def __createWidgets(self):
		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры")

		self.layoutParams = QtGui.QVBoxLayout()
		self.layoutParamsLine1 = QtGui.QHBoxLayout()
		self.layoutDateInterval = QtGui.QFormLayout()
		self.labelDateInterval = QtGui.QLabel(self.groupBoxParams)
		self.labelDateInterval.setText("Интервал")
		self.labelDateInterval.setMinimumWidth(90)
		self.labelDateInterval.setMaximumWidth(90)
		self.labelDateInterval.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.dateRangeWidget = DateRangeWidget(self.groupBoxParams)
		self.dateRangeWidget.setMaximumWidth(300)

		self.layoutObjectFilter = QtGui.QFormLayout()
		self.labelObjectFilter = QtGui.QLabel(self.groupBoxParams)
		self.labelObjectFilter.setText("Объектный фильтр")
		self.labelObjectFilter.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.dropDownShapeFilter= DropDownWidget(self.groupBoxParams)
		self.shapeFilter = ShapeFilterWidget(self.dropDownShapeFilter)
		self.dropDownShapeFilter.setWidget(self.shapeFilter)


		self._font = QtGui.QFont()
		self._font.setPointSize(8.5)
		self.statusBarLoaderWindow = QtGui.QStatusBar(self)
		self.statusBarLoaderWindow.setSizeGripEnabled(False)
		self.statusBarLoaderWindow.setFont(self._font)

		self.view = CustomDataCheckView(self)
		self.model = CustomDataCheckModel(self.view, [[""]], [""])
		self._total_count_objects = self.model.rowCount()
		self._filtred_count_objects = self._total_count_objects
		self.set_status_bar()

		self.view.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
		self.view.setModel(self.model)

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
		self.view.customAnyRowChecked.connect(self.on_model_customAnyRowChecked)
		self.model.customRowsCountChanged.connect(self.on_model_customRowsCountChanged)


	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxParams)
		
		self.groupBoxParams.setLayout(self.layoutParams)
		self.layoutParams.addLayout (self.layoutParamsLine1)
		self.layoutParamsLine1.addLayout(self.layoutDateInterval)
		self.layoutDateInterval.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelDateInterval)
		self.layoutDateInterval.setWidget(0, QtGui.QFormLayout.FieldRole, self.dateRangeWidget)

		self.layoutParamsLine1.addLayout(self.layoutObjectFilter)
		self.layoutObjectFilter.setWidget(0,QtGui.QFormLayout.LabelRole, self.labelObjectFilter)
		self.layoutObjectFilter.setWidget(0,QtGui.QFormLayout.FieldRole, self.dropDownShapeFilter )

		self.layoutControls.addWidget(self.view)
		self.layoutControls.addWidget(self.statusBarLoaderWindow)



	def showDateFilter(self):
		self.groupBoxParams.setVisible(True)
		self._hasDateFilter = True
	def hideDateFilter(self):
		self.groupBoxParams.setVisible(False)
		self._hasDateFilter = False


	def on_model_customAnyRowChecked(self, sender):
		self._checked_count_objects = self.model.getSelectedCount()
		self.set_status_bar()

	def on_model_customRowsCountChanged(self, count):
		self._filtred_count_objects = count
		self.set_status_bar()


	def set_status_bar(self):
		self.statusBarLoaderWindow.showMessage("Количество объектов - всего: {0}, отфильтровано: {1}, выбрано: {2}".format(self._total_count_objects,
																													 self._filtred_count_objects,
																													 self._checked_count_objects))



	@property
	def hasDateFilter(self):
		return self._hasDateFilter

	def setDataset(self, dataset, header, iDateFromField = None, iDateToField = None):
		self.model = CustomDataCheckModel(self.view, dataset, header)
		self.view.setModel(self.model)
		self._total_count_objects = self.model.rowCount()
		self._filtred_count_objects = self._total_count_objects
		self.set_status_bar()

		self.model.customRowsCountChanged.connect(self.on_model_customRowsCountChanged)


		if iDateFromField is not None and iDateToField is not None:
			try:
				mindate = datetime.strptime(min([row[iDateFromField] for row in dataset if row[iDateFromField]]), "%Y-%m-%d")
			except:
				mindate = datetime.strptime ("1961-04-12", "%Y-%m-%d")
			try:
				maxdate = datetime.strptime(max([row[iDateToField] for row in dataset if row[iDateToField]]), "%Y-%m-%d")
			except:
				maxdate = datetime.strptime ("2161-04-12", "%Y-%m-%d")

			self.dateRangeWidget.setDateRange(mindate, maxdate)
			self.dateRangeWidget.setDateFrom(mindate)
			self.dateRangeWidget.setDateTo(maxdate)

	def check(self):
		if not super(DatabaseLoaderWindow, self).check():
			return
		#self._dataload = self.view.getSelected()
		self._dataload = self.model.getSelected()
		#
		if len(self._dataload) == 0:
			qtmsgbox.show_error(self, "Ошибка", "Нет выбранных объектов для загрузки")
			return
		#
		return True
