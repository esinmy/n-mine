from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


def createListTreeItem(value, itemType, itemCategory):
	itDict = {}

	itDict[ITEM_TRIDB] = ListTreeTridbItem
	itDict[ITEM_DATA] = ListTreeDatItem
	itDict[ITEM_STRING] = ListTreeStringItem
	itDict[ITEM_ANNOTATION] = ListTreeAnnotationItem
	itDict[ITEM_MODEL] = ListTreeModelItem
	itDict[ITEM_BM] = ListTreeBmItem
	itDict[ITEM_WIREFRAME] = ListTreeWireframeItem
	itDict[ITEM_SECTIONPACK] = ListTreeSectionPackItem
	itDict[ITEM_SECTION] = ListTreeSectionItem
	itDict[ITEM_STOCK] = ListTreeStockItem
	itDict[ITEM_STOCKSECTOR] = ListTreeStockSectorItem

	item = itDict[itemType](value, itemCategory)

	return item

class ListTreeItem(QtGui.QListWidgetItem):
	def __init__(self, value, itemType, itemCategory):
		self._itemType = itemType
		self._itemCategory = itemCategory
		self._filePath = None

		if not itemType in ITEM_TYPE_LIST:			raise Exception("Неверный тип объекта списка. Разрешенные значения %s" % str(ITEM_TYPE_LIST))
		if not itemCategory in ITEM_CATEGORY_LIST:	raise Exception("Неверная категория объекта списка. Разрешенные значения %s" % str(ITEM_CATEGORY_LIST))

		super(ListTreeItem, self).__init__(value)

		self.setIcon(QtGui.QIcon(ITEM_ICO_DICT[self._itemType]))

	def name(self):				return self.text(0)
	def setName(self, value):	self.setText(0, value)
	def itemCategory(self):		return self._itemCategory
	def itemType(self):			return self._itemType
	#
	def isDirectory(self):		return self._itemType == ITEM_DIR
	def isDat(self):			return self._itemType == ITEM_DATA
	def isString(self):			return self._itemType == ITEM_STRING
	def isModel(self):			return self._itemType == ITEM_MODEL
	def isBlockModel(self):		return self._itemType == ITEM_BM
	def isTridb(self):			return self._itemType == ITEM_TRIDB
	def isWireframe(self):		return self._itemType == ITEM_WIREFRAME
	def isAnnotation(self):		return self._itemType == ITEM_ANNOTATION
	def isSectionPack(self):	return self._itemType == ITEM_SECTIONPACK
	def isSection(self):		return self._itemType == ITEM_SECTION
	def isStock(self):			return self._itemType == ITEM_STOCK
	def isStockSector(self):	return self._itemType == ITEM_STOCKSECTOR

	def setFilePath(self, value):
		if not self.itemType() in [ITEM_MODEL, ITEM_WIREFRAME, ITEM_SECTION, ITEM_STOCK, ITEM_STOCKSECTOR] and not os.path.exists(value):
			raise FileNotFoundError("Путь '%s' не существует" % value)
		else:
			self._filePath = value
	def filePath(self):
		return self._filePath

class ListTreeModelItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeModelItem, self).__init__(parent, ITEM_MODEL, item_category)

class ListTreeBmItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeBmItem, self).__init__(parent, ITEM_BM, item_category)

class ListTreeTridbItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeTridbItem, self).__init__(parent, ITEM_TRIDB, item_category)

class ListTreeWireframeItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeWireframeItem, self).__init__(parent, ITEM_WIREFRAME, item_category)

class ListTreeStringItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeStringItem, self).__init__(parent, ITEM_STRING, item_category)

class ListTreeDatItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeDatItem, self).__init__(parent, ITEM_DATA, item_category)

class ListTreeAnnotationItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeAnnotationItem, self).__init__(parent, ITEM_ANNOTATION, item_category)

class ListTreeSectionPackItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeSectionPackItem, self).__init__(parent, ITEM_SECTIONPACK, item_category)

class ListTreeSectionItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeSectionItem, self).__init__(parent, ITEM_SECTION, item_category)

class ListTreeStockItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeStockItem, self).__init__(parent, ITEM_STOCK, item_category)

class ListTreeStockSectorItem(ListTreeItem):
	def __init__(self, parent, item_category):
		super(ListTreeStockSectorItem, self).__init__(parent, ITEM_STOCKSECTOR, item_category)