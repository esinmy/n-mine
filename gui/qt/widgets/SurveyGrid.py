from PyQt4 import QtGui, QtCore

from utils.osutil import is_number
from utils.qtutil import iterWidgetParents
import geometry as geom
import math

from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import CustomDataModel

from directions.svydbapi import SvyPoint
from directions.measure import ControlMeasure, DirectionMeasure
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

N_HEADER_ROWS = 2
N_POINT_ROWS = 4

DISABLE_COLOR = QtGui.QColor(240, 240, 240)
CALC_COLOR = QtGui.QColor(225, 225, 225)
USER_COLOR = QtGui.QColor(255, 255, 255)
INVALID_COLOR = QtGui.QColor(255, 0, 0)

DISABLED_ITEM = "EMPTY"
CALC_ITEM = "CALC"
USER_ITEM = "USER"
INVALID_ITEM = "INVALID"
ITEM_COLORS = {DISABLED_ITEM: DISABLE_COLOR, CALC_ITEM: CALC_COLOR, USER_ITEM: USER_COLOR}
ITEM_TYPES = [DISABLED_ITEM, CALC_ITEM, USER_ITEM]


class SvyDataDelegate(QtGui.QItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()
		irow = index.row()
		localRow = (irow-N_HEADER_ROWS) % N_POINT_ROWS

		self.editor = QtGui.QLineEdit(widget)

		if icol == 2 and localRow == 0 or icol == 3 and localRow in [1,2]:
			validator = QtGui.QDoubleValidator(-1e6, 1e6, 3)
		else:
			return super(SvyDataDelegate, self).createEditor(widget, option, index)

		self.editor.setValidator(validator)
		return self.editor

class SvyGridItem(QtGui.QTableWidgetItem):
	def __init__(self, ittype = USER_ITEM):
		super(SvyGridItem, self).__init__()
		self._type = ittype
		self._isValid = True
		self._disabled = False
		self._readOnly = False

		self.setBackgroundColor(ITEM_COLORS[ittype])
		if ittype == USER_ITEM:		self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)
		else:						self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
	def disable(self):
		if self._type != USER_ITEM:
			return
		self._disabled = True
		self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
		self.setBackgroundColor(DISABLE_COLOR)
	def enable(self):
		if self._type != USER_ITEM:
			return
		self._disabled = False
		self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)
		self.setBackgroundColor(USER_COLOR)
	def isEnabled(self):		return self._disabled == False
	def isDisabled(self):		return self._disabled
	def isReadOnly(self):		return self._readOnly
	def setReadOnly(self, v):
		self._readOnly = bool(v)
		if self._readOnly:
			self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
		else:
			self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)
	def itemType(self):
		return self._type
	def isEmpty(self):
		return self.text().replace(" ", "") == ""
	def setIsValid(self, flag):
		self._isValid = bool(flag)
		if not self._isValid:	self.setBackgroundColor(INVALID_COLOR)
		else:					self.setBackgroundColor(ITEM_COLORS[self._type])
	def isValid(self):
		return self._isValid

class SvyGrid(QtGui.QTableWidget):
	def __init__(self, *args, **kw):
		super(SvyGrid, self).__init__(*args, **kw)

		self.setEditTriggers(QtGui.QTableWidget.SelectedClicked | QtGui.QTableWidget.EditKeyPressed | QtGui.QTableWidget.AnyKeyPressed)

		self.horizontalHeader().setVisible(False)
		self.verticalHeader().setVisible(False)
		self.verticalHeader().setDefaultSectionSize(25)
		self.setItemDelegate(SvyDataDelegate())
		self.__createVariables()

	def __createVariables(self):
		self._headerFont = QtGui.QFont()
		self._headerFont.setBold(True)
		self._cellOrder = []
		
	def keyPressEvent(self, event):
		key = event.key()
		if key == QtCore.Qt.Key_Delete or key == QtCore.Qt.Key_Backspace:
			for index in self.selectedIndexes():
				if self.item(index.row(), index.column()).itemType() == USER_ITEM:
					self.item(index.row(), index.column()).setText("")
		super(SvyGrid, self).keyPressEvent(event)
	def event(self, event):
		if event.type() == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Tab:
			item = self.selectNextCell()
			return -1
		return super(SvyGrid, self).event(event)

	def closeEditor(self, editor, hint):
		if hint == QtGui.QAbstractItemDelegate.NoHint:
			super(SvyGrid, self).closeEditor(editor, QtGui.QAbstractItemDelegate.SubmitModelCache)

		elif hint in (QtGui.QAbstractItemDelegate.EditNextItem, QtGui.QAbstractItemDelegate.EditPreviousItem):
			if hint == QtGui.QAbstractItemDelegate.EditNextItem:
				self.editNextCell()
			else:
				self.editPrevCell()
			
			super(SvyGrid, self).closeEditor(editor, 0)
		else:
			super(SvyGrid, self).closeEditor(editor, hint)

	def _getCurrentCellIndex(self):
		currCell = self.currentItem()
		if currCell is None:
			return None
		row, column = currCell.row(), currCell.column()
		cellsCount = len(self._cellOrder)
		if cellsCount == 0:
			return None
		return self._cellOrder.index([row, column]) if [row, column] in self._cellOrder else 0
	def selectNextCell(self):
		currCellIndex, cellsCount = self._getCurrentCellIndex(), len(self._cellOrder)
		if cellsCount == 0:
			return
		nextItem = self.item(*self._cellOrder[(currCellIndex+1) % cellsCount])
		self.setCurrentItem(nextItem)
		return nextItem
	def editNextCell(self):
		nextItem = self.selectNextCell()
		if nextItem and not nextItem.isReadOnly():
			self.editItem(nextItem)
	def selectPrevCell(self):
		currCellIndex, cellsCount = self._getCurrentCellIndex(), len(self._cellOrder)
		if cellsCount == 0:
			return
		prevItem = self.item(*self._cellOrder[(currCellIndex-1) % cellsCount])
		self.setCurrentItem(prevItem)
		return prevItem
	def editPrevCell(self):
		prevItem = self.selectPrevCell()
		if prevItem and not prevItem.isReadOnly():
			self.editItem(prevItem)

class SvyControlGrid(SvyGrid):
	def __init__(self, parent):
		super(SvyControlGrid, self).__init__(parent)

		self.setColumnCount(7)
		self.setRowCount(N_HEADER_ROWS + N_POINT_ROWS)

		self.__createVariables()
		self.__buildGrid()
		self.__createBindings()

	def __createVariables(self):
		self._window = iterWidgetParents(self)[-2]
		self._data = self._window.data["POINTS"]
		self._cnxn = self._window.main_app.srvcnxn
		# self._cnxn = self._window.srvcnxn

		stpt = SvyPoint(self._cnxn)
		bkpt = SvyPoint(self._cnxn)
		fwpt = SvyPoint(self._cnxn)
		self._measure = ControlMeasure(stpt, bkpt, fwpt)

		self._disabledCells = (	(0,0), (0,3),
								(1,1), (1,2), (1,4), (1,5), (1,6),
								(2,0), (2,1), (2,2), (2,4), (2,5), (2,6),
								(3,0), (3,3)	)
		# if self._window.isEditingMode():
		# 	self._disabledCells += ((3,1),)

		self._calcCells = (	(0,4), (0,5), (0,6),
							(3,4), (3,5)	)

		self._userCells = (	(0,1), (0,2), (1,0), (1,3), (2,3), (3,1), (3,2), (3,6)	)
		# if not self._window.isEditingMode():
		# 	self._userCells += ((3,1),)

		self._cellOrder = [[1,0], [0,1], [3,1], [0,2], [1,3], [2,3], [3,2], [3,6]]
		for i in range(len(self._cellOrder)):
			self._cellOrder[i][0] += N_HEADER_ROWS

	def __createBindings(self):
		self.itemDoubleClicked.connect(self.selectPoint)
		self.itemChanged.connect(self.parent().doCalculation)

	def __buildGrid(self):
		self.__buildHeader()
		self.__buildBody()
	def __buildHeader(self):
		self.blockSignals(True)
		self.setColumnWidth(0, 120)
		self.setColumnWidth(1, 120)
		for i in range(2, self.columnCount()):
			self.setColumnWidth(i, 70)

		# spanning
		self.setSpan(0,0,1,2)
		self.setSpan(0,3,2,1)
		self.setSpan(0,4,2,1)
		self.setSpan(0,5,2,1)
		self.setSpan(0,6,2,1)
		self.setSpan(0,7,2,1)

		header = ["Точки", "Длина", "Выс.инстр.\nВыс.виска", "Гориз.\nпролож.", "dz", "Гориз.\nугол"]
		iCols = [0, 2, 3, 4, 5, 6, 7]
		for iCol, name in zip(iCols, header):
			item = QtGui.QTableWidgetItem(name)
			item.setTextAlignment(QtCore.Qt.AlignCenter)
			item.setFlags(QtCore.Qt.ItemIsEnabled)
			item.setFont(self._headerFont)
			self.setItem(0, iCol, item)
		
		header = ["Стояния", "Наблюдения", "Верт.угол"]
		iCols = [0, 1, 2]
		for iCol, name in zip(iCols, header):
			item = QtGui.QTableWidgetItem(name)
			item.setTextAlignment(QtCore.Qt.AlignCenter)
			item.setFlags(QtCore.Qt.ItemIsEnabled)
			item.setFont(self._headerFont)
			self.setItem(1, iCol, item)
		self.blockSignals(False)
	def __buildBody(self):
		self.blockSignals(True)
		for iRow, iCol in self._userCells:		self.setItem(N_HEADER_ROWS + iRow, iCol, SvyGridItem(USER_ITEM))
		for iRow, iCol in self._disabledCells:	self.setItem(N_HEADER_ROWS + iRow, iCol, SvyGridItem(DISABLED_ITEM))
		for iRow, iCol in self._calcCells:		self.setItem(N_HEADER_ROWS + iRow, iCol, SvyGridItem(CALC_ITEM))

		self.item(N_HEADER_ROWS + 1, 0).setReadOnly(True)
		self.item(N_HEADER_ROWS, 1).setReadOnly(True)
		self.item(N_HEADER_ROWS + 3, 1).setReadOnly(True)

		self.item(N_HEADER_ROWS, 1).setFont(self._headerFont)
		self.item(N_HEADER_ROWS+1, 0).setFont(self._headerFont)
		self.item(N_HEADER_ROWS+3, 1).setFont(self._headerFont)
		self.blockSignals(False)

	def clear(self):
		self._measure.clear()
		self._measure.standpoint = SvyPoint(self._cnxn)
		self._measure.backpoint = SvyPoint(self._cnxn)
		self._measure.dirpoint = SvyPoint(self._cnxn)


	def keyPressEvent(self, event):
		super(SvyControlGrid, self).keyPressEvent(event)
		# делаем только для чтения ячейку с точкой визирования
		if self._window.isEditingMode():
			return
		#
		key = event.key()
		if key == QtCore.Qt.Key_Delete or key == QtCore.Qt.Key_Backspace:
			for index in self.selectedIndexes():
				if index.column() == 1 and index.row()-N_HEADER_ROWS == 3:
					self.item(index.row(), index.column()).setReadOnly(False)
					self.setDirPoint(SvyPoint(self._cnxn))
	
	def selectPoint(self, item):
		"""
		Если редактируем контроль
			Исключить равенство текующей точки одной из точек на первом шаге хода
		Если редактируем первый шаг хода
			Для задней точки и точки стояния предоставить выбор контрольных точек
			Для точки визирования предоставить выбор из удаленных точек (как ранее)
		"""

		irow, icol = item.row(), item.column()
		cell = (irow, icol)
		index = self.index()

		ptCells = [(N_HEADER_ROWS, 1), (N_HEADER_ROWS+1, 0), (N_HEADER_ROWS+3, 1)]
		ptCellsDict = dict(zip(ptCells, [self.measure.backpoint, self.measure.standpoint, self.measure.dirpoint]))

		if not cell in ptCells or item.itemType() != USER_ITEM:
			return

		curPoint = ptCellsDict[cell]

		controlIsBeingEdited = self.__class__.__name__ == "SvyControlGrid"
		# точки контрольного замера
		ctrlMeasure = self._window.frameControl.gridWidget.grids[0].measure
		ctrlBkPt = ctrlMeasure.backpoint
		ctrlStPt = ctrlMeasure.standpoint
		ctrlFwPt = ctrlMeasure.dirpoint
		ctrlPoints = [ctrlBkPt, ctrlStPt, ctrlFwPt]
		# точки хода на первом шаге
		actualMeasure = self._window.frameCalculation.gridWidget.grids[0].measure
		actualBkPt = actualMeasure.backpoint
		actualStPt = actualMeasure.standpoint
		actualFwPt = actualMeasure.dirpoint
		actualPoints = [actualBkPt, actualStPt, actualFwPt]

		filterData = None
		if not controlIsBeingEdited:
			# можно изменять только опорные точки на первом шаге
			if not cell in ptCells or (index > 0 and cell != ptCells[-1]):
				return
			if cell in ptCells[:2]:
				filterPoints = ctrlPoints
				filterData = [[pt.id, pt.cmid] for pt in filterPoints]
		if self._window.isEditingMode():
			# во время редактирования контрольного замера запрещено изменять точки,
			# используемые в новох ходе
			if controlIsBeingEdited and any(map(lambda pt: pt.similarTo(curPoint), actualPoints)):
				return

		if filterData is not None:
			# ивзлекаем точки, используемые в контроле
			ctrlData = self._window.frameControl.gridWidget.grids[0]._data[0]
			filteredData = [row for row in ctrlData if row[:2] in filterData]
		else:
			filteredData = self._data[0]
		header = self._data[1]
		#
		#Galkin
		self.dialog = SelectFromTableDialog(self)
		self.dialog.setWindowTitle("Выберите точку")
		model = CustomDataModel(self.dialog.view, filteredData, header)
		self.dialog.view.setModel(model)
		self.dialog.view.hideColumn(0)
		self.dialog.view.hideColumn(1)
		self.dialog.resize(QtCore.QSize(650, 500))
		self.dialog.view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.dialog.view.customContextMenuRequested.connect(self.displayMenu_GroupedPoints)
		self.dialog.exec()
		#
		pt = self.dialog.getResult()
		if not pt:
			return
		
		if irow == N_HEADER_ROWS + 1:	self.setStandPoint(SvyPoint(self._cnxn, pt[0][0], pt[0][1]))
		if irow == N_HEADER_ROWS:		self.setBackPoint(SvyPoint(self._cnxn, pt[0][0], pt[0][1]))
		if irow == N_HEADER_ROWS + 3:	self.setDirPoint(SvyPoint(self._cnxn, pt[0][0], pt[0][1]))
		#Galkin
		return pt

	def displayMenu_GroupedPoints(self, position):
		indexes = self.dialog.view.selectedIndexes()

		model = self.dialog.view.model()
		if indexes:
			selectedRows = list(sorted(set([index.row() for index in indexes])))
			_result = []
			for iRow in selectedRows:
				_result.append([model.data(model.index(iRow, iCol)) for iCol in range(model.columnCount())])

		menu = QtGui.QMenu()
		action = QtGui.QAction("Связанные точки", menu) 
		action.triggered.connect(lambda: self.select_grouped_point(_result))
		menu.addAction(action)
		menu.exec_(self.dialog.view.viewport().mapToGlobal(position))

	def select_grouped_point(self, rows):
		row = rows[0]
		pid = row[0]
		cmid = row[1]

		cmd = """
			SELECT
				BASEPOINT_ID, CREATEMEASURE_ID
			FROM [{0}].[dbo].[MP_BASE_POINTS] AS P
			WHERE BASE_POINT_GROUP_ID = 
				(
					SELECT TOP 1
						BASE_POINT_GROUP_ID
					FROM [{0}].[dbo].[MP_BASE_POINTS] AS P_G 
					WHERE BASEPOINT_ID = ? 
					AND CREATEMEASURE_ID = ?
				)
		""".format(self._cnxn.dbsvy)
		params = (pid, cmid)
		group_ids = self._cnxn.exec_query(cmd, params).get_rows()
		group_ids_single = [str(x[0])+'-'+str(x[1]) for x in group_ids]

		model = self.dialog.view.model()
		data = model._data
		filteredData =   [row for row in data if str(row[0])+'-'+str(row[1])  in group_ids_single]

		header = self._data[1]
		model = CustomDataModel(self.dialog.view, filteredData, header)
		self.dialog.view.updateModel(model)

	@property
	def measure(self):				return self._measure
	@measure.setter
	def measure(self, m):			self._set_measure(m)
	def _set_measure(self, m):
		if not isinstance(m, ControlMeasure):
			raise TypeError("%s - неверный тип. Ожидались ControlMeasure или DirectionMeasure" % str(type(m)))
		self._measure = m

		# сначала заносим параметры, а затем задаем точки
		# порядок важен, т.к. при задании точки вызывается doCalculation,
		# и все параметры считываются из грида, перезатирвая только что присвоенные

		self.setParams()

		self.setStandPoint(m.standpoint)
		self.setBackPoint(m.backpoint)
		self.setDirPoint(m.dirpoint)
	
	def getParams(self):
		# конвертируем из текста
		cells = [(N_HEADER_ROWS, 2), (N_HEADER_ROWS+1, 3), (N_HEADER_ROWS+2, 3)]
		for irow, icol in cells:
			value = self.item(irow, icol).text()
			if value and not is_number(value):
				QtGui.QMessageBox.critical(self, "Ошибка", "Неверное значение")
				self.blockSignals(True)
				self.item(irow, icol).setText("")
				self.blockSignals(False)
				return

		# считываем данные из грида
		vdist = self.item(N_HEADER_ROWS, 2).text()
		vang = self.item(N_HEADER_ROWS+3, 2).text()
		ioff = self.item(N_HEADER_ROWS+1, 3).text()
		voff = self.item(N_HEADER_ROWS+2, 3).text()
		hang = self.item(N_HEADER_ROWS+3, 6).text()

		self._measure.vdist = float(vdist) if vdist else None
		vangle = geom.Angle.from_text(vang)
		#self._measure.vangle = 90-vangle if vangle else None				# вертикальный угол измеряем от горизонтали
		self._measure.vangle = vangle if vangle else None
		self._measure.ioffset = float(ioff) if ioff else None
		self._measure.voffset = float(voff) if voff else None
		self._measure.hangle = geom.Angle.from_text(hang) if hang else None
	def setParams(self):
		# преобразуем данные в текст
		vdist = "" if self._measure.vdist is None else "%.3f" % self._measure.vdist
		#vang = "" if self._measure.vangle is None else (90-self._measure.vangle).to_text()	# вертикальный угол измеряем от горизонтали
		vang = "" if self._measure.vangle is None else self._measure.vangle.to_text()
		ioff = "" if self._measure.ioffset is None else "%.3f" % self._measure.ioffset
		voff = "" if self._measure.voffset is None else "%.3f" % self._measure.voffset
		hang = "" if self._measure.hangle is None else self._measure.hangle.to_text()

		# вставляем данные в грид
		self.blockSignals(True)
		self.item(N_HEADER_ROWS, 2).setText(vdist)
		self.item(N_HEADER_ROWS+3, 2).setText(vang)
		self.item(N_HEADER_ROWS+1, 3).setText(ioff)
		self.item(N_HEADER_ROWS+2, 3).setText(voff)
		self.item(N_HEADER_ROWS+3, 6).setText(hang)
		self.blockSignals(False)

		# производим вычисления
		self.doCalculation()

	def setStandPoint(self, pt):
		self._measure.standpoint = pt
		self.item(N_HEADER_ROWS+1, 0).setText(self._measure.standpoint.name)
	def setBackPoint(self, pt):
		self._measure.backpoint = pt
		self.item(N_HEADER_ROWS, 1).setText(self._measure.backpoint.name)
	def setDirPoint(self, pt):
		self._measure.dirpoint = pt
		self.item(N_HEADER_ROWS+3, 1).setText(self._measure.dirpoint.name)

	def doCalculation(self):
		# проверяем на правильность ввода углы
		cells = [(N_HEADER_ROWS+3, 2), (N_HEADER_ROWS+3, 6)]
		for irow, icol in cells:
			value = self.item(irow, icol).text()
			if value:
				angle = geom.Angle.from_text(value)
				if angle is None or (icol == 2 and not (0 <= angle <= 180)):
					self.blockSignals(True)
					self.item(irow, icol).setText("")
					self.blockSignals(False)
					QtGui.QMessageBox.critical(self, "Ошибка", "[%s] - неверный формат угла" % value)
					return
				else:
					self.blockSignals(True)
					self.item(irow, icol).setText(angle.to_text())
					self.blockSignals(False)

		# считываем данные
		self.getParams()
		
		# производим вычисления
		hang = self._measure.calc_hangle()
		truedist = self._measure.calc_true_hdist()
		dist = self._measure.calc_hdist()
		truedz = self._measure.calc_true_dz()
		dz = self._measure.calc_dz()

		# записываем данные
		self.blockSignals(True)
		self.item(N_HEADER_ROWS, 4).setText("%.3f" % truedist if truedist else "")
		self.item(N_HEADER_ROWS, 5).setText("%.3f" % truedz if truedz else "")
		self.item(N_HEADER_ROWS, 6).setText(hang.to_text() if hang else "")
		self.item(N_HEADER_ROWS+3, 4).setText("%.3f" % dist if dist else "")
		self.item(N_HEADER_ROWS+3, 5).setText("%.3f" % dz if dz else "")

		self.item(N_HEADER_ROWS+3, 6).setIsValid(self._measure.angle_satisfies)
		self.item(N_HEADER_ROWS+3, 4).setIsValid(self._measure.distance_satisfies)

		self.blockSignals(False)

	def to_list(self, header = False):
		data = []
		firstrow = N_HEADER_ROWS if not header else 0
		for irow in range(firstrow, self.rowCount()):
			row = [self.item(irow, icol).text() if self.item(irow, icol) else "" for icol in range(self.columnCount())]
			row.extend([""]*3)
			if (irow-firstrow) % 4 == 0:	row.extend([round(self.measure.backpoint.x, 3), round(self.measure.backpoint.y, 3), round(self.measure.backpoint.z, 3)])
			if (irow-firstrow) % 4 == 1:	row.extend([round(self.measure.standpoint.x, 3), round(self.measure.standpoint.y, 3), round(self.measure.standpoint.z, 3)])
			if (irow-firstrow) % 4 == 2:	row.extend([""]*3)
			if (irow-firstrow) % 4 == 3:	row.extend([round(self.measure.dirpoint.x, 3), round(self.measure.dirpoint.y, 3), round(self.measure.dirpoint.z, 3)])
			data.append(row)
		return data
	def index(self):
		return 0

class SvyDirectionGrid(SvyControlGrid):
	def __init__(self, *args, **kw):
		super(SvyDirectionGrid, self).__init__(*args, **kw)

		self.setColumnCount(13)
		self.setRowCount(N_HEADER_ROWS + 2*N_POINT_ROWS)

		self.__createVariables()
		self.__buildGrid()

		self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.customContextMenuRequested.connect(self.displayMenu)

	def displayMenu(self, position):
		item = self.itemAt(position)
		if item.row() == N_HEADER_ROWS+3 and item.column() == 1 and self._window.isEditingMode():
			menu = QtGui.QMenu()
			action = QtGui.QAction("Перекоординировать", menu) 
			action.triggered.connect(lambda: self._setOvercoordinationProp(True))
			menu.addAction(action)
			if self._measure.dirpoint.isNeedOvercoordinate == True:
				action.setEnabled(False)
				
			menu.exec_(self.viewport().mapToGlobal(position))

	def _setOvercoordinationProp(self, choice):
		self._measure.dirpoint.isNeedOvercoordinate = choice

		if self._measure.dirpoint.isNeedOvercoordinate and self._isNeedEditOvercoordinationDirpointName():
			self.item(N_HEADER_ROWS+3, 1).setBackgroundColor(INVALID_COLOR)
			self.item(N_HEADER_ROWS+3, 1).setReadOnly(False)
		else:
			self.item(N_HEADER_ROWS+3, 1).setReadOnly(True)


	def _isNeedEditOvercoordinationDirpointName(self): 
		windowSvyEditDirection = self.parent().parent().parent().parent().parent()
		traversal_measure = windowSvyEditDirection.traversal_measure
		isPointNameNeedToEdit = traversal_measure.isPointNameNeedToEdit(self._measure.dirpoint)
		return 	isPointNameNeedToEdit

	def __createVariables(self):
		stpt = SvyPoint(self._cnxn)
		bkpt = SvyPoint(self._cnxn)
		fwpt = SvyPoint(self._cnxn)
		self._measure = DirectionMeasure(stpt, bkpt, fwpt)	

		#self._data = self._window.data["POINTS_REMOVED"]
		self._data = self._window.data["POINTS"]

		self._disabledCells = (	(0,0), (0,4), (0,5), (0,6), (0,8), (0,9),
								(1,1), (1,2), (1,4), (1,5), (1,6), (1,7), (1,8), (1,9),
								(2,0), (2,2), (2,4), (2,5), (2,6), (2,7), (2,8), (2,9), (2,10), (2,11), (2,12),
								(3,3),
								(4,0), (4,1), (4,4), (4,5), (4,6), (4,7), (4,8), (4,9), (4,10), (4,11), (4,12),
								(5,0), (5,1), (5,2), (5,6), (5,7), (5,8), (5,9), (5,10), (5,11), (5,12),
								(6,0), (6,1), (6,2), (6,4), (6,5), (6,6), (6,7), (6,8), (6,9), (6,10), (6,11), (6,12),
								(7,0), (7,1), (7,3), (7,6), (7,7), (7,8), (7,9), (7,10), (7,11), (7,12)	)
		# if self._window.isEditingMode():
		# 	self._disabledCells += ((3,1),)
		# 	self._disabledCells += ((0,1), (1,0))

		self._calcCells = (	(0,3), (0,7), (0,10), (0,11), (0,12),
							(1,10), (1,11), (1,12),
							(3,4), (3,5), (3,7), (3,8), (3,9), (3,10), (3,11), (3,12),
							(4,3),
							(5,4), (5,5),
							(7,4), (7,5)	)

		self._userCells = (	(0,2),
							(1,3),
							(2,0), (2,3), 
							(3,2), (3,6),
							(4,2), 
							(5,3), 
							(6,3), 
							(7,2)	)
		if not self._window.isEditingMode():
			self._userCells += ((3,1),)

		self._cellOrder = [[3,1], [0,2], [1,3], [2,3], [3,2], [3,6], [4,2], [5,3], [6,3], [7,2]]
		for i in range(len(self._cellOrder)):
			self._cellOrder[i][0] += N_HEADER_ROWS
	
	def __buildGrid(self):
		self.__buildHeader()
		self.__buildBody()
	def __buildHeader(self):
		self.blockSignals(True)
		for i in range(7, self.columnCount()):
			self.setColumnWidth(i, 70)

		self.setSpan(0,7,2,1)
		self.setSpan(0,8,1,2)
		self.setSpan(0,10,1,3)

		header = ["Дирек.\nугол", "Приращения", "Координаты"]
		iCols = [7, 8, 10]
		for iCol, name in zip(iCols, header):
			item = QtGui.QTableWidgetItem(name)
			item.setTextAlignment(QtCore.Qt.AlignCenter)
			item.setFlags(QtCore.Qt.ItemIsEnabled)
			item.setFont(self._headerFont)
			self.setItem(0, iCol, item)

		header = ["Y", "X", "Y", "X", "Z"]
		iCols = [8, 9, 10, 11, 12]
		for iCol, name in zip(iCols, header):
			item = QtGui.QTableWidgetItem(name)
			item.setTextAlignment(QtCore.Qt.AlignCenter)
			item.setFlags(QtCore.Qt.ItemIsEnabled)
			item.setFont(self._headerFont)
			self.setItem(1, iCol, item)
		self.blockSignals(False)
	def __buildBody(self):
		self.blockSignals(True)
		for irow, icol in self._userCells:		self.setItem(N_HEADER_ROWS + irow, icol, SvyGridItem(USER_ITEM))
		for irow, icol in self._calcCells:		self.setItem(N_HEADER_ROWS + irow, icol, SvyGridItem(CALC_ITEM))
		for irow, icol in self._disabledCells:	self.setItem(N_HEADER_ROWS + irow, icol, SvyGridItem(DISABLED_ITEM))

		self.item(N_HEADER_ROWS + 1, 0).setReadOnly(True)
		self.item(N_HEADER_ROWS, 1).setReadOnly(True)
		self.item(N_HEADER_ROWS + 3, 1).setReadOnly(self._window.isEditingMode())

		self.item(N_HEADER_ROWS, 1).setFont(self._headerFont)
		self.item(N_HEADER_ROWS + 1, 0).setFont(self._headerFont)
		self.item(N_HEADER_ROWS + 3, 1).setFont(self._headerFont)
		self.item(N_HEADER_ROWS + 5, 0).setFont(self._headerFont)
		self.item(N_HEADER_ROWS + 7, 1).setFont(self._headerFont)

		self.item(N_HEADER_ROWS + 4, 4).setText("Среднее")
		self.item(N_HEADER_ROWS + 4, 4).setFont(self._headerFont)
		self.item(N_HEADER_ROWS + 4, 5).setText("Среднее")
		self.item(N_HEADER_ROWS + 4, 5).setFont(self._headerFont)
		self.blockSignals(False)

	def clear(self):
		"""Очищаем введенные параметры, кроме самих точек"""
		self.blockSignals(True)
		for irow, icol in self._userCells:
			self.item(N_HEADER_ROWS + irow, icol).setText("")
		self.blockSignals(False)

		self._measure.clear()
		self._measure.dirpoint.name = ""
		self.setDirPoint(self._measure.dirpoint)

		self.doCalculation()

	@property
	def hasResult(self):
		return self._measure.calc_coords() is not None

	def setStandPoint(self, pt):
		super(SvyDirectionGrid, self).setStandPoint(pt)
		self.blockSignals(True)
		self.item(N_HEADER_ROWS+7, 1).setText(self._measure.standpoint.name)
		if pt.name:
			self.item(N_HEADER_ROWS+1, 10).setText("%.3f" % pt.x if not math.isnan(pt.x) else "")
			self.item(N_HEADER_ROWS+1, 11).setText("%.3f" % pt.y if not math.isnan(pt.y) else "")
			self.item(N_HEADER_ROWS+1, 12).setText("%.3f" % pt.z if not math.isnan(pt.z) else "")
		else:
			self.measure.standpoint.delete_coords()
			self.item(N_HEADER_ROWS+1, 10).setText("")
			self.item(N_HEADER_ROWS+1, 11).setText("")
			self.item(N_HEADER_ROWS+1, 12).setText("")
		self.blockSignals(False)
	def setBackPoint(self, pt):
		super(SvyDirectionGrid, self).setBackPoint(pt)
		self.blockSignals(True)
		if pt.name:
			self.item(N_HEADER_ROWS, 10).setText("%.3f" % pt.x if not math.isnan(pt.x) else "")
			self.item(N_HEADER_ROWS, 11).setText("%.3f" % pt.y if not math.isnan(pt.y) else "")
			self.item(N_HEADER_ROWS, 12).setText("%.3f" % pt.z if not math.isnan(pt.z) else "")
		else:
			self.measure.backpoint.delete_coords()
			self.item(N_HEADER_ROWS, 10).setText("")
			self.item(N_HEADER_ROWS, 11).setText("")
			self.item(N_HEADER_ROWS, 12).setText("")
		self.blockSignals(False)
	def setDirPoint(self, pt):
		super(SvyDirectionGrid, self).setDirPoint(pt)
		self.blockSignals(True)
		self.item(N_HEADER_ROWS+5, 0).setText(self._measure.dirpoint.name)
		#if self._measure.dirpoint.name_exists and not self._measure.dirpoint.name_removed:	 
		if self._isNeedEditOvercoordinationDirpointName():	 
			self.item(N_HEADER_ROWS+3, 1).setBackgroundColor(INVALID_COLOR)
		else:
			self.item(N_HEADER_ROWS+3, 1).setBackgroundColor(USER_COLOR)
		self.blockSignals(False)


	def getParams(self):
		# конвертируем из текста
		cells = [(N_HEADER_ROWS+4, 2), (N_HEADER_ROWS+5, 3), (N_HEADER_ROWS+6, 3)]
		for irow, icol in cells:
			value = self.item(irow, icol).text()
			if value and not is_number(value):
				QtGui.QMessageBox.critical(self, "Ошибка", "Неверное значение")
				self.blockSignals(True)
				self.item(irow, icol).setText("")
				self.blockSignals(False)
				return

		# считываем из грида данные обратного замера
		vdistInv = self.item(N_HEADER_ROWS+4, 2).text()
		vangInv = self.item(N_HEADER_ROWS+7, 2).text()
		ioffInv = self.item(N_HEADER_ROWS+5, 3).text()
		voffInv = self.item(N_HEADER_ROWS+6, 3).text()

		# конвертируем из текста
		self._measure.vdistInv = float(vdistInv) if vdistInv else None
		vangle = geom.Angle.from_text(vangInv)
		#self._measure.vangleInv = 90-vangle if vangle else None			# вертикальный угол измеряем от горизонтали
		self._measure.vangleInv = vangle if vangle else None
		self._measure.ioffsetInv = float(ioffInv) if ioffInv else None
		self._measure.voffsetInv = float(voffInv) if voffInv else None

		# считываем из грида данные прямого замера
		super(SvyDirectionGrid, self).getParams()
	def setParams(self):
		# преобразуем данные обратного замера в текст
		vdistInv = "" if self._measure.vdistInv is None else "%.3f" % self._measure.vdistInv
		#vangInv = "" if self._measure.vangleInv is None else (90-self._measure.vangleInv).to_text()	# вертикальный угол измеряем от горизонтали
		vangInv = "" if self._measure.vangleInv is None else self._measure.vangleInv.to_text()
		ioffInv = "" if self._measure.ioffsetInv is None else "%.3f" % self._measure.ioffsetInv
		voffInv = "" if self._measure.voffsetInv is None else "%.3f" % self._measure.voffsetInv

		# вставляем данные в грид
		self.blockSignals(True)
		self.item(N_HEADER_ROWS+4, 2).setText(vdistInv)
		self.item(N_HEADER_ROWS+7, 2).setText(vangInv)
		self.item(N_HEADER_ROWS+5, 3).setText(ioffInv)
		self.item(N_HEADER_ROWS+6, 3).setText(voffInv)
		self.blockSignals(False)

		# вставляем данные прямого замера в грид
		super(SvyDirectionGrid, self).setParams()

		# производим вычисления
		self.doCalculation()

	def selectPoint(self, item):
		irow, icol = item.row(), item.column()
		cell = (irow, icol)
		index = self.parent().indexOf(self)

		pt = super(SvyDirectionGrid, self).selectPoint(item)

		irow, icol = item.row(), item.column()
		if pt and irow == N_HEADER_ROWS+3 and icol == 1:
			item.setReadOnly(True)
			self._setOvercoordinationProp(True)
		return pt
	def doCalculation(self):
		super(SvyDirectionGrid, self).doCalculation()

		self.blockSignals(True)
		self.item(N_HEADER_ROWS+3, 6).setIsValid(USER_COLOR)
		self.item(N_HEADER_ROWS+3, 4).setIsValid(USER_COLOR)
		self.blockSignals(False)

		# проверяем на правильность ввода углы
		cells = [(N_HEADER_ROWS+7, 2)]
		for irow, icol in cells:
			value = self.item(irow, icol).text()
			if value:
				angle = geom.Angle.from_text(value)
				if angle is None or not (0 <= angle <= 180):
					self.blockSignals(True)
					self.item(irow, icol).setText("")
					self.blockSignals(False)
					QtGui.QMessageBox.critical(self, "Ошибка", "[%s] - неверный формат угла" % value)
					return
				else:
					self.blockSignals(True)
					self.item(irow, icol).setText(angle.to_text())
					self.blockSignals(False)

		# производим вычисления
		lsin = self._measure.calc_lsin()
		lsinInv = self._measure.calc_lsinInv()
		dzInv = self._measure.calc_dzInv()
		avgdz = self._measure.calc_dzAvg()
		hdistInv = self._measure.calc_hdistInv()
		avgdist = self._measure.calc_hdistAvg()
		bkdirangle = self._measure.calc_bkdirangle()
		dirangle = self._measure.calc_dirangle()
		dx = self._measure.calc_dx()
		dy = self._measure.calc_dy()
		
		# вычисляем координаты (координаты присваиваются внутри функции)
		pt = self._measure.calc_coords()

		self.blockSignals(True)
		self.item(N_HEADER_ROWS, 4).setText("")
		self.item(N_HEADER_ROWS, 5).setText("")
		self.item(N_HEADER_ROWS, 6).setText("")

		self.item(N_HEADER_ROWS, 3).setText("" if lsin is None else "%.3f" % lsin)
		self.item(N_HEADER_ROWS, 7).setText("" if bkdirangle is None else bkdirangle.to_text())
		self.item(N_HEADER_ROWS+3, 7).setText("" if dirangle is None else dirangle.to_text())
		self.item(N_HEADER_ROWS+3, 8).setText("" if dx is None else "%.3f" % dx)
		self.item(N_HEADER_ROWS+3, 9).setText("" if dy is None else "%.3f" % dy)
		self.item(N_HEADER_ROWS+3, 10).setText("" if pt is None else "%.3f" % pt.x)
		self.item(N_HEADER_ROWS+3, 11).setText("" if pt is None else "%.3f" % pt.y)
		self.item(N_HEADER_ROWS+3, 12).setText("" if pt is None else "%.3f" % pt.z)
		self.item(N_HEADER_ROWS+4, 3).setText("" if lsinInv is None else "%.3f" % lsinInv)
		self.item(N_HEADER_ROWS+5, 4).setText("" if avgdist is None else "%.3f" % avgdist)
		self.item(N_HEADER_ROWS+5, 5).setText("" if avgdz is None else "%.3f" % avgdz)
		self.item(N_HEADER_ROWS+7, 4).setText("" if hdistInv is None else "%.3f" % hdistInv)
		self.item(N_HEADER_ROWS+7, 5).setText("" if dzInv is None else "%.3f" % dzInv)

		self.item(N_HEADER_ROWS+3, 6).setIsValid(True)
		self.item(N_HEADER_ROWS+3, 4).setIsValid(True)
		self.blockSignals(False)

		self._window.frameCalculation.setActions()

	def to_list(self, header = False):
		firstrow = N_HEADER_ROWS if not header else 0
		data = []
		for irow in range(firstrow, self.rowCount()):
			row = [self.item(irow, icol).text() if self.item(irow, icol) else "" for icol in range(self.columnCount())]
			data.append(row)
		return data
	def index(self):
		return self.parent().indexOf(self)

class SvyGridStackWidget(QtGui.QStackedWidget):
	def __init__(self, *args, **kw):
		super(SvyGridStackWidget, self).__init__(*args, **kw)
		self._grids = []
	def addGrid(self, grid):
		self.addWidget(grid)
		# переключаемся на только что добавленную вкладку
		# чтобы во время задания задней точки и точки стояния
		# данные записались именно в последний грид, а не в текущий
		self.setCurrentIndex(self.currentIndex() + 1)
		self._grids.append(grid)

		if self.gridsCount > 1:
			self._grids[-1].setStandPoint(self._grids[-2].measure.dirpoint)
			self._grids[-1].setBackPoint(self._grids[-2].measure.standpoint)
			# генерируем следующий номер точки
			try:
				self._grids[-1].measure.dirpoint.name = int(self._grids[-1].measure.standpoint.name) + 1
				self._grids[-1].setDirPoint(self._grids[-1].measure.dirpoint)
			except Exception as e:
				pass

		# возвращаемся на текущую вкладку
		self.setCurrentIndex(self.currentIndex() - 1)
	def clear(self):
		self._grids[0].clear()
		self.doCalculation()
	@property
	def grids(self):			return tuple(self._grids)
	@property
	def gridsCount(self):		return len(self._grids)
	def deleteGrid(self, index):
		if index > 0:
			for i in range(self.gridsCount-1, index-1, -1):
				self._grids[i].setParent(None)
				self._grids.pop()
		else:
			self.clear()
		self.grids[-1].doCalculation()
	def doCalculation(self, item):
		# проверяем, если было изменено имя передней точки
		# по какой-то причине в каких-то случаях текущим гридом оказывается ControlGrid,
		# в то время как работает в DirectionGrid. Не нашел проблему, поэтому проверяем
		# сначала, еслить текущий грид в списке
		currgrid = item.tableWidget()
		if currgrid in self._grids:
			currindex = self._grids.index(currgrid)
			irow, icol = item.row(), item.column()
			if irow == N_HEADER_ROWS + 3 and icol == 1:
				self._grids[currindex].measure.dirpoint.name = item.text()
				self._grids[currindex].setDirPoint(self._grids[currindex].measure.dirpoint)

		self._grids[0].doCalculation()
		for i in range(1, self.gridsCount):
			self._grids[i].setStandPoint(self._grids[i-1].measure.dirpoint)
			self._grids[i].setBackPoint(self._grids[i-1].measure.standpoint)
			self._grids[i].doCalculation()

		main_app = self.parent().parent().parent().parent()
		main_app.frameProject.doCalculation()

	@property
	def hasResult(self):
		return all(map(lambda grid: grid.hasResult, self._grids))

	def to_list(self, header = False):
		data = []
		for grid in self.grids:
			data.extend(grid.to_list(header))
		return data
