from gui.qt.widgets.TreeModel import *
from gui.qt.widgets.TreeModelItem import createTreeModelItem
from utils.logger import NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

# Добавляем линии между строчками в таблице
class GridDelegate(QtGui.QStyledItemDelegate):
    def paint(self, painter, option, index):
        painter.save()
        painter.setPen(QtGui.QColor(200, 200, 200))
        painter.drawRect(option.rect)
        painter.restore()

        super(GridDelegate, self).paint(painter, option, index)


class BackupExplorer(QtGui.QWidget):
    def __init__(self, *args, **kw):
        super(BackupExplorer, self).__init__(*args, **kw)

        self.__createVariables()
        self.__createWidgets()
        self.__createBindings()
        self.__gridWidgets()

        self._buildheader()

    def __createVariables(self):
        self._empty_color = QtGui.QColor(255, 255, 255, 0)
        self._select_color = QtGui.QColor(255, 255, 0)
        self._status_color = QtGui.QColor(0, 255, 0)
        self._main_app = iterWidgetParents(self)[-1]
        self._selected_units = []

    def __createWidgets(self):
        self.layoutMain = QtGui.QVBoxLayout()
        self.layoutHeader = QtGui.QHBoxLayout()
        self.labelExistNeedTransferBM = QtGui.QLabel(self)
        self.labelExistNeedTransferBM.setText("Есть модели для фиксации")
        self.labelExistNeedTransferBM.setStyleSheet('color: red; font:bold')
        self.labelExistNeedTransferBM.setVisible(False)
        self.explorer = TreeWidget(self)
        self.explorer.setAlternatingRowColors(False)
        self.explorer.header().setStretchLastSection(False)
        self.explorer.setSelectionBehavior(QtGui.QTreeView.SelectItems)
        self.explorer.setItemDelegate(GridDelegate())

    def __createBindings(self):
        self.explorer.itemClicked.connect(self.onClickItem)

    def __gridWidgets(self):
        self.setLayout(self.layoutMain)
        self.layoutMain.setSpacing(2)
        self.layoutMain.setContentsMargins(0, 0, 0, 0)
        self.layoutMain.addLayout(self.layoutHeader)
        self.layoutHeader.addWidget(self.labelExistNeedTransferBM)
        self.layoutMain.addWidget(self.explorer)

    def _buildheader(self):
        columns = ["Имя", "Дата посл. фикс. (месяц)", "Дата посл. фикс. (год)"]
        rmodes = [QtGui.QHeaderView.Stretch, QtGui.QHeaderView.ResizeToContents, QtGui.QHeaderView.ResizeToContents]

        for icol, (cname, rmode) in enumerate(zip(columns, rmodes)):
            self.explorer.headerItem().setText(icol, cname)
            self.explorer.headerItem().setTextAlignment(icol, QtCore.Qt.AlignCenter)
            self.explorer.header().setResizeMode(icol, rmode)
            self.explorer.headerItem().setTextAlignment(icol, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.explorer.setColumnWidth(0, 250)

    def onClickItem(self, item, column):
        if not item.isDirectory():
            return

        self.setSelectedUnits(item)

    # Вызывается когда в application.py идет вызов bckpex.setVisible(True)
    # Или, например, когда сворачиваем, а затем снова разворачиваем окно
    def showEvent(self, event):
        # обновление окна при смене Залежи
        # self._main_app.mainWidget.modelExplorer.standard_models[0].comboBoxLayer.currentIndexChanged.connect(
        #     self.update)

        if not self._main_app.srvcnxn:
            # подключаемся к БД
            self._main_app.connect_to_database()

        if not self._main_app.srvcnxn:
            qtmsgbox.show_error(self._main_app, "Ошибка", "Невозможно подключиться к базе данных")
            return
        #
        # Проверка наличия полей COMMIT_STATUS и COMMIT_DATE в БД таблице NM_BLOCKMODEL_METADATA
        # Закомментирована возможность добавление этих полей
        # В будущем можно убрать эту проверку
        # cnxn = self._main_app.srvcnxn
        # backup_bm_columns = {'COMMIT_STATUS':'nvarchar(30)', 'COMMIT_DATE':'date'}
        # for column in backup_bm_columns:
        #     if not cnxn.exec_query("SELECT COL_LENGTH('[%s].[dbo].[NM_BLOCKMODEL_METADATA]', '%s')" % (cnxn.dbsvy, column)).get_rows()[0][0]:
        #         qtmsgbox.show_error(self._main_app, "Ошибка", "В таблице [%s].[dbo].[NM_BLOCKMODEL_METADATA] отсутствует поле %s!" % (cnxn.dbsvy, column))
        #         return
                # cnxn.exec_query("ALTER TABLE [%s].[dbo].[NM_BLOCKMODEL_METADATA] ADD %s %s DEFAULT Null" %(cnxn.dbsvy, column, backup_bm_columns[column]))
                # cnxn.commit()
        #

        self.update()
        super(BackupExplorer, self).showEvent(event)

    def update(self):
        self._selected_units = []
        self._layer = self._main_app.mainWidget.modelExplorer.standard_models[0].comboBoxLayer.currentText()
        self._prefix = self._main_app.settings.getValue(ID_GEO_UPREFIX)

        cnxn = self._main_app.srvcnxn
        if not cnxn:
            return

        columns = ["[NAME]", "[LAYER]", "[GEOUNIT]", "[PATH]", "[CURRENT_DATE]"]
        columns_fix = ["[NAME]", "[LAYER]", "[GEOUNIT]", "[PATH]", "[COMMIT_DATE]"]
        self._bmdata_fix_monthly = cnxn.exec_query(
            "SELECT %s FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [LAYER] = N'%s' and [COMMIT_STATUS] = 'MONTHLY'" % (
                ", ".join(columns_fix), cnxn.dbsvy, self._layer)).get_rows()
        self._bmdata_fix_annual = cnxn.exec_query(
            "SELECT %s FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [LAYER] = N'%s' and [COMMIT_STATUS] = 'ANNUAL'" % (
                ", ".join(columns_fix), cnxn.dbsvy, self._layer)).get_rows()
        self._bmdata = cnxn.exec_query(
            "SELECT %s FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [LAYER] = N'%s' and [COMMIT_STATUS] is NULL" % (
                ", ".join(columns), cnxn.dbsvy, self._layer)).get_rows()
        self._buildtree()

    def _buildtree(self):
        self._buildtable(self._bmdata, self._bmdata_fix_monthly, self._bmdata_fix_annual)

    def _buildtable(self, values, values_m, values_y):
        if not values:
            self.explorer.clear()
            return

        self.explorer.clear()

        unitItemDict, bmItemDict = {}, {}
        bmNames, layerNames, unitNames, paths, currdates = zip(*values)
        for bmName, unitName, path, currdate in zip(bmNames, unitNames, paths, currdates):
            # пропускаем несуществующие блочные модели
            if not os.path.exists(path):
                continue
            # добавляем папку юнита, если необходимо
            if not unitName in unitItemDict:
                item = createTreeModelItem(self.explorer, self._prefix + unitName, ITEM_DIR, ITEM_CATEGORY_GEO)
                item.setExpanded(True)
                unitItemDict[unitName] = item
            # добавляем блочную модель
            item = createTreeModelItem(unitItemDict[unitName], bmName, ITEM_BM, ITEM_CATEGORY_GEO)
            item.setFilePath(path)

            for bmName_fix_m, layerName_fix_m, unitName_fix_m, path_fix_m, commdate_m in values_m:
                if bmName_fix_m == bmName and unitName_fix_m == unitName:
                    if all(map(os.path.exists, [path_fix_m, os.path.dirname(path_fix_m) + "\\%s.STR" % bmName_fix_m, os.path.dirname(os.path.dirname(path_fix_m)) + '\\' + \
                            self._main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1] + "\\%s.TRIDB" % bmName_fix_m])):
                        item.setText(1, '-'.join(commdate_m.split('-')[:2]))
                        break

            for bmName_fix_y, layerName_fix_y, unitName_fix_y, path_fix_y, commdate_y in values_y:
                if bmName_fix_y == bmName and unitName_fix_y == unitName:
                    if all(map(os.path.exists, [path_fix_y, os.path.dirname(path_fix_y) + "\\%s.STR" % bmName_fix_y,
                                                os.path.dirname(os.path.dirname(path_fix_y)) + '\\' + \
                                                        self._main_app.settings.getValue(ID_GEO_USUBDIRS)[1][
                                                            1] + "\\%s.TRIDB" % bmName_fix_y])):
                        item.setText(2, '-'.join(commdate_y.split('-')[:2]))
                        break
            [item.setTextAlignment(_, QtCore.Qt.AlignCenter) for _ in range(1,3)]

            bmItemDict[(unitName, bmName)] = item

    def setSelectedUnits(self, item):
        item_unit_name = item.text(0).split(self._prefix)[1]
        # снимаем выделение, если пользователь выбрал уже выбранную ячейку
        if item_unit_name in self._selected_units:
            item.setBackgroundColor(0, self._empty_color)
            self._selected_units.pop(self._selected_units.index(item_unit_name))
            return
        #
        # закрашиваем ячейку в 0-м столбце
        color = self._select_color
        item.setBackgroundColor(0, color)
        self._selected_units.append(item_unit_name)

    @property
    def selected_units(self):
        return self._selected_units

    @property
    def selected_layer(self):
        return self._layer

    @property
    def unit_prefix(self):
        return self._prefix

    def populate(self, *args, **kwargs):
        pass