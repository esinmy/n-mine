from PyQt4 import QtGui, QtCore

from utils.constants import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.TreeModel import *
from gui.qt.widgets.TreeExplorers.TreeExplorer import TreeExplorer
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class CrossSectionExplorer(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(CrossSectionExplorer, self).__init__(*args, **kw)
		self.__createVariables()
		self.__createWidgets()
		self.__gridWidgets()
	def __createVariables(self):
		self._droppedItem = None
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setText("Выберите поперечные разрезы")

		self.explorer = TreeExplorer(self)
		self.explorer.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.explorer.dropEvent = lambda e: self.dropEvent(e)
		self.explorer.setDragDropMode(QtGui.QAbstractItemView.DropOnly)
		self.explorer.headerItem().setText(0, "Имя")
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.labelHint)
		self.layoutMain.addWidget(self.explorer)

	def _dropItem(self, item):
		return self.explorer.dropItem(item)
	def _dropItems(self, items):
		droppedItems = []
		for item in items:
			if item.isSectionPack():
				item.readSectionNames()
				for it in item.iterChildren():
					droppedItems.append(self._dropItem(it))
			else:
				droppedItems.append(self._dropItem(item))
		return droppedItems
		
	def dropEvent(self, event):
		sourceModel = event.source()
		secitems = list(filter(lambda it: it.isSection() or it.isSectionPack(), sourceModel.selectedItems()))
		self._dropItems(secitems)
		self.explorer.expandAll()

class LongSectionExplorer(CrossSectionExplorer):
	def __init__(self, *args, **kw):
		super(LongSectionExplorer, self).__init__(*args, **kw)
		self.__createWidgets()
		self.__createBindings()
	def __createWidgets(self):
		self.labelHint.setText("Выберите продольные разрезы")
		self.explorer.header().setStretchLastSection(False)
		self.explorer.headerItem().setText(1, "Опорный")
		self.explorer.header().setResizeMode(0, QtGui.QHeaderView.Stretch)
		self.explorer.header().setResizeMode(1, QtGui.QHeaderView.Fixed)
		self.explorer.setColumnWidth(1, 80)
	def __createBindings(self):
		self.explorer.itemChanged.connect(self.itemChanged)

	# drop
	def _dropItem(self, item):
		droppedItem = super(LongSectionExplorer, self)._dropItem(item)
		droppedItem.checkboxBasis = CheckBoxItem(self.explorer, droppedItem, 1)
		droppedItem.checkboxBasis.setChecked(droppedItem.basis())
	def itemChanged(self, item, column):
		if column == 1:
			text = item.text(column)
			flag = True if text == "1" else False
			item.setBasis(flag)

class SectionPlotExplorer(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(SectionPlotExplorer, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.longSectionExplorer = LongSectionExplorer(self)
		self.crossSectionExplorer = CrossSectionExplorer(self)
	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.longSectionExplorer)
		self.layoutMain.addWidget(self.crossSectionExplorer)

	def clear(self):
		self.longSectionExplorer.explorer.clear()
		self.crossSectionExplorer.explorer.clear()