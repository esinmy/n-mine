try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore

from utils.constants import *
from utils.validation import DoubleValidator
from mm.mmutils import MicromineFile

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog
from gui.qt.widgets.TreeModel import *
from gui.qt.widgets.TreeModelItem import createTreeModelItem, TreeModelStockItem, TreeModelStockSectorItem
from gui.qt.widgets.TreeExplorers.TreeExplorer import TreeExplorer
from utils.logger import NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class StockExplorerDelegate(QtGui.QItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()
		self.editor = QtGui.QLineEdit(widget)
		if icol == 1:			validator = DoubleValidator(-1e6, 100, 3)
		else:					return super(StockExplorerDelegate, self).createEditor(widget, option, index)

		self.editor.setValidator(validator)
		return self.editor

class StockDialog(BaseDialog):
	def __init__(self, parent):
		self._parent = parent
		super(StockDialog, self).__init__(parent)
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setModal(True)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._cancelled = True
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setMargin(5)

		self.layoutControls = QtGui.QVBoxLayout()

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.setOrientation(QtCore.Qt.Vertical)
	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._saveAndClose)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self._cancel)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addLayout(self.layoutControls)
		self.layoutMain.addWidget(self.buttonBox)

	def _cancel(self):
		self.close()
	def _saveAndClose(self):
		if not self.check():
			return
		self._cancelled = False
		self.close()

	@property
	def cancelled(self):
		return self._cancelled

	def check(self):
		pass

class CreateStockDialog(StockDialog):
	def __init__(self, parent):
		super(CreateStockDialog, self).__init__(parent)

		self.__createVariables()
		self.__createWidgets()
		self.__gridWidgets()

		self.setFixedSize(self.sizeHint())

	def __createVariables(self):
		self._item = None
		self._header = []
		self._data = []
	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.layoutInput = QtGui.QFormLayout()

		self.labelStockField = QtGui.QLabel(self.groupBoxInput)
		self.labelStockField.setText("Поле границ")
		self.labelStockField.setMinimumWidth(150)
		self.labelStockField.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxStockField = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxStockField.setEditable(True)
		self.comboBoxStockField.setInsertPolicy(QtGui.QComboBox.NoInsert)
		self.comboBoxStockField.setMaximumWidth(150)
		self.comboBoxStockField.setFocus()

		self.labelSectorField = QtGui.QLabel(self.groupBoxInput)
		self.labelSectorField.setText("Поле сектора")
		self.labelSectorField.setMinimumWidth(150)
		self.labelSectorField.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxSectorField = QtGui.QComboBox(self.groupBoxInput)
		self.comboBoxSectorField.setEditable(True)
		self.comboBoxSectorField.setInsertPolicy(QtGui.QComboBox.NoInsert)
		self.comboBoxSectorField.setMaximumWidth(150)

		self.labelDensity = QtGui.QLabel(self.groupBoxInput)
		self.labelDensity.setText("Плотность по умолчанию")
		self.labelDensity.setMinimumWidth(150)
		self.labelDensity.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditDensity = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEditDensity.setValidator(DoubleValidator())
		self.lineEditDensity.setMaximumWidth(150)

	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)
		self.groupBoxInput.setLayout(self.layoutInput)

		self.layoutInput.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelStockField)
		self.layoutInput.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxStockField)
		self.layoutInput.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSectorField)
		self.layoutInput.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBoxSectorField)
		self.layoutInput.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelDensity)
		self.layoutInput.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditDensity)

	def check(self):
		stockField = self.comboBoxStockField.currentText()
		if is_blank(stockField):
			qtmsgbox.show_error(self, "Ошибка", "Введите поле '%s'" % self.labelStockField.text())
			return

		labels = [self.labelStockField, self.labelSectorField]
		fields = [self.comboBoxStockField, self.comboBoxSectorField]
		for label, field in zip(labels, fields):
			fieldName = field.currentText()
			if fieldName and not fieldName in self._header:
				qtmsgbox.show_error(self, "Ошибка", "Поле '%s' в файле отсутствует" % fieldName)
				return

		return True

	def setStringItem(self, item):
		self._item = item
		
		filepath = self._item.filePath()
		try:
			f = MicromineFile()
			if not f.open(filepath):
				raise Exception("Невозможно открыть файл '%s'" % filepath)
			self._header = f.header
			self._data = f.read(asstr = True)
			f.close()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", str(e))

			self._data = [['-742.055558', '739.717863', '19.459833', '', 'Патрик', '', '1'], ['-735.226523', '739.569405', '20.675190', '', 'Патрик', '', '1'], ['-730.698577', '742.538551', '16.550129', '', 'Патрик', '', '1'], ['-721.642684', '739.049805', '15.991495', '', 'Патрик', '', '1'], ['-714.665192', '731.849627', '15.763359', '', 'Патрик', '', '1'], ['-712.364106', '728.063967', '15.132491', '', 'Патрик', '', '1'], ['-712.661019', '722.274133', '17.656410', '', 'Патрик', '', '1'], ['-713.625992', '717.523501', '19.038313', '', 'Патрик', '', '1'], ['-712.067189', '711.659439', '15.414685', '', 'Патрик', '', '1'], ['-713.997136', '706.537664', '14.874851', '', 'Патрик', '', '1'], ['-718.896226', '701.935487', '16.456646', '', 'Патрик', '', '1'], ['-727.284060', '700.525144', '20.715467', '', 'Патрик', '', '1'], ['-732.480065', '698.520971', '20.220427', '', 'Патрик', '', '1'], ['-736.488411', '697.927141', '19.813810', '', 'Патрик', '', '1'], ['-743.837045', '702.083946', '21.989944', '', 'Патрик', '', '1'], ['-750.962994', '706.983036', '21.395895', '', 'Патрик', '', '1'], ['-754.377511', '711.585210', '20.774605', '', 'Патрик', '', '1'], ['-752.967167', '715.742014', '23.000940', '', 'Патрик', '', '1'], ['-755.194026', '723.164878', '20.999699', '', 'Патрик', '', '1'], ['-754.897111', '726.282480', '20.452333', '', 'Патрик', '', '1'], ['-751.482594', '732.814600', '20.070042', '', 'Патрик', '', '1'], ['-748.587677', '737.119860', '18.752338', '', 'Патрик', '', '1'], ['-742.055558', '739.717863', '19.459833', '', 'Патрик', '', '1'], ['-513.882235', '708.785098', '13.946351', '', 'Петюня', '', '4'], ['-503.450063', '712.977466', '21.264051', '', 'Петюня', '', '4'], ['-491.360442', '714.147433', '24.784030', '', 'Петюня', '', '4'], ['-481.025767', '713.074969', '24.688630', '', 'Петюня', '', '4'], ['-468.741153', '707.322650', '22.390660', '', 'Петюня', '', '4'], ['-463.183825', '701.765323', '20.327779', '', 'Петюня', '', '4'], ['-459.966430', '688.895724', '20.257161', '', 'Петюня', '', '4'], ['-464.743777', '677.098590', '23.409085', '', 'Петюня', '', '4'], ['-473.128516', '669.201343', '25.489034', '', 'Петюня', '', '4'], ['-480.830771', '665.886447', '26.446948', '', 'Петюня', '', '4'], ['-482.975707', '660.524112', '20.729615', '', 'Петюня', '', '4'], ['-501.012639', '667.056415', '25.856225', '', 'Петюня', '', '4'], ['-512.224788', '676.416125', '24.089547', '', 'Петюня', '', '4'], ['-514.954701', '680.413486', '22.831330', '', 'Петюня', '', '4'], ['-515.929671', '691.430652', '23.036901', '', 'Петюня', '', '4'], ['-519.829549', '692.990603', '16.138083', '', 'Петюня', '', '4'], ['-515.149695', '701.570331', '19.915900', '', 'Петюня', '', '4'], ['-513.882235', '708.785098', '13.946351', '', 'Петюня', '', '4'], ['-633.618226', '788.132902', '31.178792', '', 'Дерек', '3', '5'], ['-639.702035', '769.413489', '39.756675', '', 'Дерек', '3', '5'], ['-625.818471', '746.326215', '30.521716', '', 'Дерек', '3', '5'], ['-615.054809', '754.281964', '26.166127', '', 'Дерек', '3', '5'], ['-612.870876', '758.103845', '25.018270', '', 'Дерек', '3', '5'], ['-613.416862', '764.109656', '26.782571', '', 'Дерек', '3', '5'], ['-618.018714', '776.823256', '28.993995', '', 'Дерек', '3', '5'], ['-622.854564', '784.311022', '28.648835', '', 'Дерек', '3', '5'], ['-628.860376', '789.068872', '28.487245', '', 'Дерек', '3', '5'], ['-633.618226', '788.132902', '31.178792', '', 'Дерек', '3', '5'], ['-639.702035', '769.413489', '39.756675', '', 'Дерек', '2', '6'], ['-667.703157', '769.335492', '29.739141', '', 'Дерек', '2', '6'], ['-664.505256', '778.617201', '29.630347', '', 'Дерек', '2', '6'], ['-656.393511', '785.090998', '31.123239', '', 'Дерек', '2', '6'], ['-650.198722', '793.428330', '26.173881', '', 'Дерек', '2', '6'], ['-644.537883', '792.422767', '28.478389', '', 'Дерек', '2', '6'], ['-633.618226', '788.132902', '31.178792', '', 'Дерек', '2', '6'], ['-639.702035', '769.413489', '39.756675', '', 'Дерек', '2', '6'], ['-639.702035', '769.413489', '39.756675', '', 'Дерек', '1', '7'], ['-667.703157', '769.335492', '29.739141', '', 'Дерек', '1', '7'], ['-666.065208', '762.393710', '31.314195', '', 'Дерек', '1', '7'], ['-666.923181', '755.607922', '29.013150', '', 'Дерек', '1', '7'], ['-662.633315', '751.942036', '30.999688', '', 'Дерек', '1', '7'], ['-658.655440', '744.922257', '29.365331', '', 'Дерек', '1', '7'], ['-650.231705', '742.036347', '30.834275', '', 'Дерек', '1', '7'], ['-642.275954', '739.618424', '29.906131', '', 'Дерек', '1', '7'], ['-638.532072', '733.300622', '21.866780', '', 'Дерек', '1', '7'], ['-627.456420', '737.200501', '22.714833', '', 'Дерек', '1', '7'], ['-626.208459', '741.022382', '26.380750', '', 'Дерек', '1', '7'], ['-625.818471', '746.326215', '30.521716', '', 'Дерек', '1', '7'], ['-639.702035', '769.413489', '39.756675', '', 'Дерек', '1', '7'], ['-521.184817', '834.889045', '21.259484', '', 'Калеб', '2', '8'], ['-512.137101', '847.493449', '28.265275', '', 'Калеб', '2', '8'], ['-510.389955', '852.797282', '28.179343', '', 'Калеб', '2', '8'], ['-508.455617', '857.601932', '26.996532', '', 'Калеб', '2', '8'], ['-510.327557', '863.779337', '24.223008', '', 'Калеб', '2', '8'], ['-510.015568', '866.587249', '22.118554', '', 'Калеб', '2', '8'], ['-507.956432', '870.643122', '17.469160', '', 'Калеб', '2', '8'], ['-497.660756', '863.529745', '19.793342', '', 'Калеб', '2', '8'], ['-494.977639', '861.096221', '19.266084', '', 'Калеб', '2', '8'], ['-489.736205', '851.299729', '17.085678', '', 'Калеб', '2', '8'], ['-494.478455', '842.314412', '20.928067', '', 'Калеб', '2', '8'], ['-496.974378', '835.825015', '19.214372', '', 'Калеб', '2', '8'], ['-506.271686', '829.273219', '17.759996', '', 'Калеб', '2', '8'], ['-514.009043', '833.329093', '22.391194', '', 'Калеб', '2', '8'], ['-521.184817', '834.889045', '21.259484', '', 'Калеб', '2', '8'], ['-521.184817', '834.889045', '21.259484', '', 'Калеб', '1', '9'], ['-512.137101', '847.493449', '28.265275', '', 'Калеб', '1', '9'], ['-510.389955', '852.797282', '28.179343', '', 'Калеб', '1', '9'], ['-508.455617', '857.601932', '26.996532', '', 'Калеб', '1', '9'], ['-510.327557', '863.779337', '24.223008', '', 'Калеб', '1', '9'], ['-510.015568', '866.587249', '22.118554', '', 'Калеб', '1', '9'], ['-507.956432', '870.643122', '17.469160', '', 'Калеб', '1', '9'], ['-516.629760', '870.019141', '17.665443', '', 'Калеб', '1', '9'], ['-526.738243', '868.334394', '11.716764', '', 'Калеб', '1', '9'], ['-527.923805', '863.716939', '16.487987', '', 'Калеб', '1', '9'], ['-532.478863', '855.168407', '16.242667', '', 'Калеб', '1', '9'], ['-528.984572', '846.807071', '21.095199', '', 'Калеб', '1', '9'], ['-528.547786', '838.258538', '17.996123', '', 'Калеб', '1', '9'], ['-521.184817', '834.889045', '21.259484', '', 'Калеб', '1', '9']]
			self._header = ['EAST', 'NORTH', 'RL', 'STRING', 'Склад', 'Сектор', 'JOIN']

			# self._header = ["EAST", "NORTH", "RL", "STRING", "JOIN"]
			# self._data = (
			# 	[-432.068543, 233.782130, 0.000000, "Пак", ""],
			# 	[-591.187271, 451.652387, 0.000000, "Пак", ""],
			# 	[-356.181151, 703.794370, 0.000000, "Пак", ""],
			# 	[82.007344, 725.826193, 0.000000, "Пак", ""],
			# 	[231.334149, 726.826193, 0.000000, "Пак", ""],
			# 	[-62.423501, 265.605875, 0.000000, "Пак", ""],
			# 	[-432.068543, 233.782130, 0.000000, "Пак", ""],
			# 	[-47.735618, -414.932681, 0.000000, "Пок", ""],
			# 	[-385.556916, -321.909425, 0.000000, "Пок", ""],
			# 	[-336.597307, -96.695226, 0.000000, "Пок", ""],
			# 	[317.013464, -126.070991, 0.000000, "Пок", ""],
			# 	[270.501836, -375.764994, 0.000000, "Пок", ""],
			# 	[490.820073, -843.329253, 0.000000, "Пок", ""],
			# 	[-123.623011, -872.705018, 0.000000, "Пок", ""],
			# 	[-263.157895, -588.739290, 0.000000, "Пок", ""],
			# 	[-47.735618, -414.932681, 0.000000, "Пок", ""],
			# 	[1067.510710, -84.340575, 0.000000, "Пук", "1"],
			# 	[708.919829, 6.502448, 0.000000, "Пук", "1"],
			# 	[670.670135, 455.936353, 0.000000, "Пук", "1"],
			# 	[981.448898, 546.779376, 0.000000, "Пук", "1"],
			# 	[1067.510710, -84.340575, 0.000000, "Пук", "1"],
			# 	[1067.510710, -84.340575, 0.000000, "Пук", "2"],
			# 	[981.448898, 546.779376, 0.000000, "Пук", "2"],
			# 	[1100.979192, 757.152693, 0.000000, "Пук", "2"],
			# 	[1363.945838, 541.998164, 0.000000, "Пук", "2"],
			# 	[1277.884027, 365.093329, 0.000000, "Пук", "2"],
			# 	[1507.382191, 126.032742, 0.000000, "Пук", "2"],
			# 	[1292.227662, -79.559364, 0.000000, "Пук", "2"],
			# 	[1067.510710, -84.340575, 0.000000, "Пук", "2"]
			# )

		self.comboBoxStockField.addItems([""] + self._header)
		self.comboBoxSectorField.addItems([""] + self._header)

	def addStocksAndSectors(self, parentNode):
		tree = parentNode.treeWidget()
		stockField = self.comboBoxStockField.currentText()
		sectorField = self.comboBoxSectorField.currentText()

		if not is_blank(stockField) and not stockField in self._header:	return []
		if not is_blank(sectorField) and not sectorField in self._header:	return []

		idxBounds = self._header.index(stockField)
		idxSector = self._header.index(sectorField) if not is_blank(sectorField) else None

		stockNames = {}
		addedItems = []
		for row in self._data:
			stockName = row[idxBounds]
			sectorName = "" if not idxSector else row[idxSector]
			
			stockItemPath = PATH_SPLITTER.join([parentNode.getPath(), stockName + ITEM_STOCK])
			stockItemExists = tree.itemPathExists(stockItemPath)
			if not stockItemExists:
				stockItem = createTreeModelItem(parentNode, stockName, ITEM_STOCK, ITEM_CATEGORY_SVY)
				if not stockName in stockNames:
					stockNames[stockName] = stockItem
					addedItems.append(stockItem)
			else:
				stockItem = tree.getItemByPath(stockItemPath)
			#
			if sectorName:
				sectorItemPath = PATH_SPLITTER.join([stockItem.sectorDirItem().getPath(), sectorName + ITEM_STOCKSECTOR])
				if not tree.itemPathExists(sectorItemPath):
					sectorItem = stockItem.addSector(sectorName)
					addedItems.append(sectorItem)
		return addedItems

class AddTopoDialog(StockDialog):
	def __init__(self, parent):
		super(AddTopoDialog, self).__init__(parent)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._stockItems = []
	def __createWidgets(self):
		self.groupBoxTopoType = QtGui.QGroupBox(self)
		self.groupBoxTopoType.setTitle("Тип поверхности")

		self.layoutTopoType = QtGui.QVBoxLayout()

		self.radioButtonGround = QtGui.QRadioButton(self)
		self.radioButtonGround.setText("Основание")
		self.radioButtonGround.setChecked(True)
		self.radioButtonTopo = QtGui.QRadioButton(self)
		self.radioButtonTopo.setText("Поверхность")
		#
		radioButtonGroup = QtGui.QButtonGroup(self)
		radioButtonGroup.addButton(self.radioButtonGround)
		radioButtonGroup.addButton(self.radioButtonTopo)

		#
		self.groupBoxStockList = QtGui.QGroupBox(self)
		self.groupBoxStockList.setTitle("Склады")
		self.layoutStockList = QtGui.QVBoxLayout()

		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setText("Выберите склады, к которым относится каркас")
		self.listStock = QtGui.QListWidget(self)
		self.listStock.setEditTriggers(QtGui.QListWidget.NoEditTriggers)
		self.listStock.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

	def __createBindings(self):
		self.listStock.itemDoubleClicked.connect(self._saveAndClose)
	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxTopoType)
		self.groupBoxTopoType.setLayout(self.layoutTopoType)
		self.layoutTopoType.addWidget(self.radioButtonGround)
		self.layoutTopoType.addWidget(self.radioButtonTopo)

		self.layoutControls.addWidget(self.groupBoxStockList)
		self.groupBoxStockList.setLayout(self.layoutStockList)
		self.layoutStockList.addWidget(self.labelHint)
		self.layoutStockList.addWidget(self.listStock)

	def setStockList(self, stockItems):
		self._stockItems = []
		self.listStock.clear()
		for stockItem in stockItems:
			# item = QtGui.QListWidgetItem(stockItem.text(0))
			# item.setIcon(stockItem.icon(0))
			self.listStock.addItem(stockItem.toListTreeItem())
			self._stockItems.append(stockItem)

	def check(self):
		selectedStockItems  = self.listStock.selectedItems()
		if not selectedStockItems:
			qtmsgbox.show_error(self, "Ошибка", "Выберите склады")
			return
		return True

	def selectedStockItems(self):
		return [self._stockItems[i] for i in range(len(self._stockItems)) if self.listStock.item(i).isSelected()]

class StockExplorer(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(StockExplorer, self).__init__(*args, **kw)
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
	def __createVariables(self):
		self._groundSelected = True
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.explorer = TreeExplorer(self)
		self.explorer.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.explorer.dropEvent = lambda e: self.dropEvent(e)
		self.explorer.keyPressEvent = lambda e: self.keyPressEvent(e)
		self.explorer.setDragDropMode(QtGui.QAbstractItemView.DropOnly)
		self.explorer.headerItem().setText(0, "Имя")
		self.explorer.headerItem().setText(1, "Плотность")
		self.explorer.setColumnWidth(1, 50)
		self.explorer.header().setStretchLastSection(False)
		self.explorer.header().setResizeMode(0, QtGui.QHeaderView.Stretch)
		self.explorer.header().setResizeMode(1, QtGui.QHeaderView.Fixed)
		self.explorer.setEditTriggers(QtGui.QTreeWidget.NoEditTriggers)
		self.explorer.setItemDelegate(StockExplorerDelegate())
	def __createBindings(self):
		self.explorer.itemDoubleClicked.connect(self.itemDoubleClicked)
		self.explorer.itemChanged.connect(self.itemChanged)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.explorer)

	def keyPressEvent(self, event):
		key = event.key()
		if key == QtCore.Qt.Key_Delete:
			root = self.explorer.invisibleRootItem()

			systemItems = []
			for stockItem in self.stockItems():
				systemItems.extend([stockItem.sectorDirItem(), stockItem.groundDirItem(), stockItem.topoDirItem()])


			selectedItems = self.explorer.selectedItems()
			for item in selectedItems:
				if not item in systemItems:
					parent = item.parent()
					parents = item.iterParents()[:-1]

					(parent or root).removeChild(item)

					for p in parents:
						if p.childCount() < 1 and not p in systemItems:
							(p.parent() or root).removeChild(p)
				else:
					nchildren = item.childCount()
					for i in range(nchildren):
						item.removeChild(item.child(nchildren - i - 1))

		else:
			TreeExplorer.keyPressEvent(self.explorer, event)

	def _dropStringItems(self, stringItems):
		ret = []
		#
		for stringItem in stringItems:
			dialog = CreateStockDialog(self)
			dialog.setWindowTitle("Определить склады %s" % stringItem.text(0))
			dialog.setStringItem(stringItem)
			dialog.exec()

			if dialog.cancelled:
				continue

			item = self.explorer.dropItem(stringItem)

			self.explorer.blockSignals(True)
			addedItems = dialog.addStocksAndSectors(item)
			ret.extend(addedItems)
			self.explorer.blockSignals(False)
			#
			density = dialog.lineEditDensity.text()

			item.stockField = dialog.comboBoxStockField.currentText()
			item.sectorField = dialog.comboBoxSectorField.currentText()

			for item in filter(lambda it: it.isStock(), addedItems):
				item.setDensity(density)
				font = QtGui.QFont()
				font.setBold(True)
				item.setFont(0, font)
				item.setForeground(0, QtGui.QColor(0,0,255))
			#
			for item in filter(lambda it: it.isStockSector(), addedItems):
				item.setDensity(density)
		#
		return ret

	def _dropWireframeItems(self, wireframeItems):
		ret = []
		#
		for wireframeItem in wireframeItems:
			dialog = AddTopoDialog(self)
			dialog.setWindowTitle("Добавить поверхность %s" % wireframeItem.text(0))
			if self._groundSelected:	dialog.radioButtonGround.setChecked(True)
			else:						dialog.radioButtonTopo.setChecked(True)
			dialog.setStockList(self.stockItems())
			dialog.exec()

			if dialog.cancelled:
				continue

			self._groundSelected = dialog.radioButtonGround.isChecked()
			for stockItem in dialog.selectedStockItems():
				if self._groundSelected:	ret.append(stockItem.addGround(wireframeItem))
				else:						ret.append(stockItem.addTopo(wireframeItem))
		#
		return ret

	def dropEvent(self, event):
		sourceModel = event.source()

		stringItems = list(filter(lambda it: it.isString(), sourceModel.selectedItems()))
		wireframeItems = list(filter(lambda it: it.isWireframe(), sourceModel.selectedItems()))

		self._dropStringItems(stringItems)
		self.explorer.expandAll()
		#
		if wireframeItems:
			if self.stockItems():
				self._dropWireframeItems(wireframeItems)
				self.explorer.expandAll()
			else:
				qtmsgbox.show_error(self, "Ошибка", "Выберите границы и создайте склад")
				return

	def itemDoubleClicked(self, item, column):
		if not item.itemType() in [ITEM_STOCK, ITEM_STOCKSECTOR] or column != 1:
			return

		self.explorer.editItem(item, column)
	def itemChanged(self, item, column):
		if column != 1:
			return

		self.explorer.blockSignals(True)

		text = item.text(column)
		if item.isStock():
			for sectorItem in item.sectorItems():
				sectorItem.setText(column, text)
		elif item.isStockSector():
			if all(map(lambda it: text == it.text(1), item.parent().iterChildren())):
				item.stockItem().setText(column, text)
			elif text != item.stockItem().text(column):
				item.stockItem().setText(column, "")
		#
		self.explorer.blockSignals(False)

	def clear(self):
		self.explorer.clear()
		
	def showEvent(self, e):
		self.clear()
		super(StockExplorer, self).showEvent(e)

	def stockItems(self):
		return list(it for it in self.explorer.iterChildren() if it.isStock())
