from gui.qt.widgets.TreeModel import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class TreeExplorer(TreeModelWidget):
	def __init__(self, *args, **kw):
		super(TreeExplorer, self).__init__(*args, **kw)
	def keyPressEvent(self, event):
		key = event.key()
		if key == QtCore.Qt.Key_Delete:
			root = self.invisibleRootItem()

			selectedItems = self.selectedItems()
			for item in selectedItems:
				parent = item.parent()
				parents = item.iterParents()[:-1]

				(parent or root).removeChild(item)

				for p in parents:
					if p.childCount() < 1:
						(p.parent() or root).removeChild(p)

		else:
			TreeModelWidget.keyPressEvent(self, event)