from PyQt4 import QtGui, QtCore
from datetime import datetime

from utils.constants import *
from utils.dbconnection import generate_connection_string

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.TreeModel import *
from gui.qt.widgets.TreeModelItem import createTreeModelItem
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class GridDelegate(QtGui.QStyledItemDelegate):
	def paint(self, painter, option, index):
		# if index.column() > 0:
		painter.save()
		painter.setPen(QtGui.QColor(200,200,200))
		painter.drawRect(option.rect)
		painter.restore()

		super(GridDelegate, self).paint(painter, option, index)

class DateWidget(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DateWidget, self).__init__(*args, **kw)

		self.__createWidgets()
		self.__gridWidgets()

	def __createWidgets(self):
		self.layoutMain = QtGui.QGridLayout()
		self.buttonPrev = QtGui.QPushButton(self)
		self.buttonPrev.setMaximumSize(QtCore.QSize(25, 25))
		self.buttonPrev.setText("<")
		
		self.lineEditYear = QtGui.QLineEdit(self)
		self.lineEditYear.setMaximumSize(QtCore.QSize(100, 25))
		self.lineEditYear.setMaximumSize(QtCore.QSize(100, 25))
		self.lineEditYear.setReadOnly(True)
		self.lineEditYear.setAlignment(QtCore.Qt.AlignCenter)
		self.lineEditYear.setText(str(datetime.now().year))

		self.buttonNext = QtGui.QPushButton(self)
		self.buttonNext.setMaximumSize(QtCore.QSize(25, 25))
		self.buttonNext.setText(">")
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.setSpacing(2)
		self.layoutMain.setMargin(0)
		self.layoutMain.addWidget(self.buttonPrev, 0, 0, QtCore.Qt.AlignRight)
		self.layoutMain.addWidget(self.lineEditYear, 0, 1)
		self.layoutMain.addWidget(self.buttonNext, 0, 2, QtCore.Qt.AlignLeft)

class TransferExplorer(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(TransferExplorer, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self._buildheader()
	def __createVariables(self):
		self._empty_color = QtGui.QColor(255, 255, 255, 0)
		self._select_color = QtGui.QColor(255, 255, 0)
		self._status_color = QtGui.QColor(0, 255, 0)
		self._selected_date = {}
		self._main_app = iterWidgetParents(self)[-1]
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutHeader = QtGui.QHBoxLayout()
		self.labelExistNeedTransferBM = QtGui.QLabel(self)
		self.labelExistNeedTransferBM.setText("Есть модели для перевода")
		self.labelExistNeedTransferBM.setStyleSheet('color: red; font:bold')
		self.labelExistNeedTransferBM.setVisible(False)
		self.dateSelector = DateWidget(self)
		self.dateSelector.setDisabled(True)
		self.explorer = TreeWidget(self)
		self.explorer.setAlternatingRowColors(False)
		self.explorer.header().setStretchLastSection(False)
		self.explorer.setSelectionBehavior(QtGui.QTreeView.SelectItems)
		self.explorer.setItemDelegate(GridDelegate())
	def __createBindings(self):
		self.explorer.itemClicked.connect(self.onClickItem)
		self.dateSelector.buttonPrev.clicked.connect(lambda: self.changeDate(-1))
		self.dateSelector.buttonNext.clicked.connect(lambda: self.changeDate(1))
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.setSpacing(2)
		self.layoutMain.setContentsMargins(0,0,0,0)
		self.layoutMain.addLayout(self.layoutHeader)
		self.layoutHeader.addWidget(self.labelExistNeedTransferBM)
		self.layoutHeader.addWidget(self.dateSelector)
		self.layoutMain.addWidget(self.explorer)

	def _buildheader(self):
		currtime = datetime.now()
		
		columns = ["Имя"] + ["    %s    " % str(i).zfill(2) for i in range(1, 13)]
		rmodes = [QtGui.QHeaderView.Stretch] + [QtGui.QHeaderView.ResizeToContents]*12
		
		for icol, (cname, rmode) in enumerate(zip(columns, rmodes)):
			self.explorer.headerItem().setText(icol, cname)
			self.explorer.headerItem().setTextAlignment(icol, QtCore.Qt.AlignCenter)
			self.explorer.header().setResizeMode(icol, rmode)
			self.explorer.headerItem().setTextAlignment(icol, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
		self.explorer.setColumnWidth(0, 250)

	def _buildtree(self):
		self._buildtable(self._bmdata)

	def _buildtable(self, values):
		if not values:
			return

		self.explorer.clear()

		dirIcon = QtGui.QIcon(ITEM_DIR_ICO_PATH)
		bmIcon = QtGui.QIcon(ITEM_BM_ICO_PATH)

		unitItemDict, bmItemDict = {}, {}

		bmNames, layerNames, unitNames, paths, currdates = zip(*values)
		for bmName, unitName, path, currdate in zip(bmNames, unitNames, paths, currdates):
			# пропускаем несуществующие блочные модели
			if not os.path.exists(path):
				continue
			try:		currdate = datetime.strptime(currdate, "%Y-%m-%d")
			except:		continue
			# добавляем папку юнита, если необходимо
			if not unitName in unitItemDict:
				item = createTreeModelItem(self.explorer, self._prefix + unitName, ITEM_DIR, ITEM_CATEGORY_GEO)
				item.setExpanded(True)
				unitItemDict[unitName] = item
			# добавляем блочную модель
			# Отсекались правые от самой правой точки символы, веротно применялось для отсечения расширения файла, сейчас расширения нет
			#bmName = bmName[:bmName.rfind(".")] if "." in bmName else bmName
			item = createTreeModelItem(unitItemDict[unitName], bmName, ITEM_BM, ITEM_CATEGORY_GEO)
			item.setFilePath(path)
			if item.is_need_transfer():
				item.setTextColor(0,QtCore.Qt.red)
			self._selected_date[item.filePath()] = None

			for i in range(1, 13):
				if currdate.year == int(self.dateSelector.lineEditYear.text()) and i < currdate.month+1:
					item.setBackgroundColor(i, self._status_color)
				else:
					break
			bmItemDict[(unitName, bmName)] = item

	def onClickItem(self, item, column):
		if not item.isBlockModel() or column == 0:
			return

		year = int(self.dateSelector.lineEditYear.text())
		self.setSelectedDate(item, datetime(year, column, 1))

	def showEvent(self, event):
		if not self._main_app.srvcnxn:
			# подключаемся к БД
			self._main_app.connect_to_database()
			self.dateSelector.setDisabled(False)
		else:
			self.dateSelector.setDisabled(False)

		if not self._main_app.srvcnxn:
			qtmsgbox.show_error(self._main_app, "Ошибка", "Невозможно подключиться к базе данных")
			return
		#
		self.update()
		super(TransferExplorer, self).showEvent(event)
	
	def update(self):
		self._layer = self._main_app.mainWidget.modelExplorer.standard_models[0].comboBoxLayer.currentText()
		self._prefix = self._main_app.settings.getValue(ID_GEO_UPREFIX)

		cnxn = self._main_app.srvcnxn
		if not cnxn:
			return

		columns = ["[NAME]", "[LAYER]", "[GEOUNIT]", "[PATH]", "[CURRENT_DATE]"]
		self._bmdata = cnxn.exec_query(
			"SELECT %s FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [LAYER] = N'%s' and [COMMIT_STATUS] is NULL" % (
			", ".join(columns), cnxn.dbsvy, self._layer)).get_rows()
		#
		self._buildtree()
		self.checkExistNeedTransferBM()

	def checkExistNeedTransferBM(self):
		
		cnxn = self._main_app.srvcnxn
		if not cnxn:
			return
		cmd = """
			SELECT count(*) FROM  [{0}].[dbo].[NM_BLOCKMODEL_METADATA] WHERE IS_NEED_TRANSFER = 'true'
		"""
		cmd = cmd.format(cnxn.dbsvy)
		data = cnxn.exec_query(cmd).get_rows()
		self.labelExistNeedTransferBM.setVisible(False)
		if data:
			if data[0][0] > 0:
				self.labelExistNeedTransferBM.setVisible(True)

	def changeDate(self, direction):
		year = int(self.dateSelector.lineEditYear.text())
		self.dateSelector.lineEditYear.setText(str(year+direction))
		self.update()

	def selectedDate(self, item):
		return self._selected_date[item.filePath()]
	def setSelectedDate(self, item, date):
		for row in self._bmdata:
			name, layer, unit, path, bmdate = row
			unit = self._prefix + unit
			bmdate = datetime.strptime(bmdate, "%Y-%m-%d")
			if name == item.text(0) and layer == self._layer and unit == item.parent().text(0):
				break

		key = item.filePath()
		# снимаем выделение, если пользователь выбрал уже выбранную ячейку
		if self._selected_date[key] and self._selected_date[key].year == date.year and self._selected_date[key].month == date.month:
			item.setBackgroundColor(date.month, self._empty_color)
			self._selected_date[key] = None
			return

		# снимаем предыдущее выделение
		if self._selected_date[key] and self._selected_date[key].year == date.year:
			item.setBackgroundColor(self._selected_date[key].month, self._empty_color)
	
		if date.year < bmdate.year:
			color = self._empty_color
			self._selected_date[key] = None
		if date.year > bmdate.year:
			color = self._select_color
			self._selected_date[key] = datetime(date.year, date.month, 1)
		if date.year == bmdate.year and date.month > bmdate.month:
			color = self._select_color
			self._selected_date[key] = datetime(date.year, date.month, 1)
		if bmdate.year == date.year and date.month <= bmdate.month:
			color = self._status_color
			self._selected_date[key] = None
		item.setBackgroundColor(date.month, color)