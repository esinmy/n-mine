from PyQt4 import QtGui, QtCore

from utils.constants import *
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.TreeModel import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class UserTabs:
	def __init__(self, params):
		self._params = params
	@property
	def names(self):
		return [name for name, folder in self._params[ID_USRFOLDERS]]
	@property
	def folders(self):
		return [folder for name, folder in self._params[ID_USRFOLDERS]]
	def getFormsetByPath(self, path, mmtype):
		lowerdirs = list(map(lambda x: x.lower(), self.folders))
		path = path.lower()
		if not path in lowerdirs:
			raise Exception("Путь '%s' не найден в списке Избранное")
		if not mmtype.upper() in map(lambda x: x.upper(), MMTYPE_LIST):
			raise ValueError("%s - неверный тип данных Micromine" % mmtype)

		index = lowerdirs.index(path)
		formsetid = [setid for mmtype_, setid in self._params[ID_USRFORMSETS][index] if mmtype_ == mmtype][0]
		return formsetid
	def getCategoryNameByPath(self, path):
		path = path.lower()
		lowerdirs = list(map(lambda x: x.lower(), self.folders))
		if not path in lowerdirs:
			raise Exception("Путь '%s' не найден в списке Избранное")
		return self.names[lowerdirs.index(path)]

class ModelExplorerFilter(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(ModelExplorerFilter, self).__init__(*args, **kw)

		self._createWidgets()
		self._gridWidgets()

	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.layoutOptions = QtGui.QHBoxLayout()
		self.checkBoxExpandAll = QtGui.QCheckBox(self)
		self.checkBoxExpandAll.setText("Развернуть все")
		self.checkBoxExpandAll.setChecked(False)

		self.checkBoxHideEmpty = QtGui.QCheckBox(self)
		self.checkBoxHideEmpty.setText("Спрятать пустые директории")
		self.checkBoxHideEmpty.setChecked(False)
		self.checkBoxHideEmpty.setVisible(False)

		self.line = QtGui.QFrame(self)
		self.line.setGeometry(QtCore.QRect(85, 110, 118, 3))
		self.line.setFrameShape(QtGui.QFrame.HLine)
		self.line.setFrameShadow(QtGui.QFrame.Sunken)

		self.layoutButtons = QtGui.QHBoxLayout()
		self.showModelButton = QtGui.QToolButton()
		self.showModelButton.setAutoRaise(True)
		self.showModelButton.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed))
		self.showDataButton = QtGui.QToolButton()
		self.showDataButton.setAutoRaise(True)
		self.showDataButton.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed))
		self.showStringButton = QtGui.QToolButton()
		self.showStringButton.setAutoRaise(True)
		self.showStringButton.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed))
		self.showAnnotationButton = QtGui.QToolButton()
		self.showAnnotationButton.setAutoRaise(True)
		self.showAnnotationButton.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed))
		self.showTridbButton = QtGui.QToolButton()
		self.showTridbButton.setAutoRaise(True)
		self.showTridbButton.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed))

		self.lineEditSearch = QtGui.QLineEdit(self)
		self.lineEditSearch.setPlaceholderText("Поиск")
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addLayout(self.layoutOptions)
		self.layoutOptions.addWidget(self.checkBoxExpandAll)
		self.layoutOptions.addWidget(self.checkBoxHideEmpty)
		
		self.layoutMain.addWidget(self.line)

		self.layoutMain.addLayout(self.layoutButtons)
		self.layoutButtons.addWidget(self.showModelButton)
		self.layoutButtons.addWidget(self.showDataButton)
		self.layoutButtons.addWidget(self.showStringButton)
		self.layoutButtons.addWidget(self.showAnnotationButton)
		self.layoutButtons.addWidget(self.showTridbButton)

		self.layoutMain.addWidget(self.lineEditSearch)

	def filterButtons(self):
		return (self.showModelButton, self.showDataButton, self.showStringButton, self.showAnnotationButton, self.showTridbButton)

class ModelExplorer(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(ModelExplorer, self).__init__(*args, **kw)

		self._createVariables()
		self._createWidgets()
		self._createBindings()
		self._gridWidgets()

		self.setMinimumSize(QtCore.QSize(420, 600))

	def _createVariables(self):
		self._sectionIsShown = False		# отображается ли вкладка с разрезами
		self._standard_models = []
		self._user_models = []
		self._usertabs = None
	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(5)
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.explorer = QtGui.QTabWidget(self)
		self.explorer.setMovable(True)
		self.geoTreeModel = GeoTreeModel(self.explorer)
		self.svyTreeModel = SvyTreeModel(self.explorer)
		self.dsnTreeModel = DsnTreeModel(self.explorer)
		self.secTreeModel = SecTreeModel(self.explorer)

		self._createStandardModels()

		self.explorerFilter = ModelExplorerFilter(self)
		self._createFilter()

	def _createBindings(self):
		self.explorerFilter.checkBoxExpandAll.stateChanged.connect(self.setExpandMode)
		self.explorerFilter.checkBoxHideEmpty.stateChanged.connect(self.filterItems)
		self.explorerFilter.lineEditSearch.textChanged.connect(self.filterItems)

	def _gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.explorer)
		self.layoutMain.addWidget(self.explorerFilter)
	
	def _createStandardModels(self):
		# вставка стандартных вкладок
		self._standard_models = [self.geoTreeModel, self.svyTreeModel, self.dsnTreeModel, self.secTreeModel]
		main_app = self.parent().parent().parent()
		if main_app.settings.values():
			for model in self._standard_models:
				model.readSettings()

		names = [ITEM_CATEGORY_GEO_NAME, ITEM_CATEGORY_SVY_NAME, ITEM_CATEGORY_DSN_NAME, ITEM_CATEGORY_SECTIONS_NAME]
		for i, (model, name) in enumerate(zip(self._standard_models, names)):
			self.explorer.addTab(model, name)
		# скрываем вкладку с разрезами
		self._sectionIsShown = True
		self.hideSectionsTab()
	def _createUserModels(self):
		# вставка пользовательских вкладок
		main_app = self.parent().parent().parent()
		self._user_models = []
		# пока что считывание каких-либо форм происходит через отдельно подключение
		# потом необходимо переделать обращение к разным формам
		usrparams = FormSet(ID_FAVORITE)
		usrparams.open(0)
		self._usertabs = UserTabs(usrparams.values())
		usrparams.close()
		for name, folderpath in zip(self._usertabs.names, self._usertabs.folders):
			if not os.path.exists(folderpath):
				continue
			model = DirTreeModel(self.explorer)
			self._user_models.append(model)
			self.explorer.addTab(model, name)
	def _deleteUserModels(self):
		names = [ITEM_CATEGORY_GEO_NAME, ITEM_CATEGORY_SVY_NAME, ITEM_CATEGORY_DSN_NAME, ITEM_CATEGORY_SECTIONS_NAME]
		for index in range(self.explorer.count()-1, -1, -1):
			if not self.explorer.tabText(index) in names:
				self.explorer.removeTab(index)


	def updateUserModelsList(self):
		currentIndex = self.explorer.currentIndex()
		self._deleteUserModels()
		self._createUserModels()
		if currentIndex < self.explorer.count():
			self.explorer.setCurrentIndex(currentIndex)
		else:
			self.explorer.setCurrentIndex(0)

	def _createFilter(self):
		buttons = (self.explorerFilter.showModelButton, self.explorerFilter.showDataButton, self.explorerFilter.showStringButton,
				self.explorerFilter.showAnnotationButton, self.explorerFilter.showTridbButton)
		icons = [ITEM_BM_ICO_PATH, ITEM_DATA_ICO_PATH, ITEM_STRING_ICO_PATH, ITEM_ANNOTATION_ICO_PATH, ITEM_TRIDB_ICO_PATH]
		hints = ["Блочные модели", "Данные", "Стринги", "Аннотации", "Каркасы"]

		for but, icon, hint in zip(buttons, icons, hints):
			but.setCheckable(True)
			but.setChecked(True)
			but.setIcon(QtGui.QIcon(icon))
			but.setIconSize(QtCore.QSize(20, 20))
			# but.setMaximumSize(QtCore.QSize(20, 30))
			# but.setText(hint)
			but.setToolTip("Показать/скрыть %s" % hint.lower())
			but.clicked.connect(self.filterItems)
			but.enabledChange = lambda e: self.filterItems()

	@property
	def standard_models(self):
		return tuple(self._standard_models)
	@property
	def user_models(self):
		return tuple(self._user_models)
	@property
	def usertabs(self):
		return self._usertabs

	def applyModelView(self):
		self.setExpandMode()
		self.filterItems()

	def expandAll(self):
		for model in self._standard_models + self._user_models:
			for child in filter(lambda it: it.isDirectory() or it.isModel(), model.treeModel.iterChildren()):
				model.populateRecursively(child)
			for child in filter(lambda it: it.isDirectory() or it.isModel(), model.treeModel.iterChildren()):
				child.setExpanded(True)
		self.filterItems()
	def collapseAll(self, parent = None):
		for model in self._standard_models + self._user_models:
			for item in model.treeModel.iterChildren():
				item.setExpanded(False)
	def setExpandMode(self):
		flag = self.explorerFilter.checkBoxExpandAll.isChecked()
		if flag:	self.expandAll()
		else:		self.collapseAll()

	def filterItems(self):
		for model in self._standard_models + self._user_models:
			model.treeModel.showAll()

		hideEmpty = self.explorerFilter.checkBoxHideEmpty.isChecked()
		for model in self._standard_models + self._user_models:
			for item in model.treeModel.iterChildren():
				if item.isDirectory() and item.childCount() == 0:
					item.setHidden(hideEmpty)

		hideModels = not self.explorerFilter.showModelButton.isChecked() or not self.explorerFilter.showModelButton.isEnabled()
		hideData = not self.explorerFilter.showDataButton.isChecked() or not self.explorerFilter.showDataButton.isEnabled()
		hideString = not self.explorerFilter.showStringButton.isChecked() or not self.explorerFilter.showStringButton.isEnabled()
		hideTridb = not self.explorerFilter.showTridbButton.isChecked() or not self.explorerFilter.showTridbButton.isEnabled()
		hideAnnotation = not self.explorerFilter.showAnnotationButton.isChecked() or not self.explorerFilter.showAnnotationButton.isEnabled()

		statusDict = {ITEM_BM: hideModels, ITEM_STRING: hideString, ITEM_DATA: hideData, ITEM_TRIDB: hideTridb, ITEM_ANNOTATION: hideAnnotation}
		for model in self._standard_models + self._user_models:
			for item in model.treeModel.iterChildren():
				itemType = item.itemType()
				if itemType in statusDict:
					item.setHidden(statusDict[itemType])

		text = self.explorerFilter.lineEditSearch.text()
		if not text:
			return
		# filter search box
		for model in self._standard_models + self._user_models:
			items = model.treeModel.findItems(text, QtCore.Qt.MatchContains | QtCore.Qt.MatchRecursive, 0)
			# считываем всех родителей найденных элементов
			filteredItems = []
			for item in items:
				parents = [item] + list(reversed(item.iterParents()))[1:]
				filteredItems.extend(parents)
			# прячем все элементы, которые не удовлетворяют фильтру
			for item in model.treeModel.iterChildren():
				if not item.isHidden() and not item in filteredItems:
					item.setHidden(True)

	def showSectionsTab(self):
		self.explorer.insertTab(3, self.secTreeModel, ITEM_CATEGORY_SECTIONS_NAME)
		self._sectionIsShown = True
	def hideSectionsTab(self):
		if self._sectionIsShown:
			self.explorer.removeTab(3)
			self._sectionIsShown = False
