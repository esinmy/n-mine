from PyQt4 import QtGui, QtCore
import sqlite3

from utils.constants import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
from gui.qt.widgets.TableFilter import TableFilterWireframe
from gui.qt.widgets.TreeExplorers.TreeExplorer import TreeExplorer
from gui.qt.widgets.TreeModel import CheckBoxItem
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DisplayParametersFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DisplayParametersFrame, self).__init__(*args, **kw)

		self._createWidgets()
		self._gridWidgets()

	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(5)
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры загрузки")
		self.layoutParams = QtGui.QHBoxLayout()

		self.checkBoxGroupCategories = QtGui.QCheckBox(self)
		self.checkBoxGroupCategories.setText("Создать один набор для категории")

		self.checkBoxSelectAll = QtGui.QCheckBox(self)
		self.checkBoxSelectAll.setText("Загрузить для редактирования") # "Выбрать все"
		# Хотелка (поиск: Василий Радзион) грузить для редактирования по умолчанию
		self.checkBoxSelectAll.setChecked(True)
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.groupBoxParams)

		self.groupBoxParams.setLayout(self.layoutParams)
		self.layoutParams.addWidget(self.checkBoxSelectAll)
		self.layoutParams.addWidget(self.checkBoxGroupCategories)


class SelectionExplorer(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(SelectionExplorer, self).__init__(*args, **kw)

		self._createVariables()
		self._createWidgets()
		self._createBindings()
		self._gridWidgets()

	def _createVariables(self):
		self._filterEnabled = True
	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(0)
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.splitter = QtGui.QSplitter(QtCore.Qt.Vertical, parent = self)

		self.explorer = TreeExplorer(self)
		self.explorer.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.explorer.dropEvent = lambda e: self.dropEvent(e)
		self.explorer.keyPressEvent = lambda e: self.keyPressEvent(e)
		self.explorer.itemChanged.connect(self.itemChanged)
		self.explorer.setDragDropMode(QtGui.QAbstractItemView.DropOnly)
		self.explorer.header().setStretchLastSection(False)
		self.explorer.headerItem().setText(0, "Имя")
		self.explorer.headerItem().setText(1, "Редактировать")
		self.explorer.headerItem().setText(2, "Форма")	
		self.explorer.setColumnWidth(1, 100)
		self.explorer.setColumnWidth(2, 100)
		self.explorer.header().setResizeMode(0, QtGui.QHeaderView.Stretch)
		self.explorer.header().setResizeMode(1, QtGui.QHeaderView.Fixed)
		self.explorer.header().setResizeMode(2, QtGui.QHeaderView.Fixed)

		self.tableWireframeFilter = TableFilterWireframe(self)
		self.tableWireframeFilter.gridEditorFilter.toolBar.labelHint.setText("Выберите условия для загружаемых каркасов")
		self.tableWireframeFilter.setVisible(False)

		self.displayParams = DisplayParametersFrame(self)
	def _createBindings(self):
		self.displayParams.checkBoxSelectAll.stateChanged.connect(self.selectAll)
		self.explorer.itemDoubleClicked.connect(self._itemDoubleClicked)
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.explorer)
		self.layoutMain.addWidget(self.splitter)
		self.layoutMain.addWidget(self.tableWireframeFilter)
		self.layoutMain.addWidget(self.displayParams)

		self.splitter.addWidget(self.explorer)
		self.splitter.addWidget(self.tableWireframeFilter)

		self.splitter.setStretchFactor(0, 70)
		self.splitter.setStretchFactor(1, 30)

	def _itemDoubleClicked(self, item, column):
		if column != 2 or not item.canHaveFormsetId():
			return

		expanded = item.isExpanded()
		#
		tree = TreeMmFormsetBrowserDialog(self)
		if tree.readFormsets(MMTYPE_COMMAND_ID_DICT[MMTYPE_DICT[item.itemType()]]):
			tree.exec()
			if tree.result:
				setId = tree.result
				item.setText(2, str(setId))
				item.setFormsetId(setId)
		#
		item.setExpanded(not expanded)

	def keyPressEvent(self, event):
		selectedItems = self.explorer.selectedItems()

		#
		key = event.key()
		TreeExplorer.keyPressEvent(self.explorer, event)
		#
		if self.needsToRestoreFilter(selectedItems):
			self.updateFilterIfRequired()
			self.showWireframeFilterIfRequired()

	def clear(self):
		self.explorer.clear()
		self.hideWirerameFilter()

	def selectAll(self):
		items = list(filter(lambda x: x.canModify() and x.allowModify(), self.explorer.iterChildren()))
		flag = not all(item.modifyMode() for item in items) and self.displayParams.checkBoxSelectAll.isChecked()
		for item in items:
			item.checkBoxModify.setChecked(flag)

	def isWireframeFilterRequired(self):
		main_app = self.parent().parent().parent().parent()
		isadmin = main_app.userRole() == ID_USERADMIN
		tridbItems = list(it for it in self.explorer.iterChildren() if it.isTridb())
		isowner = all(map(lambda it: it.itemCategory() == ITEM_ROLE_CATEGORY_DICT[main_app.userRole()], tridbItems))
		return (isadmin or isowner) and any(it.isTridb() for it in self.explorer.iterChildren())

	def hideWirerameFilter(self):
		self.tableWireframeFilter.clear()
		self.tableWireframeFilter.setVisible(False)

	def showWireframeFilter(self):
		self.tableWireframeFilter.setVisible(True)

	def showWireframeFilterIfRequired(self):
		if self._filterEnabled and self.isWireframeFilterRequired():
			self.showWireframeFilter()
		else:
			self.hideWirerameFilter()

	def filterEnabled(self):			return self._filterEnabled
	def setFilterEnabled(self, val):	self._filterEnabled = bool(val)

	def needsToRestoreFilter(self, items):
		msg = "Изменение списка выбранных каркасов при настроенном фильтре приведет к его сбросу. Продолжить?"
		filterRowCount = self.tableWireframeFilter.gridEditorFilter.dataGrid.rowCount()
		#
		allItems = []
		for it in items:
			allItems.append(it)
			allItems.extend(it.iterChildren())
		#
		if self._filterEnabled and any(it.isTridb() or it.isWireframe() for it in allItems) and filterRowCount > 0:
			if not qtmsgbox.show_question(self, "Внимание", msg):
				return False
			else:
				self.tableWireframeFilter.clear()
				return True
		return True

	def itemChanged(self, item, column):
		if column == 1:
			text = item.text(column)
			flag = True if text == "1" else False
			item.setModifyMode(flag)

	# drop
	def dropEvent(self, event):
		main_app = self.parent().parent().parent().parent()
		try:
			sourceModel = event.source()
			#
			selectedItems = sourceModel.selectedItems()
			for selectedItem in selectedItems:
				sourceModel.parent().populateRecursively(selectedItem)
			main_app.mainWidget.modelExplorer.filterItems()
			# обновляем список выбранных элементов на случай, если при обновлении дерева
			# выделение некоторых элементов было сброшено
			selectedItems = sourceModel.selectedItems()
			#
			continue_ = self.needsToRestoreFilter(selectedItems)
			if not continue_:
				return

			# проверяем, надо ли добавлять разрезы
			if main_app.currentMode not in [MODE_SVY_UPDATE_GRAPHIC]:
				selectedItems = list(filter(lambda it: not (it.isSectionPack() or it.isSection()), selectedItems))

			#
			allItems = []
			for item in selectedItems:
				allItems.append(item)
				if item.isSectionPack():
					item.readSectionNames()
				allItems.extend(item.iterChildren())
			# исключаем скрытые объекты

			allItems = list(filter(lambda it: not it.isHidden() and not (it.isDirectory() and it.childCount() == 0), allItems))
			#
			addedItems = [self.explorer.dropItem(item) for item in allItems]
			
			for selectedItem, addedItem in zip(allItems, addedItems):
				if not addedItem or addedItem.itemType() in [ITEM_SECTION, ITEM_SECTIONPACK]:
					continue
				#
				if selectedItem.canModify():
					# setting modify mode
					addedItem.setModifyMode(selectedItem.modifyMode())
					addedItem.setAllowModify(selectedItem.allowModify())
					# setting widget
					addedItem.checkBoxModify = CheckBoxItem(self, addedItem, 1)
					addedItem.checkBoxModify.setChecked(selectedItem.modifyMode())
					addedItem.checkBoxModify.setEnabled(selectedItem.allowModify())
				
				setId = selectedItem.formsetId()
				addedItem.setText(2, str(setId) if setId else "")
				addedItem.setTextAlignment(2, QtCore.Qt.AlignCenter)

			self.explorer.expandAll()

			# обновление фильтра
			self.updateFilterIfRequired()
			self.showWireframeFilterIfRequired()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Необработанная ошибка", str(e))
			raise


	def updateFilterIfRequired(self):
		if self._filterEnabled and self.isWireframeFilterRequired():
			self.updateFilter()

	def updateFilter(self):
		progress = self.parent().parent().parent().progressBar
		progress.setRange(0,0)
		# считывание категорий
		categories = [it.itemCategoryName() for it in self.explorer.children()]
		# создания словаря атрибутов
		# stdAttrs = ("Name", "Code", "Title", "Colour", "CreateDateTime", "CreateBy", "CreateCompany",
		# 			"EditDateTime", "EditBy", "EditCompany", "MetaNotes", "Accuracy", "XMinimum", "XMaximum",
		# 			"YMinimum", "YMaximum", "ZMinimum", "ZMaximum", "DTMPlane", "Volume", "SurfaceArea",
		# 			"Validated", "IsLocked", "LockBy", "LockReason", "LockDateTime")

		stdAttrs = ("Name", "Code", "Title", "Volume", "CreateBy")
		
		attrNamesDict = {}
		attrValuesDict = {}
		sqlCmd = ""
		for i, category in enumerate(categories):
			tridbItems = list(filter(lambda it: it.isTridb(), self.explorer.children()[i].iterChildren()))
			attrNamesDict[category] = []
			n = len(tridbItems)
			for k, tridbItem in enumerate(tridbItems):
				progress.setText("Считывание каркасов (%s, %d/%d)" % (category, k+1, n))
				selectedWireframeNames = None
				if tridbItem.childCount() > 0:
					selectedWireframeNames = [it.name() for it in tridbItem.iterChildren()]
				cnxn = None
				try:
					cnxn = sqlite3.connect(tridbItem.filePath())
					cursor = cnxn.cursor()

					selectedWireframesIds = None
					if selectedWireframeNames:
						cmd = "SELECT Id FROM GeneralInformation WHERE Name in ('%s')" % "','".join(selectedWireframeNames)
						selectedWireframesIds = [row[0] for row in cnxn.execute(cmd)]

					attrs = [row[0] for row in cnxn.execute("SELECT Name FROM UserAttributeDefinition")]
					attrNamesDict[category].extend(attrs)

					#
					cmd = "SELECT D.Name, ValueText FROM UserAttributeDefinition D JOIN UserAttributeValue V on D.Id = V.AttributeId"
					if selectedWireframesIds:
						cmd += " WHERE V.TriangulationId in (%s)" % ",".join(map(str, selectedWireframesIds))
					attrValues = [row for row in cnxn.execute(cmd)]
					for attrName, attrValue in attrValues:
						if not attrName in attrValuesDict:
							attrValuesDict[attrName] = []
						attrValuesDict[attrName].append(attrValue)

					cmd = "SELECT %s FROM GeneralInformation" % ",".join(stdAttrs)
					if selectedWireframeNames:
						cmd += " WHERE Id in (%s)" % ",".join(map(str, selectedWireframesIds))
					cursor.execute(cmd)
					columnNames = [row[0] for row in cursor.description]
					for (i, values) in enumerate(zip(*cursor.fetchall())):
						#
						attrName = columnNames[i]
						if not attrName in attrValuesDict:
							attrValuesDict[attrName] = []
						attrValuesDict[attrName].extend(list(map(str, values)))
					cnxn.close()
				except Exception as e:
					qtmsgbox.show_error(self, "Ошибка", "Невозможно считать атрибуты каркасов. %s" % str(e))
				finally:
					if cnxn is not None:
						cnxn.close()
				QtGui.QApplication.processEvents()
			# сортировка и извелчение уникальных значений для атрибутов и значений
			attrNamesDict[category] = list(sorted(set(attrNamesDict[category]))) + list(stdAttrs)

		for attrName in attrValuesDict:
			attrValuesDict[attrName] = list(sorted(set(attrValuesDict[attrName])))
			if "" in attrValuesDict[attrName]:
				attrValuesDict[attrName].remove("")
		# задаем списки значений
		self.tableWireframeFilter.setDictionaries(attrNamesDict, attrValuesDict)

		progress.setText("")
		progress.setRange(0,100)