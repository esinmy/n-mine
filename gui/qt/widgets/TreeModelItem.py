from PyQt4 import QtGui, QtCore
import os, sqlite3

from mm.mmutils import MicromineFile, Tridb
from mm.formsets import copy_wireframe
from utils.constants import *
from utils.qtutil import iterWidgetParents
from utils.blockmodel import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.commands.geo.GeoRenameModel import GeoRenameModel
from gui.qt.commands.geo.GeoDeleteModel import GeoDeleteModel
from gui.qt.widgets.ListTreeItem import createListTreeItem
from utils.logger import  NLogger
import shutil


LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

PATH_SPLITTER = "***"

def createTreeModelItem(parent, text, item_type, item_category):
	itDict = {}
	itDict[ITEM_DIR] = TreeModelDirItem
	itDict[ITEM_TRIDB] = TreeModelTridbItem
	itDict[ITEM_DATA] = TreeModelDataItem
	itDict[ITEM_STRING] = TreeModelStringItem
	itDict[ITEM_ANNOTATION] = TreeModelAnnotationItem
	itDict[ITEM_MODEL] = TreeModelModelItem
	itDict[ITEM_BM] = TreeModelBlockModelItem
	itDict[ITEM_WIREFRAME] = TreeModelWireframeItem
	itDict[ITEM_SECTIONPACK] = TreeModelSectionPackItem
	itDict[ITEM_SECTION] = TreeModelSectionItem
	itDict[ITEM_STOCK] = TreeModelStockItem
	itDict[ITEM_STOCKSECTOR] = TreeModelStockSectorItem

	item = itDict[item_type](parent, item_category)
	item.setText(0, text)

	return item

class TreeModelItem(QtGui.QTreeWidgetItem):
	def __init__(self, parent, item_type, item_category):
		self._filePath = None
		self._root = None
		self._itemType = item_type
		self._itemCategory = item_category

		self._allowModify = False		# разрешить загружать для редактирования
		self._modifyMode = False		# загружать для редактирования по умолчанию
		self._formsetId = None

		if not item_type in ITEM_TYPE_LIST:
			raise Exception("Неверный тип объекта дерева. Разрешенные значения %s" % str(ITEM_TYPE_LIST))
		if not item_category in ITEM_CATEGORY_LIST:
			raise Exception("Неверная категория объекта дерева. Разрешенные значения %s" % str(ITEM_CATEGORY_LIST))

		super(TreeModelItem, self).__init__(parent)

		self.setIcon(0, QtGui.QIcon(ITEM_ICO_DICT[self.itemType()]))

	def isDirectory(self):		return self._itemType == ITEM_DIR
	def isDat(self):			return self._itemType == ITEM_DATA
	def isString(self):			return self._itemType == ITEM_STRING
	def isModel(self):			return self._itemType == ITEM_MODEL
	def isBlockModel(self):		return self._itemType == ITEM_BM
	def isTridb(self):			return self._itemType == ITEM_TRIDB
	def isWireframe(self):		return self._itemType == ITEM_WIREFRAME
	def isAnnotation(self):		return self._itemType == ITEM_ANNOTATION
	def isSectionPack(self):	return self._itemType == ITEM_SECTIONPACK
	def isSection(self):		return self._itemType == ITEM_SECTION
	def isStock(self):			return self._itemType == ITEM_STOCK
	def isStockSector(self):	return self._itemType == ITEM_STOCKSECTOR
	# DsnUnitEdition.Galkin.Insert.Begin
	def isDsnUnit(self):
		if not (self.isDirectory() and self._itemCategory == ITEM_CATEGORY_DSN):
			return False
		layer_name = self._root.split('\\')[-1]
		split_self_path = self._filePath.split('\\')
		index_layer_name = split_self_path.index(layer_name) 
		if len (split_self_path) > index_layer_name + 1:
			unit_name = split_self_path[index_layer_name + 1]
		else: 
			return False
		if unit_name != self.name():
			return False

		return True

	def isSvyUnit(self):
		if not (self.isDirectory() and self._itemCategory == ITEM_CATEGORY_SVY):
			return False
		layer_name = self._root.split('\\')[-1]
		split_self_path = self._filePath.split('\\')
		index_layer_name = split_self_path.index(layer_name) 
		if len (split_self_path) > index_layer_name + 1:
			unit_name = split_self_path[index_layer_name + 1]
		else: 
			return False
		if unit_name != self.name():
			return False

		return True

	def isTriDbDsnUnit(self):
		item_has_parent_is_dsn_unit = any(item.isDsnUnit() for item in self.iterParents()[:-1])		    # warn
		if not (self.isTridb() and self._itemCategory == ITEM_CATEGORY_DSN and item_has_parent_is_dsn_unit):
			return False

		return True

	def isTriDbSvyUnit(self):
		item_has_parent_is_svy_unit = any(item.isSvyUnit() for item in self.iterParents()[:-1])		    
		if not (self.isTridb() and self._itemCategory == ITEM_CATEGORY_SVY and item_has_parent_is_svy_unit):
			return False

		return True

	 # DsnUnitEdition.Galkin.Insert.End
	def itemType(self):			return self._itemType
	def itemCategory(self):		return self._itemCategory
	def itemCategoryName(self):
		if self.itemCategory() != ITEM_CATEGORY_USER:
			return ITEM_CATEGORY_DICT[self.itemCategory()]
		else:
			return iterWidgetParents(self.treeWidget())[-1].mainWidget.modelExplorer.usertabs.getCategoryNameByPath(self.root())


	def canHaveFormsetId(self):
		return self.itemType() in MMTYPE_DICT

	def _getFormsetId(self):
		ittype = self.itemType()
		itpath = self.filePath()
		itname = self.text(0)
		itparent = self.parent()
		itcat = self.itemCategory()

		if not all([ittype, itpath, itname, itcat]) or not self.canHaveFormsetId():
			return None

		#
		main_app = iterWidgetParents(self.treeWidget())[-1]
		if itcat == ITEM_CATEGORY_USER:
			setId = main_app.mainWidget.modelExplorer.usertabs.getFormsetByPath(self.root(), MMTYPE_DICT[ittype])
			return int(setId) if setId else None

		formsets = list(zip(*main_app.settings.getValue(ITEM_CATEGORY_FORMSET_DICT[itcat])))[1]
		formsetsDict = dict(zip(MMTYPE_LIST, formsets))

		mmtype = MMTYPE_DICT[ittype]
		setId = formsetsDict[mmtype]

		return int(setId) if setId else None
	def formsetId(self):
		if self._formsetId is None:
			self._formsetId = self._getFormsetId()
		return self._formsetId
	def setFormsetId(self, setId):
		self._formsetId = int(setId) if setId else None

	def canModify(self):		return hasattr(self, "allowModify")

	def name(self):					return self.text(0)
	def setName(self, value):		self.setText(0, str(value))
	def filePath(self):			return self._filePath
	def setFilePath(self, path):
		# несуществующий путь может быть только у определенной, но не созданной модели
		# и у каркаса, так как создается мнимый путь
		if not self.itemType() in [ITEM_MODEL, ITEM_WIREFRAME, ITEM_SECTION, ITEM_STOCK, ITEM_STOCKSECTOR] and not os.path.exists(path):
			raise FileNotFoundError("Путь '%s' не существует" % path)
		else:
			self._filePath = path
	def root(self):				return self._root
	def setRoot(self, root):
		if not os.path.exists(root):
			raise FileNotFoundError("Путь '%s' не существует")
		self._root = root
	def rootName(self):
		return os.path.basename(self._root)

	def iterParents(self):
		ret = [self.parent()]
		if ret[-1]:
			ret.extend(ret[-1].iterParents())
		return ret
	def iterChildren(self):
		ret = []
		for i in range(self.childCount()):
			child = self.child(i)
			ret.append(child)
			if child.childCount():
				ret.extend(child.iterChildren())
		return ret
	def children(self):
		return (self.child(i) for i in range(self.childCount()))
	def getPath(self):
		return PATH_SPLITTER.join([it.text(0) + it.itemType() for it in list(reversed(self.iterParents()))[1:]] + [self.text(0) + self.itemType()])

	def toListTreeItem(self):
		item = createListTreeItem(self.name(), self.itemType(), self.itemCategory())
		item.setFilePath(self.filePath())
		return item

	def getLayerName(self):
		layer_name = self._root.split('\\')[-1]
		return layer_name 


class TreeModelDirItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelDirItem, self).__init__(parent, ITEM_DIR, item_category)
#########################################################################################################

	def archive_dsn_loc_proj(self, silent=False):
		# Главный объект N-Mine
		main_app = iterWidgetParents(self.treeWidget())[-1]
		# ITEM_DSN
		itcat = self.itemCategory()
		# Каркасы
		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]
		# Имя локального проекта
		dsn_loc_proj_name = self.name()
		# Путь к локальному проекту
		dsn_loc_proj_path = self.filePath()

		# Имя участка можно получить или через запрос к базе данных
		# или сначала получив имя залежи self.root() и спуститься на один уровень вниз
		# main_app.connect_to_database()
		# cmd = """SELECT D.DepartName
		# 		FROM [{0}].[dbo].[DEPARTS] D
		# 		JOIN [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] T ON D.DepartId = T.DepartId
		# 		JOIN [{0}].[dbo].[LOCAL_PROJECT] N ON N.LOC_PROJECT_ID = T.LOC_PROJECT_ID
		# 		WHERE N.LOC_PROJECT_NAME = ?
		# 		""".format(main_app.srvcnxn.dbsvy)
		# params = (self.name())
		# data = main_app.srvcnxn.exec_query(cmd, params).get_rows()

		# Получаем имена залежи и участка
		dir_list = self.filePath().split('\\')
		# Имя залежи
		layer_name = self.rootName()
		# Имя участка
		minesite_name = dir_list[dir_list.index(layer_name)+1]
		# Путь к участку
		dir_minesite = '\\'.join(dir_list[:dir_list.index(minesite_name)+1])
		# Путь к залежи
		dir_layer = self.root()
		# Путь к архиву
		dir_archive = dir_minesite + '\\' + DIR_ARCHIVE
		# Проверка существования папки архива в залежи и её создание, если False
		if not os.path.exists(dir_archive):
			os.makedirs(dir_archive)
		# Директория для архивирования
		cur_target_dir = dir_archive
		# Проверка существование промежуточных папок между участком и локальным проектом
		# и их создание если False
		start_index = dir_list.index(minesite_name)
		end_index = dir_list.index(self.name())
		folders_to_check = dir_list[start_index + 1: end_index]
		if folders_to_check:
			for i in folders_to_check:
				cur_target_dir += '\\' + i
				if not os.path.exists(cur_target_dir):
					os.makedirs(cur_target_dir)

		minesite_item = self
		for i in range(self.getPath().count("***")): minesite_item = minesite_item.parent()
		## Архивирование локального проекта
		target_dir = cur_target_dir
		target_dir_loc_proj = target_dir + '\\' + dsn_loc_proj_name
		if os.path.exists(target_dir_loc_proj):
			if qtmsgbox.show_question(self, "Внимание",
											"Папка локального проекта уже существует в архиве. Перезаписать?"):
				shutil.rmtree(target_dir_loc_proj)
			else:
				return
		# Архивирование папки локального проекта
		shutil.move(self.filePath(), target_dir)
		# Обновление дерева каталогов и файлов
		self.parent().treeWidget().parent().populate(self.parent(), needsFilter=False)
		minesite_item.treeWidget().parent().populate(minesite_item, needsFilter=False)
		return True

	def unarchive_dsn_loc_proj(self, silent=False):
		# Главный объект N-Mine
		main_app = iterWidgetParents(self.treeWidget())[-1]
		# ITEM_DSN
		itcat = self.itemCategory()
		# Каркасы
		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]
		# Имя локального проекта
		dsn_loc_proj_name = self.name()
		# Путь к локальному проекту в архиве
		dsn_loc_proj_path = self.filePath()
		# Получаем имена залежи и участка
		dir_list = self.filePath().split('\\')
		# Имя залежи
		layer_name = self.rootName()
		# Имя участка
		minesite_name = dir_list[dir_list.index(layer_name) + 1]
		# Путь к участку
		dir_minesite = '\\'.join(dir_list[:dir_list.index(minesite_name) + 1])
		# Путь к залежи
		dir_layer = self.root()
		# Директория для восстановления
		cur_target_dir = dir_minesite
		# # Проверка существование промежуточных папок между участком и локальным проектом
		# # и их создание если False
		start_index = dir_list.index(DIR_ARCHIVE)
		end_index = dir_list.index(self.name())
		folders_to_check = dir_list[start_index + 1: end_index]
		if folders_to_check:
			for i in folders_to_check:
				cur_target_dir += '\\' + i
				if not os.path.exists(cur_target_dir):
					os.makedirs(cur_target_dir)
		# Получение ссылки на айтем залежи
		minesite_item = self
		for i in range(self.getPath().count("***")): minesite_item = minesite_item.parent()
		# Восстановление локального проекта из архива
		# Корневая директория, куда записывается локальный проект
		target_dir = cur_target_dir
		# Целевая директория локального проекта
		target_dir_loc_proj = target_dir + '\\' + dsn_loc_proj_name
		# Диалоговое окно при проверке существования папки локального проекта
		if os.path.exists(target_dir_loc_proj):
			if qtmsgbox.show_question(self, "Внимание",
											"Папка локального проекта уже существует. Перезаписать?"):
				shutil.rmtree(target_dir_loc_proj)
			else:
				return
		# Восстановление папки локального проекта их архива
		shutil.move(self.filePath(), target_dir)
		# Обновление дерева каталогов и файлов
		self.parent().treeWidget().parent().populate(self.parent(), needsFilter=False)
		minesite_item.treeWidget().parent().populate(minesite_item, needsFilter=False)
		return True
##########################################################################################################

class TreeModelTridbItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelTridbItem, self).__init__(parent, ITEM_TRIDB, item_category)
	def readWireframeNames(self):
		# clear existing children
		for i in range(self.childCount()):
			self.removeChild(self.child(0))
		# read wireframes
		if not os.path.exists(self.filePath()):
			qtmsgbox.show_error(self.treeWidget(), "Ошибка", "Файл '%s' не найден" % self.filePath())
			return

		cnxn = sqlite3.connect(self.filePath())
		names = [row[0] for row in cnxn.execute("select Name from GeneralInformation")]
		cnxn.close()

		#
		for name in names:
			it = createTreeModelItem(self, name, ITEM_WIREFRAME, self.itemCategory())
			# it.setFilePath(os.path.join(self.filePath(), name))
			it.setFilePath(self.filePath())
			it.setRoot(self.root())
		self.setExpanded(False)

	def allowModify(self):			return self._allowModify
	def setAllowModify(self, val):	self._allowModify = bool(val)
	def modifyMode(self):			return self._modifyMode
	def setModifyMode(self, v):		self._modifyMode = bool(v)

	def archiveSvyTriDB(self, silent=False):

		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()

		svy_user_attributes = list(SVY_USER_ATTRIBUTES)
		svy_user_attributes.append(main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())

		tridb = Tridb(self.filePath())
		srcWireframeNames = tridb.wireframe_names
		tridbAttributes = tridb.user_attributes
		tridb.close()

		# if set(tridbAttributes) != set(SVY_USER_ATTRIBUTES):
		# 	qtmsgbox.show_error(main_app, "Ошибка", "Файл tridb имеет неверные атрибуты")
		# 	return


		if itcat == ITEM_CATEGORY_SVY:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower():
				continue
			elif i == dirWf:
				break
			else:
				path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\' + dirWf)], DIR_ARCHIVE,
									   extra_path)
		else:
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\' + dirWf)], DIR_ARCHIVE)
		archiveExists = os.path.exists(archivePath)
		if not archiveExists:
			try:
				os.makedirs(archivePath)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка",
										"Невозможно создать директорию '%s'. %s" % (archivePath, str(e)))
				return

		targetTridbPath = os.path.join(archivePath, self.name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)
		if targetTridbExists:
			tridb = Tridb(targetTridbPath)
			userAttributes = tridb.user_attributes
			dstWireframeNames = tridb.wireframe_names
			tridb.close()
			srcname = self.name()
			if set(userAttributes) != set(svy_user_attributes):
				qtmsgbox.show_error(main_app, "Ошибка", "Архивный файл tridb имеет неверные атрибуты")
				return
			if dstWireframeNames:
				reply = qtmsgbox.show_question_triDB(main_app, "Внимание",
															 "Непустая выработка {} уже существует в Архиве.".format(srcname))
				if not silent and reply == 0:
					return
				if not silent and reply == 1:
					try:
						shutil.copyfile(self.filePath(), targetTridbPath)
						wireframelist = list(map(lambda x: x, self.children()))
						for i in srcWireframeNames:
							for j in wireframelist:
								if i == j.name():

									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, DIR_ARCHIVE)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(j.getPath().count("***")): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
									j.remove()
						return True
					except Exception as e:
						if not silent:
							qtmsgbox.show_error(main_app, "Ошибка",
												"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
						return
				if not silent and reply == 2:
					WireFrames_to_move = set(srcWireframeNames) - set(dstWireframeNames)
					wireframelist = list(map(lambda x: x, self.children()))
					for i in WireFrames_to_move:
						try:
							copy_wireframe(self.filePath(), i, targetTridbPath, i, copy_attributes=True)
							for j in wireframelist:
								if i == j.name():
									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, DIR_ARCHIVE)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(j.getPath().count("***")): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
									j.remove()

						except Exception as e:
							if not silent:
								qtmsgbox.show_error(main_app, "Ошибка",
													"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
							return
					return True
			else:
				try:
					shutil.copyfile(self.filePath(), targetTridbPath)
					wireframelist = list(map(lambda x: x, self.children()))
					for i in srcWireframeNames:
						for j in wireframelist:
							if i == j.name():

								srcname = dstname = j.name()
								to_iter = path[::-1]
								to_iter.insert(0, DIR_ARCHIVE)
								archiveDirItem = None
								targetTridbItem = None
								unitDirItem = j
								for i in range(j.getPath().count("***")): unitDirItem = unitDirItem.parent()
								for m in to_iter:
									for n in range(unitDirItem.childCount()):
										child = unitDirItem.child(n)
										if child.name() == m:
											unitDirItem = child
											archiveDirItem = child
											j.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
											break

								# поиск элемента tridb
								for n in range(archiveDirItem.childCount()):
									child = archiveDirItem.child(n)
									if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
										targetTridbItem = child
										break

								targetTridbItem.readWireframeNames()
								if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
									wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME,
																 j.itemCategory())
									wfItem.setFilePath(targetTridbPath)
									wfItem.setRoot(j.root())
								j.remove()
					return True
				except Exception as e:
					if not silent:
						qtmsgbox.show_error(main_app, "Ошибка",
											"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
					return
		else:
			try:
				unitDirItem = self
				to_iter = path[::-1]
				to_iter.insert(0, DIR_ARCHIVE)
				for i in range(self.getPath().count("***")): unitDirItem = unitDirItem.parent()
				curdir = self.filePath()[:self.filePath().find('\\' + dirWf)]
				if not archiveExists:
					for i in to_iter:
						curdir += '\\' + i
						archiveDirItem = createTreeModelItem(unitDirItem, i, ITEM_DIR, self.itemCategory())
						archiveDirItem.setFilePath(curdir)
						archiveDirItem.setRoot(self.root())
						unitDirItem = archiveDirItem
				archiveDirItem = None
				targetTridbItem = None
				unitDirItem = self
				for i in range(self.getPath().count("***")): unitDirItem = unitDirItem.parent()
				for j in to_iter:
					for i in range(unitDirItem.childCount()):
						child = unitDirItem.child(i)
						if child.name() == j:
							unitDirItem = child
							archiveDirItem = child
							self.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
							break

				shutil.copyfile(self.filePath(), targetTridbPath)
				wireframelist = list(map(lambda x: x, self.children()))
				for i in srcWireframeNames:
					for j in wireframelist:
						if i == j.name(): j.remove()
				# если выработка-источник не был раскрыт в виджете, то чтобы удалились каркасы внутри нее,
				# мы пересоздаем этот файл
				Tridb.create(self.filePath(), svy_user_attributes)
				return True
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка",
										"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
				return

	def unarchiveSvyTriDB(self, silent=False):

		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()

		svy_user_attributes = list(SVY_USER_ATTRIBUTES)
		svy_user_attributes.append(main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())

		if itcat == ITEM_CATEGORY_SVY:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]

		tridb = Tridb(self.filePath())
		srcWireframeNames = tridb.wireframe_names
		tridb.close()

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower():
				continue
			elif i == DIR_ARCHIVE:
				break
			else:
				path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)], dirWf,
											extra_path)
		else:
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)], dirWf)

		wireframeDirExists = os.path.exists(wireframeDirPath)
		if not wireframeDirExists:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Директория '%s' не найдена" % wireframeDirPath)
			return

		targetTridbPath = os.path.join(wireframeDirPath, self.name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)
		if not targetTridbExists:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Выработка '%s' не найдена" % targetTridbPath)
			return
		if targetTridbExists:
			tridb = Tridb(targetTridbPath)
			userAttributes = tridb.user_attributes
			dstWireframeNames = tridb.wireframe_names
			tridb.close()
			srcname = self.name()
			if set(userAttributes) != set(svy_user_attributes):
				qtmsgbox.show_error(main_app, "Ошибка", "Архивный файл tridb имеет неверные атрибуты")
				return
			if dstWireframeNames:
				reply = qtmsgbox.show_question_triDB(main_app, "Внимание",
															 "Непустая выработка {} уже существует в Каркасах.".format(srcname))
				if not silent and reply == 0:
					return
				if not silent and reply == 1:
					try:
						shutil.copyfile(self.filePath(), targetTridbPath)
						wireframelist = list(map(lambda x: x, self.children()))
						for i in srcWireframeNames:
							for j in wireframelist:
								if i == j.name():
									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, dirWf)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(j.getPath().count("***")): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
						self.parent().removeChild(self)
						os.remove(self.filePath())
						return True
					except Exception as e:
						if not silent:
							qtmsgbox.show_error(main_app, "Ошибка",
												"Невозможно создать файл '%s'. %s" % (targetTridbPath, str(e)))
						return
				if not silent and reply == 2:
					try:
						WireFrames_to_move = set(srcWireframeNames) - set(dstWireframeNames)
						wireframelist = list(map(lambda x: x, self.children()))
						for i in WireFrames_to_move:
							copy_wireframe(self.filePath(), i, targetTridbPath, i, copy_attributes=True)
							for j in wireframelist:
								if i == j.name():
									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, dirWf)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(j.getPath().count("***")): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
						self.parent().removeChild(self)
						os.remove(self.filePath())
					except Exception as e:
						if not silent:
							qtmsgbox.show_error(main_app, "Ошибка",
												"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
						return
					return True
			else:
				try:
					shutil.move(self.filePath(), targetTridbPath)
					wireframelist = list(map(lambda x: x, self.children()))
					for i in srcWireframeNames:
						for j in wireframelist:
							if i == j.name():

								srcname = dstname = j.name()
								to_iter = path[::-1]
								to_iter.insert(0, dirWf)
								archiveDirItem = None
								targetTridbItem = None
								unitDirItem = j
								for i in range(j.getPath().count("***")): unitDirItem = unitDirItem.parent()
								for m in to_iter:
									for n in range(unitDirItem.childCount()):
										child = unitDirItem.child(n)
										if child.name() == m:
											unitDirItem = child
											archiveDirItem = child
											j.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
											break

								# поиск элемента tridb
								for n in range(archiveDirItem.childCount()):
									child = archiveDirItem.child(n)
									if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
										targetTridbItem = child
										break

								targetTridbItem.readWireframeNames()
								if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
									wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME,
																 j.itemCategory())
									wfItem.setFilePath(targetTridbPath)
									wfItem.setRoot(j.root())
					self.parent().removeChild(self)
					return True
				except Exception as e:
					if not silent:
						qtmsgbox.show_error(main_app, "Ошибка",
											"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
					return

	def archiveDsnTriDB(self, silent=False):

		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()

		tridb = Tridb(self.filePath())
		srcWireframeNames = tridb.wireframe_names
		tridbAttributes = tridb.user_attributes
		tridb.close()

		# if set(tridbAttributes) != set(DSN_USER_ATTRIBUTES):
		# 	qtmsgbox.show_error(main_app, "Ошибка", "Файл tridb имеет неверные атрибуты")
		# 	return

		if itcat == ITEM_CATEGORY_DSN:        dirWf = \
			tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower():
				continue
			elif i == dirWf:
				break
			else:
				path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\' + dirWf)],
									   DIR_ARCHIVE,
									   extra_path)
		else:
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\' + dirWf)],
									   DIR_ARCHIVE)
		archiveExists = os.path.exists(archivePath)
		if not archiveExists:
			try:
				os.makedirs(archivePath)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка",
										"Невозможно создать директорию '%s'. %s" % (
										archivePath, str(e)))
				return

		targetTridbPath = os.path.join(archivePath, self.name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)
		if targetTridbExists:
			tridb = Tridb(targetTridbPath)
			userAttributes = tridb.user_attributes
			dstWireframeNames = tridb.wireframe_names
			tridb.close()
			srcname = self.name()
			if set(userAttributes) != set(DSN_USER_ATTRIBUTES):
				qtmsgbox.show_error(main_app, "Ошибка", "Архивный файл tridb имеет неверные атрибуты")
				return
			if dstWireframeNames:
				reply = qtmsgbox.show_question_triDB(main_app, "Внимание",
													 "Непустая выработка {} уже существует в Архиве.".format(
														 srcname))
				if not silent and reply == 0:
					return
				if not silent and reply == 1:
					try:
						shutil.copyfile(self.filePath(), targetTridbPath)
						wireframelist = list(map(lambda x: x, self.children()))
						for i in srcWireframeNames:
							for j in wireframelist:
								if i == j.name():

									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, DIR_ARCHIVE)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(
										j.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem,
																				 needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in
													   targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname,
																	 ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
									j.remove()
						return True
					except Exception as e:
						if not silent:
							qtmsgbox.show_error(main_app, "Ошибка",
												"Невозможно скопировать каркас '%s'. %s" % (
												self.name(), str(e)))
						return
				if not silent and reply == 2:
					WireFrames_to_move = set(srcWireframeNames) - set(dstWireframeNames)
					wireframelist = list(map(lambda x: x, self.children()))
					for i in WireFrames_to_move:
						try:
							copy_wireframe(self.filePath(), i, targetTridbPath, i, copy_attributes=True)
							for j in wireframelist:
								if i == j.name():
									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, DIR_ARCHIVE)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(
										j.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem,
																				 needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in
													   targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname,
																	 ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
									j.remove()

						except Exception as e:
							if not silent:
								qtmsgbox.show_error(main_app, "Ошибка",
													"Невозможно скопировать каркас '%s'. %s" % (
													self.name(), str(e)))
							return
					return True
			else:
				try:
					shutil.copyfile(self.filePath(), targetTridbPath)
					wireframelist = list(map(lambda x: x, self.children()))
					for i in srcWireframeNames:
						for j in wireframelist:
							if i == j.name():

								srcname = dstname = j.name()
								to_iter = path[::-1]
								to_iter.insert(0, DIR_ARCHIVE)
								archiveDirItem = None
								targetTridbItem = None
								unitDirItem = j
								for i in range(
									j.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
								for m in to_iter:
									for n in range(unitDirItem.childCount()):
										child = unitDirItem.child(n)
										if child.name() == m:
											unitDirItem = child
											archiveDirItem = child
											j.treeWidget().parent().populate(archiveDirItem,
																			 needsFilter=False)
											break

								# поиск элемента tridb
								for n in range(archiveDirItem.childCount()):
									child = archiveDirItem.child(n)
									if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
										targetTridbItem = child
										break

								targetTridbItem.readWireframeNames()
								if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
									wfItem = createTreeModelItem(targetTridbItem, dstname,
																 ITEM_WIREFRAME,
																 j.itemCategory())
									wfItem.setFilePath(targetTridbPath)
									wfItem.setRoot(j.root())
								j.remove()
					return True
				except Exception as e:
					if not silent:
						qtmsgbox.show_error(main_app, "Ошибка",
											"Невозможно скопировать каркас '%s'. %s" % (
											self.name(), str(e)))
					return
		else:
			try:
				unitDirItem = self
				to_iter = path[::-1]
				to_iter.insert(0, DIR_ARCHIVE)
				for i in range(self.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
				curdir = self.filePath()[:self.filePath().find('\\' + dirWf)]
				if not archiveExists:
					for i in to_iter:
						curdir += '\\' + i
						archiveDirItem = createTreeModelItem(unitDirItem, i, ITEM_DIR,
															 self.itemCategory())
						archiveDirItem.setFilePath(curdir)
						archiveDirItem.setRoot(self.root())
						unitDirItem = archiveDirItem
				archiveDirItem = None
				targetTridbItem = None
				unitDirItem = self
				for i in range(self.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
				for j in to_iter:
					for i in range(unitDirItem.childCount()):
						child = unitDirItem.child(i)
						if child.name() == j:
							unitDirItem = child
							archiveDirItem = child
							self.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
							break

				shutil.copyfile(self.filePath(), targetTridbPath)
				wireframelist = list(map(lambda x: x, self.children()))
				for i in srcWireframeNames:
					for j in wireframelist:
						if i == j.name(): j.remove()
				# если выработка-источник не был раскрыт в виджете, то чтобы удалились каркасы внутри нее,
				# мы пересоздаем этот файл
				Tridb.create(self.filePath(), DSN_USER_ATTRIBUTES)
				return True
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка",
										"Невозможно скопировать каркас '%s'. %s" % (
										self.name(), str(e)))
				return

	def unarchiveDsnTriDB(self, silent=False):

		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()

		if itcat == ITEM_CATEGORY_DSN:        dirWf = \
			tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		tridb = Tridb(self.filePath())
		srcWireframeNames = tridb.wireframe_names
		tridb.close()

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower():
				continue
			elif i == DIR_ARCHIVE:
				break
			else:
				path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)],
											dirWf,
											extra_path)
		else:
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)],
											dirWf)

		wireframeDirExists = os.path.exists(wireframeDirPath)
		if not wireframeDirExists:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Директория '%s' не найдена" % wireframeDirPath)
			return

		targetTridbPath = os.path.join(wireframeDirPath, self.name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)
		if not targetTridbExists:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Выработка '%s' не найдена" % targetTridbPath)
			return

		if targetTridbExists:
			tridb = Tridb(targetTridbPath)
			userAttributes = tridb.user_attributes
			dstWireframeNames = tridb.wireframe_names
			tridb.close()
			srcname = self.name()
			if set(userAttributes) != set(DSN_USER_ATTRIBUTES):
				qtmsgbox.show_error(main_app, "Ошибка", "Архивный файл tridb имеет неверные атрибуты")
				return
			if dstWireframeNames:
				reply = qtmsgbox.show_question_triDB(main_app, "Внимание",
													 "Непустая выработка {} уже существует в Каркасах.".format(
														 srcname))
				if not silent and reply == 0:
					return
				if not silent and reply == 1:
					try:
						shutil.copyfile(self.filePath(), targetTridbPath)
						wireframelist = list(map(lambda x: x, self.children()))
						for i in srcWireframeNames:
							for j in wireframelist:
								if i == j.name():
									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, dirWf)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(
										j.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem,
																				 needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in
													   targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname,
																	 ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
						self.parent().removeChild(self)
						os.remove(self.filePath())
						return True
					except Exception as e:
						if not silent:
							qtmsgbox.show_error(main_app, "Ошибка",
												"Невозможно создать файл '%s'. %s" % (
												targetTridbPath, str(e)))
						return
				if not silent and reply == 2:
					try:
						WireFrames_to_move = set(srcWireframeNames) - set(dstWireframeNames)
						wireframelist = list(map(lambda x: x, self.children()))
						for i in WireFrames_to_move:
							copy_wireframe(self.filePath(), i, targetTridbPath, i, copy_attributes=True)
							for j in wireframelist:
								if i == j.name():
									srcname = dstname = j.name()
									to_iter = path[::-1]
									to_iter.insert(0, dirWf)
									archiveDirItem = None
									targetTridbItem = None
									unitDirItem = j
									for i in range(
										j.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
									for m in to_iter:
										for n in range(unitDirItem.childCount()):
											child = unitDirItem.child(n)
											if child.name() == m:
												unitDirItem = child
												archiveDirItem = child
												j.treeWidget().parent().populate(archiveDirItem,
																				 needsFilter=False)
												break

									# поиск элемента tridb
									for n in range(archiveDirItem.childCount()):
										child = archiveDirItem.child(n)
										if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
											targetTridbItem = child
											break

									targetTridbItem.readWireframeNames()
									if not dstname in [it.name() for it in
													   targetTridbItem.iterChildren()]:
										wfItem = createTreeModelItem(targetTridbItem, dstname,
																	 ITEM_WIREFRAME,
																	 j.itemCategory())
										wfItem.setFilePath(targetTridbPath)
										wfItem.setRoot(j.root())
						os.remove(self.filePath())
						self.parent().removeChild(self)
					except Exception as e:
						if not silent:
							qtmsgbox.show_error(main_app, "Ошибка",
												"Невозможно скопировать каркас '%s'. %s" % (
												self.name(), str(e)))
						return
					return True
			else:
				try:
					shutil.move(self.filePath(), targetTridbPath)
					wireframelist = list(map(lambda x: x, self.children()))
					for i in srcWireframeNames:
						for j in wireframelist:
							if i == j.name():

								srcname = dstname = j.name()
								to_iter = path[::-1]
								to_iter.insert(0, dirWf)
								archiveDirItem = None
								targetTridbItem = None
								unitDirItem = j
								for i in range(
									j.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
								for m in to_iter:
									for n in range(unitDirItem.childCount()):
										child = unitDirItem.child(n)
										if child.name() == m:
											unitDirItem = child
											archiveDirItem = child
											j.treeWidget().parent().populate(archiveDirItem,
																			 needsFilter=False)
											break

								# поиск элемента tridb
								for n in range(archiveDirItem.childCount()):
									child = archiveDirItem.child(n)
									if child.name() == j.parent().name() and child.filePath().lower() == targetTridbPath.lower():
										targetTridbItem = child
										break

								targetTridbItem.readWireframeNames()
								if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
									wfItem = createTreeModelItem(targetTridbItem, dstname,
																 ITEM_WIREFRAME,
																 j.itemCategory())
									wfItem.setFilePath(targetTridbPath)
									wfItem.setRoot(j.root())
					self.parent().removeChild(self)
					return True
				except Exception as e:
					if not silent:
						qtmsgbox.show_error(main_app, "Ошибка",
											"Невозможно скопировать каркас '%s'. %s" % (
											self.name(), str(e)))
					return
###end edit

class TreeModelWireframeItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelWireframeItem, self).__init__(parent, ITEM_WIREFRAME, item_category)

	def archiveGeo(self, silent = False):
		# нет проверки на атрибуты файла-источника
		# в результате если в папке для проектировщиков лежит файл для маршейдеров,
		# то он будет спокойно архивироваться
		# никакой проверки на атрибуты при разархивировании нет
		main_app = iterWidgetParents(self.treeWidget())[-1]
		#
		archivePath = os.path.join(os.path.dirname(os.path.dirname(self.filePath())), DIR_ARCHIVE)
		archiveExists = os.path.exists(archivePath)
		if not archiveExists:
			try:
				os.mkdir(archivePath)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка", "Невозможно создать директорию '%s'. %s" % (archivePath, str(e)))
				return
		#
		targetTridbPath = os.path.join(archivePath, self.parent().parent().name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)
		geo_user_attributes = list(GEO_USER_ATTRIBUTES)
		geo_user_attributes.insert(1, main_app.settings.getValue(ID_GEO_UDSPNAME).upper())
		if not targetTridbExists:
			try:
				Tridb.create(targetTridbPath, geo_user_attributes)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка", "Невозможно создать файл '%s'. %s" % (targetTridbPath, str(e)))
				return
		else:
			tridb = Tridb(targetTridbPath)
			userAttributes = tridb.user_attributes
			tridb.close()
			if set(userAttributes) != set(geo_user_attributes):
				qtmsgbox.show_error(main_app, "Ошибка", "Архивный файл tridb имеет неверные атрибуты")
				return
		#
		srcname = dstname = self.name()
		#
		dstTridb = Tridb(targetTridbPath)
		dstWireframeNames = dstTridb.wireframe_names
		dstTridb.close()
		#
		wireframeExists = srcname not in dstWireframeNames
		if not wireframeExists:
			if not silent and not qtmsgbox.show_question(main_app, "Внимание", "Каркас '%s' уже существует в архиве. Заменить?" % srcname):
				return
		
		# добавление элемента дерева
		#
		unitDirItem = self.parent().parent().parent()
		targetTridbItem = None
		archiveDirItem = None
		# поиск элемента директории архива
		for i in range(unitDirItem.childCount()):
			child = unitDirItem.child(i)
			if child.name() == DIR_ARCHIVE and child.filePath().lower() == archivePath.lower():
				archiveDirItem = child
				break
		# 
		if archiveDirItem is None:
			archiveDirItem = createTreeModelItem(unitDirItem, DIR_ARCHIVE, ITEM_DIR, self.itemCategory())
			archiveDirItem.setFilePath(archivePath)
			archiveDirItem.setRoot(self.root())
		#
		# поиск элемента tridb
		self.treeWidget().parent().populate(archiveDirItem, needsFilter = True)
		for i in range(archiveDirItem.childCount()):
			child = archiveDirItem.child(i)
			if child.name() == self.parent().parent().name() and child.filePath().lower() == targetTridbPath.lower():
				targetTridbItem = child
				break

		#
		try:
			copy_wireframe(self.filePath(), srcname, targetTridbPath, dstname, copy_attributes = True)
		except Exception as e:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
			return

		# добавление элемента tridb, если необходимо
		if not targetTridbItem:
			targetTridbItem = createTreeModelItem(archiveDirItem, self.parent().parent().name(), ITEM_TRIDB, self.itemCategory())
			targetTridbItem.setFilePath(targetTridbPath)
			targetTridbItem.setRoot(self.root())
		# добавление элемента каркаса, если необходимо
		targetTridbItem.readWireframeNames()
		if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
			wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME, self.itemCategory())
			wfItem.setFilePath(targetTridbPath)
			wfItem.setRoot(self.root())
		# удаление исходного каркаса
		self.remove()

		return True
	def archiveSvy(self, silent = False):
		# нет проверки на атрибуты файла-источника
		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()

		svy_user_attributes = list(SVY_USER_ATTRIBUTES)
		svy_user_attributes.append(main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())

		if itcat == ITEM_CATEGORY_SVY:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower(): continue
			elif i == dirWf: break
			else: path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\'+dirWf)], DIR_ARCHIVE, extra_path)
		else:
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\'+dirWf)], DIR_ARCHIVE)
		archiveExists = os.path.exists(archivePath)
		if not archiveExists:
			try:
				os.makedirs(archivePath)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка", "Невозможно создать директорию '%s'. %s" % (archivePath, str(e)))
				return
		#
		targetTridbPath = os.path.join(archivePath, self.parent().name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)

		if not targetTridbExists:
			try:
				Tridb.create(targetTridbPath, svy_user_attributes)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка", "Невозможно создать файл '%s'. %s" % (targetTridbPath, str(e)))
				return
		else:
			tridb = Tridb(targetTridbPath)
			userAttributes = tridb.user_attributes
			tridb.close()
			if set(userAttributes) != set(svy_user_attributes):
				qtmsgbox.show_error(main_app, "Ошибка", "Архивный файл tridb имеет неверные атрибуты")
				return

		#
		# имя каркаса
		srcname = dstname = self.name()
		#
		dstTridb = Tridb(targetTridbPath)
		# имена каркасов в tridb файле в архиве
		dstWireframeNames = dstTridb.wireframe_names
		dstTridb.close()
		#
		# проверка наличия каркаса в выработке
		wireframeExists = srcname not in dstWireframeNames
		if not wireframeExists:
			if not silent and not qtmsgbox.show_question(main_app, "Внимание", "Каркас '%s' уже существует в архиве. Заменить?" % srcname):
				return

		unitDirItem = self
		to_iter = path[::-1]
		to_iter.insert(0, DIR_ARCHIVE)
		for i in range(self.getPath().count("***")): unitDirItem = unitDirItem.parent()
		curdir = self.filePath()[:self.filePath().find('\\'+dirWf)]
		if not archiveExists:
			for i in to_iter:
				curdir += '\\' + i
				archiveDirItem = createTreeModelItem(unitDirItem, i, ITEM_DIR, self.itemCategory())
				archiveDirItem.setFilePath(curdir)
				archiveDirItem.setRoot(self.root())
				unitDirItem = archiveDirItem


		archiveDirItem = None
		targetTridbItem = None
		unitDirItem = self
		for i in range(self.getPath().count("***")): unitDirItem = unitDirItem.parent()
		for j in to_iter:
			for i in range(unitDirItem.childCount()):
				child = unitDirItem.child(i)
				if child.name() == j:
					unitDirItem = child
					archiveDirItem = child
					self.treeWidget().parent().populate(archiveDirItem, needsFilter=False)
					break

		#поиск элемента tridb
		for i in range(archiveDirItem.childCount()):
			child = archiveDirItem.child(i)
			if child.name() == self.parent().name() and child.filePath().lower() == targetTridbPath.lower():
				targetTridbItem = child
				break


		try:
			copy_wireframe(self.filePath(), srcname, targetTridbPath, dstname, copy_attributes = True)
		except Exception as e:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
			return

		# добавление элемента tridb, если необходимо
		if not targetTridbItem:
			targetTridbItem = createTreeModelItem(archiveDirItem, self.parent().name(), ITEM_TRIDB, self.itemCategory())
			targetTridbItem.setFilePath(targetTridbPath)
			targetTridbItem.setRoot(self.root())
		# добавление элемента каркаса, если необходимо
		targetTridbItem.readWireframeNames()
		if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
			wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME, self.itemCategory())
			wfItem.setFilePath(targetTridbPath)
			wfItem.setRoot(self.root())
		# удаление исходного каркаса
		self.remove()

		return True

	def archiveDsn(self, silent=False):
		# нет проверки на атрибуты файла-источника
		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()
		if itcat == ITEM_CATEGORY_GEO:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]
		if itcat == ITEM_CATEGORY_DSN:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower():
				continue
			elif i == dirWf:
				break
			else:
				path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\' + dirWf)], DIR_ARCHIVE,
									   extra_path)
		else:
			archivePath = os.path.join(self.filePath()[:self.filePath().find('\\' + dirWf)], DIR_ARCHIVE)
		archiveExists = os.path.exists(archivePath)
		if not archiveExists:
			try:
				os.makedirs(archivePath)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка",
										"Невозможно создать директорию '%s'. %s" % (archivePath, str(e)))
				return
		# #
		targetTridbPath = os.path.join(archivePath, self.parent().name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)
		if not targetTridbExists:
			try:
				Tridb.create(targetTridbPath, DSN_USER_ATTRIBUTES)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка",
										"Невозможно создать файл '%s'. %s" % (targetTridbPath, str(e)))
				return
		else:
			tridb = Tridb(targetTridbPath)
			userAttributes = tridb.user_attributes
			tridb.close()
			if set(userAttributes) != set(DSN_USER_ATTRIBUTES):
				qtmsgbox.show_error(main_app, "Ошибка", "Архивный файл tridb имеет неверные атрибуты")
				return
		# #
		# имя каркаса
		srcname = dstname = self.name()
		#
		dstTridb = Tridb(targetTridbPath)
		# имена каркасов в tridb файле в архиве
		dstWireframeNames = dstTridb.wireframe_names
		dstTridb.close()
		#
		# проверка наличия каркаса в выработке
		wireframeExists = srcname not in dstWireframeNames
		if not wireframeExists:
			if not silent and not qtmsgbox.show_question(main_app, "Внимание",
														 "Каркас '%s' уже существует в архиве. Заменить?" % srcname):
				return




		unitDirItem = self
		to_iter = path[::-1]
		to_iter.insert(0, DIR_ARCHIVE)
		for i in range(self.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
		curdir = self.filePath()[:self.filePath().find('\\' + dirWf)]
		if not archiveExists:
			for i in to_iter:
				curdir += '\\' + i
				archiveDirItem = createTreeModelItem(unitDirItem, i, ITEM_DIR, self.itemCategory())
				archiveDirItem.setFilePath(curdir)
				archiveDirItem.setRoot(self.root())
				unitDirItem = archiveDirItem

		archiveDirItem = None
		targetTridbItem = None
		unitDirItem = self
		for i in range(self.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
		to_iter = path[::-1]
		to_iter.insert(0, DIR_ARCHIVE)
		for j in to_iter:
			for i in range(unitDirItem.childCount()):
				child = unitDirItem.child(i)
				if child.name() == j:
					unitDirItem = child
					archiveDirItem = child
					self.treeWidget().parent().populate(archiveDirItem, needsFilter=True)
					break

		for i in range(archiveDirItem.childCount()):
			child = archiveDirItem.child(i)
			if child.name() == self.parent().name() and child.filePath().lower() == targetTridbPath.lower():
				targetTridbItem = child
				break

		try:
			copy_wireframe(self.filePath(), srcname, targetTridbPath, dstname, copy_attributes=True)
		except Exception as e:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка",
									"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
			return

		#добавление элемента tridb, если необходимо
		if not targetTridbItem:
			targetTridbItem = createTreeModelItem(archiveDirItem, self.parent().name(), ITEM_TRIDB, self.itemCategory())
			targetTridbItem.setFilePath(targetTridbPath)
			targetTridbItem.setRoot(self.root())
		# добавление элемента каркаса, если необходимо
		targetTridbItem.readWireframeNames()
		if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
			wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME, self.itemCategory())
			wfItem.setFilePath(targetTridbPath)
			wfItem.setRoot(self.root())

		self.remove()

		return True



	def unarchiveGeo(self, silent = False):
		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()
		if itcat != ITEM_CATEGORY_GEO:
			return
		#
		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		#
		wireframeDirPath = os.path.join(os.path.dirname(os.path.dirname(self.filePath())), dirWf)
		wireframeDirExists = os.path.exists(wireframeDirPath)
		if not wireframeDirExists:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Директория '%s' не найдена" % wireframeDirPath)
			return
		#
		targetTridbPath = os.path.join(wireframeDirPath, self.parent().name() + ITEM_TRIDB)
		targetTridbExists = os.path.exists(targetTridbPath)
		geo_user_attributes = list(GEO_USER_ATTRIBUTES)
		geo_user_attributes.insert(1, main_app.settings.getValue(ID_GEO_UDSPNAME).upper())
		if not targetTridbExists:
			qtmsgbox.show_error(main_app, "Ошибка", "Файл геологических каркасов не найден")
			return
			try:
				Tridb.create(targetTridbPath, geo_user_attributes)
			except Exception as e:
				if not silent:
					qtmsgbox.show_error(main_app, "Ошибка", "Невозможно создать файл '%s'. %s" % (targetTridbPath, str(e)))
				return

		#
		srcname = dstname = self.name()
		#
		dstTridb = Tridb(targetTridbPath)
		dstWireframeNames = dstTridb.wireframe_names
		dstTridb.close()
		#
		wireframeExists = srcname not in dstWireframeNames
		if not wireframeExists:
			if not silent and not qtmsgbox.show_question(main_app, "Внимание", "Каркас '%s' уже существует. Заменить?" % srcname):
				return
		# добавление элемента дерева
		#
		unitDirItem = self.parent().parent().parent()
		targetTridbItem = None
		wfModelItem = None
		# поиск элемента директории архива
		for i in range(unitDirItem.childCount()):
			child = unitDirItem.child(i)
			if child.name() == self.parent().name() and child.isModel():
				wfModelItem = child
				break
		# 
		if wfModelItem is None:
			qtmsgbox.show_error(main_app, "Ошибка", "Модель '%s' не определена" % self.name())
			return

		# поиск элемента tridb
		self.treeWidget().parent().populate(wfModelItem, needsFilter = True)
		for i in range(wfModelItem.childCount()):
			child = wfModelItem.child(i)
			if child.name() == "Геологические каркасы" and child.filePath().lower() == targetTridbPath.lower():
				targetTridbItem = child
				break
		#
		if not targetTridbItem:
			qtmsgbox.show_error(main_app, "Ошибка", "Невозможно найти элемент 'Геологические каркасы'")
			return
		#
		try:
			copy_wireframe(self.filePath(), srcname, targetTridbPath, dstname, copy_attributes = True)
		except Exception as e:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
			return

		# добавление элемента каркаса, если необходимо
		targetTridbItem.readWireframeNames()
		if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
			wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME, self.itemCategory())
			wfItem.setFilePath(targetTridbPath)
			wfItem.setRoot(self.root())
		# удаление исходного каркаса
		self.remove()

		return True
	def unarchiveSvy(self, silent = False):

		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()
		#
		if itcat == ITEM_CATEGORY_GEO:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower(): continue
			elif i == DIR_ARCHIVE: break
			else: path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)], dirWf, extra_path)
		else:
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)], dirWf)
		######## end edit
		# проверка существования папки, куда будет восстановлен каркас
		wireframeDirExists = os.path.exists(wireframeDirPath)

		if not wireframeDirExists:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Директория '%s' не найдена" % wireframeDirPath)
			return
		#
		targetTridbPath = os.path.join(wireframeDirPath, self.parent().name() + ITEM_TRIDB)
		# проверка существования файла TRIDB, куда будет восстановлен каркас
		targetTridbExists = os.path.exists(targetTridbPath)

		if not targetTridbExists:
			qtmsgbox.show_error(main_app, "Ошибка", "Каркасы выработки '%s' не найден" % self.parent().name())
			return

		#
		srcname = dstname = self.name()
		#
		dstTridb = Tridb(targetTridbPath)
		dstWireframeNames = dstTridb.wireframe_names
		dstTridb.close()
		#
		wireframeExists = srcname not in dstWireframeNames
		if not wireframeExists:
			if not silent and not qtmsgbox.show_question(main_app, "Внимание", "Каркас '%s' уже существует. Заменить?" % srcname):
				return

		unitDirItem = self
		for i in range(self.getPath().count("***")): unitDirItem = unitDirItem.parent()
		targetTridbItem = None
		wfDirItem = None
		to_iter = path[::-1]
		to_iter.insert(0, dirWf)
		for j in to_iter:
			for i in range(unitDirItem.childCount()):
				child = unitDirItem.child(i)
				if child.name() == j:
					unitDirItem = child
					wfDirItem = child
					self.treeWidget().parent().populate(wfDirItem, needsFilter=True)
					break

		# поиск элемента tridb
		for i in range(wfDirItem.childCount()):
			child = wfDirItem.child(i)
			if child.name() == self.parent().name() and child.filePath().lower() == targetTridbPath.lower():
				targetTridbItem = child
				break
		if not targetTridbItem:
			return
		#
		try:
			copy_wireframe(self.filePath(), srcname, targetTridbPath, dstname, copy_attributes = True)
		except Exception as e:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
			return

		# добавление элемента каркаса, если необходимо
		targetTridbItem.readWireframeNames()
		if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
			wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME, self.itemCategory())
			wfItem.setFilePath(targetTridbPath)
			wfItem.setRoot(self.root())

		self.remove()

		return True

	def unarchiveDsn(self, silent=False):

		main_app = iterWidgetParents(self.treeWidget())[-1]
		itcat = self.itemCategory()

		if itcat == ITEM_CATEGORY_GEO:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]
		if itcat == ITEM_CATEGORY_DSN:        dirWf = \
		tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		path = []
		for i in self.filePath().split('\\')[::-1]:
			if ITEM_TRIDB.lower() in i.lower():
				continue
			elif i == DIR_ARCHIVE:
				break
			else:
				path.append(i)
		if path:
			extra_path = '\\'.join(path[::-1])
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)], dirWf,
											extra_path)
		else:
			wireframeDirPath = os.path.join(self.filePath()[:self.filePath().find('\\' + DIR_ARCHIVE)], dirWf)
		######## end edit
		# проверка существования папки, куда будет восстановлен каркас
		wireframeDirExists = os.path.exists(wireframeDirPath)

		if not wireframeDirExists:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка", "Директория '%s' не найдена" % wireframeDirPath)
			return
		#
		targetTridbPath = os.path.join(wireframeDirPath, self.parent().name() + ITEM_TRIDB)
		# проверка существования файла TRIDB, куда будет восстановлен каркас
		targetTridbExists = os.path.exists(targetTridbPath)

		if not targetTridbExists:
			qtmsgbox.show_error(main_app, "Ошибка", "Каркасы выработки '%s' не найден" % self.parent().name())
			return

		#
		srcname = dstname = self.name()
		#
		dstTridb = Tridb(targetTridbPath)
		dstWireframeNames = dstTridb.wireframe_names
		dstTridb.close()
		#
		wireframeExists = srcname not in dstWireframeNames
		if not wireframeExists:
			if not silent and not qtmsgbox.show_question(main_app, "Внимание",
														 "Каркас '%s' уже существует. Заменить?" % srcname):
				return


		unitDirItem = self
		for i in range(self.getPath().count("***")-1): unitDirItem = unitDirItem.parent()
		targetTridbItem = None
		wfDirItem = None
		to_iter = path[::-1]
		to_iter.insert(0, dirWf)
		for j in to_iter:
			for i in range(unitDirItem.childCount()):
				child = unitDirItem.child(i)
				if child.name() == j:
					unitDirItem = child
					wfDirItem = child
					self.treeWidget().parent().populate(wfDirItem, needsFilter=True)
					break

		# поиск элемента tridb
		for i in range(wfDirItem.childCount()):
			child = wfDirItem.child(i)
			if child.name() == self.parent().name() and child.filePath().lower() == targetTridbPath.lower():
				targetTridbItem = child
				break
		if not targetTridbItem:
			return

		try:
			copy_wireframe(self.filePath(), srcname, targetTridbPath, dstname, copy_attributes=True)
		except Exception as e:
			if not silent:
				qtmsgbox.show_error(main_app, "Ошибка",
									"Невозможно скопировать каркас '%s'. %s" % (self.name(), str(e)))
			return

		targetTridbItem.readWireframeNames()
		if not dstname in [it.name() for it in targetTridbItem.iterChildren()]:
			wfItem = createTreeModelItem(targetTridbItem, dstname, ITEM_WIREFRAME, self.itemCategory())
			wfItem.setFilePath(targetTridbPath)
			wfItem.setRoot(self.root())

		self.remove()

		return True





	def remove(self):
		tridbItem = self.parent()
		tripath = tridbItem.filePath()
		#
		tridbItem.removeChild(self)
		#
		tridb = Tridb(tripath)
		tridb.remove(self.text(0))
		tridb.commit()
		tridb.close()

class TreeModelDataItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelDataItem, self).__init__(parent, ITEM_DATA, item_category)
	def allowModify(self):			return self._allowModify
	def setAllowModify(self, val):	self._allowModify = bool(val)
	def modifyMode(self):			return self._modifyMode
	def setModifyMode(self, v):		self._modifyMode = bool(v)

class TreeModelStringItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelStringItem, self).__init__(parent, ITEM_STRING, item_category)
	def allowModify(self):			return self._allowModify
	def setAllowModify(self, val):	self._allowModify = bool(val)
	def modifyMode(self):			return self._modifyMode
	def setModifyMode(self, v):		self._modifyMode = bool(v)

class TreeModelAnnotationItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelAnnotationItem, self).__init__(parent, ITEM_ANNOTATION, item_category)
	def allowModify(self):			return self._allowModify
	def setAllowModify(self, val):	self._allowModify = bool(val)
	def modifyMode(self):			return self._modifyMode
	def setModifyMode(self, v):		self._modifyMode = bool(v)

class TreeModelSectionPackItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelSectionPackItem, self).__init__(parent, ITEM_SECTIONPACK, item_category)

	def readSectionNames(self):
		# clear existing children
		for i in range(self.childCount()):
			self.removeChild(self.child(0))
		# read wireframes
		filepath = self.filePath()
		if not os.path.exists(filepath):
			qtmsgbox.show_error(self.treeWidget(), "Ошибка", "Файл '%s' не найден" % filepath)
			return

		f = MicromineFile()
		try:
			if not f.open(filepath):
				qtmsgbox.show_error(self.treeWidget(), "Ошибка", "Невозможно открыть файл '%s'" % filepath)
				return
		except Exception as e:
			qtmsgbox.show_error(self.treeWidget(), "Ошибка", str(e))
			return

		names = [row[0] for row in f.read(columns = ["NAME"])]
		f.close()
		#
		for name in names:
			it = createTreeModelItem(self, name, ITEM_SECTION, self.itemCategory())
			it.setFilePath(os.path.join(filepath, name))
			it.setRoot(self.root())
		self.setExpanded(False)

class TreeModelSectionItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelSectionItem, self).__init__(parent, ITEM_SECTION, item_category)
		self._is_basis = True
	def setBasis(self, flag):		self._is_basis = bool(flag)
	def basis(self):				return self._is_basis

class TreeModelModelItem(TreeModelItem):
	def __init__(self, parent, item_category):
		super(TreeModelModelItem, self).__init__(parent, ITEM_MODEL, item_category)

	def is_need_transfer(self):
		main_app = iterWidgetParents(self.treeWidget())[-1]
		if not main_app.srvcnxn:
			main_app.connect_to_database()
		blockModel = BlockModel(self._filePath, main_app)
		if blockModel.has_metadata:
			return blockModel.metadata.is_need_transfer
		else:
			return False
	def set_need_transfer(self, is_need_transfer = False):
		main_app = iterWidgetParents(self.treeWidget())[-1]
		if not main_app.srvcnxn:
			main_app.connect_to_database()
		try:
			blockModel = BlockModel(self._filePath, main_app)

			if not blockModel.has_metadata:
				qtmsgbox.show_error(main_app, "Внимание", "Блочная модель не имеет метаданных")
				raise Exception("Блочная модель не имеет метаданных")
			if is_need_transfer == False :
				blockModel.metadata.is_need_transfer = False
				blockModel.save()
				return

			bm_current_date = blockModel.metadata.current_date 
			if bm_current_date is None:
				qtmsgbox.show_error(main_app, "Внимание", "Блочная модель не имеет текущей даты")
				raise Exception("Блочная модель не имеет текущей даты")
				
			today_date = get_current_time()
			if 	(	(today_date.year < bm_current_date.year) or
					(today_date.year == bm_current_date.year and today_date.month <= bm_current_date.month)	):
				qtmsgbox.show_information(main_app, "Внимание", "Блочная модель имеет текущую дату %s" % bm_current_date.strftime("%Y-%m"))
				return

			blockModel.metadata.is_need_transfer = True
			blockModel.save()
		except:
			main_app.srvcnxn.rollback()
			raise


	def rename(self):
		main_app = iterWidgetParents(self.treeWidget())[-1]
		#
		command = GeoRenameModel(main_app, use_connection = True)
		command.setModelItem(self)
		#
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def delete(self):
		main_app = iterWidgetParents(self.treeWidget())[-1]
		#
		command = GeoDeleteModel(main_app, use_connection = True)
		command.setModelItem(self)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

class TreeModelBlockModelItem(TreeModelItem):
	def __init__(self, parent, item_category = ITEM_CATEGORY_GEO):
		super(TreeModelBlockModelItem, self).__init__(parent, ITEM_BM, item_category)
	def is_need_transfer(self):
		main_app = iterWidgetParents(self.treeWidget())[-1]
		if not main_app.srvcnxn:
			main_app.connect_to_database()
		blockModel = BlockModel(self._filePath, main_app)
		if blockModel.has_metadata:
			return blockModel.metadata.is_need_transfer
		else:
			return False

class TreeModelStockSectorCommonItem(TreeModelItem):
	def __init__(self, parent, item_type, item_category = ITEM_CATEGORY_SVY):
		super(TreeModelStockSectorCommonItem, self).__init__(parent, item_type, item_category)

		self.setFlags(self.flags() | QtCore.Qt.ItemIsEditable)
		self.setTextAlignment(1, QtCore.Qt.AlignCenter)

	def density(self):				return self.text(1)
	def setDensity(self, value):	self.setText(1, str(value))

class TreeModelStockItem(TreeModelStockSectorCommonItem):
	def __init__(self, parent, item_category = ITEM_CATEGORY_SVY):
		super(TreeModelStockItem, self).__init__(parent, ITEM_STOCK, item_category)

		self._sectorDirItem = createTreeModelItem(self, "Секторы", ITEM_DIR, item_category)
		self._groundDirItem = createTreeModelItem(self, "Основания", ITEM_DIR, item_category)
		self._topoDirItem = createTreeModelItem(self, "Поверхности", ITEM_DIR, item_category)
	def sectorDirItem(self):		return self._sectorDirItem
	def groundDirItem(self):		return self._groundDirItem
	def topoDirItem(self):			return self._topoDirItem

	def addSector(self, sectorName):
		return createTreeModelItem(self._sectorDirItem, sectorName, ITEM_STOCKSECTOR, self.itemCategory())
	def addGround(self, wfItem):
		parent = wfItem.parent()
		itemTridbPath = PATH_SPLITTER.join([self.getPath(), self._groundDirItem.text(0) + self._groundDirItem.itemType(), parent.text(0) + parent.itemType()])
		itemWireframePath = PATH_SPLITTER.join([itemTridbPath, wfItem.text(0) + wfItem.itemType()])

		tree = self.treeWidget()
		if not tree.itemPathExists(itemTridbPath):
			tridbItem = createTreeModelItem(self._groundDirItem, wfItem.parent().text(0), ITEM_TRIDB, wfItem.parent().itemCategory())
			tridbItem.setFilePath(parent.filePath())
			tridbItem.setRoot(parent.root())
		else:
			tridbItem = tree.getItemByPath(itemTridbPath)
		#
		if not tree.itemPathExists(itemWireframePath):
			groundItem = createTreeModelItem(tridbItem, wfItem.text(0), ITEM_WIREFRAME, wfItem.itemCategory())
			groundItem.setFilePath(wfItem.filePath())
			groundItem.setRoot(wfItem.root())
		else:
			groundItem = None
		#
		return groundItem
	def addTopo(self, wfItem):
		parent = wfItem.parent()
		itemTridbPath = PATH_SPLITTER.join([self.getPath(), self._topoDirItem.text(0) + self._topoDirItem.itemType(), parent.text(0) + parent.itemType()])
		itemWireframePath = PATH_SPLITTER.join([itemTridbPath, wfItem.text(0) + wfItem.itemType()])

		tree = self.treeWidget()
		if not tree.itemPathExists(itemTridbPath):
			tridbItem = createTreeModelItem(self._topoDirItem, wfItem.parent().text(0), ITEM_TRIDB, wfItem.parent().itemCategory())
			tridbItem.setFilePath(parent.filePath())
			tridbItem.setRoot(parent.root())
		else:
			tridbItem = tree.getItemByPath(itemTridbPath)
		#
		if not tree.itemPathExists(itemWireframePath):
			topoItem = createTreeModelItem(tridbItem, wfItem.text(0), ITEM_WIREFRAME, wfItem.itemCategory())
			topoItem.setFilePath(wfItem.filePath())
			topoItem.setRoot(wfItem.root())
		else:
			topoItem = None
		#
		return topoItem
	def sectorItems(self):
		return self._sectorDirItem.iterChildren()

class TreeModelStockSectorItem(TreeModelStockSectorCommonItem):
	def __init__(self, parent, item_category = ITEM_CATEGORY_SVY):
		super(TreeModelStockSectorItem, self).__init__(parent, ITEM_STOCKSECTOR, item_category)
	def stockItem(self):
		return self.parent().parent()