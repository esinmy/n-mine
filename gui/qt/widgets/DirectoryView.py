from PyQt4 import QtCore, QtGui

from utils.constants import *
from utils.osutil import *
from gui.qt.widgets.TreeModel import TreeWidget
from gui.qt.widgets.TreeModelItem import *

class DirectoryView(TreeWidget):
	def __init__(self, *args, **kw):
		super(DirectoryView, self).__init__(*args, **kw)

		self.__folderPath = None
		self.__displayItemTypes = {ittype: True for ittype in ITEM_TYPE_LIST}

		self.headerItem().setText(0, "Имя")
		self.setHeaderHidden(True)

#### Start edit
	def mousePressEvent(self, event):
		if self.itemAt(event.pos()) is None:
			self.clearSelection()
		TreeWidget.mousePressEvent(self, event)

	def keyPressEvent(self, event):
		if (event.key() == QtCore.Qt.Key_Escape and
					event.modifiers() == QtCore.Qt.NoModifier):
			self.clearSelection()

#### End edit

	def folderPath(self):
		return self.__folderPath
	def setFolderPath(self, path):
		if not path or not os.path.exists(path) or not os.path.isdir(path):
			self.clear()
			return

		self.__folderPath = path
		self.populate()

	def displayItemTypes(self):
		return self.__displayItemTypes.copy()

	def hideItemType(self, ittype):
		if not ittype in self.__displayItemTypes:
			raise Exception("'%s' - неизвестный тип элемента дерева" % type(ittype))
		self.__displayItemTypes[ittype] = False
	def showItemType(self, ittype):
		if not ittype in self.__displayItemTypes:
			raise Exception("'%s' - неизвестный тип элемента дерева" % type(ittype))
		self.__displayItemTypes[ittype] = True
	
	def populate(self):
		self.clear()
		if self.__folderPath is None or not os.path.exists(self.__folderPath):
			return

		parentItemDict = {self.__folderPath: self.invisibleRootItem()}
		for rootPath, folderNames, fileNames in os.walk(self.__folderPath):
			# вставляем директории
			for folderName in folderNames:
				folderPath = os.path.join(rootPath, folderName)
				#
				item = createTreeModelItem(parentItemDict[rootPath], folderName, ITEM_DIR, ITEM_CATEGORY_USER)
				parentItemDict[folderPath] = item
				item.setFilePath(folderPath)
				item.setRoot(self.__folderPath)

			# вставляем файлы
			for fileName in fileNames:
				filePath = os.path.join(rootPath, fileName)
				extension = get_file_extension(fileName).upper()
				fileName = remove_file_extension(fileName)
				#
				if not self.__displayItemTypes.get(extension):
					continue
				#
				if extension == ITEM_DATA:
					if isBlockModelFile(filePath):	extension = ITEM_BM
					if isSectionFile(filePath):		extension = ITEM_SECTIONPACK
				#
				item = createTreeModelItem(parentItemDict[rootPath], fileName, extension, ITEM_CATEGORY_USER)
				item.setFilePath(filePath)
				item.setRoot(self.__folderPath)

	def expandAll(self):
		for childNode in self.iterChildren():
			childNode.setExpanded(True)
	def collapseAll(self):
		for childNode in self.iterChildren():
			childNode.setExpanded(False)
		
