from PyQt4 import QtCore, QtGui
import os

from utils.validation import DoubleValidator
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class TopCutGradeItemDelegate(QtGui.QStyledItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()

		editor = QtGui.QLineEdit(widget)
		if icol > 0:
			self._validator = DoubleValidator(0, 100000, 3)
		editor.setValidator(self._validator)
		return editor

class TopCutGradeGridItem(QtGui.QTableWidgetItem):
	def __init__(self, *args, **kw):
		super(TopCutGradeGridItem, self).__init__(*args, **kw)
		self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)

	def setReadOnly(self, value):
		if bool(value):			self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
		else:					self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)
	def readOnly(self):			return QtCore.Qt.ItemIsEditable in self.flags()

class MethodTopCutGradeGridItem(TopCutGradeGridItem):
	def __init__(self, *args, **kw):
		super(MethodTopCutGradeGridItem, self).__init__(*args, **kw)
		self.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
		self.setBackgroundColor(QtGui.QColor(240, 240, 240))

class SummaryTopCutGradeGridItem(MethodTopCutGradeGridItem):
	def __init__(self, *args, **kw):
		super(SummaryTopCutGradeGridItem, self).__init__(*args, **kw)
		self.setBackgroundColor(QtGui.QColor(225, 225, 225))

class TopCutGridWidget(QtGui.QTableWidget):
	def __init__(self, parent):
		super(TopCutGridWidget, self).__init__(parent)

		self.__createVariables()
		#
		self.horizontalHeader().setVisible(True)
		#
		self.verticalHeader().setVisible(False)
		self.verticalHeader().setDefaultSectionSize(20)
		#
		self.setItemDelegate(TopCutGradeItemDelegate())

	def __createVariables(self):
		self._methods = []
		self._hasDefaultRow = False
		self._hasUserRow = False

	def keyPressEvent(self, event):
		key = event.key()
		if key == QtCore.Qt.Key_Delete:
			selectedIndexes = self.selectedIndexes()
			for selectedIndex in selectedIndexes:
				iRow, iCol = selectedIndex.row(), selectedIndex.column()
				if iRow == self.rowCount()-1:
					self.item(iRow, iCol).setText("")
		else:
			super(QtGui.QTableWidget, self).keyPressEvent(event)

	def addMethod(self, method):
		# data = method.data()
		# methodName = method.name()

		# nRows = self.rowCount()
		# curRow = nRows-1
		# self.insertRow(curRow)

		# nameItem = MethodTopCutGradeGridItem(methodName)
		# self.setItem(0, curRow, nameItem)
		# for iCol, value in enumerate(data, start = 1):
		# 	item = MethodTopCutGradeGridItem(value)
		# 	item.setTextAlignment(QtCore.Qt.AlignRight)
		# 	self.setItem(iCol, curRow, item)

		# if len(methods) == 0:
		# 	self.insertRow(nRows-1)
		# 	nameItem = MethodTopCutGradeGridItem("Среднее")
		# 	self.setItem(nRows, 0, nameItem)
		# 	for iCol in range(1, self.columnCount()):
		# 		item = MethodTopCutGradeGridItem()
		# 		item.setTextAlignment(QtCore.Qt.AlignRight)
		# 		self.setItem(nRows, iCol, item)

		# self._methods.append(method)

		# self.setSummaryData()
		pass

	def setDefaultData(self, data):
		if not self._hasDefaultRow:
			return
		#
		nRows = self.rowCount()
		nameItem = SummaryTopCutGradeGridItem("По умолчанию")
		self.setItem(nRows-1, 0, nameItem)
		for iCol, value in enumerate(data, start = 1):
			item = SummaryTopCutGradeGridItem(value)
			item.setTextAlignment(QtCore.Qt.AlignRight)
			self.setItem(nRows-1, iCol, item)
	# def setSummaryData(self):
	# 	if len(methods) == 0:
	# 		return
	# 	nRows, nCols = self.rowCount(), self.columnCount()
	# 	for iCol in range(1, nCols):
	# 		avgValue, nValues = 0, 0
	# 		for iRow in range(nRows-2):
	# 			value = self.item(iRow, iCol).text()
	# 			if not value:
	# 				continue
	# 			avgValue += value
	# 			nValues += 1
	# 		avgValue /= nValues
	# 		self.item(icol, nRows-2).setText("%.3f" % avgValue)

	def setHeader(self, header):
		columnNames = ["Метод"] + list(header)
		nColumns = len(columnNames)
		#
		self.setRowCount(0)
		self.setColumnCount(0)
		self.setColumnCount(nColumns)
		#
		for iCol, columnName in enumerate(columnNames):
			headerItem = TopCutGradeGridItem(columnName)
			self.setHorizontalHeaderItem(iCol, headerItem)
			if iCol == 0:	self.setColumnWidth(0, 150)
			else:			self.setColumnWidth(iCol, 40)

	def hideMethods(self):
		nRows = self.rowCount() - int(self._hasDefaultRow) - int(self._hasUserRow)
		for iRow in range(nRows):
			self.hideRow(iRow)
	def showMethods(self):
		nRows = self.rowCount() - int(self._hasDefaultRow) - int(self._hasUserRow)
		for iRow in range(nRows):
			self.showRow(iRow)

	def insertDefaultRow(self):
		if self._hasDefaultRow:
			return
		#
		self.insertRow(self.rowCount())
		self._hasDefaultRow = True
	def defaultData(self):
		iRow = self.defaultRowIndex()
		if iRow is None:	return []
		else:				return [float(self.item(iRow, iCol).text()) if self.item(iRow, iCol).text() else None for iCol in range(1, self.columnCount())]
	def defaultRowIndex(self):
		return self.rowCount()-2 if self._hasDefaultRow else None

	def insertUserRow(self):
		if self._hasUserRow:
			return
		#
		iRow = self.rowCount() - int(not self._hasDefaultRow)
		self.insertRow(iRow)

		item = TopCutGradeGridItem("Решение")
		item.setReadOnly(True)
		self.setItem(iRow, 0, item)
		for iCol in range(1, self.columnCount()):
			item = TopCutGradeGridItem()
			item.setTextAlignment(QtCore.Qt.AlignRight)
			self.setItem(iRow, iCol, item)
		self._hasUserRow = True
	def userData(self):
		iRow = self.userRowIndex()
		if iRow is None:	return []
		else:				return [float(self.item(iRow, iCol).text()) if self.item(iRow, iCol).text() else None for iCol in range(1, self.columnCount())]
	def userRowIndex(self):
		return self.rowCount()-1 if self._hasUserRow else None
