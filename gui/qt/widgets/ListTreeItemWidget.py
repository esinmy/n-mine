from PyQt4 import QtGui, QtCore
import os
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class ListTreeItemWidget(QtGui.QListWidget):
	def __init__(self, parent):
		super(ListTreeItemWidget, self).__init__(parent)

	def setItems(self, items):
		self.clear()
		for item in items:
			self.addItem(item)