from datetime import datetime
from PyQt4 import QtGui, QtCore
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DateRangePicker(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DateRangePicker, self).__init__(*args, **kw)

		self.setWindowFlags(QtCore.Qt.Popup)
		self.setWindowModality(QtCore.Qt.WindowModal)
		self.setAutoFillBackground(True)

		palette = self.palette()
		palette.setColor(self.backgroundRole(), QtCore.Qt.white)
		self.setPalette(palette)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QGridLayout()

		self.labelFrom = QtGui.QLabel(self)
		self.labelFrom.setText("С")
		self.labelFrom.setAlignment(QtCore.Qt.AlignCenter)
		self.labelTo = QtGui.QLabel(self)
		self.labelTo.setText("По")
		self.labelTo.setAlignment(QtCore.Qt.AlignCenter)

		fmtBold = QtGui.QTextCharFormat()
		fmtBold.setFontWeight(QtGui.QFont.Bold)
		self.calendarFrom = QtGui.QCalendarWidget(self)
		self.calendarFrom.setVerticalHeaderFormat(QtGui.QCalendarWidget.NoVerticalHeader)
		self.calendarFrom.setFirstDayOfWeek(QtCore.Qt.Monday)
		self.calendarFrom.setSelectedDate(datetime.now())
		self.calendarFrom.setHeaderTextFormat(fmtBold)
		self.calendarFrom.setGridVisible(True)
		self.calendarFrom.setMinimumSize(QtCore.QSize(200, 200))
		self.calendarTo = QtGui.QCalendarWidget(self)
		self.calendarTo.setVerticalHeaderFormat(QtGui.QCalendarWidget.NoVerticalHeader)
		self.calendarTo.setFirstDayOfWeek(QtCore.Qt.Monday)
		self.calendarTo.setSelectedDate(datetime.now())
		self.calendarTo.setHeaderTextFormat(fmtBold)
		self.calendarTo.setGridVisible(True)
		self.calendarTo.setMinimumSize(QtCore.QSize(200, 200))

		self.buttonClose = QtGui.QPushButton(self)
		self.buttonClose.setMinimumWidth(100)
		self.buttonClose.setMaximumWidth(100)
		self.buttonClose.setText("Закрыть")
		self.buttonRestore = QtGui.QPushButton(self)
		self.buttonRestore.setMinimumWidth(100)
		self.buttonRestore.setMaximumWidth(100)
		self.buttonRestore.setText("Сбросить")
	def __createBindings(self):
		self.calendarFrom.selectionChanged.connect(self._calendarFromClicked)
		self.calendarTo.selectionChanged.connect(self._calendarToClicked)

		self.buttonRestore.clicked.connect(self._restore)
		self.buttonClose.clicked.connect(self.close)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.labelFrom, 0, 0, 1, 1, QtCore.Qt.AlignCenter)
		self.layoutMain.addWidget(self.labelTo, 0, 1, 1, 1, QtCore.Qt.AlignCenter)
		self.layoutMain.addWidget(self.calendarFrom, 1, 0, 1, 1)
		self.layoutMain.addWidget(self.calendarTo, 1, 1, 1, 1)
		self.layoutMain.addWidget(self.buttonClose, 2, 0, 1, 1, QtCore.Qt.AlignRight)
		self.layoutMain.addWidget(self.buttonRestore, 2, 1, 1, 1, QtCore.Qt.AlignLeft)

	def _calendarFromClicked(self):
		self.parent().setDateFrom(self.calendarFrom.selectedDate())
	def _calendarToClicked(self):
		self.parent().setDateTo(self.calendarTo.selectedDate())
	def _restore(self):
		mindate = self.parent().minimumDate()
		maxdate = self.parent().maximumDate()
		self.calendarFrom.setSelectedDate(mindate)
		self.calendarTo.setSelectedDate(maxdate)

	def setDateFrom(self, date):
		self.calendarFrom.setSelectedDate(date)
	def setDateTo(self, date):
		self.calendarTo.setSelectedDate(date)

class DateRangeWidget(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DateRangeWidget, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)

	def __createVariables(self):
		self._mindate = datetime.now()
		self._maxdate = datetime.now()
		self._picker = DateRangePicker(self)
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setMargin(0)

		self.dateEditFrom = QtGui.QDateEdit(self)
		self.dateEditFrom.setMinimumWidth(120)
		self.dateEditFrom.setDate(datetime(2013, 8, 8))
		
		self.buttonCalendar = QtGui.QPushButton(self)
		self.buttonCalendar.setMinimumSize(QtCore.QSize(25, 25))
		self.buttonCalendar.setMaximumSize(QtCore.QSize(25, 25))
		self.buttonCalendar.setIcon(QtGui.QIcon("gui/img/icons/daterange.png"))
		self.buttonCalendar.setIconSize(QtCore.QSize(15, 15))

		self.dateEditTo = QtGui.QDateEdit(self)
		self.dateEditTo.setMinimumWidth(120)
		self.dateEditTo.setDate(datetime.now())
	def __createBindings(self):
		self.buttonCalendar.clicked.connect(self.showCalendar)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.dateEditFrom)
		self.layoutMain.addWidget(self.buttonCalendar)
		self.layoutMain.addWidget(self.dateEditTo)

	def showCalendar(self):
		# инициализация
		self._picker.setDateFrom(self.dateEditFrom.date())
		self._picker.setDateTo(self.dateEditTo.date())
		
		# определяем расположение всплывающего окна
		position = self.getPopupCoordinates()
		self._picker.move(position)

		#
		self._picker.show()
		
	def getPopupCoordinates(self):
		# разрешение экрана
		screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
		resolution = QtGui.QApplication.desktop().screenGeometry(screen)
		srcW = resolution.width()
		scrH = resolution.height()

		#
		w = self.dateEditFrom
		wgtH = w.geometry().height()
		wgtW = w.geometry().width()
		wPos = self.mapToGlobal(w.pos())
		
		#
		pW = self._picker.size().width()
		pH = self._picker.size().height()

		# отсутп
		offset = 5
		x, y = 10, 10

		x_ = wPos.x() + wgtW + pW + offset
		y_ = wPos.y() + wgtH + pH + offset

		# если окно слева
		if x_ <= srcW:
			x = wPos.x()
			if y_ <= scrH:		y = wPos.y() + wgtH + offset	# если окно вверху
			if y_ > scrH:		y = wPos.y() - pH - offset		# если окно внизу
		else:
			x = wPos.x() - (pW - wgtW)
			if y_ <= scrH:		y = wPos.y() + wgtH + offset	# если окно вверху
			if y_ > scrH:		y = wPos.y() - pH - offset		# если окно внизу

		if x < 0:
			x = 0

		return QtCore.QPoint(x, y)

	def minimumDate(self):
		return self._mindate
	def setMinimumDate(self, date):
		self._mindate = date
		self.dateEditFrom.setMinimumDate(date)
		self.dateEditTo.setMaximumDate(date)
	def maximumDate(self):
		return self._maxdate
	def setMaximumDate(self, date):
		self._maxdate = date
		self.dateEditFrom.setMaximumDate(date)
		self.dateEditTo.setMaximumDate(date)
	def setDateRange(self, mindate, maxdate):
		self.setMinimumDate(mindate)
		self.setMaximumDate(maxdate)

	def dateFrom(self):				return self.dateEditFrom.date()
	def setDateFrom(self, date):	self.dateEditFrom.setDate(date)
	def dateTo(self):				return self.dateEditTo.date()
	def setDateTo(self, date):		self.dateEditTo.setDate(date)

