import os, pyodbc, pymssql
from PyQt4 import QtGui, QtCore
from datetime import datetime
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class LineEditFilter(QtGui.QLineEdit):
	def __init__(self, parent, column):
		super(LineEditFilter, self).__init__(parent)
		self._column = column
		self.setPlaceholderText("Фильтр")
		self.textChanged.connect(self.filterColumn)
	def keyPressEvent(self, event):
		key = event.key()
		if key in [QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter]:
			self.filterColumn()
		super(LineEditFilter, self).keyPressEvent(event)
	def filterColumn(self):
		text = self.text()
		model = self.parent().parent().model()
		model.setFilterString(text, self._column)
		model.applyFilter()
	def event(self, event):
		if event.type() == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Tab:
			self.parent().filterEdit(self._column+1)
			return -1		
		return QtGui.QLineEdit.event(self, event)

class ComboBoxFilter(QtGui.QComboBox):
	def __init__(self, parent, column):
		super(ComboBoxFilter, self).__init__(parent)
		self.setEditable(True)
		self.setInsertPolicy(QtGui.QComboBox.NoInsert)
		self.pFilterModel = QtGui.QSortFilterProxyModel(self)
		self.pFilterModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
		self.pFilterModel.setSourceModel(self.model())
		self.completer = QtGui.QCompleter(self.pFilterModel, self)
		self.completer.setCompletionMode(QtGui.QCompleter.UnfilteredPopupCompletion)
		self.setCompleter(self.completer)
		self.lineEdit().textEdited[str].connect(self.pFilterModel.setFilterFixedString)
		self.completer.activated.connect(self.on_completer_activated)
		self._column = column
		combobox_list = []
		for i in self.parent().parent().model()._data:
			combobox_list.append(str(i[self._column]))
		combobox_list = [""] + list(set(combobox_list))
		combobox_list.sort()
		self.addItems(combobox_list)
		self.currentIndexChanged.connect(self.filterColumn)

	def on_completer_activated(self, text):
		if text:
			index = self.findText(text)
			self.setCurrentIndex(index)

	def keyPressEvent(self, event):
		key = event.key()
		if key in [QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter]:
			self.filterColumn()
		else:
			super(ComboBoxFilter, self).keyPressEvent(event)

	def filterColumn(self):
		text = self.currentText()
		model = self.parent().parent().model()
		model.setFilterString(text, self._column)
		model.applyFilter()

	def event(self, event):
		if event.type() == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Tab:
			self.parent().filterEdit(self._column+1)
			return -1		
		return QtGui.QComboBox.event(self, event)

class FilterHeaderView(QtGui.QHeaderView):
	def __init__(self, parent):
		self._createVariables()
		super(FilterHeaderView, self).__init__(QtCore.Qt.Horizontal, parent)

		self.setClickable(True)
		self.setDefaultSectionSize(130)
		self.setDefaultAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
		self.setStretchLastSection(True)
		self.setResizeMode(QtGui.QHeaderView.Stretch)

		self._createBindings()


	def set_comboboxfilter_editable(self, arg=True):
		if self._filterWidgets:
			[i.setEditable(arg) for i in self._filterWidgets
			 if type(i).__bases__[0] is QtGui.QComboBox]

	# def filterWidget(self, index):
	# 	return self._filterWidgets[index]


	def edit_on(self):
		return

	def _createVariables(self):
		self._filterWidgets = []
		self._defaultSectionHeight = 25

	def _createBindings(self):
		scrollBar = self.parent().horizontalScrollBar()
		scrollBar.valueChanged.connect(self.updateFilterGeometry)
		self.sectionResized.connect(self.updateFilterGeometry)

	def _createFilter(self, combobox_filter=[]):
		for i in range(self.parent().model().columnCount()):
			if i in combobox_filter:
				edit = ComboBoxFilter(self, i)
			else:
				edit = LineEditFilter(self, i)
			edit.setVisible(True)
			self._filterWidgets.append(edit)

	def showFilter(self, combobox_filter=[]):
		if self._filterWidgets:
			return

		self._createFilter(combobox_filter)
		self.updateSections()
		self.updateFilterGeometry()

	def removeFilter(self):
		for edit in self._filterWidgets:
			edit.deleteLater()
		self._filterWidgets.clear()
		self.updateSections()

	def sizeHint(self):
		size = super(FilterHeaderView, self).sizeHint()
		if self.hasFilter():	size.setHeight(2*self._defaultSectionHeight)
		else:					size.setHeight(self._defaultSectionHeight)
		return size

	def hasFilter(self):
		return len(self._filterWidgets) != 0

	def updateSections(self):
		# needed to resize the section height (super hack)
		for i in range(self.parent().model().columnCount()):
			self.resizeSection(i, self.sectionSize(i)-1)
			self.resizeSection(i, self.sectionSize(i)+1)

	def updateGeometries(self):
		super(FilterHeaderView, self).updateGeometries()

		self.updateFilterGeometry()

	def updateFilterGeometry(self):
		for i, edit in enumerate(self._filterWidgets):
			x = self.sectionPosition(i) - self.offset()
			y = self.sizeHint().height() // 2
			w = self.sectionSize(i)
			h = self.sizeHint().height() // 2
			edit.setGeometry(x, y, w, h)
	def filterEdit(self, i):
		if i >= len(self._filterWidgets):
			i = 0
		self._filterWidgets[i].setFocus()
		if type(self._filterWidgets[i]) is not ComboBoxFilter:
			self._filterWidgets[i].selectAll()

class CustomDataModel(QtCore.QAbstractTableModel):
	def __init__(self, parent, data, header, *args):
		super(CustomDataModel, self).__init__(parent, *args)
		self._parent = parent
		self._header = header
		self._data = data
		self._original = self._data.copy()
		self._fstrings = {i: "" for i in range(self.columnCount())}
	def rowCount(self, parent = None):
		return len(self._data)
	def columnCount(self, parent = None):
		return len(self._data[0]) if self._data else 0
	def data(self, index, role = QtCore.Qt.DisplayRole):
		if not index.isValid() or role != QtCore.Qt.DisplayRole:
			return None
		ret = self._data[index.row()][index.column()]
		if isinstance(ret, bool):		return int(ret)
		elif isinstance(ret, float):		return '{:.3f}'.format(round(ret, 3))
		elif ret is None:				return ""
		else:							return ret
	def headerData(self, col, orientation, role):
		if role == QtCore.Qt.DisplayRole:
			if orientation == QtCore.Qt.Horizontal:		return self._header[col]
			if orientation == QtCore.Qt.Vertical:		return col
		return None
	def applyFilter(self):
		if set(self._fstrings.values()) == {""}:
			self._data = self._original.copy()
		else:
			self._data = []
			for row in range(len(self._original)):
				self._parent.hideRow(row)
				filtered = [self._fstrings[col] in str(self._original[row][col]).lower() for col in self._fstrings if self._fstrings[col]]
				if filtered and all(filtered):
					self._data.append(self._original[row])

		if self.rowCount() > 0:
			self.emit(QtCore.SIGNAL("dataChanged(QModelIndex, QModelIndex)"), self.createIndex(0, self.columnCount()), self.createIndex(self.rowCount(), self.columnCount()))
		else:
			self.emit(QtCore.SIGNAL("dataChanged(QModelIndex, QModelIndex)"), self.createIndex(0, self.columnCount()), self.createIndex(1, self.columnCount()))
		for i in range(self.rowCount()):
			self._parent.showRow(i)
	def setFilterString(self, string, column):
		self._fstrings[column] = string.lower()

class CustomDataView(QtGui.QTableView):
	def __init__(self, parent):
		super(CustomDataView, self).__init__(parent)
		self.verticalHeader().setVisible(False)
		#
		self.setSelectionBehavior(QtGui.QTableView.SelectRows)
		self.setSelectionMode(QtGui.QTableView.SingleSelection)
		self.setAlternatingRowColors(True)

	def setModel(self, model, combobox_filter=[]):
		super(CustomDataView, self).setModel(model)
		self.header = FilterHeaderView(self)
		self.setHorizontalHeader(self.header)
		self.horizontalHeader().showFilter(combobox_filter)
	def updateModel (self, model):
		super(CustomDataView, self).setModel(model)


###################################################################################################################
################################################### EXAMPLE #######################################################
###################################################################################################################

class Window(QtGui.QWidget):
	def __init__(self, parent = None):
		super(Window, self).__init__(parent)

		self._createVariables()
		self._createWidgets()
		self._gridWidgets()

	def _createVariables(self):
		tick = datetime.now()
		cnxnString = "DRIVER={SQL Server};Server=(local);Database=SURVEY_ORACLE"
		cnxn = pyodbc.connect(cnxnString)
		print ("connected", datetime.now() - tick)

		self._cursor = cnxn.cursor()

		self.pointsValues, self.pointsHeader = self.execQuery("select * from BASEPOINTS")
		self.measuresValues, self.measuresHeader = self.execQuery("select * from MEASURES")
		self._k = 0

	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.but1 = QtGui.QPushButton()
		self.but1.clicked.connect(self.showData)
	def showData(self):
		dialog = QtGui.QDialog(self)
		layout = QtGui.QVBoxLayout()
		view = CustomDataView(dialog)
		if self._k % 2:		model = CustomDataModel(view, self.pointsValues, self.pointsHeader)
		else:				model = CustomDataModel(view, self.measuresValues, self.measuresHeader)
		view.setModel(model)
		dialog.setLayout(layout)
		layout.addWidget(view)
		dialog.exec()
		
		self._k += 1

	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.but1)

	def execQuery(self, query):
		self._cursor.execute(query)
		header = [row[0] for row in self._cursor.description]
		values = self._cursor.fetchall()
		return values, header

	def closeEvent(self, event):
		self._cursor.connection.close()
		event.accept()

# app = QtGui.QApplication(os.sys.argv)
# w = Window()
# w.show()
# os.sys.exit(app.exec_())