from PyQt4 import QtGui, QtCore
from gui.qt.widgets.GridEditor import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class TableFilterWireframe(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(TableFilterWireframe, self).__init__(*args, **kw)


		self._createVariables()
		self._createWidgets()
		self._createBindings()
		self._gridWidgets()

	def _createVariables(self):
		self._attrNamesDict = {}
		self._attrValuesDict = {}
	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxFilter = QtGui.QGroupBox(self)
		self.groupBoxFilter.setTitle("Фильтр каркасов")

		self.layoutFilter = QtGui.QVBoxLayout()

		self.gridEditorFilter = GridEditor(self.groupBoxFilter, ["Категория", "Атрибут", "Значение"], [QtGui.QComboBox, QtGui.QComboBox, ComboBoxEditable])
		self.gridEditorFilter.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.gridEditorFilter.dataGrid.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		self.gridEditorFilter.dataGrid.verticalHeader().setHighlightSections(True)

		self.layoutFilterOptions = QtGui.QHBoxLayout()
		self.radioButtonGroup = QtGui.QButtonGroup(self.groupBoxFilter)
		self.radioButtonAnd = QtGui.QRadioButton(self.groupBoxFilter)
		self.radioButtonAnd.setText("И")
		self.radioButtonAnd.setMaximumSize(QtCore.QSize(30, 20))
		self.radioButtonOr = QtGui.QRadioButton(self.groupBoxFilter)
		# self.radioButtonOr.setMinimumSize(QtCore.QSize(800, 20))
		self.radioButtonOr.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred))
		self.radioButtonOr.setText("ИЛИ")
		self.radioButtonOr.setChecked(True)
		self.radioButtonGroup.addButton(self.radioButtonAnd)
		self.radioButtonGroup.addButton(self.radioButtonOr)
		# self.checkBoxGroupCategories = QtGui.QCheckBox(self)
		# self.checkBoxGroupCategories.setText("Создать один набор для категории")
		# self.checkBoxGroupCategories.setChecked(True)
		# self.checkBoxGroupCategories.setToolTip("Создать несколько наборов внутри каждой категории")
		# self.checkBoxGroupCategories.setMinimumSize(QtCore.QSize(200, 20))
	def _createBindings(self):
		self.gridEditorFilter.dataGrid.keyPressEvent = lambda e: self.keyPressEvent(e)
		self.gridEditorFilter.toolBar.pushButtonRefresh.setVisible(False)
		self.gridEditorFilter.toolBar.pushButtonInsert.setVisible(False)
		self.gridEditorFilter.toolBar.pushButtonMoveUp.setVisible(False)
		self.gridEditorFilter.toolBar.pushButtonMoveDown.setVisible(False)

		self.gridEditorFilter.toolBar.pushButtonAppend.clicked.disconnect()
		self.gridEditorFilter.toolBar.pushButtonAppend.clicked.connect(self.appendRow)
		self.gridEditorFilter.toolBar.pushButtonDelete.clicked.disconnect()
		self.gridEditorFilter.toolBar.pushButtonDelete.clicked.connect(self.removeSelectedRows)
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.setSpacing(5)
		self.layoutMain.setContentsMargins(0,0,0,0)

		self.layoutMain.addWidget(self.groupBoxFilter)
		self.groupBoxFilter.setLayout(self.layoutFilter)
		self.layoutFilter.setSpacing(5)

		self.layoutFilter.addWidget(self.gridEditorFilter)
		self.layoutFilter.addLayout(self.layoutFilterOptions)
		self.layoutFilterOptions.addWidget(self.radioButtonAnd)
		self.layoutFilterOptions.addWidget(self.radioButtonOr)
		# self.layoutFilterOptions.addWidget(self.checkBoxGroupCategories)

	# override these methods to set combobox values
	def keyPressEvent(self, event):
		key = event.key()

		# Delete and Backspace
		if key == QtCore.Qt.Key_Delete or key == QtCore.Qt.Key_Backspace:
			for index in self.gridEditorFilter.dataGrid.selectedIndexes():
				item = self.gridEditorFilter.dataGrid.itemFromIndex(index)
				if not item.isLocked() and not self.gridEditorFilter.dataGrid.readOnly():
					item.setText("")

		# Ctrl+Delete
		if event.modifiers() & QtCore.Qt.ControlModifier and key == QtCore.Qt.Key_Delete:
			self.gridEditorFilter.dataGrid.removeSelectedRows()

		# return
		if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
			self.gridEditorFilter.dataGrid.clearSelection()
			#
			currIndex = self.gridEditorFilter.dataGrid.currentIndex()
			nRows = self.gridEditorFilter.dataGrid.rowCount()
			# append row if the last row is a current row
			iRow, iCol = currIndex.row(), currIndex.column()
			if iRow == nRows-1 and self.gridEditorFilter.dataGrid.appendByKeyboard():
				self.appendRow()
			# select cell
			if currIndex.model():		self.gridEditorFilter.dataGrid.setCurrentCell(iRow+1, iCol)
			else:						self.gridEditorFilter.dataGrid.setCurrentCell(0,0)
	def appendRow(self):
		iRow = self.gridEditorFilter.dataGrid.rowCount()
		#
		comboCat = QtGui.QComboBox()
		comboCat.row = iRow
		comboCat.column = 0
		for category in sorted(self._attrNamesDict):
			comboCat.addItem(category)
		comboCat.currentIndexChanged.connect(lambda: self.categoryChanged(comboCat))
		#
		comboAttr = QtGui.QComboBox()
		comboAttr.currentIndexChanged.connect(lambda: self.attributeChanged(comboAttr))
		comboAttr.row = iRow
		comboAttr.column = 1
		#
		comboValues = QtGui.QComboBox()
		comboValues.setEditable(True)
		comboValues.row = iRow
		comboValues.column = 2
		#
		self.gridEditorFilter.dataGrid.insertRow(iRow)
		self.gridEditorFilter.dataGrid.setCellWidget(iRow, 0, comboCat)
		self.gridEditorFilter.dataGrid.setCellWidget(iRow, 1, comboAttr)
		self.gridEditorFilter.dataGrid.setCellWidget(iRow, 2, comboValues)
		#
		self.categoryChanged(comboCat)
	def removeSelectedRows(self):
		selectedIndexes = self.gridEditorFilter.dataGrid.selectedIndexes()
		iRows = self.gridEditorFilter.dataGrid.getRowsFromIndexes(selectedIndexes)
		diff = 0
		for iRow in range(self.gridEditorFilter.dataGrid.rowCount()):
			if iRow in iRows:
				diff += 1
			for iCol in range(self.gridEditorFilter.dataGrid.columnCount()):
				self.gridEditorFilter.dataGrid.cellWidget(iRow, iCol).row -= diff

		for iRow in reversed(iRows):
			self.gridEditorFilter.dataGrid.removeRow(iRow)

	#
	def categoryChanged(self, combo):
		iRow = combo.row
		cat = combo.currentText()
		self.gridEditorFilter.dataGrid.cellWidget(iRow, 1).clear()
		for attrName in self._attrNamesDict[cat]:
			self.gridEditorFilter.dataGrid.cellWidget(iRow, 1).addItem(attrName)
	def attributeChanged(self, combo):
		iRow = combo.row
		self.gridEditorFilter.dataGrid.cellWidget(iRow, 2).clear()
		attrName = self.gridEditorFilter.dataGrid.cellWidget(iRow, 1).currentText()
		if attrName and attrName in self._attrValuesDict:
			for attrValue in self._attrValuesDict[attrName]:
				self.gridEditorFilter.dataGrid.cellWidget(iRow, 2).addItem(attrValue)
			self.gridEditorFilter.dataGrid.cellWidget(iRow, 2).clearEditText()

	def clear(self):
		self.gridEditorFilter.dataGrid.clear()
		self.clearCache()

	def clearCache(self):
		self._attrNamesDict = {}
		self._attrValuesDict = {}
		#

	def setDictionaries(self, attrNamesDict, attrValuesDict):
		self._attrNamesDict = attrNamesDict
		self._attrValuesDict = attrValuesDict

