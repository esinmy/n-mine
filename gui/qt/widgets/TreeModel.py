try:		import MMpy
except:		pass

from PyQt4 import QtGui, QtCore

import os, sqlite3
from utils.osutil import *
from utils.qtutil import *
from utils.constants import *
from utils.fsettings import FormSet
from utils.blockmodel import BlockModel

from mm.mmutils import MicromineFile

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.TreeModelItem import *
from gui.qt.commands.dsn.DsnRenameUnit import DsnRenameUnit
from gui.qt.commands.dsn.DsnDeleteUnit import DsnDeleteUnit
from gui.qt.commands.dsn.DsnRenameTriDb import DsnRenameTriDb
from gui.qt.commands.dsn.DsnDeleteTriDb import DsnDeleteTriDb
from gui.qt.commands.svy.SvyDeleteTriDb import SvyDeleteTriDb


from gui.qt.commands.RenameTriDb import RenameTriDb
from gui.qt.commands.DeleteTriDb import DeleteTriDb
from gui.qt.commands.RenameDatItem import RenameDatItem
from gui.qt.commands.DeleteDatItem import DeleteDatItem
from gui.qt.commands.RenameStrItem import RenameStrItem
from gui.qt.commands.DeleteStrItem import DeleteStrItem
from gui.qt.commands.RenameAnnotItem import RenameAnnotItem
from gui.qt.commands.DeleteAnnotItem import DeleteAnnotItem

from utils.logger import  NLogger

# import importlib
# importlib.reload(unarchiveSvy)

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class CheckBoxItem(QtGui.QWidget):
	def __init__(self, parent, item, column):
		super(CheckBoxItem, self).__init__(parent)

		self._item = item
		self._item.treeWidget().setItemWidget(self._item, column, self)
		self._item.setTextAlignment(column, QtCore.Qt.AlignCenter)
		self._column = column

		self._checkbox = QtGui.QCheckBox(self)
		self._checkbox.stateChanged.connect(self._itemChange)

		layout = QtGui.QHBoxLayout()
		layout.addWidget(self._checkbox)
		layout.setAlignment(QtCore.Qt.AlignCenter)
		layout.setContentsMargins(0,0,0,0)
		layout.setSpacing(0)
		layout.setMargin(0)

		self.setLayout(layout)
	@property
	def checkBox(self):
		return self._checkbox
	def getValue(self):
		return getWidgetValue(self._checkbox)
	def setValue(self, val):
		setWidgetValue(self._checkbox, val)

	def setChecked(self, flag):
		self._checkbox.setChecked(flag)
		self._itemChange(flag)
	def isChecked(self):
		return self._checkbox.isChecked()

	def _itemChange(self, state):
		self._item.setText(self._column, str(int(self.isChecked())))

class TreeWidget(QtGui.QTreeWidget):
	def __init__(self, *args, **kw):
		super(TreeWidget, self).__init__(*args, **kw)
		self.setAlternatingRowColors(True)

		self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.customContextMenuRequested.connect(self._openMenu)

		self._droppedItems = []

	def iterChildren(self, parent = None):
		if parent is None:
			parent = self.invisibleRootItem()
			ret = []
			for i in range(parent.childCount()):
				child = parent.child(i)
				ret.append(child)
				ret.extend(child.iterChildren())
		else:
			ret = parent.iterChildren()
		return ret

	def children(self):
		root = self.invisibleRootItem()
		return [root.child(i) for i in range(root.childCount())]

	def _openMenu(self, position):
		selectedItems = self.selectedItems()
		if not selectedItems:
			return

		main_app = iterWidgetParents(self)[-1]
		userRole = main_app.userRole()
		#
		menu = None

		if all(map(lambda it: it.isWireframe(), selectedItems)):
			menu = self._createWireframeContextMenu(selectedItems, userRole)
		if all(map(lambda it: it.isModel(), selectedItems)):
			menu = self._createModelContextMenu(selectedItems, userRole)
		if all(map(lambda it: it.isTridb(), selectedItems)):
			menu = self._createTriDbContextMenu(selectedItems, userRole)
		if all(map(lambda it: it.isDirectory(), selectedItems)):
			menu = self._create_dir_cntx_menu(selectedItems, userRole)
		if all(map(lambda it: it.isString(), selectedItems)):
			menu = self._create_str_cntx_menu(selectedItems, userRole)
		if all(map(lambda it: it.isDat(), selectedItems)):
			menu = self._create_dat_cntx_menu(selectedItems, userRole)
		if all(map(lambda it: it.isAnnotation(), selectedItems)):
			menu = self._create_annot_cntx_menu(selectedItems, userRole)

		if menu is None:
			return

		menu.exec_(self.viewport().mapToGlobal(position))

	def _createWireframeContextMenu(self, wireframeItems, userRole):
		if not all(map(lambda it: it.isWireframe(), wireframeItems)):
			return
		#
		main_app = iterWidgetParents(self)[-1]
		isAdmin = userRole == ID_USERADMIN
		#
		itcat = wireframeItems[0].itemCategory()
		isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
		if not isOwner:
			return
		#
		menu = QtGui.QMenu()
		menu.addAction("Удалить", lambda: self.removeSelectedItems())
		#
		# allSelectedFromUnitDir = all(map(lambda it: os.path.dirname(os.path.dirname(os.path.dirname(it.filePath()))) == it.root(), wireframeItems))
		# if not allSelectedFromUnitDir:
		# 	return menu
		#
		if itcat == ITEM_CATEGORY_GEO:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]
		if itcat == ITEM_CATEGORY_DSN:      dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]
		#
		###### start edit
		# allInWireframeDir = all(map(lambda it: os.path.basename(os.path.dirname(it.filePath())) == dirWf, wireframeItems))
		# allInArchiveDir = all(map(lambda it: os.path.basename(os.path.dirname(it.filePath())) == DIR_ARCHIVE, wireframeItems))
		# def allInWireframeDir():


		allInWireframeDir = all(
			map(lambda it: dirWf in os.path.dirname(it.filePath()), wireframeItems))
		allInArchiveDir = all(
			map(lambda it: DIR_ARCHIVE.upper() in os.path.dirname(it.filePath()).upper(), wireframeItems))

		###### end edit
		# #
		if allInWireframeDir:
			menu.addAction("Переместить в архив", lambda: self.archiveSelectedItems())
		if allInArchiveDir:
			menu.addAction("Восстановить из архива", lambda: self.unarchiveSelectedItems())
		#
		return menu

	def _createModelContextMenu(self, modelItems, userRole):
		if len(modelItems) != 1:
			return
		#
		modelItem = modelItems[0]
		isAdmin = userRole == ID_USERADMIN
		#
		menu = QtGui.QMenu()
		itcat = modelItem.itemCategory()
		isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
		if isOwner:
			menu.addAction("Переименовать", lambda: modelItem.rename())
			menu.addSeparator()
			menu.addAction("Удалить", lambda: modelItem.delete())

		#
		isSvyUser = userRole == ID_USERSVY
		isAllowed = isSvyUser and itcat == ITEM_ROLE_CATEGORY_DICT[ID_USERGEO]
		if isAllowed:
			action = QtGui.QAction("Запросить перевод БМ", menu)
			action.triggered.connect(lambda: modelItem.set_need_transfer(True))
			action.setEnabled(not modelItem.is_need_transfer())
			menu.addAction(action)
			action = QtGui.QAction("Отменить запрос перевода БМ", menu)
			action.triggered.connect(lambda: modelItem.set_need_transfer(False))
			action.setEnabled(modelItem.is_need_transfer())
			menu.addAction(action)

		if len(menu.actions()) == 0:
			return
		return menu

##############################################################################################
	def _create_dir_cntx_menu(self, selected_items, userRole):
		if len(selected_items) != 1:
			return

		isAdmin = userRole == ID_USERADMIN
		item = selected_items[0]
		itcat = item.itemCategory()
		isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
		if not isOwner:
			return

		main_app = iterWidgetParents(item.treeWidget())[-1]
		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		item.treeWidget().parent().populate(item, needsFilter=False)
		dir_children_names = []
		for i in item.iterChildren():
			dir_children_names.append(i.name())
		if dirWf not in dir_children_names or itcat != ITEM_CATEGORY_DSN:
			return
		menu = QtGui.QMenu()
		if DIR_ARCHIVE not in item.getPath():
			menu.addAction("Переименовать", lambda: self.renameDsnUnit(item))
			menu.addAction("Переместить в архив", lambda: self.archive_dsn_loc_proj_prepare())
			menu.addSeparator()
			menu.addAction("Удалить", lambda: self.deleteDsnUnit(item))
		else:
			menu.addAction("Восстановить из архива", lambda: self.unarchive_dsn_loc_proj_prepare())

		return menu

########################################################################################################
	def archive_dsn_loc_proj_prepare(self, silent=False):
		item = self.selectedItems()[0]
		main_app = iterWidgetParents(item.treeWidget())[-1]
		if not silent:
			if item.archive_dsn_loc_proj(silent=False):
				qtmsgbox.show_information(main_app, "Выполнено", "Локальный проект перенесен в архив")
			else:
				qtmsgbox.show_information(main_app, "Внимание", "Локальный проект не был перенесен в архив")


	def unarchive_dsn_loc_proj_prepare(self, silent=False):
		item = self.selectedItems()[0]
		main_app = iterWidgetParents(item.treeWidget())[-1]
		if not silent:
			if item.unarchive_dsn_loc_proj(silent=False):
				qtmsgbox.show_information(main_app, "Выполнено", "Локальный проект восстановлен из архива")
			else:
				qtmsgbox.show_information(main_app, "Внимание", "Локальный проект не был восстановлен из архива")

#######################################################################################################

#### start edit
	def _create_str_cntx_menu(self, items, userRole):
		if not all(map(lambda it: it.isString(), items)):
			return

		main_app = iterWidgetParents(self)[-1]
		isAdmin = userRole == ID_USERADMIN
		#
		itcat = items[0].itemCategory()
		isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
		if not isOwner:
			return
		#
		menu = QtGui.QMenu()

		dirWf = '__Избранное__'
		if itcat == ITEM_CATEGORY_GEO:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]
		if itcat == ITEM_CATEGORY_DSN:      dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		str_item = items[0]

		menu.addAction("Переименовать", lambda: self.StrItemrename(str_item))
		menu.addSeparator()
		menu.addAction("Удалить", lambda: self.StrItemdelete(str_item))

		return menu


	def _create_dat_cntx_menu(self, items, userRole):
		if not all(map(lambda it: it.isDat(), items)):
			return

		main_app = iterWidgetParents(self)[-1]
		isAdmin = userRole == ID_USERADMIN
		#
		itcat = items[0].itemCategory()
		isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
		if not isOwner:
			return
		#
		menu = QtGui.QMenu()

		dirWf = '__Избранное__'
		if itcat == ITEM_CATEGORY_GEO:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]
		if itcat == ITEM_CATEGORY_DSN:      dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		dat_item = items[0]

		menu.addAction("Переименовать", lambda: self.DatItemrename(dat_item))
		menu.addSeparator()
		menu.addAction("Удалить", lambda: self.DatItemdelete(dat_item))

		return menu

	def _create_annot_cntx_menu(self, items, userRole):
		if not all(map(lambda it: it.isAnnotation(), items)):
			return

		main_app = iterWidgetParents(self)[-1]
		isAdmin = userRole == ID_USERADMIN
		#
		itcat = items[0].itemCategory()
		isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
		if not isOwner:
			return
		#
		menu = QtGui.QMenu()

		dirWf = '__Избранное__'
		if itcat == ITEM_CATEGORY_GEO:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]
		if itcat == ITEM_CATEGORY_DSN:      dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		annot_item = items[0]

		menu.addAction("Переименовать", lambda: self.AnnotItemrename(annot_item))
		menu.addSeparator()
		menu.addAction("Удалить", lambda: self.AnnotItemdelete(annot_item))

		return menu


	def _createTriDbContextMenu(self, wireframeItems, userRole):
		if not all(map(lambda it: it.isTridb(), wireframeItems)):
			return
		#
		main_app = iterWidgetParents(self)[-1]
		isAdmin = userRole == ID_USERADMIN
		#
		itcat = wireframeItems[0].itemCategory()
		isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]	or itcat == ITEM_CATEGORY_USER
		if not isOwner:
			return
		#
		menu = QtGui.QMenu()

		dirWf = '__Избранное__'
		if itcat == ITEM_CATEGORY_GEO:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]
		if itcat == ITEM_CATEGORY_SVY:		dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_SVY_USUBDIRS)))[0]
		if itcat == ITEM_CATEGORY_DSN:      dirWf = tuple(map(lambda x: x[1], main_app.settings.getValue(ID_DSN_USUBDIRS)))[1]

		allInWireframeDir = all(
			map(lambda it: dirWf in os.path.dirname(it.filePath()), wireframeItems))
		allInArchiveDir = all(
			map(lambda it: DIR_ARCHIVE.upper() in os.path.dirname(it.filePath()).upper(), wireframeItems))

		if allInWireframeDir:
			menu.addAction("Переместить в архив", lambda: self.archiveSelectedItems())
		if allInArchiveDir:
			menu.addAction("Восстановить из архива", lambda: self.unarchiveSelectedItems())
		#

		if len(wireframeItems) != 1:
			return
		if all(map(lambda it: it.isTriDbDsnUnit(), wireframeItems)):
			#
			triDbDsnUnitItem = wireframeItems[0]
			isAdmin = userRole == ID_USERADMIN
			itcat = triDbDsnUnitItem.itemCategory()
			isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
			if not isOwner:
				return
			#
			menu.addAction("Переименовать", lambda: self.renameTriDbDsnUnit(triDbDsnUnitItem))
			menu.addSeparator()
			menu.addAction("Удалить", lambda: self.deleteTriDbDsnUnit(triDbDsnUnitItem))
			#
		elif all(map(lambda it: it.isTriDbSvyUnit(), wireframeItems)):
			#
			triDbSvyUnitItem = wireframeItems[0]
			isAdmin = userRole == ID_USERADMIN
			itcat = triDbSvyUnitItem.itemCategory()
			isOwner = isAdmin or itcat == ITEM_ROLE_CATEGORY_DICT[userRole]
			if not isOwner:
				return
			#
			# 	Переименовать не возможно так как, в некоторых случаях переименование это создание новой выработки с привязками, а недостаточно данных для этого
			#menu.addAction("Переименовать", lambda: self.renameTriDbSvyUnit(triDbSvyUnitItem))	
			menu.addSeparator()
			menu.addAction("Удалить", lambda: self.deleteTriDbSvyUnit(triDbSvyUnitItem))
			#
		else:
			triDbItem = wireframeItems[0]
			menu.addAction("Переименовать", lambda: self.triDBrename(triDbItem))
			menu.addSeparator()
			menu.addAction("Удалить", lambda: self.triDBdelete(triDbItem))

		return menu


	def removeWireframeItem(self, item, silent = False):
		main_app = iterWidgetParents(self)[-1]
		if not silent and not qtmsgbox.show_question(main_app, "Внимание", "Удалить каркас '%s'" % item.name()):
			return
		item.remove()
	def removeSelectedItems(self, silent = False):
		selectedItems = self.selectedItems()
		if not selectedItems:
			return
		#
		singleSelected = len(selectedItems) == 1
		main_app = iterWidgetParents(self)[-1]
		if not singleSelected:
			if not silent:
				if not qtmsgbox.show_question(main_app, "Внимание", "Удалить выбранные элементы?"):
					return
				else:
					silent = True
		#
		for selectedItem in selectedItems:
			if selectedItem.isWireframe():
				self.removeWireframeItem(selectedItem, silent)

	def archiveSelectedItems(self, silent = False):
		main_app = iterWidgetParents(self)[-1]
		#
		result = []
		selectedItems = self.selectedItems()
		for selectedItem in selectedItems:
			if selectedItem.isWireframe():
				result.append(self.archiveWireframeItem(selectedItem, silent))
############ start edit
			if selectedItem.isTridb():
				result.append(self.archiveTriDBItem(selectedItem, silent))
############ end edit
#######################################################################################3
			if selectedItem.isDirectory():
				result.append(self.archiveWireframeItem(selectedItem, silent))
###########################################################################################
		if not silent:
			if all(result):
				qtmsgbox.show_information(main_app, "Выполнено", "Каркасы успешно помещены в архив")
			else:
				skipped = [selectedItem.name() for i, selectedItem in enumerate(selectedItems) if not result[i]]
				qtmsgbox.show_information(main_app, "Выполнено", "Следующие каркасы не удалось переместить в архив: %s" % "; ".join(skipped))

	def archiveWireframeItem(self, item, silent = False):
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_SVY:
			return item.archiveSvy(silent)
		if itcat == ITEM_CATEGORY_GEO:		return item.archiveGeo(silent)
		if itcat == ITEM_CATEGORY_DSN:      return item.archiveDsn(silent)

#### start edit
	def archiveTriDBItem(self, item, silent = False):
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_SVY:      return item.archiveSvyTriDB(silent)
		if itcat == ITEM_CATEGORY_DSN:      return item.archiveDsnTriDB(silent)
#### end edit
	#
	def unarchiveSelectedItems(self, silent = False):
		main_app = iterWidgetParents(self)[-1]
		#
		result = []
		selectedItems = self.selectedItems()
		for selectedItem in selectedItems:
			if selectedItem.isWireframe():
				result.append(self.unarchiveWireframeItem(selectedItem, silent))
			############ start edit
			if selectedItem.isTridb():
				result.append(self.unarchiveTriDBItem(selectedItem, silent))
			############ end edit
		#
		if not silent:
			if all(result):
				qtmsgbox.show_information(main_app, "Выполнено", "Каркасы успешно восстановлены из архива")
			else:
				skipped = [selectedItem.name() for i, selectedItem in enumerate(selectedItems) if not result[i]]
				qtmsgbox.show_information(main_app, "Выполнено", "Следующие каркасы не удалось восстановить из архива: %s" % "; ".join(skipped))
	def unarchiveWireframeItem(self, item, silent = False):
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_GEO:		return item.unarchiveGeo(silent)
		if itcat == ITEM_CATEGORY_SVY:		return item.unarchiveSvy(silent)
		if itcat == ITEM_CATEGORY_DSN:      return item.unarchiveDsn(silent)

	#### start edit
	def unarchiveTriDBItem(self, item, silent=False):
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_SVY:        return item.unarchiveSvyTriDB(silent)
		if itcat == ITEM_CATEGORY_DSN:      return item.unarchiveDsnTriDB(silent)

	#### end edit
 # DsnUnitEdition.Galkin.Insert.Begin
	def renameDsnUnit(self, dsnUnitItem):
		main_app = iterWidgetParents(dsnUnitItem.treeWidget())[-1]
		#
		command = DsnRenameUnit(main_app, dsnUnitItem, use_connection = True)
		#
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def deleteDsnUnit(self, dsnUnitItem):
		main_app = iterWidgetParents(dsnUnitItem.treeWidget())[-1]
		#
		command = DsnDeleteUnit(main_app, dsnUnitItem, use_connection = True)
		#
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def renameTriDbDsnUnit(self, triDbDsnUnitItem):
		main_app = iterWidgetParents(triDbDsnUnitItem.treeWidget())[-1]
		#
		command = DsnRenameTriDb(main_app, triDbDsnUnitItem, use_connection = True)
		#
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def deleteTriDbDsnUnit(self, triDbDsnUnitItem):
		main_app = iterWidgetParents(triDbDsnUnitItem.treeWidget())[-1]
		#
		command = DsnDeleteTriDb(main_app, triDbDsnUnitItem, use_connection = True)
		#
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def deleteTriDbSvyUnit(self, triDbSvyUnitItem):
		main_app = iterWidgetParents(triDbSvyUnitItem.treeWidget())[-1]
		#
		command = SvyDeleteTriDb(main_app, triDbSvyUnitItem, use_connection = True)
		#
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return


	def triDBrename(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = RenameTriDb(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def triDBdelete(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = DeleteTriDb(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return


	def StrItemrename(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = RenameStrItem(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def StrItemdelete(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = DeleteStrItem(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def DatItemrename(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = RenameDatItem(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def DatItemdelete(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = DeleteDatItem(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def AnnotItemrename(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = RenameAnnotItem(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return

	def AnnotItemdelete(self, item):
		main_app = iterWidgetParents(item.treeWidget())[-1]
		itcat = item.itemCategory()
		if itcat == ITEM_CATEGORY_DSN:
			return
		command = DeleteAnnotItem(main_app, item)
		try:
			command.exec()
		except Exception as e:
			qtmsgbox.show_error(main_app, "Ошибка", str(e))
			return




	 # DsnUnitEdition.Galkin.Insert.End

class TreeModelWidget(TreeWidget):
	def showAll(self):
		for item in self.iterChildren():
			item.setHidden(False)
	def hideAll(self):
		for item in self.iterChildren():
			item.setHidden(True)
	# drop
	def dropItems(self, items):
		droppedItems = []
		for item in items:
			droppedItems.append(self.dropItem(item))
			# добавление вложенных элементов
			if not item.isTridb():
				for child in item.iterChildren():
					if child.isHidden() or (child.isDirectory() and child.childCount() == 0):
						continue
					droppedItems.append(self.dropItem(child))
		return droppedItems
	def dropItem(self, selectedItem):
		item = None
		# создаем директорию категории, если не существует
		catPath = selectedItem.itemCategoryName() + ITEM_DIR
		if not self.itemPathExists(catPath):
			catItem = createTreeModelItem(self, catPath[:catPath.rfind(ITEM_DIR)], ITEM_DIR, selectedItem.itemCategory())
			catItem.setRoot(selectedItem.root())
			font = QtGui.QFont()
			font.setBold(True)
			catItem.setFont(0, font)
		else:
			catItem = self.getItemByPath(catPath)

		# добавление элемента, если не существует
		path = PATH_SPLITTER.join([selectedItem.itemCategoryName() + ITEM_DIR, selectedItem.getPath()])
		item = self.addItem(selectedItem) if not self.itemPathExists(path) else self.getItemByPath(path)
		return item
	def dropEvent(self, event):
		sourceModel = event.source()
		self.dropItems(sourceModel.selectedItems())
		self.expandAll()

	def getItemByPath(self, path):
		levels = list(map(lambda x: x.upper(), path.split(PATH_SPLITTER)))
		ret = None

		currItem = self.invisibleRootItem()
		currItemChildren = list(currItem.child(i) for i in range(currItem.childCount()))
		currItemChildrenNames = list(it.text(0).upper() + it.itemType() for it in currItemChildren)
		for level in levels:
			try:
				currItem = currItemChildren[currItemChildrenNames.index(level)]
				currItemChildren = list(currItem.child(i) for i in range(currItem.childCount()))
				currItemChildrenNames = list(it.text(0).upper() + it.itemType() for it in currItem.children())
				ret = currItem
			except:
				ret = None
				break
		return ret
	def itemPathExists(self, path):
		return self.getItemByPath(path) is not None
	def addItem(self, item):
		path = PATH_SPLITTER.join([item.itemCategoryName() + ITEM_DIR, item.getPath()])

		levels = path.split(PATH_SPLITTER)
		parents = list(reversed(item.iterParents())) + [item]

		try:
			currItem = self
			for i, (level, node) in enumerate(zip(levels, parents)):
				currPath = PATH_SPLITTER.join(levels[:i+1])
				if not self.itemPathExists(currPath):
					currItem = createTreeModelItem(currItem, level[:level.rfind(node.itemType())], node.itemType(), item.itemCategory())
					currItem.setFilePath(node.filePath())
					currItem.setRoot(item.root())
				else:
					currItem = self.getItemByPath(currPath)
		except Exception as e:
			print (str(e))
		return currItem


class TreeModel(QtGui.QWidget):
	def __init__(self, parent):
		super(TreeModel, self).__init__(parent)

		# self._parent = parent
		self._app = self.parent().parent().parent().parent().parent()

		self._createWidgets()
		self._gridWidgets()
		self._createBindings()

	def readSettings(self):
		userRoles = [ID_USERGEO, ID_USERSVY, ID_USERDSN, ID_USERADMIN]
		self._userRole = userRoles[list(self._app.settings.getValue(role) for role in userRoles).index(1)]

	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.layoutHeader = QtGui.QGridLayout()

		self.layoutLayer = QtGui.QFormLayout()
		self.labelLayer = QtGui.QLabel(self)
		self.labelLayer.setMinimumSize(QtCore.QSize(50, 0))
		self.labelLayer.setText("Залежь")
		self.labelLayer.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxLayer = QtGui.QComboBox(self)

		self.layoutRoot = QtGui.QFormLayout()
		self.labelRoot = QtGui.QLabel(self)
		self.labelRoot.setMinimumSize(QtCore.QSize(50, 0))
		self.labelRoot.setText("Папка")
		self.labelRoot.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditRoot = QtGui.QLineEdit(self)
		self.lineEditRoot.setReadOnly(True)

		self.pushButtonRefresh = QtGui.QPushButton(self)
		self.pushButtonRefresh.setMinimumSize(QtCore.QSize(20, 20))
		self.pushButtonRefresh.setMaximumSize(QtCore.QSize(20, 20))
		self.pushButtonRefresh.setIcon(QtGui.QIcon("/".join([IMG_PATH, "icons/refresh.png"])))
		self.pushButtonRefresh.setIconSize(QtCore.QSize(12, 12))
		self.pushButtonRefresh.setToolTip("Обновить")

		self.treeModel = TreeModelWidget(self)
		self.treeModel.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.treeModel.setDragDropMode(QtGui.QAbstractItemView.DragOnly)
		self.treeModel.headerItem().setText(0, "Имя")
	def _gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addLayout(self.layoutHeader)
		self.layoutHeader.addLayout(self.layoutLayer, 0, 0, 1, 1)
		self.layoutLayer.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelLayer)
		self.layoutLayer.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxLayer)

		self.layoutHeader.addLayout(self.layoutRoot, 0, 0, 1, 1)
		self.layoutRoot.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelRoot)
		self.layoutRoot.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditRoot)

		self.layoutHeader.addWidget(self.pushButtonRefresh, 0, 1, 1, 1)

		self.layoutMain.addWidget(self.treeModel)

	def _createBindings(self):
		self.treeModel.startDrag = lambda e: self.startDrag(e)
		self.treeModel.itemDoubleClicked.connect(self.treeModelItemDoubleClick)
		#
		self.comboBoxLayer.currentIndexChanged.connect(self.update_root)
		# self.comboBoxLayer.currentIndexChanged.connect(self._app.applyModelView)
		self.lineEditRoot.textChanged.connect(self.update_root)
		#
		self.pushButtonRefresh.clicked.connect(self._app.refreshStandardModels)
		self.pushButtonRefresh.clicked.connect(self._app.refreshUserModels)
		#
		QtGui.QShortcut(QtGui.QKeySequence("Ctrl+E"), self, None, self._openExplorer)

	def _openExplorer(self):
		selectedItems = self.treeModel.selectedItems()
		if not selectedItems:
			return
		path = selectedItems[0].filePath()
		if os.path.exists(path):
			if os.path.isfile(path):
				path = os.path.dirname(path)
			os.startfile(path)

	def startDrag(self, event):
		isAdmin = self._userRole == ID_USERADMIN
		for item in self.treeModel.selectedItems():
			allowModifyFlag = isAdmin or item.itemCategory() == ITEM_ROLE_CATEGORY_DICT[self._userRole] or item.itemCategory() == ITEM_CATEGORY_USER
			modifyModeFlag = True if allowModifyFlag else False
			if hasattr(item, "setAllowModify"):
				item.setAllowModify(allowModifyFlag)
				item.setModifyMode(modifyModeFlag)
			for child in item.iterChildren():
				if hasattr(child, "setAllowModify"):
					child.setAllowModify(allowModifyFlag)
					child.setModifyMode(modifyModeFlag)
			if item.itemType() == ITEM_WIREFRAME:
				if hasattr(item.parent(), "setAllowModify"):
					item.parent().setAllowModify(allowModifyFlag)
					item.parent().setModifyMode(modifyModeFlag)
		#
		TreeModelWidget.startDrag(self.treeModel, event)

	#
	def expandTreeModelItem(self, item):
		if item.isTridb():
			try:						item.readWireframeNames()
			except Exception as e:		qtmsgbox.show_error(self, "Ошибка", "Невозможно считать каркасы. %s" % str(e))
		if item.isSectionPack():
			try:						item.readSectionNames()
			except Exception as e:		qtmsgbox.show_error(self, "Ошибка", "Невозможно считать разрезы. %s" % str(e))
		if item.isDirectory() or item.isModel():
			self.populate(item)
	#
	def treeModelItemDoubleClick(self, item):
		if not item.isExpanded():
			self.expandTreeModelItem(item)
	#
	def removeItemChildren(self, item):
		item = self.treeModel.invisibleRootItem() if item is None else item
		for i in range(item.childCount()):
			item.removeChild(item.child(0))
	def populate(self, item = None, needsFilter = True):
		# считываем настройки
		self.readSettings()
		# очищаем элемент дерева
		self.removeItemChildren(item)
		#
		if item is None or item.isDirectory():
			self.populateDirectoryItem(item)
		elif item.isModel():
			self.populateModelItem(item)
		#
		if needsFilter:
			self._app.mainWidget.modelExplorer.filterItems()

	def populateDirectoryItem(self, item):
		return
	def populateModelItem(self, item):
		return
	def populateRecursively(self, parentItem = None, needsFilter = False):
		self.populate(parentItem, needsFilter)
		parentItem = self.treeModel if parentItem is None else parentItem
		for i, child in enumerate(filter(lambda it: it.isDirectory() or it.isModel(), parentItem.iterChildren())):
			self.populateRecursively(child, needsFilter)

	def update_root(self, root = None):
		index = None
		if type(self) == GeoTreeModel:		index = 0
		if type(self) == SvyTreeModel:		index = 1
		if type(self) == DsnTreeModel:		index = 2
		if type(self) == SecTreeModel:		index = 3
		if type(self) == DirTreeModel:		index = 4

		if index is None:
			return

		if index < 3:
			self.comboBoxLayer.blockSignals(True)
			self.lineEditRoot.blockSignals(True)
			self.pushButtonRefresh.blockSignals(True)

			currlayer = self.comboBoxLayer.currentText()
			self.comboBoxLayer.clear()
			#
			idMainDir, idHideDirs = [[ID_GEO_MAINDIR, ID_GEO_UHIDEDIRS], [ID_SVY_MAINDIR, ID_SVY_UHIDEDIRS], [ID_DSN_MAINDIR, ID_DSN_UHIDEDIRS]][index]
			maindir = os.path.join(self._app.settings.getValue(ID_NETPATH), self._app.settings.getValue(idMainDir))
			if os.path.exists(maindir):
				ignorelist = [dirname for hidedir, dirname in self._app.settings.getValue(idHideDirs) if hidedir == 1]
				for root_, folders, filenames in os.walk(maindir):
					layers = list(filter(lambda x: x not in ignorelist, folders))
					break
				#
				self.comboBoxLayer.addItems(layers)
				if currlayer in layers:
					self.comboBoxLayer.setCurrentIndex(layers.index(currlayer))
			self.comboBoxLayer.blockSignals(False)
			self.lineEditRoot.blockSignals(False)
			self.pushButtonRefresh.blockSignals(False)
			#
		elif index == 3:
			try:		self.lineEditRoot.setText(MMpy.Project.path() + "SECTIONS")
			except:		self.lineEditRoot.setText(r"D:\Workspace\Micromine\Projects\Норильский Никель\Скалистый\SECTIONS")
		elif index == 4:
			self.lineEditRoot.setText(root)

		if self._app.mainWidget.modelExplorer.explorerFilter.checkBoxExpandAll.isChecked():
			self.populateRecursively(needsFilter = False)
			for child in filter(lambda it: it.isDirectory() or it.isModel(), self.treeModel.iterChildren()):
				child.setExpanded(True)
		else:
			self.populate(needsFilter = False)
		self._app.mainWidget.modelExplorer.filterItems()


	#
	def pathHasBlockModelHeader(self, path):
		try:
			f = MicromineFile()
			if not f.open(path):
				return False
			header = f.header
			f.close()
			#
			reqfields = ["EAST", "NORTH", "RL", "_EAST", "_NORTH", "_RL"]
			if all(fld in header for fld in reqfields):
				return True
		except:
			pass
		return False
	def pathHasSectionHeader(self, path):
		try:
			f = MicromineFile()
			if not f.open(path):
				return False
			header = f.header
			f.close()
			#

			reqfields = ["NAME", "X_CENTRE", "Y_CENTRE", "Z_CENTRE", "LENGTH", "TOWARDS", "AWAY", "AZIMUTH", "INCLINATION", "ROLL", "STEP"]
			if all(fld in header for fld in reqfields):
				return True
		except:
			pass
		return False

class GeoTreeModel(TreeModel):
	def __init__(self, *args, **kw):
		super(GeoTreeModel, self).__init__(*args, **kw)
		QtGui.QShortcut(QtGui.QKeySequence("Ctrl+E"), self, None, self._openExplorer)

		self.labelRoot.setHidden(True)
		self.lineEditRoot.setHidden(True)

	def getRootFolder(self):
		layer = self.comboBoxLayer.currentText()
		if not layer:
			return None
		rootFolder = os.path.join(self._app.settings.getValue(ID_NETPATH), self._app.settings.getValue(ID_GEO_MAINDIR), layer)
		return rootFolder

	def populateDirectoryItem(self, parentDirItem):
		super(GeoTreeModel, self).populateDirectoryItem(parentDirItem)
		#
		rootFolder = self.getRootFolder()
		if rootFolder is None or not os.path.exists(rootFolder):
			return
		currentPath = rootFolder if parentDirItem is None else parentDirItem.filePath()
		parentDirItem = self.treeModel.invisibleRootItem() if parentDirItem is None else parentDirItem

		# считывание данных их настроек
		prefix = self._app.settings.getValue(ID_GEO_UPREFIX)
		dirBm = tuple(map(lambda x: x[1], self._app.settings.getValue(ID_GEO_USUBDIRS)))[0]
		dirWf = tuple(map(lambda x: x[1], self._app.settings.getValue(ID_GEO_USUBDIRS)))[1]

		for i, (root, folders, filenames) in enumerate(os.walk(currentPath)):
			# вставляем директории
			for folder in folders:
				folderpath = os.path.join(root, folder)

				# пропускаем директории внутри залежи, которые не соответствуют префиксу
				if root.lower() == rootFolder.lower() and not folder.startswith(prefix):
					continue

				# пропускаем директорию БМ и все вложенные директории
				if root.lower().endswith(dirBm.lower()) and os.path.dirname(os.path.dirname(root)).lower() == rootFolder.lower():
					continue

				# если папка модели
				if folder.lower() == dirBm.lower() and os.path.dirname(root).lower() == rootFolder.lower():
					modelNames = []
					for filename in os.listdir(folderpath):
						filepath = os.path.join(folderpath, filename)
						extension = get_file_extension(filename).upper()
						filename = remove_file_extension(filename)
						if not os.path.isfile(filepath) or extension not in [ITEM_DATA, ITEM_STRING]:
							continue

						modelName = "%s_%s" % (os.path.basename(os.path.dirname(root)), os.path.basename(root))
						if not filename.lower().startswith(modelName.lower()):
							continue

						if extension == ITEM_DATA:			bmpath = filepath
						elif extension  == ITEM_STRING:		bmpath = filepath[:-len(extension)] + ITEM_DATA
						bm = BlockModel(bmpath, self._app)
						if bm.is_defined:
							if not filename in modelNames:
								modelItem = createTreeModelItem(parentDirItem, filename, ITEM_MODEL, ITEM_CATEGORY_GEO)
								modelItem.setFilePath(bmpath)
								modelItem.setRoot(rootFolder)
								modelNames.append(filename)
				# если любая другая папка
				else:
					item = createTreeModelItem(parentDirItem, folder, ITEM_DIR, ITEM_CATEGORY_GEO)
					item.setFilePath(folderpath)
					item.setRoot(rootFolder)
			#
			# вставляем файлы
			for filename in filenames:
				filepath = os.path.join(root, filename)
				extension = get_file_extension(filename).upper()
				filename = remove_file_extension(filename)
				#
				if not extension in [ITEM_DATA, ITEM_TRIDB, ITEM_ANNOTATION, ITEM_STRING]:
					continue

				# пропускаем геологические каркасы
				if os.path.basename(root).lower() == dirWf.lower() and os.path.dirname(os.path.dirname(root)).lower() == rootFolder.lower():
					bmPath = os.path.join(os.path.dirname(root), dirBm, filename + extension)
					bm = BlockModel(bmPath, self._app)
					if extension == ITEM_TRIDB and bm.is_defined:
						continue
				#
				item = createTreeModelItem(parentDirItem, filename, extension, ITEM_CATEGORY_GEO)
				item.setFilePath(filepath)
				item.setRoot(rootFolder)
			#
			break
	def populateModelItem(self, parentModelItem):
		super(GeoTreeModel, self).populateModelItem(parentModelItem)
		rootFolder = self.getRootFolder()
		#
		bm = BlockModel(parentModelItem.filePath(), self._app)
		bmPath = bm.path
		# добавляем блочную модель, если существует
		if os.path.exists(bmPath):
			bmItem = createTreeModelItem(parentModelItem, "Блочная модель", ITEM_BM, ITEM_CATEGORY_GEO)
			bmItem.setFilePath(bmPath)
			bmItem.setRoot(rootFolder)
		# добавляем границы блочной модели
		strPath = bm.str_bounds_path
		strItem = createTreeModelItem(parentModelItem, "Границы", ITEM_STRING, ITEM_CATEGORY_GEO)
		strItem.setFilePath(strPath)
		strItem.setRoot(rootFolder)
		# добавляем геологические каркасы
		triPath = bm.tri_wireframes_path
		triItem = createTreeModelItem(parentModelItem, "Геологические каркасы", ITEM_TRIDB, ITEM_CATEGORY_GEO)
		triItem.setFilePath(triPath)
		triItem.setRoot(rootFolder)

class SvyTreeModel(TreeModel):
	def __init__(self, *args, **kw):
		super(SvyTreeModel, self).__init__(*args, **kw)
		QtGui.QShortcut(QtGui.QKeySequence("Ctrl+E"), self, None, self._openExplorer)

		self.labelRoot.setHidden(True)
		self.lineEditRoot.setHidden(True)

	def getRootFolder(self):
		layer = self.comboBoxLayer.currentText()
		if not layer:
			return None
		rootFolder = os.path.join(self._app.settings.getValue(ID_NETPATH), self._app.settings.getValue(ID_SVY_MAINDIR), layer)
		return rootFolder

	def populateDirectoryItem(self, parentDirItem):
		super(SvyTreeModel, self).populateDirectoryItem(parentDirItem)
		#
		rootFolder = self.getRootFolder()
		if rootFolder is None or not os.path.exists(rootFolder):
			return
		currentPath = rootFolder if parentDirItem is None else parentDirItem.filePath()
		parentDirItem = self.treeModel.invisibleRootItem() if parentDirItem is None else parentDirItem

		# считывание данных их настроек
		prefix = self._app.settings.getValue(ID_SVY_UPREFIX)

		for i, (root, folders, filenames) in enumerate(os.walk(currentPath)):
			# вставляем директории
			for folder in folders:
				folderpath = os.path.join(root, folder)

				# пропускаем директории внутри залежи, которые не соответствуют префиксу
				if root.lower() == rootFolder.lower() and not folder.startswith(prefix):
					continue
				item = createTreeModelItem(parentDirItem, folder, ITEM_DIR, ITEM_CATEGORY_SVY)
				item.setFilePath(folderpath)
				item.setRoot(rootFolder)
			#
			# вставляем файлы
			for filename in filenames:
				filepath = os.path.join(root, filename)
				extension = get_file_extension(filename).upper()
				filename = remove_file_extension(filename)
				#
				if not extension in [ITEM_DATA, ITEM_TRIDB, ITEM_ANNOTATION, ITEM_STRING]:
					continue
				#
				item = createTreeModelItem(parentDirItem, filename, extension, ITEM_CATEGORY_SVY)
				item.setFilePath(filepath)
				item.setRoot(rootFolder)
			#
			break

class DsnTreeModel(TreeModel):
	def __init__(self, *args, **kw):
		super(DsnTreeModel, self).__init__(*args, **kw)
		#
		QtGui.QShortcut(QtGui.QKeySequence("Ctrl+E"), self, None, self._openExplorer)

		self.labelRoot.setHidden(True)
		self.lineEditRoot.setHidden(True)

	def getRootFolder(self):
		layer = self.comboBoxLayer.currentText()
		if not layer:
			return None
		rootFolder = os.path.join(self._app.settings.getValue(ID_NETPATH), self._app.settings.getValue(ID_DSN_MAINDIR), layer)
		return rootFolder

	def populateDirectoryItem(self, parentDirItem):
		super(DsnTreeModel, self).populateDirectoryItem(parentDirItem)

		#
		rootFolder = self.getRootFolder()
		if rootFolder is None or not os.path.exists(rootFolder):
			return
		currentPath = rootFolder if parentDirItem is None else parentDirItem.filePath()
		parentDirItem = self.treeModel.invisibleRootItem() if parentDirItem is None else parentDirItem

		# считывание данных их настроек
		prefix = self._app.settings.getValue(ID_DSN_UPREFIX)

		for i, (root, folders, filenames) in enumerate(os.walk(currentPath)):
			# вставляем директории
			for folder in folders:
				folderpath = os.path.join(root, folder)

				# пропускаем директории внутри залежи, которые не соответствуют префиксу
				if root.lower() == rootFolder.lower() and not folder.startswith(prefix):
					continue
				item = createTreeModelItem(parentDirItem, folder, ITEM_DIR, ITEM_CATEGORY_DSN)
				item.setFilePath(folderpath)
				item.setRoot(rootFolder)
			#
			# вставляем файлы
			for filename in filenames:
				filepath = os.path.join(root, filename)
				extension = get_file_extension(filename).upper()
				filename = remove_file_extension(filename)
				#
				if not extension in [ITEM_DATA, ITEM_TRIDB, ITEM_ANNOTATION, ITEM_STRING]:
					continue
				#
				item = createTreeModelItem(parentDirItem, filename, extension, ITEM_CATEGORY_DSN)
				item.setFilePath(filepath)
				item.setRoot(rootFolder)
			#
			break

class DirTreeModel(TreeModel):
	def __init__(self, *args, **kw):
		super(DirTreeModel, self).__init__(*args, **kw)
		# почему-то исчезает куда-то горячая клавиша при инициализации объекта
		QtGui.QShortcut(QtGui.QKeySequence("Ctrl+E"), self, None, self._openExplorer)

		self.labelLayer.setHidden(True)
		self.comboBoxLayer.setHidden(True)

	def getRootFolder(self):
		return self.lineEditRoot.text()

	def populateDirectoryItem(self, parentDirItem):
		super(DirTreeModel, self).populateDirectoryItem(parentDirItem)
		#
		rootFolder = self.getRootFolder()
		if rootFolder is None or not os.path.exists(rootFolder):
			return
		currentPath = rootFolder if parentDirItem is None else parentDirItem.filePath()
		parentDirItem = self.treeModel.invisibleRootItem() if parentDirItem is None else parentDirItem
		#
		for i, (root, folders, filenames) in enumerate(os.walk(currentPath)):
			# вставляем директории
			for folder in folders:
				folderpath = os.path.join(root, folder)
				#
				item = createTreeModelItem(parentDirItem, folder, ITEM_DIR, ITEM_CATEGORY_USER)
				item.setFilePath(folderpath)
				item.setRoot(rootFolder)
			#
			# вставляем файлы
			for filename in filenames:
				filepath = os.path.join(root, filename)
				extension = get_file_extension(filename).upper()
				filename = remove_file_extension(filename)
				#
				if not extension in [ITEM_DATA, ITEM_TRIDB, ITEM_ANNOTATION, ITEM_STRING]:
					continue
				#
				if extension == ITEM_DATA:
					if self.pathHasBlockModelHeader(filepath):
						extension = ITEM_BM
					if self.pathHasSectionHeader(filepath):
						extension = ITEM_SECTIONPACK
				#
				item = createTreeModelItem(parentDirItem, filename, extension, ITEM_CATEGORY_USER)
				item.setFilePath(filepath)
				item.setRoot(rootFolder)
			#
			break

	def _browse_opendirectory(self):
		widget = self.lineEditRoot
		root = widget.text()

		path = QtGui.QFileDialog.getExistingDirectory(self, "Выберите директорию", root).replace(PATH_SPLITTER, "\\")
		# ничего не выбрано
		if not path:
			return

		setWidgetValue(widget, path)
		self.populate()

# модель для отображения сечений
class SecTreeModel(TreeModel):
	def __init__(self, *args, **kw):
		super(SecTreeModel, self).__init__(*args, **kw)

		self.labelLayer.setHidden(True)
		self.comboBoxLayer.setHidden(True)
	def populate(self, parentItem = None, needsFilter = False):
		super(SecTreeModel, self).populate(parentItem)

		if parentItem is not None and not parentItem.isSectionPack():
			return

		rootFolder = self.lineEditRoot.text()
		if not os.path.exists(rootFolder):
			return

		parentItem = self.treeModel if parentItem is None else parentItem
		for i, (root, folders, filenames) in enumerate(os.walk(rootFolder)):
			#
			# вставляем файлы
			for filename in filenames:
				filepath = os.path.join(root, filename)
				extension = get_file_extension(filename).upper()
				filename = remove_file_extension(filename)

				if extension != ITEM_DATA:
					continue

				if not self._isValidSectionFile(filepath):
					continue

				item = createTreeModelItem(parentItem, filename, ITEM_SECTIONPACK, ITEM_CATEGORY_SECTIONS)
				item.setFilePath(filepath)
				item.setRoot(rootFolder)
			break

	def _isValidSectionFile(self, path):
		try:
			real_fields = ["X_CENTRE", "Y_CENTRE", "Z_CENTRE", "LENGTH", "TOWARDS", "AWAY", "AZIMUTH", "INCLINATION", "ROLL", "STEP"]
			valid_structure = ["NAME:C:128:0"]
			for real_field in real_fields:
				valid_structure.append("%s:R:8:3" % real_field)
			#
			f = MicromineFile()
			if not f.open(path):
				return
			structure = f.structure
			#
			filestruct = MicromineFile.from_filestruct(structure)
			f.close()
			return filestruct == valid_structure
		except:
			return False
