from PyQt4 import QtCore, QtGui

import os
from utils.qtutil import *
from utils.constants import *

from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
import gui.qt.dialogs.messagebox as qtmsgbox
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DataGridItem(QtGui.QTableWidgetItem):
	def __init__(self, *args, **kw):
		super(DataGridItem, self).__init__(*args, **kw)
		self._is_locked = False
	def isLocked(self):					return self._is_locked
	def setLocked(self, flag):
		self._is_locked = bool(flag)
		if flag:
			self.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
		else:
			self.setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsSelectable)
	def widget(self):
		return self.tableWidget().cellWidget(self.row(), self.column())
	def setWidget(self, widget):
		self.tableWidget.setCellWidget(self.row(), self.column(), widget)

	def setValidator(self, validator):
		self.tableWidget().setItemDelegate(validator)

class DataGridBrowseItem(DataGridItem):
	def __init__(self, *args, **kw):
		super(DataGridBrowseItem, self).__init__(*args, **kw)
		iconPath = "gui/img/icons/editbutton.png"
		icon = QtGui.QIcon(iconPath)
		self.setIcon(icon)

class CheckBoxCellWidget(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(CheckBoxCellWidget, self).__init__(*args, **kw)

		self._checkbox = QtGui.QCheckBox(self)

		layout = QtGui.QHBoxLayout()
		layout.addWidget(self._checkbox)
		layout.setAlignment(QtCore.Qt.AlignCenter)
		layout.setContentsMargins(0,0,0,0)
		layout.setSpacing(0)
		layout.setMargin(0)

		self.setLayout(layout)

	@property
	def checkBox(self):
		return self._checkbox
	def getValue(self):
		return getWidgetValue(self._checkbox)
	def setValue(self, val):
		setWidgetValue(self._checkbox, val)

	def setChecked(self, flag):
		self._checkbox.setChecked(flag)
	def isChecked(self, flag):
		return self._checkbox.isChecked()


class ComboBoxEditable(QtGui.QComboBox):
	def __init__(self, *args, **kw):
		super(ComboBoxEditable, self).__init__(*args, **kw)
		self.setEditable(True)

# class LineEditButton(QtGui.QLineEdit):
# 	buttonClicked = QtCore.pyqtSignal(bool)

# 	def __init__(self, parent = None, ico_path = None):
# 		super(LineEditButton, self).__init__(parent)

# 		self.button = QtGui.QToolButton(self)
# 		self.button.setIcon(QtGui.QIcon(ico_path))
# 		self.button.setStyleSheet('border: 0px; padding: 0px;')
# 		self.button.setCursor(QtCore.Qt.ArrowCursor)
# 		self.button.clicked.connect(self.buttonClicked.emit)

# 		frameWidth = self.style().pixelMetric(QtGui.QStyle.PM_DefaultFrameWidth)
# 		buttonSize = self.button.sizeHint()

# 		self.setStyleSheet('QLineEdit {padding-right: %dpx; }' % (buttonSize.width() + frameWidth + 1))
# 		self.setMinimumSize(max(self.minimumSizeHint().width(), buttonSize.width() + frameWidth*2 + 2),
# 							max(self.minimumSizeHint().height(), buttonSize.height() + frameWidth*2 + 2))

# 	def resizeEvent(self, event):
# 		buttonSize = self.button.sizeHint()
# 		frameWidth = self.style().pixelMetric(QtGui.QStyle.PM_DefaultFrameWidth)
# 		self.button.move(self.rect().right() - frameWidth - buttonSize.width(),
# 						 (self.rect().bottom() - buttonSize.height() + 1)/2)
# 		super(LineEditButton, self).resizeEvent(event)




class DataGrid(QtGui.QTableWidget):

	comboItemChanged = QtCore.pyqtSignal(int, int)  # index, row
	def __init__(self, parent = None, columns = [], colwidgets = []):
		super(DataGrid, self).__init__(parent)

		self.setAlternatingRowColors(True)
		self._columns = columns

		self.setEditTriggers(QtGui.QTableWidget.SelectedClicked | QtGui.QTableWidget.EditKeyPressed | QtGui.QTableWidget.AnyKeyPressed)

		self._create_variables()
		self._build_grid()
		self._build_columns()

		nCols = len(columns)
		if colwidgets == []:		self._colwidgets = [None for i in range(nCols)]
		else:						self._colwidgets = colwidgets
		if nCols != len(self._colwidgets):
			raise ValueError("Sizes of data grid and column types are different")

	# protected methods
	def _create_variables(self):
		self._appendByKeyboard = True
		self._isReadOnly = False
		self._deleteByKeyboard = True
		self._doubleClickBindings = [None for i in range(len(self._columns))]
		self._readOnlyColumn = [0 for i in range(len(self._columns))]
	def _build_grid(self):
		self.horizontalHeader().setHighlightSections(False)
		self.verticalHeader().setHighlightSections(False)
		self.verticalHeader().setResizeMode(QtGui.QHeaderView.Fixed)
		self.verticalHeader().setDefaultSectionSize(20)
		self.setFocusPolicy(QtCore.Qt.StrongFocus)
	def _build_columns(self):
		self.setColumnCount(len(self._columns))
		for i, colname in enumerate(self._columns):
			self.setHorizontalHeaderItem(i, DataGridItem(colname))
	#
	# public methods	
	#
	def appendByKeyboard(self):			return self._appendByKeyboard
	def setAppendByKeyboard(self, val):	self._appendByKeyboard = bool(val)
	def deleteByKeyboard(self):			return self._deleteByKeyboard
	def setDeleteByKeyboard(self, val):	self._deleteByKeyboard = bool(val)		
	def readOnly(self):					return self._isReadOnly
	def setReadOnly(self, val):			self._isReadOnly = bool(val)

	# overriding
	def appendRow(self):
		self.insertRow(self.rowCount())
	def clear(self):
		# deletes rows if they do not contain locked items, clears cells otherwise
		nRows = self.rowCount()
		nCols = self.columnCount()
		for iRow in range(nRows-1, -1, -1):
			hasLockedItem = False
			for iCol in range(nCols):
				item = self.item(iRow, iCol)
				if not item.isLocked():	item.setText("")
				else:					hasLockedItem = True
			if not hasLockedItem:
				self.removeRow(iRow)
	def insertRow(self, iRow = False):
		if type(iRow) == bool:
			selectedIndexes = self.selectedIndexes()
			if not selectedIndexes:		iRow = 0
			else:						iRow = self.getRowsFromIndexes(selectedIndexes, reverse = False)[0]

		super(DataGrid, self).insertRow(iRow)
		nCols = self.columnCount()
		for iCol in range(nCols):
			# set widget if necessary
			widget = self._colwidgets[iCol]
			if widget == DataGridBrowseItem:
				item = DataGridBrowseItem()
				widget = None
			else:
				item = DataGridItem()
				widget = None if self._colwidgets[iCol] is None else self._colwidgets[iCol]()

			if self._isReadOnly or self._readOnlyColumn[iCol]:
				item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
			self.setItem(iRow, iCol, item)
			self.setCellWidget(iRow, iCol, widget)
			if type(widget) == QtGui.QComboBox :
				widget.currentIndexChanged.connect(lambda index: self.__comboboxChanged(index, iRow))				

	def __comboboxChanged(self,index, row):

		self.comboItemChanged.emit( index,row)

	def removeRow(self, iRow, ignoreLocked = False):
		if not self.isLockedRow(iRow):	super(DataGrid, self).removeRow(iRow)
		elif ignoreLocked:				super(DataGrid, self).removeRow(iRow)

		nRows = self.rowCount()
		nCols = self.columnCount()
		for iRow in range(nRows):
			for iCol in range(nCols):
				widget = self.cellWidget(iRow, iCol)
				if type(widget) == QtGui.QComboBox :
					try:		widget.currentIndexChanged.disconnect()
					except:		pass
					widget.currentIndexChanged.connect(lambda index: self.__comboboxChanged(index, iRow))



	#
	def bindColumnDoubleClick(self, iCol, function):
		self._doubleClickBindings[iCol] = function
		self.itemDoubleClicked.connect(self._doubleClickBindings[iCol])
	def columnDoubleClickFunction(self, iCol):
		return self._doubleClickBindings[iCol]

	# additional methods
	def getCellValue(self, iRow, iCol):
		widget = self.cellWidget(iRow, iCol)
		value = self.item(iRow, iCol).text() if widget is None else getWidgetValue(widget)
		return value
	def setCellValue(self, iRow, iCol, value):
		widget = self.cellWidget(iRow, iCol)
		if widget is not None:
			setWidgetValue(self.cellWidget(iRow, iCol), value)
		else:
			self.item(iRow, iCol).setText(str(value))
	def getRowsFromIndexes(self, indexes, reverse = False):
		# returns unique row indexes
		return sorted(set([index.row() for index in indexes]), reverse = reverse)
	def setLockedRow(self, iRow, flag):
		flag = bool(flag)
		for iCol in range(self.columnCount()):
			self.item(iRow, iCol)
	def setReadOnlyColumn(self, iCol, flag):
		for iRow in range(self.rowCount()):
			self.item(iRow, iCol).setReadOnly(bool(flag))
		self._readOnlyColumn[iCol] = bool(flag)
	def isReadOnlyColumn(self, iCol):
		return self._readOnlyColumn[iCol]
	def isLockedRow(self, iRow):
		return any([self.item(iRow, iCol).isLocked() if self.item(iRow, iCol) else False for iCol in range(self.columnCount())])
	#
	def moveRowDown(self, iRow):
		if iRow == self.rowCount() - 1:
			return
		#
		for iCol in range(self.columnCount()):
			currItem = self.item(iRow, iCol)
			nextItem = self.item(iRow+1, iCol)

			currWidget = currItem.widget()
			nextWidget = nextItem.widget()
			if not currWidget:
				tmp = currItem.text()
				currItem.setText(nextItem.text())
				nextItem.setText(tmp)
			elif type(currWidget) != CheckBoxCellWidget:
				tmp = getWidgetValue(currWidget)
				setWidgetValue(currWidget, getWidgetValue(nextWidget))
				setWidgetValue(nextWidget, tmp)
			else:
				tmp = currWidget.checkBox.isChecked()
				currWidget.checkBox.setChecked(nextWidget.checkBox.isChecked())
				nextWidget.checkBox.setChecked(tmp)

	def moveRowUp(self, iRow):
		if iRow == 0:
			return
		#
		for iCol in range(self.columnCount()):
			prevItem = self.item(iRow-1, iCol)
			currItem = self.item(iRow, iCol)

			prevWidget = prevItem.widget()
			currWidget = currItem.widget()
			if not prevWidget:
				tmp = prevItem.text()
				prevItem.setText(currItem.text())
				currItem.setText(tmp)
			elif type(prevWidget) != CheckBoxCellWidget:
				tmp = getWidgetValue(prevWidget)
				setWidgetValue(prevWidget, getWidgetValue(currWidget))
				setWidgetValue(currWidget, tmp)
			else:
				tmp = prevWidget.checkBox.isChecked()
				prevWidget.checkBox.setChecked(currWidget.checkBox.isChecked())
				currWidget.checkBox.setChecked(tmp)

	#
	def moveSelectedRowsDown(self):
		nRows = self.rowCount()
		selectionModel = self.selectionModel()
		selectFlag = QtGui.QItemSelectionModel.Select
		#
		selectedIndexes = self.selectedIndexes()
		if not selectedIndexes:
			return
		iRows = self.getRowsFromIndexes(selectedIndexes, reverse = True)
		if iRows[0] == nRows - 1:
			return
		# move rows
		for iRow in iRows:
			self.moveRowDown(iRow)
		# select items
		selectionModel.clearSelection()
		for index in selectedIndexes:
			iRow, iCol = index.row(), index.column()
			selectionModel.select(index.model().index(iRow + 1, iCol), selectFlag)
	def moveSelectedRowsUp(self):
		selectionModel = self.selectionModel()
		selectFlag = QtGui.QItemSelectionModel.Select
		#
		selectedIndexes = self.selectedIndexes()
		if not selectedIndexes:
			return
		iRows = self.getRowsFromIndexes(selectedIndexes, reverse = False)
		if iRows[0] == 0:
			return
		# move rows
		for iRow in iRows:
			self.moveRowUp(iRow)
		# select items
		selectionModel.clearSelection()
		for index in selectedIndexes:
			iRow, iCol = index.row(), index.column()
			selectionModel.select(index.model().index(iRow - 1, iCol), selectFlag)

	def removeSelectedRows(self, ignoreLocked = False):
		selectedIndexes = self.selectedIndexes()
		if not selectedIndexes:
			return
		#
		iRows = self.getRowsFromIndexes(selectedIndexes, reverse = True)
		for iRow in iRows:
			self.removeRow(iRow, ignoreLocked)
	def removeAll(self, ignoreLocked = False):
		for iRow in range(self.rowCount()-1, -1, -1):
			self.removeRow(iRow)
	# export
	def to_xml(self):
		grid = ElementTree.Element("Grid")
		nRows = self.rowCount()
		nCols = self.columnCount()
		cols = ElementTree.SubElement(grid, "Cols", {"num": str(nCols)})

		# set column description
		columnInfo = []
		iRow = 0
		for iCol in range(nCols):
			item = self.item(iRow, iCol)
			widget = self.cellWidget(iRow, iCol)
			col = ElementTree.SubElement(cols, "Col", {"id": str(iCol), "type": self._varWidgetTypes[type(widget)], "extra": "-1"})

		rows = ElementTree.SubElement(grid, "Rows", {"num": str(nRows)})
		for iRow in range(nRows):
			row = ElementTree.SubElement(rows, "R")
			for iCol in range(nCols):
				item = self.item(iRow, iCol)
				widget = self.cellWidget(iRow, iCol)

				# retrieve the value
				value = self.getCellValue(iRow, iCol)

				col = ElementTree.SubElement(row, "C", {"v": value})

		xmlstring = ElementTree.tostring(grid, "utf-8")
		parsed = minidom.parseString(xmlstring).toprettyxml()#encoding = "utf-8")
		return parsed

	def to_list(self):
		nRows = self.rowCount()
		nCols = self.columnCount()

		ret = []
		for iRow in range(nRows):
			ret.append([self.getCellValue(iRow, iCol) for iCol in range(nCols)])
		return ret
	def from_list(self, values):
		self.removeAll()
		if not values:
			return
			
		nRows = len(values)
		nCols = min([len(values[0]), self.columnCount()])
		for iRow in range(nRows):
			if iRow >= self.rowCount():
				self.appendRow()
			for iCol in range(nCols):
				self.setCellValue(iRow, iCol, values[iRow][iCol])

class GridEditorToolBar(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(GridEditorToolBar, self).__init__(*args, **kw)

		self.__create_variables()
		self.__create_widgets()
		self.__set_tooltips()
		self.__grid_widgets()

	def __create_variables(self):
		impath = "gui/img/icons"
		imlist = ["clear.png", "refresh.png", "insert.png", "append.png", "delete.png", "moveup.png", "movedown.png"]
		self._images = tuple(map(lambda x: os.path.join(impath, x), imlist))
	def __create_widgets(self):
		self.horizontalLayout = QtGui.QHBoxLayout()
		self.horizontalLayout.setSpacing(0)
		self.horizontalLayout.setMargin(0)

		self.labelHint = QtGui.QLabel(self)
		self.labelHint.setWordWrap(True)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)

		# buttons		
		self.pushButtonClear = QtGui.QPushButton(self)
		self.pushButtonRefresh = QtGui.QPushButton(self)
		self.pushButtonInsert = QtGui.QPushButton(self)
		self.pushButtonAppend = QtGui.QPushButton(self)
		self.pushButtonDelete = QtGui.QPushButton(self)
		self.pushButtonMoveUp = QtGui.QPushButton(self)
		self.pushButtonMoveDown = QtGui.QPushButton(self)
		self._buttons =  [self.pushButtonClear, self.pushButtonRefresh, self.pushButtonInsert, self.pushButtonAppend]
		self._buttons += [self.pushButtonDelete, self.pushButtonMoveUp, self.pushButtonMoveDown]

		for icopath, button in zip(self._images, self._buttons):
			icon = QtGui.QIcon()
			icon.addPixmap(QtGui.QPixmap(icopath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
			button.setIcon(icon)
			button.setIconSize(QtCore.QSize(13, 13))
			button.setMinimumSize(QtCore.QSize(20, 20))
			button.setMaximumSize(QtCore.QSize(20, 20))
			# button.setFlat(True)
			button.setFocusPolicy(QtCore.Qt.ClickFocus)
	def __grid_widgets(self):
		self.setLayout(self.horizontalLayout)
		self.horizontalLayout.addWidget(self.labelHint)
		self.horizontalLayout.addWidget(self.pushButtonClear)
		self.horizontalLayout.addWidget(self.pushButtonRefresh)
		self.horizontalLayout.addWidget(self.pushButtonInsert)
		self.horizontalLayout.addWidget(self.pushButtonAppend)
		self.horizontalLayout.addWidget(self.pushButtonDelete)
		self.horizontalLayout.addWidget(self.pushButtonMoveUp)
		self.horizontalLayout.addWidget(self.pushButtonMoveDown)
	def __set_tooltips(self):
		self.pushButtonClear.setToolTip("Очистить")
		self.pushButtonRefresh.setToolTip("Обновить")
		self.pushButtonInsert.setToolTip("Вставить строку")
		self.pushButtonAppend.setToolTip("Добавить строку")
		self.pushButtonDelete.setToolTip("Удалить строку")
		self.pushButtonMoveUp.setToolTip("Переместить наверх")
		self.pushButtonMoveDown.setToolTip("Переместить вниз")

	@property
	def buttons(self):
		return tuple(self._buttons)

class GridEditor(QtGui.QWidget):
	def __init__(self, parent = None, columns = [], colwidgets = []):
		super(GridEditor, self).__init__(parent)
		self._columns = columns
		self._colwidgets  = colwidgets

		self.__create_variables()
		self.__create_widgets()
		self.__create_bindings()
		self.__grid_widgets()

	def __create_variables(self):
		pass
	def __create_widgets(self):
		self.mainLayout = QtGui.QVBoxLayout()
		self.mainLayout.setSpacing(0)
		self.mainLayout.setMargin(0)

		self.toolBar = GridEditorToolBar(self)
		self.dataGrid = DataGrid(self, self._columns, self._colwidgets)
	def __create_bindings(self):
		self.toolBar.pushButtonClear.clicked.connect(self.clear)
		self.toolBar.pushButtonAppend.clicked.connect(self.appendRow)
		self.toolBar.pushButtonInsert.clicked.connect(self.insertRowAboveSelected)
		self.toolBar.pushButtonDelete.clicked.connect(self.removeSelectedRows)
		self.toolBar.pushButtonMoveDown.clicked.connect(self.moveSelectedRowsDown)
		self.toolBar.pushButtonMoveUp.clicked.connect(self.moveSelectedRowsUp)


	def __grid_widgets(self):
		self.setLayout(self.mainLayout)
		self.mainLayout.addWidget(self.toolBar)
		self.mainLayout.addWidget(self.dataGrid)

	def keyPressEvent(self, event):
		DataGrid.keyPressEvent(self.dataGrid, event)

		key = event.key()

		# Delete and Backspace
		if key == QtCore.Qt.Key_Delete or key == QtCore.Qt.Key_Backspace:
			for index in self.dataGrid.selectedIndexes():
				item = self.dataGrid.itemFromIndex(index)
				if not item.isLocked() and not self.dataGrid.isReadOnly():
					item.setText("")

		# Ctrl+Delete
		if event.modifiers() & QtCore.Qt.ControlModifier and key == QtCore.Qt.Key_Delete and self.dataGrid.deleteByKeyboard():
			self.removeSelectedRows()

		# return
		if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
			self.dataGrid.clearSelection()
			#
			currIndex = self.dataGrid.currentIndex()
			nRows = self.dataGrid.rowCount()
			# append row if the last row is a current row
			iRow, iCol = currIndex.row(), currIndex.column()
			if iRow == nRows-1 and self.dataGrid.appendByKeyboard():
				self.appendRow()
			# select cell
			if currIndex.model():		self.dataGrid.setCurrentCell(iRow+1, iCol)
			else:						self.dataGrid.setCurrentCell(0,0)	


	def clear(self):
		self.dataGrid.clear()
	def appendRow(self):
		self.dataGrid.appendRow()
	def insertRowAboveSelected(self):
		iRows = sorted(self.dataGrid.getRowsFromIndexes(self.dataGrid.selectedIndexes()))
		if not iRows:
			return
		iRow = iRows[0]
		self.insertRow(iRow)
	def removeSelectedRows(self):
		self.dataGrid.removeSelectedRows()
	def moveSelectedRowsDown(self):
		self.dataGrid.moveSelectedRowsDown()
	def moveSelectedRowsUp(self):
		self.dataGrid.moveSelectedRowsUp()


class FileStructureCellValidator(QtGui.QStyledItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()
		if not icol in [2,3]:
			return super(FileStructureCellValidator, self).createEditor(widget, option, index)

		editor = QtGui.QLineEdit(widget)
		if icol == 2:	validator = QtGui.QIntValidator(0, 255)
		if icol == 3:	validator = QtGui.QIntValidator(0, 9)
		editor.setValidator(validator)
		return editor

class FileStructureEditor(GridEditor):
	def __init__(self, *args, **kw):
		super(FileStructureEditor, self).__init__(*args, **kw)

		self.__create_variables()
		self.__create_widgets()
		self.__create_bindings()
		self.__retranslate_ui()

	def __create_variables(self):
		self._field_types = ["C", "R", "N", "F", "L", "S"]
		self._comboboxes = []
	def __create_widgets(self):
		self.tableWidget.keyPressEvent = self.keyPressEvent
		self.tableWidget.setRowCount(0)
		self.tableWidget.setColumnCount(4)
		for i in range(self.tableWidget.columnCount()):
			item = DataGridItem()
			self.tableWidget.setHorizontalHeaderItem(i, item)
		self.tableWidget.setItemDelegate(FileStructureCellValidator())
		self._append_row()
	def __create_bindings(self):
		pass

	def __retranslate_ui(self):
		columns = ["ИМЯ ПОЛЯ", "ТИП", "ШИРИНА", "ПОСЛЕ ЗАП."]
		for i, name in enumerate(columns):
			item = self.tableWidget.horizontalHeaderItem(i)
			item.setText(name)

	def keyPressEvent(self, event):
		key = event.key()

		if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
			self.tableWidget.clearSelection()
			
			index = self.tableWidget.currentIndex()
			nrows = self.tableWidget.rowCount()

			row, col = index.row(), index.column()
			if row == nrows-1:
				self._append_row()

			if not index.model():
				self.tableWidget.setCurrentCell(0,0)
				self.tableWidget.setCurrentIndex(index)
				return

			nextCell = index.model().index(row+1, col)
			self.tableWidget.setCurrentIndex(nextCell)
		else:
			TableWidget.keyPressEvent(self.tableWidget, event)
			
	def _append_row(self):
		super(FileStructureEditor, self)._append_row()
		irow = self.tableWidget.rowCount()-1
		combo = self._create_combobox(irow, 1)
		self.tableWidget.setCellWidget(irow, 1, combo)
		self.set_actions(combo)
	def _insert_row(self):
		super(FileStructureEditor, self)._insert_row()
		currIndex = self.tableWidget.currentIndex()
		irow = currIndex.row()-1
		if irow < 0:
			return

		combo = self._create_combobox(irow, 1)
		self.tableWidget.setCellWidget(irow, 1, combo)
		self.set_actions(combo)

	def _create_combobox(self, irow, icol):
		combo = QtGui.QComboBox()
		combo.row = irow
		combo.column = icol
		for ft in self._field_types:
			combo.addItem(ft)
		combo.currentIndexChanged.connect(lambda: self.set_actions(combo))
		return combo
	
	def set_actions(self, combo):
		irow, index = combo.row, combo.currentIndex()
		if index == 0:
			item = self.tableWidget.item(irow, 2)
			item.setFlags(QtCore.Qt.NoItemFlags | item.flags() | QtCore.Qt.ItemIsEditable)
			item.setBackground(QtGui.QColor(255, 255, 255))

			item = self.tableWidget.item(irow, 3)
			item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
			item.setBackground(QtGui.QColor(240, 240, 240))
		elif index in [1,2]:
			item = self.tableWidget.item(irow, 2)
			item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
			item.setBackground(QtGui.QColor(240, 240, 240))

			item = self.tableWidget.item(irow, 3)
			item.setFlags(QtCore.Qt.NoItemFlags | item.flags() | QtCore.Qt.ItemIsEditable)
			item.setBackground(QtGui.QColor(255, 255, 255))
		else:
			item = self.tableWidget.item(irow, 2)
			item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
			item.setBackground(QtGui.QColor(240, 240, 240))

			item = self.tableWidget.item(irow, 3)
			item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
			item.setBackground(QtGui.QColor(240, 240, 240))


class GridEditorFolders(GridEditor):
	def __init__(self, parent):
		columns = ["Описание", "Директория"]
		super(GridEditorFolders, self).__init__(parent, columns)

		self._customizeView()
		# set getter and setter
		self.getValue = self.dataGrid.to_list
		self.setValue = self.dataGrid.from_list

	def _customizeView(self):
		self.toolBar.labelHint.setText("Директории внутри юнита")
		#
		self.dataGrid.horizontalHeader().setHighlightSections(False)
		self.dataGrid.horizontalHeader().setMinimumSectionSize(100)
		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)
		self.dataGrid.setMinimumWidth(310)

		# toolbar
		self.toolBar.pushButtonRefresh.setVisible(False)
		self.toolBar.pushButtonInsert.setVisible(False)
		self.toolBar.pushButtonMoveUp.setVisible(False)
		self.toolBar.pushButtonMoveDown.setVisible(False)

	def insertDefaultData(self, data):
		self.dataGrid.setRowCount(0)
		for irow, row in enumerate(data):
			self.dataGrid.appendRow()
			for icol, val in enumerate(row):
				item = DataGridItem(val)
				if icol == 0:
					item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
					item.setLocked(True)
				self.dataGrid.setItem(irow, icol, item)

class GridEditorFormsets(GridEditor):
	def __init__(self, parent):
		columns = ["Тип данных", "Форма загрузки"]
		super(GridEditorFormsets, self).__init__(parent, columns)

		self._customizeView()

		self.__createVariables()
		self.__createBindings()

		self.insertDefaultData(self._defaultFormsetData)

		# set getter and setter
		self.getValue = self.dataGrid.to_list
		self.setValue = self.dataGrid.from_list

	def __createVariables(self):
		self._defaultFormsetData = [(mmtype, "") for mmtype in MMTYPE_LIST]

	def _customizeView(self):
		self.toolBar.labelHint.setText("Выберите форму загрузку для каждого типа данных")

		#
		self.dataGrid.horizontalHeader().setHighlightSections(False)
		self.dataGrid.horizontalHeader().setStretchLastSection(False)
		self.dataGrid.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Stretch)
		self.dataGrid.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Fixed)
		self.dataGrid.setColumnWidth(1, 100)
		self.dataGrid.verticalHeader().setVisible(False)
		self.dataGrid.setMinimumWidth(150)

		self.dataGrid.setAppendByKeyboard(False)
		self.dataGrid.setDeleteByKeyboard(False)

		# toolbar
		self.dataGrid.setReadOnly(True)
		self.toolBar.pushButtonAppend.setVisible(False)
		self.toolBar.pushButtonInsert.setVisible(False)
		self.toolBar.pushButtonMoveUp.setVisible(False)
		self.toolBar.pushButtonMoveDown.setVisible(False)
		self.toolBar.pushButtonDelete.setVisible(False)
		self.toolBar.pushButtonRefresh.setVisible(False)

	def __createBindings(self):
		self.dataGrid.itemDoubleClicked.connect(self._selectSetId)

	def insertDefaultData(self, data):
		self.dataGrid.setRowCount(0)
		for irow, row in enumerate(data):
			self.dataGrid.appendRow()
			for icol, val in enumerate(row):
				item = DataGridItem(val)
				if icol == 0:
					item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
					item.setLocked(True)
				self.dataGrid.setItem(irow, icol, item)

	def _selectSetId(self, item):
		if item.column() != 1:
			return

		mmtype = item.tableWidget().item(item.row(), 0).text()
		cmdId = MMTYPE_COMMAND_ID_DICT[mmtype]

		tree = TreeMmFormsetBrowserDialog(self)
		if not tree.readFormsets(cmdId):
			return
		
		tree.exec()
		if tree.result:
			item.setText(str(tree.result))
