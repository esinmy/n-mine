import os
from PyQt4 import QtGui, QtCore
from datetime import datetime
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class LineEditFilter(QtGui.QLineEdit):
	def __init__(self, parent, column):
		super(LineEditFilter, self).__init__(parent)
		self._column = column
		self.setPlaceholderText("Фильтр")
		self.textChanged.connect(self.filterColumn)
	def keyPressEvent(self, event):
		# key = event.key()
		# if key in [QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter]:
		# 	self.filterColumn()
		super(LineEditFilter, self).keyPressEvent(event)
	def filterColumn(self):
		text = self.text()
		model = self.parent().parent().model()
		model.setFilterString(text, self._column)
		# model.applyFilter()
	def event(self, event):
		if event.type() == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Tab:
			self.parent().filterEdit(self._column+1)
			return -1
		return QtGui.QLineEdit.event(self, event)

class ButtonFilter(QtGui.QPushButton):
	def __init__(self, parent, column):
		super(ButtonFilter, self).__init__(parent)
		self._column = column
		self.setText("Выделить все")
		self.clicked.connect(self.selectAll)
	def event(self, event):
		if event.type() == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Tab:
			self.parent().filterEdit(self._column+1)
			return -1
		return QtGui.QPushButton.event(self, event)
	def selectAll(self):
		view = self.parent().parent()
		model = view.model()
		checkAll = not all(model.data(model.createIndex(irow, self._column))  for irow in range(model.rowCount()))
		if checkAll:
			for irow in range(model.rowCount()):
				model.setData(model.createIndex(irow, self._column), True, QtCore.Qt.EditRole)
		else:
			for irow in range(model.rowCount()):
				model.setData(model.createIndex(irow, self._column), False, QtCore.Qt.EditRole)
		view.setFocus()
		view.customAnyRowChecked.emit(view)

class FilterHeaderCheckView(QtGui.QHeaderView):
	def __init__(self, parent):
		self._createVariables()
		super(FilterHeaderCheckView, self).__init__(QtCore.Qt.Horizontal, parent)

		self.setClickable(True)
		self.setDefaultSectionSize(130)
		self.setDefaultAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
		self.setStretchLastSection(True)
		# self.setResizeMode(QtGui.QHeaderView.Stretch)

		self._createBindings()

	def _createVariables(self):
		self._controls = []
		self._defaultSectionHeight = 25

	def _createBindings(self):
		scrollBar = self.parent().horizontalScrollBar()
		scrollBar.valueChanged.connect(self.updateFilterGeometry)
		self.sectionResized.connect(self.updateFilterGeometry)

	def _createFilter(self):
		button = ButtonFilter(self, 0)
		button.setVisible(True)
		self._controls.append(button)
		for i in range(1, self.parent().model().columnCount()):
			edit = LineEditFilter(self, i)
			edit.setVisible(True)
			self._controls.append(edit)

	def showFilter(self):
		if self._controls:
			return

		self._createFilter()
		self.updateSections()
		self.updateFilterGeometry()

	def removeFilter(self):
		for edit in self._controls:
			edit.deleteLater()
		self._controls.clear()
		self.updateSections()

	def sizeHint(self):
		size = super(FilterHeaderCheckView, self).sizeHint()
		if self.hasFilter():	size.setHeight(2*self._defaultSectionHeight)
		else:					size.setHeight(self._defaultSectionHeight)
		return size

	def hasFilter(self):
		return len(self._controls) != 0

	def updateSections(self):
		# needed to resize the section height (super hack)
		for i in range(1, self.parent().model().columnCount()):
			self.resizeSection(i, self.sectionSize(i)-1)
			self.resizeSection(i, self.sectionSize(i)+1)

	def updateGeometries(self):
		super(FilterHeaderCheckView, self).updateGeometries()

		self.updateFilterGeometry()

	def updateFilterGeometry(self):
		for i, edit in enumerate(self._controls):
			x = self.sectionPosition(i) - self.offset()
			y = self.sizeHint().height() // 2
			w = self.sectionSize(i)
			h = self.sizeHint().height() // 2
			edit.setGeometry(x, y, w, h)
	def filterEdit(self, i):
		if i >= len(self._controls):
			i = 0
		if type(self._controls[i]) == LineEditFilter:
			self._controls[i].selectAll()
		self._controls[i].setFocus()

	@property
	def controls(self):
		return tuple(self._controls)

class CustomDataCheckModel(QtCore.QAbstractTableModel):
	customRowsCountChanged = QtCore.pyqtSignal(int) # count
	def __init__(self, parent, data = None, header = None):
		super(CustomDataCheckModel, self).__init__(parent)
		self._parent = parent
		if data is not None:
			self._original = [[False] + list(row) for row in data]
			self._data = self._original.copy()
			self._fstrings = {i: "" for i in range(self.columnCount())}
		else:
			self._original = None
			self._data = None
			self._fstrings = None

		if header is not None:
			self._header = ["ЗАГРУЗИТЬ"] + list(header)
		else:
			self._header = None

	def rowCount(self, parent = None):
		return len(self._data) if self._data else 0
	def columnCount(self, parent = None):
		return len(self._data[0]) if self._data else 0
	def data(self, index, role = QtCore.Qt.DisplayRole):
		if not index.isValid() or role != QtCore.Qt.DisplayRole:
			return None
		#
		ret = self._data[index.row()][index.column()]
		#
		if ret is None:						return ""
		if isinstance(ret, datetime):		return ret.strftime("%d.%m.%Y")
		if isinstance(ret, bool):			return int(ret)
		
		return ret
	def headerData(self, col, orientation, role):
		if role == QtCore.Qt.DisplayRole:
			if orientation == QtCore.Qt.Horizontal:		return self._header[col] if self._header is not None else ""
			if orientation == QtCore.Qt.Vertical:		return col+1
		return None
	def setData(self, index, value, role):
		self._data[index.row()][index.column()] = value
		return True
	def flags(self, index):
		if index.column() == 0:
			return QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable
		return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
	def applyFilter(self, datefrcol = None, datetocol = None, extents = None, xcol = None, ycol = None, zcol = None):
		window = self.parent().parent()
		if window.hasDateFilter:
			selectedDateFrom = window.dateRangeWidget.dateFrom()
			selectedDateTo = window.dateRangeWidget.dateTo()
			datefrom = datetime(*selectedDateFrom.getDate()).strftime("%Y-%m-%d")
			dateto = datetime(*selectedDateTo.getDate()).strftime("%Y-%m-%d")
		
		if datefrcol is not None and datefrcol > 0:
			datefrcol += 1
		if datetocol is not None and datetocol > 0:
			datetocol += 1

		xmin_criterion= xmax_criterion= ymin_criterion= ymax_criterion= zmin_criterion= zmax_criterion = False
		if not extents is None and {'xmin', 'ymin', 'zmin', 'xmax', 'ymax', 'zmax'} - set(extents.keys()) == set():
			if all ([xcol, extents['xmin']]): xmin_criterion = True
			if all ([xcol, extents['xmax']]): xmax_criterion = True
			if all ([ycol, extents['ymin']]): ymin_criterion = True
			if all ([ycol, extents['ymax']]): ymax_criterion = True
			if all ([zcol, extents['zmin']]): zmin_criterion = True
			if all ([zcol, extents['zmax']]): zmax_criterion = True





		self._data = []
		for row in range(len(self._original)):
			self._parent.hideRow(row)
			
			filteredByDate = True
			filteredByObject = True
			if window.hasDateFilter and datefrcol is not None and datetocol is not None:
				dfr = self._original[row][datefrcol]
				dto = self._original[row][datetocol]
				if dfr or dto:
					filteredByDate = (dfr >= datefrom and dfr <= dateto) or (dto >= datefrom and dto <= dateto)

			if filteredByObject and xmin_criterion:
				xcol_value = float(self._original[row][xcol])
				filteredByObject = xcol_value >= extents['xmin']
			if filteredByObject and xmax_criterion:
				xcol_value = float(self._original[row][xcol])
				filteredByObject = xcol_value <= extents['xmax']
			if filteredByObject and ymin_criterion:
				ycol_value = float(self._original[row][ycol])
				filteredByObject = ycol_value >= extents['ymin']
			if filteredByObject and ymax_criterion:
				ycol_value = float(self._original[row][ycol])
				filteredByObject = ycol_value <= extents['ymax']
			if filteredByObject and zmin_criterion:
				zcol_value = float(self._original[row][zcol])
				filteredByObject = zcol_value >= extents['zmin']
			if filteredByObject and zmax_criterion:
				zcol_value = float(self._original[row][zcol])
				filteredByObject = zcol_value <= extents['zmax']


			if not (filteredByDate and filteredByObject):
				continue

			filtered = [self._fstrings[col] in str(self._original[row][col]).lower() for col in self._fstrings]
			if all(filtered):
				self._data.append(self._original[row])

		if self.rowCount() > 0:
			self.emit(QtCore.SIGNAL("dataChanged(QModelIndex, QModelIndex)"), self.createIndex(0, self.columnCount()), self.createIndex(self.rowCount(), self.columnCount()))
		else:
			self.emit(QtCore.SIGNAL("dataChanged(QModelIndex, QModelIndex)"), self.createIndex(0, self.columnCount()), self.createIndex(1, self.columnCount()))
		for i in range(self.rowCount()):
			self._parent.showRow(i)

		self.customRowsCountChanged.emit(self.rowCount())

	def setFilterString(self, string, column):
		self._fstrings[column] = string.lower()
	def parent(self):
		return self._parent
	def getSelectedCount(self):
		count = 0
		zipped_data = list(zip(*self._original))
		count = len(list(filter(lambda x: x==True, zipped_data[0])))
		return count

	def getSelected(self):
		data = []
		for irow in range(len(self._original)):
			loadbool = self._original[irow][0]
			if not loadbool:
				continue
			data.append([self._original[irow][icol] for icol in range(1, self.columnCount())])
		return data

class CustomDataCheckView(QtGui.QTableView):
	customAnyRowChecked = QtCore.pyqtSignal(object)  
	def __init__(self, parent):
		super(CustomDataCheckView, self).__init__(parent)
		self.verticalHeader().setVisible(False)
		#
		self.setSelectionBehavior(QtGui.QTableView.SelectRows)
		# self.setSelectionMode(QtGui.QTableView.SingleSelection)
		self.setSelectionMode(QtGui.QTableView.ExtendedSelection)
		self.setAlternatingRowColors(True)
		self.checkBoxDelegate = CheckBoxDelegate()
		self.checkBoxDelegate.customAnyRowChecked.connect(self.on_checkBoxDelegate_customAnyRowChecked)
		self.setItemDelegate(self.checkBoxDelegate)

	def on_checkBoxDelegate_customAnyRowChecked(self):
		self.customAnyRowChecked.emit(self)
	def setModel(self, model):
		super(CustomDataCheckView, self).setModel(model)
		self.setHorizontalHeader(FilterHeaderCheckView(self))
		self.horizontalHeader().showFilter()

		self.setVerticalHeader(QtGui.QHeaderView(QtCore.Qt.Vertical, self))
		self.verticalHeader().setResizeMode(QtGui.QHeaderView.Fixed)

		self.setColumnWidth(0, 100)



	def getSelected(self):
		data = []
		for irow in range(self.model().rowCount()):
			loadbool = self.model().data(self.model().createIndex(irow, 0))
			if not loadbool:
				continue
			data.append([self.model().data(self.model().createIndex(irow, icol)) for icol in range(1, self.model().columnCount())])
		return data

class CheckBoxDelegate(QtGui.QStyledItemDelegate):
	customAnyRowChecked = QtCore.pyqtSignal() 
	def createEditor(self, parent, option, index):
		# Important, otherwise an editor is created if the user clicks in this cell.
		return None

	def paint(self, painter, option, index):
		# Paint a checkbox without the label.

		if index.column() > 0:
			return super(CheckBoxDelegate, self).paint(painter, option, index)

		checked = bool(index.model().data(index, QtCore.Qt.DisplayRole))
		chbopt = QtGui.QStyleOptionButton()

		if index.flags() & QtCore.Qt.ItemIsEditable:
			chbopt.state |= QtGui.QStyle.State_Enabled

		if checked:		chbopt.state |= QtGui.QStyle.State_On
		else:			chbopt.state |= QtGui.QStyle.State_Off

		chbopt.rect = self.getCheckBoxRect(option)

		if option.state & QtGui.QStyle.State_Selected:
			if option.state & QtGui.QStyle.State_Active:
				painter.setBrush(QtGui.QBrush(QtGui.QPalette().highlight()))
			else:
				painter.setBrush(QtGui.QBrush(QtGui.QPalette().color(QtGui.QPalette.Inactive, QtGui.QPalette.Highlight)))
			painter.drawRect(option.rect)

		QtGui.QApplication.style().drawControl(QtGui.QStyle.CE_CheckBox, chbopt, painter)


	def editorEvent(self, event, model, option, index):
		# Change the data in the model and the state of the checkbox
		# if the user presses the left mousebutton or presses
		# Key_Space or Key_Select and this cell is editable. Otherwise do nothing.
		
		if not int(index.flags() & QtCore.Qt.ItemIsEditable) > 0:
			return False

		# Do not change the checkbox-state
		if event.type() == QtCore.QEvent.MouseButtonRelease or event.type() == QtCore.QEvent.MouseButtonDblClick:
			if event.button() != QtCore.Qt.LeftButton or not self.getCheckBoxRect(option).contains(event.pos()):
				return False
			if event.type() == QtCore.QEvent.MouseButtonDblClick:
				return True
		elif event.type() == QtCore.QEvent.KeyPress:
			if event.key() != QtCore.Qt.Key_Space and event.key() != QtCore.Qt.Key_Select:
				return False
		else:
			return False

		# Change the checkbox-state
		self.setModelData(None, model, index)
		self.customAnyRowChecked.emit()
		return True

	def setModelData (self, editor, model, index):
		view = model.parent()
		checkAll = not all(model.data(index) for index in view.selectionModel().selectedRows())
		if checkAll:
			for index in view.selectionModel().selectedRows():
				model.setData(index, True, QtCore.Qt.EditRole)
		else:
			for index in view.selectionModel().selectedRows():
				model.setData(index, False, QtCore.Qt.EditRole)

	def getCheckBoxRect(self, option):
		check_box_style_option = QtGui.QStyleOptionButton()
		check_box_rect = QtGui.QApplication.style().subElementRect(QtGui.QStyle.SE_CheckBoxIndicator, check_box_style_option, None)
		check_box_point = QtCore.QPoint (option.rect.x() +
							 option.rect.width() / 2 -
							 check_box_rect.width() / 2,
							 option.rect.y() +
							 option.rect.height() / 2 -
							 check_box_rect.height() / 2)
		return QtCore.QRect(check_box_point, check_box_rect.size())

###################################################################################################################
################################################### EXAMPLE #######################################################
###################################################################################################################

class Window(QtGui.QWidget):
	def __init__(self, parent = None):
		super(Window, self).__init__(parent)

		self._createVariables()
		self._createWidgets()
		self._gridWidgets()

	def _createVariables(self):
		tick = datetime.now()


	def _createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()


		self._data = [[True, "12312", 1, -i, i] for i in range(10)]

		self.view = CustomDataCheckView(self)
		self.model = CustomDataCheckModel(self.view, self._data, ["ЗАГРУЗИТЬ", "POINT", "EAST", "NORTH", "RL"])
		self.view.setModel(self.model)

		self.view.doubleClicked.connect(self.check)

	def _gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.view)

	def check(self, index):
		data = []
		for irow in range(self.model.rowCount()):
			if self.model.data(self.model.createIndex(irow, 0)):
				data.append([self.model.data(self.model.createIndex(irow, icol)) for icol in range(self.model.columnCount())])



# app = QtGui.QApplication(os.sys.argv)
# w = Window()
# w.resize(800, 400)
# w.show()
# os.sys.exit(app.exec_())