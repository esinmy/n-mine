﻿try:		import MMpy
except:		pass
from PyQt4 import QtGui, QtCore
import os
from mm.mmutils import Tridb, MicromineFile
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog
#from gui.qt.modes.geo.GeoUpdateBm import get_wireframes_extents
import gui.qt.dialogs.messagebox as qtmsgbox
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def get_wireframes_extents(tripath, wfnames):
	import sqlite3

	ret = {"xmin": None, "xmax": None, "ymin": None, "ymax": None, "zmin": None, "zmax": None}

	cnxn = sqlite3.connect(tripath)
	cursor = cnxn.cursor()
	cmd = """
		SELECT MIN(XMinimum), MAX(XMaximum), MIN(YMinimum), MAX(YMaximum), MIN(ZMinimum), MAX(ZMaximum)
		FROM GeneralInformation
		WHERE Name in ('%s')""" % "','".join(wfnames)
	cursor.execute(cmd)
	extents = cursor.fetchone()
	cnxn.close()

	if extents:
		xmin, xmax, ymin, ymax, zmin, zmax = extents
		ret["xmin"], ret["xmax"] = xmin, xmax
		ret["ymin"], ret["ymax"] = ymin, ymax
		ret["zmin"], ret["zmax"] = zmin, zmax	
	return ret

class DropDownWidget(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DropDownWidget, self).__init__(*args, **kw)
		self.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)

		self.__createVariables()
		self.__createWidgets()
		self.__gridWidgets()

		

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.mainLayout = QtGui.QHBoxLayout(self)
		self.mainLayout.setMargin(0)

		self.button = QtGui.QToolButton(self)

		self.button.setText("<не определено>")
		self.button.setMinimumWidth(200)
		self.button.setMaximumWidth(300)
		self.button.setPopupMode(QtGui.QToolButton.InstantPopup)

		self.menu = QtGui.QMenu(self.button)
		self.menu.installEventFilter(self)
		self.button.setMenu(self.menu)

		self.action = QtGui.QWidgetAction(self.button)
		self.action.setCheckable(True)
		self.menu.addAction(self.action)

		

	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.mainLayout)
		self.mainLayout.addWidget(self.button)
		

	def setWidget(self, Widget):
		self.action.setDefaultWidget(Widget)
		self.menu.clear() 
		self.menu.addAction(self.action)
		Widget.filterApplied.connect(self.filterApplied)
		Widget.filterDefaultRestored.connect(self.filterDefaultRestored)

	def filterApplied(self, sender):
		self.button.setText(sender.tittle)

	def filterDefaultRestored(self, sender):
		self.button.setText(sender.tittle)


class ShapeFilterWidget(QtGui.QWidget):	   
	filterApplied = QtCore.pyqtSignal(object)
	filterDefaultRestored = QtCore.pyqtSignal(object)
	def __init__(self, parent,  *args, **kw):
		super(ShapeFilterWidget, self).__init__(*args, **kw)
		self._parent = parent
		self.__createVariables()
		self.__createWidgets()
		self.__gridWidgets()
		self.__createBindings()
		self.__initForm()
		

	def __createVariables(self):
		self._xmin = None
		self._xmax = None
		self._ymin = None
		self._ymax = None
		self._zmin = None
		self._zmax = None
		self._coords=(self._xmin, self._xmax, self._ymin, self._ymax, self._zmin, self._zmax)

		self.__get_xxx_extents = None

		self.tittle = "<не определено>"
	def __createWidgets(self):
		self._font = QtGui.QFont()
		self._font.setPointSize(8.5)
		self.setFont(self._font)

		self.mainLayout = QtGui.QVBoxLayout(self)
		self.mainLayout.setMargin(0)

		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Файл границ")
		self.inputLayout = QtGui.QFormLayout()

		self.labelFile = QtGui.QLabel(self.groupBoxInput)
		self.labelFile.setText("Файл")
		self.labelFile.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelFile.setMinimumWidth(60)
		self.lineFile = QtGui.QLineEdit(self.groupBoxInput)
		self.lineFile.setMinimumWidth(100)	

		self.labelShape = QtGui.QLabel(self.groupBoxInput)
		self.labelShape.setText("Объект")
		self.labelShape.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelShape.setMinimumWidth(60)
		self.lineShape = QtGui.QLineEdit(self.groupBoxInput)
		self.lineShape.setMinimumWidth(100)

		self.filterModeGroupBox = QtGui.QGroupBox(self)
		self.filterModeGroupBox.setTitle("Режим фильтрации")
		self.filterModeLayout = QtGui.QVBoxLayout()

		self.filterModePlan = QtGui.QRadioButton("План", self.filterModeGroupBox)
		self.filterModeSolid = QtGui.QRadioButton("Объём", self.filterModeGroupBox)
		self.filterModePlan.setChecked(True)

		self.ExtensionLabel = QtGui.QLabel(self)
		self.ExtensionLabel.setText("Расширение границ")
		
		self.ExtensionSpinBox = QtGui.QSpinBox(self)
		self.ExtensionSpinBox.setValue(0)
		self.ExtensionSpinBox.setMinimum(0)
		self.ExtensionSpinBox.setMaximum(1000)


		self.boundariesGroupBox = QtGui.QGroupBox(self)
		self.boundariesGroupBox.setTitle("Координаты границ")
		self.boundariesLayout = QtGui.QGridLayout()

		self.minLabel = QtGui.QLabel("Мин.")
		self.maxLabel = QtGui.QLabel("Макс.")
		self.xLabel = QtGui.QLabel("X")
		self.yLabel = QtGui.QLabel("Y")
		self.zLabel = QtGui.QLabel("Z")

		self.xminLineEdit = QtGui.QLineEdit(self.boundariesGroupBox)
		self.xmaxLineEdit = QtGui.QLineEdit(self.boundariesGroupBox)
		self.yminLineEdit = QtGui.QLineEdit(self.boundariesGroupBox)
		self.ymaxLineEdit = QtGui.QLineEdit(self.boundariesGroupBox)
		self.zminLineEdit = QtGui.QLineEdit(self.boundariesGroupBox)
		self.zmaxLineEdit = QtGui.QLineEdit(self.boundariesGroupBox)

		self.xminLineEdit.setReadOnly(True)
		self.xmaxLineEdit.setReadOnly(True)
		self.yminLineEdit.setReadOnly(True)
		self.ymaxLineEdit.setReadOnly(True)
		self.zminLineEdit.setReadOnly(True)
		self.zmaxLineEdit.setReadOnly(True)


		#Общие кнопки диалогового окна
		self.buttonBoxGeneral = QtGui.QDialogButtonBox(self)
		self.buttonBoxGeneral.addButton(QtGui.QDialogButtonBox.Ok)
		self.buttonBoxGeneral.addButton(QtGui.QDialogButtonBox.RestoreDefaults)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(150)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).setText("Применить")
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.RestoreDefaults).setMinimumWidth(150)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.RestoreDefaults).setText("Сбросить")

			
	def __gridWidgets(self):
		self.setLayout(self.mainLayout)		 
		
		self.mainLayout.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelFile)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineFile)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelShape)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineShape)

		self.mainLayout.addWidget(self.filterModeGroupBox)	  

		self.filterModeGroupBox.setLayout(self.filterModeLayout)
		self.filterModeLayout.addWidget(self.filterModePlan)
		self.filterModeLayout.addWidget(self.filterModeSolid)
		self.filterModeLayout.addWidget(self.ExtensionLabel)
		self.filterModeLayout.addWidget(self.ExtensionSpinBox)
		
		self.mainLayout.addWidget(self.boundariesGroupBox)
		self.boundariesGroupBox.setLayout(self.boundariesLayout)
		self.boundariesLayout.addWidget(self.minLabel,0,1)
		self.boundariesLayout.addWidget(self.maxLabel,0,2)
		self.boundariesLayout.addWidget(self.zLabel,3,0)
		self.boundariesLayout.addWidget(self.xLabel,1,0)
		self.boundariesLayout.addWidget(self.yLabel,2,0)
		self.boundariesLayout.addWidget(self.zLabel,3,0)

		self.boundariesLayout.addWidget(self.xminLineEdit,1,1) 
		self.boundariesLayout.addWidget(self.xmaxLineEdit,1,2) 
		self.boundariesLayout.addWidget(self.yminLineEdit,2,1)
		self.boundariesLayout.addWidget(self.ymaxLineEdit,2,2)
		self.boundariesLayout.addWidget(self.zminLineEdit,3,1)
		self.boundariesLayout.addWidget(self.zmaxLineEdit,3,2)

		self.mainLayout.addWidget(self.buttonBoxGeneral)

	def __createBindings(self):
		self.lineFile.mouseDoubleClickEvent = lambda e: self.select_file(e, self.lineFile)
		self.lineShape.mouseDoubleClickEvent = lambda e: self.select_shape(e, self.lineFile, self.lineShape)
		self.filterModePlan.toggled.connect(self.filterModePlan_changeEvent)
		self.filterModeSolid.toggled.connect(self.filterModeSolid_changeEvent)
		self.ExtensionSpinBox.valueChanged.connect(self.on_ExtensionSpinBox_valueChanged)

		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.button_ok_clicked)
		self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.RestoreDefaults).clicked.connect(self.button_restore_defaults_clicked)

	def __initForm(self):
		self.lineFile.setText("")
		self.lineShape.setText("")
		self.tittle = "<не определено>"
		self.filterModePlan.setChecked(True)
		self.ExtensionSpinBox.setValue(0)
		self._xmin = None
		self._xmax = None
		self._ymin = None
		self._ymax = None
		self._zmin = None
		self._zmax = None
		self._coords=(self._xmin, self._xmax, self._ymin, self._ymax, self._zmin, self._zmax)
		self.xminLineEdit.setText(str(self.xmin if self.xmin else ""))
		self.xmaxLineEdit.setText(str(self.xmax if self.xmax else ""))
		self.yminLineEdit.setText(str(self.ymin if self.ymin else ""))
		self.ymaxLineEdit.setText(str(self.ymax if self.ymax else ""))
		self.zminLineEdit.setText(str(self.zmin if self.zmin else ""))
		self.zmaxLineEdit.setText(str(self.zmax if self.zmax else ""))
		self.__setButtonBoxGeneralState()

	def select_file(self, e, widget):
		filetypes = "ЛИНИИ (*.STR);;КАРКАСЫ (*.TRIDB)"
		try:		initialdir = MMpy.Project.path()
		except:		initialdir = os.getcwd()

		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		self._parent.menu.show()
		# ничего не выбрано
		if not path:
			return

		widget.setText(path)

	def select_shape(self, e, filewidget, widget):
		if filewidget == self.lineFile:
			filepath = self.lineFile.text()
		if not os.path.exists(filepath):
			qtmsgbox.show_error(self, "Ошибка", "Файл '%s' не существует" % filepath)
			return
		if os.path.splitext(filepath)[1].upper() == ".TRIDB":
			try:
				tridb = Tridb(filepath)
				wireframe_names = tridb.wireframe_names
				tridb.close()
			except Exception as e:
				qtmsgbox.show_error(self, "Ошибка", "Невозможно считать список каркасов. %s" % str(e))
				return

			dialog = SelectFromListDialog(self)
			dialog.setValues(wireframe_names)
			dialog.setWindowTitle("Выберите каркас")
			dialog.labelTitle.setText("Выберите каркас")
			dialog.exec()
			self._parent.menu.show()
			result = dialog.getValues()
			if result:
				widget.setText(result[0])
				self.tittle = result[0]
				self.__get_xxx_extents =  self.__get_wireframes_extents
				self.fill_extents (self.__get_xxx_extents)

		elif os.path.splitext(filepath)[1].upper() == ".STR":
			strfile = StringsFile(filepath)
			strings = strfile.getStrings()
			dialog = SelectFromListDialog(self)
			dialog.setValues(strings)
			dialog.setWindowTitle("Выберите стринг")
			dialog.labelTitle.setText("Выберите стринг")
			dialog.exec()
			self._parent.menu.show()
			result = dialog.getValues()
			if result:
				widget.setText(result[0])
				self.tittle = result[0]
				self.__get_xxx_extents = self.__get_string_extents
				self.fill_extents (self.__get_xxx_extents)

	def filterModePlan_changeEvent(self):

		self.fill_extents (self.__get_xxx_extents)

	def filterModeSolid_changeEvent(self):

		self.fill_extents (self.__get_xxx_extents)

	def on_ExtensionSpinBox_valueChanged(self):
		self.fill_extents (self.__get_xxx_extents)

	def button_ok_clicked(self):
		self._parent.menu.close()

		self.filterApplied.emit(self)

	def button_restore_defaults_clicked(self):
		self.__initForm()
		self._parent.menu.close()

		self.filterDefaultRestored.emit(self)

	def __get_wireframes_extents (self):
		_fileName = self.lineFile.text()
		_shapeName = self.lineShape.text()
		if not os.path.exists(_fileName):
			return {"xmin": None, "xmax": None, "ymin": None, "ymax": None, "zmin": None, "zmax": None}
		wfextents = get_wireframes_extents(_fileName, [_shapeName])
		return wfextents
	def __get_string_extents (self):
		_fileName = self.lineFile.text()
		_shapeName = self.lineShape.text()
		if not os.path.exists(_fileName):
			return {"xmin": None, "xmax": None, "ymin": None, "ymax": None, "zmin": None, "zmax": None}

		strfile = StringsFile(_fileName)
		strextents = strfile.getExtents([_shapeName])
		return strextents

	def fill_extents(self, func, *args, **kw):
		extents = func( *args, **kw)
		self._xmin = extents['xmin']
		self._xmax = extents['xmax']
		self._ymin = extents['ymin']
		self._ymax = extents['ymax']
		self._zmin = extents['zmin']
		self._zmax = extents['zmax']
		self._coords=(self._xmin, self._xmax, self._ymin, self._ymax, self._zmin, self._zmax)
		self.xminLineEdit.setText(str(self.xmin if self.xmin else ""))
		self.xmaxLineEdit.setText(str(self.xmax if self.xmax else ""))
		self.yminLineEdit.setText(str(self.ymin if self.ymin else ""))
		self.ymaxLineEdit.setText(str(self.ymax if self.ymax else ""))
		self.zminLineEdit.setText(str(self.zmin if self.zmin else ""))
		self.zmaxLineEdit.setText(str(self.zmax if self.zmax else ""))
		self.__setButtonBoxGeneralState()

	def __setButtonBoxGeneralState(self):

		if any(self._coords):
			self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).setEnabled(True)
			self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.RestoreDefaults).setEnabled(True)
		else:
			self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.Ok).setEnabled(False)
			self.buttonBoxGeneral.button(QtGui.QDialogButtonBox.RestoreDefaults).setEnabled(False)

	def __getExtension(self):
		return float(self.ExtensionSpinBox.value())

	@property
	def xmin(self):
		result = self._xmin
		if not self._xmin is None: 
			result -= self.__getExtension()
		return result 
	@property
	def xmax(self):
		result = self._xmax
		if not self._xmax is None: 
			result += self.__getExtension()
		return result
	@property
	def ymin(self):
		result = self._ymin
		if not self._ymin is None: 
			result -= self.__getExtension()
		return result
	@property
	def ymax(self):
		result = self._ymax
		if not self._ymax is None: 
			result += self.__getExtension()
		return result
	@property
	def zmin(self):
		if self.filterModePlan.isChecked():
			return None
		else:
			result = self._zmin
			if not self._zmin is None: 
				result -= self.__getExtension()
		return result
	@property
	def zmax(self):
		if self.filterModePlan.isChecked():
			return None
		else:
			result = self._zmax
			if not self._zmax is None: 
				result += self.__getExtension()
		return result

class StringsFile():
	def __init__(self, path):
		if not os.path.exists(path):
			raise Exception("Файл не существует. %s" % path)
		self._path =  path


	def getStrings(self):
		f = MicromineFile()
		if not f.open(self._path):			
			raise Exception("Невозможно открыть файл %s" % srcpath)
		try:							
			source = f.read(columns = ["STRING", "JOIN"], asstr = True)
		except Exception as e:		
			raise Exception("Невозможно считать файл %s. %s" % (srcpath, str(e)))
		finally:
			f.close()


		lstStrings = set([':'.join(i) for i in source])

		return lstStrings

	def getExtents(self,stringsList):
		strings = [ i.split(':') for i in stringsList]

		f = MicromineFile()
		if not f.open(self._path):			
			raise Exception("Невозможно открыть файл %s" % srcpath)
		try:							
			source = f.read(columns = ["STRING", "JOIN", "EAST", "NORTH", "RL"], asstr = True)
		except Exception as e:		
			raise Exception("Невозможно считать файл %s. %s" % (srcpath, str(e)))
		finally:
			f.close()

		filtred_source = list(filter(lambda x: x[0]+':'+x[1] in stringsList ,source))

		zipped_filtred_source = list(zip(*filtred_source))

		result = {	"xmin": min(list(map(float,zipped_filtred_source[2]))),
					"xmax": max(list(map(float,zipped_filtred_source[2]))),
					"ymin": min(list(map(float,zipped_filtred_source[3]))),
					"ymax": max(list(map(float,zipped_filtred_source[3]))),
					"zmin": min(list(map(float,zipped_filtred_source[4]))),
					"zmax": max(list(map(float,zipped_filtred_source[4])))
				}



		return result

	def getDataHeader(self):   
		f = MicromineFile()
		if not f.open(self._path):			
			raise Exception("Невозможно открыть файл %s" % srcpath)
		try:							
			header = f.header
		except Exception as e:		
			raise Exception("Невозможно считать файл %s. %s" % (srcpath, str(e)))
		finally:
			f.close() 

		return header

	def getData(self, stringsList):
		strings = [ i.split(':') for i in stringsList]   
		f = MicromineFile()
		if not f.open(self._path):			
			raise Exception("Невозможно открыть файл %s" % srcpath)
		try:	
			header = f.header						
			source = f.read(header, asstr = True)

		except Exception as e:		
			raise Exception("Невозможно считать файл %s. %s" % (srcpath, str(e)))
		finally:
			f.close()

				 
		if "STRING" not in header:
			raise Exception("Поле STRING не найденно в файле линий")
		if "JOIN" not in header:
			raise Exception("Поле JOIN не найденно в файле линий")

		idx_string = header.index("STRING")
		idx_join = header.index("JOIN")

		if len(stringsList) > 0:
			filtred_source = list(filter(lambda x: x[idx_string]+':'+x[idx_join] in stringsList ,source))
		else:
			filtred_source = source



		return filtred_source



