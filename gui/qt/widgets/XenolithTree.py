from PyQt4 import QtGui, QtCore
import os

from utils.constants import *
from mm.mmutils import Tridb

from gui.qt.widgets.TreeModel import TreeWidget
from gui.qt.widgets.TreeModelItem import createTreeModelItem
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class XenolithTreeWidget(TreeWidget):
	def __init__(self, *args, **kw):
		super(XenolithTreeWidget, self).__init__(*args, **kw)

		self.headerItem().setText(0, "Наименование")
		self.setColumnWidth(0, 200)

	def setValues(self, filenames):
		for path in filenames:
			try:		tridb = Tridb(path)
			except:		continue
			#
			basename = os.path.basename(path)
			basename = basename[:basename.rfind(".")] if "." in basename else basename
			#
			wireframe_names = tridb.wireframe_names
			tridb.close()
			if wireframe_names:
				tridbItem = createTreeModelItem(self, basename, ITEM_TRIDB, ITEM_CATEGORY_GEO)
				tridbItem.setFilePath(path)
				for name in wireframe_names:
					wfItem = createTreeModelItem(tridbItem, name, ITEM_WIREFRAME, ITEM_CATEGORY_GEO)
					wfItem.setCheckState(0, QtCore.Qt.Unchecked)
					wfItem.setFilePath(path)

	def values(self):
		values = []
		for wfItem in filter(lambda it: it.isWireframe(), self.iterChildren()):
			if wfItem.checkState(0) == QtCore.Qt.Checked:
				values.append([wfItem.filePath(), wfItem.name()])
		return values