from PyQt4 import QtCore, QtGui
import os, math, csv
from copy import deepcopy
from itertools import groupby

nMineDir = "D:\\NN\\N-Mine"
os.chdir(nMineDir)
os.sys.path.append(nMineDir)

from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog

from gui.qt.widgets.ListTreeItem import createListTreeItem
from gui.qt.widgets.TableDataView import CustomDataView, CustomDataModel
from gui.qt.widgets.TreeModel import TreeWidget
from utils.constants import *
# from mm.mmutils import MicromineFile
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


def isBlank(value):
	return value.replace(" ", "") == ""

sep = ", "
csvBmPath = "D:\\BM"
csvBmPath = "D:\\log.txt"
floatIndexes = list(range(6)) + list(range(12, 27))
bmData = []
with open(csvBmPath) as fbm:
	bmHeader = fbm.readline().strip().split(sep)
	for k, row in enumerate(fbm):
		bmRow = []
		for i, value in enumerate(row.strip().split(sep)):
			value = value.replace("'","")
			if i in floatIndexes:
				value = math.nan if isBlank(value) else float(value)
			bmRow.append(value)
		bmData.append(bmRow)
		if k > 100000:
			break



fontBold = QtGui.QFont()
fontBold.setBold(True)
fontItalic = QtGui.QFont()
fontItalic.setItalic(True)

RPTFLD_VOLUME = "ОБЪЕМ"
RPTFLD_FACT_TONNAGE = "ТОННАЖ"
RPTFLD_FACT_DENSITY = "ПЛОТНОСТЬ"
RPTFLD_MINE_TONNAGE = "ВЫЕМ_ТОННАЖ"
RPTFLD_MINE_DENSITY = "ВЫЕМ_ПЛОТНОСТЬ"

SVY_MINEWORK_CATEGORIES = ["ПРОХ", "КК", "ОР"]
SVY_BACKFIELD_CATEGORY = "ЗАКЛ"
SVY_OPERATION_CATEGORY = "ОПЕР"
SVY_MINING_CATEGORIES = SVY_MINEWORK_CATEGORIES + [SVY_BACKFIELD_CATEGORY, SVY_OPERATION_CATEGORY]

BMVW_ITEM_MATERIAL = "BMVW_ITEM_MATERIAL"
BMVW_ITEM_PURPOSE = "BMVW_ITEM_PURPOSE"
BMVW_ITEM_MINEWORK = "BMVW_ITEM_MINEWORK"
BMVW_ITEM_SUMMARY = "BMVW_ITEM_SUMMARY"
BMVW_ITEM_TYPES = (BMVW_ITEM_MATERIAL, BMVW_ITEM_PURPOSE, BMVW_ITEM_MINEWORK, BMVW_ITEM_SUMMARY)

# class MicromineFile(MMpy.File):
class MicromineFile():
	@staticmethod
	def determine_structure(data, header, nrows = 100):
		if len(data) < nrows:
			nrows = len(data)
		coldata = tuple(zip(*data))
		if len(coldata) != len(header):
			raise Exception("Header and data have different number of columns")
		structure = MMpy.FileStruct()
		for k, cdata in enumerate(coldata):
			# determine field type
			ctype = "R"
			for i in range(nrows):
				if type(cdata[i]) == str:
					ctype = "C"
					break
			# convert column data to string
			sdata = map(str, cdata)
			# determine field width if character
			if ctype == "C":
				w = max(map(len, sdata))
				if w == 0:		w = 1
				if w > 255:		w = 255
				structure.add_field(header[k], MMpy.FieldType.character, w, 0)
			# determine field precision otherwise
			else:
				p = max(map(lambda x: len(x[x.rfind(".")+1:]) if "." in x else 0, sdata))
				structure.add_field(header[k], MMpy.FieldType.real, 0, p)
		return structure
	@staticmethod
	def to_filestruct(s):
		types = {ft[0]: MMpy.FieldType.names[ft] for ft in MMpy.FieldType.names}
		structure = MMpy.FileStruct()
		for fld in s:
			name, ft, width, prec = fld.split(":")
			structure.add_field(name, types[ft.lower()], int(width), int(prec))
		return structure
	@staticmethod
	def from_filestruct(s):
		structure = []
		for i in range(s.fields_count):
			n = s.get_field_name(i)
			t = str(s.get_field_type(i))[0].upper()
			w = s.get_field_width(i)
			p = s.get_field_precision(i)
			structure.append("%s:%s:%d:%d" % (n, t, w, p))
		return structure
	@staticmethod
	def create(path, structure):		
		s = structure if type(structure) == MMpy.FileStruct else MicromineFile.to_filestruct(structure)
		return MMpy.File.create_from_template(path, "", s)
	@property
	def header(self):
		s = self.structure
		return [s.get_field_name(i) for i in range(s.fields_count)]
	def read(self, columns = None, asstr = False):
		if columns is None:
			columns = self.header
		#
		colIndexes = [self.get_field_id(col) for col in columns]
		fldTypes = [self.structure.get_field_type(colIndex) for colIndex in colIndexes]
		#
		getNum, getStr = self.get_num_field_value, self.get_str_field_value
		#
		ret = []
		if asstr:
			for iRow in range(self.records_count):
				ret.append([getStr(iCol, iRow+1) for iCol in colIndexes])
		else:
			for iRow in range(self.records_count):
				ret.append([getStr(iCol, iRow+1) if fldTypes[iCol] == MMpy.FieldType.character else getNum(iCol, iRow+1) for iCol in colIndexes])
		return ret
	@staticmethod
	def write(path, data, header = None, structure = None):
		if header is None and structure and None:
			raise Exception("Необходимо задать либо заголовок файла, либо его структуру")

		if header is not None:
			if structure is None:
				structure = MicromineFile.determine_structure(data, header)
			elif type(structure) != MMpy.FileStruct:
				structure = MicromineFile.to_filestruct(structure)
				header = [structure.get_field_name(i) for i in range(structure.fields_count)]
		elif type(structure) != MMpy.FileStruct:
			structure = MicromineFile.to_filestruct(structure)
			header = [structure.get_field_name(i) for i in range(structure.fields_count)]
		else:
			header = [structure.get_field_name(i) for i in range(structure.fields_count)]


		nrows, ncols = len(data), len(header)

		if os.path.exists(path):
			try:					os.remove(path)
			except Exception as e:	raise Exception("Невозможно удалить файл '%s'. %s" % (path, str(e)))
	
		f = MMpy.File()
		if not os.path.exists(path):
			if not MicromineFile.create(path, structure):
				raise Exception("Couldn't create the file %s" % path)
			if not f.open(path):
				raise Exception("Couldn't open the file %s" % path)
		else:
			# удаление данных из файла
			f = MMpy.File()
			if not f.open(path):
				raise Exception("Couldn't open the file %s" % path)
			for i in range(f.records_count):
				f.delete_record(1)
			# добавление полей
			f.structure = structure
		# запись данных
		for i in range(nrows):
			f.add_record()
			for j in range(ncols):
				value = data[i][j] if data[i][j] is not None else ""
				f.set_field_value(j, f.records_count, value)
		f.close()

class CheckBoxTreeItem(QtGui.QWidget):
	def __init__(self, item, column):
		super(CheckBoxTreeItem, self).__init__()

		self._item = item
		self._item.setTextAlignment(column, QtCore.Qt.AlignCenter)
		self._column = column

		self._checkBox = QtGui.QCheckBox()
		self._checkBox.stateChanged.connect(self._stateChanged)

		layout = QtGui.QHBoxLayout()
		layout.addWidget(self._checkBox)
		layout.setAlignment(QtCore.Qt.AlignCenter)
		layout.setContentsMargins(0,0,0,0)
		layout.setSpacing(0)
		layout.setMargin(0)

		self.setLayout(layout)
	def checkBox(self):		return self._checkBox
	def item(self):		return self._item
	def _stateChanged(self, state):
		tree = self._item.treeWidget()
		self._item.setText(self._column, str(state))
		selectedItems = tree.selectedItems()
		if not self._item.isSelected() or not selectedItems or len(selectedItems) == 1:
			tree.clearSelection()
			tree.setItemSelected(self._item, True)
		tree.setFocus()

class ComboBoxTreeItem(QtGui.QComboBox):
	def __init__(self, item, column):
		super(ComboBoxTreeItem, self).__init__()
		self._item = item
		self._column = column

		self.__createBindings()

	def __createBindings(self):
		self.editTextChanged.connect(self._textChanged)
		self.currentIndexChanged.connect(self._indexChanged)

	def _indexChanged(self, index):
		self._item.treeWidget().blockSignals(True)
		self._item.setText(self._column, self.currentText())
		self._item.treeWidget().blockSignals(False)
	def _textChanged(self, text):
		self._item.treeWidget().blockSignals(True)
		self._item.setText(self._column, text)
		self._item.treeWidget().blockSignals(False)
	def column(self):
		return self._column

class CustomTreeWidgetItem(QtGui.QTreeWidgetItem):
	def __init__(self, *args, **kw):
		super(CustomTreeWidgetItem, self).__init__(*args, **kw)
		self._itemWidgets = {}

	def setCheckBoxToColumn(self, idxColumn):
		checkBoxItemWidget = CheckBoxTreeItem(self, idxColumn)
		self.treeWidget().setItemWidget(self, idxColumn, checkBoxItemWidget)
		return checkBoxItemWidget.checkBox()
	def checkBox(self, idxColumn):
		return self.treeWidget().itemWidget(self, idxColumn).checkBox()

	def setComboBoxToColumn(self, idxColumn):
		comboBox = ComboBoxTreeItem(self, idxColumn)	
		self.treeWidget().setItemWidget(self, idxColumn, comboBox)
	def comboBox(self, idxColumn):
		return self.treeWidget().itemWidget(self, idxColumn)

	def iterParents(self):
		ret = [self.parent()]
		if ret[-1]:
			ret.extend(ret[-1].iterParents())
		return ret
	def iterChildren(self):
		ret = []
		for i in range(self.childCount()):
			child = self.child(i)
			ret.append(child)
			if child.childCount():
				ret.extend(child.iterChildren())
		return ret

	def removeAllChildren(self):
		for i in range(self.childCount()):
			self.removeChild(self.child(0))


class BmViewItem(CustomTreeWidgetItem):
	#
	def __init__(self, parent, itemType, category):
		super(BmViewItem, self).__init__(parent)
		if not itemType in BMVW_ITEM_TYPES:
			raise Exception("%s - неверный тип элемента дерева" % itemType)
		#
		self._font = QtGui.QFont()
		self._itemType = itemType
		self._category = category
		#
		self._key = ()					# список ключевых значений, по которым произошла группировка
		self._reportData = []
		#
		self._volume = None
		self._tonnage = None
		#
		self._buildOptionColumns()
		self._setSummaryColumnProperties()

	def _formatSummary(self, number, decimals = 1):
		number = round(number, decimals)
		return "{:,}".format(number).replace(",", " ")
	def _buildOptionColumns(self):
		includeInReportColumnIndex = self.treeWidget().includeInReportColumnIndex()
		restoreMiningWorkColumnIndex = self.treeWidget().restoreMiningWorkColumnIndex()
		restoreGeologyColumnIndex = self.treeWidget().restoreGeologyColumnIndex()

		includeInReportCheckBox = self.setCheckBoxToColumn(includeInReportColumnIndex)
		restoreMiningCheckBox = self.setCheckBoxToColumn(restoreMiningWorkColumnIndex)
		restoreMiningCheckBox.setDisabled(True)
		restoreGeologyCheckBox = self.setCheckBoxToColumn(restoreGeologyColumnIndex)
		restoreGeologyCheckBox.setDisabled(True)

		includeInReportCheckBox.stateChanged.connect(lambda: self.optionCheckBoxStateChanged(includeInReportColumnIndex))
		restoreMiningCheckBox.stateChanged.connect(lambda: self.optionCheckBoxStateChanged(restoreMiningWorkColumnIndex))
		restoreGeologyCheckBox.stateChanged.connect(lambda: self.optionCheckBoxStateChanged(restoreGeologyColumnIndex))

		if self.category() not in SVY_MINING_CATEGORIES:
			restoreGeologyCheckBox.setVisible(False)

		if not self.category() == SVY_BACKFIELD_CATEGORY:
			restoreMiningCheckBox.setVisible(False)

	def _setSummaryColumnProperties(self):
		for summaryColumnIndex in self.treeWidget().summaryColumnIndexes():
			self.setTextAlignment(summaryColumnIndex, QtCore.Qt.AlignRight)

	def key(self):					return self._key[1:]
	#
	def isPurposeItem(self):		return self._itemType == BMVW_ITEM_PURPOSE
	def isMineWorkItem(self):		return self._itemType == BMVW_ITEM_MINEWORK
	def isMaterialItem(self):		return self._itemType == BMVW_ITEM_MATERIAL
	def isSummaryItem(self):		return self._itemType == BMVW_ITEM_SUMMARY
	#
	def itemType(self):				return self._itemType
	#
	def isIncluded(self):
		includeInReportColumnIndex = self.treeWidget().includeInReportColumnIndex()
		return self.checkBox(includeInReportColumnIndex).isChecked()
	def isExcluded(self):
		includeInReportColumnIndex = self.treeWidget().includeInReportColumnIndex()
		return not self.checkBox(includeInReportColumnIndex).isChecked()
	def isRestoreMiningWork(self):
		restoreMiningWorkColumnIndex = self.treeWidget().restoreMiningWorkColumnIndex()
		return self.checkBox(restoreMiningWorkColumnIndex).isChecked()
	def isRestoreGeology(self):
		restoreGeologyColumnIndex = self.treeWidget().restoreGeologyColumnIndex()
		return self.checkBox(restoreGeologyColumnIndex).isChecked()
	#
	def category(self):
		return self._category
	
	def _updateKey(self):
		return
	#
	def _populate(self, report):
		self.removeAllChildren()

	def setText(self, iColumn, text):
		super(BmViewItem, self).setText(iColumn, text)
		self.setFont(iColumn, self._font)

	def setReport(self, key, report):
		self._key = deepcopy(key)
		self._reportData = deepcopy(report)
		self._updateKey()
		self._populate(report)
		#
		self.updateSummary()

	def volume(self):
		tree = self.treeWidget()
		childCount = self.childCount()
		vol = 0
		if childCount == 0:
			idxVolume = self.treeWidget().reportHeader().index(RPTFLD_VOLUME)
			vol = sum(row[idxVolume] for row in self._reportData)
		else:
			if self.isPurposeItem():
				for materialItem in filter(lambda it: it.isMaterialItem(), self.iterChildren()):
					childCheckBox = materialItem.checkBox(tree.includeInReportColumnIndex())
					isChecked = childCheckBox.checkState() in [QtCore.Qt.Checked]
					if isChecked:
						vol += materialItem.volume()
			else:
				vol = sum(self.child(i).volume() for i in range(childCount))
		return vol
	def tonnage(self):
		tree = self.treeWidget()
		childCount = self.childCount()
		if childCount == 0:
			checkBox = self.checkBox(tree.restoreMiningWorkColumnIndex())
			if not checkBox.isChecked():	idxTonnage = tree.reportHeader().index(RPTFLD_FACT_TONNAGE)
			else:							idxTonnage = tree.reportHeader().index(RPTFLD_MINE_TONNAGE)
			#
			ton = sum(row[idxTonnage] for row in self._reportData)
		else:
			ton = 0
			if self.isPurposeItem():
				for materialItem in filter(lambda it: it.isMaterialItem(), self.iterChildren()):
					childCheckBox = materialItem.checkBox(tree.includeInReportColumnIndex())
					isChecked = childCheckBox.checkState() in [QtCore.Qt.Checked]
					if isChecked:
						ton += materialItem.tonnage()
			else:
				ton = sum(self.child(i).tonnage() for i in range(childCount))
		return ton

	def updateVolume(self):
		volume = self.volume()
		fmtValue = self._formatSummary(volume)
		self.setText(self.treeWidget().volumeColumnIndex(), fmtValue)
	def updateTonnage(self):
		tonnage = self.tonnage()
		fmtValue = self._formatSummary(tonnage)
		self.setText(self.treeWidget().tonnageColumnIndex(), fmtValue)
	def updateSummary(self):
		self.updateVolume()
		self.updateTonnage()

	def reportData(self):
		childCount = self.childCount()
		ret = []
		if childCount == 0:		ret = deepcopy(self._reportData)
		else:
			for i in range(childCount):
				ret.extend(self.child(i).reportData())
		return ret

	def setDefaultCheckBoxStates(self, column):
		tree = self.treeWidget()
		checkBox = self.checkBox(column)
		isChecked = checkBox.checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]

		checkBox.blockSignals(True)
		checkBox.setChecked(isChecked)
		checkBox.blockSignals(False)

		# enable/disable other checkboxes for current item
		if column == tree.includeInReportColumnIndex():
			for optionColumnIndex in tree.optionColumnIndexes()[1:]:
				self.checkBox(optionColumnIndex).setDisabled(not isChecked)

		# setting child checkbox state
		for childItem in self.iterChildren():
			childCheckBox = childItem.checkBox(column)
			#
			childCheckBox.blockSignals(True)
			childCheckBox.setChecked(isChecked)
			childCheckBox.blockSignals(False)
			# enable/disable other checkboxes for child item
			isChildChecked = childCheckBox.checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]
			if column == tree.includeInReportColumnIndex():
				for optionColumnIndex in tree.optionColumnIndexes()[1:]:
					childItem.checkBox(optionColumnIndex).setDisabled(not isChildChecked)

		# setting parent checkbox state
		for parentItem in self.iterParents()[:-1]:
			parentCheckBox = parentItem.checkBox(column)
			#
			parentCheckBox.blockSignals(True)
			childrenAreChecked = [it.checkBox(column).isChecked() for it in parentItem.iterChildren()]
			if all(childrenAreChecked):		parentCheckBox.setCheckState(QtCore.Qt.Checked)
			elif any(childrenAreChecked):	parentCheckBox.setCheckState(QtCore.Qt.PartiallyChecked)
			else:							parentCheckBox.setCheckState(QtCore.Qt.Unchecked)
			parentCheckBox.blockSignals(False)
			# enable/disable other checkboxes for parent item
			isParentChecked = parentCheckBox.checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]
			if column == tree.includeInReportColumnIndex():
				for optionColumnIndex in tree.optionColumnIndexes()[1:]:
					parentItem.checkBox(optionColumnIndex).setDisabled(not isParentChecked)

		# updating purpose summary
		purposeItem = self if self.isPurposeItem() else self.iterParents()[-2]
		purposeItem.updateSummary()
		# updating total summary
		summaryItem = tree.summaryItem()
		summaryItem.updateSummary()
	#
	def setSelectedCheckBoxStates(self, column):
		tree = self.treeWidget()
		selectedItems = tree.selectedItems()
		#
		isChecked = self.checkBox(column).checkState() in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]
		itemType = self.itemType()
		#
		items = list(filter(lambda it: it.itemType() == itemType, selectedItems))
		for item in items:
			checkBox = item.checkBox(column)
			#
			checkBox.blockSignals(True)
			checkBox.setChecked(isChecked)
			checkBox.blockSignals(False)
			#
			item.setDefaultCheckBoxStates(column)

	def optionCheckBoxStateChanged(self, column):
		tree = self.treeWidget()
		selectedItems = tree.selectedItems()
		#
		if len(selectedItems) == 1:		self.setDefaultCheckBoxStates(column)
		else:							self.setSelectedCheckBoxStates(column)

class PurposeTreeWidgetItem(BmViewItem):
	def __init__(self, parent, category):
		self._category = category
		super(PurposeTreeWidgetItem, self).__init__(parent, BMVW_ITEM_PURPOSE, category)
		self._font.setBold(True)
	def key(self):
		return None
	def setReport(self, key, report):
		self._category = key
		super(PurposeTreeWidgetItem, self).setReport(key, report)
	def _populate(self, report):
		super(PurposeTreeWidgetItem, self)._populate(report)
		#
		keyIndexes = tuple(self.treeWidget().reportHeader().index(field) for field in self.groupFields())
		for key, values in groupby(report, lambda x: tuple(x[keyIndex] for keyIndex in keyIndexes)):
			values = list(values)
			#
			if self.category() in SVY_MINING_CATEGORIES:
				mineWorkItem = MineWorkTreeWidgetItem(self, self.category())
				mineWorkItem.setReport(key, values)
			else:
				mineWorkGroupFields = self.treeWidget().mineWorkGroupFields()
				keyIndexes = tuple(self.treeWidget().reportHeader().index(field) for field in mineWorkGroupFields)
				for materialKey, materialValues in groupby(values, lambda x: tuple(x[keyIndex] for keyIndex in keyIndexes)):
					materialItem = MaterialTreeWidgetItem(self, self.category())
					materialItem.setReport(materialKey, tuple(materialValues))
		self.setExpanded(True)
	def _updateKey(self):
		category = self._key
		purpose = self.treeWidget().catPurposeDict().get(category)
		displayKey = purpose if purpose else category
		self.setText(0, displayKey)
	def groupFields(self):
		return self.treeWidget().purposeGroupFields()
		
class MineWorkTreeWidgetItem(BmViewItem):
	def __init__(self, parent, category):
		super(MineWorkTreeWidgetItem, self).__init__(parent, BMVW_ITEM_MINEWORK, category)
	
	def category(self):
		return self.parent().category()
	def _populate(self, report):
		super(MineWorkTreeWidgetItem, self)._populate(report)
		#
		keyIndexes = tuple(self.treeWidget().reportHeader().index(field) for field in self.groupFields())
		for key, values in groupby(report, lambda x: tuple(x[keyIndex] for keyIndex in keyIndexes)):
			materialItem = MaterialTreeWidgetItem(self, self.category())
			materialItem.setReport(key, tuple(values))
	def _updateKey(self):
		idxCategory = self.treeWidget().reportHeader().index(self.treeWidget().currDateField())
		idxFactWork = self.treeWidget().reportHeader().index(BMFLD_FACTWORK)

		category, code = self._key[0], self._key[-1]
		codeDescription = self.treeWidget().catCodeDescriptionDict().get((category, code))
		displayCodeDescription = codeDescription if codeDescription else code
		displayKeys = tuple(self._key)[1:5] + (displayCodeDescription,)
		#
		for i, value in enumerate(displayKeys):
			if i == 3 and self.category() != SVY_BACKFIELD_CATEGORY:
				continue
			self.setText(i, value)
	def groupFields(self):
		return self.treeWidget().mineWorkGroupFields()

class MaterialTreeWidgetItem(BmViewItem):
	def __init__(self, parent, category):
		super(MaterialTreeWidgetItem, self).__init__(parent, BMVW_ITEM_MATERIAL, category)
		
		#
		self._font.setItalic(True)
	def category(self):
		return self.parent().category()
	# def isIncluded(self):
	# 	checkBox = self.checkBox(self.treeWidget().includeInReportColumnIndex())
	# 	return checkBox.checkState() == QtCore.Qt.Checked
	# def isExcluded(self):
	# 	checkBox = self.checkBox(self.treeWidget().includeInReportColumnIndex())
	# 	return checkBox.checkState() == QtCore.Qt.Unchecked
	def setText(self, iColumn, text):
		super(BmViewItem, self).setText(iColumn, text)
		if iColumn == self.treeWidget().volumeColumnIndex()-1:
			self.setTextAlignment(iColumn, QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.setFont(iColumn, self._font)
	def setReport(self, key, report):
		# заменяем Руда/Порода на исходную категорию
		idxCategory = self.treeWidget().reportHeader().index(self.treeWidget().currDateField())
		idxMaterial = self.treeWidget().reportHeader().index(BMFLD_MATERIAL)
		self._reportData = [list(row) for row in report]
		for i, row in enumerate(self._reportData):
			material = row[idxMaterial]
			category = row[idxCategory]
			if not material in SVY_MINING_CATEGORIES and category != material:
				self._reportData[i][idxCategory] = material
		#
		super(MaterialTreeWidgetItem, self).setReport(key, report)
	def _updateKey(self):
		material = self._key[-1]
		materialDescription = self.treeWidget().materialDescriptionDict().get(material)
		displayKey = materialDescription if materialDescription else material
		self.setText(len(self._key)-3, displayKey)	

class SummaryTreeWidgetItem(BmViewItem):
	def __init__(self, parent):
		super(SummaryTreeWidgetItem, self).__init__(parent, BMVW_ITEM_SUMMARY, None)
		self._font.setBold(True)
		self._font.setItalic(True)
	
		tree = self.treeWidget()
		for optionColumnIndex in tree.optionColumnIndexes():
			self.checkBox(optionColumnIndex).setVisible(False)
		#
		self.setTextAlignment(tree.volumeColumnIndex()-1, QtCore.Qt.AlignRight)
		self.setText(tree.volumeColumnIndex()-1, "Итого")
		self.updateSummary()

	def updateVolume(self):
		tree = self.treeWidget()
		volume = sum(purposeItem.volume() for purposeItem in filter(lambda it: it.isPurposeItem(), tree.iterChildren()))
		fmtValue = self._formatSummary(volume)
		self.setText(self.treeWidget().volumeColumnIndex(), fmtValue)

	def updateTonnage(self):
		tree = self.treeWidget()
		tonnage = sum(purposeItem.tonnage() for purposeItem in filter(lambda it: it.isPurposeItem(), tree.iterChildren()))
		fmtValue = self._formatSummary(tonnage)
		self.setText(self.treeWidget().tonnageColumnIndex(), fmtValue)
#

def bmDataPreviewReport(bmData, bmHeader, groupFields = [], fldDensity = None, elemFields = [], elemUnits = [], geoCodes = None):
	if len(elemFields) != len(elemUnits):
		raise Exception("Указаны разное количество элементов и единиц измерений")
	#
	#geoCodes = [[['Б', 'Богатая руда', 0], {'BM': [['Х-1О', '3.5'], ['Х-Л', '3.5'], ['Х-1В', '3.5'], ['Х-1Н', '3.5'], ['З-1В', '3.5'], ['З-1Н', '3.5'], [u'З-ЛВ', '3.5']], 'ASSAY': [['Х-1О'], ['Х-1В'], ['З-1В'], ['Х-Л']]}], [['М', 'Медистая руда', 0], {'BM': [['МПОВ', '3.2'], ['МПОН', '3.2'], ['М-О', 3.54]], 'ASSAY': [['М-О'], ['МПОВ'], ['МПОН']]}], [['В', 'Вкрапленная руда', 0], {'BM': [['В-О', '3'], ['В-1Н', '3'], ['В-Л', '3'], ['В-1В', '3']], 'ASSAY': [['В-О'], ['В-1Н'], ['В-Л']]}], [['Р', 'Роговик (легкая порода)', 2], {'BM': [['Р', '2.75']], 'ASSAY': []}], [['И', 'Интрузия (тяжелая порода)', 1], {'BM': [['И', '2.9']], 'ASSAY': []}], [['П', 'Прочая', 3], {'BM': [['ПР', '']], 'ASSAY': []}]]
	#geoCodes = self._parent.main_app.settings.getValue(ID_GEOCODES)

	materialIndexDensityDict = {}
	for (material, materialDesc, materialType), indexDict in geoCodes:
		for bmIndex, indexDensity, indexDensityWaste in list(zip(*indexDict["BM"]))[0]:
			try:		density = float(indexDensity)
			except:		density = math.nan
			materialIndexDensityDict[(material, bmIndex)] = density
	materialIndexDensityDict[("ЗАКЛ", "ЗАКЛ")] = 1.85
	materialIndexDensityDict[("ЗАКЛ", "СУХЗ")] = 1.85
	# процент: elemUnit = 0
	# г/т: elemUnit = 1
	coeff = None
	elemCoeffDict = {0: 100, 1: 1000}
	summFields = (RPTFLD_VOLUME, RPTFLD_FACT_TONNAGE, RPTFLD_MINE_TONNAGE, RPTFLD_FACT_DENSITY)
	#
	elemCoeffs = [elemCoeffDict.get(elemUnit) for elemUnit in elemUnits]
	elemCoeffUnknown = [elemCoeff is not None for elemCoeff in elemCoeffs]
	if not all(elemCoeffs):
		raise Exception("Неизвестная единица измерения '%s'" % elemFields[elemCoeffUnknown.index(False)])

	#
	blockFldX, blockFldY, blockFldZ = "_EAST", "_NORTH", "_RL"
	idxBlockFldX, idxBlockFldY, idxBlockFldZ  = bmHeader.index(blockFldX), bmHeader.index(blockFldY), bmHeader.index(blockFldZ)

	# индекс поля признака динамической плотности
	idxIsUseDynamicDensity = bmHeader.index('isUseDynamicDensity')

	# индекс поля плотности
	idxDensity = bmHeader.index(fldDensity) if fldDensity in bmHeader else None
	idxGeoMaterial = bmHeader.index(BMFLD_GEOMATERIAL)
	idxGeoIndex = bmHeader.index(BMFLD_GEOINDEX)

	# проверка на наличие полей
	allFields = groupFields + elemFields
	allFieldsInHeader = list(map(lambda field: field in bmHeader, allFields))
	if not allFieldsInHeader:
		raise Exception("Поле '%s' отсутствует в блочной модели" % allFields[allFieldsInHeader.index(False)])

	# индексы полей
	groupIndexes = [bmHeader.index(field) for field in groupFields]
	elemIndexes = [bmHeader.index(field) for field in elemFields]

	# elements кортеж хим элементов из српавочника
	elements = []
	indexes = list(zip(*geoCodes))[1]
	codes = [i['BM'] for i in indexes]
			
	for bm_indexes in codes:
		for bm_index in bm_indexes:
			for factor in bm_index[1]['FACTOR']:
				elements.append(factor[0])
	elements = set(elements)

	# Словарь индексов граф с химическими элементами в блочной модели
	idxElements = {}
	for element in elements:
		idx = bmHeader.index(element)
		idxElements[element] = idx

	#
	# расчет объемов/тоннажа
	dictionary = {}
	for row in bmData:
		density = 0 if math.isnan(row[idxDensity]) else row[idxDensity] 
		isUseDynamicDensity = row[idxIsUseDynamicDensity]
		if isUseDynamicDensity:
			# getDynamicDensity - функция возвращает динаически рассчитанный объёмный вес
			# header - заголовки полей БМ
			# row  - список значений  строки БМ, для которой рассчитываем объёмный вес
			# geocodes  - геологические коды, индексы, плотности, коэффициенты учавствующие в рассчёте
			# idxElements - словарь {'хим. элемент': индекс поля в БМ}
			# idxMaterial - индекс поля материала БМ
			# idxIndex - индекс поля  индекса БМ
			geoDensity = getDynamicDensity(bmHeader, row, geoCodes, idxElements, idxGeoMaterial, idxGeoIndex)
		else:
			geoDensity = materialIndexDensityDict.get((row[idxGeoMaterial], row[idxGeoIndex]))

		geoDensity = 0 if geoDensity is None else geoDensity
		#
		volume = row[idxBlockFldX]*row[idxBlockFldY]*row[idxBlockFldZ]
		tonnage = density*volume
		mineTonnage = geoDensity*volume
		#
		elemTonnage = [tonnage*row[elemIndex] / elemCoeffs[i] if not math.isnan(row[elemIndex]) else 0 for i, elemIndex in enumerate(elemIndexes)]
		#
		key = tuple(row[index] for index in groupIndexes)
		if not key in dictionary:
			dictionary[key] = [volume, tonnage, mineTonnage] + elemTonnage
		else:
			for iElem, value in enumerate([volume, tonnage, mineTonnage] + elemTonnage):
				dictionary[key][iElem] += value
	#
	# переход к содержаниям
	for key, values in dictionary.items():
		volume, tonnage, mineTonnage, elemTonnages = values[0], values[1], values[2], values[3:]
		density = tonnage / volume if volume != 0 else 0
		#
		if tonnage == 0:		elemGrades = tuple(0 for i, elemTonnage in enumerate(elemTonnages))
		else:					elemGrades = tuple(elemCoeffs[i]*elemTonnage / tonnage for i, elemTonnage in enumerate(elemTonnages))
		dictionary[key] = (volume, tonnage, mineTonnage, density) + elemGrades
	#
	rptData = [list(key) + list(dictionary[key]) for key in sorted(dictionary)]
	rptHeader = groupFields + summFields + elemFields
	return rptData, rptHeader

class BlockModelView(TreeWidget):
	def __init__(self, parent):
		super(BlockModelView, self).__init__(parent)

		self.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
		self.setSortingEnabled(True)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()

		self._buildHeader()

	def __createVariables(self):
		self._volumeColumnIndex, self._tonnageColumnIndex, self._densityColumnIndex = 5, 6, 7
		self._includeInReportColumnIndex, self._restoreMiningWorkColumnIndex, self._restoreGeologyColumnIndex = 8, 9, 10
		#
		self._catPurposeDict = {}
		self._catCodeDescriptionDict = {}
		self._materialDescriptionDict = {}
		self._materialIndexDensityDict = {}
		#
		self._currDateField = None
		self._summaryItem = None
		#
		self._originReport = []
		self._previewReport = []
		self._reportHeader = []
		self._elementHeader = []
		self._elementUnits = []
	def __createWidgets(self):
		pass

	def __createBindings(self):
		pass

	def selectedReportData(self):
		selectedItems = self.selectedItems()
		if not selectedItems:
			return []
		# выводим сводную информацию только по однотипным строкам
		materialItems = []
		for selectedItem in filter(lambda it: it.category() != SVY_BACKFIELD_CATEGORY, selectedItems):
			if selectedItem.isMaterialItem():
				materialItems.append(selectedItem)
			else:
				materialItems.extend(list(filter(lambda it: it.isMaterialItem() and it not in materialItems, selectedItem.iterChildren())))
		#
		idxMaterial = self.reportHeader().index(BMFLD_MATERIAL)
		reportData = []
		for materialItem in materialItems:
			reportData.extend(list(filter(lambda row: row[idxMaterial] != SVY_BACKFIELD_CATEGORY, materialItem.reportData())))
		return reportData

	def includedItems(self):
		return tuple(filter(lambda item: item.isMaterialItem() and item.isIncluded(), self.iterChildren()))
	def restoreMiningWorkItems(self):
		return tuple(filter(lambda item: item.isMaterialItem() and item.isIncluded() and item.isRestoreMiningWork(), self.iterChildren()))

	def gradeReport(self, reportData):
		idxIndex = self.reportHeader().index(BMFLD_INDEX)
		idxVolume = self.reportHeader().index(RPTFLD_VOLUME)
		idxTonnage = self.reportHeader().index(RPTFLD_FACT_TONNAGE)
		#
		reportData.sort(key = lambda x: x[idxIndex])


		elemUnits = self.elementUnits()
		elemIndexes = [self.reportHeader().index(element) for element in self.elementHeader()]
		elemCoeffDict = {0: 100, 1: 1000}
		elemCoeffs = [elemCoeffDict.get(elemUnit) for elemUnit in elemUnits]

		dictionary = {}
		for row in reportData:
			volume = row[idxVolume]
			tonnage = row[idxTonnage]

			elemTonnage = [tonnage*row[elemIndex] / elemCoeffs[i] if not math.isnan(row[elemIndex]) else 0 for i, elemIndex in enumerate(elemIndexes)]
			#
			key = (row[idxIndex],)
			if not key in dictionary:
				dictionary[key] = [tonnage] + elemTonnage
			else:
				for iElem, value in enumerate([tonnage] + elemTonnage):
					dictionary[key][iElem] += value
		#
		# переход к содержаниям
		for key, values in dictionary.items():
			tonnage, elemTonnages = values[0], values[1:]
			#
			if tonnage == 0:		elemGrades = tuple(0 for i, elemTonnage in enumerate(elemTonnages))
			else:					elemGrades = tuple(elemCoeffs[i]*elemTonnage / tonnage for i, elemTonnage in enumerate(elemTonnages))
			dictionary[key] = (tonnage,) + elemGrades

		report = [list(key) + list(dictionary[key])[1:] for key in sorted(dictionary)]
		return report

	def setMaterialDescriptionDict(self, d):	self._materialDescriptionDict = deepcopy(d)
	def materialDescriptionDict(self):			return self._materialDescriptionDict
	#
	def setMaterialIndexDensityDict(self, d):	self._materialIndexDensityDict = deepcopy(d)
	def materialIndexDensityDict(self):			return self._materialIndexDensityDict
	#
	def setCatPurposeDict(self, d):				self._catPurposeDict = deepcopy(d)
	def catPurposeDict(self):					return self._catPurposeDict
	#
	def setCatCodeDescriptionDict(self, d):		self._catCodeDescriptionDict = deepcopy(d)
	def catCodeDescriptionDict(self):			return self._catCodeDescriptionDict

	#
	def volumeColumnIndex(self):				return self._volumeColumnIndex
	def tonnageColumnIndex(self):				return self._tonnageColumnIndex
	def densityColumnIndex(self):				return self._densityColumnIndex

	def includeInReportColumnIndex(self):		return self._includeInReportColumnIndex
	def restoreMiningWorkColumnIndex(self):		return self._restoreMiningWorkColumnIndex
	def restoreGeologyColumnIndex(self):		return self._restoreGeologyColumnIndex

	def summaryColumnIndexes(self):				return (self.volumeColumnIndex(), self.tonnageColumnIndex(), self.densityColumnIndex())
	def optionColumnIndexes(self):				return (self._includeInReportColumnIndex, self._restoreMiningWorkColumnIndex, self._restoreGeologyColumnIndex)

	def currDateField(self):					return self._currDateField

	def purposeGroupFields(self):				return (self.currDateField(), BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL, BMFLD_MINEDATE, BMFLD_FACTDATE, BMFLD_FACTWORK)
	def mineWorkGroupFields(self):				return self.purposeGroupFields() + (BMFLD_MATERIAL,)

	def _buildHeader(self):
		headerItem = self.headerItem()
		columnNames = ("Выработка", "Интервал", "Дата выемки", "Дата закладки", "Вид работ", "Объем, м3", "Тоннаж, т", "Плотность",
						"Включить\nв отчет", "Восстановить\nвыемку", "Восстановить\nгеологию")
		columnWidths = (220, 130, 90, 90, 170, 80, 80, 80, 85, 85, 85)
		for idxColumn, (columnName, columnWidth) in enumerate(zip(columnNames, columnWidths)):
			headerItem.setText(idxColumn, columnName)
			headerItem.setTextAlignment(idxColumn, QtCore.Qt.AlignCenter)
			self.setColumnWidth(idxColumn, columnWidth)
		self.hideDensityColumn()

	def reportHeader(self):						return self._reportHeader
	def elementHeader(self):					return self._elementHeader
	def elementUnits(self):						return self._elementUnits

	def setReport(self, rptData, rptHeader, elemFields, elemUnits, currDateField = None):
		if not all((self._catPurposeDict, self._catCodeDescriptionDict, self._materialDescriptionDict, self._materialIndexDensityDict)):
			raise Exception("Не заданы словари для расшифровки кодов")

		self._originReport = deepcopy(rptData)
		self._previewReport = deepcopy(rptData)
		self._reportHeader = deepcopy(rptHeader)
		self._elementHeader = deepcopy(elemFields)
		self._elementUnits = deepcopy(elemUnits)

		self._currDateField = currDateField

		self._prepare()
		self._populate()

	def _prepare(self):
		# сортируем по назначению
		idxCurrDate = self.reportHeader().index(self.currDateField())
		for i, row in enumerate(self._previewReport):
			category = row[idxCurrDate]
			if not category in SVY_MINING_CATEGORIES:
				self._previewReport[i][idxCurrDate] = self.catPurposeDict()[category]

		groupFields = [self.currDateField(), BMFLD_MINE_MINENAME, BMFLD_MINEDATE, BMFLD_MINE_INTERVAL, BMFLD_FACTWORK, BMFLD_MATERIAL]
		sortIndexes = [self.reportHeader().index(field) for field in groupFields]
		self._previewReport.sort(key = lambda x: tuple(x[index] for index in sortIndexes), reverse = True)
		
	def _populate(self):
		# вставляем отчет
		self.blockSignals(True)
		self.clear()
		#
		for purposeGroup, purposeGroupValues in groupby(self._previewReport, key = lambda x: x[0]):
			purposeGroupValues = tuple(purposeGroupValues)
			#
			purposeItem = PurposeTreeWidgetItem(self, purposeGroup)
			purposeItem.setReport(purposeGroup, purposeGroupValues)
		self._summaryItem = SummaryTreeWidgetItem(self)
		#
		self.blockSignals(False)

	def _includeIntoReportStateChanged(self, item):
		isChecked = item.checkBox(self._includeInReportColumnIndex).isChecked()
		for checkBoxColumnIndex in self.optionColumnIndexes()[1:]:
			item.checkBox(checkBoxColumnIndex).setDisabled(not isChecked)

	def _itemChanged(self, item, column):
		if not column in self.optionColumnIndexes():
			return
		self.setFocus()

	def summaryItem(self):
		return self._summaryItem

	def hideIncludeIntoReportColumn(self):		self.hideColumn(self._includeInReportColumnIndex)
	def showIncludeIntoReportColumn(self):		self.showColumn(self._includeInReportColumnIndex)
	def hideRestoreMiningWorkColumn(self):		self.hideColumn(self._restoreMiningWorkColumnIndex)
	def showRestoreMiningWorkColumn(self):		self.showColumn(self._restoreMiningWorkColumnIndex)
	def hideRestoreGeologyColumn(self):			self.hideColumn(self._restoreGeologyColumnIndex)
	def showRestoreGeologyColumn(self):			self.showColumn(self._restoreGeologyColumnIndex)
	def hideDensityColumn(self):				self.hideColumn(self._densityColumnIndex)
	def showDensityColumn(self):				self.showColumn(self._densityColumnIndex)



class BlockModelGradesWidget(QtGui.QSplitter):
	def __init__(self, parent):
		super(BlockModelGradesWidget, self).__init__(QtCore.Qt.Vertical, parent = parent)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createBindings(self):
		self.blockModelView.itemSelectionChanged.connect(self.showSelectedGrades)
	def __createWidgets(self):
		self.blockModelView = BlockModelView(self)
		self.blockModelGrades = CustomDataView(self)
		self.blockModelGrades.setVisible(False)
	def __gridWidgets(self):
		self.addWidget(self.blockModelView)
		self.addWidget(self.blockModelGrades)

		self.setStretchFactor(0, 70)
		self.setStretchFactor(1, 30)

	def showSelectedGrades(self):
		selectedData = self.blockModelView.selectedReportData()
		isGradeReportVisible = bool(selectedData)
		self.blockModelGrades.setVisible(isGradeReportVisible)
		if not isGradeReportVisible:
			return
		#
		gradeReport = self.blockModelView.gradeReport(selectedData)
		gradeHeader = (BMFLD_INDEX,) + tuple(self.blockModelView.elementHeader())
		gradeModel = CustomDataModel(self.blockModelGrades, gradeReport, gradeHeader)
		self.blockModelGrades.setModel(gradeModel)
		self.blockModelGrades.horizontalHeader().removeFilter()
		self.blockModelGrades.horizontalHeader().setVisible(True)
		self.blockModelGrades.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
		self.blockModelGrades.horizontalHeader().setStretchLastSection(False)
		self.blockModelGrades.horizontalHeader().setDefaultSectionSize(70)

class BmPreviewOptionsFrame(QtGui.QWidget):
	def __init__(self, parent):
		super(BmPreviewOptionsFrame, self).__init__(parent)

		self.setMaximumWidth(250)

		self.__createVariables()
		self.__initUi()
		self.__createBindings()

	def __initUi(self):
		self.__createWidgets()
		self.__gridWidgets()

	def __createVariables(self):
		self._mineSiteId = None
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(0)

		self.groupBoxReportType = QtGui.QGroupBox(self)
		self.groupBoxReportType.setTitle("Тип отчета")
		self.layoutReportType = QtGui.QVBoxLayout()
		self.reportTypeButtonGroup = QtGui.QButtonGroup()
		self.radioButtonByMonth = QtGui.QRadioButton(self.groupBoxReportType)
		self.radioButtonByMonth.setText("По месяцу")
		self.radioButtonByMonth.setChecked(True)
		self.radioButtonByMine = QtGui.QRadioButton(self.groupBoxReportType)
		self.radioButtonByMine.setText("По выработке")
		self.reportTypeButtonGroup.addButton(self.radioButtonByMonth)
		self.reportTypeButtonGroup.addButton(self.radioButtonByMine)

		self.groupBoxSaveToDataBase = QtGui.QGroupBox(self)
		self.groupBoxSaveToDataBase.setTitle("Сохранить в базу данных")
		self.groupBoxSaveToDataBase.setCheckable(True)
		self.groupBoxSaveToDataBase.setChecked(False)
		self.layoutSaveToDataBase = QtGui.QFormLayout()
		self.labelMineSiteName = QtGui.QLabel(self.groupBoxSaveToDataBase)
		self.labelMineSiteName.setText("Участок")
		self.labelMineSiteName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelMineSiteName.setMinimumWidth(50)
		self.lineEditMineSiteName = QtGui.QLineEdit(self.groupBoxSaveToDataBase)
		self.lineEditMineSiteName.setReadOnly(True)
	def __createBindings(self):
		self.lineEditMineSiteName.mouseDoubleClickEvent = lambda e: self.selectMineSiteName()
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxReportType)
		self.groupBoxReportType.setLayout(self.layoutReportType)
		self.layoutReportType.addWidget(self.radioButtonByMonth)
		self.layoutReportType.addWidget(self.radioButtonByMine)

		self.layoutMain.addWidget(self.groupBoxSaveToDataBase)
		self.groupBoxSaveToDataBase.setLayout(self.layoutSaveToDataBase)
		self.layoutSaveToDataBase.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelMineSiteName)
		self.layoutSaveToDataBase.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditMineSiteName)

	def selectMineSiteName(self):
		projname = self._parent.main_app.settings.getValue(ID_PROJNAME)
		cmd = """
			SELECT DepartId, DepartName, PitName
			FROM [{0}].[dbo].[DEPARTS] D LEFT JOIN [{0}].[dbo].[PITS] P ON D.DepartPitID = P.PitID
			WHERE P.LOCATION = N'{1}'""".format(self._parent.main_app.srvcnxn.dbsvy, projname)
		data = self._parent.main_app.srvcnxn.exec_query(cmd).get_rows()

		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите участок")
		model = CustomDataModel(dialog.view, data, ["ID", "УЧАСТОК", "ШАХТА"])
		dialog.view.setModel(model)
		dialog.view.hideColumn(0)
		dialog.view.setColumnWidth(1, 250)
		dialog.resize(QtCore.QSize(500, 500))
		dialog.exec()
		#
		mineSiteId = dialog.getResult()
		if not mineSiteId:
			return

		self.lineEditMineSiteName.setText(mineSiteId[0][1])
		self._mineSiteId = mineSiteId[0][0]

	def mineSiteId(self):
		return self._mineSiteId

#class BlockModelPreviewDialog(QtGui.QDialog):
#	def __init__(self, parent):
#		super(BlockModelPreviewDialog, self).__init__(parent)

#		self.__createVariables()
#		self.__createWidgets()
#		self.__createBindings()
#		self.__gridWidgets()

#	def __createVariables(self):
#		self._is_cancelled = True
#		self._needsRestoreGeology = False
#		self._fldCurrdate = None
#		self._itemsAndReports = []

#		#
#		geoCodes = (
#			[
#				['Б', 'Богатая руда', 0], 
#				{
#					'BM': [['Х-1О', '3.5'], ['Х-Л', '3.5'], ['Х-1В', '3.5'], ['Х-1Н', '3.5'], ['З-1В', '3.5'], ['З-1Н', '3.5'], ['З-ЛВ', '3.5']], 
#					'ASSAY': [['Х-1О'], ['Х-1В'], ['З-1В'], ['Х-Л']]
#				}
#			], 
#			[
#				['М', 'Медистая руда', 0], 
#				{
#					'BM': [['МПОВ', '3.2'], ['МПОН', '3.2'], ['М-О', 3.54]], 
#					'ASSAY': [['М-О'], ['МПОВ'], ['МПОН']]
#				}
#			], 
#			[
#				['В', 'Вкрапленная руда', 0], 
#				{
#					'BM': [['В-О', '3'], ['В-1Н', '3'], ['В-Л', '3'], ['В-1В', '3']], 
#					'ASSAY': [['В-О'], ['В-1Н'], ['В-Л']]
#				}
#			], 
#			[
#				['Р', 'Роговик (легкая порода)', 2], 
#				{
#					'BM': [['Р', '2.75']], 
#					'ASSAY': []
#				}
#			], 
#			[
#				['И', 'Интрузия (тяжелая порода)', 1], 
#				{
#					'BM': [['И', '2.9']], 
#					'ASSAY': []
#				}
#			], 
#			[
#				['П', 'Прочая', 3], 
#				{
#					'BM': [['ПР', '']], 
#					'ASSAY': []
#				}
#			]
#		)
#		#
#		self._materialIndexDensityDict = {}
#		for (material, materialDesc, materialType), indexDict in geoCodes:
#			for bmIndex, indexDensity in indexDict["BM"]:
#				try:		density = float(indexDensity)
#				except:		density = math.nan
#				self._materialIndexDensityDict[(material, bmIndex)] = density
#		self._materialIndexDensityDict[("ЗАКЛ", "ЗАКЛ")] = 1.85
#		self._materialIndexDensityDict[("ЗАКЛ", "СУХЗ")] = 1.85

#		#
#		svyCatsAndCodes = (
#			[
#				['Горно-капитальные работы', 'ПРОХ'], 
#				[['ГКР', 'Горно-капитальная проходка']]
#			], 
#			[
#				['Камерно-кубажные работы', 'КК'], 
#				[['КК', 'Камерно-кубажные работы']]
#			], 
#			[
#				['Горно-подготовительные работы', 'ПРОХ'], 
#				[['НП', 'Нарезная проходка'], ['ПП', 'Подготовительная проходка'], ['ЭП', 'Эксплуатационная проходка']]
#			], 
#			[
#				['Очистные работы', 'ОР'], 
#				[['ОР', 'Очистные работы'], ['РАСШ', 'Расширение'], ['ОТСЛ', 'Отслоение']]
#			], 
#			[
#				['Оперативный учет', 'ОПЕР'], 
#				[['ОР', 'Очистные работы'], ['КК', 'Камерно-кубажные работы'], ['НП', 'Нарезная проходка'], ['ПП', 'Подготовительная проходка'], ['ЭП', 'Эксплуатационная проходка']]
#			], 
#			[
#				['Неучитываемые объемы', 'НЕУЧ'], 
#				[['НД', 'Недозаклад'], ['ПОТЕРИ', 'Потери']]
#			], 
#			[
#				['Закладка', 'ЗАКЛ'], 
#				[['ЗАКЛ', 'Твердеющая закладка'], ['СУХЗ', 'Сухая закладка']]
#			], 
#			[
#				['Сыпучий материал', 'СМ'], 
#				[['ГРУЗ', ''], ['ПОДС', ''], ['СКЛАД', '']]
#			]
#		)
#		#
#		self._catPurposeDict = {}
#		self._catCodeDescriptionDict = {}
#		for (purpose, category), codesAndDescriptions in svyCatsAndCodes:
#			self._catPurposeDict[category] = purpose
#			for code, description in codesAndDescriptions:
#				self._catCodeDescriptionDict[(category, code)] = description
#		for material, index in self._materialIndexDensityDict.keys():
#			if material in SVY_MINING_CATEGORIES:
#				continue
#			self._catPurposeDict[material] = "Руда/Порода"
#			self._catCodeDescriptionDict[(material, "")] = ""

#		#
#		self._materialDescriptionDict = {"Б": "Богатая руда", "М": "Медистая руда", "В": "Вкрапленная руда", "И": "Тяжелая порода", "Р": "Легкая порода", "ЗАКЛ": "Бетон"}

#	def __createWidgets(self):
#		self.layoutMain = QtGui.QVBoxLayout()

#		self.layoutControls = QtGui.QHBoxLayout()

#		self.layoutLeftPanel = QtGui.QVBoxLayout()
#		self.objectList = QtGui.QListWidget(self)
#		self.objectList.setMaximumWidth(250)
#		self.objectList.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
#		self.paramsFrame = BmPreviewOptionsFrame(self)
#		#
#		self.bmPreviewStacked = QtGui.QStackedWidget(self)
#		#
#		self.buttonBox = QtGui.QDialogButtonBox()
#		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Save)
#		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setText("Продолжить")
#		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
#		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
#		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
#		self.buttonBox.button(QtGui.QDialogButtonBox.Save).setText("Экспорт")
#		self.buttonBox.button(QtGui.QDialogButtonBox.Save).setMinimumWidth(100)
#	def __createBindings(self):
#		self.objectList.itemSelectionChanged.connect(self.listItemSelected)
#		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self._cancell)
#		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._continue)
#		self.buttonBox.button(QtGui.QDialogButtonBox.Save).clicked.connect(self._exportToExcel)
#	def __gridWidgets(self):
#		self.setLayout(self.layoutMain)

#		self.layoutMain.addLayout(self.layoutControls)

#		self.layoutControls.addLayout(self.layoutLeftPanel)
#		self.layoutLeftPanel.addWidget(self.objectList)
#		self.layoutLeftPanel.addWidget(self.paramsFrame)
#		#
#		self.layoutControls.addWidget(self.bmPreviewStacked)
#		#
#		self.layoutMain.addWidget(self.buttonBox)

#	def _addPreviewPage(self, reportData, reportHeader):
#		elemFields = ("Ni", "Cu", "Co", "S", "Pt", "Pd", "Rh", "Os", "Ir", "Ru", "Au", "Ag", "Te", "Se")
#		elemUnits = (   0,    0,    0,   0,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)
#		#
#		bmPreviewWidget = BlockModelGradesWidget(self.bmPreviewStacked)
#		#
#		if not self._needsRestoreGeology:	bmPreviewWidget.blockModelView.hideRestoreGeologyColumn()
#		else:								bmPreviewWidget.blockModelView.hideRestoreMiningWorkColumn()
#		#
#		bmPreviewWidget.blockModelView.setCatPurposeDict(self._catPurposeDict)
#		bmPreviewWidget.blockModelView.setCatCodeDescriptionDict(self._catCodeDescriptionDict)
#		bmPreviewWidget.blockModelView.setMaterialIndexDensityDict(self._materialIndexDensityDict)
#		bmPreviewWidget.blockModelView.setMaterialDescriptionDict(self._materialDescriptionDict)
#		bmPreviewWidget.blockModelView.setReport(reportData, reportHeader, elemFields, elemUnits, self._fldCurrdate)
#		#
#		self.bmPreviewStacked.addWidget(bmPreviewWidget)

#	def addBlockModelPreview(self, listItem, reportData, reportHeader):
#		self._itemsAndReports.append((listItem, (reportData, reportHeader)))
#		#
#		self.objectList.addItem(listItem)
#		self._addPreviewPage(reportData, reportHeader)

#	def _removeMultiPreviewPage(self):
#		stackedCount = self.bmPreviewStacked.count()
#		itemsCount = self.objectList.count()
#		if stackedCount == itemsCount:
#			return
#		#
#		self.bmPreviewStacked.removeWidget(self.bmPreviewStacked.widget(stackedCount-1))

#	def listItemSelected(self):
#		selectedItems = self.objectList.selectedItems()
#		selectedItemsCount = len(selectedItems)
#		if selectedItemsCount == 0:
#			self.bmPreviewStacked.setCurrentIndex(0)
#			return
#		#
#		self._removeMultiPreviewPage()
#		#
#		userItems, reports = tuple(zip(*self._itemsAndReports))
#		if selectedItemsCount == 1:
#			selectedItemIndex = userItems.index(selectedItems[0])
#			self.bmPreviewStacked.setCurrentIndex(selectedItemIndex)
#		else:
#			multiReportData = []
#			for i in range(self.objectList.count()):
#				listItem = self.objectList.item(i)
#				if not listItem.isSelected():
#					continue
#				#
#				reportData, reportHeader = self._itemsAndReports[i][1]
#				multiReportData.extend(reportData)
#			#
#			self._addPreviewPage(multiReportData, reportHeader)
#			self.bmPreviewStacked.setCurrentIndex(self.bmPreviewStacked.count()-1)

#	def includedItems(self):
#		currentStackedIndex = self.bmPreviewStacked.currentIndex()
#		listItemsCount = self.objectList.count()
#		if currentStackedIndex == listItemsCount:
#			includedItems = self.bmPreviewStacked.widget(currentStackedIndex).blockModelView.includedItems()
#		else:
#			includedItems = []
#			for i in range(listItemsCount):
#				includedItems.extend(self.bmPreviewStacked.widget(i).blockModelView.includedItems())
#		return includedItems
#	def restoreMiningWorkItems(self):
#		currentStackedIndex = self.bmPreviewStacked.currentIndex()
#		listItemsCount = self.objectList.count()
#		if currentStackedIndex == listItemsCount:
#			restoreMiningWorkItems = self.bmPreviewStacked.widget(currentStackedIndex).blockModelView.restoreMiningWorkItems()
#		else:
#			restoreMiningWorkItems = []
#			for i in range(listItemsCount):
#				restoreMiningWorkItems.extend(self.bmPreviewStacked.widget(i).blockModelView.restoreMiningWorkItems())
#		return restoreMiningWorkItems

#	def needsRestoreGeology(self):				return self._needsRestoreGeology
#	def setNeedsRestoreGeology(self, value):	self._needsRestoreGeology = bool(value)
#	def fldCurrdate(self):						return self._fldCurrdate
#	def setFldCurrdate(self, value):			self._fldCurrdate = value

#	def _browseSaveFile(self):
#		filetypes = "EXCEL (*.XLSX)"
#		try:			initialdir = MMpy.Project.path()
#		except:			initialdir = os.getcwd()
#		path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
#		if not path:
#			return
#		return path

#	def _exportToExcel(self):
#		path = self._browseSaveFile()
#		print (path)

#	def _continue(self):
#		self._is_cancelled = False
#		self.close()

#	def _cancell(self):
#		self.close()

#	@property
#	def is_cancelled(self):
#		return self._is_cancelled

#BMFLD_CURRDATE = "2017-03"
#BMFLD_CURRDATE = "2017-01"
#groupFields = (BMFLD_CURRDATE, BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL, BMFLD_MINEDATE, BMFLD_FACTDATE, BMFLD_FACTWORK, BMFLD_MATERIAL, BMFLD_INDEX)
#elemFields = ("Ni", "Cu", "Co", "S", "Pt", "Pd", "Rh", "Os", "Ir", "Ru", "Au", "Ag", "Te", "Se")
#elemUnits = (   0,    0,    0,   0,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1)

#rptParams = dict(
#	bmData = bmData, 
#	bmHeader = bmHeader,
#	groupFields = groupFields,
#	fldDensity = "ОБЪЕМНЫЙ_ВЕС",
#	elemFields = elemFields,
#	elemUnits = elemUnits
#)
#logger.debug('1')
#largeReportData, largeReportHeader = bmDataPreviewReport(**rptParams)
#smallReportData, smallReportHeader = deepcopy(largeReportData)[:30], deepcopy(largeReportHeader)

##

#app = QtGui.QApplication(os.sys.argv)
##
#bmPreviewDialog = BlockModelPreviewDialog(None)
#bmPreviewDialog.resize(QtCore.QSize(1340, 600))
##
#bmPreviewDialog.setNeedsRestoreGeology(False)
#bmPreviewDialog.setFldCurrdate(BMFLD_CURRDATE)
##
#listItemLarge = createListTreeItem("LARGE", ITEM_BM, ITEM_CATEGORY_GEO)
#bmPreviewDialog.addBlockModelPreview(listItemLarge, largeReportData, largeReportHeader)
#listItemSmall = createListTreeItem("SMALL", ITEM_BM, ITEM_CATEGORY_GEO)
#bmPreviewDialog.addBlockModelPreview(listItemSmall, smallReportData, smallReportHeader)
##
#bmPreviewDialog.exec()
##


#app.exec_()
