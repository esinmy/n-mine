from PyQt4 import QtGui, QtCore
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class CustomProgressBar(QtGui.QProgressBar):
	def __init__(self, *args, **kw):
		super(CustomProgressBar, self).__init__(*args, **kw)
		self.setAlignment(QtCore.Qt.AlignCenter)
		self._text = None
		self._value = 0
		self._step = 1
	def setText(self, text):
		self._text = text
		QtCore.QCoreApplication.processEvents()
	def text(self):
		return self._text
	def setValue(self, value):
		self._value = value
		super(CustomProgressBar, self).setValue(int(self._value))
		QtCore.QCoreApplication.processEvents()
	def value(self):
		return self._value
	def step(self):
		return self._step
	def setStep(self, value):
		self._step = float(value)
	def do_step(self, step = None):
		step = self.step() if not step else step
		self.setValue(self.value() + step)