try:					import MMpy
except:					pass

from PyQt4 import QtGui, QtCore

from mm.mmutils import Tridb, MicromineFile, SvyWireframeFormSet

from utils.constants import *
from utils.dbconnection import generate_connection_string
from utils.nnutil import prev_date
from utils.fsettings import *
from utils.osutil import *
from utils.qtutil import iterWidgetParents
from utils.validation import is_valid_date, is_blank

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog

from datetime import datetime
import math
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class ProcessWindow(QtGui.QWidget):
	def __init__(self, parent, use_connection = False):
		super().__init__(parent)

		self._use_connection = use_connection
		self.__createVariables()

	def __createVariables(self):
		# рабочая директория (сетевой проект)
		self._wrkpath = self.main_app.settings.getValue(ID_NETPATH)
		self._cleartemp = True
		# временная директория
		self._tmpprefix = ""
		try:			self._tmpfolder = MMpy.Project.path() + "TEMPORARY_SCRIPT_FOLDER"
		except:			self._tmpfolder = "D:\\TEMPORARY_SCRIPT_FOLDER"

		self._is_running = False
		self._is_success = False
		self._in_process = False
		self._is_cancelled = False

		self._lockfile = {}
		self._selectedItems = {}

	def exec(self, cleartemp = False):
		self._cleartemp = bool(cleartemp)
		self.main_app.mainWidget.progressBar.setRange(0, 100)
		self.main_app.mainWidget.progressBar.setValue(0)
		self.prepare()

	def exec_proc(self, function, *args, **kw):
		self._in_process = True
		try:
			function(*args, **kw)
			self._in_process = False
			return None
		except Exception as e:
			self._in_process = False
			return str(e)
	def exec_mmproc(self, function, *args, **kw):
		# возвращает сообщение об ошибке
		self._mm_running = True
		self._in_process = True
		try:
			function(*args, **kw)
			self._mm_running = False
			self._in_process = False
			err = None
		except Exception as e:
			self._mm_running = False
			self._in_process = False
			return str(e)

	### =================================================================================== ###
	### ================== СЧИТЫВАНИЕ НАИМЕНОВАНИЕ ДИРЕКТОРИЙ ИЗ НАСТРОЕК ================= ###
	### =================================================================================== ###

	def getGeoDirList(self):			return list(filter(lambda x: x, list(zip(*self.main_app.settings.getValue(ID_GEO_USUBDIRS)))[1]))
	def getGeoDirBlockModel(self):		return self.main_app.settings.getValue(ID_GEO_USUBDIRS)[0][1]
	def getGeoDirWireframes(self):		return self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1]
	def getGeoDirGraphic(self):			return self.main_app.settings.getValue(ID_GEO_USUBDIRS)[2][1]
	def getGeoDirContours(self):		return self.main_app.settings.getValue(ID_GEO_USUBDIRS)[3][1]
	def getGeoDirImages(self):			return self.main_app.settings.getValue(ID_GEO_USUBDIRS)[4][1]

	def getSvyDirList(self):			return filter(lambda x: x, list(zip(*self.main_app.settings.getValue(ID_GEO_USUBDIRS)))[1])
	def getSvyDirWireframes(self):		return self.main_app.settings.getValue(ID_SVY_USUBDIRS)[0][1]
	def getSvyDirGraphic(self):			return self.main_app.settings.getValue(ID_SVY_USUBDIRS)[1][1]
	def getSvyDirStrings(self):			return self.main_app.settings.getValue(ID_SVY_USUBDIRS)[2][1]
	def getSvyDirSurvey(self):			return self.main_app.settings.getValue(ID_SVY_USUBDIRS)[3][1]

	def getDsnDirList(self):			return filter(lambda x: x, list(zip(*self.main_app.settings.getValue(ID_GEO_USUBDIRS)))[1])
	def getDsnDirBlockModel(self):		return self.main_app.settings.getValue(ID_DSN_USUBDIRS)[0][1]
	def getDsnDirWireframes(self):		return self.main_app.settings.getValue(ID_DSN_USUBDIRS)[1][1]
	def getDsnDirStrings(self):			return self.main_app.settings.getValue(ID_DSN_USUBDIRS)[2][1]
	def getDsnDirGraphic(self):			return self.main_app.settings.getValue(ID_DSN_USUBDIRS)[3][1]
	def getDsnDirBlastHoles(self):		return self.main_app.settings.getValue(ID_DSN_USUBDIRS)[4][1]
	def getDsnDirTables(self):			return self.main_app.settings.getValue(ID_DSN_USUBDIRS)[5][1]

	def getMineUnitDirList(self):		return list(filter(lambda x: x, list(zip(*self.main_app.settings.getValue(ID_MUSUBDIRS)))))
	def getMineUnitBlockModel(self):	return self.main_app.settings.getValue(ID_MUSUBDIRS)[0][1]
	def getMineUnitWireframes(self):	return self.main_app.settings.getValue(ID_MUSUBDIRS)[0][1]
	def getMineUnitReports(self):		return self.main_app.settings.getValue(ID_MUSUBDIRS)[2][1]

	def getIdwElements(self):			return list(zip(*self.main_app.settings.getValue(ID_IDWELEM)))[0]
	def getIdwUnits(self):				return list(zip(*self.main_app.settings.getValue(ID_IDWELEM)))[1]
	def getIdwRadiuses(self):			return list(zip(*self.main_app.settings.getValue(ID_IDWRADIUS)))[0]

	### =================================================================================== ###
	### ========= СЧИТЫВАНИЕ ПЛОТНОСТЕЙ ,КОДОВ, КАТЕГОРИЙ, НАЗНАЧЕНИЙ ИЗ НАСТРОЕК ========= ###
	### =================================================================================== ###

	# геология
	def getGeoCodes(self):						return list(zip(*self.main_app.settings.getValue(ID_GEOCODES)))[0]
	def getGeoIndexes(self):					return list(zip(*self.main_app.settings.getValue(ID_GEOCODES)))[1]
	def getGeoEmptyCode(self):
		codes = list(filter(lambda x: x[-1] == 2, self.getGeoCodes()))
		return None if not codes else codes[0][0]
	def getGeoEmptyIndexAndDensity(self):
		index = list(filter(lambda x: x[0][-1] == 2, self.main_app.settings.getValue(ID_GEOCODES)))
		index = None if not index else index[0][1]["BM"][0]
		return (None, None, None) if not index else index[0]

	# маркшейдерия
	def getSvyCategoriesAndCodes(self):				return self.main_app.settings.getValue(ID_SVYCAT)
	def getPurposeByCategoryAndCode(self, ucategory, ucode):
		for (purpose, category), CodesAndDescriptions in self.getSvyCategoriesAndCodes():
			if category == ucategory:
				for code, description in CodesAndDescriptions:
					if code == ucode:
						return purpose
	def getEmptyPurposeByCode(self, ucode):
		# возвращает НАЗНАЧЕНИЕ по коду работ для пустот
		for (purpose, category), CodesAndDescriptions in self.getSvyCategoriesAndCodes()[:4]:
			for code, description in CodesAndDescriptions:
				if code == ucode:
					return purpose
	def getDensityByMaterialAndIndex(self, umaterial, uindex):
		svyCatsAndCodes = self.getSvyCategoriesAndCodes()
		concDensity = self.main_app.settings.getValue(ID_CONCDENS)
		materialIndexDensityDict = {}

		for (material, materialDesc, materialType), indexDict in self.main_app.settings.getValue(ID_GEOCODES):
			for bmIndex, indexDensity, indexDensityWaste in list(zip(*indexDict["BM"]))[0]:
				try:		density = float(indexDensity)
				except:		density = math.nan
				materialIndexDensityDict[(material, bmIndex)] = density
		materialIndexDensityDict[(svyCatsAndCodes[6][0][1], svyCatsAndCodes[6][1][0][0])] = concDensity
		materialIndexDensityDict[(svyCatsAndCodes[6][0][1], svyCatsAndCodes[6][1][0][1])] = concDensity
		return materialIndexDensityDict.get((umaterial, uindex))


	### =================================================================================== ###
	### ========== СЧИТЫВАНИЕ МАРКШЕЙДЕРСКИХ НАЗНАЧЕНИЙ И КАТЕГОРИЙ ИЗ НАСТРОЕК =========== ###
	### =================================================================================== ###

	def getPurposesAndCategories(self):
		purposesAndCategories = [(purpose, category) for (purpose, category), codesAndDescriptions in self.getSvyCategoriesAndCodes()]
		purposes, categories = tuple(zip(*purposesAndCategories))
		return purposes, categories
	def getPurposes(self):						return self.getPurposesAndCategories()[0]
	def getCategories(self):					return self.getPurposesAndCategories()[1]
	# выемочные работы
	def getMineWorkPurposes(self):				return self.getPurposes()[:5] + (self.getBackfillPurpose(),)
	def getMineWorkCategories(self):			return self.getCategories()[:5] + (self.getBackfillCategory(),)
	# закладочные работы
	def getBackfillPurpose(self):				return self.getPurposes()[6]
	def getBackfillCategory(self):				return self.getCategories()[6]
	def getBackfillIndexes(self):
		backfill_indexes = []
		backfillSvyCategoriesAndCodes =list( filter(lambda x: x[0][1] == self.getBackfillCategory() , self.getSvyCategoriesAndCodes()))
		for cat, indexes in backfillSvyCategoriesAndCodes:
			backfill_indexes.extend(list(zip(*indexes))[0])
		return 	backfill_indexes
	# оперативные работы
	def getCurrWorkPurpose(self):				return self.getPurposes()[4]
	def getCurrWorkCategory(self):				return self.getCategories()[4]

	# словари
	def getMineWorkPurposeCategoryDict(self):
		purposes = self.getMineWorkPurposes()
		categories = self.getMineWorkCategories()
		return dict(zip(purposes, categories))
	def getMineWorkPurposeCodeDict(self):
		purposes = self.getMineWorkPurposes()
		codes = [tuple(row[0] for row in codesAndDescriptions) for (purpose, category), codesAndDescriptions in self.getSvyCategoriesAndCodes() if purpose in purposes]
		return dict(zip(purposes, codes))
	def getMineWorkPurposePriorityDict(self):
		allMiningPurposes = self.getMineWorkPurposes()
		backfillPurpose = self.getBackfillPurpose()
		purposePriorityDict = {}
		purposePriorityDict[allMiningPurposes[0]] = 4		# Горно-капитальные работы (приоритет 4)
		purposePriorityDict[allMiningPurposes[1]] = 1		# Камерно-кубажные работы (приоритет 1)
		purposePriorityDict[allMiningPurposes[2]] = 3		# Горно-подготовительные работы (приоритет 3)
		purposePriorityDict[allMiningPurposes[3]] = 2		# Очистные работы (приоритет 2)
		purposePriorityDict[backfillPurpose] = 5			# Закладочные работы (приоритет 5)
		return purposePriorityDict
	def getMaterialCodes(self):
		return tuple(oreCode for (oreCode, oreDesc, oreType) in self.getGeoCodes()) + (self.getBackfillCategory(),)

	### =================================================================================== ###
	### ============== СОЗДАНИЕ СЛОВАРЯ {"<ПУТЬ_К_TRIDB>" : СПИСОК_КАРКАСОВ} ============== ###
	### =================================================================================== ###

	def getTridbPathsDict(self, tridbItems, copy = False):
		progress = self.main_app.mainWidget.progressBar
		#
		tridbPathsDict = {}
		for tridbItem in tridbItems:
			netTriPath = tridbItem.filePath()
			tridbName = os.path.basename(netTriPath)
			if copy:	triPath = os.path.join(self._tmpfolder, get_temporary_prefix() + tridbName)
			else:		triPath = netTriPath
			# копирование
			if copy:
				err = self.exec_proc(self.copy_file, netTriPath, triPath)
				if err:					raise Exception("Невозможно скопировать файл %s. %s" % (netTriPath, err))
				if self.is_cancelled:	return
			#
			tridbPathsDict[triPath] = []
			if tridbItem.childCount() == 0:
				try:		tridb = Tridb(triPath)
				except:		continue
				tridbPathsDict[triPath] = tridb.wireframe_names
				tridb.close()
			else:
				for wfItem in tridbItem.iterChildren():
					tridbPathsDict[triPath].append(wfItem.name())
		return tridbPathsDict

	### =================================================================================== ###
	### ====================== СОЗДАНИЕ МАРКШЕЙДЕРСКОГО НАБОРА КАРКАСОВ =================== ###
	### =================================================================================== ###

	def generateDsnWireframeSets(self, tridbPathsDict, bmCurrentDate, groupByProject = True):
		progress = self.main_app.mainWidget.progressBar
		#
		purposeCategoryDict = self.getMineWorkPurposeCategoryDict()
		purposeCodeDict = self.getMineWorkPurposeCodeDict()
		purposePriorityDict = self.getMineWorkPurposePriorityDict()
		# изменяем приоритет на 0 для закладочных работ, чтобы можно было запроектировать бетон
		purposePriorityDict[self.getBackfillPurpose()] = 0

		categories = self.getCategories()
		
		### =================================================================================== ###
		### ========================== СЧИТЫВАНИЕ КОДОВ И КАТЕГОРИЙ =========================== ###
		### =================================================================================== ###

		progress.setText("Считывание кодов, категорий и проектов")
		wfdata = []
		selectedProjects = set()
		for tripath in tridbPathsDict:
			tridb = Tridb(tripath)
			for wfname in tridbPathsDict[tripath]:
				wireframe = tridb[wfname]

				title = wireframe.default_attribute(DEFAULT_ATTR_TITLE)
				stage = wireframe.user_attribute(DSN_ATTR_STAGE)
				if not stage.isnumeric():
					continue
				stage = int(stage)
				category = wireframe.user_attribute(DSN_ATTR_CATEGORY)
				code = wireframe.default_attribute(DEFAULT_ATTR_CODE)
				projectName = wireframe.user_attribute(DSN_ATTR_PROJECT)
				selectedProjects.add(projectName)
				purpose = self.getPurposeByCategoryAndCode(category, code)

				if not purpose in purposePriorityDict:
					continue

				wfdata.append([tripath, wfname, projectName, title, stage, purposePriorityDict[purpose]])
			tridb.close()

		projectList = list(sorted(selectedProjects))
		# сортируем каркасы по ПРОЕКТУ, КАТЕГОРИИ, ТИТУЛУ и СТАДИИ
		wfdata.sort(key = lambda x: (x[2], x[-1], x[3], -x[4]))

		if groupByProject:
			# создаем наборы по проекту
			ret = []
			for projectName in projectList:
				data = [[row[0], DEFAULT_ATTR_NAME, row[1]] for row in wfdata if row[2] == projectName]
				#
				wfset = SvyWireframeFormSet()
				wfset.useWildcards()
				wfset.setWireframeSetData(data)
				wfset.setDisplayName(projectName)
				#
				ret.append(wfset)
			return ret
		else:
			# создаем единый набор каркасов
			data = [[row[0], DEFAULT_ATTR_NAME, row[1]] for row in wfdata]
			#
			wfset = SvyWireframeFormSet()
			wfset.useWildcards()
			wfset.setWireframeSetData(data)
			wfset.setDisplayName("Каркасы")
			return wfset

	def generateSvyWireframeSets(self, tridbPathsDict, bmCurrentDate, isReport):
		progress = self.main_app.mainWidget.progressBar
		#
		currWorkPurpose, currWorkCategory = self.getCurrWorkPurpose(), self.getCurrWorkCategory()
		backfillPurpose, backfillCategory = self.getBackfillPurpose(), self.getBackfillCategory()
		allMiningPurposes = self.getMineWorkPurposes()
		#
		purposeCategoryDict = self.getMineWorkPurposeCategoryDict()
		purposeCodeDict = self.getMineWorkPurposeCodeDict()
		purposePriorityDict = self.getMineWorkPurposePriorityDict()

		progress.setText("Считывание кодов и категорий")
		# формируем список каркасов для сортировки
		wfdata = []
		for tridbPath in tridbPathsDict:
			tridb = Tridb(tridbPath)
			#
			for wfname in tridbPathsDict[tridbPath]:
				wireframe = tridb[wfname]
				#
				category = wireframe.user_attribute(SVY_ATTR_CATEGORY)
				code = wireframe.default_attribute(DEFAULT_ATTR_CODE)
				purpose = self.getPurposeByCategoryAndCode(category, code)
				if not purpose in allMiningPurposes:
					continue
				wfdate = wireframe.user_attribute(SVY_ATTR_DATE)
				#
				try:		wfdate = datetime.strptime(wfdate, "%Y-%m-%d")
				except:		continue

				# пропускаем каркас, у которого дата не соответствует дате блочной модели
				if wfdate.year != bmCurrentDate.year or wfdate.month != bmCurrentDate.month:
					continue
				# если встретился каркас оперативного учета
				if category == currWorkCategory:
					# если не в режиме отчета, то пропускаем каркас
					if not isReport:
						continue
					# в противном случае переопределяем НАЗНАЧЕНИЕ,
					# чтобы оно соответствовало НАЗНАЧЕНИЮ пустоты
					purpose = self.getEmptyPurposeByCode(code)
				# если код не соответствует пустоте
				if not code in purposeCodeDict[purpose]:
					continue

				priority = purposePriorityDict[purpose]
				isBackfill = purpose == backfillPurpose
				wfdata.append([tridbPath, wfname, wfdate, isBackfill, priority])
			tridb.close()

		# сортируем по дате и приоритету
		wfdata.sort(key = lambda x: (x[2], x[-1]))
		
		if not wfdata:
			raise Exception("Ни один из выбранных каркасов не содержит информации по горным работам")

		# список данных для наборов
		# собираются отдельные наборы по закладке и пустоте в хронологическом порядке
		# если встретилась закладка, то создается новый набор, состоящий только из закладки, пока не встретится пустота
		# если встретилась пустота, то создается новый набор, состоящий только из пустоты, пока не встретится закладка
		wfsetsList = []
		wfsetData = [wfdata[0]]
		for row in wfdata[1:]:
			if row[-2] == wfsetData[-1][-2]:
				wfsetData.append(row)
			else:
				wfsetsList.append(wfsetData)
				wfsetData = [row]
		#
		if wfsetData:
			wfsetsList.append(wfsetData)

		# подготавливаем набор по пустоте в обратной хронологической последовательности
		# и в прямой по приоритетности (начинаем с самых старых КК и заканчиваем самыми свежими ПРОХ),
		# отдельно формируем наборы с закладкой, расположенные в прямой хронологической последовательности
		wireframeSets = []
		for i, wfsetlist in enumerate(wfsetsList):
			if not wfsetlist:
				wfset = None
				continue
			#
			datefr, dateto = wfsetlist[0][2], wfsetlist[-1][2]
			strDateFr = "%s.%s" % (str(datefr.day).zfill(2), str(datefr.month).zfill(2))
			strDateTo = "%s.%s" % (str(dateto.day).zfill(2), str(dateto.month).zfill(2))
			#
			isBackfill = wfsetlist[0][-2] == True
			#
			wfset = SvyWireframeFormSet()
			wfset.setIsBackfillSet(isBackfill)
			#
			if isBackfill:
				data = [[line[0], DEFAULT_ATTR_NAME, line[1]] for line in wfsetlist]
				if datefr != dateto:	displayName = "%s-%s Закладка" % (strDateFr, strDateTo)
				else:					displayName = "%s Закладка" % (strDateFr)
			else:
				data = [[line[0], DEFAULT_ATTR_NAME, line[1]] for line in sorted(wfsetlist, key = lambda x: (x[2], -x[-1]), reverse = True)]
				if datefr != dateto:	displayName = "%s-%s Выемка" % (strDateFr, strDateTo)
				else:					displayName = "%s Выемка" % (strDateFr)

			wfset.setWireframeSetData(data)
			wfset.setDisplayName(displayName)
			wfset.useWildcards()
			wireframeSets.append(wfset)

		return wireframeSets

	def generateCustomWireframeSets(self, tridbPathsDict, removeTemporaryPrefix = True):
		# Внимание! Изменяет значение стандартного атрибута Title
		# В Title прописывается имя tridb файла без расширения
		wfdata = []
		for tridbPath in tridbPathsDict:
			tridb = Tridb(tridbPath)
			#
			basename = os.path.basename(tridbPath)
			title = basename[:basename.rfind(".")] if "." in basename else basename
			if removeTemporaryPrefix:
				title = title[len(get_temporary_prefix()):]
			#
			for wfname in tridbPathsDict[tridbPath]:
				wireframe = tridb[wfname]
				wireframe.set_default_attribute(DEFAULT_ATTR_TITLE, title)
				wfdata.append([tridbPath, DEFAULT_ATTR_NAME, wfname])
			tridb.commit()
			tridb.close()
		#
		wfset = SvyWireframeFormSet()
		wfset.setWireframeSetData(wfdata)
		return [wfset]

	### =================================================================================== ###
	### ============================= РАБОТА С БЛОЧНЫМИ МОДЕЛЯМИ ========================== ###
	### =================================================================================== ###

	def addFieldsToBlockModel(self, bmPath, structure):
		#
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл '%s'" % bmPath)
		bmStructure = bmFile.structure
		#
		types = {ft[0]: MMpy.FieldType.names[ft] for ft in MMpy.FieldType.names}
		for fld in structure:
			name, ft, width, prec = fld.split(":")
			bmStructure.add_field(name, types[ft.lower()], int(width), int(prec))
		bmFile.structure = bmStructure
		bmFile.close()

	def removeFieldsFromBlockModel(self, bmPath, userFields):
		progress = self.main_app.mainWidget.progressBar
		progress.setText("Редактирование структуры блочной модели. Удаление полей")
		#
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл '%s'" % bmPath)
		#
		bmHeader = bmFile.header
		bmStructure = bmFile.structure
		fieldIndexes = sorted([bmHeader.index(userField) for userField in userFields if userField in bmHeader], reverse = True)
		for fieldIndex in fieldIndexes:
			bmStructure.delete_field(fieldIndex)
		bmFile.structure = bmStructure
		bmFile.close()

	def copyDsnFieldValues(self, bmPath, rowIndexes = None, clearSource = False):
		systemFields = BMSYSFIELDS_DSN
		requiredFields = BMFIELDS_DSN
		#
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл '%s'" % bmPath)
		bmHeader = bmFile.header
		#
		systemFieldsInHeader = [systemField in bmHeader for systemField in systemFields]
		if not all(systemFieldsInHeader):
			bmFile.close()
			raise Exception("Поле '%s' отсутствует" % systemFields[systemFieldsInHeader.index(False)])
		requiredFieldsInHeader = [requiredField in bmHeader for requiredField in requiredFields]
		if not all(requiredFieldsInHeader):
			bmFile.close()
			raise Exception("Поле '%s' отсутствует" % requiredFields[requiredFieldsInHeader.index(False)])

		# индексы системных полей
		idxSysProjectCategory = bmHeader.index(BMFLD_SYS_PROJECT_CATEGORY)
		idxSysProjectWorkType = bmHeader.index(BMFLD_SYS_PROJECT_WORKTYPE)
		idxSysProjectMineName = bmHeader.index(BMFLD_SYS_PROJECT_MINENAME)
		idxSysProjectName = bmHeader.index(BMFLD_SYS_PROJECT_NAME)
		idxSysProjectStage = bmHeader.index(BMFLD_SYS_PROJECT_STAGE)
		idxSysProjectInterval = bmHeader.index(BMFLD_SYS_PROJECT_INTERVAL)
		systemIndexes = (idxSysProjectCategory, idxSysProjectWorkType, idxSysProjectMineName, idxSysProjectName, idxSysProjectStage, idxSysProjectInterval)
		# индексы полей блочной модели
		idxProjectCategory = bmHeader.index(BMFLD_PROJECT_CATEGORY)
		idxProjectWorkType = bmHeader.index(BMFLD_PROJECT_WORKTYPE)
		idxProjectMineName = bmHeader.index(BMFLD_PROJECT_MINENAME)
		idxProjectName = bmHeader.index(BMFLD_PROJECT_NAME)
		idxProjectStage = bmHeader.index(BMFLD_PROJECT_STAGE)
		idxProjectInterval = bmHeader.index(BMFLD_PROJECT_INTERVAL)
		projectIndexes = (idxProjectCategory, idxProjectWorkType, idxProjectMineName, idxProjectName, idxProjectStage, idxProjectInterval)

		#
		rowIndexRange = tuple(range(bmFile.records_count)) if rowIndexes is None else tuple(rowIndexes)
		step = len(rowIndexRange) // 100 + 1
		for rowIndex in rowIndexRange:
			if rowIndex % step == 0:
				QtCore.QCoreApplication.processEvents()
			# перенос данных
			for projectIndex, systemIndex in zip(projectIndexes, systemIndexes):
				value = bmFile.get_str_field_value(systemIndex, rowIndex+1)
				bmFile.set_field_value(projectIndex, rowIndex+1, value)
			# очистка системных полей
			if clearSource:
				for systemIndex in systemIndexes:
					bmFile.set_field_value(systemIndex, rowIndex+1, "")
		bmFile.close()

	def copySvyFieldValues(self, bmPath, BMFLD_CURRDATE, rowIndexes = None, onlyMaterial = False, clearSource = False):
		BMFLD_SYS_CURRDATE = "SYS_%s" % BMFLD_CURRDATE
		bmfld_MINEBINDATTRNAME = self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper()
		bmfld_sys_MINEBINDATTRNAME = "SYS_%s" % bmfld_MINEBINDATTRNAME
		systemFields = (BMFLD_SYS_CURRDATE, BMFLD_SYS_FACTWORK, BMFLD_SYS_MINENAME, BMFLD_SYS_FACTDATE, BMFLD_SYS_CURRMINED,
									BMFLD_SYS_INTERVAL, BMFLD_SYS_SVYBLOCK, bmfld_sys_MINEBINDATTRNAME)
		requiredFields = (
			BMFLD_MATERIAL, BMFLD_INDEX, BMFLD_DENSITY,									# геология
			BMFLD_FACTWORK, BMFLD_FACTDATE, BMFLD_MINENAME, BMFLD_INTERVAL,				# фактические работы
			BMFLD_MINEWORK, BMFLD_MINEDATE, BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL,	# выемочные работы
			BMFLD_SVYBLOCK, bmfld_MINEBINDATTRNAME, BMFLD_CURRDATE, BMFLD_CURRMINED 			
		)

		materialCodes = self.getMaterialCodes()
		concDensity = self.main_app.settings.getValue(ID_CONCDENS)
		#
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл '%s'" % bmPath)
		bmHeader = bmFile.header
		#
		systemFieldsInHeader = [systemField in bmHeader for systemField in systemFields]
		if not all(systemFieldsInHeader):
			raise Exception("Поле '%s' отсутствует" % systemFields[systemFieldsInHeader.index(False)])
		requiredFieldsInHeader = [requiredField in bmHeader for requiredField in requiredFields]
		if not all(requiredFieldsInHeader):
			raise Exception("Поле '%s' отсутствует" % requiredFields[requiredFieldsInHeader.index(False)])
		# индексы системных полей
		idxSysCurrMined = bmHeader.index(BMFLD_SYS_CURRMINED)
		idxSysCurrDate = bmHeader.index(BMFLD_SYS_CURRDATE)
		idxSysFactWork = bmHeader.index(BMFLD_SYS_FACTWORK)
		idxSysMineName = bmHeader.index(BMFLD_SYS_MINENAME)
		idxSysInterval = bmHeader.index(BMFLD_SYS_INTERVAL)
		idxSysFactDate = bmHeader.index(BMFLD_SYS_FACTDATE)
		idxSysSvyBlock = bmHeader.index(BMFLD_SYS_SVYBLOCK)
		idxSysLongwall = bmHeader.index("SYS_%s" % self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
		systemIndexes = (idxSysCurrMined, idxSysCurrDate, idxSysFactWork, idxSysMineName, idxSysInterval, idxSysFactDate, idxSysSvyBlock, idxSysLongwall)
		# индексы полей блочной модели
		idxMaterial = bmHeader.index(BMFLD_MATERIAL)
		idxIndex = bmHeader.index(BMFLD_INDEX)
		idxDensity = bmHeader.index(BMFLD_DENSITY)
		idxCurrMined = bmHeader.index(BMFLD_CURRMINED)
		idxCurrDate = bmHeader.index(BMFLD_CURRDATE)
		idxMineWork = bmHeader.index(BMFLD_MINEWORK)
		idxFactWork = bmHeader.index(BMFLD_FACTWORK)
		idxMineMineName = bmHeader.index(BMFLD_MINE_MINENAME)
		idxMineName = bmHeader.index(BMFLD_MINENAME)
		idxMineInterval = bmHeader.index(BMFLD_MINE_INTERVAL)
		idxInterval = bmHeader.index(BMFLD_INTERVAL)
		idxMineDate = bmHeader.index(BMFLD_MINEDATE)
		idxFactDate = bmHeader.index(BMFLD_FACTDATE)
		idxSvyBlock = bmHeader.index(BMFLD_SVYBLOCK)
		idxLongwall = bmHeader.index(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
		#
		rowIndexRange = tuple(range(bmFile.records_count)) if rowIndexes is None else tuple(rowIndexes)
		step = len(rowIndexRange) // 100 + 1
		for rowIndex in rowIndexRange:
			if rowIndex % step == 0:
				QtCore.QCoreApplication.processEvents()
			# если необходимо переносить данные только для строк, где в текущем месяце материал
			currCategory = bmFile.get_str_field_value(idxCurrDate, rowIndex+1)
			if onlyMaterial and currCategory not in materialCodes:
				continue
			# системные значения
			sysFactWork = bmFile.get_str_field_value(idxSysFactWork, rowIndex+1)
			sysFactDate = bmFile.get_str_field_value(idxSysFactDate, rowIndex+1)
			sysLongwall = bmFile.get_str_field_value(idxSysLongwall, rowIndex+1)
			sysSvyBlock = bmFile.get_str_field_value(idxSysSvyBlock, rowIndex+1)
			sysMineName = bmFile.get_str_field_value(idxSysMineName, rowIndex+1)
			sysInterval = bmFile.get_str_field_value(idxSysInterval, rowIndex+1)
			sysCurrDate = bmFile.get_str_field_value(idxSysCurrDate, rowIndex+1)
			sysCurrMined = bmFile.get_str_field_value(idxSysCurrMined, rowIndex+1)

			# перенос	
			bmFile.set_field_value(idxFactDate, rowIndex+1, sysFactDate)
			bmFile.set_field_value(idxFactWork, rowIndex+1, sysFactWork)
			bmFile.set_field_value(idxMineName, rowIndex+1, sysMineName)
			bmFile.set_field_value(idxInterval, rowIndex+1, sysInterval)
			bmFile.set_field_value(idxLongwall, rowIndex+1, sysLongwall)
			bmFile.set_field_value(idxSvyBlock, rowIndex+1, sysSvyBlock)
			bmFile.set_field_value(idxCurrDate, rowIndex+1, sysCurrDate)
			bmFile.set_field_value(idxCurrMined, rowIndex+1, sysCurrMined)
			# если присваивается бетон
			if sysCurrDate == self.getBackfillCategory():
				bmFile.set_field_value(idxMaterial, rowIndex+1, sysCurrDate)
				bmFile.set_field_value(idxIndex, rowIndex+1, sysFactWork)
				bmFile.set_field_value(idxDensity, rowIndex+1, concDensity)
			else:
				bmFile.set_field_value(idxMineMineName, rowIndex+1, sysMineName)
				bmFile.set_field_value(idxMineInterval, rowIndex+1, sysInterval)
				bmFile.set_field_value(idxMineWork, rowIndex+1, sysFactWork)
				bmFile.set_field_value(idxMineDate, rowIndex+1, sysFactDate)
			# очистка системных полей
			if clearSource:
				for systemIndex in systemIndexes:
					bmFile.set_field_value(systemIndex, rowIndex+1, "")
		bmFile.close()
	
	def extractBlockModelData(self, bmInPath, keyFields, keyValues, bmOutPath = None, rowIndexRange = None):
		bmInFile = MicromineFile()
		if not bmInFile.open(bmInPath):
			raise Exception("Невозможно открыть файл '%s'" % bmInPath)
		#
		bmHeader = bmInFile.header
		bmStructure = bmInFile.structure
		keyFieldsInHeader = tuple(keyField in bmHeader for keyField in keyFields)
		if not all(keyFieldsInHeader):
			bmInFile.close()
			raise Exception("Поле '%s' отсутствует" % keyFields[keyFieldsInHeader.index(False)])
		getString = bmInFile.get_str_field_value
		getNumber = bmInFile.get_num_field_value
		#
		keyIndexes = [bmHeader.index(keyField) for keyField in keyFields]
		keyValues = tuple(tuple(row) for row in keyValues)
		columnCount = len(bmHeader)
		#
		bmData = []
		numIndexes = [columnIndex for columnIndex in range(bmInFile.structure.fields_count) if bmInFile.structure.get_field_type(columnIndex) != MMpy.FieldType.character]
		rowIndexRange = range(bmInFile.records_count) if rowIndexRange is None else rowIndexRange

		for rowIndex in rowIndexRange:
			key = tuple(getString(keyIndex, rowIndex+1) for keyIndex in keyIndexes)
			keyFieldsAreBlank = tuple(is_blank(value) for value in key)
			if not key in keyValues:
				continue
			bmData.append([getString(columnIndex, rowIndex+1) if columnIndex not in numIndexes else getNumber(columnIndex, rowIndex+1) for columnIndex in range(columnCount)])
		#
		if bmOutPath is not None:
			MicromineFile.write(bmOutPath, bmData, structure = bmStructure)
		#
		return bmData

	def getRowIndexesByKeys(self, bmPath, keyFields, keyValues, rowIndexRange = None):
		# Извлекает индексы строк БМ при совпадении keyValues в keyFields.
		# Только для строковых значений. Числовые значения обрабатываются,
		# как строки.
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть фал %s" % bmPath)
		bmHeader = bmFile.header
		fieldsInHeader = [keyField in bmHeader for keyField in keyFields]
		if not all(fieldsInHeader):
			bmFile.close()
			raise Exception("Поле '%s' отсутствует" % keyFields[fieldsInHeader.index(False)])
		#
		keyIndexes = [bmHeader.index(keyField) for keyField in keyFields]
		keyValues = tuple(tuple(row) for row in keyValues)
		rowIndexes = []
		rowIndexRange = tuple(range(bmFile.records_count)) if rowIndexRange is None else tuple(rowIndexRange)
		#
		step = len(rowIndexRange) // 100 + 1
		for rowIndex in rowIndexRange:
			if rowIndex % step == 0:
				QtCore.QCoreApplication.processEvents()
			#
			key = tuple(bmFile.get_str_field_value(keyIndex, rowIndex+1) for keyIndex in keyIndexes)
			if key in keyValues:
				rowIndexes.append(rowIndex)
		bmFile.close()
		#
		return rowIndexes

	def restoreMiningWorks(self, bmPath, keyIndexes, BMFLD_CURRDATE, rowIndexRange = None, restorePrevDate = False):
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл %s" % bmPath)
		bmHeader = bmFile.header

		purposeCategoryDict = self.getMineWorkPurposeCategoryDict()
		#
		reqFields = (BMFLD_CURRDATE, BMFLD_GEOMATERIAL, BMFLD_GEOINDEX, BMFLD_MATERIAL, BMFLD_INDEX, BMFLD_DENSITY, BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL)
		fieldsInHeader = [reqField in bmHeader for reqField in reqFields]
		if not all(fieldsInHeader):
			bmFile.close()
			raise Exception("Поле '%s' отсутствует" % reqFields[fieldsInHeader.index(False)])
		#
		idxCurrDate = bmHeader.index(BMFLD_CURRDATE)
		idxPrevDate = bmHeader.index(prev_date(BMFLD_CURRDATE))
		idxDensity = bmHeader.index(BMFLD_DENSITY)
		idxGeoMaterial, idxMaterial = bmHeader.index(BMFLD_GEOMATERIAL), bmHeader.index(BMFLD_MATERIAL)		# материал
		idxGeoIndex, idxIndex = bmHeader.index(BMFLD_GEOINDEX), bmHeader.index(BMFLD_INDEX)					# индек
		idxMineMineName, idxMineName = bmHeader.index(BMFLD_MINE_MINENAME), bmHeader.index(BMFLD_MINENAME)	# выработка
		idxMineInterval, idxInterval = bmHeader.index(BMFLD_MINE_INTERVAL), bmHeader.index(BMFLD_INTERVAL) 	# интервал
		idxMineWork, idxFactWork = bmHeader.index(BMFLD_MINEWORK), bmHeader.index(BMFLD_FACTWORK)			# работы
		idxMineDate, idxFactDate = bmHeader.index(BMFLD_MINEDATE), bmHeader.index(BMFLD_FACTDATE)			# дата
		
		#
		rowIndexRange = tuple(range(bmFile.records_count)) if rowIndexRange is None else tuple(rowIndexRange)
		step = len(rowIndexRange) // 100 + 1
		for rowIndex in rowIndexRange:
			if rowIndex % step == 0:
				QtCore.QCoreApplication.processEvents()
			#
			if not rowIndex in keyIndexes:
				continue
			# восстанавливаемые значения
			geoMaterial = bmFile.get_str_field_value(idxGeoMaterial, rowIndex+1)
			geoIndex = bmFile.get_str_field_value(idxGeoIndex, rowIndex+1)
			density = self.getDensityByMaterialAndIndex(geoMaterial, geoIndex)
			if density is None:
				bmFile.close()
				raise Exception("Невозможно определить плотность для (%s, %s). Строка %d" % (geoMaterial, geoIndex, rowIndex+1))
			mineMineName = bmFile.get_str_field_value(idxMineMineName, rowIndex+1)
			mineInterval = bmFile.get_str_field_value(idxMineInterval, rowIndex+1)
			mineWork = bmFile.get_str_field_value(idxMineWork, rowIndex+1)
			mineDate = bmFile.get_str_field_value(idxMineDate, rowIndex+1)
			# восстанавливаем выемку
			bmFile.set_field_value(idxDensity, rowIndex+1, density)
			bmFile.set_field_value(idxMaterial, rowIndex+1, geoMaterial)
			bmFile.set_field_value(idxIndex, rowIndex+1, geoIndex)
			# если есть выемка
			if not is_blank(mineMineName):
				bmFile.set_field_value(idxMineName, rowIndex+1, mineMineName)		# 
				bmFile.set_field_value(idxInterval, rowIndex+1, mineInterval)
				bmFile.set_field_value(idxFactWork, rowIndex+1, mineWork)
				bmFile.set_field_value(idxFactDate, rowIndex+1, mineDate)
				bmFile.set_field_value(idxCurrDate, rowIndex+1, purposeCategoryDict[self.getEmptyPurposeByCode(mineWork)])
			# если нет выемки
			else:
				bmFile.set_field_value(idxCurrDate, rowIndex+1, geoMaterial)
				if restorePrevDate:
					bmFile.set_field_value(idxPrevDate, rowIndex+1, geoMaterial)
		#
		bmFile.close()

	def restoreGeology(self, bmPath, keyIndexes, BMFLD_CURRDATE, rowIndexRange = None):
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл %s" % bmPath)
		bmHeader = bmFile.header

		purposeCategoryDict = self.getMineWorkPurposeCategoryDict()
		#
		reqFields = (BMFLD_CURRDATE, BMFLD_GEOMATERIAL, BMFLD_GEOINDEX, BMFLD_MATERIAL, BMFLD_INDEX, BMFLD_DENSITY, BMFLD_MINE_MINENAME, BMFLD_MINE_INTERVAL)
		fieldsInHeader = [reqField in bmHeader for reqField in reqFields]
		if not all(fieldsInHeader):
			bmFile.close()
			raise Exception("Поле '%s' отсутствует" % reqFields[fieldsInHeader.index(False)])
		#
		idxCurrDate = bmHeader.index(BMFLD_CURRDATE)
		idxPrevDate = bmHeader.index(prev_date(BMFLD_CURRDATE))
		idxDensity = bmHeader.index(BMFLD_DENSITY)
		idxGeoMaterial, idxMaterial = bmHeader.index(BMFLD_GEOMATERIAL), bmHeader.index(BMFLD_MATERIAL)		# материал
		idxGeoIndex, idxIndex = bmHeader.index(BMFLD_GEOINDEX), bmHeader.index(BMFLD_INDEX)					# индек
		idxMineMineName, idxMineName = bmHeader.index(BMFLD_MINE_MINENAME), bmHeader.index(BMFLD_MINENAME)	# выработка
		idxMineInterval, idxInterval = bmHeader.index(BMFLD_MINE_INTERVAL), bmHeader.index(BMFLD_INTERVAL) 	# интервал
		idxMineWork, idxFactWork = bmHeader.index(BMFLD_MINEWORK), bmHeader.index(BMFLD_FACTWORK)			# работы
		idxMineDate, idxFactDate = bmHeader.index(BMFLD_MINEDATE), bmHeader.index(BMFLD_FACTDATE)			# дата
		idxCurrMined, idxTotalMined = bmHeader.index(BMFLD_CURRMINED), bmHeader.index(BMFLD_TOTALMINED)
		idxSvyBlock = bmHeader.index(BMFLD_SVYBLOCK)
		idxSvyMineBindAttr = bmHeader.index(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
		
		#
		rowIndexRange = tuple(range(bmFile.records_count)) if rowIndexRange is None else tuple(rowIndexRange)
		step = len(rowIndexRange) // 100 + 1
		for rowIndex in rowIndexRange:
			if rowIndex % step == 0:
				QtCore.QCoreApplication.processEvents()
			#
			if not rowIndex in keyIndexes:
				continue
			# восстанавливаемые значения
			geoMaterial = bmFile.get_str_field_value(idxGeoMaterial, rowIndex+1)
			geoIndex = bmFile.get_str_field_value(idxGeoIndex, rowIndex+1)
			density = self.getDensityByMaterialAndIndex(geoMaterial, geoIndex)
			if density is None:
				bmFile.close()
				raise Exception("Невозможно определить плотность для (%s, %s). Строка %d" % (geoMaterial, geoIndex, rowIndex+1))


			bmFile.set_field_value(idxMaterial, rowIndex+1, geoMaterial)	
			bmFile.set_field_value(idxIndex, rowIndex+1, geoIndex)
			bmFile.set_field_value(idxDensity, rowIndex+1, density)
			
			bmFile.set_field_value(idxMineName, rowIndex+1, "")
			bmFile.set_field_value(idxInterval, rowIndex+1, "")
			bmFile.set_field_value(idxMineDate, rowIndex+1, "")
			bmFile.set_field_value(idxMineWork, rowIndex+1, "")

			bmFile.set_field_value(idxSvyBlock, rowIndex+1, "")
			bmFile.set_field_value(idxSvyMineBindAttr, rowIndex+1, "")
			bmFile.set_field_value(idxMineMineName, rowIndex+1, "")
			bmFile.set_field_value(idxMineInterval, rowIndex+1, "")
			bmFile.set_field_value(idxFactDate, rowIndex+1, "")
			bmFile.set_field_value(idxFactWork, rowIndex+1, "")
			
			bmFile.set_field_value(idxPrevDate, rowIndex+1, geoMaterial)
			bmFile.set_field_value(idxCurrDate, rowIndex+1, geoMaterial)
			bmFile.set_field_value(idxCurrMined, rowIndex+1, "")
			bmFile.set_field_value(idxTotalMined, rowIndex+1, "")

		bmFile.close()

	def simulateConcrete(self, bmPath, keyIndexes, BMFLD_CURRDATE, rowIndexRange = None):
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл %s" % bmPath)
		bmHeader = bmFile.header

		#purposeCategoryDict = self.getMineWorkPurposeCategoryDict()
		#
		reqFields = (BMFLD_CURRDATE,  BMFLD_MATERIAL, BMFLD_INDEX, BMFLD_DENSITY)
		fieldsInHeader = [reqField in bmHeader for reqField in reqFields]
		if not all(fieldsInHeader):
			bmFile.close()
			raise Exception("Поле '%s' отсутствует" % reqFields[fieldsInHeader.index(False)])
		#
		idxCurrDate = bmHeader.index(BMFLD_CURRDATE)
		idxPrevDate = bmHeader.index(prev_date(BMFLD_CURRDATE))
		idxDensity = bmHeader.index(BMFLD_DENSITY)
		idxGeoMaterial, idxMaterial = bmHeader.index(BMFLD_GEOMATERIAL), bmHeader.index(BMFLD_MATERIAL)		# материал
		idxGeoIndex, idxIndex = bmHeader.index(BMFLD_GEOINDEX), bmHeader.index(BMFLD_INDEX)					# индек
		idxMineMineName, idxMineName = bmHeader.index(BMFLD_MINE_MINENAME), bmHeader.index(BMFLD_MINENAME)	# выработка
		idxMineInterval, idxInterval = bmHeader.index(BMFLD_MINE_INTERVAL), bmHeader.index(BMFLD_INTERVAL) 	# интервал
		idxMineWork, idxFactWork = bmHeader.index(BMFLD_MINEWORK), bmHeader.index(BMFLD_FACTWORK)			# работы
		idxMineDate, idxFactDate = bmHeader.index(BMFLD_MINEDATE), bmHeader.index(BMFLD_FACTDATE)			# дата
		idxCurrMined, idxTotalMined = bmHeader.index(BMFLD_CURRMINED), bmHeader.index(BMFLD_TOTALMINED)
		idxSvyBlock = bmHeader.index(BMFLD_SVYBLOCK)
		idxSvyMineBindAttr = bmHeader.index(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
		
		#
		rowIndexRange = tuple(range(bmFile.records_count)) if rowIndexRange is None else tuple(rowIndexRange)
		step = len(rowIndexRange) // 100 + 1
		for rowIndex in rowIndexRange:
			if rowIndex % step == 0:
				QtCore.QCoreApplication.processEvents()
			#
			if not rowIndex in keyIndexes:
				continue
			# восстанавливаемые значения
			geoMaterial = 'ЗАКЛ'
			geoIndex = 'ЗАКЛ'
			density = self.getDensityByMaterialAndIndex(geoMaterial, geoIndex)
			if density is None:
				bmFile.close()
				raise Exception("Невозможно определить плотность для (%s, %s). Строка %d" % (geoMaterial, geoIndex, rowIndex+1))

			# Не менять idxMaterial ибо далее при перемещении данных не находятся индексы строк БМ, так как МАТЕРИАЛ входит в ключ
			# bmFile.set_field_value(idxMaterial, rowIndex+1, geoMaterial) 	
			bmFile.set_field_value(idxIndex, rowIndex+1, geoIndex)
			bmFile.set_field_value(idxDensity, rowIndex+1, density)

		bmFile.close()


	### =================================================================================== ###
	### ============================= РАБОТА С ФАЙЛАМИ БЛОКИРОВКИ ========================= ###
	### =================================================================================== ###


	def getLockPath(self, path):
		return path + LOCK_EXTENSION
	def lockFileExists(self, path):
		lockpath = self.getLockPath(path)
		return os.path.exists(lockpath)
	def createLockFile(self, path):
		lockpath = self.getLockPath(path)
		if self.lockFileExists(path):
			owner = get_file_owner(lockpath)
			raise Exception("Блочная модель используется пользователем '%s'" % owner)
		else:
			try:					self._lockfile[path] = open(lockpath, "w")
			except Exception as e:	raise Exception("Не удалось создать файл блокировки. %s" % str(e))
	def createLockFiles(self, filepaths):
		for filepath in filepaths:
			self.createLockFile(filepath)
	def removeLockFile(self, path):
		lockpath = self.getLockPath(path)
		if not path in self._lockfile:
			return
		self._lockfile[path].close()
		os.remove(lockpath)
		self._lockfile.pop(path)
	# удаление LCK файлов
	def removeLockFiles(self):
		locked_path = list(self._lockfile.keys())
		for path in locked_path:
			self.removeLockFile(path)

	### =================================================================================== ###
	### ================================= КОПИРОВАНИЕ ФАЙЛОВ ============================== ###
	### =================================================================================== ###

	def copy_file(self, srcpath, dstpath):
		fsize = os.path.getsize(srcpath)
		step = round(fsize / 100)
		srcfile, dstfile = open(srcpath, "rb"), open(dstpath, "wb")
		iniStatus = self.main_app.mainWidget.progressBar.text()
		currsize = 0
		while currsize < fsize:
			if self._is_cancelled:
				srcfile.close()
				dstfile.close()
				os.remove(dstpath)
				return
			QtCore.QCoreApplication.processEvents()
			self.main_app.mainWidget.progressBar.setText(iniStatus + " (%.2f/%.2f Мб)" % (currsize / 1024 / 1024, fsize / 1024 / 1024))
			currsize += step
			dstfile.write(srcfile.read(step))
		srcfile.close()
		dstfile.close()
		#
		self.main_app.mainWidget.progressBar.setText(iniStatus)

	@property
	def is_success(self):		return self._is_success
	@property
	def is_running(self):		return self._is_running
	@property
	def mm_running(self):		return self._mm_running
	@property
	def is_cancelled(self):		return self._is_cancelled

	@property
	def process_name(self):			return self._process_name
	@property
	def main_app(self):				return iterWidgetParents(self)[-1]

	def selectedItems(self):		return self._selectedItems.copy()

	def check(self):
		if self._is_running:
			return False
		self._is_cancelled = False
		self._is_success = False
		
		return True

	def prepare(self):
		if self._is_running:
			return False
		self._is_cancelled = False
		self._is_success = False
		
		# считывание выбранных файлов
		self._selectedItems = {}
		for ITEM_TYPE in ITEM_TYPE_LIST:
			self._selectedItems[ITEM_TYPE] = tuple(it for it in self.main_app.mainWidget.selectionExplorer.explorer.iterChildren() if it.itemType() == ITEM_TYPE)
		#
		self._tmpprefix = get_temporary_prefix()
		return True

	def run(self):
		# удаление временной директории
		if os.path.exists(self._tmpfolder):
			if self._cleartemp:
				for filename in os.listdir(self._tmpfolder):
					path = os.path.join(self._tmpfolder, filename)
					try:			os.remove(path)
					except:			pass
		# создание временной директории
		else:
			try:					os.mkdir(self._tmpfolder)
			except Exception as e:	raise Exception("НЕРЕАЛЬНО создать временную директорию. %s" % str(e))
		return True


	def after_run(self):
		self.removeLockFiles()

		self.main_app.mainWidget.progressBar.setText("")
		self.main_app.mainWidget.progressBar.setRange(0, 100)
		self.main_app.mainWidget.progressBar.setValue(0)

		self._in_process = False
		# self._is_cancelled = False
		self._is_running = False

class DialogWindow(BaseDialog, ProcessWindow):
	def __init__(self, parent, use_connection = True):
		super(DialogWindow, self).__init__(parent)
		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint | QtCore.Qt.MSWindowsFixedSizeDialogHint)

		self._use_connection = use_connection

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		# рабочая директория (сетевой проект)
		self._wrkpath = self.main_app.settings.getValue(ID_NETPATH)
		# временная директория
		self._tmpprefix = ""
		self._cleartemp = True
		try:			self._tmpfolder = MMpy.Project.path() + "TEMPORARY_SCRIPT_FOLDER"
		except:			self._tmpfolder = "D:\\TEMPORARY_SCRIPT_FOLDER"

		self._is_running = False
		self._is_success = False
		self._in_process = False
		self._is_cancelled = False

		self._lockfile = {}
		self._selectedItems = {}

	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setContentsMargins(5,5,5,5)

		self.layoutControls = QtGui.QVBoxLayout()
		self.layoutControls.setContentsMargins(0,0,0,0)

		self.buttonBox = QtGui.QDialogButtonBox()
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumSize(QtCore.QSize(100, 25))
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumSize(QtCore.QSize(100, 25))
		self.buttonBox.setOrientation(QtCore.Qt.Vertical)

		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		self.buttonBox.setSizePolicy(sizePolicy)
	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addLayout(self.layoutControls, 1)
		self.layoutMain.addWidget(self.buttonBox, 0)

	def close(self):
		self._is_cancelled = True
		super(DialogWindow, self).close()
	