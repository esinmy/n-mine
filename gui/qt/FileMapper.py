from functools import wraps
from PyQt4 import QtCore, QtGui	
from gui.qt.dialogs.BaseDialog import BaseDialog
from mm.mmutils import *
from utils.constants import *
from utils.osutil import *
from utils.qtutil import *
import gui.qt.dialogs.messagebox as qtmsgbox
from collections import OrderedDict
import os
import sys



class ComboBoxDelegate(QtGui.QStyledItemDelegate):
	itemChanged = QtCore.pyqtSignal(object, int,str,str)
	def __init__(self,  parent=None):
		super(ComboBoxDelegate, self).__init__(parent)
		self._list = [{'Information':'', 'Name':''}]
	def createEditor(self, parent, options, index):
		editor = ComboBoxEditor(self._list,parent)
		editor.setFrame(False)
		editor.currentIndexChanged.connect(self.currentItemChanged)
		return editor

	def setEditorData(self, editor, index):
		value = index.model().data(index, QtCore.Qt.EditRole)
		idx = editor.findText(value,QtCore.Qt.MatchFixedString)
		editor.setCurrentIndex(idx if idx > 0 else 0)

	def updateEditorGeometry(self, editor, options, index):
		editor.setGeometry(options.rect)

	def setModelData(self, editor, model, index):
		text = editor.currentText()
		idx = editor.currentIndex()
		userData = editor.itemData(idx, QtCore.Qt.UserRole)
		model.setData(index, text, QtCore.Qt.EditRole)
		model.setData(index, userData, QtCore.Qt.UserRole)


	def currentItemChanged(self): 
		_index = self.sender().currentIndex()
		_text = self.sender().currentText()
		_data = self.sender().itemData(_index)
		self.itemChanged.emit(self.sender(), _index, _text, _data)

	def editorEvent(self, event, model, option, index):

		## Do not change the checkbox-state
		#if event.type() == QtCore.QEvent.MouseButtonRelease or event.type() == QtCore.QEvent.MouseButtonDblClick:
		#	if event.button() != QtCore.Qt.LeftButton or not self.getCheckBoxRect(option).contains(event.pos()):
		#		return False
		#	if event.type() == QtCore.QEvent.MouseButtonDblClick:
		#		return True
		#elif event.type() == QtCore.QEvent.KeyPress:
		#	if event.key() != QtCore.Qt.Key_Space and event.key() != QtCore.Qt.Key_Select:
		#		return False
		#else:
		#	return False
		#if event.type() not in (2,6,3,4):
		#	 i=1
			
		return False

	def setList(self, newList):
		__firstItem	  =  [{'Information':'', 'Name':''}]
		self._list = __firstItem
		if newList != None :
			self._list.extend( newList)

	

class ComboBoxEditor(QtGui.QComboBox):
	def __init__(self, DbObjects, parent = None, **kwargs):
		super(ComboBoxEditor, self).__init__(parent, **kwargs)
		for row in DbObjects:
			self.addItem(str(row['Name']), str(row['Information']))


class FileMapper(BaseDialog):

	def __init__(self, *args, **kw):
		super(FileMapper, self).__init__(*args, **kw)

		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
		self.setWindowTitle("Сопоставление файлов")

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.setFixedSize(self.sizeHint())

		self.buildViewTable()
	
		self.setModal(True)
	def __createVariables(self):
		self._model = []
		self._filePaths = []  
		self._dbObjects = []   

		self.headerMapper = OrderedDict ([
										( "file",{"header":"Файл",
													"modeSize":QtGui.QHeaderView.ResizeToContents,
													"hidden":False}),
										( "object", {"header":"Выработка",
																"modeSize":QtGui.QHeaderView.Stretch,
																"hidden":False})
			
																])

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setMargin(5)
		self.layoutMain.setAlignment(QtCore.Qt.AlignCenter)

		self.teMessage =  QtGui.QTextEdit()
		self.teMessage.setTextColor(QtGui.QColor('red'))
		self.teMessage.setEnabled(False)
		self.teMessage.setMaximumHeight(50)

		self.layoutOperation = QtGui.QHBoxLayout()

		self.layoutMapper = QtGui.QVBoxLayout()

		self.labelMapper = QtGui.QLabel("Файлы и объекты")

		self.tvFileMapper = QtGui.QTableView(self)
		self.tvFileMapper.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
		self.tvFileMapper.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
		self.tvFileMapper.setMinimumWidth(400)
		self.tvFileMapper.setMinimumHeight(400)
		self.tvFileMapper.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
		self.tvFileMapper.setAlternatingRowColors(True)

		self.comboBoxDelegate= ComboBoxDelegate()

		self.modelFileMapper = QtGui.QStandardItemModel(self)
		self.tvFileMapper.setModel(self.modelFileMapper)

		self.layoutInfo = QtGui.QVBoxLayout()

		self.labelInfo = QtGui.QLabel("Информация")

		self.teInformation =  QtGui.QTextEdit()
		self.teInformation.setEnabled(False)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
		self.buttonBox.setOrientation(QtCore.Qt.Horizontal )

	def __createBindings(self):
		self.tvFileMapper.selectionModel().selectionChanged.connect(self.on_selectionChanged_tvFileMapper)
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._saveAndClose)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.reject)
		self.comboBoxDelegate.itemChanged.connect(self.itemChanged_comboBoxDelegate)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.teMessage)
		self.layoutMain.addLayout(self.layoutOperation)
		self.layoutOperation.addLayout(self.layoutMapper)
		self.layoutOperation.addLayout(self.layoutInfo)


		self.layoutMapper.addWidget(self.labelMapper)
		self.layoutMapper.addWidget(self.tvFileMapper)

		self.layoutInfo.addWidget(self.labelInfo)
		self.layoutInfo.addWidget(self.teInformation )

		self.layoutMain.addWidget(self.buttonBox)

	def buildViewTable(self):
		self.modelFileMapper.clear()
		self.modelFileMapper.setHorizontalHeaderLabels([item["header"] for item in self.headerMapper.values()])
		for row in self._model :
			itemRow = []
			for field in self.headerMapper:	
				item = QtGui.QStandardItem(str(row[field]))
				item.setData({"path":row["path"]})
				itemRow.append(item)
			self.modelFileMapper.appendRow(itemRow)

		for i in range(0,len(self.headerMapper)):
			self.tvFileMapper.resizeColumnToContents(i)

		header = self.tvFileMapper.horizontalHeader ()
		for i, key in enumerate(self.headerMapper):
			header.setResizeMode(i, self.headerMapper[key]["modeSize"])
			if self.headerMapper[key]["hidden"]:  
				header.hideSection(i)	
		
		self.comboBoxDelegate.setList(self._dbObjects)
		self.tvFileMapper.setItemDelegateForColumn(1, self.comboBoxDelegate )   

	def itemChanged_comboBoxDelegate(self, sender, index, text, information):
		_index = self.tvFileMapper.selectedIndexes()[0]
		_userData = _index.model().itemFromIndex(_index).data()
		_path  = _userData["path"]
		self.teInformation.setText("<b>Файл:</b> <br/>"+_path)

		self.teInformation.append("<b>Объект:</b> <br/>"+information)

	def on_selectionChanged_tvFileMapper(self):
		_index = self.tvFileMapper.selectedIndexes()[0]
		_userData = _index.model().itemFromIndex(_index).data()
		_path  = _userData["path"]
		self.teInformation.setText("<b>Файл:</b> <br/>"+_path)
		
		_index = self.tvFileMapper.selectedIndexes()[1]
		objectInformation = self.tvFileMapper.model().data(_index,QtCore.Qt.UserRole) 
		self.teInformation.append("<b>Объект:</b> <br/>"+ (objectInformation if objectInformation != None else ""))


	def _saveAndClose(self):
		self._model = []
		for row in range(self.modelFileMapper.rowCount()):
			index_file = self.modelFileMapper.index(row,0)
			index_object = self.modelFileMapper.index(row,1)
			fileName = self.modelFileMapper.itemFromIndex(index_file).data(QtCore.Qt.DisplayRole)
			filePath = self.modelFileMapper.itemFromIndex(index_file).data()
			objectName = self.modelFileMapper.itemFromIndex(index_object).data(QtCore.Qt.DisplayRole)
			model_row = {'path':filePath["path"],'file': fileName, 'object':objectName}
			self._model.append(model_row)
		
		self.close()

	def setFiles(self, FilePaths):
		if FilePaths == None:
			raise Exception("Аргумент не должен быть пустым")
		self._model = []
		for i in FilePaths:
			path, name = os.path.split(i)
			self._model.append({'path':i,'file':name, 'object':'' })
		self.buildViewTable()

	def setDbObjects(self, DbObjects):
		if DbObjects == None:
			raise Exception("Аргумент не должен быть пустым")
		self._dbObjects = []
		for i in DbObjects:
			self._dbObjects.append({ 'Name':i[0], 'Information':i[1] })
		self.buildViewTable()
	
	def setMessage(self,strMessage):
		if strMessage == None:
			raise Exception("Аргумент не должен быть пустым")
		self.teMessage.setText(strMessage)		

	def getModel(self):
		 return  self._model


