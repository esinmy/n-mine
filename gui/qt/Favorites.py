try:		import MMpy
except:		pass

from PyQt4 import QtCore, QtGui

from gui.qt.widgets.GridEditor import *

from utils.constants import *
from utils.fsettings import FormSet
from utils.qtutil import *

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
from gui.qt.dialogs.BaseDialog import BaseDialog
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class FavoritesManager(BaseDialog):
	def __init__(self, *args, **kw):
		super(FavoritesManager, self).__init__(*args, **kw)

		self.setWindowTitle("Редактировать избранное")

		self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.__setAccessibleNames()

		self.setFixedSize(QtCore.QSize(640, 480))

		self.setParams()

	def __createVariables(self):
		# пока что считывание каких-либо форм происходит через отдельно подключение
		# потом необходимо переделать обращение к разным формам
		self._settings = FormSet(ID_FAVORITE)
		self._settings.open(0)
		self._grids = []
		self._needsUpdate = False
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxTablist = QtGui.QGroupBox(self)
		self.groupBoxTablist.setTitle("Список директорий")
		self.layoutTablist = QtGui.QVBoxLayout()

		self.gridDirectories = GridEditor(self.groupBoxTablist, ["Имя", "Путь"])
		self.gridDirectories.getValue = self.getUserDirectories
		self.gridDirectories.setValue = self.setUserDirectories
		self._buildGridDirectories()
		
		self.groupBoxFormsets = QtGui.QGroupBox(self)
		self.groupBoxFormsets.setTitle("Настройки отображения")
		self.layoutFormsets = QtGui.QVBoxLayout()
		self.formsetStackedWidget = QtGui.QStackedWidget(self.groupBoxFormsets)
		self.formsetStackedWidget.getValue = self.getUserFormsets
		self.formsetStackedWidget.setValue = self.setUserFormsets
		self.directoryAppend()

		self.buttonBox = QtGui.QDialogButtonBox()
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).setMinimumWidth(100)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setMinimumWidth(100)
	def __createBindings(self):
		self.gridDirectories.dataGrid.itemDoubleClicked.connect(self.browseDirectiory)
		self.gridDirectories.dataGrid.itemSelectionChanged.connect(self.directorySelectionChanged)
		self.gridDirectories.dataGrid.keyPressEvent = self.gridDirectoriesKeyPressEvent

		self.gridDirectories.toolBar.pushButtonAppend.clicked.connect(self.directoryAppend)
		self.gridDirectories.toolBar.pushButtonInsert.clicked.connect(self.directoryInsert)
		self.gridDirectories.toolBar.pushButtonRefresh.setVisible(False)
		self.gridDirectories.toolBar.pushButtonMoveUp.clicked.connect(self.directoryMoveUp)
		self.gridDirectories.toolBar.pushButtonMoveDown.clicked.connect(self.directoryMoveDown)
		self.gridDirectories.toolBar.pushButtonDelete.clicked.disconnect()
		self.gridDirectories.toolBar.pushButtonDelete.clicked.connect(self.directoryDeleteSelected)

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self._save)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)
	def __setAccessibleNames(self):
		self.gridDirectories.setAccessibleName(ID_USRFOLDERS)
		self.formsetStackedWidget.setAccessibleName(ID_USRFORMSETS)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxTablist)
		self.groupBoxTablist.setLayout(self.layoutTablist)

		self.layoutTablist.addWidget(self.gridDirectories)

		self.layoutMain.addWidget(self.groupBoxFormsets)
		self.groupBoxFormsets.setLayout(self.layoutFormsets)

		self.layoutFormsets.addWidget(self.formsetStackedWidget)

		self.layoutMain.addWidget(self.buttonBox)

	#
	def _buildGridDirectories(self):
		self.gridDirectories.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.gridDirectories.dataGrid.verticalHeader().setVisible(False)
		self.gridDirectories.dataGrid.setColumnWidth(0, 150)
		self.gridDirectories.dataGrid.setReadOnlyColumn(1, True)
		self.gridDirectories.toolBar.labelHint.setText("Укажите имя, отображаемое в названии вкладки, и путь к директории")

	#
	def browseDirectiory(self, item):
		irow, icol = item.row(), item.column()
		if icol == 0:
			return

		initialdir = item.text()
		if not initialdir:
			try:		initialdir = MMpy.Project.path()
			except:		initialdir = os.getcwd()

		path = QtGui.QFileDialog.getExistingDirectory(self, "Выберите директорию", initialdir).replace("/", "\\")
		if path:
			item.setText(path)
	def selectFormSetId(self, item):
		irow, icol = item.row(), item.column()
		if icol != 1:
			return
		#
		commands = {0: 506, 1: 486, 2: 437, 3: 488}
		try:
			tree = TreeMmFormsetBrowserDialog(self)
			tree.readFormsets(commands[irow])
			tree.exec()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать наборы форм. %s" % str(e))
			return
		item.setText(tree.result)

	#
	def createFormsetGrid(self):
		grid = GridEditorFormsets(self.formsetStackedWidget)
		grid.toolBar.labelHint.setText("Выберите формы для отображения данных")
		return grid
	def setFormsets(self, srcwgt, dstwgt):
		for irow in range(srcwgt.dataGrid.rowCount()):
			item = DataGridItem(srcwgt.dataGrid.item(irow, 1).text())
			dstwgt.dataGrid.setItem(irow, 1, item)

	#
	def directoryAppend(self):
		grid = self.createFormsetGrid()

		self._grids.append(grid)
		self.formsetStackedWidget.addWidget(grid)
	def directoryInsert(self):
		dirgrid = self.gridDirectories.dataGrid
		selectedIndexes = dirgrid.selectedIndexes()
		if not selectedIndexes:
			return
		#
		selectedRows = dirgrid.getRowsFromIndexes(dirgrid.selectedIndexes(), reverse = True)
		index = selectedRows[0]-1
		#
		grid = self.createFormsetGrid()
		self._grids.insert(index, grid)
		self.formsetStackedWidget.insertWidget(index, grid)
	def directoryMoveUp(self):
		# переставляем виджеты в stacked widget, после того как строки были поменяны местами
		dirgrid = self.gridDirectories.dataGrid
		selectedItems = dirgrid.selectedItems()
		if not selectedItems:
			return

		#
		selectedRows = sorted([item.row() for item in selectedItems])
		for selectedRow in selectedRows:
			# вставляем новый грид
			grid = self.createFormsetGrid()
			self.formsetStackedWidget.insertWidget(selectedRow, grid)
			self._grids.insert(selectedRow, grid)
			# вставляем значения из перемещаемого грида в новый
			self.setFormsets(self._grids[selectedRow+2], self._grids[selectedRow])
			# удаляем перемещаемый грид
			self.formsetStackedWidget.removeWidget(self._grids[selectedRow+2])
			self._grids.remove(self._grids[selectedRow+2])
		# фокусируемся на первом выделенном гриде
		self.formsetStackedWidget.setCurrentIndex(selectedRows[0])
	def directoryMoveDown(self):
		# переставляем виджеты в stacked widget, после того как строки были поменяны местами
		dirgrid = self.gridDirectories.dataGrid
		selectedIndexes = dirgrid.selectedIndexes()
		if not selectedIndexes:
			return

		#
		selectedRows = dirgrid.getRowsFromIndexes(selectedIndexes, reverse = True)
		for selectedRow in selectedRows:
			# вставляем новый грид
			grid = self.createFormsetGrid()
			self.formsetStackedWidget.insertWidget(selectedRow+1, grid)
			self._grids.insert(selectedRow+1, grid)
			# вставляем значения из перемещаемого грида в новый
			self.setFormsets(self._grids[selectedRow-1], self._grids[selectedRow+1])
			# удаляем перемещаемый грид
			self.formsetStackedWidget.removeWidget(self._grids[selectedRow-1])
			self._grids.remove(self._grids[selectedRow-1])
		# фокусируемся на первом выделенном гриде
		self.formsetStackedWidget.setCurrentIndex(selectedRows[0])
	def directoryDelete(self, index):
		if index == 0:		self._grids[0].dataGrid.clear()
		else:
			self.formsetStackedWidget.removeWidget(self._grids[index])
			self._grids.remove(self._grids[index])
	def directoryDeleteSelected(self):
		dirgrid = self.gridDirectories.dataGrid
		selectedIndexes = dirgrid.selectedIndexes()
		if not selectedIndexes:
			return
		#
		selectedRows = dirgrid.getRowsFromIndexes(dirgrid.selectedIndexes(), reverse = True)
		for selectedRow in selectedRows:
			self.directoryDelete(selectedRow)
			dirgrid.removeRow(selectedRow)
	def deleteAllFormsets(self):
		n = len(self._grids)
		for i in range(n, 0, -1):
			self.formsetStackedWidget.removeWidget(self._grids[i])
			self._grids.remove(self._grids[i])
	#
	def directorySelectionChanged(self):
		selectedItems = self.gridDirectories.dataGrid.selectedItems()
		if not selectedItems:
			return
		currow = selectedItems[0].row()
		self.formsetStackedWidget.setCurrentIndex(currow)
	def keyPressEvent(self, event):
		key = event.key()
		# 
		if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
			event.ignore()
		else:
			super(FavoritesManager, self).keyPressEvent(event)
	def gridDirectoriesKeyPressEvent(self, event):
		dirgrid = self.gridDirectories.dataGrid

		key = event.key()
		# 
		if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
			selectedItems = dirgrid.selectedItems()
			if selectedItems:
				irow, icol = selectedItems[0].row(), selectedItems[0].column()
				if irow == dirgrid.rowCount()-1:
					self.directoryAppend()
	
		# Control+Delete
		if event.modifiers() & QtCore.Qt.ControlModifier and key == QtCore.Qt.Key_Delete and dirgrid.deleteByKeyboard():
			selectedItems = dirgrid.selectedItems()
			if selectedItems:
				selectedRows = sorted(list(set([item.row() for item in selectedItems])), reverse = True)
				for selectedRow in selectedRows:
					self.directoryDelete(selectedRow)
		#
		DataGrid.keyPressEvent(dirgrid, event)
	
	#
	def setParams(self):
		self.setUserDirectories(self._settings.getValue(ID_USRFOLDERS))
		self.setUserFormsets(self._settings.getValue(ID_USRFORMSETS))
	def getParams(self):
		self._settings.setValue(ID_USRFOLDERS, self.getUserDirectories())
		self._settings.setValue(ID_USRFORMSETS, self.getUserFormsets())

	def getUserFormsets(self):
		values = [grid.dataGrid.to_list() for grid in self._grids]
		return values
	def setUserFormsets(self, values):
		if not values:
			return

		if tuple(mmtype for mmtype, formsetid in values[0]) == MMTYPE_LIST:
			self._grids[0].dataGrid.from_list(values[0])
		for i, vals in enumerate(values[1:]):
			grid = self.createFormsetGrid()
			if tuple(mmtype for mmtype, formsetid in vals) == MMTYPE_LIST:
				grid.dataGrid.from_list(vals)
			self._grids.append(grid)
			self.formsetStackedWidget.addWidget(grid)
	def getUserDirectories(self):
		return self.gridDirectories.dataGrid.to_list()
	def setUserDirectories(self, values):
		if not values:
			return

		self.gridDirectories.dataGrid.from_list(values)

	def _save(self):
		self._needsUpdate = True

		self.getParams()
		self._settings.save("", 0)
		self._settings.close()

		self.close()

	@property
	def needsUpdate(self):
		return self._needsUpdate
