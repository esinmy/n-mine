from PyQt4 import QtGui, QtCore
import os, shutil

from mm.formsets import *
from mm.mmutils import MicromineFile

from utils.constants import *
from utils.blockmodel import BlockModel
from utils.nnutil import date_to_fieldname
from utils.osutil import get_temporary_prefix

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.AutoVizexPlotDialog import AutoVizexPlotDialog
from gui.qt.ProcessWindow import ProcessWindow
from gui.qt.modes.DisplayData import DisplayData

import geometry as geom

from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def get_pel_extents(pelpath):
	with open(pelpath, "r") as fpel:
		fpel.readline()		# пропускаем заголовок
		xmin = float(fpel.readline().split(" : ")[1])
		ymin = float(fpel.readline().split(" : ")[1])
		xmax = float(fpel.readline().split(" : ")[1])
		ymax = float(fpel.readline().split(" : ")[1])
	return xmin, ymin, xmax, ymax

class AutoVizexPlot(ProcessWindow):
	def __init__(self, *args, **kw):
		super(AutoVizexPlot, self).__init__(*args, **kw)

	def create_pel(self, pelpath):
		mcrpath = os.path.join(self._tmpfolder, "%sSCRIPT_MACRO.MCR" % get_temporary_prefix())
		rptpath = os.path.join(self._tmpfolder, "%sSCRIPT_MACRO_REPORT.MOF" % get_temporary_prefix())
		# создание пустого файла макроса
		
		create_macro(mcrpath)
		# создание формы файла чертежа
		plot_id = create_plot_form()
		# запись в макрос
		f = MicromineFile()
		if not f.open(mcrpath):
			raise Exception("Невозможно открыть файл '%s'" % mcrpath)
		f.add_record()
		f.set_field_value(0, 1, "VXPLOT")
		f.set_field_value(1, 1, str(plot_id))
		f.set_field_value(2, 1, pelpath)
		f.close()
		# запуск макроса
		MMpy.Macro.run(mcrpath, rptpath)
		# удаление файла макроса и отчета
		try:		os.remove(mcrpath)
		except:		pass
		try:		os.remove(rptpath)
		except: 	pass

	def check(self):
		if not super(AutoVizexPlot, self).check():
			return

		secpex = self.main_app.mainWidget.sectionPlotExplorer
		longex = secpex.longSectionExplorer.explorer
		crossex = secpex.crossSectionExplorer.explorer

		longitems = list(filter(lambda it: it.isSection(), longex.iterChildren()))
		crossitems = list(filter(lambda it: it.isSection(), crossex.iterChildren()))

		if not longitems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите продольные разрезы")
			return
		if not crossitems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите поперечные разрезы")
			return

		if not any(filter(lambda it: it.basis(), longitems)):
			qtmsgbox.show_error(self.main_app, "Ошибка", "Укажите опорный разрез")
			return

		return True

	def prepare(self):
		if not super(AutoVizexPlot, self).prepare():
			return

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))

		self.after_run()

	def after_run(self):
		super(AutoVizexPlot, self).after_run()
		# Galkin.Begin
		#self.main_app.mainWidget.sectionPlotExplorer.clear()	 #
		# Galkin.End

	def run(self):
		if not super(AutoVizexPlot, self).run():
			return

		dialog = AutoVizexPlotDialog(self)
		dialog.exec()
		if dialog.is_cancelled:
			self._is_cancelled = True
			return

		displayData = DisplayData(self.main_app)
		if any(self.selectedItems().values()):
			displayData.exec()

		### =================================================================================== ###
		### ============================== ОБЪЯВЛЕНИЕ ПЕРЕМЕННЫХ ============================== ###
		### =================================================================================== ###

		pexExtension, ptxExtension, pelExtension = ".PEX", ".PTX", ".PEL"

		reverseCrossSections = dialog.checkBoxReverse.isChecked()
		needsLongSections = dialog.groupBoxPlotHeaderParams.isChecked()
		needsCrossSections = dialog.groupBoxPlotBodyParams.isChecked()

		longPrefix = dialog.lineEditLongPrefix.text()
		crossPrefix = dialog.lineEditCrossPrefix.text()
		sheetName = "Лист %s" + pexExtension
		bodyName = "body_%s" + ptxExtension
		#
		if needsCrossSections:
			nfrags = int(dialog.comboBoxPlotBodyFragmentsCount.currentText())
		curSheetNum = 1
		curPelNum = 1
		#
		offsetDistance = 5
		zlevel = float(dialog.lineEditLevelZ.text())
		zmin, zmax = zlevel - 10, zlevel + 10

		#
		xCenterMask = "<SeqId>6</SeqId><Value>"
		yCenterMask = "<SeqId>7</SeqId><Value>"
		longmask = "111111"
		planmask = "222222"
		crossmask = "555555"

		#
		outputDirectory = dialog.lineEditOutputDirectory.text()
		outputName = dialog.lineEditOutputName.text()
		outputPath = os.path.join(outputDirectory, outputName)
		peldirPath = os.path.join(outputPath, "Фрагменты")
		relpath = os.path.join(peldirPath[len(MMpy.Project.path()):])

		# выбранные разрезы

		secpex = self.main_app.mainWidget.sectionPlotExplorer
		longex = secpex.longSectionExplorer.explorer
		crossex = secpex.crossSectionExplorer.explorer

		longPacks = list(filter(lambda it: it.isSectionPack(), longex.iterChildren()))
		longSectionItems = list(filter(lambda it: it.isSection(), longex.iterChildren()))
		nLongSections = len(longSectionItems)

		crossPacks = list(filter(lambda it: it.isSectionPack(), crossex.iterChildren()))
		crossSectionItems = list(filter(lambda it: it.isSection(), crossex.iterChildren()))
		nCrossSections = len(crossSectionItems)

		nzeros = len(str(len(crossSectionItems) * len(longSectionItems)))

		### =================================================================================== ###
		### ================================ СОЗДАНИЕ ДИРЕКТОРИЙ ============================== ###
		### =================================================================================== ###

		try:						os.mkdir(outputPath)
		except Exception as e:		raise Exception("Невозможно создать директорию файла печати. %s" % str(e))
		try:						os.mkdir(peldirPath)
		except Exception as e:		raise Exception("Невозможно создать директорию 'Фрагменты'. %s" % str(e))

		### =================================================================================== ###
		### ========================= СЧИТЫВАНИЕ ПРОДОЛЬНЫХ РАЗРЕЗОВ ========================== ###
		### =================================================================================== ###

		longSections = []
		for longPack in longPacks:
			filepath = longPack.filePath()

			f = MicromineFile()
			if not f.open(filepath):
				raise Exception("Невозможно открыть файл '%s'" % filepath)
			#
			secdata = f.read()
			secnames = [row[0] for row in secdata]
			#
			for i in range(longPack.childCount()):
				sectionItem = longPack.child(i)
				#
				secname = sectionItem.text(0)
				is_basis = sectionItem.basis()
				secindex = secnames.index(secname)
				#
				section = geom.Section(secdata[secindex])
				section.is_basis = is_basis
				#
				longSections.append(section)
			#
			f.close()

		### =================================================================================== ###
		### ========================= СЧИТЫВАНИЕ ПОПЕРЕЧНЫХ РАЗРЕЗОВ ========================== ###
		### =================================================================================== ###

		crossSections = []
		for crossPack in crossPacks:
			filepath = crossPack.filePath()

			f = MicromineFile()
			if not f.open(filepath):
				raise Exception("Невозможно открыть файл '%s'" % filepath)
			#
			secdata = f.read()
			secnames = [row[0] for row in secdata]
			#
			for i in range(crossPack.childCount()):
				sectionItem = crossPack.child(i)
				#
				secname = sectionItem.text(0)
				is_basis = sectionItem.basis()
				secindex = secnames.index(secname)
				#
				section = geom.Section(secdata[secindex])
				#
				# исключаем разрезы, которые параллельны одному из продольных разрезов
				if all(map(lambda x: not x.is_parallel(section), longSections)):
					crossSections.append(section)
			#
			f.close()

		if reverseCrossSections:
			crossSections = crossSections[::-1]

		### =================================================================================== ###
		### =============================== ПОИСК ТОЧЕК ПЕРЕСЕЧЕНИЙ =========================== ###
		### =================================================================================== ###

		intersectionsList = []
		intersectionsDict = {}
		longIntersections = []
		for longSection in longSections:
			firstPoint, lastPoint = None, None
			for i, crossSection in enumerate(crossSections):
				line = geom.Line(crossSection.center, vector = longSection.vector)
				intersection = longSection.center_plane.intersection(line)
				intersection.z = zlevel
				#
				intersectionsDict[longSection, crossSection] = intersection
				intersectionsList.append([longSection, crossSection])

				if i == 0:					firstPoint = intersection
				if i == nCrossSections-1:	lastPoint = intersection
			#
			longIntersections.append(0.5*(firstPoint + lastPoint))

		### =================================================================================== ###
		### ======================= СОЗДАНИЕ ПЛАНОВ И ПРОДОЛЬНЫХ РАЗРЕЗОВ ===================== ###
		### =================================================================================== ###

		if needsLongSections:
			#
			tmpltHeaderRoot = dialog.getPlotHeaderRoot()
			if not os.path.exists(tmpltHeaderRoot):
				raise Exception("Путь '%s' не найден" % tmpltHeaderRoot)
			tmpltHeaderPaths = [os.path.join(tmpltHeaderRoot, filename) for filename in filter(lambda x: x.upper().endswith(ptxExtension), os.listdir(tmpltHeaderRoot))]
			#
			for tmpltHeaderPath in tmpltHeaderPaths:
				nplans, nlongs = 0, 0
				#
				pexpath = os.path.join(outputPath, sheetName % str(curSheetNum).zfill(nzeros))
				shutil.copy(tmpltHeaderPath, pexpath)
				#
				with open(pexpath, "r", encoding = "utf8") as fpex:
					xmlpex = fpex.read()
				xmlpex = xmlpex.replace("\n", "").replace("\t", "")
				with open(pexpath, "w", encoding = "utf8") as fpex:
					fpex.write(xmlpex)
				#
				fpex = open(pexpath, encoding = "utf8")
				xmlpex = fpex.read()
				fpex.close()
				#
				# поиск планов
				tmpxml = xmlpex[:]
				mask = xCenterMask + planmask
				while mask in tmpxml:
					nplans += 1
					tmpxml = tmpxml[tmpxml.index(mask) + len(mask):]
				# поиск продольников
				tmpxml = xmlpex[:]
				mask = xCenterMask + longmask
				while mask in tmpxml:
					nlongs += 1
					tmpxml = tmpxml[tmpxml.index(mask) + len(mask):]

				# cоздание планов
				for i in range(nplans):
					number = str(i+1).zfill(2)
					#
					pelMask = "plan_%s"
					pelFileName = pelMask % str(curPelNum).zfill(2)
					pelDataMask =  os.path.join(tmpltHeaderRoot, "fragments", pelMask % number)
					#
					msg = "Убедитесь, что в Визекс загружена информация, необходимая для плана"
					qtmsgbox.show_information(self.main_app, "Создание печати", msg)
					#
					# if i >= len(longSections):
					# 	continue
					intersection = longIntersections[0]
					longSection = longSections[0]
					#
					ptA = longSection.offset(intersection, offsetDistance, -1)
					ptB = longSection.offset(intersection, offsetDistance, 1)
					#
					display_limits(ptA, ptB, "", zmin, zmax)
					# Вид в плане
					MMpy.Command("#33523").run()
					# создание файла печати
					pelpath = os.path.join(peldirPath, pelFileName + pelExtension)
					self.create_pel(pelpath)
					# вычисление центра фрагмента
					pelxmin, pelymin, pelxmax, pelymax = get_pel_extents(pelpath)
					pelxcenter = 0.5 * (pelxmin + pelxmax)
					pelycenter = 0.5 * (pelymin + pelymax)
					# заменяем в файле печати необходимую информацию
					xmlpex = xmlpex.replace(xCenterMask + planmask + "1%s" % number, xCenterMask + "%.3f" % pelxcenter)
					xmlpex = xmlpex.replace(yCenterMask + planmask + "2%s" % number, yCenterMask + "%.3f" % pelycenter)
					xmlpex = xmlpex.replace(">%s<" % (pelDataMask + pelExtension), ">%s<" % pelpath)
					xmlpex = xmlpex.replace(">%s<" % pelDataMask, ">%s<" % pelpath)

					curPelNum += 1

				curPelNum = 1

				# создание продольников
				for i in range(nlongs):
					number = str(i+1).zfill(2)
					#
					pelMask = "longsect_%s"
					pelFileName = pelMask % str(curPelNum).zfill(2)
					pelDataMask = os.path.join(tmpltHeaderRoot, "fragments", pelMask % number)
					pelHeaderMask = "Header_Longsect_%s" % number
					#
					msg = "Убедитесь, что в Визекс загружена информация, необходимая для продольного разреза"
					qtmsgbox.show_information(self.main_app, "Создание печати", msg)
					#
					if i >= len(longSections):
						continue
					intersection = longIntersections[i]
					longSection = longSections[i]
					#
					ptA = longSection.offset(intersection, offsetDistance, -1)
					ptB = longSection.offset(intersection, offsetDistance, 1)
					#
					display_limits(ptA, ptB, longSection.azimuth, zmin, zmax, longSection.towards, longSection.away)
					# создание файла печати
					pelpath = os.path.join(peldirPath, pelFileName + pelExtension)
					self.create_pel(pelpath)
					# вычисление центра фрагмента
					pelxmin, pelymin, pelxmax, pelymax = get_pel_extents(pelpath)
					pelxcenter = 0.5 * (pelxmin + pelxmax)
					# заменяем в файле печати координаты центра
					xmlpex = xmlpex.replace(xCenterMask + longmask + "1%s" % number, xCenterMask + "%.3f" % pelxcenter)
					xmlpex = xmlpex.replace(yCenterMask + longmask + "2%s" % number, yCenterMask + "%.3f" % zlevel)
					xmlpex = xmlpex.replace(">%s<" % (pelDataMask + pelExtension), ">%s<" % pelpath)
					xmlpex = xmlpex.replace(">%s<" % pelDataMask, ">%s<" % pelpath)
					xmlpex = xmlpex.replace(pelHeaderMask, longPrefix + longSection.name)

					curPelNum += 1

				# запись в файл
				fpex = open(pexpath, "w", encoding = "utf8")
				fpex.write(xmlpex)
				fpex.close()

				#
				curSheetNum += 1

		### =================================================================================== ###
		### ============================ СОЗДАНИЕ ПОПЕРЕЧНЫХ РАЗРЕЗОВ ========================= ###
		### =================================================================================== ###

		curIntersectionNum = 0

		if needsCrossSections:
			#
			tmpltBodyRoot = dialog.getPlotBodyRoot()
			#
			msg = "Убедитесь, что в Визекс загружена информация, необходимая для поперечных разрезов"
			qtmsgbox.show_information(self.main_app, "Создание печати", msg)

			# число заполненных листов
			basisLongSections = [section for section in longSections if section.is_basis]
			fullListNumber = (len(crossSections) * len(basisLongSections)) // nfrags
			partListNumber = (len(crossSections) * len(basisLongSections)) % nfrags

			#
			curPelNum = 1
			tmpltBodyPath = os.path.join(tmpltBodyRoot, bodyName % str(nfrags).zfill(2))
			for k in range(fullListNumber):
				ncross = 0
				#
				pexpath = os.path.join(outputPath, sheetName % str(curSheetNum).zfill(nzeros))

				shutil.copy(tmpltBodyPath, pexpath)				
				#
				with open(pexpath, "r", encoding = "utf8") as fpex:
					xmlpex = fpex.read()
				xmlpex = xmlpex.replace("\n", "").replace("\t", "")
				with open(pexpath, "w", encoding = "utf8") as fpex:
					fpex.write(xmlpex)
				# считываем файл
				fpex = open(pexpath, encoding = "utf8")
				xmlpex = fpex.read()
				fpex.close()
				#
				tmpxml = xmlpex[:]
				mask = xCenterMask + crossmask
				while mask in tmpxml:
					ncross += 1
					tmpxml = tmpxml[tmpxml.index(mask) + len(mask):]

				# создание поперечников
				for i in range(ncross):
					#
					longSection, crossSection = intersectionsList[curIntersectionNum]
					# пропускаем неопорные продольные разрезы
					if not longSection.is_basis:
						continue
					#
					intersection =  intersectionsDict[longSection, crossSection]
					number = str(i+1).zfill(2)
					#
					pelMask = "bodypart_%s"
					pelFileName = pelMask % str(curPelNum).zfill(2)
					pelDataMask = os.path.join(tmpltBodyRoot, "fragments", pelMask % number)
					pelHeaderMask = "Header_Part_%s" % number
					#
					ptA = crossSection.offset(intersection, offsetDistance, -1)
					ptB = crossSection.offset(intersection, offsetDistance, 1)
					#
					display_limits(ptA, ptB, crossSection.azimuth, zmin, zmax, crossSection.towards, crossSection.away)
					# создание файла печати
					pelpath = os.path.join(peldirPath, pelFileName + pelExtension)
					self.create_pel(pelpath)
					# вычисление центра фрагмента
					pelxmin, pelymin, pelxmax, pelymax = get_pel_extents(pelpath)
					pelxcenter = 0.5 * (pelxmin + pelxmax)
					# заменяем в файле печати координаты центра
					xmlpex = xmlpex.replace(xCenterMask + crossmask + "1%s" % number, xCenterMask + "%.3f" % pelxcenter)
					xmlpex = xmlpex.replace(yCenterMask + crossmask + "2%s" % number, yCenterMask + "%.3f" % zlevel)
					xmlpex = xmlpex.replace(">%s<" % (pelDataMask + pelExtension), ">%s<" % pelpath)
					xmlpex = xmlpex.replace(">%s<" % pelDataMask, ">%s<" % pelpath)
					xmlpex = xmlpex.replace(pelHeaderMask, crossPrefix + crossSection.name) #+ " (%s)" % (longPrefix + longSection.name))
					#
					curPelNum += 1
					curIntersectionNum += 1
			
				# запись в файл
				fpex = open(pexpath, "w", encoding = "utf8")
				fpex.write(xmlpex)
				fpex.close()

				curSheetNum += 1

			if partListNumber != 0:
				tmpltBodyPath = os.path.join(tmpltBodyRoot, bodyName % str(partListNumber).zfill(2))
				ncross = 0
				#
				pexpath = os.path.join(outputPath, sheetName % str(curSheetNum).zfill(nzeros))
				shutil.copy(tmpltBodyPath, pexpath)
				#
				with open(pexpath, "r", encoding = "utf8") as fpex:
					xmlpex = fpex.read()
				xmlpex = xmlpex.replace("\n", "").replace("\t", "")
				with open(pexpath, "w", encoding = "utf8") as fpex:
					fpex.write(xmlpex)
				# считываем файл
				fpex = open(pexpath, encoding = "utf8")
				xmlpex = fpex.read()
				fpex.close()
				#
				tmpxml = xmlpex[:]
				mask = xCenterMask + crossmask
				while mask in tmpxml:
					ncross += 1
					tmpxml = tmpxml[tmpxml.index(mask) + len(mask):]

				# создание поперечников
				for i in range(ncross):
					#
					longSection, crossSection = intersectionsList[curIntersectionNum]
					#
					if not longSection.is_basis:
						continue
					#
					intersection =  intersectionsDict[longSection, crossSection]
					number = str(i+1).zfill(2)
					#
					pelMask = "bodypart_%s"
					pelFileName = pelMask % str(curPelNum).zfill(2)
					pelDataMask = os.path.join(tmpltBodyRoot, "fragments",pelMask % number)
					pelHeaderMask = "Header_Part_%s" % number
					#
					ptA = crossSection.offset(intersection, offsetDistance, -1)
					ptB = crossSection.offset(intersection, offsetDistance, 1)
					#
					display_limits(ptA, ptB, crossSection.azimuth, zmin, zmax, crossSection.towards, crossSection.away)
					# создание файла печати
					pelpath = os.path.join(peldirPath, pelFileName + pelExtension)
					self.create_pel(pelpath)
					# вычисление центра фрагмента
					pelxmin, pelymin, pelxmax, pelymax = get_pel_extents(pelpath)
					pelxcenter = 0.5 * (pelxmin + pelxmax)
					# заменяем в файле печати координаты центра
					xmlpex = xmlpex.replace(xCenterMask + crossmask + "1%s" % number, xCenterMask + "%.3f" % pelxcenter)
					xmlpex = xmlpex.replace(yCenterMask + crossmask + "2%s" % number, yCenterMask + "%.3f" % zlevel)
					xmlpex = xmlpex.replace(">%s<" % (pelDataMask + pelExtension), ">%s<" % pelpath)
					xmlpex = xmlpex.replace(">%s<" % pelDataMask, ">%s<" % pelpath)
					xmlpex = xmlpex.replace(pelHeaderMask, crossPrefix + crossSection.name) # + " (%s)" % (longPrefix + longSection.name))
					#
					curPelNum += 1
					curIntersectionNum += 1
			
				# запись в файл
				fpex = open(pexpath, "w", encoding = "utf8")
				fpex.write(xmlpex)
				fpex.close()

		qtmsgbox.show_information(self.main_app, "Выполнено", "Файл печати успешно создан")
			