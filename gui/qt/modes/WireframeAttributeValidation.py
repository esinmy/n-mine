from PyQt4 import QtGui, QtCore
import os
from datetime import datetime

from gui.qt.ProcessWindow import ProcessWindow
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog

from mm.mmutils import Tridb

from utils.nnutil import is_valid_short_date
from utils.osutil import is_blank, is_number, in_range
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

NAN = "НЕ ЧИСЛО"
OUT_OF_RANGE = "ВНЕ ДИАПАЗОНА"
OUT_OF_LIST = "НЕВЕРНЫЙ КОД"
EMPTY = "ПУСТОЕ"
MISS_ATTR = "ОТСУТСТВУЕТ АТРИБУТ"
OUT_OF_GROUP = "НЕВЕРНЫЙ КОД ДЛЯ %s"
INVALID_FORMAT = "НЕВЕРНЫЙ ФОРМАТ"

class WireframeAttributeValidation(ProcessWindow):
	def __init__(self, *args, **kw):
		super(WireframeAttributeValidation, self).__init__(*args, **kw)
		self._isSilent = False
	def validate_file(self, tripath):
		pass
	def validate_folder(self, path):
		progress = self.main_app.mainWidget.progressBar
		value = progress.value()
		progress.setRange(0,0)
		#
		tridbpaths = []
		for filename in os.listdir(path):
			filepath = os.path.join(path, filename)
			if os.path.isfile(filepath) and filepath.lower().endswith(".tridb"):
				tridbpaths.append(filepath)
		#
		n = len(tridbpaths)
		for i, filepath in enumerate(tridbpaths):
			progress.setText("Проверка атрибутов каркасов (%d/%d)" % (i+1, n))
			self.validate_file(filepath)
			QtCore.QCoreApplication.processEvents()
			
		progress.setRange(0, 100)
		progress.setValue(value)

	@property
	def report(self):			return self._report
	@property
	def result(self):			return self._result
	@property
	def isSilent(self):			return self._isSilent
	@isSilent.setter
	def isSilent(self, v):		self._isSilent = bool(v)

	def exec(self, filelist = []):
		self._report = []
		self._result = None
		#
		progress = self.main_app.mainWidget.progressBar
		value = progress.value()
		progress.setRange(0,0)

		if filelist:
			for filepath in filelist:
				basename = os.path.basename(filepath)
				basename = basename[:basename.rfind(".")] if "." in basename else basename
				progress.setText("Проверка %s" % basename)
				self.validate_file(filepath)
				QtCore.QCoreApplication.processEvents()
		else:
			selex = self.main_app.mainWidget.selectionExplorer
			tridbItems = list(it for it in selex.explorer.iterChildren() if it.isTridb())
			for item in tridbItems:
				filepath = item.filePath()
				progress.setText("Проверка %s" % item.text(0))
				self.validate_file(filepath)
				QtCore.QCoreApplication.processEvents()

		progress.setRange(0, 100)
		progress.setText("")
		progress.setValue(value)

		if not self.report:
			self._result = True
			if not self._isSilent:
				qtmsgbox.show_information(self.main_app, "Выполнено","Ошибок не обнаружено")
			return self._result

		self.showReport()

	def showReport(self):
		self._dialog = BaseDialog(self.main_app)
		self._dialog.setWindowFlags(self._dialog.windowFlags() | QtCore.Qt.WindowMaximizeButtonHint)
		self._dialog.setWindowTitle("Проверка атрибутов каркасов")
		
		layout = QtGui.QVBoxLayout()
		
		self.reportTable = QtGui.QTableWidget(self._dialog)
		# self.reportTable.verticalHeader().setVisible(False)
		self.reportTable.verticalHeader().setHighlightSections(False)
		self.reportTable.horizontalHeader().setHighlightSections(False)
		self.reportTable.horizontalHeader().setStretchLastSection(True)
		self.reportTable.setSortingEnabled(True)
		self.reportTable.setColumnCount(5)
		self.reportTable.itemChanged.connect(self._attributeChanged)

		#
		header = ["ТИП", "ИМЯ", "АТРИБУТ", "ЗНАЧЕНИЕ", "ОШИБКА"]
		widths = [200, 100, 100, 100, 200]
		for i in range(self.reportTable.columnCount()):
			item = QtGui.QTableWidgetItem(header[i])
			self.reportTable.setHorizontalHeaderItem(i, item)
			self.reportTable.setColumnWidth(i, widths[i])
		#
		self._setReportTable()

		layoutButtons = QtGui.QHBoxLayout()
		layoutButtons.setAlignment(QtCore.Qt.AlignRight)
		buttonOk = QtGui.QPushButton(self._dialog)
		buttonOk.clicked.connect(self._continue)
		buttonOk.setMaximumSize(QtCore.QSize(100, 25))
		buttonOk.setMinimumSize(QtCore.QSize(100, 25))
		buttonOk.setText("OK")
		buttonCancel = QtGui.QPushButton(self._dialog)
		buttonCancel.clicked.connect(self._dialog.close)
		buttonCancel.setMaximumSize(QtCore.QSize(100, 25))
		buttonCancel.setMinimumSize(QtCore.QSize(100, 25))
		buttonCancel.setText("Отмена")

		self._dialog.setLayout(layout)
		layout.addWidget(self.reportTable)
		layout.addLayout(layoutButtons)
		layoutButtons.addWidget(buttonCancel)
		layoutButtons.addWidget(buttonOk)

		self._dialog.resize(800, 480)
		self._dialog.exec()

	def _attributeChanged(self, item):
		irow, icol = item.row(), item.column()
		if icol != 3:
			return

		tripath, triname, attrName = self._report[irow][:-2]
		attrValue = item.text()

		tridb = Tridb(tripath)
		wireframe = tridb[triname]

		if attrName in tridb.default_attributes:	wireframe.set_default_attribute(attrName, attrValue)
		if attrName in tridb.user_attributes:		wireframe.set_user_attribute(attrName, attrValue)

		tridb.commit()
		tridb.close()
	def _continue(self):
		self._result = True
		self._dialog.close()
	def _setReportTable(self):
		self.reportTable.blockSignals(True)
		self.reportTable.setRowCount(0)
		for i, row in enumerate(self.report):
			self.reportTable.insertRow(i)
			for j in range(self.reportTable.columnCount()):
				if j == 0:
					text = os.path.basename(row[j])
					text = text[:text.rfind(".")] if "." in text else text
				else:
					text = row[j]
				item = QtGui.QTableWidgetItem(text)
				if j != 3 or not row[1]:
					item.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
				self.reportTable.setItem(i, j, item)
		self.reportTable.blockSignals(False)

class GeoWireframeAttributeValidation(WireframeAttributeValidation):
	def validate_file(self, tripath):
		geo_user_attributes = list(GEO_USER_ATTRIBUTES)
		geo_user_attributes.insert(1, self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper())
		LAYER, PANEL, ORE, VOL, DIR, DIP, ROT  = geo_user_attributes
		CODE = DEFAULT_ATTR_CODE

		# типы руды
		CODE_LIST, CODE_DESCRIPTION, CODE_TYPE = zip(*self.getGeoCodes())
		INDEX_LIST = []
		for code in self.getGeoIndexes():
			list_index_dens_wstdenst = list(zip(*code["BM"]))[0]
			INDEX_LIST.append(list(zip(*list_index_dens_wstdenst))[0])
		ORECODE_DIC = dict(zip(CODE_LIST, INDEX_LIST))

		# подключение к файлу
		tridb = Tridb(tripath)
		# проверка на наличие пользовательских атрибутов 
		diff = set(geo_user_attributes).difference(set(tridb.user_attributes))
		if diff != set():
			self._report.append([tripath, "", "", "|".join(diff), MISS_ATTR])
			tridb.close()
			return

		for wf in tridb:
			ore = wf.user_attribute(ORE)
			# если ТИП РУДЫ принимает какое-то значение не из справочника
			if ore not in CODE_LIST:
				self._report.append([tripath, wf.name, ORE, ore, OUT_OF_LIST])
				continue
			#
			idx = CODE_LIST.index(ore)
			oretype = CODE_TYPE[idx]
			if not wf.default_attribute(CODE) in ORECODE_DIC[ore]:
				self._report.append([tripath, wf.name, CODE, wf.default_attribute(CODE), OUT_OF_GROUP % ore])
				continue

			# если нерудная порода
			if oretype == 1:
				# если пустое
				if is_blank(wf.user_attribute(VOL)):
					self._report.append([tripath, wf.name, VOL, wf.user_attribute(VOL), EMPTY])
				# если не пустое и не число
				elif not is_number(wf.user_attribute(VOL)):
					self._report.append([tripath, wf.name, VOL, wf.user_attribute(VOL), NAN])
				# если число и вне диапазона
				elif not in_range(float(wf.user_attribute(VOL)), 1.5, 5):
					self._report.append([tripath, wf.name, VOL, wf.user_attribute(VOL), OUT_OF_RANGE])
			# если рудная порода
			elif oretype == 0:
				# для каждого числового атрибута
				for attrName in [DIP, DIR, ROT, VOL]:
					attrValue = wf.user_attribute(attrName)
					# если пустое
					if is_blank(attrValue):
						self._report.append([tripath, wf.name, attrName, attrValue, EMPTY])
						continue
					# если не пустое и не число
					if not is_number(attrValue):
						self._report.append([tripath, wf.name, attrName, attrValue, NAN])
						continue
					# если ПОГРУЖЕНИЕ вне диапазона
					if attrName == DIP and not in_range(float(attrValue), -90, 90):
						self._report.append([tripath, wf.name, attrName, attrValue, OUT_OF_RANGE])
						continue
					# если АЗИМУТ вне диапазона
					if attrName == DIR and not in_range(float(attrValue), 0, 360):
						self._report.append([tripath, wf.name, attrName, attrValue, OUT_OF_RANGE])
						continue
					# если ВРАЩЕНИЕ вне диапазона
					if attrName == ROT and not in_range(float(attrValue), -90, 90):
						self._report.append([tripath, wf.name, attrName, attrValue, OUT_OF_RANGE])
						continue
					# если ОБЪЕМНЫЙ_ВЕС вне диапазона
					if attrName == VOL and not in_range(float(attrValue), 1.5, 5):
						self._report.append([tripath, wf.name, attrName, attrValue, OUT_OF_RANGE])
						continue
		tridb.close()

class SvyWireframeAttributeValidation(WireframeAttributeValidation):
	def validate_file(self, tripath):
		CODE, TITLE = DEFAULT_ATTR_CODE, DEFAULT_ATTR_TITLE
		CATEGORY, LAYER, BLOCK, DATE, MMYY, ACCURACY, LEGEND  = SVY_USER_ATTRIBUTES
		MINEBINDATTR = self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper()
		dfltAttrs = [CODE, TITLE]

		#
		CATEGORY_LIST = []		# маркшейдерские категории работ
		CODE_LIST = []			# коды
		CATEGORY_DIC = {}
		for cat in self.getSvyCategoriesAndCodes():
			category = cat[0][1]
			codes = [value[0] for value in cat[1]]
			CATEGORY_LIST.append(category)
			CODE_LIST.extend(codes)
			if not category in CATEGORY_DIC:
				CATEGORY_DIC[category] = []
			CATEGORY_DIC[category].extend(codes)

		# проверка на наличие пользовательских атрибутов 
		tridb = Tridb(tripath)
		svy_user_attributes = list(SVY_USER_ATTRIBUTES)
		svy_user_attributes.append(MINEBINDATTR)
		diff = set(svy_user_attributes).difference(set(tridb.user_attributes))
		if diff != set():
			self._report.append([tripath, "", "", "|".join(diff), MISS_ATTR])
			tridb.close()
			return

		for wf in tridb:
			# задаем Заголовок по умолчанию
			basename = os.path.basename(tripath)
			basename = basename[:basename.rfind(".")] if "." in basename else basename
			try:
				wf.set_default_attribute(TITLE, basename)
			except:
				title = wf.default_attribute(TITLE)
				if title != basename:
					self._report.append([tripath, wf.name, TITLE, title, OUT_OF_GROUP % basename])


			category = wf.user_attribute(CATEGORY)
			# если КАТЕГОРИЯ принимает какое-то значение не из справочника
			if category not in CATEGORY_LIST:
				self._report.append([tripath, wf.name, CATEGORY, category, OUT_OF_LIST])
				continue

			code = wf.default_attribute(CODE)
			# если Code пустое
			if is_blank(code):
				self._report.append([tripath, wf.name, CODE, code, EMPTY])
			# если Code принимает какое-то значение не из справочника
			elif code not in CODE_LIST:
				self._report.append([tripath, wf.name, CODE, code, OUT_OF_LIST])
			elif code not in CATEGORY_DIC[category]:
				self._report.append([tripath, wf.name, CODE, code, OUT_OF_GROUP % category])
			# в противном случае
			else:
				for attrName in svy_user_attributes:
					attrValue = wf.user_attribute(attrName)
					# если пустое
					if is_blank(attrValue):
						if attrName == MINEBINDATTR:
							if wf.user_attribute(CATEGORY) == CATEGORY_LIST[3]:
								self._report.append([tripath, wf.name, attrName, attrValue, EMPTY])
								continue
						#elif attrName == ACCURACY:
						#	wf.set_user_attribute(ACCURACY, 'Д') 
						#	attrValue = 'Д'
							
						#elif attrName == LEGEND:
						#	continue
						else:
							self._report.append([tripath, wf.name, attrName, attrValue, EMPTY])
							continue
					if attrName == DATE:
						try:		datetime.strptime(attrValue, "%Y-%m-%d")
						except:		self._report.append([tripath, wf.name, attrName, attrValue, INVALID_FORMAT])
					if attrName == MMYY and not is_valid_short_date(attrValue):
						self._report.append([tripath, wf.name, attrName, attrValue, INVALID_FORMAT])
					if attrName == ACCURACY:
						if attrValue not in ('Д','НД'):
							self._report.append([tripath, wf.name, attrName, attrValue, OUT_OF_RANGE])
					if attrName == LEGEND:
						trueValue = '1:' + wf.default_attribute(CODE) + '_' + '2:' + wf.user_attribute(ACCURACY)
						if attrValue != trueValue:
							self._report.append([tripath, wf.name, attrName, attrValue, OUT_OF_GROUP % trueValue])

						   
		tridb.close()

class DsnWireframeAttributeValidation(WireframeAttributeValidation):
	def validate_file(self, tripath):
		CATEGORY_LIST = []		# маркшейдерские категории работ
		CODE_LIST = []			# коды
		CATEGORY_DIC = {}
		for cat in self.getSvyCategoriesAndCodes():
			categoryCode = cat[0][1]
			CATEGORY_LIST.append(categoryCode)
			if not categoryCode in CATEGORY_DIC:
				CATEGORY_DIC[categoryCode] = []
			CATEGORY_DIC[categoryCode].extend([value[0] for value in cat[1]])

		# проверка на наличие пользовательских атрибутов 
		tridb = Tridb(tripath)
		diff = set(DSN_USER_ATTRIBUTES).difference(set(tridb.user_attributes))
		if diff != set():
			self._report.append([tripath, "", "", "|".join(diff), MISS_ATTR])
			tridb.close()
			return

		for wf in tridb:
			basename = os.path.basename(tripath)
			basename = basename[:basename.rfind(".")]

			# задаем Title по умолчанию
			wf.set_default_attribute(DEFAULT_ATTR_TITLE, basename)
				
			code = wf.default_attribute(DEFAULT_ATTR_CODE)
			# проверка пользовательских атрибутов
			for attrName in DSN_USER_ATTRIBUTES:
				attrValue = wf.user_attribute(attrName)
				if attrName != DSN_ATTR_SECTION and is_blank(attrValue):
					self._report.append([tripath, wf.name, attrName, attrValue, EMPTY])
				# проверка атрибутов КАТЕГОРИЯ и Code
				if attrName == DSN_ATTR_CATEGORY:
					if not attrValue in CATEGORY_LIST:
						self._report.append([tripath, wf.name, attrName, attrValue, OUT_OF_LIST])
					elif not code in CATEGORY_DIC[attrValue]:
						self._report.append([tripath, wf.name, DEFAULT_ATTR_CODE, code, OUT_OF_GROUP % attrValue])

				# проверяем, что СТАДИЯ число
				if attrName == DSN_ATTR_STAGE and not attrValue.isdigit():
					self._report.append([tripath, wf.name, attrName, attrValue, NAN])

				# проверяем формат даты
				if attrName == DSN_ATTR_DATE:
					try:		datetime.strptime(attrValue, "%Y-%m-%d")
					except:		self._report.append([tripath, wf.name, attrName, attrValue, INVALID_FORMAT])

		tridb.commit()
		tridb.close()
