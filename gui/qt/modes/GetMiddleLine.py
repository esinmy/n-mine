try:
	import MMpy
except:
	pass

from PyQt4 import QtGui, QtCore

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow

from mm.mmutils import MicromineFile, Tridb
from mm.formsets import defaultLoadStrings

from utils.constants import *

import math, os, sqlite3, numbers, enum, statistics, sys, shutil
from utils.logger import NLogger

import numpy as np
import shapely.geometry as sg
import shapely.affinity as sa

try:
	import matplotlib.pyplot as plt
except:
	pass

from scipy.spatial import Delaunay
from scipy.interpolate import interp1d
import copy
import platform

NO_LIBRARIES = False
try:
	from sklearn.neighbors import NearestNeighbors
	import networkx as nx
except Exception:
	NO_LIBRARIES = True

TRIANGLE_TOLERANCE = 0.2
ANGLE_DEF_DIRECTION = 30
LEN_TOLERANCE_DEF_DIRECTION = 4
EXTRAPOLATED_RATIO_INTERPOLATION_method = 1
EXTRAPOLATED_RATIO_SKLEARN_method = 1
NUM_EXTRAPOLATED = 400
INTERPOLATED_LENGTH = 0.15
SIMPLIED_COEF = 0.1

try:
	TEMP_TRIDB_POINT = MMpy.Project.path() + "точки.STR"
	TEMP_TRIDB_TRI = MMpy.Project.path() + "треугольники.STR"
except:
	TEMP_TRIDB_POINT = "D:\\TEMPORARY_SCRIPT_FOLDER\\точки.STR"
	TEMP_TRIDB_TRI = "D:\\TEMPORARY_SCRIPT_FOLDER\\треугольники.STR"

MIDDLE_LINE_ATTRIBUTES = ['Использование ШАС', 'Время на закладочные работы, ч', 'Наличие крепи', 'Тип крепи',
						  'Среднее количество шпуров, шт', 'Среднее количество скважин в веере, шт',
						  'Средняя длина взрывной скважины, м', 'Количество вееров в стадии взрыва, шт', 'ЛНС, м']

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

################################################# Костыль
try:
	_log_folder = MMpy.Project.path() + "LOG_MIDDLE_LINE"
except:
	_log_folder = "D:\\TEMPORARY_SCRIPT_FOLDER\\LOG_MIDDLE_LINE"

if not os.path.exists(_log_folder):
	try:
		os.mkdir(_log_folder)
	except:
		_log_folder = os.getcwd()
try:
	_user_name = os.getlogin()
except:
	_user_name = 'unknown'
try:
	_platform = platform.uname()
	_pc_info = _platform.node
except:
	_pc_info = 'unknown'
_log_filename = _log_folder + '\\' +_pc_info+'_'+_user_name+'.log'
################################################# Временный костыль конец

def write_strings_in_file(path, data):
	if not os.path.exists(path):
		strStructure = MMpy.FileStruct()
		strStructure.add_field("EAST", MMpy.FieldType.real, 0, 3)
		strStructure.add_field("NORTH", MMpy.FieldType.real, 0, 3)
		strStructure.add_field("RL", MMpy.FieldType.real, 0, 3)
		strStructure.add_field("Имя выработки", MMpy.FieldType.character, 20)
		strStructure.add_field("JOIN", MMpy.FieldType.real, 0, 3)
		strStructure.add_field("Имя tridb", MMpy.FieldType.character, 20)
		for attribute in MIDDLE_LINE_ATTRIBUTES:
			strStructure.add_field(attribute, MMpy.FieldType.character, 20)
		MMpy.File.create_from_template(path, "", strStructure)

	mmFile = MMpy.File()
	if not mmFile.open(path):
		raise Exception("Невозможно открыть файл %s" % path)

	records_count_init = mmFile.records_count
	for irow, row in enumerate(data):
		mmFile.add_record()
		for icol, value in enumerate(row):
			mmFile.set_field_value(icol, irow + records_count_init + 1, value)
	mmFile.close()


def get_normal_vector_and_point(triangle):
	point = list(triangle.boundary.coords)
	vector_1 = np.array(point[1]) - np.array(point[0])
	vector_2 = np.array(point[2]) - np.array(point[0])
	return np.cross(vector_1, vector_2), np.array(point[0])


# intersection function
def isect_line_plane_v3(p0, p1, p_co, p_no, epsilon=1e-6):
	"""
	p0, p1: define the line
	p_co, p_no: define the plane:
		p_co is a point on the plane (plane coordinate).
		p_no is a normal vector defining the plane direction;
			 (does not need to be normalized).

	return a Vector or None (when the intersection can't be found).
	"""

	u = sub_v3v3(p1, p0)
	dot = dot_v3v3(p_no, u)

	if abs(dot) > epsilon:
		# the factor of the point between p0 -> p1 (0 - 1)
		# if 'fac' is between (0 - 1) the point intersects with the segment.
		# otherwise:
		#  < 0.0: behind p0.
		#  > 1.0: infront of p1.
		w = sub_v3v3(p0, p_co)
		fac = -dot_v3v3(p_no, w) / dot
		u = mul_v3_fl(u, fac)
		return add_v3v3(p0, u)
	else:
		# The segment is parallel to plane
		return None

def add_v3v3(v0, v1):
	return (
		v0[0] + v1[0],
		v0[1] + v1[1],
		v0[2] + v1[2],
	)

def sub_v3v3(v0, v1):
	return (
		v0[0] - v1[0],
		v0[1] - v1[1],
		v0[2] - v1[2],
	)

def dot_v3v3(v0, v1):
	return (
		(v0[0] * v1[0]) +
		(v0[1] * v1[1]) +
		(v0[2] * v1[2])
	)

def len_squared_v3(v0):
	return dot_v3v3(v0, v0)

def mul_v3_fl(v0, f):
	return (
		v0[0] * f,
		v0[1] * f,
		v0[2] * f,
	)

def len_calc(coord1, coord2):
	x1, y1 = coord1
	x2, y2 = coord2
	return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

def tridb_to_points_and_triangles(wrf_type, wrf_name):
	WireframesExport_FormSet1 = MMpy.FormSet("WIREFRAMES_EXPORT", "16.1.1251.2")
	WireframesExport_FormSet1.set_field("POINTSFILE_TYPE", "2")
	WireframesExport_FormSet1.set_field("POINTSFILE", TEMP_TRIDB_POINT)
	WireframesExport_FormSet1.set_field("TRIANGLESFILE_TYPE", "2")
	WireframesExport_FormSet1.set_field("TRIANGLESFILE", TEMP_TRIDB_TRI)
	WireframesExport_FormSet1.set_field("EXPORT_TYPE", "5")
	WireframesExport_FormSet1.set_field("FILENAME", "")
	WireframesExport_FormSet1.set_field("LAYER_BOOL", "0")
	WireframesExport_FormSet1.set_field("WF_NAME", wrf_name)
	WireframesExport_FormSet1.set_field("WFTYPE", wrf_type)
	WireframesExport_FormSet1.set_field("SET_BOOL", "0")
	WireframesExport_FormSet1.set_field("SINGLE_BOOL", "1")

	WireframesExport_FormSet1.run()


def delete_temp_file(path):
	if os.path.exists(path):
		try:
			os.remove(path)
			return True
		except:
			return False
	return True
delete_temp_file(_log_filename)

def getExtrapoledLine(p1, p2, ratio):
	'Creates a line extrapoled in p1->p2 direction'
	a = p1
	b = (p1[0] + ratio * (p2[0] - p1[0]), p1[1] + ratio * (p2[1] - p1[1]))
	return sg.LineString([a, b])


def unit_vector(vector):
	return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
	v1_u = unit_vector(v1)
	v2_u = unit_vector(v2)
	return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def createSilhouette(triPath, triName, silPath, point=None, azimuth=None, ort_switch="2"):
	WireframeSilhouette_FormSet1 = MMpy.FormSet("WIREFRAME_SILHOUETTE", "16.1.1196.1")
	WireframeSilhouette_FormSet1.set_field("AUTOLOAD", "0")
	WireframeSilhouette_FormSet1.set_field("SET_BOOL", "0")
	WireframeSilhouette_FormSet1.set_field("SINGLE_BOOL", "1")
	WireframeSilhouette_FormSet1.set_field("STR_SWITCH", "2")
	WireframeSilhouette_FormSet1.set_field("STR_NAME", silPath, MMpy.append_flag.none)
	WireframeSilhouette_FormSet1.set_field("WF_NAME", triName)
	WireframeSilhouette_FormSet1.set_field("TYPE", triPath)
	if point is None and azimuth is None:
		WireframeSilhouette_FormSet1.set_field("Z", "0.0")
		WireframeSilhouette_FormSet1.set_field("Y", "0")
		WireframeSilhouette_FormSet1.set_field("X", "0")
		WireframeSilhouette_FormSet1.set_field("DIP", "0")
		WireframeSilhouette_FormSet1.set_field("DIR", "90.0")
		WireframeSilhouette_FormSet1.set_field("TRANS_BOOL", "0")
		WireframeSilhouette_FormSet1.set_field("ORT_SWITCH", ort_switch)
		WireframeSilhouette_FormSet1.set_field("ORT_BOOL", "1")
	else:
		WireframeSilhouette_FormSet1.set_field("Z", str(point.z))
		WireframeSilhouette_FormSet1.set_field("Y", str(point.y))
		WireframeSilhouette_FormSet1.set_field("X", str(point.x))
		WireframeSilhouette_FormSet1.set_field("DIP", "90")
		WireframeSilhouette_FormSet1.set_field("DIR", str(azimuth))
		WireframeSilhouette_FormSet1.set_field("TRANS_BOOL", "1")
		WireframeSilhouette_FormSet1.set_field("ORT_SWITCH", "2")
		WireframeSilhouette_FormSet1.set_field("ORT_BOOL", "0")
	WireframeSilhouette_FormSet1.run()


def readSilhouetteCoordinates(silPath):
	# считывание внешнего контура силуэта
	# предполагается, что силуэт имеет один внешний контур
	mmFile = MicromineFile()
	if not mmFile.open(silPath):
		raise Exception("Невозможно открыть файл %s" % silPath)
	points = MultiPoint(mmFile.read(columns=["EAST", "NORTH", "RL"]))
	return points


def getSilhouetteRectangle(silhouette):
	xx, yy = silhouette.x, silhouette.y
	xmin, ymin = min(xx), min(yy)
	xmax, ymax = max(xx), max(yy)
	origin = (xmin, ymin)
	zmean = silhouette.z.mean()
	#
	convexHull = sg.MultiPoint(silhouette).convex_hull
	silArea = convexHull.area

	# создание диапазона углов для поиска наилучшего направления прямоугольника границы
	angleCount = 360
	angleStep = 360 / angleCount
	angleRange = [i * angleStep for i in range(1, angleCount)]
	# вращение
	rotAngle = 0
	optRect = Rectangle([[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin]])
	optRectAreaDiff = optRect.area() - silArea
	for angle in angleRange:
		rotSilPoly = sa.rotate(convexHull, angle, origin=origin)

		xx, yy = rotSilPoly.exterior.coords.xy
		xmin, xmax = min(xx), max(xx)
		ymin, ymax = min(yy), max(yy)

		curRect = Rectangle([[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin]])
		curRectArea = curRect.area()
		curRectAreaDiff = curRectArea - silArea
		if optRectAreaDiff > curRectAreaDiff:
			optRectAreaDiff = curRectAreaDiff
			optRect = curRect.copy()
			rotAngle = angle

	return Rectangle([Point(x, y, zmean) for x, y, z in optRect.rotate(-rotAngle, Point(origin)).coordinates()])


def writeBoundaries(path, geomObject):
	if not os.path.exists(path):
		strStructure = MMpy.FileStruct()
		strStructure.add_field("EAST", MMpy.FieldType.real, 0, 3)
		strStructure.add_field("NORTH", MMpy.FieldType.real, 0, 3)
		strStructure.add_field("RL", MMpy.FieldType.real, 0, 3)
		strStructure.add_field("JOIN", MMpy.FieldType.real, 0, 0)
		MMpy.File.create_from_template(path, "", strStructure)

	mmFile = MicromineFile()
	if not mmFile.open(path):
		raise Exception("Невозможно открыть файл %s" % path)
	for irow in range(mmFile.records_count):
		mmFile.delete_record(1)

	for irow, row in enumerate(geomObject.coordinates()):
		mmFile.add_record()
		for icol, value in enumerate(row):
			mmFile.set_field_value(icol, irow + 1, value)
	mmFile.close()


def getWireframeBoundaries(triPath, triName):
	triName_to_path = triName
	for el in ['*', '.', ',', '/',  ':', '|', '"', "'", '<', '>', '+']:
		if el in triName_to_path:
			triName_to_path = triName_to_path.replace(el, '_')
	silPath = MMpy.Project.path() + os.path.basename(triPath) + '_' + triName_to_path + "__SILHOUETTE.str"

	rect_old_path = MMpy.Project.path() + "__RECT_OLD.STR"
	if not os.path.exists(triPath):
		raise FileNotFoundError("Файл %s не существует" % triPath)
	#
	cnxn = sqlite3.connect(triPath)
	cursor = cnxn.cursor()
	cmd = """
		SELECT XMinimum, XMaximum, YMinimum, YMaximum, ZMinimum, ZMaximum
		FROM GeneralInformation
		WHERE Name = '{0}'
	""".format(triName)
	cursor.execute(cmd)
	wfXmin, wfXmax, wfYmin, wfYmax, wfZmin, wfZmax = cursor.fetchone()
	cnxn.close()
	#
	# создание силуэта
	# проецируем каркас на план
	createSilhouette(triPath, triName, silPath)
	# считывание силуэта
	planeSilhouette = readSilhouetteCoordinates(silPath)
	# находим оптимальный прямоугольник на плоскости плана, в который вписан силуэт
	planeRect = getSilhouetteRectangle(planeSilhouette)
	writeBoundaries(rect_old_path, planeRect)
	xmin, ymin = planeRect.x.min(), planeRect.y.min()
	origin = Point(xmin, ymin)
	planeRectCoords = planeRect.coordinates().tolist()
	planeRectLongSegment = planeRect.longSegment()
	startPoint1, endPoint1 = planeRectLongSegment.coordinates().tolist()
	idxStartPoint1, idxEndPoint1 = planeRectCoords.index(startPoint1), planeRectCoords.index(endPoint1)
	idxStartPoint2, idxEndPoint2 = (idxEndPoint1 + 2) % 4, idxEndPoint1 + 1
	planeStartPoint1, planeStartPoint2 = Point(planeRectCoords[idxStartPoint1]), Point(planeRectCoords[idxStartPoint2])
	planeEndPoint1, planeEndPoint2 = Point(planeRectCoords[idxEndPoint1]), Point(planeRectCoords[idxEndPoint2])

	planeRectShortSegment = LineSegment([planeEndPoint1, planeEndPoint2])
	planeRectLongAx, planeRectShortAx = planeRectLongSegment.vector(), planeRectShortSegment.vector()
	# # создание силуэта
	viewPoint = planeRectLongSegment.start()
	if - ANGLE_DEF_DIRECTION <= planeRectLongSegment.vector().azimuth() <= ANGLE_DEF_DIRECTION or math.degrees(math.pi) - ANGLE_DEF_DIRECTION <= planeRectLongSegment.vector().azimuth() <= math.degrees(math.pi) + ANGLE_DEF_DIRECTION or math.degrees(2*math.pi) - ANGLE_DEF_DIRECTION <= planeRectLongSegment.vector().azimuth() <= math.degrees(2*math.pi) + ANGLE_DEF_DIRECTION:
		wf_orientation = "ЮС"
	elif ANGLE_DEF_DIRECTION < planeRectLongSegment.vector().azimuth() < math.degrees(math.pi) - ANGLE_DEF_DIRECTION or math.degrees(math.pi) + ANGLE_DEF_DIRECTION < planeRectLongSegment.vector().azimuth() < math.degrees(2*math.pi) - ANGLE_DEF_DIRECTION:
		wf_orientation = "ЗВ"
	else:
		wf_orientation = 'Non-defined'

	[delete_temp_file(file) for file in [silPath, rect_old_path, silPath]]

	return planeSilhouette, wf_orientation


class Direction(enum.Enum):
	East = 0
	North = 1
	West = 2
	South = 3
	Up = 4
	Down = 5


class Side(enum.Enum):
	Left = 0
	Right = 1


class Angle(float):
	def __new__(cls, *args):
		nargs = len(args)
		if nargs > 1:                                    raise ValueError("Неверные аргументы")
		if nargs == 0:
			value = 0
		else:
			value = args[0] % 360
		return float.__new__(cls, value)

	def __str__(self):
		return "<ANGLE ({0})>".format(super(Angle, self).__str__())

	def __add__(self, value):
		return Angle(float.__add__(self, value))

	def __radd__(self, value):
		return Angle(float.__radd__(self, value))

	def __sub__(self, value):
		return Angle(float.__sub__(self, value))

	def __rsub__(self, value):
		return Angle(float.__rsub__(self, value))

	def radians(self):
		return math.radians(self)

	def sin(self):
		return math.sin(self.radians())

	def cos(self):
		return math.cos(self.radians())

	def tan(self):
		return math.tan(self.radians())

	@staticmethod
	def betweenVectors(v1, v2):
		if v1.count() != v2.count():                    raise ValueError("Неверные аргументы")
		return np.degrees(np.arccos(np.round(v1.dot(v2) / v1.length() / v2.length(), 4)))


def rotate(obj, angle, origin=None, axis=None):
	if origin is None:        origin = obj.centroid()
	if axis is None:            axis = Vector(0, 0, 1)

	ang = Angle(angle)
	cosA, sinA = ang.cos(), ang.sin()

	if np.all(np.isnan(axis)) or np.isclose(ang, 0):
		return obj.copy()

	_obj = obj.coordinates() - origin.coordinates()
	objMatrix = np.matrix(_obj)

	x, y, z = axis.unit().coordinates()

	rotMatrix = []
	rotMatrix.append([cosA + (1 - cosA) * x * x, (1 - cosA) * x * y - z * sinA, (1 - cosA) * x * z + y * sinA])
	rotMatrix.append([(1 - cosA) * x * y + z * sinA, cosA + (1 - cosA) * y * y, (1 - cosA) * y * z - x * sinA])
	rotMatrix.append([(1 - cosA) * x * z - y * sinA, (1 - cosA) * y * z + x * sinA, cosA + (1 - cosA) * z * z])
	rotMatrix = np.matrix(rotMatrix)

	ret = np.array((rotMatrix * objMatrix.T).T.tolist()) + origin
	if type(obj) in [Point, Vector]:
		ret = ret[0]
	return obj.__class__(np.round(ret, 10))


class EmptyGeometryObject(np.ndarray):
	def __new__(cls, *args):
		return np.asarray([]).view(cls)

	def __str__(self):
		return "<{0} {1}>".format(self.__class__.__name__.upper(), str(self.tolist()))

	def __str__(self):
		return "<{0} {1}>".format(self.__class__.__name__.upper(), str(self.tolist()))

	def area(self):
		return 0

	def centroid(self):
		return EmptyGeometryObject()

	def contains(self, obj, tolerance=1e-7):
		return self.touches(obj, tolerance)

	def coordinates(self):
		return np.array([])

	def count(self):
		return 0

	def length(self):
		return 0

	def isEmpty(self):
		return self.count() == 0

	def rotate(self, angle, origin=None, axis=None):
		return rotate(self, angle, origin, axis)

	def touches(self, obj, tolerance=1e-7):
		if self.shape == obj.shape and self == obj:
			return True
		elif self.isEmpty():
			return False
		elif obj.isEmpty():
			return True
		else:
			return False

	def volume(self):
		return 0


class GeometryCollection(EmptyGeometryObject):
	def __init__(self, *args):
		nargs = len(args)
		if nargs != 1:                                    raise ValueError("Неверные аргументы")
		if not all(map(lambda arg: isinstance(arg, EmptyGeometryObject), args[0])):
			raise TypeError("Неверные аргументы")
		self.__geometries = list(args[0])

	def __str__(self):
		return "<{0} (\n\t{1}\n)>".format(self.__class__.__name__.upper(), "\n\t".join(map(str, self.__geometries)))

	def __repr__(self):
		return "<{0} (\n\t{1}\n)>".format(self.__class__.__name__.upper(), "\n\t".join(map(str, self.__geometries)))

	def geometries(self):
		return self.__geometries

	def area(self):
		return np.array([geom.area() for geom in self.__geometries])

	def centroid(self):
		return MultiPoint([geom.centroid() for geom in self.__geometries])

	def contains(self, obj):
		return np.array([geom.contains(obj) for geom in self.__geometries])

	def count(self):
		return len(self.__geometries)

	def length(self):
		return np.array([geom.length() for geom in self.__geometries])

	def volume(self):
		return np.array([geom.volume() for geom in self.__geometries])


class Point(EmptyGeometryObject):
	def __new__(cls, *args):
		nargs = len(args)
		data = []
		if nargs == 0:
			data = [0, 0, 0]
		elif nargs == 1:
			data = [args[0], 0, 0] if isinstance(args[0], numbers.Number) else list(args[0]) + [0] * (3 - len(args[0]))
		elif nargs in range(1, 4):
			data = [arg for arg in args if isinstance(arg, numbers.Number)] + [0] * (3 - nargs)
		if len(data) == 0:                                raise ValueError("Неверные аргументы")
		return np.asarray(data, dtype=np.float64).view(cls)

	def __add__(self, obj):
		if obj.count() not in [1, self.count()]:
			raise ValueError("Неверная размерность операнда")
		return Vector(super(Point, self).__add__(obj))

	def __sub__(self, obj):
		if obj.count() not in [1, self.count()]:
			raise ValueError("Неверная размерность операнда")
		return Vector(super(Point, self).__sub__(obj))

	def __eq__(self, v):
		return np.all(np.array(v) - np.array(self) == 0)

	def __ne__(self, v):
		return not self == v

	@property
	def x(self):
		return self[0]

	@x.setter
	def x(self, value):
		self[0] = value

	@property
	def y(self):
		return self[1]

	@y.setter
	def y(self, value):
		self[1] = value

	@property
	def z(self):
		return self[2]

	@z.setter
	def z(self, value):
		self[2] = value

	def coordinates(self):
		return np.array(self)

	def centroid(self):
		return Point(self)

	def count(self):
		return 1

	def distance(self, obj):
		if type(obj) == Point:
			return np.sqrt(np.sum(np.power(obj.coordinates() - self.coordinates(), 2)))
		elif type(obj) == MultiPoint:
			return np.sqrt(np.sum(np.power(obj.coordinates() - self.coordinates(), 2), axis=1))
		else:
			raise Exception("Неподдерживаемый тип объекта")


class MultiPoint(Point):
	def __new__(cls, points):
		data = [pt if isinstance(pt, Point) else Point(pt) for pt in points]
		return np.asarray(data, dtype=np.float64).view(cls)

	def __add__(self, obj):
		if obj.count() in [1, self.count()]:
			return MultiVector(super(MultiPoint, self).__add__(obj))
		else:
			raise ValueError("Неверная размерность операнда")

	def __sub__(self, obj):
		if obj.count() in [1, self.count()]:
			return MultiVector(super(MultiPoint, self).__sub__(obj))
		else:
			raise ValueError("Неверная размерность операнда")

	@property
	def x(self):
		return self[0] if len(self[0].shape) != 1 else np.array(self[:, 0])

	@x.setter
	def x(self, value):
		if len(self[0].shape) != 1:
			self[0] = value
		else:
			self[:, 0] = value

	@property
	def y(self):
		return self[1] if len(self[1].shape) != 1 else np.array(self[:, 1])

	@y.setter
	def y(self, value):
		if len(self[1].shape) != 1:
			self[1] = value
		else:
			self[:, 1] = value

	@property
	def z(self):
		return self[2] if len(self[2].shape) != 1 else np.array(self[:, 2])

	@z.setter
	def z(self, value):
		if len(self[2].shape) != 1:
			self[2] = value
		else:
			self[:, 2] = value

	def coordinates(self):
		return np.copy(self)

	### axis = 0 - осреднение по x, y, z
	### т.е. np.mean([[1,2], [3,4]], axis = 0) == [2, 3]
	### т.е. np.mean([[1,2], [3,4]], axis = 1) == [1.5, 3.5]
	def centroid(self):
		return Point(np.mean(self, axis=0).tolist())

	def count(self):
		if len(self.shape) == 1:
			return 1
		else:
			return self.shape[0]

	# проверка, что все вектора в одной плоскости
	def inPlane(self, tolerance=1e-3):
		if self.count() < 4:
			return True
		else:
			vectors = MultiVector(self[1:] - self[:-1])
			units = vectors.unit()
			normals = []
			for product in units[1:].cross(units[0]):
				normal = Vector(product)
				# print (normal)
				if not np.isclose(normal, Vector(), atol=tolerance).all():
					normals.append(normal)
			normals = MultiVector(normals)
			return normals.allParallel()


class Vector(Point):
	def __new__(cls, *args):
		args = list(args)
		nargs = len(args)
		if nargs == 1:
			if type(args[0]) == Direction:
				if args[0] == Direction.East:    args[0] = (1, 0, 0)
				if args[0] == Direction.North:    args[0] = (1, 0, 0)
				if args[0] == Direction.West:    args[0] = (-1, 0, 0)
				if args[0] == Direction.South:    args[0] = (0, -1, 0)
				if args[0] == Direction.Up:        args[0] = (0, 0, 1)
				if args[0] == Direction.Down:    args[0] = (0, 0, -1)
			else:
				args[0] = [args[0], 0, 0] if not hasattr(args[0], "__getitem__") else list(args[0]) + [0] * (
					3 - len(args[0]))
		obj = Point.__new__(cls, *args)
		return obj

	def angle(self, obj):
		return np.degrees(np.arccos(np.round(self.dot(obj) / self.length() / obj.length(), 4)))

	def azimuth(self):
		if not np.any(self[:2]):                    return 0
		beta = np.round(np.degrees(np.arctan2(self.y, self.x)), 6)
		if self.x >= 0:
			angle = 90 - beta
		elif self.y >= 0:
			angle = 450 - beta
		else:
			angle = 90 + abs(beta)
		return angle

	def inclination(self):
		length = self.length()
		if np.isclose([self.z, length], [0, 0]).all():
			return Angle(0)
		else:
			try:
				ret = math.degrees(math.asin(self.z / length)) % 180
			except:
				ret = math.degrees(round(math.asin(self.z / length), 6)) % 180
		if self.z < 0:    ret = ret - 180
		return Angle(ret)

	def length(self):
		return np.linalg.norm(self)

	def unit(self):
		return self / self.length()

	def isCodirectional(self, v, tolerance=1e-7):
		return np.isclose(self.unit().dot(v.unit()), 1, atol=tolerance)

	def isOpposite(self, v, tolerance=1e-7):
		return np.isclose(self.unit().dot(v.unit()), -1, atol=tolerance)

	def isParallel(self, v, tolerance=1e-7):
		return np.sum(np.isclose(self.cross(v).coordinates(), Vector(), atol=tolerance)) == 3

	def isPerpendicular(self, v, tolerance=1e-7):
		return np.isclose(self.dot(v), 0, atol=tolerance)

	@staticmethod
	def fromAngles(phi, theta=0):
		phi, theta = Angle(phi), Angle(90 - theta)
		return Vector(theta.sin() * phi.cos(), theta.sin() * phi.sin(), theta.cos())

	def cross(self, obj):
		if obj.count() != 1:
			raise ValueError("Неверная размерность операнда %s" % str(type(obj)))
		return Vector(np.cross(self, obj))

	def dot(self, obj):
		return np.dot(obj, self)

	def count(self):
		return 1


class MultiVector(MultiPoint):
	def angle(self, obj):
		if type(obj) == LineSegment:
			objVector = obj.vector()
		elif type(obj) == Line:
			objVector = obj.vector()
		elif type(obj) == Plane:
			objVector = obj.normal()
		elif type(obj) == Polygon:
			objVector = obj.normal()
		else:
			objVector = obj

		selfCount, objCount = self.count(), objVector.count()
		if objCount not in [1, selfCount]:
			raise ValueError("Неверная размерность операнда %s" % str(type(obj)))

		selfLength = self.length()
		objLength = objVector.length()
		return np.degrees(np.arccos(np.round(self.dot(objVector) / selfLength / objLength, 4)))

	def dot(self, obj):
		selfCount, objCount = self.count(), obj.count()
		if not objCount in [1, selfCount]:
			raise ValueError("Неверная размерность операнда %s" % str(type(obj)))
		if selfCount == 1 and objCount == 1:
			return Vector(self).dot(Vector(obj))
		else:
			return np.array(np.sum(self * obj, axis=1))

	def cross(self, obj):
		selfCount, objCount = self.count(), obj.count()
		if objCount not in [1, selfCount]:
			raise ValueError("Неверная размерность операнда %s" % str(type(obj)))
		if objCount == 1 and selfCount == 1:
			return Vector(np.cross(self, obj))
		else:
			return MultiVector(np.cross(self, obj))

	def length(self):
		if self.count() <= 1:
			return Vector(self).length()
		else:
			return np.linalg.norm(self, axis=1)

	def unit(self):
		count = self.count()
		if count <= 1:
			return Vector(self).unit()
		else:
			return self / self.length()[:, None]

	def isCodirectional(self, v, tolerance=1e-7):
		if self.count() == 1 and v.count() == 1:
			return Vector(self).isCodirectional(Vector(v), tolerance)
		else:
			return np.isclose(self.unit().dot(v.unit()), 1, atol=tolerance)

	def isOpposite(self, v, tolerance=1e-7):
		if self.count() == 1 and v.count() == 1:
			return Vector(self).isOpposite(Vector(v), tolerance)
		else:
			return np.isclose(self.unit().dot(v.unit()), -1, atol=tolerance)

	def isParallel(self, obj, tolerance=1e-7):
		if obj.count() == 1 and self.count() == 1:
			return Vector(self).isParallel(Vector(obj))
		else:
			return np.sum(np.isclose(self.cross(v).coordinates(), Vector(), atol=tolerance), axis=1) == 3

	def isPerpendicular(self, v, tolerance=1e-7):
		if self.count() == 1 and v.count() == 1:
			return Vector(self).isPerpendicular(Vector(v), tolerance)
		else:
			return np.isclose(self.dot(v), 0, atol=tolerance)

	def allParallel(self, tolerance=1e-7):
		selfCount = self.count()
		if selfCount < 2:
			return True
		elif selfCount == 2:
			return Vector(self[0]).isParallel(Vector(self[1]), tolerance)
		else:
			return np.all(
				np.sum(np.isclose(self[1:].cross(self[0]).coordinates(), Vector(), atol=tolerance), axis=1) == 3)

	def azimuth(self):
		if self.count() == 1:
			return Vector(self).azimuth()
		else:
			angles = np.degrees(np.arctan2(self.y, self.x))
			indexes = self.x >= 0
			angles[indexes] = 90 - angles[indexes]
			indexes = (self.y >= 0) & (self.x < 0)
			angles[indexes] = 450 - angles[indexes]
			indexes = (self.x < 0) & (self.y < 0)
			angles[indexes] = 90 + np.abs(angles[indexes])
			indexes = (self.x == 0) & (self.y == 0)
			angles[indexes] = 0
			return angles


class LineString(MultiVector):
	def __new__(cls, data):
		obj = MultiVector.__new__(cls, data)
		diff = np.diff(obj.coordinates(), axis=0)
		indexes = np.where(np.sum(np.isclose(diff, (0, 0, 0)), axis=1) == 3)[0]
		lsdata = [row for irow, row in enumerate(data) if not irow in indexes]
		return MultiVector.__new__(cls, lsdata)

	def angle(self, obj):
		raise ValueError("Операция не применима для это типа. Используйте векторы")

	def __add__(self, obj):
		if obj.count() in [1, self.count()]:
			return LineString(super(LineString, self).__add__(obj))
		else:
			raise ValueError("Неверная размерность операнда")

	def __sub__(self, obj):
		if obj.count() in [1, self.count()]:
			return LineString(super(LineString, self).__sub__(obj))
		else:
			raise ValueError("Неверная размерность операнда")

	def length(self):
		if self.count() <= 1:
			return Vector(self).length()
		else:
			return np.sum(np.linalg.norm(self.vectors(), axis=1))

	def vectors(self):
		return MultiVector(np.diff(self.coordinates(), axis=0))

	def segments(self):
		return tuple(map(LineSegment, zip(self[:-1], self[1:])))

	def centroid(self):
		centers = 0.5 * (self[:-1] + self[1:])
		return Point(np.ma.average(centers, axis=0, weights=self.vectors().length()))

	def interpolate(self, distance=np.nan, ratio=np.nan):
		if np.isnan(distance) and np.isnan(ratio):        raise ValueError("Неверные аргументы")
		if ratio and (ratio < 0 or ratio > 1):            return EmptyGeometryObject()

		cumsum = np.cumsum(np.linalg.norm(self.vectors(), axis=1))
		if not np.isnan(ratio) and np.isnan(distance):
			distance = cumsum[-1] * ratio

		index = None
		for i, cumlen in enumerate(cumsum, start=1):
			if cumlen > distance:
				index = i - 1
				break

		segment = self.segments()[index]
		rem = distance - cumsum[index - 1]
		return segment.interpolate(rem)

	def ratio(self, point, tolerance=1e-7):
		length = self.length()
		cumlen = 0
		for segment in self.segments():
			if segment.contains(point, tolerance):
				rem = (point - segment.start()).length()
				return round((cumlen + rem) / length / tolerance) * tolerance
			cumlen += segment.length()
		return np.nan

	def touched(self, points, tolerance):
		return GeometryCollection([segment for segment in self.segments() if segment.contains(points, tolerance)])

	def touches(self, points, tolerance=1e-7):
		ret = super(LineString, self).touches(points, tolerance)
		if ret:                                            return True
		if points.count() > 1:
			return np.array([self.touched(row, tolerance).count() > 0 for row in points])
		else:
			return self.touched(points, tolerance).count() > 0


class LineSegment(MultiPoint):
	def __new__(cls, data):
		obj = MultiPoint.__new__(cls, data)
		if obj.count() != 2:        raise Exception("Неверные аргументы")
		return obj

	### ??? Смысл сложения
	def __add__(self, obj):
		if obj.count() in [1, self.count()]:
			return LineSegment(super(LineSegment, self).__add__(obj))
		else:
			raise ValueError("Неверная размерность операнда")

	def __sub__(self, obj):
		if obj.count() in [1, self.count()]:
			return LineSegment(super(LineSegment, self).__sub__(obj))
		else:
			raise ValueError("Неверная размерность операнда")

	def end(self):
		return self[1]

	def interpolate(self, distance=None, ratio=None):
		if distance is None and ratio is None:            raise ValueError("Неверные аргументы")
		if ratio and (ratio < 0 or ratio > 1):            return EmptyGeometryObject()
		if ratio and distance is None:
			distance = self.length * ratio
		return Point(np.round(self.start() + self.vector().unit() * distance, 10))

	def ratio(self, points, tolerance=1e-7):
		ret = (points - self.start()).length() / self.length() if self.contains(points, tolerance) else np.nan
		ret = int(ret / tolerance) * tolerance
		return ret

	def start(self):
		return self[0]

	def touched(self, point, tolerance=1e-7):
		if point.count() != 1:
			raise ValueError("Неверные аргументы")
		v1, v2 = point - self.start(), self.end() - point
		cross = v1.cross(v2)
		isclose = np.isclose(cross, Vector(), atol=tolerance)
		if not all(isclose):                            return EmptyGeometryObject()

		length, v1len, v2len = self.length(), v1.length(), v2.length()

		if np.isclose(v1len, 0, atol=tolerance):        return Point(self.start())
		if np.isclose(v2len, 0, atol=tolerance):        return Point(self.end())
		if v1len <= length and v2len <= length:            return self.copy()
		return EmptyGeometryObject()

	def touches(self, points, tolerance=1e-7):
		return self.touched(points, tolerance).count() > 0

	def vector(self):
		return Point(self.end()) - Point(self.start())

	def length(self):
		return self.vector().length()


class Polygon(LineString):
	def __new__(cls, data):
		data = list(data)
		if list(data[0]) != list(data[-1]):
			data.append(data[0])
		return LineString.__new__(cls, data)

	def interiorAngles(self):
		if not self.inPlane():
			raise Exception(
				"Невозможно вычислить внутренние углы, так как точки полигона не принадлежат одной плоскости")

		normal = self.normal().unit().coordinates()

		vectors = self.vectors()
		normVectors = vectors.length  # np.linalg.norm(vectors, axis = 1)
		unitVectors = vectors / normVectors[:, None]

		crossProducts = np.cross(-unitVectors[:-1], unitVectors[1:])
		normCrossProducts = np.linalg.norm(crossProducts, axis=1)
		unitCrossProducts = crossProducts / normCrossProducts[:, None]

		directions = np.sum(unitCrossProducts * normal, axis=1) == 1

		angles = np.degrees(np.arccos(np.sum(-unitVectors[:-1] * unitVectors[1:], axis=1)))
		angles = [angle if not isCodirectional else 360 - angle for angle, isCodirectional in zip(angles, directions)]
		npoints = len(angles) + 1
		angles.insert(0, 180 * (npoints - 2) - sum(angles))

		return np.array([Angle(round(value, 4)) for value in angles])

	def exteriorAngles(self):
		return 360 - self.interiorAngles()

	def normal(self):
		vectors = self.vectors()
		for i in range(len(vectors) - 1):
			if not vectors[i].isParallel(vectors[i + 1]):
				nvector = vectors[i].cross(vectors[i + 1])
		return nvector

	def area(self):
		if not self.inPlane():
			raise Exception("Невозможно вычислить площадь, так как точки полигона не принадлежат одной плоскости")
		return 0.5 * np.linalg.norm(np.sum(self[:-1].cross(self[1:]), axis=0))


class Rectangle(Polygon):
	def length(self):                                return self.longSegment().length()

	def width(self):                                return self.shortSegment().length()

	def longSegment(self):
		segments = self.segments()
		s1len, s2len = segments[0].length(), segments[1].length()
		ret = segments[0] if s1len >= s2len else segments[1]
		return ret

	def shortSegment(self):
		segments = self.segments()
		return segments[0] if segments[1] == self.longSegment() else segments[1]


class Line(EmptyGeometryObject):
	def __init__(self, *args):
		vector = None
		point = Point(0, 0, 0)

		nargs = len(args)
		# three points
		if nargs == 3 and not all(map(lambda arg: hasattr(arg, "__getitem__"), args)):
			vector = Vector(args[:2])
		# two points, point and vector, point and direction
		if nargs == 2:
			if all(map(lambda arg: type(arg) == Point, args)):
				point = args[0]
				vector = args[1] - args[0]
			if type(args[0]) == Point and type(args[1]) == Vector:
				point = args[0]
				vector = args[1]
			if type(args[0]) == Point and isinstance(args[1], Direction):
				point = args[0]
				vector = Vector(args[1])
		# segment
		if nargs == 1 and isinstance(args[0], LineSegment):
			point = args[0].start()
			vector = args[0].vector()

		if vector is None or point is None:
			raise ValueError("Неверные аргументы")

		self.__point = point
		self.__vector = vector

	@property
	def vector(self):
		return self.__vector


class Plane(EmptyGeometryObject):
	def __init__(self, *args):
		nargs = len(args)
		point = None
		vector = None

		if nargs == 3 and all(map(lambda arg: isinstance(arg, Point))):
			point = args[0]
			v1 = args[1] - point
			v2 = args[2] - point
			vector = v1 ** v2
		if nargs == 2 and type(args[0]) == Vector and type(args[1]) == Point:
			point = args[1]
			vector = args[0]
		if nargs == 2 and type(args[0]) in [LineSegment, Line] and type(args[1]) == Point:
			point = args[1]
			vector = args[0].vector

		if vector is None or point is None:
			raise ValueError("Неверные аргументы")

		self.__point = point
		self.__vector = vector

	def __str__(self):
		return "PLANE ({0}; {1})".format(str(self.vector), str(self.point))

	@property
	def point(self):
		return self.__point

	@property
	def vector(self):
		return self.__vector


class GetMiddleLine(DialogWindow):
	def __init__(self, parent, use_connection=True):
		super(GetMiddleLine, self).__init__(parent, use_connection)
		self.setMinimumSize(743, 605)
		self.__createVariables()
		self.setWindowTitle("Построение осевой линии")
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
		self._parent = parent
		self.calculation_error = False
		if NO_LIBRARIES: self.__no_libraries()

	def __no_libraries(self):
		qtmsgbox.show_information(self.main_app, "Информация", "Метод построения 'Графы' недоступен.\nНе установлены необходимые библиотеки.")

	def __createVariables(self):
		self._tridbitems = {}

	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Атрибуты")
		self.inputLayout = QtGui.QVBoxLayout()

		self.table = QtGui.QTableWidget()
		self.table.setColumnCount(3)
		self.table.setColumnWidth(0, 300)
		self.table.setColumnWidth(1, 135)
		self.table.setColumnWidth(2, 135)
		self.table.setHorizontalHeaderLabels(['Имя', 'Источник', 'По умолчанию'])
		self.table.verticalHeader().hide()
		if not self.main_app.settings.getValue(ID_SIM_SET):
			data2 = [""] * len(MIDDLE_LINE_ATTRIBUTES)
		else:
			data2 = [i[1] for i in self.main_app.settings.getValue(ID_SIM_SET)]
		combo_box_options = ["", "Option 1", "Option 2", "Option 3"]

		self.table.setRowCount(len(MIDDLE_LINE_ATTRIBUTES))

		for index in range(len(MIDDLE_LINE_ATTRIBUTES)):
			item1 = QtGui.QTableWidgetItem(MIDDLE_LINE_ATTRIBUTES[index])
			item1.setFlags(QtCore.Qt.ItemIsEnabled)
			self.table.setItem(index, 0, item1)

			combo = QtGui.QComboBox()
			for t in combo_box_options:
				combo.addItem(t)
			self.table.setCellWidget(index, 1, combo)

			item2 = QtGui.QTableWidgetItem(data2[index])
			item2.setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled)
			item2.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
			self.table.setItem(index, 2, item2)

		self.groupBoxOption = QtGui.QGroupBox(self)
		self.groupBoxOption.setTitle("Опции")
		self.layoutOutBoxOption = QtGui.QGridLayout(self.groupBoxOption)

		self.layout_methods = QtGui.QHBoxLayout()
		self.label_method = QtGui.QLabel(self.groupBoxOption)
		self.label_method.setText("Метод построения:")

		self.radioButtonBox = QtGui.QButtonGroup(self.groupBoxOption)
		self.radioButtonSimple = QtGui.QRadioButton(self.groupBoxOption)
		self.radioButtonSimple.setText("Интерполяция (для простых каркасов)")
		self.radioButtonSimple.setChecked(True)
		self.radioButtonComplex = QtGui.QRadioButton(self.groupBoxOption)
		self.radioButtonComplex.setText("Графы (для сложных каркасов)")
		if NO_LIBRARIES:
			self.radioButtonComplex.setDisabled(True)
		self.radioButtonBox.addButton(self.radioButtonSimple)
		self.radioButtonBox.addButton(self.radioButtonComplex)

		self.layout_tolerances_douglas = QtGui.QHBoxLayout()
		self.label_douglas_tolerance = QtGui.QLabel(self.groupBoxOption)
		self.label_douglas_tolerance.setText("Сглаживание (Дуглас-Пекер):".ljust(25))
		self.label_douglas_tolerance.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
		self.lineEdit_douglas_tolerance = QtGui.QLineEdit(self.groupBoxOption)
		self.lineEdit_douglas_tolerance.setToolTip("Алгоритм Дугласа-Пекера.\n"
												  "Чем больше этот коэффициент,\n"
												  "тем сильнее сглаживание.\n"
												   "Рекомендуемые значения от 1 до 10.\n"
												  "По умолчанию = 1.")
		self.lineEdit_douglas_tolerance.setText("1")

		self.layout_tolerances = QtGui.QHBoxLayout()
		self.label_length_tolerance = QtGui.QLabel(self.groupBoxOption)
		self.label_length_tolerance.setText("Сглаживание (по длине):".ljust(25))
		self.label_length_tolerance.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
		self.lineEdit_length_tolerance = QtGui.QLineEdit(self.groupBoxOption)
		self.lineEdit_length_tolerance.setToolTip("Чем больше этот коэффициент,\n"
												  "тем точнее проецируется каркас.\n"
												  "Но точнее - не значит лучше!\n"
												  "Рекомендуемые значения от 30 до 100.\n"
												  "По умолчанию = 80.")
		self.lineEdit_length_tolerance.setText("80")

		self.layout_tolerances_angle = QtGui.QHBoxLayout()
		self.label_angle_tolerance = QtGui.QLabel(self.groupBoxOption)
		self.label_angle_tolerance.setText("Сглаживание (по углу):".ljust(27))
		self.label_angle_tolerance.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
		self.lineEdit_angle_tolerance = QtGui.QLineEdit(self.groupBoxOption)
		self.lineEdit_angle_tolerance.setToolTip("Чем меньше этот коэффициент,\n"
												  "тем точнее проецируется каркас.\n"
												  "Но точнее - не значит лучше!\n"
												  "Рекомендуемые значения от 0.1 до 4.\n"
												  "По умолчанию = 1.5")
		self.lineEdit_angle_tolerance.setText("1.5")

		self.layout_dummy = QtGui.QHBoxLayout()
		self.label_dummy = QtGui.QLabel(self.groupBoxOption)
		self.label_dummy.setText(" "*70)

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QFormLayout()
		self.label_output_folder = QtGui.QLabel(self.groupBoxOutput)
		self.label_output_folder.setText("Файл")
		self.label_output_folder.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
		self.label_output_folder.setMinimumWidth(100)
		self.lineEdit_output_file = QtGui.QLineEdit(self.groupBoxOutput)
		self.lineEdit_output_file.setReadOnly(True)
		self.checkBox_autoload = QtGui.QCheckBox(self.groupBoxOutput)
		self.checkBox_autoload.setText("Автозагрузка")
		self.checkBox_autoload.setChecked(True)

	def __createBindings(self):
		self.lineEdit_output_file.mouseDoubleClickEvent = lambda e: self.select_output_file()
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.addWidget(self.table)

		self.layoutControls.addWidget(self.groupBoxOption)
		self.layoutOutBoxOption.addLayout(self.layout_methods, 0, 0, 1, 2)
		self.layout_methods.addWidget(self.label_method)
		self.layout_methods.addWidget(self.radioButtonSimple)
		self.layout_methods.addWidget(self.radioButtonComplex)

		self.layoutOutBoxOption.addLayout(self.layout_tolerances_douglas, 1, 0, 1, 1)
		self.layout_tolerances_douglas.addWidget(self.label_douglas_tolerance)
		self.layout_tolerances_douglas.addWidget(self.lineEdit_douglas_tolerance)

		self.layoutOutBoxOption.addLayout(self.layout_tolerances_angle, 2, 0, 1, 1)
		self.layout_tolerances_angle.addWidget(self.label_angle_tolerance)
		self.layout_tolerances_angle.addWidget(self.lineEdit_angle_tolerance)

		self.layoutOutBoxOption.addLayout(self.layout_tolerances, 3, 0, 1, 1)
		self.layout_tolerances.addWidget(self.label_length_tolerance)
		self.layout_tolerances.addWidget(self.lineEdit_length_tolerance)


		self.layoutOutBoxOption.addLayout(self.layout_dummy, 1, 1, 1, 1)
		self.layout_dummy.addWidget(self.label_dummy)

		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_output_folder)
		self.layoutOutput.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEdit_output_file)
		self.layoutOutput.setWidget(1, QtGui.QFormLayout.FieldRole, self.checkBox_autoload)
		# self.groupBoxOption.setLayout(self.layout_tolerances)

	def select_output_file(self):
		filetypes = "СТРИНГ (*.STR)"
		try:
			initialdir = self._parent.settings.getValue(ID_NETPATH)
		except:
			initialdir = os.getcwd()
		path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir, filetypes).replace("/", "\\")
		if not path:
			return
		if '.str' not in path.lower():
			path = path + '.STR'
		self.lineEdit_output_file.setText(path)

	def build_comboBox_attributes(self, attributes):
		for index in range(self.table.rowCount()):
			combo = QtGui.QComboBox()
			for t in attributes:
				combo.addItem(t)
			self.table.setCellWidget(index, 1, combo)

	def exec(self):
		if not super(GetMiddleLine, self).prepare():
			return

		tridb_items = self.selectedItems()[ITEM_TRIDB]
		attributes = []

		if not tridb_items:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите каркасы")
			return

		for tridb_item in tridb_items:
			net_tridb_path = tridb_item.filePath()
			tridb = Tridb(net_tridb_path)
			attributes.extend(tridb.user_attributes)

		[i.upper() for i in attributes]
		attributes = sorted(list(set(attributes)))
		attributes.insert(0, "")
		self.build_comboBox_attributes(attributes)
		super(GetMiddleLine, self).exec()

	def prepare(self):
		if not self.lineEdit_output_file.text():
			qtmsgbox.show_error(self, "Ошибка", "Необходимо указать файл вывода")
			self.lineEdit_output_file.setFocus()
			return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))

	def run(self):
		self.calculation_error = False
		# пока такой обход возможных ошибок при вводе коэффициентов сглаживания
		num_len_intervals = self.lineEdit_length_tolerance.text()
		try: num_len_intervals = float(num_len_intervals)
		except: num_len_intervals = ''
		if not num_len_intervals: num_len_intervals = 80

		angle_tolerance_degrees = self.lineEdit_angle_tolerance.text()
		try: angle_tolerance_degrees = float(angle_tolerance_degrees)
		except: angle_tolerance_degrees = ''
		if not num_len_intervals: num_len_intervals = 1

		tolerance_douglas = self.lineEdit_douglas_tolerance.text()
		try:
			tolerance_douglas = float(tolerance_douglas)
		except:
			tolerance_douglas = ''
		if not tolerance_douglas: num_len_intervals = 1

		tridb_items = self.selectedItems()[ITEM_TRIDB]
		wfdata = []
		self.not_success = []
		netTridbPathsDict = self.getTridbPathsDict(tridb_items, copy=False)
		for tripath in netTridbPathsDict:
			for wfname in netTridbPathsDict[tripath]:
				wfdata.append([tripath, wfname])
		middle_line_count = 0
		output_file_path = self.lineEdit_output_file.text()
		delete_temp_file(output_file_path)
		if self.radioButtonSimple.isChecked():
			method = 1
		else:
			method = 2

		chosen_attributes_fields = [self.table.cellWidget(i, 1).currentText() for i in
									range(len(MIDDLE_LINE_ATTRIBUTES))]
		default_attributes_values = [self.table.item(i, 2).text() for i in range(len(MIDDLE_LINE_ATTRIBUTES))]

		wrf_number = 0
		wrf_total_number = len(wfdata)
		for net_tridb_path, wrf_name in wfdata:
			wrf_number += 1
			print('{} out of {}'.format(wrf_number, wrf_total_number))
			try:
				tridb = Tridb(net_tridb_path)
				middle_line_attributes = []
				middle_line_attributes.append(tridb.name.lower().split('.tridb')[0])
				middle_line_attributes.append(tridb[wrf_name].default_attribute('Name'))
				for k, val in enumerate(chosen_attributes_fields):
					if val == '':
						middle_line_attributes.append(default_attributes_values[k])
					elif tridb[wrf_name].user_attribute_for_mdl_line(val) not in ['', False]:
						middle_line_attributes.append(tridb[wrf_name].user_attribute_for_mdl_line(val))
					else:
						middle_line_attributes.append(default_attributes_values[k])
				middle_line_count += 1
				# Получаем координаты проекции каркаса на план
				points, orientation = getWireframeBoundaries(net_tridb_path, wrf_name)
				# points = points + [points[1]]
				# print(type(points))
				# print(points)
				# print("Первоначальные точки в результате проекции ", points)
				# print("Ориентация в пространстве ", orientation)
				silhouette_polygon = sg.Polygon(points[:])
				# print(orientation)
				# print(silhouette_polygon)
				# Определяем допуск по длине
				len_tolerance = silhouette_polygon.length / num_len_intervals

				# print('до дугласа точки ', points)

				# Первый фильтр для сглаживания (удаление лишних точек)
				# Этот коффициент подобран в процессе тестирования скрипта
				# temp_silhoutte = sg.LineString(points)
				# simplified_temp_silhoutte = temp_silhoutte.simplify(tolerance_douglas)
				# points = [list(i) for i in list(simplified_temp_silhoutte.coords)]
				# points.append(points[0])
				# points.pop(0)
				# points = MultiPoint(points)
				#
				# temp_silhoutte = sg.LineString(points)
				# simplified_temp_silhoutte = temp_silhoutte.simplify(tolerance_douglas)
				# points = [list(i) for i in list(simplified_temp_silhoutte.coords)]
				# points.append(points[0])
				# points.pop(0)
				# points = MultiPoint(points)


				temp_silhoutte = sg.LineString(points)
				simplified_temp_silhoutte = temp_silhoutte.simplify(tolerance_douglas)
				points = [list(i) for i in list(simplified_temp_silhoutte.coords)]
				points = MultiPoint(points)

				# print('после дугласа точки', points)


				# Критерий коллинеарности. Убираем одну из трех соседних точек, если они находятся на прямой
				cur_point = points[0]
				points_after_collinear_filter = [cur_point, ]
				# Критерий на основе скалярного произведение. Явное задание угла
				for i in range(1, len(points) - 1):
					if abs(angle_between([points[i + 1][0] - cur_point[0], points[i + 1][1] - cur_point[1]],
										 [points[i][0] - cur_point[0], points[i][1] - cur_point[1]])) > math.radians(
						angle_tolerance_degrees):
						points_after_collinear_filter.append(points[i])
						cur_point = points[i]

				# cur_point = points[0]
				# points_after_second_collinear_filter = [cur_point, ]
				# for i in range(1, len(points) - 1):
				#     if not math.radians(160) <= abs(angle_between([points[i + 1][0] - points[i][0], points[i + 1][1] - points[i][1]],
				#                          [cur_point[0] - points[i][0], cur_point[1] - points[i][1]])) <= math.radians(200):
				#         points_after_second_collinear_filter.append(points[i])
				#         cur_point = points[i]

				# print('после критерия скалярного произведения ', points_after_collinear_filter)
				# pointsss= np.array(points_after_collinear_filter)
				# plt.plot(pointsss[:, 0], pointsss[:, 1], 'b*')
				# plt.savefig('D:\после_фильтрации.png')
				# plt.close('all')
				# Критерий по длине. Убираем одну из двух соседних точек, если расстояние между ними меньше допуска
				cur_point = points_after_collinear_filter[0]
				points_after_length_filter = [cur_point, ]
				for i, val in enumerate(points_after_collinear_filter[1:]):
					if math.sqrt((val[0] - cur_point[0]) ** 2 + (val[1] - cur_point[1]) ** 2) > len_tolerance:
						points_after_length_filter.append(val)
						cur_point = val
				# print('после критерия по длине ', points_after_length_filter)
				# После фильтрации у нас не замкнутый контур, поэтому замыкаем его, добавлением первой точки в конец списка
				points_after_length_filter.append(points_after_collinear_filter[0])
				# Проверяем
				# print("После фильтрации точек ", points)
				# temp_linestring = sg.LineString(points)
				# simplified_linestring = temp_linestring.simplify(0.1)
				# points_after_length_filter = list(simplified_linestring.coords)

				points = np.array(points)
				points_new2 = np.array(points_after_length_filter)
				# print("После фильтрации точек ", points_after_length_filter)
				# plt.plot(points_new2[:, 0], points_new2[:, 1], 'r+')
				# plt.savefig('D:\после_фильтрации.png')
				# plt.close()
				# gc.collect()

				# Для большого количества точек определяем середины через триангуляцию
				if len(points_new2) > 5:
					# print('больше 5 точек')
					# Чтобы найти осевую линию, сначала делаем триангуляцию
					points_for_triangulation = np.array([[i.x, i.y] for i in points_after_length_filter])
					# print([[i.x, i.y] for i in points_after_length_filter])
					triangulation = Delaunay(points_for_triangulation)
					# plt.triplot(points_for_triangulation[:, 0], points_for_triangulation[:, 1], 'r+')
					# plt.triplot(points_for_triangulation[:, 0], points_for_triangulation[:, 1], triangulation.simplices.copy())
					# plt.savefig('D:\треугольники.png')
					# plt.close()
					# return
					# Найдем координаты треугольников триангуляции и разложим в список списков по треугольникам (с помощью индексов точек треугольников triangulation.simplices)
					# triangulation.points[tri.simplices] - может давать неправильные результаты, см. документацию
					# Кстати, индексы у точек в triangulation.simplices такие как в исходном контуре
					triangulation_points_coord = []
					for tri_index in triangulation.simplices:
						triangulation_points_coord.append(
							[triangulation.points[tri_index[i]] for i in range(len(tri_index))])
					# Создаем полигоны из треугольников триангуляции
					triangulation_polygons = [sg.Polygon(triangulation_points_coord[i]) for i in
											  range(len(triangulation_points_coord))]
					# Отфильтруем треугольники триангуляции, которые не попадают в исходную проекцию каркаса
					triangulation_points_coord_inside_proj = []
					triangulation_points_index_inside_proj = []
					projection = sg.Polygon(points_after_length_filter)
					for i, triangle in enumerate(triangulation_polygons):
						if projection.contains(triangle):
							triangulation_points_coord_inside_proj.append(triangle)
							triangulation_points_index_inside_proj.append(triangulation.simplices[i].tolist())

					triangulation_points_coord_result = []
					triangulation_points_index_result = []
					# # print(triangulation_points_coord_inside_proj)
					# print([polygon.area for polygon in triangulation_points_coord_inside_proj])
					# # Дополнительно отсеиваваемый треугольники с маленькой площадью
					# # Обычно они лежат близко к сторонам проекции и ведут к неправильным результатам
					median_area = statistics.median([polygon.area for polygon in triangulation_points_coord_inside_proj])
					# print('median = ', median_area)
					average_area = sum([polygon.area for polygon in triangulation_points_coord_inside_proj])/len(triangulation_points_coord_inside_proj)
					# print('average = ', average_area)
					# print('number of triangles inside = ', len(triangulation_points_coord_inside_proj))
					for i, triangle in enumerate(triangulation_points_coord_inside_proj):
						# print(triangle.area)
						if triangle.area > TRIANGLE_TOLERANCE * average_area:
							triangulation_points_coord_result.append(triangulation_points_coord_inside_proj[i])
							triangulation_points_index_result.append(triangulation_points_index_inside_proj[i])
					# print('number of triangles after median filter = ', len(triangulation_points_coord_result))
					# Найдем общие точки общих граней у треугольников. Середины этих граней и дадут осевую линию
					if len(triangulation_points_coord_result) <= 2:
						raise Exception('В силуэте каркаса после фильтрации триангуляции осталось только {} треугольника.\n'.format(len(triangulation_points_coord_result)))
					common_sides_triangles_index = []
					cur_index = 1
					for triangle in triangulation_points_index_result:
						for i in range(cur_index, len(triangulation_points_index_result)):
							if len(set(triangle) & set(triangulation_points_index_result[i])) == 2:
								common_sides_triangles_index.append(
									list((set(triangle) & set(triangulation_points_index_result[i]))))
						cur_index += 1
					# В итоге находим середины граней, делая mapping между индексами и координатами
					intersection_points = []
					for i in common_sides_triangles_index:
						intersection_points.append(
							[(points_for_triangulation[i[0]][0] + points_for_triangulation[i[1]][0]) / 2,
							 (points_for_triangulation[i[0]][1] + points_for_triangulation[i[1]][1]) / 2])
					# print('ориентация = ', orientation)
					# print('точки пересечений ', intersection_points)
					if method == 1 and orientation == 'ЗВ':
						# Полученные точки необходимо правильным образом упорядочить, используем интерполяцию
						x__ = [point[0] for point in intersection_points]
						y__ = [point[1] for point in intersection_points]
						if x__[0] >= x__[-1]:
							x__ = list(reversed(x__))
							y__ = list(reversed(y__))
						x_ = np.array(x__)
						y_ = np.array(y__)
						change_direction = False
						change_direction_second_time = False
						# Иногда определенное направление выработки не совпадает с получающимся направлением оси
						# Которая получена из средних точек общих сторон треугольников триангуляции
						# Тогда считаем триангуляцию главнее, и меняем ориентацию выработки
						# ВООБЩЕ ПОТОМ МОЖНО БУДЕТ ПОПРОБОВАТЬ ДЕЛАТЬ ИНАЧЕ
						# НАХОДИТЬ ПРЯМУЮ ПЕРПЕНДИКУЛЯРНУЮ ИСХОДНОЙ
						# ТАК КАК ОБЫЧНО НАЙДЕННАЯ ОРИЕНТАЦИЯ ВЫРАБОТКИ - КОРРЕКТНА
						if abs(x__[0] - x__[-1]) > abs(y__[0] - y__[-1]):
							if len(intersection_points) <= 1:
								raise Exception('Для построения средней линии нужно как минимум две точки.\n'
											'В триангуляции осталось меньше двух точку.')
							x_middle_line = np.linspace(x_.min(), x_.max(), 100)
							f = interp1d(x_, y_, kind='linear')
							y_middle_line = f(x_middle_line)
							x_middle_line = x_middle_line.tolist()[:]
							y_middle_line = y_middle_line.tolist()[:]

							# Проверка на большие расстояния между точками
							# Это может произойти если неверно определено направление выработки
							# Например, если ось параллельна короткой стороне описанного прямоугольника вокруг проекции, а не длинной стороне
							all_points = list(zip(x_middle_line, y_middle_line))
							all_lengths = []
							for i in range(len(all_points) - 1):
								all_lengths.append(len_calc(all_points[i], all_points[i+1]))
							median_length = statistics.median(
								[length for length in all_lengths])
							# print(median_length, all_lengths)
							for length in all_lengths:
								if length > median_length * LEN_TOLERANCE_DEF_DIRECTION:
									change_direction = True
									# print('direction changed')
									break
						else:
							change_direction = True
						if change_direction:
							orientation = 'ЮС'
							# print('change direction from запад-восток на юг-север')
							x__ = [point[0] for point in intersection_points]
							y__ = [point[1] for point in intersection_points]
							if y__[0] >= y__[-1]:
								x__ = list(reversed(x__))
								y__ = list(reversed(y__))
							x_ = np.array(x__)
							y_ = np.array(y__)
							# print('исопльзуемые координаты x ', x_)
							# print('исопльзуемые координаты н ', y_)

							# x_ = np.array([point[0] for point in intersection_points])
							# y_ = np.array([point[1] for point in intersection_points])
							y_middle_line = np.linspace(y_.min(), y_.max(), 100)
							f = interp1d(y_, x_, kind='linear')
							x_middle_line = f(y_middle_line)
							x_middle_line = x_middle_line.tolist()[:]
							y_middle_line = y_middle_line.tolist()[:]
							# print('найденая линия x после интерполяции', x_middle_line)
							# print('найденая линия y после интерполяции', y_middle_line)

							all_points = list(zip(x_middle_line, y_middle_line))
							all_lengths = []
							for i in range(len(all_points) - 1):
								all_lengths.append(len_calc(all_points[i], all_points[i + 1]))
							median_length = statistics.median(
								[length for length in all_lengths])
							# print(median_length, all_lengths)
							for length in all_lengths:
								if length > median_length * LEN_TOLERANCE_DEF_DIRECTION:
									# print('direction changed second time')
									change_direction_second_time = True
									orientation = 'ЗВ'
									break
						if change_direction_second_time:
							self.not_success.append(tridb.name.lower().split('.tridb')[0] + ' ' + wrf_name)


					elif method == 1 and orientation == 'ЮС':
						# Полученные точки необходимо правильным образом упорядочить, используем интерполяцию
						# В дальнейшем будем использовать другой алгоритм
						x__ = [point[0] for point in intersection_points]
						y__ = [point[1] for point in intersection_points]
						if y__[0] >= y__[-1]:
							x__ = list(reversed(x__))
							y__ = list(reversed(y__))
						x_ = np.array(x__)
						y_ = np.array(y__)
						change_direction = False
						change_direction_second_time = False
						# print('для интерполяции x ', x_)
						# print('для интерполяции y ', y_)
						if abs(y__[0] - y__[-1]) > abs(x__[0] - x__[-1]):
							if len(intersection_points) <= 1:
								raise Exception('Для построения средней линии нужно как минимум две точки.\n'
												'В триангуляции осталось меньше двух точку.')
							y_middle_line = np.linspace(y_.min(), y_.max(), 100)
							f = interp1d(y_, x_, kind='linear')
							x_middle_line = f(y_middle_line)
							x_middle_line = x_middle_line.tolist()[:]
							y_middle_line = y_middle_line.tolist()[:]
							# print('сразу после интерполяции x ', x_middle_line)
							# print('сразу после интерполяции y ', y_middle_line)

							# Проверка на большие расстояния между точками
							# Это может произойти если неверно определено направление выработки
							# Например, если ось параллельна короткой стороне описанного прямоугольника вокруг проекции, а не длинной стороне
							all_points = list(zip(x_middle_line, y_middle_line))
							all_lengths = []
							for i in range(len(all_points) - 1):
								all_lengths.append(len_calc(all_points[i], all_points[i + 1]))
							median_length = statistics.median(
								[length for length in all_lengths])
							for length in all_lengths:
								if length > median_length * LEN_TOLERANCE_DEF_DIRECTION:
									change_direction = True
									# print('direction changed')
									break
						else:
							change_direction = True
						if change_direction:
							orientation = 'ЗВ'
							x_ = np.array([point[0] for point in intersection_points])
							y_ = np.array([point[1] for point in intersection_points])
							x_middle_line = np.linspace(x_.min(), x_.max(), 100)
							f = interp1d(x_, y_, kind='linear')
							y_middle_line = f(x_middle_line)
							x_middle_line = x_middle_line.tolist()[:]
							y_middle_line = y_middle_line.tolist()[:]

							all_points = list(zip(x_middle_line, y_middle_line))
							all_lengths = []
							for i in range(len(all_points) - 1):
								all_lengths.append(len_calc(all_points[i], all_points[i + 1]))
							median_length = statistics.median(
								[length for length in all_lengths])
							for length in all_lengths:
								if length > median_length * LEN_TOLERANCE_DEF_DIRECTION:
									change_direction_second_time = True
									orientation = 'ЮС'
									# print('direction changed second time')
									break
						if change_direction_second_time:
							self.not_success.append(tridb.name.lower().split('.tridb')[0] + ' ' + wrf_name)

					elif method == 2:
						x_ = np.array([point[0] for point in intersection_points])
						y_ = np.array([point[1] for point in intersection_points])
						points_ml = np.array(intersection_points)
						clf = NearestNeighbors(2).fit(points_ml)
						G = clf.kneighbors_graph()
						T = nx.from_scipy_sparse_matrix(G)
						paths = [list(nx.dfs_preorder_nodes(T, i)) for i in range(len(points_ml))]
						mindist = np.inf
						minidx = 0

						for i in range(len(points_ml)):
							p = paths[i]  # order of nodes
							ordered = points_ml[p]  # ordered nodes
							# find cost of that order by the sum of euclidean distances between points_ml (i) and (i+1)
							cost = (((ordered[:-1] - ordered[1:]) ** 2).sum(1)).sum()
							if cost < mindist:
								mindist = cost
								minidx = i

						opt_order = paths[minidx]
						xx = x_[opt_order]
						yy = y_[opt_order]

						start_index = round(len(xx[1:]) / 2)
						cur_point = (xx[start_index], yy[start_index])

						x_middle_line = [xx[start_index], ]
						y_middle_line = [yy[start_index], ]

						for i in range(start_index + 1, len(xx) - 1):
							if angle_between([xx[i] - cur_point[0], yy[i] - cur_point[1]],
											 [xx[i + 1] - xx[i], yy[i + 1] - yy[i]]) < math.radians(100):
								x_middle_line.append(xx[i])
								y_middle_line.append(yy[i])
								cur_point = (xx[i], yy[i])
							else:
								break
							if i == len(xx) - 2:
								x_middle_line.append(xx[i + 1])
								y_middle_line.append(yy[i + 1])

						for i in range(start_index - 1, 0, -1):
							if angle_between([xx[i] - cur_point[0], yy[i] - cur_point[1]],
											 [xx[i - 1] - xx[i], yy[i - 1] - yy[i]]) < math.radians(90):
								x_middle_line.insert(0, xx[i])
								y_middle_line.insert(0, yy[i])
								cur_point = (xx[i], yy[i])
							else:
								x_middle_line.insert(0, xx[i])
								y_middle_line.insert(0, yy[i])
								break
							if i == 1:
								x_middle_line.insert(0, xx[i - 1])
								y_middle_line.insert(0, yy[i - 1])
					# построение точек, когда больше 5 вершин
					# plt.plot(xx, yy, 'b+')
					# plt.plot(x_middle_line, y_middle_line, 'r')
					# plt.savefig(r'D:\треугольники.png')
					# plt.close()
				# Для четырехугольника найдем середины коротких сторон и соединим
				elif len(points_new2) in [5]:
					# print('4-х угольник!')
					x_middle_line = []
					y_middle_line = []
					# рабочий вариант
					# if np.linalg.norm(points_new2[0] - points_new2[1]) > np.linalg.norm(
					#                 points_new2[1] - points_new2[2]):
					# тестовый вариант
					if np.linalg.norm(points_new2[0] - points_new2[1])**2 + np.linalg.norm(points_new2[2] - points_new2[3])**2 >= \
							np.linalg.norm(points_new2[1] - points_new2[2])**2 + np.linalg.norm(points_new2[3] - points_new2[4])**2:
						x_middle_line.append(np.array([(points_new2[1][0] + points_new2[2][0]) / 2]))
						y_middle_line.append(np.array([(points_new2[1][1] + points_new2[2][1]) / 2]))
						x_middle_line.append(np.array([(points_new2[3][0] + points_new2[4][0]) / 2]))
						y_middle_line.append(np.array([(points_new2[3][1] + points_new2[4][1]) / 2]))
					else:
						x_middle_line.append(np.array([(points_new2[0][0] + points_new2[1][0]) / 2]))
						y_middle_line.append(np.array([(points_new2[0][1] + points_new2[1][1]) / 2]))
						x_middle_line.append(np.array([(points_new2[2][0] + points_new2[3][0]) / 2]))
						y_middle_line.append(np.array([(points_new2[2][1] + points_new2[3][1]) / 2]))
						# plt.figure()
						# plt.plot(x_middle_line, y_middle_line, 'r')
				else:
					raise Exception("Недостаточное количество точек в проекции выработки {} {}. "
									"Процесс остановлен!".format(tridb.name.lower().split('.tridb')[0], wrf_name))

				# Полученная средняя линия может не доходить до границ проекции каркаса, поэтому удлиняем ее NUM_EXTRAPOLATED раз
				# Коэффициент в зависимости от метода
				# print('x before extra ', x_middle_line)
				# print('y before extra ', y_middle_line)
				if method == 1:
					ratio = EXTRAPOLATED_RATIO_INTERPOLATION_method
				elif method == 2:
					ratio = EXTRAPOLATED_RATIO_SKLEARN_method
				# try:
				# p1 = [x_middle_line[1], y_middle_line[1]]
				# except:
				#     raise Exception("Не удалось получить осевую линию выработки {} {}.\n"
				#                     "Процесс остановлен!\nПопробуйте метод интерполяции".format(tridb.name.lower().split('.tridb')[0], wrf_name))

				# Нужно не только продлить среднюю линию за пределы крайних точек
				# Добавим интерполяцию - линейно добавим точки между существующими
				if len(points_new2) in [4, 5]:
					x_middle_line_to_interpolate = [i.tolist()[0] for i in x_middle_line]
					y_middle_line_to_interpolate = [i.tolist()[0] for i in y_middle_line]
				else:
					x_middle_line_to_interpolate = copy.deepcopy(x_middle_line)
					y_middle_line_to_interpolate = copy.deepcopy(y_middle_line)

				# plt.figure()
				# plt.plot(x_middle_line_to_interpolate, y_middle_line_to_interpolate, 'r*')
				# plt.show()

				# print("точки для интерполяции")
				# print(x_middle_line_to_interpolate)
				# print(y_middle_line_to_interpolate)
				if orientation == 'ЗВ':
					# print('Интерполяция для каркаса Запад-Восток')
					if x_middle_line_to_interpolate[0] >= x_middle_line_to_interpolate[-1]:
						x_middle_line_to_interpolate = list(reversed(x_middle_line_to_interpolate))
						y_middle_line_to_interpolate = list(reversed(y_middle_line_to_interpolate))
					x_middle_line = np.linspace(x_middle_line[0], x_middle_line[-1], len_calc([x_middle_line[0], y_middle_line[0]], [x_middle_line[-1], y_middle_line[-1]]) / INTERPOLATED_LENGTH)
					y_middle_line = np.interp(x_middle_line, x_middle_line_to_interpolate, y_middle_line_to_interpolate)
				else:
					# print('Интерполяция для каркаса Юг-Восток')
					if y_middle_line_to_interpolate[0] >= y_middle_line_to_interpolate[-1]:
						x_middle_line_to_interpolate = list(reversed(x_middle_line_to_interpolate))
						y_middle_line_to_interpolate = list(reversed(y_middle_line_to_interpolate))
					y_middle_line = np.linspace(y_middle_line[0], y_middle_line[-1],
												len_calc([x_middle_line[0], y_middle_line[0]],
														 [x_middle_line[-1], y_middle_line[-1]]) / INTERPOLATED_LENGTH)
					x_middle_line = np.interp(y_middle_line, y_middle_line_to_interpolate, x_middle_line_to_interpolate)

				# plt.figure()
				# plt.plot(x_middle_line, y_middle_line, 'r')
				# plt.show()
				# А вот тут удлиняем
				x_middle_line = x_middle_line.tolist()
				y_middle_line = y_middle_line.tolist()
				# print('перед экстраполяцией ', x_middle_line)
				# print('перед экстраполяцией ', y_middle_line)

				p0 = [x_middle_line[0], y_middle_line[0]]
				p1 = [x_middle_line[1], y_middle_line[1]]
				p_n_1 = [x_middle_line[len(x_middle_line) - 2], y_middle_line[len(x_middle_line) - 2]]
				p_n = [x_middle_line[len(x_middle_line) - 1], y_middle_line[len(x_middle_line) - 1]]

				for i in range(NUM_EXTRAPOLATED):
					line_start = getExtrapoledLine(p1, p0, 1 + ratio * (i + 1))
					line_end = getExtrapoledLine(p_n_1,
												 p_n, 1 + ratio * (i + 1))
					x_middle_line.insert(0, list(line_start.coords)[1][0])
					y_middle_line.insert(0, list(line_start.coords)[1][1])
					x_middle_line.insert(len(x_middle_line), list(line_end.coords)[1][0])
					y_middle_line.insert(len(y_middle_line), list(line_end.coords)[1][1])

				# Кладем осевую линию обратно на подошву каркаса
				# print('x extra ', x_middle_line)
				# print('y extra ', y_middle_line)
				# plt.plot(x_middle_line, y_middle_line, 'r')
				# plt.show()
				tridb_to_points_and_triangles(net_tridb_path, wrf_name)
				wrf_points_file = MicromineFile()
				wrf_tri_file = MicromineFile()
				if not wrf_points_file.open(TEMP_TRIDB_POINT):
					raise Exception("Невозможно открыть файл '%s'" % TEMP_TRIDB_POINT)
				if not wrf_tri_file.open(TEMP_TRIDB_TRI):
					raise Exception("Невозможно открыть файл '%s'" % TEMP_TRIDB_TRI)
				points = wrf_points_file.read()
				tris = wrf_tri_file.read()
				wrf_points_file.close()
				wrf_tri_file.close()
				# Создадим список полигонов
				tri_poligons = []
				for index, tri in enumerate(tris):
					polygon = sg.Polygon([points[int(vertex)][1:] for vertex in tri[1:]])
					tri_poligons.append(polygon)
				# print(len(tri_poligons))
				# Определим в какой из полигонов попадает точка плоской осевой линии и пересчитаем координаты
				middle_line_points = [sg.Point(x_middle_line[index], y_middle_line[index]) for index in
									  range(len(x_middle_line))]
				middle_line_points_final = []
				for point in middle_line_points:
					cur_polygon = None
					cur_z = 999999
					final_coords = None
					for polygon in tri_poligons:
						if point.within(polygon):
						# Находим координаты проекции точки плоской осевой линии на плоскость полигона, в который попала точка
							normal_vector_to_polygon, plane_point = get_normal_vector_and_point(polygon)
							coords_cur = list(isect_line_plane_v3([point.x, point.y, -999999], [point.x, point.y, 999999],
														 plane_point.tolist(), normal_vector_to_polygon.tolist()))
							if cur_z > coords_cur[2]:
								cur_z = coords_cur[2]
								cur_polygon = polygon
								final_coords = copy.deepcopy(coords_cur)
					if cur_polygon and final_coords:
						p = final_coords + [middle_line_attributes[1]] + [middle_line_count] + \
							[middle_line_attributes[0]] + middle_line_attributes[2:]
						middle_line_points_final.append(p)
				write_strings_in_file(output_file_path, middle_line_points_final)
				[delete_temp_file(p) for p in [TEMP_TRIDB_POINT, TEMP_TRIDB_TRI]]
			# plt.show()
			except Exception as e:
				with open(_log_filename, "a") as file:
					exc_type, exc_obj, exc_tb = sys.exc_info()
					fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
					file.write("Каркас {}: {}\nОшибка:\n".format(net_tridb_path, wrf_name) + str(e))
					file.write(str(exc_type) + '\n')
					file.write(fname + '\n')
					file.write(str(exc_tb.tb_lineno) + '\n')
					self.calculation_error = True
					# shutil.copy(net_tridb_path, r'D:\D_Micromine_Projects\Октябрьский\ГЕО\Медь\Панель_1\trouble-trouble')

				continue

		if self.checkBox_autoload.isChecked():
			defaultLoadStrings(output_file_path, 'Осевые линии')
		if self.calculation_error:
			qtmsgbox.show_information(self.main_app, "Завершено", "В процессе расчета возникли проблемы.\nПроверьте файл лога.")
		elif self.not_success:
			problems = '\n'.join(self.not_success)
			qtmsgbox.show_information(self.main_app, "Завершено", "В процессе расчета возникли проблемы.\nПроверьте полученные осевые линии выработок:\n{}".format(problems))
		else:
			qtmsgbox.show_information(self.main_app, "Выполнено", "Осевые линии успешно получены.")
