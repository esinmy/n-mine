try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os
from datetime import datetime

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import ProcessWindow

from mm.mmutils import Tridb, MicromineFile
from mm.formsets import create_wireframe_set, loadTriangulationSet, loadBlockModel

from utils.constants import *
from utils.blockmodel import BlockModel
from utils.osutil import get_file_owner, get_temporary_prefix
from utils.validation import is_blank
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class CustomAssignBm(ProcessWindow):
	def __init__(self, *args, **kw):
		super(CustomAssignBm, self).__init__(*args, **kw)
		self.__create_variables()

	def __create_variables(self):
		self._netBmPathList = []			# список сетевых блочных моделей
		self._locBmPathList = []			# список локальных блочных моделей
		self._locBmPartPathList = []		# список извлеченных кусков по непустому полю

		self._wireframe_sets = []			# список использованных наборов каркасов

	def check(self):
		# проверяем среди выбранных каркасов наличие маркшейдерских каркасов
		tridbItems = self.selectedItems()[ITEM_TRIDB]
		if not tridbItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите  каркасы")
			return

		bmItems = self.selectedItems()[ITEM_BM]
		if not bmItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите блочные модели")
			return
		#
		sysdate = datetime.now()
		currdates = []
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()

			# проверка наличия файла блокировки
			if self.lockFileExists(netBmPath):
				lockpath = self.getLockPath(netBmPath)
				msg = "Блочная модель используется другим пользователем %s." % get_file_owner(lockpath)
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			bm = BlockModel(netBmPath, self.main_app)
			# проверка наличия метаданных блочных моделей
			if not bm.has_metadata:
				msg = "Блочная модель '%s' не имеет метаданных. Обратитесь к геологу" % bm.name
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			# проверка на соответствие метаданных содержанию блочной модели
			metadate = bm.metadata.current_date
			bmdate = bm.blockmodel_date
			if bmdate.year != metadate.year or bmdate.month != metadate.month:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Метаданные не соответствуют содержанию блочной модели '%s'. Обратитесь к геологу." % bm.name)
				return

			currdates.append(metadate)

		currdates = list(sorted(set(currdates)))
		if len(currdates) > 1:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выбранные блочные модели переведены в разные месяца. Выберите блочные модели, переведенные в один и тот же месяц")
			return
		elif (currdates[0].year == sysdate.year and currdates[0].month != sysdate.month) or (currdates[0].year < sysdate.year):
			if not qtmsgbox.show_question(self.main_app, "Внимание", "Состояние блочных моделей не соответствует текущей календарной дате. Продолжить?"):
				return
		return True

	# подготовка к запуску
	def prepare(self):
		if not super(CustomAssignBm, self).prepare():
			return

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	def run(self):
		super(CustomAssignBm, self).run()

		progress = self.main_app.mainWidget.progressBar

		bmItems = self.selectedItems()[ITEM_BM]
		tridbItems = self.selectedItems()[ITEM_TRIDB]

		subxsize = self.main_app.settings.getValue(ID_SUBXSVY)
		subysize = self.main_app.settings.getValue(ID_SUBYSVY)
		subzsize = self.main_app.settings.getValue(ID_SUBZSVY)

		### =================================================================================== ###
		### ============================== КОПИРОВАНИЕ КАРКАСОВ =============================== ###
		### =================================================================================== ###

		try:					loctripaths = self.getTridbPathsDict(tridbItems, copy = True)
		except Exception as e:	raise Exception(str(e))
		
		### =================================================================================== ###
		### =========================== СОЗДАНИЕ НАБОРА КАРКАСОВ ============================== ###
		### =================================================================================== ###

		wfdata = []
		for loctripath in loctripaths:
			for wfname in loctripaths[loctripath]:
				wfdata.append([loctripath, DEFAULT_ATTR_NAME, wfname, nettripath])
		#
		self._wireframe_sets = []
		for row in wfdata:
			wfset = create_wireframe_set([row[:-1]])
			tridbName = os.path.basename(row[-1])
			tridbName = tridbName[:tridbName.rfind(".")]
			self._wireframe_sets.append([wfset, ":".join([tridbName, row[2]])])

		nzfill = len(str(len(self._wireframe_sets)))

		#
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()
			net_bm = BlockModel(netBmPath, self.main_app)
			locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % net_bm.name)

			self._netBmPathList.append(netBmPath)
			self._locBmPathList.append(locBmPath)

			### =================================================================================== ###
			### ============================ КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ =========================== ###
			### =================================================================================== ###

			progress.setText("Копирование блочной модели")
			err = self.exec_proc(self.copy_file, netBmPath, locBmPath)
			if err:					raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
			if self.is_cancelled:	return

			loc_bm = BlockModel(locBmPath, self.main_app)

			f = MicromineFile()
			if not f.open(locBmPath):
				raise Exception("Невозможно открыть файл %s" % locBmPath)
			structure = f.structure
			structure.add_field(BMFLD_SYS_MINENAME, MMpy.FieldType.character, 25, 0)
			structure.add_field(BMFLD_SYS_INTERVAL, MMpy.FieldType.character, 25, 0)
			f.structure = structure
			f.close()

			### =================================================================================== ###
			### ============================ ПРИСВАИВАНИЕ АТРИБУТОВ =============================== ###
			### =================================================================================== ###

			for i, (wfset, displayName) in enumerate(self._wireframe_sets, start = 1):
				assing_params = {}
				assing_params["attributes"] = [[1, displayName, BMFLD_SYS_MINENAME]]
				assing_params["sub_xsize"] = subxsize
				assing_params["sub_ysize"] = subysize
				assing_params["sub_zsize"] = subzsize
				assing_params["overwrite_bool"] = True
				assing_params["purge_bool"] = True
				assing_params["wireframe_set"] = wfset
				try:					loc_bm.assign_wireframes(assing_params)
				except Exception as e:	raise Exception("Не удалось присвоить атрибуты. %s" % str(e))

				### =================================================================================== ###
				### ======================= ИЗВЛЕЧЕНИЕ КУСКА БЛОЧНОЙ МОДЕЛИ =========================== ###
				### =================================================================================== ###

				locBmPartPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_PART%s.DAT" % (net_bm.name, str(i).zfill(nzfill)))
				self._locBmPartPathList.append(locBmPartPath)

				# создаем файл куска блочной модели
				MicromineFile.create(locBmPartPath, structure)

				# извлечение куска блочной модели
				err = self.exec_proc(self.extractBlockModelData, locBmPath, BMFLD_SYS_MINENAME, locBmPartPath)
				if err:
					raise Exception("Невозможно извлечь данные из блочной модели. %s" % err)

		return True
		