try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os, shutil

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
from gui.qt.dialogs.CalcStockDialog import CalcStockDialog

from gui.qt.ProcessWindow import ProcessWindow
from gui.qt.modes.DisplayData import DisplayData
from gui.qt.modes.AutoVizexPlot import get_pel_extents

from mm.mmutils import Tridb, MicromineFile
from mm.formsets import create_wireframe_set, copy_wireframe, loadIsolineSet, display_limits, create_macro, create_plot_form

from utils.constants import *
from utils.validation import is_blank
from utils.osutil import get_temporary_prefix

import geometry as geom
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def loadContour(path, display_name):
	WsLoadStrings_FormSet1= MMpy.FormSet("WS_LOAD_STRINGS","16.1.1131.0")
	WsLoadStrings_FormSet1.set_field("TRANSPARENCY","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ANGLE_FORMAT","1")
	WsLoadStrings_FormSet1.set_field("PLAN","0")
	WsLoadStrings_FormSet1.set_field("CHECK_ANGLE_FIELD","0")
	WsLoadStrings_FormSet1.set_field("DISPLAY","0")
	WsLoadStrings_FormSet1.set_field("POLYGON_COLOUR",MMpy.Colour(0,0,100).serialise())
	WsLoadStrings_FormSet1.set_field("CHECK","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIELD","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("GRADIENT","0")
	WsLoadStrings_FormSet1.set_field("SINGLE_POINTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("HATCH",MMpy.Hatch(MMpy.Colour(0,0,0),MMpy.Colour(255,255,255),MMpy.Colour(0,0,0),False,MMpy.LineType.solid,13,0,"MM Hatch Patterns 1").serialise())
	WsLoadStrings_FormSet1.set_field("POLYGONS_BOOL","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_LAST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_FIRST","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_NTH","0")
	WsLoadStrings_FormSet1.set_field("POINT_SHOW_ALL","1")
	WsLoadStrings_FormSet1.set_field("ON","0")
	WsLoadStrings_FormSet1.set_field("BELOW","0")
	WsLoadStrings_FormSet1.set_field("ABOVE","1")
	WsLoadStrings_FormSet1.set_field("POINT_COLOUR",MMpy.Colour(0,0,100).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_COLOUR",MMpy.Colour(0,0,100).serialise())
	WsLoadStrings_FormSet1.set_field("SEGMENT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("POINT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("POINT_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet1.set_field("INCLINATION_FORMAT","1")
	WsLoadStrings_FormSet1.set_field("BEARING_FORMAT","0")
	WsLoadStrings_FormSet1.set_field("SEGMENT_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INCLINATION","0")
	WsLoadStrings_FormSet1.set_field("BEARING","0")
	WsLoadStrings_FormSet1.set_field("CUM_LENGTH","0")
	WsLoadStrings_FormSet1.set_field("INT_ANGLE","0")
	WsLoadStrings_FormSet1.set_field("SEQ","0")
	WsLoadStrings_FormSet1.set_field("Z","0")
	WsLoadStrings_FormSet1.set_field("Y","0")
	WsLoadStrings_FormSet1.set_field("X","0")
	WsLoadStrings_FormSet1.set_field("SEGMENTS","0")
	WsLoadStrings_FormSet1.set_field("LABEL_POINTS","0")
	WsLoadStrings_FormSet1.set_field("SHOW_POINTS","0")
	WsLoadStrings_FormSet1.set_field("LINE_TYPE","0")
	WsLoadStrings_FormSet1.set_field("JOIN_VARB","JOIN")
	WsLoadStrings_FormSet1.set_field("STRING_VARB","")
	WsLoadStrings_FormSet1.set_field("THICK","1")
	WsLoadStrings_FormSet1.set_field("DEFAULT_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet1.set_field("Z_VARB","RL")
	WsLoadStrings_FormSet1.set_field("Y_VARB","NORTH")
	WsLoadStrings_FormSet1.set_field("X_VARB","EAST")
	WsLoadStrings_FormSet1.set_field("FILTER","0")
	WsLoadStrings_FormSet1.set_field("FILE_TYPE","2")
	WsLoadStrings_FormSet1.set_field("WSSTR_FILE", path)
	WsLoadStrings_FormSet1.run(display_name)

def wireframeMerge(outpath, outname, wfdata):
	formset = MMpy.FormSet("WIREFRAME_BOOLEAN","16.1.1122.0")
	DataGrid2 = MMpy.DataGrid(2,1)
	DataGrid2.set_column_info(0,1,-1)
	DataGrid2.set_column_info(1,0,8)
	DataGrid2.set_row(0, ["",""])
	formset.set_field("ATT_GRID",DataGrid2.serialise())
	formset.set_field("F","0")
	DataGrid3 = MMpy.DataGrid(2,1)
	DataGrid3.set_column_info(0,2,11)
	DataGrid3.set_column_info(1,3,12)
	for i, row in enumerate(wfdata):
		DataGrid3.set_row(i, row)
	formset.set_field("WFBOOL_GRID",DataGrid3.serialise())
	formset.set_field("TRIANGLES","0")
	formset.set_field("OUTPUT","1")
	formset.set_field("VIZEX","0")
	formset.set_field("CHECK","0")
	formset.set_field("OPERATION","0")
	formset.set_field("COLOUR",MMpy.Colour(128,128,0).serialise())
	formset.set_field("NAME", outname)
	formset.set_field("TYPE", outpath)
	formset.set_field("COLOUR1",MMpy.Colour(128,128,0).serialise())
	formset.set_field("USER","1")
	formset.set_field("A","0")
	formset.set_field("BOUTA_CHECK1","0")
	formset.set_field("BINA_CHECK1","0")
	formset.set_field("AOUTB_CHECK1","0")
	formset.set_field("AINB_CHECK1","1")
	formset.set_field("NAME1", outname)
	formset.set_field("TYPE1", outpath)
	formset.run()

def dtmVolume(groundTridbPath, groundName, dtmTridbPath, dtmName, strPath, dbFilter, reportPath):
	formset = MMpy.FormSet("DTM_VOLUME","16.1.1122.0")
	formset.set_field("SW","1")
	formset.set_field("JOIN_FLD", ID_JOIN)
	formset.set_field("STRING_FLD","")
	formset.set_field("FLTNUM", dbFilter)
	formset.set_field("FLTBOOL","1")
	formset.set_field("SELECT5","0")
	formset.set_field("SELECT4","0")
	formset.set_field("SELECT3","0")
	formset.set_field("SELECT2","0")
	formset.set_field("SELECT1","0")
	formset.set_field("OUTLINEBOOL","0")
	formset.set_field("STRINGBOOL","1")
	formset.set_field("YFLD", ID_NORTH)
	formset.set_field("XFLD", ID_EAST)
	formset.set_field("FTYPE","2")
	formset.set_field("FILE", strPath)
	formset.set_field("DTM2_TYPE", dtmTridbPath)
	formset.set_field("DTM1_TYPE", groundTridbPath)
	formset.set_field("REPORTFILE", reportPath, MMpy.append_flag.none)
	formset.set_field("SHOWTRI","0")
	formset.set_field("DTMFILE2", dtmName)
	formset.set_field("DTMFILE1", groundName)
	formset.set_field("VOLUME","0")
	formset.run()

#
class SvyCalcStock(ProcessWindow):
	def __init__(self, *args, **kw):
		super(SvyCalcStock, self).__init__(*args, **kw)

	def create_pel(self, pelpath):
		mcrpath = os.path.join(self._tmpfolder, "%sSCRIPT_MACRO.MCR" % get_temporary_prefix())
		rptpath = os.path.join(self._tmpfolder, "%sSCRIPT_MACRO_REPORT.MOF" % get_temporary_prefix())
		# создание пустого файла макроса
		
		create_macro(mcrpath)
		# создание формы файла чертежа
		plot_id = create_plot_form()
		# запись в макрос
		f = MicromineFile()
		if not f.open(mcrpath):
			raise Exception("Невозможно открыть файл '%s'" % mcrpath)
		f.add_record()
		f.set_field_value(0, 1, "VXPLOT")
		f.set_field_value(1, 1, str(plot_id))
		f.set_field_value(2, 1, pelpath)
		f.close()
		# запуск макроса
		MMpy.Macro.run(mcrpath, rptpath)
		# удаление файла макроса и отчета
		try:		os.remove(mcrpath)
		except:		pass
		try:		os.remove(rptpath)
		except: 	pass

	def check(self):
		stockex = self.main_app.mainWidget.stockExplorer

		# проверка на наличие складов
		stockItems = stockex.stockItems()
		if not stockItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите склады")
			return

		# проверка на наличие каркасов
		for stockItem in stockItems:
			if stockItem.groundDirItem().childCount() == 0:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите каркас основания для склада '%s'" % stockItem.name())
				return
			if stockItem.topoDirItem().childCount() == 0:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите каркас поверхности для склада '%s'" % stockItem.name())
				return

		# проверка на наличие плотностей
		for stockItem in stockItems:
			if stockItem.sectorDirItem().childCount() == 0:
				if is_blank(stockItem.density()):
					qtmsgbox.show_error(self.main_app, "Ошибка", "Укажите плотность для '%s'" % stockItem.name())
					return
			elif any(is_blank(sectorItem.density()) for sectorItem in stockItem.sectorItems()):
				qtmsgbox.show_error(self.main_app, "Ошибка", "Укажите плотность для всех секторов склада '%s'" % stockItem.name())
				return

		return True

	def prepare(self):
		super(SvyCalcStock, self).prepare()

		if not self.check():
			return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	def run(self):
		if not super(SvyCalcStock, self).run():
			return

		dialog = CalcStockDialog(self.main_app)
		dialog.exec()
		if dialog.cancelled:
			self._is_cancelled = True
			return

		### =================================================================================== ###
		### =============================== ОБЪЯВЛЕНИЕ ПЕРЕМЕННЫХ ============================= ###
		### =================================================================================== ###

		progress = self.main_app.mainWidget.progressBar
		progress.setRange(0,0)

		scale = dialog.comboBoxScale.currentText().split(":")[-1]
		isoLineFormsetId = int(dialog.lineEditIsoLine.text()) if dialog.lineEditIsoLine.text() else None
		templatePath = dialog.getTemplatePath()
		templateBodyRoot = dialog.getTemplateBodyRoot()
		outputDirectory = dialog.lineEditOutputDirectory.text()
		outputName = dialog.lineEditOutputName.text()
		outputPlotDirectory = os.path.join(outputDirectory, outputName)

		stockex = self.main_app.mainWidget.stockExplorer
		stockItems = stockex.stockItems()

		groundTridbItems = []
		for stockItem in stockItems:
			groundTridbItems.extend(stockItem.groundDirItem().iterChildren())
		stockStringItems = list(filter(lambda item: item.isString(), stockex.explorer.iterChildren()))
		
		tmpStrPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "STRING.STR")
		reportPathMask = os.path.join(self._tmpfolder, get_temporary_prefix() + "REPORT_%s.RPT")
		tmpReportPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "TEMP_REPORT.RPT")
		tmpTridbPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "DTM.TRIDB")
		tmpGroundName = "GROUND"
		tmpTopoNameMask = "TOPO_%s"

		reportPathList = []

		stockField, sectorField = "STOCK", "SECTOR"

		planMask = "222222"
		scaleMask = "999999"

		### =================================================================================== ###
		### ================================= ОБЪЕДИНЕНИЕ СТРИНГОВ ============================ ###
		### =================================================================================== ###

		# создание файла
		structure = ["%s:R:0:6" % ID_EAST, "%s:R:0:6" % ID_NORTH, "%s:R:0:6" % ID_RL, "%s:N:4:0" % ID_JOIN, "%s:C:50:0" % stockField, "%s:C:50:0" % sectorField]
		if not MicromineFile.create(tmpStrPath, structure):
			raise Exception("Невозможно создать файл '%s'" % tmpStrPath)

		fdst = MicromineFile()
		if not fdst.open(tmpStrPath):
			raise Exception("Невозможно открыть файл '%s'" % tmpStrPath)

		for stringItem in stockStringItems:
			filepath = stringItem.filePath()
			#
			fsrc = MicromineFile()
			if not fsrc.open(filepath):
				raise Exception("Невозможно открыть файл '%s'" % filepath)
			#
			srcHeader = fsrc.header
			fields = [ID_EAST, ID_NORTH, ID_RL, ID_JOIN, stringItem.stockField, stringItem.sectorField]
			dstIndexes = list(range(6))
			srcIndexes = []
			for field in fields:
				if field == fields[-1] and not field:
					continue
				if not field in srcHeader:
					raise Exception("Поле '%s' отсутствует в файле '%s'" % (field, filepath))
				srcIndexes.append(srcHeader.index(field))

			# копирование данных
			for irow in range(fsrc.records_count):
				fdst.add_record()
				for srcIndex, dstIndex in zip(srcIndexes, dstIndexes):
					fdst.set_field_value(dstIndex, fdst.records_count, fsrc.get_str_field_value(srcIndex, irow+1))
			fsrc.close()
		fdst.close()

		### =================================================================================== ###
		### ============================ ИЗВЛЕЧЕНИЕ КЛЮЧЕВЫХ ЗАПИСЕЙ ========================== ###
		### =================================================================================== ###

		userKeys = []
		f = MicromineFile()
		if not f.open(tmpStrPath):
			raise Exception("Невозможно открыть файл '%s'" % tmpStrPath)
		for row in f.read(asstr = True):
			stockName, sectorName = row[-2:]
			if not (stockName, sectorName) in userKeys:
				userKeys.append((stockName, sectorName))
		f.close()

		### =================================================================================== ###
		### =============================== ОБЪЕДИНЕНИЕ ОСНОВАНИЙ ============================= ###
		### =================================================================================== ###

		progress.setText("Объединение оснований")
		# создаем временную tridb
		Tridb.create(tmpTridbPath)
		#
		wfdata = []
		groundTridbPaths = []
		for groundTridbItem in groundTridbItems:
			tripath = groundTridbItem.filePath()
			if tripath in groundTridbPaths:
				continue
			#
			for wfItem in groundTridbItem.iterChildren():
				wfname = wfItem.text(0)
				wfdata.append([tripath, wfname])
			groundTridbPaths.append(tripath)
		#
		if len(groundTridbPaths) > 1:
			try:					wireframeMerge(tmpTridbPath, tmpGroundName, wfdata)
			except Exception as e:	raise Exception("Невозможно объединить основания. %s" % str(e))
		else:
			try:					copy_wireframe(wfdata[0][0], wfdata[0][1], tmpTridbPath, tmpGroundName, False)
			except Exception as e:	raise Exception("Невозможно скопировать каркас основания. %s" % str(e))

		### =================================================================================== ###
		### ============================= ОБЪЕДИНЕНИЕ ПОВЕРХНОСТЕЙ ============================ ###
		### =================================================================================== ###

		progress.setText("Объединение поверхностей")
		# объединяем поверхности для каждого склада, если требуется
		topoWireframeData = []
		for i, stockItem in enumerate(stockItems, start = 1):
			wfdata = []
			for wfItem in filter(lambda it: it.isWireframe(), stockItem.topoDirItem().iterChildren()):
				wfdata.append([wfItem.parent().filePath(), wfItem.text(0)])
			#
			topoName = tmpTopoNameMask % str(i).zfill(3)
			if len(wfdata) > 1:
				try:					wireframeMerge(tmpTridbPath, topoName, wfdata)
				except Exception as e:	raise Exception("Невозможно объединить поверхности. %s" % str(e))
			else:
				try:					copy_wireframe(wfdata[0][0], wfdata[0][1], tmpTridbPath, topoName, False)
				except Exception as e:	raise Exception("Невозможно скопировать каркас поверхности. %s" % str(e))
			topoWireframeData.append([tmpTridbPath, topoName])

		### =================================================================================== ###
		### ==================================== РАСЧЕТ ОБЪЕМОВ =============================== ###
		### =================================================================================== ###

		#
		progress.setText("Создание файла отчета")
		reportStructure = ["ПАРАМЕТР:C:50:0", "ЗНАЧЕНИЕ:C:50:0"]
		
		progress.setText("Формирование списка контуров")
		for k, stockItem in enumerate(stockItems, start = 1):
			stockName = stockItem.name()
			topoName = tmpTopoNameMask % str(k).zfill(3)
			sectorNames = [("", str(stockItem.density()))] + [(sectorItem.name(), str(sectorItem.density())) for sectorItem in stockItem.sectorItems()]

			reportPath = reportPathMask % str(k).zfill(3)
			reportPathList.append(reportPath)
			MicromineFile.create(reportPath, reportStructure)
	
			fdst = MicromineFile()
			if not fdst.open(reportPath):
				raise Exception("Невозможно открыть файл '%s'" % reportPath)
			
			for i, (sectorName, sectorDensity) in enumerate(sectorNames):
				# запись в файл
				if i == 0:
					fdst.add_record()
					fdst.set_field_value(0, fdst.records_count, "Склад")
					fdst.set_field_value(1, fdst.records_count, stockName)

				if not (stockName, sectorName) in userKeys:
					continue

				# формирование фильтра
				dbFilter = MMpy.FormSet("DBFILTER","16.0.875.2")
				dataGrid = MMpy.DataGrid(4,1)
				dataGrid.set_column_info(0,1,4)
				dataGrid.set_column_info(1,2,6)
				dataGrid.set_column_info(2,3,-1)
				dataGrid.set_column_info(3,4,14)
				dataGrid.set_row(0, [stockField, "0", stockName, "0"])
				dataGrid.set_row(1, [sectorField, "0", sectorName, "0"])
				dbFilter.set_field("GRID", dataGrid.serialise())
				dbFilter.set_field("COMBINE_EQ","0")
				dbFilter.set_field("OR","0")
				dbFilter.set_field("AND","1")
				dbFilter.set_field("FTYPE","0")
				dbFilter.set_field("FILE", tmpStrPath)

				# запуск расчета объема
				try:					dtmVolume(tmpTridbPath, tmpGroundName, tmpTridbPath, topoName, tmpStrPath, dbFilter, tmpReportPath)
				except Exception as e:	pass#raise Exception("Невозможно рассчитать объем склада '%s'. %s" % (stockName, str(e)))

				# извлечение информации из отчета и запись в конечный файл
				fsrc = MicromineFile()
				if not os.path.exists(tmpReportPath):		raise Exception("Объем по складу '%s' не был рассчитан" % stockName)
				if not fsrc.open(tmpReportPath):			raise Exception("Невозможно открыть файл '%s'" % tmpReportPath)
				if fsrc.records_count != 8:					raise Exception("Отчет '%s' содержит неверное количество строк" % tmpReportPath)
				srcData = fsrc.read(asstr = True)
				fsrc.close()

				params = ["Сектор", "Площадь поверхности", "Объем", "Плотность", "Тоннаж"]
				values = (sectorName, srcData[7][1], srcData[3][2],	sectorDensity, str(round(float(sectorDensity)*float(srcData[3][2]),2)))
				for param, value in zip(params, values):
					fdst.add_record()
					fdst.set_field_value(0, fdst.records_count, param)
					fdst.set_field_value(1, fdst.records_count, value)
				#
				if i < len(sectorNames)-1:
					fdst.add_record()
			#
			fdst.close()

		### =================================================================================== ###
		### ================================== ЗАГРУЗКА ОБЪЕКТОВ ============================== ###
		### =================================================================================== ###

		progress.setText("Загрузка выбранных объектов")
		displayData = DisplayData(self.main_app)
		if any(self.selectedItems().values()):
			displayData.exec(cleartemp = False)

		progress.setText("Загрузка стрингов")
		for stringItem in stockStringItems:
			filepath = stringItem.filePath()
			try:					loadContour(filepath, "Склад|Стринги|%s" % os.path.basename(filepath))
			except Exception as e:	qtmsgbox.show_error(self.main_app, "Ошибка", "Не удалось загрузить стринг '%s'. Ошибка: %s" % (filepath, str(e)))

		progress.setText("Создание набора каркасов")
		wfset = create_wireframe_set([[row[0], DEFAULT_ATTR_NAME, row[1]] for row in topoWireframeData])

		progress.setText("Загрузка каркасов")
		try:						loadIsolineSet(wfset, "Склад|Изолинии", isoLineFormsetId)
		except Exception as e:		qtmsgbox.show_error(self.main_app, "Ошибка", "Не удалось загрузить каркасы съемки. %s" % str(e))


		### =================================================================================== ###
		### =================================== ВЫВОД НА ПЕЧАТЬ =============================== ###
		### =================================================================================== ###

		# создание директорий вывода
		progress.setText("Создание директорий вывода")
		if not os.path.exists(outputPlotDirectory):
			try:					os.mkdir(outputPlotDirectory)
			except Exception as e:	raise Exception("Невозможно создать директорию '%s'. %s" % (outputPlotDirectory, str(e)))
		outputFragmentsDirectory = os.path.join(outputPlotDirectory, "fragments")
		if not os.path.exists(outputFragmentsDirectory):
			try:					os.mkdir(outputFragmentsDirectory)
			except Exception as e:	raise Exception("Невозможно создать директорию '%s'. %s" % (outputFragmentsDirectory, str(e)))

		# копирование шаблона
		progress.setText("Копирование шаблона")
		sheetName = "Лист.PEX"
		pexpath = os.path.join(outputPlotDirectory, sheetName)
		shutil.copy(templatePath, pexpath)
		#
		fpex = open(pexpath, encoding = "utf8")
		xmlpex = fpex.read()
		fpex.close()
		
		# cоздание планов
		progress.setText("Создание планов")
		pelExtension = ".PEL"
		for i, (reportPath, (tridbPath, wireframeName)) in enumerate(zip(reportPathList, topoWireframeData), start = 1):
			# определение границ просмотра
			progress.setText("Создание планов. Определение границ просмотра")
			tridb = Tridb(tridbPath)
			wireframe = tridb[wireframeName]
			xmin, xmax = wireframe.default_attribute("XMinimum"), wireframe.default_attribute("XMaximum")
			ymin, ymax = wireframe.default_attribute("YMinimum"), wireframe.default_attribute("YMaximum")
			zmin, zmax = wireframe.default_attribute("ZMinimum"), wireframe.default_attribute("ZMaximum")
			tridb.close()

			# фокусировка
			msg = "Убедитесь, что в Визекс загружена информация, необходимая для плана"
			qtmsgbox.show_information(self.main_app, "Создание печати", msg)

			ptA = geom.Point(xmin, ymin, 0)
			ptB = geom.Point(xmax, ymax, 0)
			# Вид в плане
			display_limits(ptA, ptB, 0, zmin, zmax)
			MMpy.Command("#33523").run()

			#
			curPelNum = str(i).zfill(2)
			pelBodyMask = "bodypart_%s"
			pelTableNameMask = "tablepart_%s"
			pelFileName = pelBodyMask % curPelNum
			pelDataMask =  os.path.join(templateBodyRoot, pelBodyMask % curPelNum)
			pelTableMask =  os.path.join(templateBodyRoot, pelTableNameMask % curPelNum)
			
			# создание файла печати
			pelpath = os.path.join(outputFragmentsDirectory, pelFileName + pelExtension)
			self.create_pel(pelpath)
			# вычисление центра фрагмента
			pelxmin, pelymin, pelxmax, pelymax = get_pel_extents(pelpath)
			pelxcenter = 0.5 * (pelxmin + pelxmax)
			pelycenter = 0.5 * (pelymin + pelymax)

			# копирование отчета
			tablePath = os.path.join(outputFragmentsDirectory, pelTableNameMask % curPelNum)
			shutil.copy(reportPath, tablePath + '.DAT')

			# замена координат центра
			xmlpex = xmlpex.replace(planMask + "1%s" % curPelNum, "%.3f" % pelxcenter)
			xmlpex = xmlpex.replace(planMask + "2%s" % curPelNum, "%.3f" % pelycenter)
			# замена масштаба
			xmlpex = xmlpex.replace(scaleMask, scale)
			# замена ссылок на фрагменты
			xmlpex = xmlpex.replace(">%s<" % (pelDataMask + pelExtension), ">%s<" % pelpath)
			xmlpex = xmlpex.replace(">%s<" % pelDataMask, ">%s<" % pelpath)
			# замена ссылок на таблицы
			xmlpex = xmlpex.replace(">%s<" % (pelTableMask + pelExtension), ">%s<" % tablePath)
			xmlpex = xmlpex.replace(">%s<" % pelTableMask, ">%s<" % tablePath)

		# запись в файл
		fpex = open(pexpath, "w", encoding = "utf8")
		fpex.write(xmlpex)
		fpex.close()

		qtmsgbox.show_information(self.main_app, "Выполнено", "Склады успешно рассчитаны")