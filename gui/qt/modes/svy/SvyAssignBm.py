try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.ConcreteByMineDialog import ConcreteByMineDialog, AssignedDataDialog, AssignedAllDataDialog
from gui.qt.dialogs.BlockModelPreviewDialog import BlockModelPreviewDialog, bmDataPreviewReport, createBmPreviewListTreeItem
from gui.qt.ProcessWindow import ProcessWindow
from gui.qt.modes.WireframeAttributeValidation import SvyWireframeAttributeValidation

from mm.mmutils import Tridb, MicromineFile, SvyWireframeFormSet
from mm.formsets import create_wireframe_set, loadTriangulationSet

from utils.nnutil import prev_date, date_to_fieldname
from utils.blockmodel import BlockModel
from utils.constants import *
from utils.osutil import get_file_owner, get_temporary_prefix, get_current_time
from utils.validation import is_blank

from datetime import datetime
from itertools import groupby
import os, math, pyodbc, re
from copy import deepcopy
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def metal_percision(descr):
	metal_weight_percisions = {'NI' : 1,
								'CU' : 1,
								'CO' : 2,
								'TE' : 2,
								'SE' : 2,
								'S[" ","("]': 0,
								'PT' : 2,
								'PD' : 2,
								'RH' : 2,
								'OS' : 2,
								'IR' : 2,
								'RU' : 2,
								'AU' : 2,
								'AG' : 2
							}
	metal_percentage_percisions={ 'NI' : 2,
								'CU' : 2,
								'CO' : 3,
								'TE' : 4,
								'SE' : 4,
								'S[" ","("]' : 2,
								'PT' : 2,
								'PD' : 2,
								'RH' : 2,
								'OS' : 2,
								'IR' : 2,
								'RU' : 2,
								'AU' : 2,
								'AG' : 2
							}
	up_descr = descr.upper()
	metal_dict_percisions = {}
	if re.search('\(КГ\)', up_descr) or re.search('\(Т\)', up_descr):
		metal_dict_percisions = metal_weight_percisions
	else:
		metal_dict_percisions = metal_percentage_percisions
	percision = None
	for item in metal_dict_percisions:
		if  re.search(item, up_descr):
			percision =	metal_dict_percisions[item]
			break
	return percision


		 
		 
	     	 	 	 		 	 	 	 


class AssignBm(ProcessWindow):
	def __init__(self, *args, **kw):
		super(AssignBm, self).__init__(*args, **kw)
		self.__createVariables()
	def __createVariables(self):
		self._netBmPathList = []			# список сетевых блочных моделей
		self._locBmPathList = []			# список локальных блочных моделей
		self._locBmExtractedPathList = []	# список извлеченных блочных моделей

		self._wireframe_sets = []			# список использованных наборов каркасов
		self._bmPreviewDialog = None		# диалог для выбора данных в блочной модели
		self._bmCurrentDate = None			# текущее состояние блочных моделей

	def addBlockModelsData(self, bmPathList):
		bmData, bmHeader = [], []
		for bmPath in bmPathList:
			bmFile = MicromineFile()
			if not bmFile.open(bmPath):
				raise Exception("Невозможно открыть файл '%s'" % bmPath)
			data = bmFile.read()
			header = bmFile.header
			if not bmHeader:			bmHeader = deepcopy(header)
			elif bmHeader != header:	raise Exception("Невозможно объединить блочные модели с разными заголовками")
			bmData.extend(data)
			bmFile.close()
		return bmData, bmHeader
	# отчет
	def bmDataReport(self, bmData, bmHeader, groupFields, densityField = None, elemFields = [], elemUnits = [], elemTonnageFlag = True):
		progress = self.main_app.mainWidget.progressBar

		### =================================================================================== ###
		### ====================================== ПРОВЕРКА =================================== ###
		### =================================================================================== ###

		if len(elemFields) != len(elemUnits):
			raise Exception("Указаны разное количество элементов и единиц измерений")
		# процент: elemUnit = 0
		# г/т: elemUnit = 1
		coeff = None
		elemCoeffDict = {0: 100, 1: 1000}
		elemGradeLabel = {0: "%", 1: "г/т"}
		elemGradeTonnage = {0: "т", 1: "кг"}
		#
		elemCoeffs = [elemCoeffDict.get(elemUnit) for elemUnit in elemUnits]
		elemCoeffUnknown = [elemCoeff is not None for elemCoeff in elemCoeffs]
		if not all(elemCoeffs):
			raise Exception("Неизвестная единица измерения '%s'" % elemFields[elemCoeffUnknown.index(False)])
		#
		blockFldX, blockFldY, blockFldZ = "_%s" % ID_EAST, "_%s" % ID_NORTH, "_%s" % ID_RL
		idxBlockFldX, idxBlockFldY, idxBlockFldZ  = bmHeader.index(blockFldX), bmHeader.index(blockFldY), bmHeader.index(blockFldZ)

		# индекс поля плотности
		idxDensity = bmHeader.index(densityField) if densityField in bmHeader else None

		# проверка на наличие полей
		allFields = groupFields + elemFields
		allFieldsInHeader = list(map(lambda field: field in bmHeader, allFields))
		if not allFieldsInHeader:
			raise Exception("Поле '%s' отсутствует в блочной модели" % allFields[allFieldsInHeader.index(False)])

		### =================================================================================== ###
		### ================================= СОЗДАНИЕ ОТЧЕТА ================================= ###
		### =================================================================================== ###

		# индексы полей
		groupIndexes = [bmHeader.index(field) for field in groupFields]
		elemIndexes = [bmHeader.index(field) for field in elemFields]
		#
		# расчет объемов/тоннажа
		progress.setText("Создание отчета по блочной модели")
		dictionary = {}
		for row in bmData:
			density = 0 if idxDensity is None or math.isnan(row[idxDensity]) else row[idxDensity] 
			volume = row[idxBlockFldX]*row[idxBlockFldY]*row[idxBlockFldZ]
			tonnage = density*volume
			elemTonnage = [tonnage*row[elemIndex] / elemCoeffs[i] if not math.isnan(row[elemIndex]) else 0 for i, elemIndex in enumerate(elemIndexes)]
			#
			key = tuple(row[index] for index in groupIndexes)
			if not key in dictionary:
				dictionary[key] = [volume, tonnage] + elemTonnage
			else:
				for iElem, value in enumerate([volume, tonnage] + elemTonnage):
					dictionary[key][iElem] += value
		#
		# переход к содержаниям
		for key, values in dictionary.items():
			volume, tonnage, elemTonnages = values[0], values[1], values[2:]
			density = tonnage / volume if volume != 0 else 0
			#
			if tonnage == 0:		elemGrades = tuple(0 for i, elemTonnage in enumerate(elemTonnages))
			else:					elemGrades = tuple(elemCoeffs[i]*elemTonnage / tonnage for i, elemTonnage in enumerate(elemTonnages))
			dictionary[key] = (volume, tonnage, density)
			if not elemTonnageFlag:
				dictionary[key] += elemGrades
			else:
				for elemGrade, elemTonnage in zip(elemGrades, elemTonnages):
					dictionary[key] += (elemGrade, elemTonnage)
		#
		reportData = [key + tuple(dictionary[key]) for key in sorted(dictionary)]
		reportHeader = tuple(groupFields) + (RPTFLD_VOLUME, RPTFLD_FACT_TONNAGE, RPTFLD_FACT_DENSITY)
		if not elemTonnageFlag:
			for elemUnit, elemField in zip(elemUnits, elemFields):
				gradeLabel = "{0}, {1}".format(elemField, elemGradeLabel[elemUnit])
				reportHeader += (gradeLabel,)
		else:
			for elemUnit, elemField in zip(elemUnits, elemFields):
				gradeLabel = "{0} ({1})".format(elemField, elemGradeLabel[elemUnit])
				tonnageLabel = "{0} ({1})".format(elemField, elemGradeTonnage[elemUnit])
				reportHeader += (gradeLabel, tonnageLabel)
		reportData = list(map(list, reportData))
		reportHeader = list(reportHeader)
		return reportData, reportHeader
	def bmFileReport(self, bmPath, groupFields, densityField = None, elemFields = [], elemUnits = [], elemTonnageFlag = True):
		progress = self.main_app.mainWidget.progressBar
		#
		bmFile = MicromineFile()
		if not bmFile.open(bmPath):
			raise Exception("Невозможно открыть файл '%s'" % bmPath)
		bmData = bmFile.read()
		bmHeader = bmFile.header
		bmFile.close()
		#
		try:
			reportData, reportHeader = self.bmDataReport(bmData, bmHeader, groupFields, densityField, elemFields, elemUnits, elemTonnageFlag)
		except Exception as e:
			raise Exception("Невозможно создать отчет по блочной модели. %s" % str(e))
		#
		return reportData, reportHeader

	def copyBlockModelsToServer(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()

		# копирование
		for locBmPath, netBmPath in zip(self._locBmPathList, self._netBmPathList):
			if not os.path.exists(locBmPath):
				continue
			#
			netBm = BlockModel(netBmPath, self.main_app)
			bkpBmPath = netBm.backup_path
			# создание резервной копии
			progress.setText("Создание резервной копии блочной модели %s" % netBm.name)
			try:
				self.copy_file(netBmPath, bkpBmPath)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно создать резервную копию файла %s. %s" % (netBmPath, str(e)))
				return False
			# копирование
			progress.setText("Копирование блочной модели %s" % netBm.name)
			try:
				self.copy_file(locBmPath, netBmPath)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно скопировать файл '%s'. %s" % (os.path.basename(locBmPath), str(e)))
				return False
		# обновление статуса в БД
		for locBmPath, netBmPath in zip(self._locBmPathList, self._netBmPathList):
			if not os.path.exists(locBmPath):
				continue
			netBm = BlockModel(netBmPath, self.main_app)
			netBm.metadata.trigger_update()
			netBm.save()
		progress.setText(oldProgressText)
		return True

	def displayData(self):
		raise NotImplementedError("Необходимо определить методы загрузки данных")
	def loadToVizex(self):
		# Переопределение методы необходимо, чтобы задать верные значения
		# аргументам restoreFields, dataTransfer
		# prepareBlockModels (extracted = True, restoreFields, dataTransfer)
		# displayData
		raise NotImplementedError("Необходимо определить метод подготовки данных перед загрузкой")

	def extractBlockModels(self):
		progress = self.main_app.mainWidget.progressBar
		#
		(includeValues, restoreMiningValues, restoreGeologyValues, simulateConcreteValues), includeFields = self.selectedKeysAndFields()

		self._locBmExtractedPathList = []
		for i, (netBmPath, locBmPath) in enumerate(zip(self._netBmPathList, self._locBmPathList)):
			netBm = BlockModel(netBmPath, self.main_app)
			#
			locBmExtractedPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_PART%s.DAT" % (netBm.name, str(i+1).zfill(3)))
			self._locBmExtractedPathList.append(locBmExtractedPath)
			# извлечение данных
			progress.setText("%s. Извлечение данных" % netBm.name)
			try:
				self.extractBlockModelData(locBmPath, includeFields, includeValues, locBmExtractedPath)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно извлечь данные из блочной модели. %s" % str(e))
				return False
		return True
	def prepareBlockModels(self, extracted, restoreFields, dataTransfer):
		progress = self.main_app.mainWidget.progressBar
		progress.setText("Обработка выбранных данных")
		# извлечение данных
		if extracted and not self.extractBlockModels():
			return False
		# восстановление полей
		if restoreFields and not self.restoreFields(extracted):
			return False
		# перенос данных
		if dataTransfer and not self.transferBlockModelData(extracted):
			return False
		return True
	def removeBlockModelSystemFields(self, extracted, excludeFields = []):
		progress = self.main_app.mainWidget.progressBar
		progress.setText("Удаление системных полей")
		locBmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		#
		for locBmPath, netBmPath in zip(locBmPathList, self._netBmPathList):
			if not os.path.exists(locBmPath):
				continue
			#
			netBmFile = MicromineFile()
			if not netBmFile.open(netBmPath):
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно открыть файл %s" % netBmPath)
				return False
			netBmHeader = netBmFile.header
			netBmFile.close()
			#
			locBmFile = MicromineFile()
			if not locBmFile.open(locBmPath):
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно открыть файл %s" % locBmPath)
				return False
			locBmHeader = locBmFile.header
			locBmFile.close()
			#
			systemFields = [field for field in locBmHeader if not field in netBmHeader + excludeFields]
			try:
				self.removeFieldsFromBlockModel(locBmPath, systemFields)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно удалить системные поля. %s" % str(e))
				return False
		#
		return True

	def restoreFields(self, extracted, restorePrevDate = False):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		locBmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		#
		(includeValues, restoreMiningValues, restoreGeologyValues, simulateConcreteValues), includeFields = self.selectedKeysAndFields()

		for netBmPath, locBmPath in zip(self._netBmPathList, locBmPathList):
			netBm = BlockModel(netBmPath, self.main_app)
			BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)

			progress.setText("%s. Определение индексов выбранных записей" % netBm.name)
			try:
				includeIndexes = self.getRowIndexesByKeys(locBmPath, includeFields, includeValues)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось определить индексы строк для ключевых записей. %s" % str(e))
				return False
			# восстановление выемочных работ
			if len(restoreMiningValues) > 0:
				progress.setText("%s. Определение индексов для восстановления выемочных работ" % netBm.name)
				try:
					restoreMiningIndexes = self.getRowIndexesByKeys(locBmPath, includeFields, restoreMiningValues)
				except Exception as e:
					qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось определить индексы строк для ключевых записей. %s" % str(e))
					return False
				#
				progress.setText("%s. Восстановление выемки" % netBm.name)
				try:
					self.restoreMiningWorks(locBmPath, restoreMiningIndexes, BMFLD_CURRDATE, includeIndexes, restorePrevDate)
				except Exception as e:
					qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось восстановить выемочные работы. %s" % str(e))
					return False
			# восстановление геологии
			if len(restoreGeologyValues) > 0:
				progress.setText("%s. Определение индексов для восстановления геологии" % netBm.name)
				try:
					restoreGeologyIndexes = self.getRowIndexesByKeys(locBmPath, includeFields, restoreGeologyValues)
				except Exception as e:
					qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось определить индексы строк для ключевых записей. %s" % str(e))
					return False
				#
				progress.setText("%s. Восстановление геологии" % netBm.name)
				try:
					self.restoreGeology(locBmPath, restoreGeologyIndexes, BMFLD_CURRDATE, includeIndexes)
				except Exception as e:
					qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось восстановить геологию. %s" % str(e))
					return False
			# проектный бетон
			if len(simulateConcreteValues) > 0:
				progress.setText("%s. Определение индексов для эммуляции проектного бетона" % netBm.name)
				try:
					simulateConcreteIndexes = self.getRowIndexesByKeys(locBmPath, includeFields, simulateConcreteValues)
				except Exception as e:
					qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось определить индексы строк для ключевых записей. %s" % str(e))
					return False
				#
				progress.setText("%s. Эммуляция проектного бетона" % netBm.name)
				try:
					self.simulateConcrete(locBmPath, simulateConcreteIndexes, BMFLD_CURRDATE, includeIndexes)
				except Exception as e:
					qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось съэмулировать проектный бетон. %s" % str(e))
					return False
		progress.setText(oldProgressText)
		return True

	def selectedKeys(self):
		includeValues = self._bmPreviewDialog.includedItemKeys()
		restoreMiningValues = self._bmPreviewDialog.restoredMiningWorkItemKeys()
		restoreGeologyValues = self._bmPreviewDialog.restoredGeologyItemKeys()
		simulateConcreteValues = self._bmPreviewDialog.simulateConcreteItemKeys()
		return includeValues, restoreMiningValues, restoreGeologyValues, simulateConcreteValues
	def selectedKeysAndFields(self):
		selectedKeys = self.selectedKeys()
		# последовательность полей важна
		selectedFields = self._bmPreviewDialog.groupFields() + self._bmPreviewDialog.bmPreviewStacked.widget(0).blockModelView.mineWorkGroupFields()
		return selectedKeys, selectedFields

	#
	def transferBlockModelData(self, extracted):
		# правило переноса данных, как правило, для всех режимов разное
		# (маркшейдерия, проектные отчеты или отсутствие переноса вообще)
		raise NotImplementedError("Необходимо определить метод переноса данных")
	

		
class SvyAssignBm(AssignBm):
	def __init__(self, *args, **kw):
		super(SvyAssignBm, self).__init__(*args, **kw)
		self.__create_variables()
	def __create_variables(self):
		self._is_report = False				# отчет (при создании наборов учитывается оперативный учет)
		self._is_monthly_report = False		# отчет по месяцу (системные поля переносятся)
		self._is_bymine_report = False		# отчет по выработке (системные поля не переносятся)
		self._is_bydate_report = False		# отчет по дате (нет системных полей)
		self._is_custom_report = False		# отчет по произвольному каркасу (системные поля не переносятся)

	def isMonthlyReport(self):				return self._is_report and self._is_monthly_report and not self._is_bydate_report and not self._is_custom_report and not self._is_bymine_report
	def isByMineReport(self):				return self._is_report and not self._is_monthly_report and not self._is_bydate_report and not self._is_custom_report and self._is_bymine_report
	def isCustomReport(self):				return self._is_report and not self._is_monthly_report and not self._is_bydate_report and self._is_custom_report and not self._is_bymine_report
	def isByDateReport(self):				return self._is_report and not self._is_monthly_report and self._is_bydate_report and not self._is_custom_report and not self._is_bymine_report
	def isUpdate(self):						return not self._is_report

	#
	def loadToVizex(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()

		if self._bmPreviewDialog.paramsFrame.isVisible():
			monthlySelected = self._bmPreviewDialog.paramsFrame.radioButtonMonthly.isChecked()
			self._is_monthly_report = monthlySelected
			self._is_bymine_report = not monthlySelected

		extracted = True
		restoreFields = True
		dataTransfer = not self.isCustomReport()
		# перенос данных из системных полей
		if not self.prepareBlockModels(extracted = extracted, restoreFields = restoreFields, dataTransfer = dataTransfer):
			progress.setText(oldProgressText)
			return False
		# удаление системных полей
		if not self.removeBlockModelSystemFields(extracted):
			progress.setText(oldProgressText)
			return False
		# загрузка
		try:
			self.displayData(extracted = extracted)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно загрузить данные. %s" % str(e))
			progress.setText(oldProgressText)
			return
		progress.setText(oldProgressText)
		return True

	#
	def transferBlockModelData(self, extracted):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		locBmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		#
		(includeValues, restoreMiningValues, restoreGeologyValues, simulateConcreteValues), includeFields = self.selectedKeysAndFields()
		for netBmPath, locBmPath in zip(self._netBmPathList, locBmPathList):
			netBm = BlockModel(netBmPath, self.main_app)
			BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)
			#
			progress.setText("%s. Определение индексов для переноса данных" % netBm.name)
			try:
				includeIndexes = self.getRowIndexesByKeys(locBmPath, includeFields, includeValues)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось определить индексы строк для ключевых записей. %s" % str(e))
				return False
			# перенос данных
			progress.setText("%s. Перенос данных" % netBm.name)
			try:
				onlyMaterial = self.isByMineReport()
				self.copySvyFieldValues(locBmPath, BMFLD_CURRDATE, includeIndexes, onlyMaterial, clearSource = False)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось скопировать данные из системных полей. %s" % str(e))
				return False
		#
		progress.setText(oldProgressText)
		return True
	#
	def displayData(self, extracted = False):
		progress = self.main_app.mainWidget.progressBar
		progress.setText("Загрузка объектов")
		#
		userFormsets = list(zip(*self.main_app.settings.getValue(ITEM_CATEGORY_FORMSET_DICT[ITEM_CATEGORY_SVY])))[1]
		userFormsets = list([int(setId) if setId else None for setId in userFormsets])
		formsetsDict = dict(zip(MMTYPE_LIST, userFormsets))
		#
		locBmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		# загрузка блочной модели
		setId = formsetsDict[MMTYPE_BM]
		for i, (netBmPath, locBmPath) in enumerate(zip(self._netBmPathList, locBmPathList)):
			if not os.path.exists(locBmPath):
				continue
			netBm = BlockModel(netBmPath, self.main_app)
			locBm = BlockModel(locBmPath, self.main_app)
			locBm.load(netBm.name, setId)
		# загрузка каркасов
		setId = formsetsDict[MMTYPE_TRIDB]
		if not self.isUpdate():
			wfset = self._wireframe_sets[-1]
			loadTriangulationSet(wfset, wfset.displayName(), setId)
		else:
			for wfset in self._wireframe_sets:
				loadTriangulationSet(wfset, wfset.displayName(), setId)	

	def prepareReport(self, setBackfillGradesToNull = True):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		if not self.isCustomReport():
			monthlySelected = self._bmPreviewDialog.paramsFrame.radioButtonMonthly.isChecked()
			self._is_monthly_report = monthlySelected
			self._is_bymine_report = not monthlySelected
		#
		extracted = True
		#
		if self.isMonthlyReport() or self.isByMineReport():
			if not self.prepareBlockModels(extracted = extracted, restoreFields = True, dataTransfer = True):
				progress.setText(oldProgressText)
				return None, None
		elif self.isCustomReport():
			if not self.prepareBlockModels(extracted = extracted, restoreFields = True, dataTransfer = False):
				progress.setText(oldProgressText)
				return None, None
		#
		BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)
		#
		bmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		bmData, bmHeader = self.addBlockModelsData(bmPathList)
		# Такой порядок полей важен для ПЛАН-ЗАМЕРА
		# ДАТА, ЗАЛЕЖЬ, БЛОК, ПАНЕЛЬ, ЛЕНТА, КАРКАС, ВЫРАБОТКА, ИНТЕРВАЛ, КАТЕГОРИЯ, РАБОЫ, МАТЕРИАЛ, ИНДЕКС, ОБЪЕМ, ТОННАЖ, ПЛОТНОСТЬ, МЕТАЛЛЫ 
		groupFields = (
				BMFLD_FACTDATE,
				BMFLD_LAYER, BMFLD_SVYBLOCK, self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper(), self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper(),
				BMFLD_SYS_MINENAME, BMFLD_SYS_INTERVAL,
				BMFLD_MINENAME, BMFLD_INTERVAL,
				BMFLD_CURRDATE,
				BMFLD_FACTWORK,
				BMFLD_MATERIAL, BMFLD_INDEX
			)

		elemFields = self.getIdwElements()
		elemUnits = self.getIdwUnits()
		try:
			reportData, reportHeader = self.bmDataReport(bmData, bmHeader, groupFields, BMFLD_DENSITY, elemFields, elemUnits, elemTonnageFlag = True)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно создать отчет по блочной модели. %s" % str(e))
			return None, None
		# переименование полей SYS_ВЫРАБОТКА
		if BMFLD_SYS_MINENAME in reportHeader:
			idxSysMineName = reportHeader.index(BMFLD_SYS_MINENAME)
			reportHeader[idxSysMineName] = "КАРКАС"
			if BMFLD_SYS_INTERVAL in reportHeader:
				idxSysInterval = reportHeader.index(BMFLD_SYS_INTERVAL)
				for irow, row in enumerate(reportData):
					reportData[irow][idxSysMineName] = ":".join([row[idxSysMineName], row[idxSysInterval]])
					reportData[irow] = row[:idxSysInterval] + row[idxSysInterval+1:]
				reportHeader = reportHeader[:idxSysInterval] + reportHeader[idxSysInterval+1:]
				del idxSysInterval
			del idxSysMineName
		#
		if BMFLD_CURRDATE in reportHeader:
			idxCurrDate = reportHeader.index(BMFLD_CURRDATE)
			reportHeader[idxCurrDate] = SVY_ATTR_CATEGORY
		# обнуление содержаний для закладочных работ
		if setBackfillGradesToNull:
			progress.setText("Обнуление содержаний для закладочных работ")
			columnCount = len(reportHeader)
			idxDensity = reportHeader.index(RPTFLD_FACT_DENSITY)
			idxMaterial = reportHeader.index(BMFLD_MATERIAL)
			for rowIndex, row in enumerate(reportData):
				material = row[idxMaterial]
				if material == self.getBackfillCategory():
					for columnIndex in range(idxDensity+1, columnCount):
						reportData[rowIndex][columnIndex] = 0
		progress.setText(oldProgressText)
		return reportData, reportHeader

	def _saveToDb(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		mineSiteId = self._bmPreviewDialog.mineSiteId()
		if mineSiteId is None:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Выберите участок")
			return False
		# считывание текущей даты блочных моделей
		netBm = BlockModel(self._netBmPathList[0], self.main_app)
		bmDate = netBm.blockmodel_date
		#
		reportPath = os.path.join(self._tmpfolder, get_temporary_prefix() +  "CSV_REPORT.csv")
		try:
			reportFile = open(reportPath, "w")
			reportFile.close()
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно создать отчет. Возможно файл используется")
			return False
		# подготовка отчета
		try:
			reportData, reportHeader = self.prepareReport()
			if reportData is None:
				return False			
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", str(e))
			return False
		#
		# экспорт во временный CSV файл
		with open(reportPath, "w") as reportFile:
			reportFile.write(",".join(map(str, reportHeader)) + "\n")
			for row in reportData:
				reportFile.write(",".join(map(str, row)) + "\n")
		#
		progress.setText("Запись отчета в базу данных")
		# считывание файла отчета в бинарном виде
		with open(reportPath, "rb") as reportFile:
			bindata = reportFile.read()
		#
		dataSource = "N-MINE"
		try:
			# определяем версию отчета за текущий месяц
			cmd = """
				SELECT CASE WHEN RPTVERSION IS NULL THEN 1 ELSE RPTVERSION+1 END RPTVERSION
				FROM
				(
					SELECT MAX(CONVERT(INT, SUBSTRING(REPORT_VERSION, 2, LEN(REPORT_VERSION)))) RPTVERSION
					FROM [{0}].[dbo].[MONTHLY_REPORT_DATA]
					WHERE YEAR(REPORT_DATE) = {1} AND MONTH(REPORT_DATE) = {2} AND DepartId = {3}
				) T
			""".format(self.main_app.srvcnxn.dbsvy, bmDate.year, bmDate.month, mineSiteId)
			# 
			newVer = "v%d" % self.main_app.srvcnxn.exec_query(cmd).get_column("RPTVERSION")[0]
			dataGroup = 3 if self.isByMineReport() else 1
			cmd = """
				INSERT INTO [{0}].[dbo].[MONTHLY_REPORT_DATA] (DepartID, REPORT_DATE, REPORT_VERSION, REPORT_FILE, LOADED, DATA_GROUP, DATA_SOURCE)
				VALUES (?, ?, ?, ?, ?, ?, ?)
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (mineSiteId, get_current_time(), newVer, pyodbc.Binary(bindata), 0, dataGroup, dataSource)
			#
			self.main_app.srvcnxn.exec_query(cmd, params)
			self.main_app.srvcnxn.commit()
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Необработанная ошибка", str(e))
			raise
			return False
		#
		progress.setText(oldProgressText)
		qtmsgbox.show_information(self._bmPreviewDialog, "Выполнено", "Отчет успешно сохранен в базу данных")
		#
		return True
	def saveToDb(self):
		self._bmPreviewDialog.buttonSave.setDisabled(True)
		ret = self._saveToDb()
		self._bmPreviewDialog.buttonSave.setDisabled(False)
		return ret

	def exportToExcel(self):
		try:
			import xlsxwriter as xl
			from xlsxwriter.utility import xl_range, xl_rowcol_to_cell, xl_col_to_name
			#
			filetypes = "EXCEL (*.XLSX)"
			try:			initialdir = MMpy.Project.path()
			except:			initialdir = os.getcwd()
			reportPath = QtGui.QFileDialog.getSaveFileName(self._bmPreviewDialog, "Выберите файл", initialdir, filetypes).replace("/", "\\")
			if not reportPath:
				return False
			#
			# подготовка отчета
			try:
				reportData, reportHeader = self.prepareReport()
				if reportData is None:
					return False			
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", str(e))
				return False

			rpt_cat_indexes = self.main_app.settings.getValue(ID_RPT_GEO_SVY_INDEXES)
			rptCategoryDict = {}
			for item in rpt_cat_indexes:
				index_data_list = list(zip(*item[2]['INDEX']))
				if len(index_data_list) > 0:
					indexes = index_data_list[0]
				else:
					indexes = []
				rptCategoryDict[item[0]] = indexes

			rptCategories =  list(list(zip(*rpt_cat_indexes))[0])

			backfillCategory = self.getBackfillPurpose(), self.getBackfillCategory()
			#
			month_names = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

			rptCategoryDictExtended = {}
			for catname, indexes in rptCategoryDict.items():
				for index in indexes:
					rptCategoryDictExtended[index] = catname


			wb = xl.Workbook(reportPath)
			svyWorksheet = wb.add_worksheet()
			svyWorksheet.name = "Маркшейдерия"
			svyWorksheet_2 = wb.add_worksheet()
			svyWorksheet_2.name = "Маркшейдерия_2"
			geoWorksheet = wb.add_worksheet()
			geoWorksheet.name = "Полный отчет"

			# геология
			fmtGeoHeader = wb.add_format({"border": 1, "valign": "vcenter", "align": "center", "size": 11, "bold": True})
			fmtGeoBody = wb.add_format({"border": 1, "valign": "vcenter", "align": "center", "size": 11})
			# заголовок
			column_percisions = {}
			for icol, value in enumerate(reportHeader):
				geoWorksheet.write(0, icol, value, fmtGeoHeader)
				column_percisions[icol] = metal_percision(value)
			# данные
			for irow, row in enumerate(reportData, start = 1):
				for icol, value in enumerate(row):
					if not column_percisions[icol] is None:
						try:
							present_value = float(value)
							present_value = round(present_value, column_percisions[icol])
						except:
							present_value = value
					else:
						present_value = value 


					geoWorksheet.write(irow, icol, present_value, fmtGeoBody)

			
			#region	 маркшейдерия
			fmtSvyReportHeader = wb.add_format({"font": "ISOCPEUR", "size": 13, "italic": True, "align": "center", "valign": "vcenter"})
			fmtSvyTableHeader = wb.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "text_wrap": True, "border": 1})
			fmtSvyTableBody = wb.add_format({"font": "ISOCPEUR", "size": 10, "align": "right", "valign": "vcenter", "border": 1})
			fmtSvyBoldCenter = wb.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "bold": True, "border": 1})
			fmtSvyBoldRight = wb.add_format({"font": "ISOCPEUR", "size": 10, "align": "right", "valign": "vcenter", "bold": True, "border": 1})
			fmtSvyBorder = wb.add_format({"font": "ISOCPEUR", "size": 10, "border": 1})
			#
			catNumber = len(rptCategories)
			rptColNumber = catNumber*2 + 5

			svyWorksheet.set_column("A:A", 20)
			for i, icol in enumerate(range(66, 66+rptColNumber)):
				if i in [5, 14]:	svyWorksheet.set_column("%s:%s" % (chr(icol), chr(icol)), 11.3)
				else:				svyWorksheet.set_column("%s:%s" % (chr(icol), chr(icol)), 9)

			svyWorksheet.merge_range(xl_range(0, 0, 0, rptColNumber-1), "Замер горных работ", fmtSvyReportHeader)
			svyWorksheet.merge_range(xl_range(1, 0, 1, rptColNumber-1), "%s %d" % (month_names[self._bmCurrentDate.month-1], self._bmCurrentDate.year), fmtSvyReportHeader)
			svyWorksheet.merge_range("A4:A6", "Наименование", fmtSvyBoldCenter)
			svyWorksheet.merge_range("B4:B6", "Категория", fmtSvyTableHeader)
			svyWorksheet.merge_range("C4:C6", "Вид работ", fmtSvyTableHeader)
			svyWorksheet.merge_range(xl_range(3, 3, 3, 3+catNumber), "ОБЪЕМ (КУБ. М)", fmtSvyTableHeader)
			svyWorksheet.merge_range(xl_range(3, 4+catNumber, 3, 4+2*catNumber), "МАССА (Т)", fmtSvyTableHeader)

			tblHeader = rptCategories + ["Итого"] + rptCategories + ["Итого"]
			for icol, colname in enumerate(tblHeader, start = 3):
				svyWorksheet.merge_range(xl_range(4, icol, 5, icol), colname, fmtSvyTableHeader)

			# объединяем поля ВЫРАБОТКА и ИНТЕРВАЛ
			idxMineName = reportHeader.index(BMFLD_MINENAME)
			idxInterval = reportHeader.index(BMFLD_INTERVAL)
			reportData_mine_interval_merged = deepcopy(reportData)
			for i, row in enumerate(reportData):
				reportData_mine_interval_merged[i][idxMineName] = ":".join([row[idxMineName], row[idxInterval]])
			del idxMineName, idxInterval
			# удаление ненужных столбцов
			lastIndex = reportHeader.index(RPTFLD_FACT_DENSITY)
			reportCutHeader = reportHeader[:lastIndex]
			removeColumns = [BMFLD_FACTDATE, BMFLD_LAYER, self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper(), BMFLD_SVYBLOCK, self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper(), BMFLD_INTERVAL, BMFLD_MATERIAL]
			removeIndexes = sorted([reportCutHeader.index(column) for column in removeColumns], reverse = True)
			for index in removeIndexes:
				reportCutHeader.remove(reportCutHeader[index])
			for i, row in enumerate(reportData_mine_interval_merged):
				reportData_mine_interval_merged[i] = reportData_mine_interval_merged[i][:lastIndex]
				for index in removeIndexes:
					reportData_mine_interval_merged[i] = reportData_mine_interval_merged[i][:index] + reportData_mine_interval_merged[i][index+1:]
			# удаление строк, соответствующих каркасам закладки
			idxCategory = reportCutHeader.index(SVY_ATTR_CATEGORY)
			if not self.isCustomReport() and not self.isByDateReport():
				reportData_mine_interval_merged = list(filter(lambda x: x[idxCategory] != backfillCategory, reportData_mine_interval_merged))

			# проверка индексов
			idxIndex = reportCutHeader.index(BMFLD_INDEX)
			for i, row in enumerate(reportData_mine_interval_merged):
				if not row[idxIndex] in rptCategoryDictExtended:
					raise ValueError("'%s' - неизвестный индекс" % row[idxIndex])
				reportData_mine_interval_merged[i][idxIndex] = rptCategoryDictExtended[row[idxIndex]]

			reportData_mine_interval_merged.sort(key = lambda x: (x[0], x[1], x[2], x[3], x[4]))
			if self.isByMineReport() or self.isCustomReport() or self.isByDateReport():
				majorGroups = ["КАРКАС"]
				minorGroups = [BMFLD_MINENAME, SVY_ATTR_CATEGORY, BMFLD_FACTWORK]
				majorGroupIndexes = [reportCutHeader.index(column) for column in majorGroups]
				minorGroupIndexes = [reportCutHeader.index(column) for column in minorGroups]
				#
				currow = 6
				filledCells = []
				for majorGroup, majorGroupValues in groupby(reportData_mine_interval_merged, key = lambda x: tuple(x[k] for k in majorGroupIndexes)):
					fromrow = currow
					for minorGroup, minorGroupValues in groupby(majorGroupValues, key = lambda x: tuple(x[k] for k in minorGroupIndexes)):
						minorGroup = tuple(minorGroup)
						#
						for icol, item in enumerate(minorGroup):
							if icol == 0:	svyWorksheet.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyBoldRight)
							else:			svyWorksheet.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
							filledCells.append((currow, icol))
						
						categories = []
						volumeCategories, tonnageCategories = {}, {}
						for row in minorGroupValues:
							category, volume, tonnage = row[-3:]
							if not category in categories:			categories.append(category)
							if not category in volumeCategories:	volumeCategories[category] = 0
							if not category in tonnageCategories:	tonnageCategories[category] = 0
							#
							volumeCategories[category] += row[-2]
							tonnageCategories[category] += row[-1]
						#
						for category in categories:
							idxCategory = rptCategories.index(category)
							item = volumeCategories[category]
							svyWorksheet.write(currow, len(minorGroup) + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
							filledCells.append((currow, len(minorGroup) + idxCategory))
							item = tonnageCategories[category]
							svyWorksheet.write(currow, len(minorGroup) + catNumber + 1 + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
							filledCells.append((currow, len(minorGroup) + catNumber + 1 + idxCategory))

							letter_from_1 = xl_col_to_name(len(minorGroup)  )
							letter_to_1 = xl_col_to_name(len(minorGroup) + catNumber-1)
							letter_from_2 = xl_col_to_name(len(minorGroup) + catNumber + 1 )
							letter_to_2 = xl_col_to_name(len(minorGroup) + 2*catNumber)
							svyWorksheet.write(currow, len(minorGroup) + catNumber, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1, letter_from_1 , letter_to_1), fmtSvyBorder)
							filledCells.append((currow, len(minorGroup) + catNumber))
							svyWorksheet.write(currow, len(minorGroup) + 2*catNumber+1, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1, letter_from_2 , letter_to_2), fmtSvyBorder)
							filledCells.append((currow, len(minorGroup) + 2*catNumber+1))
						currow += 1

					#
					svyWorksheet.write(currow, 0, "Итого по %s" % ",".join(majorGroup), fmtSvyBoldRight)
					filledCells.append((currow, 0))
					for icol, category in enumerate(rptCategories):
						# суммарные объемы
						letter = xl_col_to_name(len(minorGroups) + icol + 1)
						svyWorksheet.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
						filledCells.append((currow, len(minorGroups) + icol + 1))
						# суммарный тоннаж
						letter = xl_col_to_name(len(minorGroups) + catNumber + 2 + icol)
						svyWorksheet.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
						filledCells.append((currow, len(minorGroups) + catNumber + 2 + icol))

					currow += 2
			else:
				groups = [BMFLD_MINENAME, SVY_ATTR_CATEGORY, BMFLD_FACTWORK]
				groupIndexes = [reportCutHeader.index(column) for column in groups]
				#
				currow = 6
				fromrow = currow
				filledCells = []
				#
				for group, groupValues in groupby(reportData_mine_interval_merged, key = lambda x: tuple(x[k] for k in groupIndexes)):
					#
					group = tuple(group)
					for icol, item in enumerate(group):
						if icol == 0:	svyWorksheet.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyBoldRight)
						else:			svyWorksheet.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
						filledCells.append((currow, icol))
					
					categories = []
					volumeCategories, tonnageCategories = {}, {}
					for row in groupValues:
						category, volume, tonnage = row[-3:]
						if not category in categories:			categories.append(category)
						if not category in volumeCategories:	volumeCategories[category] = 0
						if not category in tonnageCategories:	tonnageCategories[category] = 0
						#
						volumeCategories[category] += row[-2]
						tonnageCategories[category] += row[-1]
					#
					for category in categories:
						idxCategory = rptCategories.index(category)
						item = volumeCategories[category]
						svyWorksheet.write(currow, len(group) + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
						filledCells.append((currow, len(group) + idxCategory))
						item = tonnageCategories[category]
						svyWorksheet.write(currow, len(group) + catNumber + 1 + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
						filledCells.append((currow, len(group) + catNumber + 1 + idxCategory))

						letter_from_1 = xl_col_to_name(len(group)  )
						letter_to_1 = xl_col_to_name(len(group) + catNumber-1)
						letter_from_2 = xl_col_to_name(len(group) + catNumber + 1 )
						letter_to_2 = xl_col_to_name(len(group) + 2*catNumber)
						svyWorksheet.write(currow, len(group) + catNumber, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1,letter_from_1 , letter_to_1), fmtSvyBorder)
						filledCells.append((currow, len(group) + catNumber))
						svyWorksheet.write(currow, len(group) + 2*catNumber+1, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1, letter_from_2, letter_to_2), fmtSvyBorder)
						filledCells.append((currow, len(group) + 2*catNumber+1))
					currow += 1
				#
				svyWorksheet.write(currow, 0, "Итого", fmtSvyBoldRight)
				filledCells.append((currow, 0))
				for icol, category in enumerate(rptCategories):
					# суммарные объемы
					letter = xl_col_to_name(len(groups) + icol + 1)
					svyWorksheet.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
					filledCells.append((currow, len(groups) + icol + 1))
					# суммарный тоннаж
					letter = xl_col_to_name(len(groups) + catNumber + 2 + icol)
					svyWorksheet.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
					filledCells.append((currow, len(groups) + catNumber + 2 + icol))
				currow += 1
			# границы у пустых ячеек
			for irow in range(6, currow):
				for icol in range(0, rptColNumber):
					if not (irow, icol) in filledCells:
						svyWorksheet.write(irow, icol,  "", fmtSvyBorder)

			#endregion

			#region	 маркшейдерия_2

			catNumber = len(rptCategories)
			rptColNumber = catNumber*2 + 7

			svyWorksheet_2.set_column("A:A", 20)
			for i, icol in enumerate(range(66, 66+rptColNumber)):
				if i in [5, 14]:	svyWorksheet_2.set_column("%s:%s" % (chr(icol), chr(icol)), 11.3)
				else:				svyWorksheet_2.set_column("%s:%s" % (chr(icol), chr(icol)), 9)

			svyWorksheet_2.merge_range(xl_range(0, 0, 0, rptColNumber-1), "Замер горных работ", fmtSvyReportHeader)
			svyWorksheet_2.merge_range(xl_range(1, 0, 1, rptColNumber-1), "%s %d" % (month_names[self._bmCurrentDate.month-1], self._bmCurrentDate.year), fmtSvyReportHeader)
			svyWorksheet_2.merge_range("A4:A6", "Наименование", fmtSvyBoldCenter)
			svyWorksheet_2.merge_range("B4:B6", "Интервал", fmtSvyTableHeader)
			svyWorksheet_2.merge_range("C4:C6", self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME), fmtSvyTableHeader) 
			#svyWorksheet_2.merge_range("D4:D6",  self.main_app.settings.getValue(ID_GEO_UDSPNAME), fmtSvyTableHeader)
			svyWorksheet_2.merge_range("D4:D6", "Категория", fmtSvyTableHeader)
			svyWorksheet_2.merge_range("E4:E6", "Вид работ", fmtSvyTableHeader)
			svyWorksheet_2.merge_range(xl_range(3, 5, 3, 5+catNumber), "ОБЪЕМ (КУБ. М)", fmtSvyTableHeader)
			svyWorksheet_2.merge_range(xl_range(3, 6+catNumber, 3, 6+2*catNumber), "МАССА (Т)", fmtSvyTableHeader)

			tblHeader = rptCategories + ["Итого"] + rptCategories + ["Итого"]
			for icol, colname in enumerate(tblHeader, start = 5):
				svyWorksheet_2.merge_range(xl_range(4, icol, 5, icol), colname, fmtSvyTableHeader)

			# объединяем поля ВЫРАБОТКА и ИНТЕРВАЛ
			#idxMineName = reportHeader.index(BMFLD_MINENAME)
			#idxInterval = reportHeader.index(BMFLD_INTERVAL)
			#for i, row in enumerate(reportData):
			#	reportData[i][idxMineName] = ":".join([row[idxMineName], row[idxInterval]])
			#del idxMineName, idxInterval

			# удаление ненужных столбцов
			lastIndex = reportHeader.index(RPTFLD_FACT_DENSITY)
			reportCutHeader = reportHeader[:lastIndex]

			removeColumns = [BMFLD_FACTDATE, BMFLD_LAYER,  BMFLD_SVYBLOCK,   BMFLD_MATERIAL]
			removeIndexes = sorted([reportCutHeader.index(column) for column in removeColumns], reverse = True)

			for index in removeIndexes:
				reportCutHeader.remove(reportCutHeader[index])


			for i, row in enumerate(reportData):
				reportData[i] = reportData[i][:lastIndex]
				for index in removeIndexes:
					reportData[i] = reportData[i][:index] + reportData[i][index+1:]

			# удаление строк, соответствующих каркасам закладки
			idxCategory = reportCutHeader.index(SVY_ATTR_CATEGORY)
			if not self.isCustomReport() and not self.isByDateReport():
				reportData = list(filter(lambda x: x[idxCategory] != backfillCategory, reportData))

			# проверка индексов
			idxIndex = reportCutHeader.index(BMFLD_INDEX)
			for i, row in enumerate(reportData):
				if not row[idxIndex] in rptCategoryDictExtended:
					raise ValueError("'%s' - неизвестный индекс" % row[idxIndex])
				reportData[i][idxIndex] = rptCategoryDictExtended[row[idxIndex]]

			#reportData.sort(key = lambda x: (x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7]))
			reportData.sort(key = lambda x: (x[0], x[1],  x[3], x[4], x[5], x[6], x[7]))
			if self.isByMineReport() or self.isCustomReport() or self.isByDateReport():
				majorGroups = ["КАРКАС"]
				#minorGroups = [BMFLD_MINENAME, BMFLD_INTERVAL, self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper(), self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper(), SVY_ATTR_CATEGORY, BMFLD_FACTWORK]
				minorGroups = [BMFLD_MINENAME, BMFLD_INTERVAL, self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper(),  SVY_ATTR_CATEGORY, BMFLD_FACTWORK]
				majorGroupIndexes = [reportCutHeader.index(column) for column in majorGroups]
				minorGroupIndexes = [reportCutHeader.index(column) for column in minorGroups]
				#
				currow = 6
				filledCells = []
				for majorGroup, majorGroupValues in groupby(reportData, key = lambda x: tuple(x[k] for k in majorGroupIndexes)):
					fromrow = currow
					for minorGroup, minorGroupValues in groupby(majorGroupValues, key = lambda x: tuple(x[k] for k in minorGroupIndexes)):
						minorGroup = tuple(minorGroup)
						#
						for icol, item in enumerate(minorGroup):
							if icol == 0:	svyWorksheet_2.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyBoldRight)
							else:			svyWorksheet_2.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
							filledCells.append((currow, icol))
						
						categories = []
						volumeCategories, tonnageCategories = {}, {}
						for row in minorGroupValues:
							category, volume, tonnage = row[-3:]
							if not category in categories:			categories.append(category)
							if not category in volumeCategories:	volumeCategories[category] = 0
							if not category in tonnageCategories:	tonnageCategories[category] = 0
							#
							volumeCategories[category] += row[-2]
							tonnageCategories[category] += row[-1]
						#
						for category in categories:
							idxCategory = rptCategories.index(category)
							item = volumeCategories[category]
							svyWorksheet_2.write(currow, len(minorGroup) + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
							filledCells.append((currow, len(minorGroup) + idxCategory))
							item = tonnageCategories[category]
							svyWorksheet_2.write(currow, len(minorGroup) + catNumber + 1 + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
							filledCells.append((currow, len(minorGroup) + catNumber + 1 + idxCategory))

							letter_from_1 = xl_col_to_name(len(minorGroup)  )
							letter_to_1 = xl_col_to_name(len(minorGroup) + catNumber-1)
							letter_from_2 = xl_col_to_name(len(minorGroup) + catNumber + 1 )
							letter_to_2 = xl_col_to_name(len(minorGroup) + 2*catNumber)
							svyWorksheet_2.write(currow, len(minorGroup) + catNumber, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1, letter_from_1 , letter_to_1), fmtSvyBorder)
							filledCells.append((currow, len(minorGroup) + catNumber))
							svyWorksheet_2.write(currow, len(minorGroup) + 2*catNumber+1, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1, letter_from_2 , letter_to_2), fmtSvyBorder)
							filledCells.append((currow, len(minorGroup) + 2*catNumber+1))
						currow += 1

					#
					svyWorksheet_2.write(currow, 0, "Итого по %s" % ",".join(majorGroup), fmtSvyBoldRight)
					filledCells.append((currow, 0))
					for icol, category in enumerate(rptCategories):
						# суммарные объемы
						letter = xl_col_to_name(len(minorGroups) + icol + 1)
						svyWorksheet_2.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
						filledCells.append((currow, len(minorGroups) + icol + 1))
						# суммарный тоннаж
						letter = xl_col_to_name(len(minorGroups) + catNumber + 2 + icol)
						svyWorksheet_2.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
						filledCells.append((currow, len(minorGroups) + catNumber + 2 + icol))

					currow += 2
			else:
				#groups = [BMFLD_MINENAME, BMFLD_INTERVAL, self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper(), self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper(), SVY_ATTR_CATEGORY, BMFLD_FACTWORK]
				groups = [BMFLD_MINENAME, BMFLD_INTERVAL,  self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper(),  SVY_ATTR_CATEGORY, BMFLD_FACTWORK]
				groupIndexes = [reportCutHeader.index(column) for column in groups]
				#
				currow = 6
				fromrow = currow
				filledCells = []
				#
				for group, groupValues in groupby(reportData, key = lambda x: tuple(x[k] for k in groupIndexes)):
					#
					group = tuple(group)
					for icol, item in enumerate(group):
						if icol == 0:	svyWorksheet_2.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyBoldRight)
						else:			svyWorksheet_2.write(currow, icol, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
						filledCells.append((currow, icol))
					
					categories = []
					volumeCategories, tonnageCategories = {}, {}
					for row in groupValues:
						category, volume, tonnage = row[-3:]
						if not category in categories:			categories.append(category)
						if not category in volumeCategories:	volumeCategories[category] = 0
						if not category in tonnageCategories:	tonnageCategories[category] = 0
						#
						volumeCategories[category] += row[-2]
						tonnageCategories[category] += row[-1]
					#
					for category in categories:
						idxCategory = rptCategories.index(category)
						item = volumeCategories[category]
						svyWorksheet_2.write(currow, len(group) + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
						filledCells.append((currow, len(group) + idxCategory))
						item = tonnageCategories[category]
						svyWorksheet_2.write(currow, len(group) + catNumber + 1 + idxCategory, round(item) if not isinstance(item, str) else item, fmtSvyTableBody)
						filledCells.append((currow, len(group) + catNumber + 1 + idxCategory))

						letter_from_1 = xl_col_to_name(len(group)  )
						letter_to_1 = xl_col_to_name(len(group) + catNumber-1)
						letter_from_2 = xl_col_to_name(len(group) + catNumber + 1 )
						letter_to_2 = xl_col_to_name(len(group) + 2*catNumber)
						svyWorksheet_2.write(currow, len(group) + catNumber, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1,letter_from_1 , letter_to_1), fmtSvyBorder)
						filledCells.append((currow, len(group) + catNumber))
						svyWorksheet_2.write(currow, len(group) + 2*catNumber+1, '=IF(SUM({1}{0}:{2}{0})=0,"",SUM({1}{0}:{2}{0}))'.format(currow+1, letter_from_2, letter_to_2), fmtSvyBorder)
						filledCells.append((currow, len(group) + 2*catNumber+1))
					currow += 1
				#
				svyWorksheet_2.write(currow, 0, "Итого", fmtSvyBoldRight)
				filledCells.append((currow, 0))
				for icol, category in enumerate(rptCategories):
					# суммарные объемы
					letter = xl_col_to_name(len(groups) + icol + 1)
					svyWorksheet_2.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
					filledCells.append((currow, len(groups) + icol + 1))
					# суммарный тоннаж
					letter = xl_col_to_name(len(groups) + catNumber + 2 + icol)
					svyWorksheet_2.write("%s%d" % (letter, currow+1), '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(letter, fromrow+1, currow), fmtSvyBoldRight)
					filledCells.append((currow, len(groups) + catNumber + 2 + icol))
				currow += 1
			# границы у пустых ячеек
			for irow in range(6, currow):
				for icol in range(0, rptColNumber):
					if not (irow, icol) in filledCells:
						svyWorksheet_2.write(irow, icol,  "", fmtSvyBorder)

			#endregion

			wb.close()
			# запуск отчета
			try:
				os.startfile(reportPath)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось открыть файл отчета")
				return False
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Необработанная ошибка", str(e))
			raise
			return False
		#
		return True

	def updateOnServer(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		extracted = False
		# считывание выбранных данных
		(includeValues, restoreMiningValues, restoreGeologyValues, simulateConcreteValues), includeFields = self.selectedKeysAndFields()

		if len(includeValues) == 0:
			progress.setText(oldProgressText)
			return False
		# перенос данных из системных полей
		try:
			self.prepareBlockModels(extracted = extracted, restoreFields = False, dataTransfer = True)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", str(e))
			progress.setText(oldProgressText)
			return False
		# удаление системных полей
		if not self.removeBlockModelSystemFields(extracted = extracted):
			return False
		# загрузка
		try:
			self.displayData(extracted = extracted)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно загрузить данные. %s" % str(e))
			progress.setText(oldProgressText)
			return False
		# копирование файлов на сервер
		if not self.copyBlockModelsToServer():
			return False
		#
		progress.setText(oldProgressText)
		#
		qtmsgbox.show_information(self._bmPreviewDialog, "Выполнено", "Блочная модель успешно обновлена и скопирована на сервер")

	def check(self):
		bmItems = self.selectedItems()[ITEM_BM]
		if not bmItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите блочные модели")
			return

		# проверяем среди выбранных каркасов наличие маркшейдерских каркасов
		if self._is_custom_report:	tridbItems = self.selectedItems()[ITEM_TRIDB]
		else:						tridbItems = tuple(filter(lambda it: it.itemCategory() == ITEM_CATEGORY_SVY, self.selectedItems()[ITEM_TRIDB]))
		#
		if not self._is_bydate_report and not tridbItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите маркшейдерские каркасы")
			return

		# проверяем, что все блочные модели имеют метаданные
		sysdate = datetime.now()
		currdates = []
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()

			# проверка наличия файла блокировки
			if self.lockFileExists(netBmPath):
				lockpath = self.getLockPath(netBmPath)
				msg = "Блочная модель используется другим пользователем %s." % get_file_owner(lockpath)
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			bm = BlockModel(netBmPath, self.main_app)
			# проверка наличия метаданных блочных моделей
			if not bm.has_metadata:
				msg = "Блочная модель '%s' не имеет метаданных. Обратитесь к геологу" % bm.name
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			# проверка на соответствие метаданных содержанию блочной модели
			metadate = bm.metadata.current_date
			bmdate = bm.blockmodel_date
			if bmdate.year != metadate.year or bmdate.month != metadate.month:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Метаданные не соответствуют содержанию блочной модели '%s'. Обратитесь к геологу." % bm.name)
				return

			currdates.append(metadate)

		currdates = list(sorted(set(currdates)))
		if len(currdates) > 1:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выбранные блочные модели переведены в разные месяца. Выберите блочные модели, переведенные в один и тот же месяц")
			return
		elif (currdates[0].year == sysdate.year and currdates[0].month != sysdate.month) or (currdates[0].year < sysdate.year):
			if not qtmsgbox.show_question(self.main_app, "Внимание", "Состояние блочных моделей не соответствует текущей календарной дате. Продолжить?"):
				return
		#
		return True

	# подготовка к запуску
	def prepare(self):
		if not super(SvyAssignBm, self).prepare():
			return

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	def run(self):
		if not super(SvyAssignBm, self).run():
			return

		if self._is_report and not self.isCustomReport() and not self.isByDateReport():
			self._is_monthly_report = True

		progress = self.main_app.mainWidget.progressBar

		bmItems = self.selectedItems()[ITEM_BM]
		# считывание текущей даты
		netBm = BlockModel(bmItems[0].filePath(), self.main_app)
		self._bmCurrentDate = netBm.metadata.current_date
		BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)
		BMFLD_SYS_CURRDATE = "SYS_%s" % BMFLD_CURRDATE
		#
		if self._is_custom_report:	tridbItems = self.selectedItems()[ITEM_TRIDB]
		else:						tridbItems = tuple(filter(lambda it: it.itemCategory() == ITEM_CATEGORY_SVY, self.selectedItems()[ITEM_TRIDB]))
		netTridbPathsDict = self.getTridbPathsDict(tridbItems, copy = False)

		subxsize = self.main_app.settings.getValue(ID_SUBXSVY)
		subysize = self.main_app.settings.getValue(ID_SUBYSVY)
		subzsize = self.main_app.settings.getValue(ID_SUBZSVY)
		#
		elemFields = self.getIdwElements()
		elemUnits = self.getIdwUnits()
		#
		concDensity = float(self.main_app.settings.getValue(ID_CONCDENS))

		flags = (self.isMonthlyReport(), self.isByMineReport(), self.isCustomReport(), self.isByDateReport(), self.isUpdate())
		if not any(flags) or flags.count(True) > 1:
			raise Exception("Процесс прерван. Нарушена логика определения режима присваивания каркасов")

		### =================================================================================== ###
		### =========================== ПРОВЕРКА АТРИБУТОВ КАРКАСОВ =========================== ###
		### =================================================================================== ###

		# запускаем проверку атрибутов, если присваивание каркасов запущено из-под админа или маркшейдера, и не отчет по каркасной модели
		if self.main_app.userRole() in [ID_USERSVY, ID_USERADMIN] and not self.isCustomReport():
			progress.setText("Проверка атрибутов каркасов")
			try:
				progress_value = progress.value()
				wfvalidation = SvyWireframeAttributeValidation(self.main_app)
				wfvalidation.isSilent = True
				wfvalidation.exec(netTridbPathsDict)
				if not wfvalidation.result:
					return
				progress.setValue(progress_value)
			except Exception as e:
				raise Exception("Невозможно произвести проверку атрибутов каркасов. %s" % str(e))


		### =================================================================================== ###
		### ============================== КОПИРОВАНИЕ КАРКАСОВ =============================== ###
		### =================================================================================== ###

		progress.setRange(0,0)
		progress.setText("Копирование каркасов")
		try:					loctripaths = self.getTridbPathsDict(tridbItems, copy = True)
		except Exception as e:	raise Exception("Невозможно скопировать каркасы и считать атрибуты. %s" % str(e))

		### =================================================================================== ###
		### =========================== СОЗДАНИЕ НАБОРА КАРКАСОВ ============================== ###
		### =================================================================================== ###
		
		progress.setText("Создание набора каркасов")
		try:
			if self.isCustomReport():
				self._wireframe_sets = self.generateCustomWireframeSets(loctripaths)
			else:
				self._wireframe_sets = self.generateSvyWireframeSets(loctripaths, self._bmCurrentDate, self._is_report)
		except Exception as e:
			raise Exception("Невозможно подготовить данные для набора каркаcов. %s" % str(e))

		progress.setText("Инициализация присваивания атрибутов")
		# исключаем последний набор, если он бетонный
		if (self.isMonthlyReport() or self.isByMineReport()) and self._wireframe_sets[-1].isBackfillSet():
			self._wireframe_sets.pop()
		#
		wfsetCount = len(self._wireframe_sets)
		for wfsetIndex, wfset in enumerate(self._wireframe_sets):
			isLastSet = wfsetIndex == wfsetCount-1
			#
			isBackfillSet = wfset.isBackfillSet()
			displayName = wfset.displayName()

			if self.isCustomReport():
				types = ("1", "0", "0")
				systemValues = ("Y", DEFAULT_ATTR_TITLE, DEFAULT_ATTR_NAME)
				systemFields = (BMFLD_SYS_FIELD, BMFLD_SYS_MINENAME, BMFLD_SYS_INTERVAL)
			else:
				svy_attr_MINEBINDATTRNAME = self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper()
				bmfld_sys_MINEBINDATTRNAME = "SYS_%s" % svy_attr_MINEBINDATTRNAME
				types = ("0", "0", "0", "0", "1", "0", "0", "0", "1")
				systemValues = (
									SVY_ATTR_CATEGORY, DEFAULT_ATTR_CODE, DEFAULT_ATTR_TITLE, SVY_ATTR_DATE, "Y",
									DEFAULT_ATTR_NAME, SVY_ATTR_BLOCK, svy_attr_MINEBINDATTRNAME, "Y"
								)
				systemFields = (
									BMFLD_SYS_CURRDATE, BMFLD_SYS_FACTWORK, BMFLD_SYS_MINENAME, BMFLD_SYS_FACTDATE, BMFLD_SYS_CURRMINED,
									BMFLD_SYS_INTERVAL, BMFLD_SYS_SVYBLOCK, bmfld_sys_MINEBINDATTRNAME, BMFLD_SYS_FIELD
								)
			#
			systemStructure = ["%s:C:25:0" % systemField for systemField in systemFields]

			# параметры присваивания атрибутов
			assing_params = {}
			assing_params["attributes"] =  [[atype, systemValue, systemField] for atype, systemValue, systemField in zip(types, systemValues, systemFields)]
			assing_params["sub_xsize"] = subxsize
			assing_params["sub_ysize"] = subysize
			assing_params["sub_zsize"] = subzsize
			assing_params["overwrite_bool"] = True
			assing_params["purge_bool"] = True
			assing_params["wireframe_set"] = wfset

			#
			assignedData = []
			for bmItemIndex, bmItem in enumerate(bmItems):
				netBmPath = bmItem.filePath()
				netBm = BlockModel(netBmPath, self.main_app)

				### =================================================================================== ###
				### ============================ КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ =========================== ###
				### =================================================================================== ###

				if wfsetIndex == 0:
					locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % netBm.name)
					locBmExtractedPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_PART%s.DAT" % (netBm.name, str(bmItemIndex+1).zfill(3)))
					#
					self._netBmPathList.append(netBmPath)
					self._locBmPathList.append(locBmPath)
					# self._locBmExtractedPathList.append(locBmExtractedPath)
					#
					progress.setText("Копирование блочной модели")
					try:					self.copy_file(netBmPath, locBmPath)
					except Exception as e:	raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, str(e)))
				else:
					locBmPath = self._locBmPathList[bmItemIndex]

				# проверка наличия столбца данных за текущий и предыдущий месяцы
				f = MicromineFile()
				if not f.open(locBmPath):			raise Exception("Невозможно открыть файл %s" % locBmPath)
				if not BMFLD_CURRDATE in f.header:	raise Exception("Блочная модель '%s' не содержит поле '%s'" % (netBm.name, BMFLD_CURRDATE))
				f.close()

				### =================================================================================== ###
				### ======================== ИЗМЕНЕНИЕ СТРУКТУРЫ БЛОЧНОЙ МОДЕЛИ ======================= ###
				### =================================================================================== ###

				if wfsetIndex == 0:
					progress.setText("Добавление полей")
					try:					self.addFieldsToBlockModel(locBmPath, systemStructure)
					except Exception as e:	raise Exception("Невозможно изменить структуру блочной модели. %s" % str(e))
					f = MicromineFile()
					if not f.open(locBmPath):
						raise Exception("Невозможно открыть файл '%s'" % locBmPath)
					bmHeader = f.header
					f.close()

				### =================================================================================== ###
				### ============================= ПРИСВАИВАНИЕ АТРИБУТОВ ============================== ###
				### =================================================================================== ###

				progress.setText("Присваивание атрибутов")
				loc_bm = BlockModel(locBmPath, self.main_app)
				try:					loc_bm.assign_wireframes(assing_params)
				except Exception as e:	raise Exception("Невозможно присвоить атрибуты. %s" % str(e))

				### =================================================================================== ###
				### ========================== ИЗВЛЕЧЕНИЕ ПРИСВОЕННЫХ ДАННЫХ ========================== ###
				### =================================================================================== ###

				progress.setText("Извлечение присвоенных данных")
				try:					bmData = self.extractBlockModelData(locBmPath, [BMFLD_SYS_FIELD], ["Y"])
				except Exception as e:	raise Exception("Невозможно извлечь присвоенные данные. %s" % str(e))
				isUseDynamicDensity = netBm.metadata.is_use_dynamic_density
				for  i, row in enumerate(bmData):
					bmData[i].append(isUseDynamicDensity) 
				assignedData.extend(bmData)

			bmHeader.append('isUseDynamicDensity')

			### =================================================================================== ###
			### ============================= ОТЧЕТ ПО БЛОЧНЫМ МОДЕЛЯМ ============================ ###
			### =================================================================================== ###

			progress.setText("Создание отчета")
			#
			groupFields = (BMFLD_SYS_MINENAME, BMFLD_SYS_INTERVAL)
			groupIndexes = tuple(bmHeader.index(keyField) for keyField in groupFields)
			assignedData.sort(key = lambda x: tuple(x[index] for index in groupIndexes))
			#
			# инициализация диалога
			canSeeGrades = self.main_app.userRole() in [ID_USERGEO, ID_USERADMIN]

			self._bmPreviewDialog = BlockModelPreviewDialog(self)
			self._bmPreviewDialog.setWindowTitle(self._bmPreviewDialog.windowTitle() + " - " + wfset.displayName())
			self._bmPreviewDialog.setCanSeeGrades(canSeeGrades)
			self._bmPreviewDialog.setNeedsRestoreGeology(False)
			self._bmPreviewDialog.setNeedsSimulateConcrete(False)
			
			if self.isUpdate():
				self._bmPreviewDialog.setNeedsRestoreMiningWork(False)
			self._bmPreviewDialog.setFldCurrdate(BMFLD_CURRDATE)
			self._bmPreviewDialog.setGroupFields(groupFields)
			# настройка функциональности кнопок
			self._bmPreviewDialog.buttonLoad.clicked.connect(self.loadToVizex)
			self._bmPreviewDialog.buttonSave.clicked.connect(self.saveToDb)
			self._bmPreviewDialog.buttonExport.clicked.connect(self.exportToExcel)
			self._bmPreviewDialog.buttonUpdate.clicked.connect(self.updateOnServer)
			# настройка видимости кнопок
			self._bmPreviewDialog.paramsFrame.groupBoxDilution.setVisible(False)
			if isLastSet:
				self._bmPreviewDialog.buttonLoad.setVisible(True)
				self._bmPreviewDialog.buttonContinue.setVisible(False)
				self._bmPreviewDialog.buttonCancel.setText("Закрыть")
				if self.isUpdate():
					self._bmPreviewDialog.paramsFrame.setVisible(False)
					self._bmPreviewDialog.buttonExport.setVisible(False)
					self._bmPreviewDialog.buttonUpdate.setVisible(True)
					self._bmPreviewDialog.buttonSave.setVisible(False)
				else:
					self._bmPreviewDialog.buttonExport.setVisible(True)
					self._bmPreviewDialog.buttonUpdate.setVisible(False)
					if self.isCustomReport():
						self._bmPreviewDialog.paramsFrame.setVisible(False)
						self._bmPreviewDialog.buttonSave.setVisible(False)
					else:
						self._bmPreviewDialog.paramsFrame.setVisible(True)
						self._bmPreviewDialog.buttonSave.setVisible(True)
			else:
				self._bmPreviewDialog.paramsFrame.setVisible(False)
				self._bmPreviewDialog.buttonLoad.setVisible(False)
				self._bmPreviewDialog.buttonExport.setVisible(False)
				self._bmPreviewDialog.buttonUpdate.setVisible(False)
				self._bmPreviewDialog.buttonSave.setVisible(False)
				self._bmPreviewDialog.buttonContinue.setVisible(True)
				self._bmPreviewDialog.buttonCancel.setVisible(True)

			#
			reportParams = dict(
				bmData = assignedData,
				bmHeader = bmHeader,
				fieldCurrDate = BMFLD_CURRDATE,
				concDensity = concDensity,
				elemFields = elemFields,
				elemUnits = elemUnits,
				geoCodes = self.main_app.settings.getValue(ID_GEOCODES),
				svyCatsAndCodes = self.main_app.settings.getValue(ID_SVYCAT)
			)
			sysKeys = []
			for group, groupValues in groupby(assignedData, key = lambda x: tuple(x[index] for index in groupIndexes)):
				sysKey = tuple(group)
				bmData = tuple(groupValues)

				reportParams["bmData"] = bmData
				reportParams["bmHeader"] = bmHeader

				reportData, reportHeader = bmDataPreviewReport(**reportParams)

				listItem = createBmPreviewListTreeItem(":".join(sysKey), ITEM_WIREFRAME, ITEM_CATEGORY_SVY)
				listItem.setKey(sysKey)
				self._bmPreviewDialog.addBlockModelPreview(listItem, reportData, reportHeader)

			# выбираем Руду/Породу по умолчанию
			# if self._is_report:
			self._bmPreviewDialog.setMaterialSelected(True)

			progress.setText("Выбор данных для занесения в блочную модель")
			if not self._bmPreviewDialog.is_empty:
				self._bmPreviewDialog.exec()
			else:
				qtmsgbox.show_information(self.main_app, "Внимание", "Нет данных. Вероятно выбранные блочные модели не пересекаются с выбранными каркассами.")

			if self._bmPreviewDialog.is_cancelled:
				return

			#
			if not isLastSet:
				self.restoreFields(extracted = False)
				self.transferBlockModelData()
		#
		progress.setText("")
		progress.setRange(0,100)

		return True


