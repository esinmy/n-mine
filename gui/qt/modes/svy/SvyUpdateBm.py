try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.modes.svy.SvyAssignBm import SvyAssignBm

from mm.formsets import loadTriangulationSet, loadBlockModel

from utils.blockmodel import BlockModel
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyUpdateBm(SvyAssignBm):
	def run(self):
		bmItems = self.selectedItems()[ITEM_BM]
		self.createLockFiles([bmItem.filePath() for bmItem in bmItems])			# создание LCK файлов

		return super(SvyUpdateBm, self).run()