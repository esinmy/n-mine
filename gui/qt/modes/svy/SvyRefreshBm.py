try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
from datetime import datetime
import os

from gui.qt.ProcessWindow import ProcessWindow
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.modes.WireframeAttributeValidation import SvyWireframeAttributeValidation

from mm.formsets import loadTriangulationSet, loadBlockModel, create_wireframe_set
from mm.mmutils import Tridb, MicromineFile

from utils.blockmodel import BlockModel
from utils.constants import *
from utils.nnutil import prev_date, next_date, date_to_fieldname
from utils.osutil import get_temporary_prefix, get_file_owner
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class SvyRefreshBm(ProcessWindow):
	def __init__(self, *args, **kw):
		super(SvyRefreshBm, self).__init__(*args, **kw)

	def check(self):
		if not super(SvyRefreshBm, self).check():
			return

		# проверяем среди выбранных каркасов наличие маркшейдерских каркасов
		tridbItems = tuple(filter(lambda it: it.itemCategory() == ITEM_CATEGORY_SVY, self.selectedItems()[ITEM_TRIDB]))
		if not tridbItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите маркшейдерские каркасы")
			return

		bmItems = self.selectedItems()[ITEM_BM]
		if not bmItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите блочные модели")
			return

		# проверка наличия файла блокировки
		for bmItem in bmItems:
			bmpath = bmItem.filePath()
			if self.lockFileExists(bmpath):
				lockpath = self.getLockPath(bmpath)
				qtmsgbox.show_error(self.main_app, "Ошибка", "Блочная модель используется пользователем '%s'" % get_file_owner(lockpath))
				return
		
		sysdate = datetime.now()
		currdates = []
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()

			bm = BlockModel(netBmPath, self.main_app)
			# проверка наличия метаданных блочных моделей
			if not bm.has_metadata:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Блочная модель '%s' не имеет метаданных. Обратитесь к геологу" % bm.name)
				return

			# проверка на соответствие метаданных содержанию блочной модели
			metadate, bmdate = bm.metadata.current_date, bm.blockmodel_date
			if bmdate.year != metadate.year or bmdate.month != metadate.month:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Метаданные не соответствуют содержанию блочной модели '%s'. Обратитесь к геологу." % bm.name)
				return

			currdates.append(bmdate)

		# данная проверка необходима, чтобы исключить из набора каркасов те каркасы,
		# дата которых больше или равно текущей дате блочной модели (каркасы с текущей датой
		# могут быть использованы только в маркшейдерском обновлении блочной модели)
		if len(set(currdates)) > 1:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выбранные блочные модели переведены в разные месяца. Выберите блочные модели, переведенные в один и тот же месяц")
			return

		return True

	#
	def prepare(self):
		if not super(SvyRefreshBm, self).prepare():
			return

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	def run(self):
		if not super(SvyRefreshBm, self).run():
			return

		progress = self.main_app.mainWidget.progressBar
		progress.setRange(0,0)

		bmItems = self.selectedItems()[ITEM_BM]
		tridbItems = list(filter(lambda it: it.itemCategory() == ITEM_CATEGORY_SVY, self.selectedItems()[ITEM_TRIDB]))
		netTridbPathsDict = self.getTridbPathsDict(tridbItems, copy = False)

		subxsize = self.main_app.settings.getValue(ID_SUBXSVY)
		subysize = self.main_app.settings.getValue(ID_SUBYSVY)
		subzsize = self.main_app.settings.getValue(ID_SUBZSVY)

		#
		concDensity = self.main_app.settings.getValue(ID_CONCDENS)

		#
		allCategoriesAndCodes = self.getSvyCategoriesAndCodes()
		# назначение и категория оперативного учета
		cwPurpose, cwCategory = allCategoriesAndCodes[4][0]
		# назначение и категория закладки
		concPurpose, concCategory = allCategoriesAndCodes[-2][0]

		purposes, categories = [], []
		codes, descriptions = [], []
		for (purpose, category), CodesAndDescriptions in allCategoriesAndCodes[:4]:
			purposes.append(purpose)
			categories.append(category)
			codes.append([])
			for code, description in CodesAndDescriptions:
				codes[-1].append(code)

		purposes.append(concPurpose)
		categories.append(concCategory)
		codes.append([code for code, description in allCategoriesAndCodes[-2][1]])

		# приоритеты категорий
		# 1. Камерно-кубажные
		# 2. Очистные работы
		# 3. Горно-подготовительные работы
		# 4. Горно-капитальные работы
		# 5. Закладка
		purpOrderList = [4, 1, 3, 2, 5]
		purpOrderDict = dict(zip(purposes, purpOrderList))

		purpCatDict = dict(zip(purposes, categories))
		purpCodeDict = dict(zip(purposes, codes))

		### =================================================================================== ###
		### =========================== ПРОВЕРКА АТРИБУТОВ КАРКАСОВ =========================== ###
		### =================================================================================== ###

		# запускаем проверку атрибутов, если присваивание каркасов запущено из-под админа или маркшейдера
		if self.main_app.userRole() in [ID_USERSVY, ID_USERADMIN]:
			progress.setText("Проверка атрибутов каркасов")
			try:
				progress_value = progress.value()
				wfvalidation = SvyWireframeAttributeValidation(self.main_app)
				wfvalidation.isSilent = True
				wfvalidation.exec(netTridbPathsDict)
				if not wfvalidation.result:
					self._is_cancelled = True
					return
				progress.setValue(progress_value)
			except Exception as e:
				raise Exception("Невозможно произвести проверку атрибутов каркасов. %s" % str(e))


		### =================================================================================== ###
		### ============================== КОПИРОВАНИЕ КАРКАСОВ =============================== ###
		### =================================================================================== ###

		# считываем локальные пути выбранных каркасов после копирования
		try:					loctripaths = self.getTridbPathsDict(tridbItems, copy = True)
		except Exception as e:	raise Exception(str(e))

		### =================================================================================== ###
		### ========================== СЧИТЫВАНИЕ КОДОВ И КАТЕГОРИЙ =========================== ###
		### =================================================================================== ###

		bm = BlockModel(bmItems[0].filePath(), self.main_app)
		metadate = bm.metadata.current_date

		# извлечение всех дат из каркасов и сортировка по дате и категории
		wfdata = []
		for tripath in loctripaths:
			try:		tridb = Tridb(tripath)
			except Exception as e:
				# qtmsgbox.show_information(self.main_app, "", "Не могу открыть тридб '%s'. %s" % (str(tripath), str(e)))
				continue
			#
			for wfname in loctripaths[tripath]:
				wireframe = tridb[wfname]
				# считываем категорию
				category = wireframe.user_attribute(SVY_ATTR_CATEGORY)
				code = wireframe.default_attribute(DEFAULT_ATTR_CODE)
				purpose = self.getPurposeByCategoryAndCode(category, code)
				if not purpose in purposes:
					qtmsgbox.show_information(self.main_app, "Ошибка", "Не могу определить назначение для каркаса %s. %s, %s" % (wfname, str(purpose), str(purposes)))
					continue
				# считываем остальные атрибуты
				try:						wfdate = datetime.strptime(wireframe.user_attribute(SVY_ATTR_DATE), "%Y-%m-%d").date()
				except Exception as e:
					# qtmsgbox.show_information(self.main_app, "", "Не могу определить дату")
					continue
				# пропускаем каркас, у которого дата позже даты блочной модели
				if (wfdate.year == metadate.year and wfdate.month >= metadate.month) or (wfdate.year > metadate.year):
					# qtmsgbox.show_information(self.main_app, "", "Не могу подсунуть более свежий каркас")
					continue
				#	
				order = purpOrderDict[purpose]
				isconc = order == purpOrderList[-1]
				wfdata.append([tripath, wfname, wfdate, isconc, order])
			tridb.close()
		# сортируем по дате и приоритету
		wfdata.sort(key = lambda x: (x[2], x[-1]))

		if len(wfdata) == 0:
			msg = "Не найдено исторических каркасов. Данная операция применима "
			msg += "для занесения в блочную модель только исторической информации по горным работам."
			qtmsgbox.show_error(self.main_app, "Ошибка", msg)
			return

		### =================================================================================== ###
		### ============== СОЗДАНИЕ НАБОРОВ КАРКАСОВ ДЛЯ ПРИСВОЕНИЯ АТРИБУТОВ ================= ###
		### =================================================================================== ###

		progress.setText("Создание набора каркасов")
		# список данных для наборов
		# собираются отдельные наборы по закладке и пустоте в хронологическом порядке
		# если встретилась закладка, то создается новый набор, состоящий только из закладки, пока не встретится пустота
		# если встретилась пустота, то создается новый набор, состоящий только из пустоты, пока не встретится закладка
		wfsetsList = []
		wfsetData = [wfdata[0]]
		for row in wfdata[1:]:
			if row[-2] == wfsetData[-1][-2]:
				wfsetData.append(row)
			else:
				wfsetsList.append(wfsetData)
				wfsetData = [row]
		if wfsetData:
			wfsetsList.append(wfsetData)

		# подготавливаем набор по пустоте в обратной хронологической последовательности
		# и в прямой по приоритетности (начинаем с самых старых КК и заканчиваем самыми свежими ПРОХ),
		# отдельно формируем наборы с закладкой, расположенные в прямой хронологической последовательности
		for i, wfsetlist in enumerate(wfsetsList):
			isconc = wfsetlist[0][-2] == True
			if isconc:			wfsetsList[i] = [[line[0], DEFAULT_ATTR_NAME, line[1]] for line in wfsetlist]
			else:				wfsetsList[i] = [[line[0], DEFAULT_ATTR_NAME, line[1]] for line in sorted(wfsetlist, key = lambda x: (x[2], -x[-1]), reverse = True)]

		# создание наборов
		wfsets = [create_wireframe_set(wfsetData) for wfsetData in wfsetsList]

		progress.setText("Пополнение блочных моделей")

		# определение текущего и предыдущего месяца
		bm = BlockModel(bmItems[0].filePath(), self.main_app)
		# названия полей предыдущего и текущего месяца соответственно
		currdate = date_to_fieldname(bm.blockmodel_date)
		prevdate = prev_date(currdate, ret = "str")

		# создание LCK файлов
		self.createLockFiles([bmItem.filePath() for bmItem in bmItems])

		# пополнение блочных моделей
		for bmItem in bmItems:
			net_bm = BlockModel(bmItem.filePath(), self.main_app)

			# пути блочных моделей
			netBmPath = net_bm.path
			locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % net_bm.name)
			locBmPartPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_%s.DAT" % (net_bm.name, currdate))
			bkpBmPath = net_bm.backup_path

			# проверка наличия столбца данных за текущий и предыдущий месяцы
			f = MicromineFile()
			if not f.open(netBmPath):			raise Exception("Невозможно открыть файл %s" % netBmPath)
			if not currdate in f.header:		raise Exception("Блочная модель '%s' не содержит поле '%s'" % (net_bm.name, currdate))
			f.close()

			### =================================================================================== ###
			### ============================ КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ =========================== ###
			### =================================================================================== ###

			# копирование
			progress.setText("%s. Создание резервной копии" % net_bm.name)
			err = self.exec_proc(self.copy_file, netBmPath, bkpBmPath)
			if err:					raise Exception("Невозможно создать резервную копию файла %s. %s" % (netBmPath, err))
			if self.is_cancelled:	return

			progress.setText("%s. Копирование блочной модели" % net_bm.name)
			err = self.exec_proc(self.copy_file, netBmPath, locBmPath)
			if err:					raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
			if self.is_cancelled:	return

			### =================================================================================== ###
			### ============================ ОПРЕДЕЛЕНИЕ ИНДЕКСОВ ПОЛЕЙ =========================== ###
			### =================================================================================== ###

			f = MicromineFile()
			if not f.open(locBmPath):
				raise Exception("Невозможно открыть файл '%s'" % locBmPath)

			# поля блочной модели
			fldCurrdate = currdate
			fldGeoMaterial = BMFLD_GEOMATERIAL
			fldMaterial = BMFLD_MATERIAL
			fldGeoIndex = BMFLD_GEOINDEX
			fldIndex = BMFLD_INDEX
			fldDensity = BMFLD_DENSITY
			fldMineMineName = BMFLD_MINE_MINENAME
			fldMineName = BMFLD_MINENAME
			fldMineWork = BMFLD_MINEWORK
			fldFactWork = BMFLD_FACTWORK
			fldMineDate = BMFLD_MINEDATE
			fldFactDate = BMFLD_FACTDATE
			fldTotalMined = BMFLD_TOTALMINED
			fldMineInterval = BMFLD_MINE_INTERVAL
			fldInterval = BMFLD_INTERVAL

			# индексы полей блочной модели
			idxMineDate = f.get_field_id(fldMineDate)
			idxFactDate = f.get_field_id(fldFactDate)
			idxMineWork = f.get_field_id(fldMineWork)
			idxFactWork = f.get_field_id(fldFactWork)
			idxMineInterval = f.get_field_id(fldMineInterval)
			idxMineMineName = f.get_field_id(fldMineMineName)
			idxMineName = f.get_field_id(fldMineName)
			idxTotalMined = f.get_field_id(fldTotalMined)
			idxInterval = f.get_field_id(fldInterval)
			idxGeoMaterial = f.get_field_id(fldGeoMaterial)
			idxMaterial = f.get_field_id(fldMaterial)
			idxGeoIndex = f.get_field_id(fldGeoIndex)
			idxIndex = f.get_field_id(fldIndex)
			idxDensity = f.get_field_id(fldDensity)
			idxBlock = f.get_field_id(BMFLD_SVYBLOCK)
			idxLongwall = f.get_field_id(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())

			# системные поля блочной модели
			fldSystem = "SYS_FIELD"
			fldSysCurrdate = "SYS_%s" % currdate
			fldSysMineName = "SYS_%s" % BMFLD_MINENAME
			fldSysFactDate = "SYS_%s" % BMFLD_FACTDATE
			fldSysFactWork = "SYS_%s" % BMFLD_FACTWORK
			fldSysTotalMined = "SYS_%s" % BMFLD_TOTALMINED
			fldSysInterval = "SYS_%s" % BMFLD_INTERVAL
			fldSysBlock = "SYS_%s" % BMFLD_SVYBLOCK
			fldSysLongwall = "SYS_%s" % self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper()

			#
			sysFields = [fldSystem, fldSysCurrdate, fldSysFactDate, fldSysFactWork, fldSysMineName, fldSysTotalMined, fldSysInterval, fldSysBlock, fldSysLongwall]
			structure = f.structure
			for sysField in sysFields:
				if not sysField in f.header:
					structure.add_field(sysField, MMpy.FieldType.character, 25, 0)
			f.structure = structure

			# индексы системных полей
			idxSystem = f.get_field_id(fldSystem)
			idxSysCurrdate = f.get_field_id(fldSysCurrdate)
			idxSysFactDate = f.get_field_id(fldSysFactDate)
			idxSysFactWork = f.get_field_id(fldSysFactWork)
			idxSysMineName = f.get_field_id(fldSysMineName)
			idxSysTotalMined = f.get_field_id(fldSysTotalMined)
			idxSysInterval = f.get_field_id(fldSysInterval)
			idxSysBlock = f.get_field_id(fldSysBlock)
			idxSysLongwall = f.get_field_id(fldSysLongwall)

			allSystemIndexes = (idxSystem, idxSysCurrdate, idxSysMineName, idxSysFactDate, idxSysFactWork, idxSysTotalMined, idxSysInterval, idxSysBlock, idxSysLongwall)

			f.close()


			# присваивание
			progress.setText("%s. Присваивание атрибутов" % net_bm.name)
			for wfset in wfsets:
				atypes = ["0", "0", "0", "0", "1", "0", "0", "0"]
				avalues = [SVY_ATTR_CATEGORY, DEFAULT_ATTR_CODE, DEFAULT_ATTR_TITLE, SVY_ATTR_DATE, "Y", DEFAULT_ATTR_NAME, SVY_ATTR_BLOCK]
				avalues.append(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
				afields = [fldSysCurrdate, fldSysFactWork, fldSysMineName, fldSysFactDate, fldSysTotalMined, fldSysInterval, fldSysBlock, fldSysLongwall]

				loc_bm = BlockModel(locBmPath, self.main_app)
				assing_params = {}
				assing_params["attributes"] = [[atype, avalue, afield] for atype, avalue, afield in zip(atypes, avalues, afields)]
				assing_params["sub_xsize"] = subxsize
				assing_params["sub_ysize"] = subysize
				assing_params["sub_zsize"] = subzsize
				assing_params["overwrite_bool"] = True
				assing_params["purge_bool"] = True
				assing_params["wireframe_set"] = wfset
				try:					loc_bm.assign_wireframes(assing_params)
				except Exception as e:	raise Exception("Не удалось присвоить атрибуты. %s" % str(e))

				# 
				f = MicromineFile()
				if not f.open(locBmPath):
					raise Exception("Невозможно открыть файл '%s'" % locBmPath)

				# создаем файл куска блочной модели
				MicromineFile.create(locBmPartPath, f.structure)

				### =================================================================================== ###
				### ==================================== ЗАНЕСЕНИЕ ДАННЫХ ============================= ###
				### =================================================================================== ###

				for i in range(f.records_count):
					if i % 500 == 0:
						QtCore.QCoreApplication.processEvents()

					#
					sysdate = f.get_str_field_value(idxSysFactDate, i+1)
					# пропускаем строку, если нет корректной даты
					if not sysdate:
						continue
					#
					try:		sysdate = datetime.strptime(sysdate, "%Y-%m-%d")
					except:		continue

					date = f.get_str_field_value(idxFactDate, i+1)
					try:		date = datetime.strptime(date, "%Y-%m-%d")
					except:		date = datetime(1961, 4, 12)

					#
					sysCategory = f.get_str_field_value(idxSysCurrdate, i+1)
					#

					# if sysdate >= date:
					# 	if sysCategory == concCategory:
					# 		if f.get_str_field_value(idxMaterial, i+1) == concCategory and f.get_str_field_value(idxMineName, i+1) != f.get_str_field_value(idxSysMineName, i+1):
					# 			f.set_field_value(idxGeoMaterial, i+1, f.get_str_field_value(idxMaterial, i+1))			#
					# 			f.set_field_value(idxGeoIndex, i+1, f.get_str_field_value(idxIndex, i+1))				#

					_currdate, _lastdate = prevdate, currdate
					while _currdate <= _lastdate:
						idxCurrdate = f.get_field_id(_currdate)
						currCategory = f.get_str_field_value(idxCurrdate, i+1)

						# если присвоенная дата больше ДАТА,
						# то пустотой заполняем все поля до текущей даты, не содержащих пустоты,
						# начиная либо с 12 месяца прошлого года, либо с присвоенной даты
						if sysdate >= date:
							# если присваиваем пустоту (по непустоте)
							if sysCategory in categories[:-1] and currCategory not in categories[:-1]:
								f.set_field_value(idxMineDate, i+1, f.get_str_field_value(idxSysFactDate, i+1))
								f.set_field_value(idxFactDate, i+1, f.get_str_field_value(idxSysFactDate, i+1))
								f.set_field_value(idxMineWork, i+1, f.get_str_field_value(idxSysFactWork, i+1))
								f.set_field_value(idxFactWork, i+1, f.get_str_field_value(idxSysFactWork, i+1))
								f.set_field_value(idxBlock, i+1, f.get_str_field_value(idxSysBlock, i+1))
								f.set_field_value(idxLongwall, i+1, f.get_str_field_value(idxSysLongwall, i+1))

								f.set_field_value(idxMineInterval, i+1, f.get_str_field_value(idxSysInterval, i+1))			#
								f.set_field_value(idxMineMineName, i+1, f.get_str_field_value(idxSysMineName, i+1))			#

								f.set_field_value(idxInterval, i+1, f.get_str_field_value(idxSysInterval, i+1))
								f.set_field_value(idxMineName, i+1, f.get_str_field_value(idxSysMineName, i+1))
								f.set_field_value(idxTotalMined, i+1, f.get_str_field_value(idxSysTotalMined, i+1))
								f.set_field_value(idxCurrdate, i+1, sysCategory)
							# если присваиваем бетон
							if sysCategory == concCategory and currCategory != concCategory:
								f.set_field_value(idxMaterial, i+1, sysCategory)
								f.set_field_value(idxIndex, i+1, f.get_str_field_value(idxSysFactWork, i+1))
								f.set_field_value(idxDensity, i+1, concDensity)
								#
								f.set_field_value(idxFactDate, i+1, f.get_str_field_value(idxSysFactDate, i+1))
								f.set_field_value(idxFactWork, i+1, f.get_str_field_value(idxSysFactWork, i+1))
								f.set_field_value(idxTotalMined, i+1, f.get_str_field_value(idxSysTotalMined, i+1))
								f.set_field_value(idxCurrdate, i+1, sysCategory)

								# if not f.get_str_field_value(idxMineName, i+1):
								f.set_field_value(idxMineName, i+1, f.get_str_field_value(idxSysMineName, i+1))
								# if not f.get_str_field_value(idxInterval, i+1):
								f.set_field_value(idxInterval, i+1, f.get_str_field_value(idxSysInterval, i+1))
								if not f.get_str_field_value(idxBlock, i+1):
									f.set_field_value(idxBlock, i+1, f.get_str_field_value(idxSysBlock, i+1))
								if not f.get_str_field_value(idxLongwall, i+1):
									f.set_field_value(idxLongwall, i+1, f.get_str_field_value(idxSysLongwall, i+1))

						# если присвоенная дата меньше ДАТА,
						# то бетоном заполняется только руда/неруда, а пустотой
						# заполняем все поля до текущей или до закладки
						if sysdate < date:
							# если присваиваем пустоту
							if sysCategory in categories[:-1]:
								# восполняем пустоту (по пустоте, руде и неруде)
								f.set_field_value(idxMineDate, i+1, f.get_str_field_value(idxSysFactDate, i+1))
								f.set_field_value(idxMineWork, i+1, f.get_str_field_value(idxSysFactWork, i+1))

								f.set_field_value(idxMineInterval, i+1, f.get_str_field_value(idxSysInterval, i+1))			#
								f.set_field_value(idxMineMineName, i+1, f.get_str_field_value(idxSysMineName, i+1))			#

								f.set_field_value(idxBlock, i+1, f.get_str_field_value(idxSysBlock, i+1))
								f.set_field_value(idxLongwall, i+1, f.get_str_field_value(idxSysLongwall, i+1))
								f.set_field_value(idxInterval, i+1, f.get_str_field_value(idxSysInterval, i+1))
								f.set_field_value(idxMineName, i+1, f.get_str_field_value(idxSysMineName, i+1))
								f.set_field_value(idxTotalMined, i+1, f.get_str_field_value(idxSysTotalMined, i+1))

								if currCategory != concCategory:
									f.set_field_value(idxFactDate, i+1, f.get_str_field_value(idxSysFactDate, i+1))
									f.set_field_value(idxFactWork, i+1, f.get_str_field_value(idxSysFactWork, i+1))
									f.set_field_value(idxCurrdate, i+1, sysCategory)
								# прерываемся, если в текующем поле встретилась закладка
								if currCategory == concCategory:
									break
							# если присваиваем бетон и в текущем месяце не было работ (геология)
							if sysCategory == concCategory and currCategory not in categories:
								f.set_field_value(idxMaterial, i+1, sysCategory)
								f.set_field_value(idxIndex, i+1, f.get_str_field_value(idxSysFactWork, i+1))
								f.set_field_value(idxDensity, i+1, concDensity)
								#
								f.set_field_value(idxFactDate, i+1, f.get_str_field_value(idxSysFactDate, i+1))
								f.set_field_value(idxFactWork, i+1, f.get_str_field_value(idxSysFactWork, i+1))
								f.set_field_value(idxBlock, i+1, f.get_str_field_value(idxSysBlock, i+1))
								f.set_field_value(idxLongwall, i+1, f.get_str_field_value(idxSysLongwall, i+1))
								f.set_field_value(idxInterval, i+1, f.get_str_field_value(idxSysInterval, i+1))
								f.set_field_value(idxMineName, i+1, f.get_str_field_value(idxSysMineName, i+1))
								f.set_field_value(idxTotalMined, i+1, f.get_str_field_value(idxSysTotalMined, i+1))
								f.set_field_value(idxCurrdate, i+1, sysCategory)
		
						# переходим в следующйи месяц
						_currdate = next_date(_currdate, ret = "str")

					# добавляем флаг во вспомогательное поле, чтобы затем извлечь присвоенный кусок
					f.set_field_value(idxSystem, i+1, "Y")
				f.close()

			# извлечение куска блочной модели
			err = self.exec_proc(self.extractBlockModelData, locBmPath, [fldSystem], ["Y"], locBmPartPath)
			if err:
				raise Exception("Невозможно извлечь данные из блочной модели. %s" % err)
			
			progress.setText("%s. Изменение структуры блочной модели" % net_bm.name)
			# изменение структуры блочной модели
			f = MicromineFile()
			if not f.open(locBmPath):
				raise Exception("Невозможно открыть файл '%s'" % locBmPath)
			structure = f.structure
			for idx in sorted(allSystemIndexes, reverse = True):
				structure.delete_field(idx)
			structure.delete_field(idxSysCurrdate)
			structure.delete_field(idxSystem)
			f.structure = structure
			f.close()

			# изменение структуры куска блочной модели
			f = MicromineFile()
			if not f.open(locBmPartPath):
				raise Exception("Невозможно открыть файл '%s'" % locBmPartPath)
			structure = f.structure
			for idx in sorted(allSystemIndexes, reverse = True):
				structure.delete_field(idx)
			structure.delete_field(idxSysCurrdate)
			structure.delete_field(idxSystem)
			f.structure = structure
			f.close()

			### =================================================================================== ###
			### ====================== КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ НА СЕРВЕР ======================= ###
			### =================================================================================== ###

			progress.setText("Копирование блочной модели")
			err = self.exec_proc(self.copy_file, locBmPath, netBmPath)
			if err:
				raise Exception("Невозможно скопировать файл '%s'. %s" % (locBmPath, err))

			progress.setText("Загрузка объектов")
			# загрузка объектов в Визекс
			userFormsets = list(zip(*self.main_app.settings.getValue(ITEM_CATEGORY_FORMSET_DICT[ITEM_CATEGORY_GEO])))[1]
			userFormsets = list([int(setId) if setId else None for setId in userFormsets])
			formsetsDict = dict(zip(MMTYPE_LIST, userFormsets))
			# загрузка блочной модели
			setId = formsetsDict[MMTYPE_BM]
			bm = BlockModel(locBmPartPath, self.main_app)
			bm.load("%s. Горные работы" % net_bm.name, setId, currdate)

			# обновление метаданных блочной модели
			net_bm.metadata.trigger_update()
			net_bm.save()

		progress.setText("")
		progress.setRange(0, 100)
		qtmsgbox.show_information(self.main_app, "Выполнено", "Пополнение блочных моделей успешно выполнено")