try:			import MMpy
except:			pass

import os

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.modes.svy.SvyAssignBm import SvyAssignBm
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyReportBm(SvyAssignBm):
	def __init__(self, *args, **kw):
		super(SvyReportBm, self).__init__(*args, **kw)
		self._is_report = True

class CustomReportBm(SvyReportBm):
	def __init__(self, *args, **kw):
		super(CustomReportBm, self).__init__(*args, **kw)
		self._is_custom_report = True
