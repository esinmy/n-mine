from gui.qt.modes.svy.SvyReportBm import SvyReportBm
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class CustomReportBm(SvyReportBm):
	def __init__(self, *args, **kw):
		super(CustomReportBm, self).__init__(*args, **kw)
		self._is_custom_report = True

