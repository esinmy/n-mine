try:		import MMpy
except:		pass

from PyQt4 import QtGui, QtCore
import os

from mm.formsets import create_wireframe_set, loadStrings, defaultLoadAnnotation
from mm.mmutils import MicromineFile  , get_micromine_version

from utils.constants import *
from utils.blockmodel import BlockModel
from utils.validation import IntValidator
from utils.nnutil import date_to_fieldname
from utils.osutil import get_file_owner, get_temporary_prefix

from gui.qt.dialogs.TreeFormsetBrowser import TreeMmFormsetBrowserDialog
from gui.qt.dialogs.BaseDialog import BaseDialog
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import ProcessWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def slice_wireframe_set(wfset, secpath, outpath, attributes = []):

	major, minor, servicepack, build = get_micromine_version()
	if  major < 18:
		secpath = secpath
	else:	
		secpath = os.path.splitext(secpath)[0]

	GenstrsFromSlicedWf_FormSet3 = MMpy.FormSet("GENSTRS_FROM_SLICED_WF","16.0.875.2")
	if attributes:
		DataGrid0 = MMpy.DataGrid(2,1)
		DataGrid0.set_column_info(0,1,7)
		DataGrid0.set_column_info(1,2,4)
		for i, (attr, field) in enumerate(attributes):
			DataGrid0.set_row(i, [attr, field])
		GenstrsFromSlicedWf_FormSet3.set_field("GRID", DataGrid0.serialise())
		GenstrsFromSlicedWf_FormSet3.set_field("TRANSFER_BOOL", "1")
	else:
		GenstrsFromSlicedWf_FormSet3.set_field("TRANSFER_BOOL","0")
	GenstrsFromSlicedWf_FormSet3.set_field("SC_FILE", secpath)
	GenstrsFromSlicedWf_FormSet3.set_field("SC_BOOL","1")
	GenstrsFromSlicedWf_FormSet3.set_field("TR_BOOL","0")
	GenstrsFromSlicedWf_FormSet3.set_field("ORT_TYPE","0")
	GenstrsFromSlicedWf_FormSet3.set_field("ORT_BOOL","0")
	GenstrsFromSlicedWf_FormSet3.set_field("JOIN_FLD","JOIN")
	GenstrsFromSlicedWf_FormSet3.set_field("Z_FLD","RL")
	GenstrsFromSlicedWf_FormSet3.set_field("NORTH_FLD","NORTH")
	GenstrsFromSlicedWf_FormSet3.set_field("EAST_FLD","EAST")
	GenstrsFromSlicedWf_FormSet3.set_field("FTYPE","2")
	GenstrsFromSlicedWf_FormSet3.set_field("OUT_FILE", outpath, MMpy.append_flag.none)
	GenstrsFromSlicedWf_FormSet3.set_field("OBJGR", wfset)
	GenstrsFromSlicedWf_FormSet3.set_field("NAME", "")
	GenstrsFromSlicedWf_FormSet3.set_field("WF_TYPE", "")
	GenstrsFromSlicedWf_FormSet3.set_field("SET_BOOL","1")
	GenstrsFromSlicedWf_FormSet3.set_field("SINGLE_BOOL","0")
	GenstrsFromSlicedWf_FormSet3.run()
# загрузка новых сечений
def load_new_sections(strpath, display_name):
	WsLoadStrings_FormSet6= MMpy.FormSet("WS_LOAD_STRINGS","16.0.875.2")
	WsLoadStrings_FormSet6.set_field("TRANSPARENCY","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_SHOW_LAST","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_SHOW_FIRST","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_SHOW_NTH","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_SHOW_ALL","1")
	WsLoadStrings_FormSet6.set_field("ANGLE_FORMAT","1")
	WsLoadStrings_FormSet6.set_field("PLAN","0")
	WsLoadStrings_FormSet6.set_field("CHECK_ANGLE_FIELD","0")
	WsLoadStrings_FormSet6.set_field("DISPLAY","0")
	WsLoadStrings_FormSet6.set_field("POLYGON_COLOUR",MMpy.Colour(0,0,0).serialise())
	WsLoadStrings_FormSet6.set_field("FIELD_ANGLE","0")
	WsLoadStrings_FormSet6.set_field("CHECK","1")
	WsLoadStrings_FormSet6.set_field("FIELD2","")		# ММ_ГГ
	WsLoadStrings_FormSet6.set_field("SEGMENT_SHOW_FIELD","0")
	WsLoadStrings_FormSet6.set_field("POINT_SHOW_FIELD","0")
	WsLoadStrings_FormSet6.set_field("POINT_SHOW_LIST_BOOL","0")
	WsLoadStrings_FormSet6.set_field("GRADIENT_FORMAT","0")
	WsLoadStrings_FormSet6.set_field("GRADIENT","0")
	WsLoadStrings_FormSet6.set_field("SINGLE_POINTS","0")
	WsLoadStrings_FormSet6.set_field("LABEL_FONT","0=0=0=0=3=2=1=34FFFF1.3=1=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FT=10=2=3 Tahoma")
	WsLoadStrings_FormSet6.set_field("FIELD1","Заголовок")
	WsLoadStrings_FormSet6.set_field("HATCH",MMpy.Hatch(MMpy.Colour(0,0,0),MMpy.Colour(255,255,255),MMpy.Colour(0,0,0),False,MMpy.LineType.solid,13,0,"MM Hatch Patterns 1").serialise())
	WsLoadStrings_FormSet6.set_field("POLYGONS_BOOL","0")
	WsLoadStrings_FormSet6.set_field("HATCH_SET","1")
	WsLoadStrings_FormSet6.set_field("HATCH_FIELD","")
	WsLoadStrings_FormSet6.set_field("POINT_SHOW_LAST","0")
	WsLoadStrings_FormSet6.set_field("POINT_SHOW_FIRST","0")
	WsLoadStrings_FormSet6.set_field("POINT_SHOW_ENDPOINTS","0")
	WsLoadStrings_FormSet6.set_field("POINT_SHOW_NTH","0")
	WsLoadStrings_FormSet6.set_field("POINT_SHOW_ALL","1")
	WsLoadStrings_FormSet6.set_field("ON","0")
	WsLoadStrings_FormSet6.set_field("BELOW","0")
	WsLoadStrings_FormSet6.set_field("ABOVE","1")
	WsLoadStrings_FormSet6.set_field("POINT_COLOUR",MMpy.Colour(0,0,100).serialise())
	WsLoadStrings_FormSet6.set_field("SEGMENT_COLOUR",MMpy.Colour(0,0,100).serialise())
	WsLoadStrings_FormSet6.set_field("SEGMENT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet6.set_field("POINT_USE_CUSTOM_COLOUR","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet6.set_field("POINT_FONT","0=0=0=0=0=0=0=0FFFF8=0=2147483647=2147483647=2147483647=0=0=TT=(0,0,-1)(0,1,0)=FF=5=2=3 Arial")
	WsLoadStrings_FormSet6.set_field("INCLINATION_FORMAT","1")
	WsLoadStrings_FormSet6.set_field("BEARING_FORMAT","0")
	WsLoadStrings_FormSet6.set_field("SEGMENT_LENGTH","0")
	WsLoadStrings_FormSet6.set_field("INCLINATION","0")
	WsLoadStrings_FormSet6.set_field("BEARING","0")
	WsLoadStrings_FormSet6.set_field("CUM_LENGTH","0")
	WsLoadStrings_FormSet6.set_field("INT_ANGLE","0")
	WsLoadStrings_FormSet6.set_field("SEQ","0")
	WsLoadStrings_FormSet6.set_field("Z","0")
	WsLoadStrings_FormSet6.set_field("Y","0")
	WsLoadStrings_FormSet6.set_field("X","0")
	WsLoadStrings_FormSet6.set_field("SEGMENTS","0")
	WsLoadStrings_FormSet6.set_field("LABEL_POINTS","0")
	WsLoadStrings_FormSet6.set_field("SHOW_POINTS","0")
	WsLoadStrings_FormSet6.set_field("LINE_TYPE","1")
	WsLoadStrings_FormSet6.set_field("JOIN_VARB","JOIN")
	WsLoadStrings_FormSet6.set_field("STRING_VARB","STRING")
	WsLoadStrings_FormSet6.set_field("THICK","23")
	WsLoadStrings_FormSet6.set_field("DEFAULT_COLOUR",MMpy.Colour(255,0,0).serialise())
	WsLoadStrings_FormSet6.set_field("Z_VARB","RL")
	WsLoadStrings_FormSet6.set_field("Y_VARB","NORTH")
	WsLoadStrings_FormSet6.set_field("X_VARB","EAST")
	WsLoadStrings_FormSet6.set_field("FILTER","0")
	WsLoadStrings_FormSet6.set_field("FILE_TYPE","2")
	WsLoadStrings_FormSet6.set_field("WSSTR_FILE", strpath)
	WsLoadStrings_FormSet6.run(display_name)

class SvyUpdateGraphicDialog(BaseDialog):
	def __init__(self, *args, **kw):
		super().__init__(*args, **kw)

		self.setWindowTitle("Параметры")

		self.__createVariables()
		self.__createWidgets()
		self.__gridWidgets()
		self.__createBindings()

	def __createVariables(self):
		self.__newSectionsFormsetId = None
		self.__cancelled = True
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout(self)

		self.layoutNewSectionFormset = QtGui.QFormLayout()
		self.labelNewSectionsFormsetId = QtGui.QLabel(self)
		self.labelNewSectionsFormsetId.setText("Форма пополненных сечений")
		self.labelNewSectionsFormsetId.setMinimumWidth(150)
		self.labelNewSectionsFormsetId.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

		self.lineEditNewSectionsFormsetId = QtGui.QLineEdit(self)
		self.lineEditNewSectionsFormsetId.setMinimumWidth(100)
		self.lineEditNewSectionsFormsetId.setText("69")
		self.lineEditNewSectionsFormsetId.setValidator(IntValidator())

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)

	def __gridWidgets(self):
		self.layoutMain.addLayout(self.layoutNewSectionFormset)
		self.layoutNewSectionFormset.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelNewSectionsFormsetId)
		self.layoutNewSectionFormset.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditNewSectionsFormsetId)
		self.layoutMain.addWidget(self.buttonBox)

	def __createBindings(self):
		self.lineEditNewSectionsFormsetId.mouseDoubleClickEvent = lambda e: self.selectFormset()
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.__save)

	def selectFormset(self):
		cmdId = 486

		try:
			tree = TreeMmFormsetBrowserDialog(self)
			tree.readFormsets(cmdId)
			tree.exec()
		except Exception as e:
			qtmsgbox.show_error(self, "Ошибка", "Невозможно считать наборы форм. %s" % str(e))
			return

		self.__newSectionsFormsetId = tree.result
		txtSetId = "" if self.__newSectionsFormsetId is None else str(self.__newSectionsFormsetId)
		self.lineEditNewSectionsFormsetId.setText(txtSetId)
		return self.__newSectionsFormsetId

	def newSectionsFormsetId(self):
		return self.__newSectionsFormsetId

	def cancelled(self):
		return self.__cancelled
	def __save(self):
		setId = self.lineEditNewSectionsFormsetId.text()
		self.__newSectionsFormsetId = int(setId) if setId else None
		self.__cancelled = False
		self.close()

class SvyUpdateGraphic(ProcessWindow):
	def __init__(self, *args, **kw):
		super(SvyUpdateGraphic, self).__init__(*args, **kw)

	def check(self):
		modex = self.main_app.mainWidget.modelExplorer

		# проверяем среди выбранных каркасов наличие маркшейдерских каркасов
		tridbItems = tuple(filter(lambda it: it.itemCategory() == ITEM_CATEGORY_SVY, self.selectedItems()[ITEM_TRIDB]))
		if not tridbItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите маркшейдерские каркасы")
			for i in range(modex.explorer.tabBar().count()):
				tab = modex.explorer.widget(i)
				if tab == modex.svyTreeModel:
					modex.explorer.setCurrentIndex(i)
					break
			return
		#
		secItems = self.selectedItems()[ITEM_SECTIONPACK] + self.selectedItems()[ITEM_SECTION]
		if not secItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите разрезы")
			for i in range(modex.explorer.tabBar().count()):
				tab = modex.explorer.widget(i)
				if tab == modex.secTreeModel:
					modex.explorer.setCurrentIndex(i)
					break
			return
		#
		strItems = self.selectedItems()[ITEM_STRING]
		if not strItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите файл стрингов")
			return

		#
		for strItem in strItems:
			strPath = strItem.filePath()
			lockStrPath = strPath + ".lock"
			if os.path.exists(lockStrPath):
				qtmsgbox.show_error(self.main_app, "Ошибка", "Файл '%s' используется пользователем '%s'" % (strPath, get_file_owner(lockStrPath)))
				return
		
		return True

	# подготовка к запуску
	def prepare(self):
		if not super(SvyUpdateGraphic, self).prepare():
			return

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	def run(self):
		if not super(SvyUpdateGraphic, self).run():
			return

		prepareDialog = SvyUpdateGraphicDialog(self.main_app)
		prepareDialog.exec()
		if prepareDialog.cancelled():
			return
		newSectionsFormsetId = prepareDialog.newSectionsFormsetId()

		progress = self.main_app.mainWidget.progressBar

		tridbItems = [it for it in self.selectedItems()[ITEM_TRIDB] if it.itemCategory() == ITEM_CATEGORY_SVY]
		strItems = self.selectedItems()[ITEM_STRING]
		annItems = self.selectedItems()[ITEM_ANNOTATION]
		secItems = self.selectedItems()[ITEM_SECTIONPACK]

		netTridbPathsDict = self.getTridbPathsDict(tridbItems, copy = False)
		netStrPaths = [it.filePath() for it in strItems]
		netAnnPaths = [it.filePath() for it in annItems]

		locStrPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "SLICES.STR")
		locSecPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "SECTIONS.DAT")

		### =================================================================================== ###
		### ============================= СОЗДАНИЕ НАБОРА КАРКАСОВ ============================ ###
		### =================================================================================== ###

		wfdata = []
		for tripath in netTridbPathsDict:
			for wfname in netTridbPathsDict[tripath]:
				wfdata.append([tripath, DEFAULT_ATTR_NAME, wfname])
		wfset = create_wireframe_set(wfdata)

		### =================================================================================== ###
		### ================================= НАРЕЗКА КАРКАСОВ ================================ ###
		### =================================================================================== ###

		progress.setText("Создание контрольного файла разрезов")
		secstruct = ('NAME:C:128:0', 'X_CENTRE:R:8:3', 'Y_CENTRE:R:8:3', 'Z_CENTRE:R:8:3', 'LENGTH:R:8:3', 'TOWARDS:R:8:3',
						'AWAY:R:8:3', 'AZIMUTH:R:8:3', 'INCLINATION:R:8:3', 'ROLL:R:8:3', 'STEP:R:8:3')

		secdata = []
		for secItem in secItems:
			secpath = secItem.filePath()
			f = MicromineFile()
			if not f.open(secpath):
				raise Exception("Невозможно открыть файл '%s'" % secpath)

			data = f.read()
			if secItem.childCount() == 0:
				secdata.extend(data)
			else:
				secnames = [secItem.child(i).text(0) for i in range(secItem.childCount())]
				secdata.extend(list(filter(lambda x: x[0] in secnames, data)))
			f.close()
		#
		MicromineFile.write(locSecPath, secdata, structure = secstruct)

		progress.setText("Нарезка каркасов")
		svy_user_attributes = list(SVY_USER_ATTRIBUTES)
		svy_user_attributes.append(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())
		wfattrs = [DEFAULT_ATTR_NAME, DEFAULT_ATTR_TITLE, DEFAULT_ATTR_CODE] + svy_user_attributes
		attributes = list(zip(wfattrs, wfattrs))
		try:					slice_wireframe_set(wfset, locSecPath, locStrPath, attributes)
		except Exception as e:	raise Exception("Невозможно нарезать каркасы. %s" % str(e))

		### =================================================================================== ###
		### =========================== ОБЪЕДИНЕНИЕ С ФАЙЛОМ ГРАФИКИ ========================== ###
		### =================================================================================== ###

		if not os.path.exists(locStrPath):
			raise Exception("Не было создано ни одного сечения")

		floc = MicromineFile()
		if not floc.open(locStrPath):
			raise Exception("Невозможно открыть файл '%s'" % locStrPath)

		#
		idxTitle = floc.get_field_id(DEFAULT_ATTR_TITLE)
		idxName = floc.get_field_id(DEFAULT_ATTR_NAME)
		locstruct = floc.structure
		locstruct.set_field_width(idxTitle, locstruct.get_field_width(idxTitle) + locstruct.get_field_width(idxName) + 1)
		floc.structure = locstruct

		#
		for i in range(floc.records_count):
			#floc.set_field_value(idxTitle, i+1, ":".join([floc.get_str_field_value(idxTitle, i+1), floc.get_str_field_value(idxName, i+1)]))
			floc.set_field_value(idxTitle, i+1, floc.get_str_field_value(idxTitle, i+1))

		#
		locstruct = floc.structure

		# Корректировка структуры файла сечений CODE -> КОД, TITLE -> ЗАГОЛОВОК
		idxCode = floc.get_field_id(DEFAULT_ATTR_CODE)
		idxSvyStrCode = floc.get_field_id(SVY_STR_ATTR_CODE)
		idxSvyStrTitle = floc.get_field_id(SVY_STR_ATTR_TITLE)
		#
		if idxCode >= 0 and idxSvyStrCode < 0:
			locstruct.set_field_name(idxCode, SVY_STR_ATTR_CODE)
		elif idxCode >= 0 and idxSvyStrCode >= 0:
				raise Exception ("Файл %s содержит одновременно поля %s и %s. Ожидается только %s."
								 "Автоматическое переименование невозможно" % (locStrPath, SVY_STR_ATTR_CODE, DEFAULT_ATTR_CODE, SVY_STR_ATTR_CODE))
		elif idxSvyStrCode < 0:
			raise Exception ("Не удаётся добавить в файл сечений поле %s" % SVY_STR_ATTR_CODE)
		#
		if idxTitle >= 0 and idxSvyStrTitle < 0:
			locstruct.set_field_name(idxTitle, SVY_STR_ATTR_TITLE)
		elif idxTitle >= 0 and idxSvyStrTitle >= 0:
			raise Exception ("Файл %s содержит одновременно поля %s и %s. Ожидается только %s."
							 "Автоматическое переименование невозможно" % (locStrPath, SVY_STR_ATTR_TITLE, DEFAULT_ATTR_TITLE, SVY_STR_ATTR_TITLE))
		elif idxSvyStrTitle < 0:
			raise Exception ("Не удаётся добавить в файл сечений поле %s" % SVY_STR_ATTR_TITLE)
		#
		floc.structure = locstruct
		locstruct = floc.structure
		locstruct.delete_field(idxName)
		floc.structure = locstruct
		floc.close()

		#
		for strItem, netStrPath in zip(strItems, netStrPaths):
			fnet = MicromineFile()
			floc = MicromineFile()

			if not fnet.open(netStrPath):	raise Exception("Невозможно открыть файл '%s'" % netStrPath)
			if not floc.open(locStrPath):	raise Exception("Невозможно открыть файл '%s'" % locStrPath)

			# Корректировка структуры файла марк графики CODE -> КОД, TITLE -> ЗАГОЛОВОК
			netstruct = fnet.structure
			idxCode = fnet.get_field_id(DEFAULT_ATTR_CODE)
			idxSvyStrCode = fnet.get_field_id(SVY_STR_ATTR_CODE)
			idxTitle = fnet.get_field_id(DEFAULT_ATTR_TITLE)
			idxSvyStrTitle = fnet.get_field_id(SVY_STR_ATTR_TITLE)
			#
			if idxCode >= 0 and idxSvyStrCode < 0:
				netstruct.set_field_name(idxCode, SVY_STR_ATTR_CODE)
			#	
			if idxTitle >= 0 and idxSvyStrTitle < 0:
				netstruct.set_field_name(idxTitle, SVY_STR_ATTR_TITLE)
			
			#
			fnet.structure	= netstruct 

			# проверка соответствия структур файлов
			netstruct, locstruct = fnet.structure, floc.structure
			netheader, locheader = fnet.header, floc.header
			for i, name in enumerate(locheader):
				if not name in netheader:
					netstruct.add_field(name, locstruct.get_field_type(i), locstruct.get_field_width(i), locstruct.get_field_precision(i))
			fnet.structure = netstruct

			MMFLD_JOIN = "JOIN"

			inetjoin, ilocjoin = fnet.get_field_id(MMFLD_JOIN), floc.get_field_id(MMFLD_JOIN)
			# определение максимального значения поля JOIN
			joins = [row[0] for row in fnet.read(MMFLD_JOIN)]
			nextjoin = max(joins) if joins else 1

			for i in range(floc.records_count):
				fnet.add_record()
				for j, colname in enumerate(locheader):
					if colname == MMFLD_JOIN:
						fnet.set_field_value(fnet.get_field_id(colname), fnet.records_count, floc.get_num_field_value(j, i+1) + nextjoin)
					else:
						fnet.set_field_value(fnet.get_field_id(colname), fnet.records_count, floc.get_str_field_value(j, i+1))
			fnet.close()
			floc.close()

			setId = strItem.formsetId()

			#
			try:					loadStrings(netStrPath, os.path.basename(netStrPath), setId)
			except Exception as e:	qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно загрузить '%s'. %s" % (netStrPath, str(e)))

		### =================================================================================== ###
		### ================================= ЗАГРУЗКА В ВИЗЕКС =============================== ###
		### =================================================================================== ###

		try:
			if newSectionsFormsetId is None:
				load_new_sections(locStrPath, "Пополненные сечения")
			else:
				loadStrings(locStrPath, "Пополненные сечения", newSectionsFormsetId)
		except Exception as e:	qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно загрузить '%s'. %s" % (locStrPath, str(e)))

		if annItems:
			for annItem in annItems:
				annpath = annItem.filePath()
				try:					defaultLoadAnnotation(annpath, os.path.basename(annpath))
				except Exception as e:	qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно загрузить '%s'. %s" % (annpath, str(e)))

		qtmsgbox.show_information(self.main_app, "Выполнено", "Маркшейдерская графика успешно пополнена")