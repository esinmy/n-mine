try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.ConcreteByMineDialog import ConcreteByMineDialog, AssignedDataDialog, AssignedAllDataDialog
from gui.qt.dialogs.BlockModelPreviewDialog import BlockModelPreviewDialog, bmDataPreviewReport, createBmPreviewListTreeItem
from gui.qt.ProcessWindow import ProcessWindow
from gui.qt.modes.WireframeAttributeValidation import SvyWireframeAttributeValidation
from gui.qt.modes.svy.SvyAssignBm import AssignBm, SvyAssignBm

from mm.mmutils import Tridb, MicromineFile, SvyWireframeFormSet
from mm.formsets import create_wireframe_set, loadTriangulationSet, loadBlockModel

from utils.nnutil import prev_date, date_to_fieldname
from utils.blockmodel import BlockModel
from utils.constants import *
from utils.osutil import get_file_owner, get_temporary_prefix, get_current_time
from utils.validation import is_blank

from datetime import datetime
from itertools import groupby
import os
from copy import deepcopy
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyRestoreGeology(AssignBm):
	def __init__(self, *args, **kw):
		super(SvyRestoreGeology, self).__init__(*args, **kw)

	def transferBlockModelData(self, extracted):
		return True

	def restoreFields(self, extracted, restorePrevDate = False):
		return super().restoreFields(extracted, restorePrevDate = True)

	def displayData(self, extracted = False):
		progress = self.main_app.mainWidget.progressBar
		progress.setText("Загрузка объектов")
		#
		userFormsets = list(zip(*self.main_app.settings.getValue(ITEM_CATEGORY_FORMSET_DICT[ITEM_CATEGORY_SVY])))[1]
		userFormsets = list([int(setId) if setId else None for setId in userFormsets])
		formsetsDict = dict(zip(MMTYPE_LIST, userFormsets))
		#
		locBmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		# загрузка блочной модели
		setId = formsetsDict[MMTYPE_BM]
		for i, (netBmPath, locBmPath) in enumerate(zip(self._netBmPathList, locBmPathList)):
			if not os.path.exists(locBmPath):
				continue
			netBm = BlockModel(netBmPath, self.main_app)
			locBm = BlockModel(locBmPath, self.main_app)
			locBm.load(netBm.name, setId)
		# загрузка каркасов
		setId = formsetsDict[MMTYPE_TRIDB]
		for wfset in self._wireframe_sets:
			loadTriangulationSet(wfset, wfset.displayName(), setId)	

	def loadToVizex(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()

		extracted, restoreFields, dataTransfer = True, True, False
		try:
			# перенос данных из системных полей
			if not self.prepareBlockModels(extracted, restoreFields, dataTransfer):
				progress.setText(oldProgressText)
				return False
			# удаление системных полей
			if not self.removeBlockModelSystemFields(extracted):
				progress.setText(oldProgressText)
				return False
			# загрузка
			try:
				self.displayData(extracted)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно загрузить данные. %s" % str(e))
				progress.setText(oldProgressText)
				return False
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "ZHOPA", str(e))
		progress.setText(oldProgressText)
		return True

	def updateOnServer(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		extracted, restoreFields, dataTransfer = False, True, False
		# считывание выбранных данных
		(includeValues, restoreMiningValues, restoreGeologyValues, simulateConcreteValues), includeFields = self.selectedKeysAndFields()
		if len(includeValues) == 0:
			progress.setText(oldProgressText)
			return False
		# перенос данных из системных полей
		try:
			self.prepareBlockModels(extracted, restoreFields, dataTransfer)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", str(e))
			progress.setText(oldProgressText)
			return False
		# удаление системных полей
		if not self.removeBlockModelSystemFields(extracted):
			return False
		# загрузка
		try:
			self.displayData(extracted)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно загрузить данные. %s" % str(e))
			progress.setText(oldProgressText)
			return False
		# копирование файлов на сервер
		if not self.copyBlockModelsToServer():
			return False
		#
		progress.setText(oldProgressText)
		#
		qtmsgbox.show_information(self._bmPreviewDialog, "Выполнено", "Блочные модели успешно обновлены и скопированы на сервер")
		return True

	def check(self):
		bmItems = self.selectedItems()[ITEM_BM]
		if not bmItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите блочные модели")
			return

		# проверяем среди выбранных каркасов наличие маркшейдерских каркасов
		tridbItems = self.selectedItems()[ITEM_TRIDB]
		if not tridbItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите каркасы")
			return

		# проверяем, что все блочные модели имеют метаданные
		sysdate = datetime.now()
		currdates = []
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()

			# проверка наличия файла блокировки
			if self.lockFileExists(netBmPath):
				lockpath = self.getLockPath(netBmPath)
				msg = "Блочная модель используется другим пользователем %s." % get_file_owner(lockpath)
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			bm = BlockModel(netBmPath, self.main_app)
			# проверка наличия метаданных блочных моделей
			if not bm.has_metadata:
				msg = "Блочная модель '%s' не имеет метаданных. Обратитесь к геологу" % bm.name
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			# проверка на соответствие метаданных содержанию блочной модели
			metadate = bm.metadata.current_date
			bmdate = bm.blockmodel_date
			if bmdate.year != metadate.year or bmdate.month != metadate.month:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Метаданные не соответствуют содержанию блочной модели '%s'. Обратитесь к геологу." % bm.name)
				return

			currdates.append(metadate)

		currdates = list(sorted(set(currdates)))
		if len(currdates) > 1:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выбранные блочные модели переведены в разные месяца. Выберите блочные модели, переведенные в один и тот же месяц")
			return
		elif (currdates[0].year == sysdate.year and currdates[0].month != sysdate.month) or (currdates[0].year < sysdate.year):
			if not qtmsgbox.show_question(self.main_app, "Внимание", "Состояние блочных моделей не соответствует текущей календарной дате. Продолжить?"):
				return
		#
		return True

	# подготовка к запуску
	def prepare(self):
		if not super(SvyRestoreGeology, self).prepare():
			return

		if not self.check():
			return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	def run(self):
		if not super(SvyRestoreGeology, self).run():
			return

		progress = self.main_app.mainWidget.progressBar

		bmItems = self.selectedItems()[ITEM_BM]
		# считывание текущей даты
		netBm = BlockModel(bmItems[0].filePath(), self.main_app)
		self._bmCurrentDate = netBm.metadata.current_date
		BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)
		#
		tridbItems = self.selectedItems()[ITEM_TRIDB]
		netTridbPathsDict = self.getTridbPathsDict(tridbItems, copy = False)

		subxsize = self.main_app.settings.getValue(ID_SUBXSVY)
		subysize = self.main_app.settings.getValue(ID_SUBYSVY)
		subzsize = self.main_app.settings.getValue(ID_SUBZSVY)
		#
		elemFields = self.getIdwElements()
		elemUnits = self.getIdwUnits()
		#
		concDensity = float(self.main_app.settings.getValue(ID_CONCDENS))

		oreTypeCodes = [row[0] for row in self.getGeoCodes()]

		### =================================================================================== ###
		### ============================== КОПИРОВАНИЕ КАРКАСОВ =============================== ###
		### =================================================================================== ###

		progress.setRange(0,0)
		progress.setText("Копирование каркасов")
		try:					loctripaths = self.getTridbPathsDict(tridbItems, copy = True)
		except Exception as e:	raise Exception("Невозможно скопировать каркасы и считать атрибуты. %s" % str(e))

		### =================================================================================== ###
		### =========================== СОЗДАНИЕ НАБОРА КАРКАСОВ ============================== ###
		### =================================================================================== ###

		for loctripath in loctripaths:
			tridb = Tridb(loctripath)
			basename = os.path.basename(loctripath)[len(get_temporary_prefix()):]
			tridbName = basename[:basename.rfind(".")]
			for wfName in tridb.wireframe_names:
				wireframe = tridb[wfName]
				wireframe.set_default_attribute(DEFAULT_ATTR_TITLE, tridbName)
			tridb.close()
		
		progress.setText("Создание набора каркасов")
		try:					self._wireframe_sets = self.generateCustomWireframeSets(loctripaths)
		except Exception as e:	raise Exception("Невозможно подготовить данные для набора каркаcов. %s" % str(e))

		progress.setText("Инициализация присваивания атрибутов")
		#
		for wfsetIndex, wfset in enumerate(self._wireframe_sets):
			#
			types = ("1", "0", "0")
			systemValues = ("Y", DEFAULT_ATTR_TITLE, DEFAULT_ATTR_NAME)
			systemFields = (BMFLD_SYS_FIELD, BMFLD_SYS_MINENAME, BMFLD_SYS_INTERVAL)

			# параметры присваивания атрибутов
			assing_params = {}
			assing_params["attributes"] =  [[atype, systemValue, systemField] for atype, systemValue, systemField in zip(types, systemValues, systemFields)]
			assing_params["sub_xsize"] = subxsize
			assing_params["sub_ysize"] = subysize
			assing_params["sub_zsize"] = subzsize
			assing_params["overwrite_bool"] = True
			assing_params["purge_bool"] = True
			assing_params["wireframe_set"] = wfset

			#
			assignedData = []
			for bmItemIndex, bmItem in enumerate(bmItems):
				netBmPath = bmItem.filePath()
				netBm = BlockModel(netBmPath, self.main_app)

				### =================================================================================== ###
				### ============================ КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ =========================== ###
				### =================================================================================== ###

				locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % netBm.name)
				locBmExtractedPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_PART%s.DAT" % (netBm.name, str(bmItemIndex+1).zfill(3)))
				#
				self._netBmPathList.append(netBmPath)
				self._locBmPathList.append(locBmPath)
				self._locBmExtractedPathList.append(locBmExtractedPath)
				#
				progress.setText("Копирование блочной модели")
				try:					self.copy_file(netBmPath, locBmPath)
				except Exception as e:	raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
			
				### =================================================================================== ###
				### ============================= ПРИСВАИВАНИЕ АТРИБУТОВ ============================== ###
				### =================================================================================== ###

				progress.setText("Присваивание атрибутов")
				loc_bm = BlockModel(locBmPath, self.main_app)
				try:					loc_bm.assign_wireframes(assing_params)
				except Exception as e:	raise Exception("Невозможно присвоить атрибуты. %s" % str(e))

				# проверка наличия столбца данных за текущий и предыдущий месяцы
				f = MicromineFile()
				if not f.open(locBmPath):			raise Exception("Невозможно открыть файл %s" % locBmPath)
				if not BMFLD_CURRDATE in f.header:	raise Exception("Блочная модель '%s' не содержит поле '%s'" % (netBm.name, BMFLD_CURRDATE))
				bmHeader = f.header
				idxCurrDate = bmHeader.index(BMFLD_CURRDATE)
				f.close()

				### =================================================================================== ###
				### ========================== ИЗВЛЕЧЕНИЕ ПРИСВОЕННЫХ ДАННЫХ ========================== ###
				### =================================================================================== ###

				progress.setText("Извлечение присвоенных данных")
				try:					bmData = self.extractBlockModelData(locBmPath, [BMFLD_SYS_FIELD], ["Y"])
				except Exception as e:	raise Exception("Невозможно извлечь присвоенные данные. %s" % str(e))
				_bmData = list(filter(lambda bmRow: bmRow[idxCurrDate] not in oreTypeCodes, bmData))
				isUseDynamicDensity = netBm.metadata.is_use_dynamic_density
				for  i, row in enumerate(_bmData):
					_bmData[i].append(isUseDynamicDensity) 
				assignedData.extend(_bmData)

			bmHeader.append('isUseDynamicDensity')

			### =================================================================================== ###
			### ============================= ОТЧЕТ ПО БЛОЧНЫМ МОДЕЛЯМ ============================ ###
			### =================================================================================== ###

			progress.setText("Создание отчета")
			#
			groupFields = (BMFLD_SYS_MINENAME, BMFLD_SYS_INTERVAL)
			groupIndexes = tuple(bmHeader.index(keyField) for keyField in groupFields)
			assignedData.sort(key = lambda x: tuple(x[index] for index in groupIndexes))
			#
			# инициализация диалога
			canSeeGrades = self.main_app.userRole() in [ID_USERGEO, ID_USERADMIN]

			self._bmPreviewDialog = BlockModelPreviewDialog(self)
			# self._bmPreviewDialog.resize(QtCore.QSize(1400, 600))
			self._bmPreviewDialog.setCanSeeGrades(canSeeGrades)
			self._bmPreviewDialog.setNeedsIncludeInReport(False)
			self._bmPreviewDialog.setNeedsRestoreGeology(True)
			self._bmPreviewDialog.setFldCurrdate(BMFLD_CURRDATE)
			self._bmPreviewDialog.setGroupFields(groupFields)
			# настройка функциональности кнопок
			self._bmPreviewDialog.buttonLoad.clicked.connect(self.loadToVizex)
			self._bmPreviewDialog.buttonUpdate.clicked.connect(self.updateOnServer)
			# настройка видимости кнопок
			self._bmPreviewDialog.buttonLoad.setVisible(True)
			self._bmPreviewDialog.buttonContinue.setVisible(False)
			self._bmPreviewDialog.buttonCancel.setVisible(True)
			self._bmPreviewDialog.paramsFrame.setVisible(False)
			self._bmPreviewDialog.buttonExport.setVisible(False)
			self._bmPreviewDialog.buttonUpdate.setVisible(True)
			self._bmPreviewDialog.buttonSave.setVisible(False)
		

			#
			reportParams = dict(
				bmData = assignedData,
				bmHeader = bmHeader,
				fieldCurrDate = BMFLD_CURRDATE,
				concDensity = concDensity,
				elemFields = elemFields,
				elemUnits = elemUnits,
				geoCodes = self.main_app.settings.getValue(ID_GEOCODES),
				svyCatsAndCodes = self.main_app.settings.getValue(ID_SVYCAT)
			)
			sysKeys = []
			for group, groupValues in groupby(assignedData, key = lambda x: tuple(x[index] for index in groupIndexes)):
				sysKey = tuple(group)
				bmData = tuple(groupValues)

				reportParams["bmData"] = bmData
				reportParams["bmHeader"] = bmHeader

				reportData, reportHeader = bmDataPreviewReport(**reportParams)
				listItem = createBmPreviewListTreeItem(":".join(sysKey), ITEM_WIREFRAME, ITEM_CATEGORY_SVY)
				listItem.setKey(sysKey)
				self._bmPreviewDialog.addBlockModelPreview(listItem, reportData, reportHeader)
			# включаем все записи
			for i in range(self._bmPreviewDialog.bmPreviewStacked.count()):
				self._bmPreviewDialog.bmPreviewStacked.widget(i).blockModelView.includeInReportAll()

			#
			progress.setText("Выбор данных для занесения в блочную модель")
			self._bmPreviewDialog.exec()

			if self._bmPreviewDialog.is_cancelled:
				return
			#
			if not self.prepareBlockModels(extracted = False, restoreFields = True, dataTransfer = False) \
					or not self.removeBlockModelSystemFields(extracted = False) \
					or not self.copyFilesToServer():
				return
		#
		progress.setText("")
		progress.setRange(0,100)

		return False


