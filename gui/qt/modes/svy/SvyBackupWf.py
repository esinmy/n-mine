from PyQt4 import QtGui, QtCore
import os
from datetime import datetime

from gui.qt.ProcessWindow import ProcessWindow
import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BaseDialog import BaseDialog

from mm.mmutils import Tridb
from mm.formsets import copy_wireframe

from utils.nnutil import is_valid_short_date
from utils.osutil import is_blank, is_number, in_range
from utils.constants import *
from utils.logger import NLogger

from collections import namedtuple

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

NAN = "НЕ ЧИСЛО"
OUT_OF_RANGE = "ВНЕ ДИАПАЗОНА"
OUT_OF_LIST = "НЕВЕРНЫЙ КОД"
EMPTY = "ПУСТОЕ"
MISS_ATTR = "ОТСУТСТВУЕТ АТРИБУТ"
OUT_OF_GROUP = "НЕВЕРНЫЙ КОД ДЛЯ %s"
INVALID_FORMAT = "НЕВЕРНЫЙ ФОРМАТ"


class SvyBackupWf(ProcessWindow):
	def __init__(self, *args, **kw):
		super(SvyBackupWf, self).__init__(*args, **kw)

	def check(self, tridbItems):
		TridbOk = namedtuple('TridbOk', 'path name wf')
		tridbs_with_many_wfs = []
		tridbs_with_no_wfs = []
		tridbs_ok = []

		for item in tridbItems:
			filepath = item.filePath()
			wfs = list(it for it in item.iterChildren())
			if not wfs:
				tridb = Tridb(filepath)
				implicit_wfs = tridb.wireframe_names
				if len(implicit_wfs) > 1:
					tridbs_with_many_wfs.append(item.text(0))
				elif not implicit_wfs:
					tridbs_with_no_wfs.append(item.text(0))
				elif len(implicit_wfs) == 1:
					tridbs_ok.append(TridbOk(filepath, item.text(0), implicit_wfs[0]))
			elif len(wfs) > 1:
				tridbs_with_many_wfs.append(item.text(0))
			elif len(wfs) == 1:
				tridbs_ok.append(TridbOk(filepath, item.text(0), wfs[0].name()))

		return tridbs_ok, tridbs_with_no_wfs, tridbs_with_many_wfs

	def get_wfs_no_dates(self, tridbs):
		no_dates = []
		for tridb in tridbs:
			srcpath = tridb.path
			srcname = tridb.wf
			tridb = Tridb(srcpath)
			try:        datetime.strptime(tridb[srcname].user_attribute(SVY_ATTR_DATE), "%Y-%m-%d")
			except: no_dates.append(tridb.name)
			tridb.close()
		return no_dates

	def exec(self, filelist=[]):
		self._report = []
		self._result = None
		#

		if filelist:
			for filepath in filelist:
				basename = os.path.basename(filepath)
				basename = basename[:basename.rfind(".")] if "." in basename else basename
				self.validate_file(filepath)
				QtCore.QCoreApplication.processEvents()
		else:
			selex = self.main_app.mainWidget.selectionExplorer
			tridbItems = list(it for it in selex.explorer.iterChildren() if it.isTridb())
			tridbs_ok, tridbs_with_no_wfs, tridbs_with_many_wfs = self.check(tridbItems)

		if tridbs_with_no_wfs or tridbs_with_many_wfs:
			if tridbs_with_no_wfs and tridbs_with_many_wfs:
				qtmsgbox.show_error(self, 'Ошибка', 'В следующих tridb нет ни одного слоя:\n{}\n'
													 'В следующих tridb больше одного слоя:\n{}\n'
													 'Процесс остановлен.'.
									format(tridbs_with_no_wfs, tridbs_with_many_wfs))
				return
			elif tridbs_with_no_wfs:
				qtmsgbox.show_error(self, 'Ошибка', 'В следующих tridb нет ни одного слоя:\n{}\n'
													 'Процесс остановлен.'.
									format(tridbs_with_no_wfs))
				return
			elif tridbs_with_many_wfs:
				qtmsgbox.show_error(self, 'Ошибка', 'В следующих tridb больше одного слоя:\n{}\n'
													 'Процесс остановлен.'.
									format(tridbs_with_many_wfs))
				return

		if not tridbs_ok:
			qtmsgbox.show_information(self, 'Внимание', 'Пожалуйста, укажите каркасы для фиксирования.')
			return

		wfs_no_dates = self.get_wfs_no_dates(tridbs_ok)
		if wfs_no_dates:
			qtmsgbox.show_error(self, 'Ошибка', 'Для следующих каркасов не удалось считать атрибут "ДАТА":\n{}\n'
												'Процесс остановлен.'.format(wfs_no_dates))
			return

		for tridb_ok in tridbs_ok:
			srcpath = tridb_ok.path
			srcname = tridb_ok.wf

			dstpath = tridb_ok.path.replace(self.main_app.settings.getValue(ID_SVY_MAINDIR), 'МАРК_МЕСЯЦ_ФИКС')

			if os.path.exists(dstpath):
				try:
					os.remove(dstpath)
				except Exception as e:
					raise Exception("Невозможно перезаписать файл '%s'. %s" % (dstpath, str(e)))

			tridb = Tridb(srcpath)
			dstname = '_'.join(tridb[tridb_ok.wf].user_attribute(SVY_ATTR_DATE).split('-')[:2])
			tridb.close()

			svy_user_attributes = list(SVY_USER_ATTRIBUTES)
			svy_user_attributes.append(self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper())

			if not os.path.exists(os.path.dirname(dstpath)):
				os.makedirs(os.path.dirname(dstpath), exist_ok=True)

			Tridb.create(dstpath, svy_user_attributes)
			copy_wireframe(srcpath, srcname, dstpath, dstname, copy_attributes=True)

		qtmsgbox.show_information(self.main_app, "Выполнено", "Маркшейдерские каркасы зафиксированы.")