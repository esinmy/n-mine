from PyQt4 import QtGui, QtCore
import os, zipfile
from datetime import datetime

from mm.mmutils import MicromineFile

from utils.constants import *
from utils.nnutil import next_date, prev_date, date_to_fieldname, fieldname_to_date
from utils.blockmodel import BlockModel
from utils.osutil import get_temporary_prefix

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import ProcessWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class GeoTransferBm(ProcessWindow):
	def __init__(self, *args, **kw):
		super(GeoTransferBm, self).__init__(*args, **kw)

	def __createVariables(self):
		self._bmdate = {}		# текущие даты блочных моделей
		self._bmjump = {}		# разрешить переводить блочную модель в будущее

	def check(self):
		self._bmdate = {}
		self._bmjump = {}

		transex = self.main_app.mainWidget.transferExplorer
		bmItems = list(filter(lambda it: it.isBlockModel() and transex.selectedDate(it), transex.explorer.iterChildren()))
		if not any(bmItems):
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите дату хотя бы для одной блочной модели")
			return

		# проверка, что все выбранные блочные модели существуют
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()
			if not os.path.exists(netBmPath):
				qtmsgbox.show_error(self.main_app, "Ошибка", "Блочная модель '%s' не существует" % netBmPath)
				return

		sysdate = datetime.now()
		# проверка соответствия метаданных и данных блочной модели
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()
			net_bm = BlockModel(netBmPath, self.main_app)
			
			metadate = net_bm.metadata.current_date
			bmdate = net_bm.blockmodel_date

			if bmdate.year != metadate.year or bmdate.month != metadate.month:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Метаданные не соответствуют содержанию блочной модели '%s'. Обновите блочную модель." % net_bm.name)
				return

			nextdate = transex.selectedDate(bmItem)
			msg = "Вы собираетесь перевести блочную модель '%s' в будущее. Работа с ней за более ранние месяцы будет невозможна. Продолжить?" % net_bm.name
			if nextdate.year > sysdate.year or (nextdate.year == sysdate.year and nextdate.month > sysdate.month):
				self._bmjump[netBmPath] = qtmsgbox.show_question(self.main_app, "Внимание", msg)

			self._bmdate[netBmPath] = bmdate

		return True

	def prepare(self):
		if not super(GeoTransferBm, self).prepare():
			return

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		finally:
			# Возврат к режиму "Загрузить данные" после завершения работы функции
			self.main_app.toolButtonDisplayData.click()
		self.after_run()

	def run(self):
		if not super(GeoTransferBm, self).run():
			return

		sysdate = date_to_fieldname(datetime.now())

		transex = self.main_app.mainWidget.transferExplorer
		bmItems = list(filter(lambda it: it.isBlockModel() and transex.selectedDate(it), transex.explorer.iterChildren()))
		progress = self.main_app.mainWidget.progressBar
		progress.setRange(0, 0)

		#
		concDensity = self.main_app.settings.getValue(ID_CONCDENS)
		concPurpose, concCategory = self.getSvyCategoriesAndCodes()[-2][0]

		# создание LCK файлов
		self.createLockFiles([bmItem.filePath() for bmItem in bmItems])
		
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()
			net_bm = BlockModel(netBmPath, self.main_app)

			currdate = date_to_fieldname(net_bm.metadata.current_date)
			prevdate = prev_date(currdate, ret = "str")
			selectedDate = transex.selectedDate(bmItem)
			nextdate = date_to_fieldname(transex.selectedDate(bmItem))

			locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_%s.DAT" % (currdate, net_bm.name))
			bkpBmPath = net_bm.backup_path
			zipBmPath = os.path.join(os.path.dirname(netBmPath), "%s_%s.zip" % (currdate, net_bm.name))

			if nextdate > sysdate and not self._bmjump[netBmPath]:
				continue

			### =================================================================================== ###
			### ============ СОЗДАНИЕ РЕЗЕРВНОЙ КОПИИ БЛОЧНОЙ МОДЕЛИ В СЕТЕВОМ ПРОЕКТЕ ============ ###
			### =================================================================================== ###

			progress.setText("Перевод %s. Создание резервной копии" % net_bm.name)
			err = self.exec_proc(self.copy_file, netBmPath, bkpBmPath)
			if err:						raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
			if self.is_cancelled:		return
			
			### =================================================================================== ###
			### =============== КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ ВО ВРЕМЕННУЮ ДИРЕКТОРИЮ ================ ###
			### =================================================================================== ###

			progress.setText("Перевод %s. Копирование" % net_bm.name)
			err = self.exec_proc(self.copy_file, netBmPath, locBmPath)
			if err:						raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
			if self.is_cancelled:		return

			### =================================================================================== ###
			### =========================== СОЗДАНИЕ АРХИВА БЛОЧНОЙ МОДЕЛИ ======================== ###
			### =================================================================================== ###

			progress.setText("Перевод %s. Архивирование" % net_bm.name)
			try:
				with zipfile.ZipFile(zipBmPath, "w", zipfile.ZIP_DEFLATED) as z:
					z.write(netBmPath, os.path.basename(netBmPath))
			except Exception as e:
				raise Exception("Невозможно создать архив блочной модели. %s" % str(e))

			### =================================================================================== ###
			###================================= ПЕРЕНОС ДАННЫХ =================================== ###
			### =================================================================================== ###

			f = MicromineFile()
			if not f.open(locBmPath):	raise Exception("Невозможно открыть файл '%s'" % locBmPath)
			# проверка наличия необходимых полей
			iprevdate = f.get_field_id(prevdate)
			icurrdate = f.get_field_id(currdate)
			icurrmined = f.get_field_id(BMFLD_CURRMINED)
			itotalmined = f.get_field_id(BMFLD_TOTALMINED)
			imaterial = f.get_field_id(BMFLD_MATERIAL)
			iindex = f.get_field_id(BMFLD_INDEX)
			iworktype = f.get_field_id(BMFLD_FACTWORK)
			idensity = f.get_field_id(BMFLD_DENSITY)

			if any(map(lambda x: x == -1, (iprevdate, icurrdate, icurrmined, itotalmined, imaterial, iindex, idensity, iworktype))):
				raise Exception("Блочная модель '%s' не содрежит всех необходимых полей")
			#
			for i in range(f.records_count):
				# переносим данные из текущего месяца в предыдущий
				f.set_field_value(iprevdate, i+1, f.get_str_field_value(icurrdate, i+1))
				# переносим информацию по горным работам текущего месяца
				currmined = f.get_str_field_value(icurrmined, i+1)
				if currmined:
					f.set_field_value(itotalmined, i+1, currmined)
					f.set_field_value(icurrmined, i+1, "")
				# переносим информацию по закладке
				if f.get_str_field_value(icurrdate, i+1) == concCategory:
					f.set_field_value(idensity, i+1, concDensity)
					f.set_field_value(imaterial, i+1, concCategory)
					f.set_field_value(iindex, i+1, f.get_str_field_value(iworktype, i+1))
				if i % 500 == 0:
					progress.setText("%s. Обработано %d/%d записей" % (net_bm.name, i+1, f.records_count))

			progress.setText("%s. Изменение структуры")
			# Переименовываем поля
			structure = f.structure
			structure.set_field_name(icurrdate, nextdate)
			structure.set_field_name(iprevdate, currdate)
			f.structure = structure
			#
			f.close()
			#
			if self.is_cancelled:		return

			### =================================================================================== ###
			### ======================== КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ НА СЕРВЕР ===================== ###
			### =================================================================================== ###

			progress.setText("%s. Копирование на сервер" % net_bm.name)
			err = self.exec_proc(self.copy_file, locBmPath, netBmPath)
			if err:						raise Exception("Невозможно скопировать файл %s. %s" % (locBmPath, err))
			if self.is_cancelled:		return

			net_bm.metadata.current_date = selectedDate
			net_bm.metadata.is_need_transfer = False
			net_bm.save()

		if len(bmItems) > 1:	msg = "Блочные модели успешно переведены"
		else:					msg = "Блочная модель успешно переведена"
		transex.update()
		progress.setText("")
		progress.setRange(0, 100)
		qtmsgbox.show_information(self.main_app, "Выполнено", msg)

