try:
	import MMpy
except:
	pass

from PyQt4 import QtGui, QtCore

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow
from gui.qt.widgets.TableDataCheckView import CustomDataCheckModel
from utils.qtutil import iterWidgetParents
from mm.formsets import create_wireframe_set

from mm.mmutils import MicromineFile

from utils.nnutil import prev_date, date_to_fieldname
from utils.blockmodel import BlockModel, assign_wireframes
from utils.constants import *
from utils.osutil import get_temporary_prefix

from datetime import datetime
import os, shutil
from utils.logger import NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class ButtonFilter(QtGui.QPushButton):
	def __init__(self, parent, column):
		super(ButtonFilter, self).__init__(parent)
		self._column = column
		self.setText("Выделить всё/\nСнять выделение")
		self.clicked.connect(self.selectAll)
	def event(self, event):
		if event.type() == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Tab:
			self.parent().filterEdit(self._column+1)
			return -1
		return QtGui.QPushButton.event(self, event)
	def selectAll(self):
		view = self.parent().parent()
		model = view.model()
		checkAll = not all(model.data(model.createIndex(irow, self._column))  for irow in range(model.rowCount()))
		if checkAll:
			for irow in range(model.rowCount()):
				model.setData(model.createIndex(irow, self._column), True, QtCore.Qt.EditRole)
		else:
			for irow in range(model.rowCount()):
				model.setData(model.createIndex(irow, self._column), False, QtCore.Qt.EditRole)
		view.setFocus()
		view.customAnyRowChecked.emit(view)

class FilterHeaderCheckView(QtGui.QHeaderView):
	def __init__(self, parent):
		self._createVariables()
		super(FilterHeaderCheckView, self).__init__(QtCore.Qt.Horizontal, parent)

		self.setClickable(False)
		self.setDefaultSectionSize(130)
		self.setDefaultAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop )
		self.setStretchLastSection(True)
		self.setResizeMode(QtGui.QHeaderView.Stretch)

		self._createBindings()

	def _createVariables(self):
		self._controls = []
		self._defaultSectionHeight = 25

	def _createBindings(self):
		scrollBar = self.parent().horizontalScrollBar()
		scrollBar.valueChanged.connect(self.updateFilterGeometry)
		self.sectionResized.connect(self.updateFilterGeometry)

	def _createFilter(self):
		button = ButtonFilter(self, 0)
		button.setVisible(True)
		self._controls.append(button)
	# 	for i in range(1, self.parent().model().columnCount()):
	# 		edit = LineEditFilter(self, i)
	# 		edit.setVisible(True)
	# 		self._controls.append(edit)

	def showFilter(self):
		if self._controls:
			return

		self._createFilter()
		self.updateSections()
		self.updateFilterGeometry()

	def removeFilter(self):
		for edit in self._controls:
			edit.deleteLater()
		self._controls.clear()
		self.updateSections()

	def sizeHint(self):
		size = super(FilterHeaderCheckView, self).sizeHint()
		if self.hasFilter():	size.setHeight(2*self._defaultSectionHeight)
		else:					size.setHeight(self._defaultSectionHeight)
		return size

	def hasFilter(self):
		return len(self._controls) != 0

	def updateSections(self):
		# needed to resize the section height (super hack)
		for i in range(1, self.parent().model().columnCount()):
			self.resizeSection(i, self.sectionSize(i)-1)
			self.resizeSection(i, self.sectionSize(i)+1)

	def updateGeometries(self):
		super(FilterHeaderCheckView, self).updateGeometries()

		self.updateFilterGeometry()

	def updateFilterGeometry(self):
		for i, edit in enumerate(self._controls):
			x = self.sectionPosition(i) - self.offset()
			# y = self.sizeHint().height() // 2
			y = 0
			w = self.sectionSize(i)
			# h = self.sizeHint().height() // 2
			h = self.sizeHint().height()
			edit.setGeometry(x, y, w, h)
	def filterEdit(self, i):
		if i >= len(self._controls):
			i = 0
		# if type(self._controls[i]) == LineEditFilter:
		# 	self._controls[i].selectAll()
		self._controls[i].setFocus()

	@property
	def controls(self):
		return tuple(self._controls)

class CustomDataCheckView(QtGui.QTableView):
	customAnyRowChecked = QtCore.pyqtSignal(object)

	def __init__(self, parent):
		super(CustomDataCheckView, self).__init__(parent)
		self.verticalHeader().setVisible(False)
		self._parent = parent
		#
		self.setSelectionBehavior(QtGui.QTableView.SelectRows)
		# self.setSelectionMode(QtGui.QTableView.SingleSelection)
		self.setSelectionMode(QtGui.QTableView.ExtendedSelection)
		self.setAlternatingRowColors(True)
		self.checkBoxDelegate = CheckBoxDelegate(parent)
		self.checkBoxDelegate.customAnyRowChecked.connect(self.on_checkBoxDelegate_customAnyRowChecked)
		self.setItemDelegate(self.checkBoxDelegate)

	def on_checkBoxDelegate_customAnyRowChecked(self):
		self.customAnyRowChecked.emit(self)

	def setModel(self, model):
		super(CustomDataCheckView, self).setModel(model)
		self.setHorizontalHeader(FilterHeaderCheckView(self))
		self.horizontalHeader().showFilter()

		self.setVerticalHeader(QtGui.QHeaderView(QtCore.Qt.Vertical, self))
		self.verticalHeader().setResizeMode(QtGui.QHeaderView.Fixed)

		self.setColumnWidth(0, 100)

	def getSelected(self):
		data = []
		for irow in range(self.model().rowCount()):
			loadbool = self.model().data(self.model().createIndex(irow, 0))
			if not loadbool:
				continue
			data.append([self.model().data(self.model().createIndex(irow, icol)) for icol in
						 range(1, self.model().columnCount())])
		return data

	def parent(self):
		return self._parent

class CustomDataCheckModel(QtCore.QAbstractTableModel):
	customRowsCountChanged = QtCore.pyqtSignal(int)  # count

	def __init__(self, parent, data=None, header=None):
		super(CustomDataCheckModel, self).__init__(parent)
		self._parent = parent
		if data is not None:
			self._original = [[False] + list(row) for row in data]
			self._data = self._original.copy()
			self._fstrings = {i: "" for i in range(self.columnCount())}
		else:
			self._original = None
			self._data = None
			self._fstrings = None

		if header is not None:
			self._header = [""] + list(header)
		else:
			self._header = None

	def rowCount(self, parent=None):
		return len(self._data) if self._data else 0

	def columnCount(self, parent=None):
		return len(self._data[0]) if self._data else 0

	def data(self, index, role=QtCore.Qt.DisplayRole):
		if not index.isValid() or role != QtCore.Qt.DisplayRole:
			return None
		#
		ret = self._data[index.row()][index.column()]
		#
		if ret is None:                        return ""
		if isinstance(ret, datetime):        return ret.strftime("%d.%m.%Y")
		if isinstance(ret, bool):            return int(ret)

		return ret

	def headerData(self, col, orientation, role):
		if role == QtCore.Qt.DisplayRole:
			if orientation == QtCore.Qt.Horizontal:        return self._header[col] if self._header is not None else ""
			if orientation == QtCore.Qt.Vertical:        return col + 1
		return None

	def setData(self, index, value, role):
		self._data[index.row()][index.column()] = value
		return True

	def flags(self, index):
		if index.column() == 0:
			return QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable
		return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

	def applyFilter(self, datefrcol=None, datetocol=None, extents=None, xcol=None, ycol=None, zcol=None):
		window = self.parent().parent()
		if window.hasDateFilter:
			selectedDateFrom = window.dateRangeWidget.dateFrom()
			selectedDateTo = window.dateRangeWidget.dateTo()
			datefrom = datetime(*selectedDateFrom.getDate()).strftime("%Y-%m-%d")
			dateto = datetime(*selectedDateTo.getDate()).strftime("%Y-%m-%d")

		if datefrcol is not None and datefrcol > 0:
			datefrcol += 1
		if datetocol is not None and datetocol > 0:
			datetocol += 1

		xmin_criterion = xmax_criterion = ymin_criterion = ymax_criterion = zmin_criterion = zmax_criterion = False
		if not extents is None and {'xmin', 'ymin', 'zmin', 'xmax', 'ymax', 'zmax'} - set(extents.keys()) == set():
			if all([xcol, extents['xmin']]): xmin_criterion = True
			if all([xcol, extents['xmax']]): xmax_criterion = True
			if all([ycol, extents['ymin']]): ymin_criterion = True
			if all([ycol, extents['ymax']]): ymax_criterion = True
			if all([zcol, extents['zmin']]): zmin_criterion = True
			if all([zcol, extents['zmax']]): zmax_criterion = True

		self._data = []
		for row in range(len(self._original)):
			self._parent.hideRow(row)

			filteredByDate = True
			filteredByObject = True
			if window.hasDateFilter and datefrcol is not None and datetocol is not None:
				dfr = self._original[row][datefrcol]
				dto = self._original[row][datetocol]
				if dfr or dto:
					filteredByDate = (dfr >= datefrom and dfr <= dateto) or (dto >= datefrom and dto <= dateto)

			if filteredByObject and xmin_criterion:
				xcol_value = float(self._original[row][xcol])
				filteredByObject = xcol_value >= extents['xmin']
			if filteredByObject and xmax_criterion:
				xcol_value = float(self._original[row][xcol])
				filteredByObject = xcol_value <= extents['xmax']
			if filteredByObject and ymin_criterion:
				ycol_value = float(self._original[row][ycol])
				filteredByObject = ycol_value >= extents['ymin']
			if filteredByObject and ymax_criterion:
				ycol_value = float(self._original[row][ycol])
				filteredByObject = ycol_value <= extents['ymax']
			if filteredByObject and zmin_criterion:
				zcol_value = float(self._original[row][zcol])
				filteredByObject = zcol_value >= extents['zmin']
			if filteredByObject and zmax_criterion:
				zcol_value = float(self._original[row][zcol])
				filteredByObject = zcol_value <= extents['zmax']

			if not (filteredByDate and filteredByObject):
				continue

			filtered = [self._fstrings[col] in str(self._original[row][col]).lower() for col in self._fstrings]
			if all(filtered):
				self._data.append(self._original[row])

		if self.rowCount() > 0:
			self.emit(QtCore.SIGNAL("dataChanged(QModelIndex, QModelIndex)"), self.createIndex(0, self.columnCount()),
					  self.createIndex(self.rowCount(), self.columnCount()))
		else:
			self.emit(QtCore.SIGNAL("dataChanged(QModelIndex, QModelIndex)"), self.createIndex(0, self.columnCount()),
					  self.createIndex(1, self.columnCount()))
		for i in range(self.rowCount()):
			self._parent.showRow(i)

		self.customRowsCountChanged.emit(self.rowCount())

	def setFilterString(self, string, column):
		self._fstrings[column] = string.lower()

	def parent(self):
		return self._parent

	def getSelectedCount(self):
		count = 0
		zipped_data = list(zip(*self._original))
		count = len(list(filter(lambda x: x == True, zipped_data[0])))
		return count

	def getSelected(self):
		data = []
		for irow in range(len(self._original)):
			loadbool = self._original[irow][0]
			if not loadbool:
				continue
			data.append([self._original[irow][icol] for icol in range(1, self.columnCount())])
		return data

	def getResult(self):
		data = []
		for irow in range(self.rowCount()):
			if self.data(self.createIndex(irow, 0)):
				data.append(self.data(self.createIndex(irow, 1)))
		return(data)

class CheckBoxDelegate(QtGui.QStyledItemDelegate):
	customAnyRowChecked = QtCore.pyqtSignal()
	key_fields = ["EAST", "_EAST", "NORTH", "_NORTH", "RL", "_RL", "X", "_X", "Y", "_Y", "Z", "_Z"]

	def __init__(self, parent):
		super(CheckBoxDelegate, self).__init__()
		self._parent = parent
		self._chosen_fields = iterWidgetParents(self)[1].lineEdit_bm_fields.text()

	def createEditor(self, parent, option, index):
		# Important, otherwise an editor is created if the user clicks in this cell.
		return None

	def paint(self, painter, option, index):
		# Paint a checkbox without the label.
		if index.column() > 0:
			return super(CheckBoxDelegate, self).paint(painter, option, index)

		checked = bool(index.model().data(index, QtCore.Qt.DisplayRole))
		chbopt = QtGui.QStyleOptionButton()
		# chbopt.state = QtGui.QStyle.State_Enabled
		if index.flags() & QtCore.Qt.ItemIsEditable:
			chbopt.state |= QtGui.QStyle.State_Enabled

		if checked:
			chbopt.state |= QtGui.QStyle.State_On
		else:
			chbopt.state |= QtGui.QStyle.State_Off

		chbopt.rect = self.getCheckBoxRect(option)

		if option.state & QtGui.QStyle.State_Selected:
			if option.state & QtGui.QStyle.State_Active:
				painter.setBrush(QtGui.QBrush(QtGui.QPalette().highlight()))
			else:
				painter.setBrush(
					QtGui.QBrush(QtGui.QPalette().color(QtGui.QPalette.Inactive, QtGui.QPalette.Highlight)))
			painter.drawRect(option.rect)

		if str(index.model().index(index.row(), 1).data()) in CheckBoxDelegate.key_fields:
			index.model().setData(index, True, 1)
			chbopt.state = QtGui.QStyle.State_On

		QtGui.QApplication.style().drawControl(QtGui.QStyle.CE_CheckBox, chbopt, painter)

	def editorEvent(self, event, model, option, index):
		# Change the data in the model and the state of the checkbox
		# if the user presses the left mousebutton or presses
		# Key_Space or Key_Select and this cell is editable. Otherwise do nothing.

		if not int(index.flags() & QtCore.Qt.ItemIsEditable) > 0:
			return False

		# Do not change the checkbox-state
		if event.type() == QtCore.QEvent.MouseButtonRelease or event.type() == QtCore.QEvent.MouseButtonDblClick:
			if event.button() != QtCore.Qt.LeftButton or not self.getCheckBoxRect(option).contains(event.pos()):
				return False
			if event.type() == QtCore.QEvent.MouseButtonDblClick:
				return True
		elif event.type() == QtCore.QEvent.KeyPress:
			if event.key() != QtCore.Qt.Key_Space and event.key() != QtCore.Qt.Key_Select:
				return False
		else:
			return False

		# Change the checkbox-state
		self.setModelData(None, model, index)
		self.customAnyRowChecked.emit()
		return True

	def setModelData(self, editor, model, index):
		view = model.parent()
		checkAll = not all(model.data(index) for index in view.selectionModel().selectedRows())
		if checkAll:
			for index in view.selectionModel().selectedRows():
				model.setData(index, True, QtCore.Qt.EditRole)
		else:
			for index in view.selectionModel().selectedRows():
				model.setData(index, False, QtCore.Qt.EditRole)

	def getCheckBoxRect(self, option):
		check_box_style_option = QtGui.QStyleOptionButton()
		check_box_rect = QtGui.QApplication.style().subElementRect(QtGui.QStyle.SE_CheckBoxIndicator,
																   check_box_style_option, None)
		check_box_point = QtCore.QPoint(option.rect.x() +
										option.rect.width() / 2 -
										check_box_rect.width() / 2,
										option.rect.y() +
										option.rect.height() / 2 -
										check_box_rect.height() / 2)
		return QtCore.QRect(check_box_point, check_box_rect.size())

	def parent(self):
		return self._parent

class GeoMergeBm(DialogWindow):
	def __init__(self, parent, use_connection = True):
		super(GeoMergeBm, self).__init__(parent, use_connection)
		self.__createVariables()
		self.setWindowTitle("Объединение блочных моделей")
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()
		self._parent = parent

	def __createVariables(self):
		self._bms = {}
		self._tridbitems = []

	def __createWidgets(self):
		self.groupBoxInput = QtGui.QGroupBox(self)
		self.groupBoxInput.setTitle("Ввод")
		self.inputLayout = QtGui.QFormLayout()

		self.label_main_bm = QtGui.QLabel(self.groupBoxInput)
		self.label_main_bm.setText("Основная БМ")
		self.label_main_bm.setMinimumWidth(180)
		self.label_main_bm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBox_main_bm = QtGui.QComboBox(self.groupBoxInput)
		self.comboBox_main_bm.setMinimumWidth(200)

		self.label_bm_fields = QtGui.QLabel(self.groupBoxInput)
		self.label_bm_fields.setMinimumWidth(180)
		self.label_bm_fields.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.label_bm_fields.setText("Поля БМ")
		self.lineEdit_bm_fields = QtGui.QLineEdit(self.groupBoxInput)
		self.lineEdit_bm_fields.setMinimumWidth(200)
		self.lineEdit_bm_fields.setReadOnly(True)

		self.groupBoxOutput = QtGui.QGroupBox(self)
		self.groupBoxOutput.setTitle("Вывод")
		self.layoutOutput = QtGui.QVBoxLayout()

		self.groupBox_bm_output = QtGui.QGroupBox(self.groupBoxOutput)
		self.layoutOut_bm_output = QtGui.QFormLayout()
		self.label_result_bm = QtGui.QLabel(self.groupBox_bm_output)
		self.label_result_bm.setText("Файл")
		self.label_result_bm.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.label_result_bm.setMinimumWidth(168)
		self.lineEdit_result_bm = QtGui.QLineEdit(self.groupBox_bm_output)
		self.lineEdit_result_bm.setMaximumWidth(250)
		self.lineEdit_result_bm.setMinimumWidth(250)
		self.lineEdit_result_bm.setReadOnly(True)

		self.groupBox_option = QtGui.QGroupBox(self.groupBoxOutput)
		self.groupBox_option.setTitle("Опции")
		self.layoutOut_option = QtGui.QFormLayout()
		self.checkbox_wf = QtGui.QCheckBox(self.groupBox_option)
		self.checkbox_wf.setText("Ограничение выбранными каркасами")

		self.checkbox_mine_sched = QtGui.QCheckBox(self.groupBox_option)
		self.checkbox_mine_sched.setText("Обнуление содержаний для добытых блоков")

		self.checkbox_air_delete = QtGui.QCheckBox(self.groupBox_option)
		self.checkbox_air_delete.setText('Удаление "блоков воздуха"')

	def __createBindings(self):
		self.comboBox_main_bm.currentIndexChanged.connect(self.bm_changed)
		self.lineEdit_bm_fields.mouseDoubleClickEvent = lambda e: self.select_bm_fields()
		self.lineEdit_result_bm.mouseDoubleClickEvent = lambda e: self.browse_bm_output()
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxInput)

		self.groupBoxInput.setLayout(self.inputLayout)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_main_bm)
		self.inputLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBox_main_bm)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_bm_fields)
		self.inputLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEdit_bm_fields)

		self.layoutControls.addWidget(self.groupBoxOutput)
		self.groupBoxOutput.setLayout(self.layoutOutput)

		self.layoutOutput.addWidget(self.groupBox_bm_output)
		self.groupBox_bm_output.setLayout(self.layoutOut_bm_output)
		self.layoutOut_bm_output.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_result_bm)
		self.layoutOut_bm_output.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEdit_result_bm)

		self.layoutOutput.addWidget(self.groupBox_option)
		self.groupBox_option.setLayout(self.layoutOut_option)
		self.layoutOut_option.setWidget(0, QtGui.QFormLayout.LabelRole, self.checkbox_wf)
		self.layoutOut_option.setWidget(1, QtGui.QFormLayout.LabelRole, self.checkbox_mine_sched)
		self.layoutOut_option.setWidget(2, QtGui.QFormLayout.LabelRole, self.checkbox_air_delete)

	def build_comboBox_main_bm(self):
		self.comboBox_main_bm.clear()
		self.comboBox_main_bm.addItems(sorted(list(self._bms.keys())))

	def exec(self):
		if not super(GeoMergeBm, self).prepare():
			return

		# считывание списка моделей
		bmItems = self.selectedItems()[ITEM_BM]
		if not bmItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите блочные модели")
			return

		if len(bmItems) < 2:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Для объединения требуется минимум две модели")
			return

		# проверяем, что все блочные модели имеют метаданные
		sysdate = datetime.now()
		currdates = []
		bmStructures = []
		for modelItem in bmItems:
			netBmPath = modelItem.filePath()

			bm = BlockModel(netBmPath, self.main_app)
			# проверка наличия метаданных блочных моделей
			if not bm.has_metadata:
				msg = "Блочная модель '%s' не имеет метаданных. Обновите блочную модель." % bm.name
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			# проверка на соответствие метаданных содержанию блочной модели
			metadate = bm.metadata.current_date
			bmdate = bm.blockmodel_date
			if bmdate.year != metadate.year or bmdate.month != metadate.month:
				qtmsgbox.show_error(self.main_app, "Ошибка",
									"Метаданные не соответствуют содержанию блочной модели '%s'. Обновите блочную модель." % bm.name)
				return

			#
			if not metadate in currdates:
				currdates.append(metadate)

			# считывание структуры файлы блочной модели
			bmFile = MicromineFile()
			if not bmFile.open(netBmPath):
				qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно открыть файл %s" % netBmPath)
				return
			struct = MicromineFile.from_filestruct(bmFile.structure)
			if not struct in bmStructures:
				bmStructures.append(struct)
			bmFile.close()

		#
		if len(currdates) > 1:
			qtmsgbox.show_error(self.main_app, "Ошибка",
								"Выбранные блочные модели переведены в разные месяца. Выберите блочные модели, переведенные в один и тот же месяц")
			return
		elif (currdates[0].year == sysdate.year and currdates[0].month != sysdate.month) or (
			currdates[0].year < sysdate.year):
			if not qtmsgbox.show_question(self.main_app, "Внимание",
										  "Состояние блочных моделей не соответствует текущей календарной дате. Продолжить?"):
				return

		for k, modelItem in enumerate(bmItems):
			netBmPath = modelItem.filePath()
			net_bm = BlockModel(netBmPath, self.main_app)
			self._bms[net_bm.name] = netBmPath
		self.build_comboBox_main_bm()

		self._tridbitems = self.selectedItems()[ITEM_TRIDB]
		if not self._tridbitems:
			self.checkbox_wf.setDisabled(True)
		else:
			self.checkbox_wf.setEnabled(True)
		super(GeoMergeBm, self).exec()

	def bm_changed(self):
		self.lineEdit_bm_fields.setText("")

	def select_bm_fields(self):
		bm_path = self._bms[self.comboBox_main_bm.currentText()]
		bmFile = MicromineFile()
		if not bmFile.open(bm_path):
			raise Exception("Невозможно открыть файл %s" % bm_path)
		bmHeader = bmFile.header
		bmFile.close()

		data = []
		for i, val in enumerate(bmHeader):
			data.append([val])
		self.dialog_window = DialogWindow(self)
		self.dialog_window._data = data.copy()
		self.dialog_window.view = CustomDataCheckView(self.dialog_window)
		self.dialog_window.model = CustomDataCheckModel(self.dialog_window.view, self.dialog_window._data, ["ПОЛЕ"])
		self.dialog_window.view.setModel(self.dialog_window.model)

		if self.lineEdit_bm_fields.text():
			checked_fields = self.lineEdit_bm_fields.text().split(', ')
			for irow in range(self.dialog_window.model.rowCount()):
				if self.dialog_window.model.data(self.dialog_window.model.createIndex(irow, 1)) in checked_fields:
					self.dialog_window.model.setData(self.dialog_window.model.createIndex(irow, 0), True, QtCore.Qt.EditRole)
		else:
			for irow in range(self.dialog_window.model.rowCount()):
					self.dialog_window.model.setData(self.dialog_window.model.createIndex(irow, 0), True, QtCore.Qt.EditRole)

		self.dialog_window.layoutControls.addWidget(self.dialog_window.view)
		self.dialog_window.resize(450, 600)
		self.dialog_window.setWindowTitle("Выбор полей")

		self.dialog_window.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.dialog_window.close)
		self.dialog_window.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.getResult)

		self.dialog_window.exec()

	def getResult(self):
		result = self.dialog_window.model.getResult()
		if result:
			self.lineEdit_bm_fields.setText(", ".join(result))
			self.lineEdit_bm_fields.setCursorPosition(0)
		self.dialog_window.close()

	def browse_bm_output(self):
		try:
			initialdir = self._parent.settings.getValue(ID_NETPATH)
		except:
			initialdir = os.getcwd()
		path = QtGui.QFileDialog.getSaveFileName(self, "Выберите файл", initialdir).replace("/", "\\")
		if not path:
			return
		if '.dat' not in path.lower():
			path = path + '.DAT'
		self.lineEdit_result_bm.setText(path)

	def prepare(self):
		if not self.lineEdit_bm_fields.text():
			qtmsgbox.show_error(self, "Ошибка", "Необходимо указать поля БМ")
			self.lineEdit_bm_fields.setFocus()
			return
		if not self.lineEdit_result_bm.text():
			qtmsgbox.show_error(self, "Ошибка", "Необходимо указать файл вывода")
			self.lineEdit_result_bm.setFocus()
			return

		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))

		self.after_run()

	def run(self):

		bmItems = self.selectedItems()[ITEM_BM]

		progress = self.main_app.mainWidget.progressBar
		netBasisBm = BlockModel(self._bms[self.comboBox_main_bm.currentText()], self.main_app)

		BMFLD_CURRDATE = netBasisBm.blockmodel_date
		BMFLD_CURRDATE_FIELD = date_to_fieldname(BMFLD_CURRDATE)
		BMFLD_PREVDATE = prev_date(BMFLD_CURRDATE)
		outBmPath_tmp = os.path.join(self._tmpfolder, get_temporary_prefix() + "merged_bm_output.DAT")

		outBmPath = self.lineEdit_result_bm.text()
		progress.setRange(0, 0)
		bmfields_output = set(self.lineEdit_bm_fields.text().split(', '))

		air_blocks_purposes = ('Горно-капитальные работы', 'Камерно-кубажные работы', 'Горно-подготовительные работы', 'Очистные работы')
		air_blocks_categories = []
		for i in air_blocks_purposes:
			if i in self.getPurposes():
				air_blocks_categories.append(self.getCategories()[self.getPurposes().index(i)])
		air_blocks_categories = tuple(set(air_blocks_categories))

		### =================================================================================== ###
		### =========================== КОПИРОВАНИЕ БЛОЧНЫХ МОДЕЛЕЙ =========================== ###
		### =================================================================================== ###

		locBmPathList = []
		for k, modelItem in enumerate(bmItems):
			netBmPath = modelItem.filePath()
			net_bm = BlockModel(netBmPath, self.main_app)
			locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % net_bm.name)
			if net_bm.name == self.comboBox_main_bm.currentText():
				locBmPathList.insert(0, locBmPath)
			else:
				locBmPathList.append(locBmPath)
			progress.setText("Копирование %s" % net_bm.name)
			err = self.exec_proc(self.copy_file, netBmPath, locBmPath)
			if err:                    raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
			if self.is_cancelled:    return

			### =================================================================================== ###
			### ===================== ИЗМЕНЕНИЕ СТРУКТУРЫ ВЫХОДНОЙ БЛОЧНОЙ МОДЕЛИ ================= ###
			### =================================================================================== ###

			bmFile = MicromineFile()
			if not bmFile.open(locBmPath):
				raise Exception("Невозможно открыть файл %s" % locBmPath)
			bmHeader = bmFile.header
			bmStructure = bmFile.structure

			# delete air blocks
			if self.checkbox_air_delete.isChecked():
				progress.setText('Удаление "блоков воздуха" %s' % net_bm.name)
				idx_cur_date = bmHeader.index(BMFLD_CURRDATE_FIELD) if BMFLD_CURRDATE_FIELD in bmHeader else None
				if not idx_cur_date:
					bmFile.close()
					raise Exception("Поле даты '%s' отсутствует в блочной модели %s" % (BMFLD_CURRDATE_FIELD, net_bm.name))
				#
				for irow in reversed(range(bmFile.records_count)):
					if irow % 500 == 0:
						QtCore.QCoreApplication.processEvents()
					if bmFile.get_str_field_value(idx_cur_date, irow + 1) in air_blocks_categories:
						bmFile.delete_record(irow + 1)

			# keep only required fields
			progress.setText("Изменение структуры %s" % net_bm.name)
			if not bmfields_output.issubset(bmHeader):
				raise Exception("Ошибка. В блочной модели %s отсутствуют выбранные поля %s" % (net_bm.name, str(bmfields_output - set(bmHeader))))
			fldsToRemove = set(bmHeader) - bmfields_output
			idxToRemove = sorted([bmHeader.index(fld) for fld in fldsToRemove], reverse=True)
			for idx in idxToRemove:
				bmStructure.delete_field(idx)
			bmFile.structure = bmStructure
			bmFile.close()

		### =================================================================================== ###
		### =========================== ОБЪЕДИНЕНИЕ БЛОЧНЫХ МОДЕЛЕЙ =========================== ###
		### =================================================================================== ###
		mergedBm = BlockModel(locBmPathList[0], self.main_app)
		bmCount = len(locBmPathList) - 1
		for i, locBmPath in enumerate(locBmPathList[1:], start=1):
			bmparams = {}
			bmparams["bmpath2"] = locBmPath
			bmparams["outpath"] = outBmPath_tmp
			try:
				mergedBm = mergedBm.merge(bmparams)
			except Exception as e:
				raise Exception("Не удалось объединить блочные модели. %s" % str(e))
			#
			if i < bmCount:
				try:
					os.remove(locBmPath)
				except:
					raise Exception("Не удалось удалить блочную модель. %s" % str(e))
				try:
					os.rename(outBmPath_tmp, locBmPath)
				except Exception as e:
					raise Exception("Не удалось переименовать блочную модель. %s" % str(e))
				mergedBm = BlockModel(locBmPath, self.main_app)
			elif i == bmCount:
				try:
					shutil.copy(outBmPath_tmp, outBmPath)
				except Exception as e:
					raise Exception("Не удалось создать объединенную БМ в указанной папке. %s" % str(e))
			#
			if self.is_cancelled:    return

		### =================================================================================== ###
		### ===================== ОБНУЛЕНИЕ СОДЕРЖАНИЙ ДЛЯ ДОБЫТЫХ БЛОКОВ ===================== ###
		### =================================================================================== ###
		if self.checkbox_mine_sched.isChecked():
			progress.setText("Обнуление содержаний для добытых блоков")
			# сделано, чтобы MineSched не добывал повторно добытые блоки (не умеет различать добыто/не добыто)
			try:
				self.run_grade_clear()
			except Exception as e:
				qtmsgbox.show_error(self.main_app, "Внимание", "Невозможно обнулить содержание для добытых блоков. %s"
															   "Блочные модели будут объединены без обнуления содержания." % str(e))


		### =================================================================================== ###
		### ======================== ОГРАНИЧЕНИЕ ВЫБРАННЫМИ КАРКАСАМИ ========================= ###
		### =================================================================================== ###
		if self.checkbox_wf.isChecked():
			progress.setText("Ограничение выбранными каркасами")
			try:
				self.run_assign_wf()
			except Exception as e:
				qtmsgbox.show_error(self.main_app, "Внимание", "Невозможно ограничить каркасами. %s"
															   "Блочные модели будут объединены без ограничения каркасами." % str(e))

		progress.setRange(0, 100)
		qtmsgbox.show_information(self.main_app, "Выполнено", "Блочные модели успешно объединены.")

	def run_grade_clear(self):
		outBmPath = self.lineEdit_result_bm.text()
		bmFile = MicromineFile()
		if not bmFile.open(outBmPath):
			raise Exception("Невозможно открыть файл '%s' для обнуления содержаний" % outBmPath)
		bmHeader = bmFile.header
		# поля элементов
		idwElements = [elem for elem in self.getIdwElements() if elem in bmHeader]
		idwElemIndexes = [bmHeader.index(elem) for elem in idwElements]
		#
		idxTotalMined = bmHeader.index(BMFLD_TOTALMINED) if BMFLD_TOTALMINED in bmHeader else None
		if not idxTotalMined:
			bmFile.close()
			raise Exception("Поле '%s' отсутствует в итоговой блочной модели.\n" % BMFLD_TOTALMINED)
		for irow in range(bmFile.records_count):
			if irow % 500 == 0:
				QtCore.QCoreApplication.processEvents()
			#
			isMined = bmFile.get_str_field_value(idxTotalMined, irow + 1) == "Y"
			if not isMined:
				continue
			for icol in idwElemIndexes:
				bmFile.set_field_value(icol, irow + 1, "")
		bmFile.close()

	def run_assign_wf(self):
		wfdata = []
		netTridbPathsDict = self.getTridbPathsDict(self._tridbitems, copy=False)
		for tripath in netTridbPathsDict:
			for wfname in netTridbPathsDict[tripath]:
				wfdata.append([tripath, DEFAULT_ATTR_NAME, wfname])
		wfset = create_wireframe_set(wfdata)

		# параметры присваивания
		assing_params = {}
		assing_params["bmpath"] = self.lineEdit_result_bm.text()
		assing_params["wireframe_set"] = wfset
		assing_params["attributes"] = [["0", "", ""], ]
		assing_params["sub_xsize"] = self.main_app.settings.getValue(ID_SUBXGEO)
		assing_params["sub_ysize"] = self.main_app.settings.getValue(ID_SUBYGEO)
		assing_params["sub_zsize"] = self.main_app.settings.getValue(ID_SUBZGEO)
		assing_params["overwrite_bool"] = False
		assing_params["purge_bool"] = False
		assing_params["delete_bool"] = True

		assign_wireframes(assing_params)