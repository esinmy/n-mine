try:
	import MMpy
except:
	pass

from PyQt4 import QtGui, QtCore

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import DialogWindow

from mm.mmutils import MicromineFile, Tridb
from mm.formsets import defaultLoadStrings

from utils.constants import *

import math, os, sqlite3, numbers, enum, statistics, shutil, zipfile

from collections import namedtuple
from datetime import datetime

from utils.nnutil import prev_date, date_to_fieldname, fieldname_to_date
from utils.logger import NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GeoBackupBm(DialogWindow):
	def __init__(self, parent, use_connection=True):
		super(GeoBackupBm, self).__init__(parent, use_connection)
		self.setMinimumSize(300, 100)
		self.setWindowTitle("Выберите тип фиксации")
		self._parent = parent
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._mode = 'MONTHLY'
		self._archive_mode = 'ARCHIVE_MONTHLY'
		self._cnxn = self._parent.srvcnxn
		self._bm_models = []
		self._selected_units = []
		self._operation_models = []
		self._columns = []
		self._selected_layer = None
		self._unit_prefix = None
		self._fix_folder_name = "ГЕО_МЕСЯЦ_ФИКС"
		self._ignored_models = []

	def __createWidgets(self):
		self.groupBoxOption = QtGui.QGroupBox(self)
		self.groupBoxOption.setTitle("Опции")
		self.layoutOutBoxOption = QtGui.QVBoxLayout()

		self.radioButtonBox = QtGui.QButtonGroup(self.groupBoxOption)
		self.radioButtonMonth = QtGui.QRadioButton(self.groupBoxOption)
		self.radioButtonMonth.setText("Ежемесячная")
		self.radioButtonMonth.setChecked(True)
		self.radioButtonYear = QtGui.QRadioButton(self.groupBoxOption)
		self.radioButtonYear.setText("Годовая")
		self.radioButtonBox.addButton(self.radioButtonMonth)
		self.radioButtonBox.addButton(self.radioButtonYear)

	def __createBindings(self):
		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)

	def __gridWidgets(self):
		self.layoutControls.addWidget(self.groupBoxOption)
		self.groupBoxOption.setLayout(self.layoutOutBoxOption)
		self.layoutOutBoxOption.addWidget(self.radioButtonMonth)
		self.layoutOutBoxOption.addWidget(self.radioButtonYear)

	# Нажатие ok в главном окне N-Mine вызовет сначала эту проверку,
	# а затем уже откроется окно для выбора ежемесячной или годовой фиксации
	def exec(self):
		if not super(GeoBackupBm, self).prepare():
			return

		# Выбранные геоюниты, залежь и префикс юнитов (например, Панель_)
		self._selected_units = self._parent.mainWidget.backupExplorer.selected_units

		# Check - в обозревателе выбран как минимум один юнит
		if not self._selected_units:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите юниты для фиксации!")
			# self.main_app.currentMode = MODE_GEO_BACKUP_BM
			return

		self._selected_layer = self._parent.mainWidget.backupExplorer.selected_layer
		self._unit_prefix = self._parent.mainWidget.backupExplorer.unit_prefix

		# Считываем записи для выбранных юнитов и залежи
		self._placeholders = ', '.join('?' for _ in self._selected_units)
		cmd = "SELECT * FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [LAYER] = '%s' " \
			  "and [COMMIT_STATUS] is NULL and GEOUNIT in (%s) " \
			  % (self._cnxn.dbsvy, self._selected_layer, self._placeholders)
		records = self._cnxn.exec_query(cmd, self._selected_units).get_rows()
		self._columns = self._cnxn.cursor_description()
		self._columns_sql = ['[' + _ + ']' for _ in self._columns]
		# Записываем информацию по выбранным моделям в список из namedtuple
		# Условие if ставится для того, чтобы оставить только модели, которые есть на диске
		Model = namedtuple('Model', ", ".join(self._columns))
		self._operation_models = [Model(*el) for el in records]
		self._operation_models = [el for el in self._operation_models if os.path.exists(el.PATH)]

		# Check - у всех выбранных моделей одинаковое значение поля [CURRENT_DATE]
		# и что эта дата соответствует текущей дате
		sysdate = datetime.now()
		current_dates = {model.CURRENT_DATE for model in self._operation_models}
		if len(current_dates) != 1:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Для фиксации требуется, чтобы все "
														 "выбранные модели находились в одном и том же месяце!")
			return
		elif int(list(current_dates)[0].split('-')[0]) != sysdate.year or int(list(current_dates)[0].split('-')[1]) != sysdate.month:
				if not qtmsgbox.show_question(self.main_app, "Внимание", "Состояние блочных моделей не соответствует текущей календарной дате. Продолжить?"):
					return

		super(GeoBackupBm, self).exec()

	# Выполняется после нажатия Ok в окне с выбором ежемесячной или годовой фиксации
	def prepare(self):
		try:
			self._is_running = True
			status = self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
			status = None
		finally:
			# Возврат к режиму "Загрузить данные" после завершения работы функции
			if status:
				self._parent.toolButtonDisplayData.click()
		self.after_run()

	def run(self):
		self._ignored_models = []
		#commit_date = date_to_fieldname(datetime.now()) + '-01'
		year, month, day = map(int, (date_to_fieldname(datetime.now()) + '-01').split("-"))
		commit_date = datetime(year, month, day)
		if self.radioButtonMonth.isChecked():
			self._mode = 'MONTHLY'
			self._archive_mode = 'ARCHIVE_MONTHLY'
			self._fix_folder_name = "ГЕО_МЕСЯЦ_ФИКС"
		else:
			self._mode = 'ANNUAL'
			self._archive_mode = 'ARCHIVE_ANNUAL'
			self._fix_folder_name = "ГЕО_ГОД_ФИКС"

		# Имена выбранных моделей
		slctd_oper_model_names = [model.NAME for model in self._operation_models]
		# Считывание записей фиксированных моделей с такими же именами
		cmd = "SELECT %s FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [LAYER] = '%s' " \
			  "and [COMMIT_STATUS] = '%s' and GEOUNIT in (%s)" \
			  % (", ".join(self._columns_sql), self._cnxn.dbsvy, self._selected_layer, self._mode, self._placeholders)

		records = self._cnxn.exec_query(cmd, self._selected_units).get_rows()
		# Записываем информацию по выбранным моделям в список из namedtuple
		# Условие if ставится для того, чтобы оставить только модели, которые есть на диске
		Model = namedtuple('Model', ", ".join(self._columns))
		self._exstd_fixed_models = [Model(*el) for el in records]
		# self._exstd_fixed_models = [el for el in self._exstd_fixed_models if os.path.exists(el.PATH)]
		self._exstd_fixed_models = [el for el in self._exstd_fixed_models]

		# Формируем список метаданных для фиксации
		# Создаем набор записей аналогичных оперативным моделям, но для фиксированных моделей
		# Меняем только значение полей PATH и COMMIT_STATUS
		self._oper_to_fixed_models = []
		for model in self._operation_models:
			fixed_model_path = model.PATH.replace(self._parent.settings.getValue(ID_GEO_MAINDIR), self._fix_folder_name)
			year, month, day = map(int, model.CURRENT_DATE.split("-"))
			current_date = datetime(year, month, day)
			self._oper_to_fixed_models.append(model._replace(
				COMMIT_STATUS=self._mode, PATH=fixed_model_path, COMMIT_DATE=commit_date, CURRENT_DATE = current_date))

		# Проверяем, что для каждой оперативной модели есть фиксированная модель в БД (метаданные)
		not_found_fixed_models_metadata = []
		for oper_to_fixed_model in self._oper_to_fixed_models:
			for fix_model in self._exstd_fixed_models:
				if oper_to_fixed_model.NAME == fix_model.NAME and oper_to_fixed_model.LAYER == fix_model.LAYER \
						and oper_to_fixed_model.GEOUNIT == fix_model.GEOUNIT: break
			else:
				not_found_fixed_models_metadata.append(oper_to_fixed_model)

		if not_found_fixed_models_metadata:
			if len(not_found_fixed_models_metadata) == 1:
				msg = "Данные для фиксированной модели %s отсутствуют в базе данных.\nДобавить метаданные фиксированной модели в базу данных?"
			else:
				msg = "Данные для фиксированных моделей %s отсутствуют в базе данных.\nДобавить метаданные фиксированных моделей в базу данных?"
			if not qtmsgbox.show_question(self, "Внимание", msg % ([model.NAME for model in not_found_fixed_models_metadata])):
				qtmsgbox.show_information(self, 'Внимание!', "Процесс остановлен.")
				self.close()
				return

		for index, oper_to_fixed_model in enumerate(self._oper_to_fixed_models):
			operation_bm_path = self._operation_models[index].PATH
			operation_str_bounds_path = os.path.dirname(self._operation_models[index].PATH) + "\\%s.STR" % self._operation_models[index].NAME
			operation_tridb_path = os.path.dirname(os.path.dirname(self._operation_models[index].PATH)) + '\\' + \
						 self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1] + "\\%s.TRIDB" % self._operation_models[index].NAME
			operation_all_paths = {'bm': operation_bm_path, 'str': operation_str_bounds_path, 'tridb': operation_tridb_path}

			fix_bm_path = oper_to_fixed_model.PATH
			fix_str_bounds_path = os.path.dirname(oper_to_fixed_model.PATH) + "\\%s.STR" % oper_to_fixed_model.NAME
			fix_tridb_path = os.path.dirname(os.path.dirname(oper_to_fixed_model.PATH)) + '\\' + \
						 self.main_app.settings.getValue(ID_GEO_USUBDIRS)[1][1] + "\\%s.TRIDB" % oper_to_fixed_model.NAME
			fix_all_paths = {'bm': fix_bm_path, 'str': fix_str_bounds_path, 'tridb':fix_tridb_path}

			if all(os.path.exists(path) for path in fix_all_paths.values()) and self.record_exists(fix_all_paths):
				if self.same_current_date(operation_all_paths['bm'], fix_all_paths['bm']):
					qtmsgbox.show_information(self, "Внимание", "Модель {} уже была зафиксирована раннее.\n"
														 "".format(oper_to_fixed_model.NAME))
					continue
				if not self.archive_model_and_update_db(fix_all_paths):
					continue
				[shutil.copyfile(operation_all_paths[key], fix_all_paths[key]) for key in fix_all_paths]
				self.add_record_to_db(oper_to_fixed_model)
			# нет никаких файлов или есть только часть файлов фиксированной модели, но есть запись метаданных в БД
			elif (any(os.path.exists(path) for path in fix_all_paths.values()) or
						  not any(os.path.exists(path) for path in fix_all_paths.values())) \
					and self.record_exists(fix_all_paths):
				for path in fix_all_paths.values():
					if not os.path.exists(os.path.dirname(path)):
						os.makedirs(os.path.dirname(path), exist_ok=True)
				[shutil.copyfile(operation_all_paths[key], fix_all_paths[key]) for key in fix_all_paths]
				# запись в бд может содержать мусор про какую-то старую фиксированную модель
				# поэтому удаляем старую запись и создаем новую
				self.delete_record_in_db(oper_to_fixed_model, fix_all_paths)
				self.add_record_to_db(oper_to_fixed_model)
			elif not any(os.path.exists(path) for path in fix_all_paths.values()) and not self.record_exists(
					fix_all_paths):
				for path in fix_all_paths.values():
					if not os.path.exists(os.path.dirname(path)):
						os.makedirs(os.path.dirname(path), exist_ok=True)
				[shutil.copyfile(operation_all_paths[key], fix_all_paths[key]) for key in fix_all_paths]
				self.add_record_to_db(oper_to_fixed_model)
			# есть файлы, но нет записи метаданных в БД
			elif any(os.path.exists(path) for path in fix_all_paths.values()) and not self.record_exists(fix_all_paths):
				for path in fix_all_paths.values():
					if not os.path.exists(os.path.dirname(path)):
						os.makedirs(os.path.dirname(path), exist_ok=True)
				[shutil.copyfile(operation_all_paths[key], fix_all_paths[key]) for key in fix_all_paths]
				self.add_record_to_db(oper_to_fixed_model)
			else:
				self._ignored_models.append(oper_to_fixed_model.NAME)
				qtmsgbox.show_error(self, "Ошибка!", "Неведомая ошибка с моделью {}!\n"
									"Пожалуйста, свяжитесь с разработчиками.".format(oper_to_fixed_model.NAME))
				continue

		if not self._ignored_models:
			qtmsgbox.show_information(self.main_app, "Выполнено", "Фиксирование моделей успешно завершено!")
		else:
			qtmsgbox.show_information(self.main_app, "Выполнено", "Процесс завершен.\n"
																  "Следующие модели не были зафиксированы:\n{}".format(self._ignored_models))
		self.close()
		return True

	def same_current_date(self, oper_model_path, fix_model_path):
		cmd_fix = "SELECT [CURRENT_DATE], [NAME] FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [PATH] = '%s' " \
			  "and [COMMIT_STATUS] is NULL" % (self._cnxn.dbsvy, oper_model_path)
		record_fix = self._cnxn.exec_query(cmd_fix).get_rows()

		cmd_oper = "SELECT [CURRENT_DATE], [NAME] FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [PATH] = '%s' " \
				  "and [COMMIT_STATUS] = '%s'" % (self._cnxn.dbsvy, fix_model_path, self._mode)
		record_oper = self._cnxn.exec_query(cmd_oper).get_rows()

		return record_fix == record_oper

	def archive_model_and_update_db(self, paths):
		directory = os.path.dirname(os.path.dirname(paths['bm']))

		cmd = "SELECT [BM_ID], [COMMIT_DATE], [NAME] FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [PATH] = '%s' " \
			  "and [COMMIT_STATUS] = '%s'" % (self._cnxn.dbsvy, paths['bm'], self._mode)
		archive_bm_id, archive_commit_date, archive_name = self._cnxn.exec_query(cmd).get_rows()[0]
		archive_commit_year, archive_commit_month = archive_commit_date.split('-')[:2]

		zip_path = os.path.join(directory, archive_name + '_' + archive_commit_year + '_' + archive_commit_month + '.zip')
		if os.path.exists(zip_path):
			if not qtmsgbox.show_question(self, 'Внимание!', "Архив {} уже существует. Перезаписать?".format(os.path.basename(zip_path))):
				self._ignored_models.append(archive_name)
				return False

		zip_file = zipfile.ZipFile(zip_path, mode='w')
		for path in paths.values():
			# закидываем файлы в zip, сохраняем структуру папок (БМ и Каркасы)
			zip_file.write(path, arcname=os.sep.join(path.split(os.sep)[-2:]))
			# если не надо сохранять структуру
			# zip_file.write(path, arcname=os.path.basename(path))
			os.remove(path)
		zip_file.close()

		cmd = "UPDATE [%s].[dbo].[NM_BLOCKMODEL_METADATA] SET [COMMIT_STATUS] = N'%s', [PATH] = N'%s' WHERE BM_ID = %s" \
			  "" % (self._cnxn.dbsvy, self._archive_mode, zip_path, archive_bm_id)
		self._cnxn.exec_query(cmd)
		self._cnxn.commit()

		return True

	def add_record_to_db(self, model):
		cmd = """INSERT INTO [{0}].[dbo].[NM_BLOCKMODEL_METADATA] (
							[NAME],
							PATH,
							LAYER,
							GEOUNIT,
							XMIN,
							XMAX,
							YMIN,
							YMAX,
							ZMIN,
							ZMAX,
							[CURRENT_DATE],
							DATE_TRANSFERED,
							TRASFERED_BY,
							DATE_INSERTED,
							INSERTED_BY,
							DATE_UPDATED,
							UPDATED_BY,
							IS_NEED_TRANSFER,
							IS_USE_DYNAMIC_DENSITY,
							EXPANSION,
							COMMIT_STATUS,
							COMMIT_DATE)
					VALUES (%s)""".format(self._cnxn.dbsvy)
		variables = list(i for i in model[1:])
		values = []
		for var in variables:
			if var is None: values.append("NULL")
			elif type(var) == str: values.append("N'%s'" % var)
			elif type(var) == datetime: values.append("N'%s'" % var.strftime("%Y-%m-%dT%H:%M:%S"))
			elif type(var) == bool: values.append("%d" % var)
			else: values.append("%f" % var)
		logger.info(",".join(values))
		self._cnxn.exec_query(cmd % ",".join(values))
		self._cnxn.commit()

	def delete_record_in_db(self, model, paths):
		cmd = """DELETE FROM [{}].[dbo].[NM_BLOCKMODEL_METADATA] where [PATH] = '{}' and [COMMIT_STATUS] = '{}'""".format(self._cnxn.dbsvy, paths['bm'], self._mode)
		self._cnxn.exec_query(cmd)
		self._cnxn.commit()

	def record_exists(self, paths):
		directory = os.path.dirname(os.path.dirname(paths['bm']))

		cmd = "SELECT [BM_ID], [COMMIT_DATE], [NAME] FROM [%s].[dbo].[NM_BLOCKMODEL_METADATA] where [PATH] = '%s' " \
			  "and [COMMIT_STATUS] = '%s'" % (self._cnxn.dbsvy, paths['bm'], self._mode)
		return True if self._cnxn.exec_query(cmd).get_rows() else False
