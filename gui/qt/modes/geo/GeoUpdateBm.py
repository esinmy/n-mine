try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.GeoUpdateBmDialog import GeoUpdateBmDialog
from gui.qt.ProcessWindow import ProcessWindow
from gui.qt.modes.WireframeAttributeValidation import GeoWireframeAttributeValidation

from mm.mmutils import Tridb, MicromineFile, get_micromine_version
from mm.formsets import create_wireframe_set, copy_wireframe

from utils.nnutil import prev_date, date_to_fieldname, fieldname_to_date		
from utils.blockmodel import BlockModel
from utils.constants import *
from utils.osutil import get_file_owner, get_temporary_prefix
from utils.validation import is_blank

from datetime import datetime
import os, math, sqlite3

import shapely.geometry as sgeom
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def modifyBlockModelStructure(structure, currdate, svy_mine_bind_attr_name, elements = [] ):
	structure.add_field(BMFLD_GEOMATERIAL, MMpy.FieldType.character, 5, 0)
	structure.add_field(BMFLD_GEOINDEX, MMpy.FieldType.character, 8, 0)
	structure.add_field(BMFLD_MATERIAL, MMpy.FieldType.character, 5, 0)
	structure.add_field(BMFLD_INDEX, MMpy.FieldType.character, 8, 0)
	structure.add_field(BMFLD_DENSITY, MMpy.FieldType.real, 0, 3)
	# добавление элементов
	for elem in elements:
		structure.add_field(elem, MMpy.FieldType.real, 0, 6)
	# # добавление проектных полей
	#
	prevdate = prev_date(currdate, "str")
	structure.add_field(prevdate, MMpy.FieldType.character, 6, 0)
	structure.add_field(currdate, MMpy.FieldType.character, 6, 0)
	#
	structure.add_field(BMFLD_MINEDATE, MMpy.FieldType.character, 10, 0)
	structure.add_field(BMFLD_FACTDATE, MMpy.FieldType.character, 10, 0)
	structure.add_field(BMFLD_SVYBLOCK, MMpy.FieldType.character, 16, 0)
	structure.add_field(svy_mine_bind_attr_name, MMpy.FieldType.character, 6, 0)
	structure.add_field(BMFLD_MINE_MINENAME, MMpy.FieldType.character, 25, 0)
	structure.add_field(BMFLD_MINE_INTERVAL, MMpy.FieldType.character, 25, 0)
	structure.add_field(BMFLD_MINENAME, MMpy.FieldType.character, 25, 0)
	structure.add_field(BMFLD_INTERVAL, MMpy.FieldType.character, 25, 0)
	structure.add_field(BMFLD_MINEWORK, MMpy.FieldType.character, 6, 0)
	structure.add_field(BMFLD_FACTWORK, MMpy.FieldType.character, 6, 0)
	structure.add_field(BMFLD_CURRMINED, MMpy.FieldType.character, 1, 0)
	structure.add_field(BMFLD_TOTALMINED, MMpy.FieldType.character, 1, 0)
	return structure
# 3d оценка блоков методом обратных расстояний
def idw(locBmPath, locAssayPath, indexFld, oretype, element, radius, indicies, FIELD_DIR, FIELD_ROT, FIELD_DIP,	minpts, maxpts, sectors, factor1, factor2, factor3):
	#
	Dbfilter_Obm3dIdw_FormSet19_Nested2= MMpy.FormSet("DBFILTER","16.0.875.2")
	DataGrid17 = MMpy.DataGrid(4,1)
	DataGrid17.set_column_info(0,1,4)
	DataGrid17.set_column_info(1,2,6)
	DataGrid17.set_column_info(2,3,-1)
	DataGrid17.set_column_info(3,4,14)
	for i, index in enumerate(indicies):
		DataGrid17.set_row(i, [indexFld, "0", index, "0"])
	Dbfilter_Obm3dIdw_FormSet19_Nested2.set_field("GRID", DataGrid17.serialise())
	Dbfilter_Obm3dIdw_FormSet19_Nested2.set_field("COMBINE_EQ","0")
	Dbfilter_Obm3dIdw_FormSet19_Nested2.set_field("OR","1")
	Dbfilter_Obm3dIdw_FormSet19_Nested2.set_field("AND","0")
	Dbfilter_Obm3dIdw_FormSet19_Nested2.set_field("FTYPE","0")
	Dbfilter_Obm3dIdw_FormSet19_Nested2.set_field("FILE", locAssayPath)

	SearchEllipse_Obm3dIdw_FormSet19_Nested1= MMpy.FormSet("SEARCH_ELLIPSE","16.0.875.2")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("DIPDIR","111.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("SENSE","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("ROT_BOOL","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("3AXES_BOOL","1")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("PLUNGE_BOOL","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("PITCH_BOOL","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("PITCH","0.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("DIP","-0.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("SPHERIC_BOOL","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("2D","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("MIN_POINTS", str(minpts))
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("MAX_POINTS", str(maxpts))
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("SECTORS", str(sectors))
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("PLUNGE3","90.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("AZIMUTH3","111.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("PLUNGE2","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("AZIMUTH2","111.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("STRIKE","21.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("FACTOR3", str(factor3))
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("FACTOR2", str(factor2))
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("ROTATION","0.000")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("FACTOR1", str(factor1))
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("PLUNGE1","0")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("AZIMUTH1","21")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("ELLIPSE_BOOL","1")
	SearchEllipse_Obm3dIdw_FormSet19_Nested1.set_field("RADIUS", str(radius))

	Dbfilter_Obm3dIdw_FormSet19_Nested0= MMpy.FormSet("DBFILTER","16.0.875.2")
	DataGrid16 = MMpy.DataGrid(4,1)
	DataGrid16.set_column_info(0,1,4)
	DataGrid16.set_column_info(1,2,6)
	DataGrid16.set_column_info(2,3,-1)
	DataGrid16.set_column_info(3,4,14)
	DataGrid16.set_row(0, [BMFLD_MATERIAL,"0", oretype,"0"])
	Dbfilter_Obm3dIdw_FormSet19_Nested0.set_field("GRID",DataGrid16.serialise())
	Dbfilter_Obm3dIdw_FormSet19_Nested0.set_field("COMBINE_EQ","0")
	Dbfilter_Obm3dIdw_FormSet19_Nested0.set_field("OR","0")
	Dbfilter_Obm3dIdw_FormSet19_Nested0.set_field("AND","1")
	Dbfilter_Obm3dIdw_FormSet19_Nested0.set_field("FTYPE","0")
	Dbfilter_Obm3dIdw_FormSet19_Nested0.set_field("FILE", locBmPath)

	Obm3dIdw_FormSet19= MMpy.FormSet("OBM3D_IDW","16.0.875.2")
	Obm3dIdw_FormSet19.set_field("FLTBOOL","0")
	Obm3dIdw_FormSet19.set_field("STR_FTYPE","0")
	Obm3dIdw_FormSet19.set_field("STR_BOOL","1")
	Obm3dIdw_FormSet19.set_field("OUTL_BOOL","0")
	Obm3dIdw_FormSet19.set_field("NAME5","0")
	Obm3dIdw_FormSet19.set_field("NAME4","0")
	Obm3dIdw_FormSet19.set_field("NAME3","0")
	Obm3dIdw_FormSet19.set_field("NAME2","0")
	Obm3dIdw_FormSet19.set_field("NAME1","0")

	major, minor, servicepack, build = get_micromine_version()
	if major > 16:
		DataGrid0 = MMpy.DataGrid(5, 1)
		DataGrid0.set_column_info(0, 1, -1)
		DataGrid0.set_column_info(1, 2, -1)
		DataGrid0.set_column_info(2, 3, -1)
		DataGrid0.set_column_info(3, 4, -1)
		DataGrid0.set_column_info(4, 5, -1)
		DataGrid0.set_row(0, ["", "", "", "", ""])
		DataGrid0.set_row(1, ["", "", "", "", ""])
		DataGrid0.set_row(2, ["", "", "", "", ""])
		Obm3dIdw_FormSet19.set_field("EXTENTS_GRID", DataGrid0.serialise())

	Obm3dIdw_FormSet19.set_field("TYPE","0")
	Obm3dIdw_FormSet19.set_field("TEMPLATE_BOOL","0")
	Obm3dIdw_FormSet19.set_field("USE_DATASEARCH","0")
	Obm3dIdw_FormSet19.set_field("ANGLES","0")
	Obm3dIdw_FormSet19.set_field("ROTATION","1")
	Obm3dIdw_FormSet19.set_field("OBM3I_OBM_RL_FLD","ZCOLLAR")
	Obm3dIdw_FormSet19.set_field("OBM3I_OBM_N_FLD","YCOLLAR")
	Obm3dIdw_FormSet19.set_field("OBM3I_OBM_E_FLD","XCOLLAR")
	Obm3dIdw_FormSet19.set_field("NEIGHBOUR","0")
	Obm3dIdw_FormSet19.set_field("EXTENTS","0")
	Obm3dIdw_FormSet19.set_field("WEIGHTS","0")
	Obm3dIdw_FormSet19.set_field("LENGTH_FLD","LENGTH")
	DataGrid14 = MMpy.DataGrid(1,1)
	DataGrid14.set_column_info(0,0,4)
	DataGrid14.set_row(0, [element])
	Obm3dIdw_FormSet19.set_field("OBM_FIELDS_GRID",DataGrid14.serialise())
	DataGrid15 = MMpy.DataGrid(5,1)
	DataGrid15.set_column_info(0,0,4)
	DataGrid15.set_column_info(1,1,6)
	DataGrid15.set_column_info(2,2,-1)
	DataGrid15.set_column_info(3,3,-1)
	DataGrid15.set_column_info(4,4,-1)
	DataGrid15.set_row(0, ["","","","",""])
	Obm3dIdw_FormSet19.set_field("EXTRA_FIELDS_GRID",DataGrid15.serialise())
	Obm3dIdw_FormSet19.set_field("AUDIT_BOOL","0")
	Obm3dIdw_FormSet19.set_field("SECTORS","0")
	Obm3dIdw_FormSet19.set_field("CLOSEST_DISTANCE","0")
	Obm3dIdw_FormSet19.set_field("AVERAGE_DISTANCE","0")
	Obm3dIdw_FormSet19.set_field("REPORT","0")
	Obm3dIdw_FormSet19.set_field("ROTATION_FIELD", FIELD_ROT)
	Obm3dIdw_FormSet19.set_field("PLUNGE_FIELD", FIELD_DIP)
	Obm3dIdw_FormSet19.set_field("AZIMUTH_FIELD", FIELD_DIR)
	Obm3dIdw_FormSet19.set_field("SEARCH_BOOL","1")
	Obm3dIdw_FormSet19.set_field("ONLY","1")
	Obm3dIdw_FormSet19.set_field("FROM_FILE","1")
	Obm3dIdw_FormSet19.set_field("P_OBM_RL_FLD","RL")
	Obm3dIdw_FormSet19.set_field("P_OBM_N_FLD","NORTH")
	Obm3dIdw_FormSet19.set_field("P_OBM_E_FLD","EAST")
	Obm3dIdw_FormSet19.set_field("BM_FILTER","1")
	Obm3dIdw_FormSet19.set_field("BM_FLTNO",Dbfilter_Obm3dIdw_FormSet19_Nested0)
	Obm3dIdw_FormSet19.set_field("BM_FTYPE","0")
	Obm3dIdw_FormSet19.set_field("BM_FILE", locBmPath)
	Obm3dIdw_FormSet19.set_field("XX","1")
	Obm3dIdw_FormSet19.set_field("BLANK_MISSING","1")
	Obm3dIdw_FormSet19.set_field("CHAR_MISSING","1")
	Obm3dIdw_FormSet19.set_field("INDEX","0")
	Obm3dIdw_FormSet19.set_field("STDEV","0")
	Obm3dIdw_FormSet19.set_field("NUMS","0")
	Obm3dIdw_FormSet19.set_field("POWER","2")
	Obm3dIdw_FormSet19.set_field("SEARCH",SearchEllipse_Obm3dIdw_FormSet19_Nested1)
	Obm3dIdw_FormSet19.set_field("METHOD","0")
	Obm3dIdw_FormSet19.set_field("OUT_FTYPE","0")
	Obm3dIdw_FormSet19.set_field("OUT_FILE", locBmPath)
	Obm3dIdw_FormSet19.set_field("OUTLINE_BOOL","0")
	Obm3dIdw_FormSet19.set_field("OBM3I_FLTNO",Dbfilter_Obm3dIdw_FormSet19_Nested2)
	Obm3dIdw_FormSet19.set_field("OBM3I_FILTER","1")
	Obm3dIdw_FormSet19.set_field("OBM3I_FTYPE","0")
	Obm3dIdw_FormSet19.set_field("OBM3I_FILE", locAssayPath)
	Obm3dIdw_FormSet19.run()
#
def extrude_wireframe(tripath, triname, strpath, zoff):
	ExtrudeWireframeFromString_FormSet1= MMpy.FormSet("EXTRUDE_WIREFRAME_FROM_STRING","16.1.70.0")
	DataGrid0 = MMpy.DataGrid(3,1)
	DataGrid0.set_column_info(0,1,4)
	DataGrid0.set_column_info(1,2,-1)
	DataGrid0.set_column_info(2,0,8)
	DataGrid0.set_row(0, ["","",""])
	ExtrudeWireframeFromString_FormSet1.set_field("GRID",DataGrid0.serialise())
	ExtrudeWireframeFromString_FormSet1.set_field("ACCURACY","0")
	ExtrudeWireframeFromString_FormSet1.set_field("BOTTOM_BOOL","0")
	ExtrudeWireframeFromString_FormSet1.set_field("TOP_BOOL","0")
	ExtrudeWireframeFromString_FormSet1.set_field("COLOUR",MMpy.Colour(32,255,0).serialise())
	ExtrudeWireframeFromString_FormSet1.set_field("WIREFRAME_NAME", triname)
	ExtrudeWireframeFromString_FormSet1.set_field("WIREFRAME_TYPE", tripath)
	ExtrudeWireframeFromString_FormSet1.set_field("ORIENTATION","0")
	ExtrudeWireframeFromString_FormSet1.set_field("SELECT5","0")
	ExtrudeWireframeFromString_FormSet1.set_field("SELECT4","0")
	ExtrudeWireframeFromString_FormSet1.set_field("SELECT3","0")
	ExtrudeWireframeFromString_FormSet1.set_field("SELECT2","0")
	ExtrudeWireframeFromString_FormSet1.set_field("SELECT1","0")
	ExtrudeWireframeFromString_FormSet1.set_field("JOIN_FIELD","JOIN")
	ExtrudeWireframeFromString_FormSet1.set_field("Z_FIELD","RL")
	ExtrudeWireframeFromString_FormSet1.set_field("Y_FIELD","NORTH")
	ExtrudeWireframeFromString_FormSet1.set_field("X_FIELD","EAST")
	ExtrudeWireframeFromString_FormSet1.set_field("FILTER","0")
	ExtrudeWireframeFromString_FormSet1.set_field("FILE_TYPE","2")
	ExtrudeWireframeFromString_FormSet1.set_field("STRING_FILE", strpath)
	ExtrudeWireframeFromString_FormSet1.set_field("OUTLINE_FILE_BOOL","0")
	ExtrudeWireframeFromString_FormSet1.set_field("STRING_FILE_BOOL","1")
	ExtrudeWireframeFromString_FormSet1.set_field("VIZEX_BOOL","0")
	ExtrudeWireframeFromString_FormSet1.set_field("VIEW","2")
	ExtrudeWireframeFromString_FormSet1.set_field("TRANSFORM_BOOL","0")
	ExtrudeWireframeFromString_FormSet1.set_field("ORTHOGONAL_BOOL","1")
	ExtrudeWireframeFromString_FormSet1.set_field("WFTYPE","Выработки")
	ExtrudeWireframeFromString_FormSet1.set_field("ENDS_BOOL","1")
	ExtrudeWireframeFromString_FormSet1.set_field("FORWARD_DISTANCE", str(zoff))
	ExtrudeWireframeFromString_FormSet1.run()
#
def assign_string(strpath, bmpath, direction):
	direction = direction.upper()

	StringAssign_FormSet2= MMpy.FormSet("STRING_ASSIGN","16.1.70.0")
	StringAssign_FormSet2.set_field("FACTORS_BOOL","0")
	StringAssign_FormSet2.set_field("FIELD_BOOL","0")
	DataGrid0 = MMpy.DataGrid(4,1)
	DataGrid0.set_column_info(0,1,6)
	DataGrid0.set_column_info(1,2,4)
	DataGrid0.set_column_info(2,3,-1)
	DataGrid0.set_column_info(3,4,4)
	DataGrid0.set_row(0, ["1","","Y", "%s_" % direction])
	StringAssign_FormSet2.set_field("GRID",DataGrid0.serialise())
	StringAssign_FormSet2.set_field("MODEL","0")
	StringAssign_FormSet2.set_field("DATA","1")
	StringAssign_FormSet2.set_field("OVERWRITE_TARGET_FIELDS","0")
	StringAssign_FormSet2.set_field("CLEAR_TARGET_FIELDS","0")
	StringAssign_FormSet2.set_field("OUTPUT_FILE_FILTER_BOOL","0")
	StringAssign_FormSet2.set_field("OUTPUT_FILE_TYPE","0")
	StringAssign_FormSet2.set_field("OUTPUT_FILE", bmpath)
	StringAssign_FormSet2.set_field("JOIN_FIELD","JOIN")
	if direction == "PLAN":
		StringAssign_FormSet2.set_field("OUTPUT_FILE_Y_FIELD","NORTH")
		StringAssign_FormSet2.set_field("OUTPUT_FILE_X_FIELD","EAST")
		StringAssign_FormSet2.set_field("INPUT_FILE_Y_FIELD","NORTH")
		StringAssign_FormSet2.set_field("INPUT_FILE_X_FIELD","EAST")
	if direction == "NORTH":
		StringAssign_FormSet2.set_field("OUTPUT_FILE_Y_FIELD","RL")
		StringAssign_FormSet2.set_field("OUTPUT_FILE_X_FIELD","EAST")
		StringAssign_FormSet2.set_field("INPUT_FILE_Y_FIELD","RL")
		StringAssign_FormSet2.set_field("INPUT_FILE_X_FIELD","EAST")
	if direction == "WEST":
		StringAssign_FormSet2.set_field("OUTPUT_FILE_Y_FIELD","RL")
		StringAssign_FormSet2.set_field("OUTPUT_FILE_X_FIELD","NORTH")
		StringAssign_FormSet2.set_field("INPUT_FILE_Y_FIELD","RL")
		StringAssign_FormSet2.set_field("INPUT_FILE_X_FIELD","NORTH")
	StringAssign_FormSet2.set_field("INPUT_FILE_FILTER_BOOL","0")
	StringAssign_FormSet2.set_field("INPUT_FILE_TYPE","2")
	StringAssign_FormSet2.set_field("INPUT_FILE", strpath)
	StringAssign_FormSet2.run()

def get_wireframes_extents(tripath, wfnames):
	import sqlite3

	ret = {"xmin": None, "xmax": None, "ymin": None, "ymax": None, "zmin": None, "zmax": None}

	cnxn = sqlite3.connect(tripath)
	cursor = cnxn.cursor()
	cmd = """
		SELECT MIN(XMinimum), MAX(XMaximum), MIN(YMinimum), MAX(YMaximum), MIN(ZMinimum), MAX(ZMaximum)
		FROM GeneralInformation
		WHERE Name in ('%s')""" % "','".join(wfnames)
	cursor.execute(cmd)
	extents = cursor.fetchone()
	cnxn.close()

	if extents:
		xmin, xmax, ymin, ymax, zmin, zmax = extents
		ret["xmin"], ret["xmax"] = xmin, xmax
		ret["ymin"], ret["ymax"] = ymin, ymax
		ret["zmin"], ret["zmax"] = zmin, zmax	
	return ret

def wireframe_silhouette(wfset, strpath, direction, startvalue):
	formset = MMpy.FormSet("WIREFRAME_SILHOUETTE","16.1.70.0")
	formset.set_field("AUTOLOAD","0")
	formset.set_field("SET", wfset)
	formset.set_field("SET_BOOL","1")
	formset.set_field("SINGLE_BOOL","0")

	formset.set_field("STR_SWITCH","2")
	if direction.upper() == "WEST":
		formset.set_field("ORT_SWITCH","0")
		formset.set_field("X", str(startvalue))
	elif direction.upper() == "NORTH":
		formset.set_field("ORT_SWITCH","1")
		formset.set_field("Y", str(startvalue))
	elif direction.upper() == "PLAN":
		formset.set_field("ORT_SWITCH","2")
		formset.set_field("Z", str(startvalue))

	formset.set_field("STR_NAME", strpath, MMpy.append_flag.none)
	formset.set_field("TRANS_BOOL","0")
	formset.set_field("ORT_BOOL","1")
	formset.run()

def create_dtm_from_string(strpath, typepath, dtmname):

	DtmCreate_FormSet1= MMpy.FormSet("DTM_CREATE","16.1.1161.0")
	DtmCreate_FormSet1.set_field("ACCURACY","0")
	DataGrid0 = MMpy.DataGrid(2,1)
	DataGrid0.set_column_info(0,1,-1)
	DataGrid0.set_column_info(1,0,8)
	DataGrid0.set_row(0, ["",""])
	DtmCreate_FormSet1.set_field("GRID",DataGrid0.serialise())
	DtmCreate_FormSet1.set_field("TYPE",typepath)
	DtmCreate_FormSet1.set_field("COLOUR",MMpy.Colour(0,0,100).serialise())
	DtmCreate_FormSet1.set_field("NAME",dtmname)
	DtmCreate_FormSet1.set_field("SW","0")
	DtmCreate_FormSet1.set_field("EDGES","0")
	DtmCreate_FormSet1.set_field("EXTCONSTPTSBOOL","0")
	DtmCreate_FormSet1.set_field("FLTBOOL","0")
	DtmCreate_FormSet1.set_field("SELECT5","0")
	DtmCreate_FormSet1.set_field("SELECT4","0")
	DtmCreate_FormSet1.set_field("SELECT3","0")
	DtmCreate_FormSet1.set_field("SELECT2","0")
	DtmCreate_FormSet1.set_field("SELECT1","0")
	DtmCreate_FormSet1.set_field("OUTLINEBOOL","0")
	DtmCreate_FormSet1.set_field("STRINGBOOL","1")
	DtmCreate_FormSet1.set_field("DTMBDY_FTYPE","0")
	DtmCreate_FormSet1.set_field("FACETS","0")
	DtmCreate_FormSet1.set_field("WFCOLOUR",MMpy.Colour(0,0,100).serialise())
	DtmCreate_FormSet1.set_field("ORTHPLANE","0")
	DtmCreate_FormSet1.set_field("PLANEFROMCAMERA","0")
	DtmCreate_FormSet1.set_field("PLANEFROMINPUT","0")
	DtmCreate_FormSet1.set_field("PLANEFROMTRANS","0")
	DtmCreate_FormSet1.set_field("PLANEFROMORTHO","1")
	DtmCreate_FormSet1.set_field("DEFINEPLANE","0")
	DtmCreate_FormSet1.set_field("CRESTSPURS","1")
	DtmCreate_FormSet1.set_field("VALLEYSPURS","1")
	DtmCreate_FormSet1.set_field("KEYSPURS","1")
	DtmCreate_FormSet1.set_field("GENERATESPURS","0")
	DtmCreate_FormSet1.set_field("VIZEX","0")
	DtmCreate_FormSet1.set_field("JOINFLD","JOIN")
	DtmCreate_FormSet1.set_field("YVAR","NORTH")
	DtmCreate_FormSet1.set_field("XVAR","EAST")
	DtmCreate_FormSet1.set_field("CONTOURS","1")
	DtmCreate_FormSet1.set_field("WFTYPE",typepath)
	DtmCreate_FormSet1.set_field("WFNAME",dtmname)
	DtmCreate_FormSet1.set_field("DTMCREATE_ZFLD","RL")
	DtmCreate_FormSet1.set_field("FILTER","0")
	DtmCreate_FormSet1.set_field("DTMCREATE_FTYPE","2")
	DtmCreate_FormSet1.set_field("MMFILE",strpath)

	DtmCreate_FormSet1.run()

def getDynamicDensity(header, row, geocodes, idxElements, idxMaterial, idxIndex ):	
	"""
	getDynamicDensity - функция возвращает динаически рассчитанный объёмный вес
	header - заголовки полей БМ
	row  - список значений  строки БМ, для которой рассчитываем объёмный вес
	geocodes  - геологические коды, индексы, плотности, коэффициенты учавствующие в рассчёте
	idxElements - словарь {'хим. элемент': индекс поля в БМ}
	idxMaterial - индекс поля материала БМ
	idxIndex - индекс поля  индекса БМ
	"""
	material = row[idxMaterial]
	geoindex = row[idxIndex]

	if material == 'ЗАКЛ':
		return

	material_branch_list = list(filter(lambda x: x[0][0] == material,geocodes))
	if len(material_branch_list) > 1:
		raise Exception("В справочнике более одного  материала с одинаковым кодом. %s" % material)
	if len(material_branch_list) == 0:
		raise Exception("В справочнике отсутствует материал. %s" % material)
	material_branch = material_branch_list[0]

	bm_branch = material_branch[1]['BM']
	index_branch_list = list(filter(lambda x: x[0][0] == geoindex,bm_branch))
	if len(index_branch_list) > 1:
		raise Exception("В справочнике более одного индекса %s принадлежащего одному материалу %s с одинаковым кодом. " % (geoindex,material))
	if len(index_branch_list) == 0:
		raise Exception("В справочнике отсутствует индекс %s принадлежащий  материалу %s. " % (geoindex,material))
	index_branch = index_branch_list[0]

	# Индекс, плотность, плотность породы
	bm_index, density_str, wst_density_str = index_branch[0]
	try:
		wst_density = float(wst_density_str)
	except:
		if is_blank(wst_density_str):
			wst_density = 0
		else:
			raise Exception("Материал %s, индекс %s имеют некоретное значение плотности породы" % (material, geoindex))

	sprElements = index_branch[1]['FACTOR']

	dynamic_density = wst_density

	for idxElement in idxElements:

		element_branch_list = list(filter(lambda x: x[0] == idxElement, sprElements))
		if len(element_branch_list) > 1:
			raise Exception("В справочнике более одного элемента %s принадлежащего одному индексу %s и материалу %s с одинаковым кодом" % (idxElement, index, material))
		elif len(element_branch_list) == 0:
			continue
		else:
			element_item = element_branch_list[0]
			if len(element_item) < 3:
				raise Exception("В справочнике коэффициентов динамической плотности для элементов не хватает значений, пересохраните свои настройки.")
			factor_square_str = element_item[1]
			factor_str = element_item[2]
				
		try:	factor_square_val = float(factor_square_str)
		except:
			if is_blank(factor_square_str): 
				continue
			else:
				raise Exception("Элемент %s, материал %s, индекс %s имеет некоретное значение коэффициента ᶄ₁" % (idxElement, material, index))		
			
		try:	factor_val = float(factor_str)
		except:
			if is_blank(factor_str): 
				continue
			else:
				raise Exception("Элемент %s, материал %s, индекс %s имеет некоретное значение коэффициента ᶄ₂" % (idxElement, material, index))
			
		sample_str = row[idxElements[idxElement]]


		try:	sample_val = float(sample_str)
		except:	continue

		if math.isnan(sample_val):
			continue

		dynamic_density +=  (factor_square_val * sample_val ** 2 ) + (factor_val * sample_val)

	return dynamic_density

class GeoUpdateBm(ProcessWindow):
	def __init__(self, *args, **kw):
		super(GeoUpdateBm, self).__init__(*args, **kw)

	def processAssayFile(self, path, elements, topCutGrades):
		progress = self.main_app.mainWidget.progressBar
		value = progress.value()
		progress.setRange(0, 0)

		f = MicromineFile()
		if not f.open(path):
			raise Exception("Невозможно открыть файл '%s'" % path)
		
		for element, topCutGrade in zip(elements, topCutGrades):
			if topCutGrade is None:
				continue
			#
			iCol = f.get_field_id(element)
			if iCol == -1:
				continue
				# raise Exception("Столбец '%s' отсутствует в файле опробования" % element)

			for iRow in range(f.records_count):
				v = f.get_num_field_value(iCol, iRow+1)
				if v > topCutGrade:
					f.set_field_value(iCol, iRow+1, topCutGrade)
				if iRow % 500 == 0:
					QtCore.QCoreApplication.processEvents()
		f.close()
		
		progress.setRange(0, 100)
		progress.setValue(value)

	# подготовка к запуску
	def prepare(self):
		if not super(GeoUpdateBm, self).prepare():
			return

		# считывание списка моделей
		modelItems = self.selectedItems()[ITEM_MODEL]
		if not modelItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите геологические модели")
			return

		#
		self._replies = []
		for modelItem in modelItems:
			bmpath = modelItem.filePath()
			if self.lockFileExists(bmpath):
				lockpath = self.getLockPath(bmpath)
				qtmsgbox.show_error(self.main_app, "Ошибка", "Блочная модель используется пользователем '%s'" % get_file_owner(lockpath))
				return
			# проверка метаданных
			net_bm = BlockModel(bmpath, self.main_app)
			if net_bm.has_metadata and not os.path.exists(bmpath):
				msg = "Выбранная блочная модель была ранее создана, но файл отсутствует. Перезаписать метаданные блочной модели?"
				self._replies.append(qtmsgbox.show_question(self.main_app, "", msg))
		
		# self.run()
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	# выполнение
	def run(self):
		if not super(GeoUpdateBm, self).run():
			return

		progress = self.main_app.mainWidget.progressBar

		### =================================================================================== ###
		### ============================= ИНИЦИАЛИЗАЦИЯ ПЕРЕМЕННЫХ ============================ ###
		### =================================================================================== ###

		#
		netExpAssayPath = os.path.join(self._wrkpath, self.main_app.settings.getValue(ID_DBDIR), self.main_app.settings.getValue(ID_EXPASSAY))
		locExpAssayPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netExpAssayPath))

		indexFld = self.main_app.settings.getValue(ID_IDXFLD)
		idwElements = self.getIdwElements()
		# элементы для заполнения
		elemToFill = [elem[0] for elem in self.main_app.settings.getValue(ID_IDWELEM) if elem[3] == 1]
		topCutGrades = list(zip(*self.main_app.settings.getValue(ID_IDWELEM)))[-1]

		xBlockSize, yBlockSize, zBlockSize = int(self.main_app.settings.getValue(ID_XSIZE)), int(self.main_app.settings.getValue(ID_YSIZE)), int(self.main_app.settings.getValue(ID_ZSIZE))
		xSubBlockSize, ySubBlockSize, zSubBlockSize = int(self.main_app.settings.getValue(ID_SUBXGEO)), int(self.main_app.settings.getValue(ID_SUBYGEO)), int(self.main_app.settings.getValue(ID_SUBZGEO))

		zoffset = int(self.main_app.settings.getValue(ID_ZOFFSET))

		allOreCodes, allOreDescr = [], []
		for (oreCode, oreDescription, oreType), assayAndBmIndexes in self.main_app.settings.getValue(ID_GEOCODES):
			allOreCodes.append(oreCode)
			allOreDescr.append(oreDescription)
		
		emptyCode = self.getGeoEmptyCode()								# МАТЕРИАЛ для вмещающих пород
		emptyIndex, emptyDensity, emptyDensityWaste = self.getGeoEmptyIndexAndDensity()	# КОД и ПЛОТНОСТЬ для вмещающих пород

		sysdate = datetime(datetime.now().year, datetime.now().month, 1)

		modelItems = self.selectedItems()[ITEM_MODEL]
		# создание LCK файлов
		self.createLockFiles([modelItem.filePath() for modelItem in modelItems])

		

		### =================================================================================== ###
		### ======================= НАСТРОЙКА ДОПОЛНИТЕЛЬНЫХ ПАРАМЕТРОВ ======================= ###
		### =================================================================================== ###

		# задаем параметры диалога
		geoUpdateBmDialog = GeoUpdateBmDialog(self.main_app)

		# параметры интерполяции
		listModelItems = list(modelItem.toListTreeItem() for modelItem in modelItems)
		geoUpdateBmDialog.frameInteroplation.setModels(listModelItems)
		#
		idwParamIds = (ID_IDWSECTOR, ID_IDWMINPTS, ID_IDWMAXPTS, ID_IDWFACTOR1, ID_IDWFACTOR2, ID_IDWFACTOR3, ID_IDWRADIUS)
		idwModelParams = {listModelItem.filePath(): {paramId: self.main_app.settings.getValue(paramId) for paramId in idwParamIds} for listModelItem in listModelItems}
		for listModelItem in listModelItems:
			geoUpdateBmDialog.frameInteroplation.setModelParams(listModelItem, idwModelParams[listModelItem.filePath()])
		#
		# параметры ураганных содержаний
		geoUpdateBmDialog.frameTopCutGrade.gridTopCutGrade.setHeader(idwElements)
		geoUpdateBmDialog.frameTopCutGrade.gridTopCutGrade.insertDefaultRow()
		geoUpdateBmDialog.frameTopCutGrade.gridTopCutGrade.setDefaultData(topCutGrades)
		geoUpdateBmDialog.frameTopCutGrade.gridTopCutGrade.insertUserRow()
		#
		# параметры геологии
		# ксенолиты
		geoUpdateBmDialog.frameGeologyParams.xenolithTree.setValues([BlockModel(modelItem.filePath(), self.main_app).tri_wireframes_path for modelItem in modelItems])
		# вмещающие породы
		geoUpdateBmDialog.frameGeologyParams.setEmptyRockCodeValues(sorted(allOreDescr), dict(zip(allOreCodes, allOreDescr))[emptyCode])
		#
		# детальная разведка
		#geoUpdateBmDialog.frameDetailedDhdb.setDrillholes(detDhNames)
		#
		# параметры
		#geoUpdateBmDialog.frameOptions.setParams({ID_ZOFFSET: self.main_app.settings.getValue(ID_ZOFFSET)})
		listModelItemsZ = list(modelItem.toListTreeItem() for modelItem in modelItems)
		geoUpdateBmDialog.frameOptions.setModels(listModelItemsZ)
		for listModelItem in listModelItemsZ:
			bm = BlockModel(listModelItem._filePath, self.main_app)
			if  bm.has_metadata:
				params = { 'zmin' : bm.metadata.zmin, 'zmax' : bm.metadata.zmax, 'expansion' : bm.metadata.expansion, 'isUseDynamicDensity' : bm.metadata.is_use_dynamic_density}
			else:
				params = { 'zmin' : None, 'zmax' : None,  'expansion' : zoffset, 'isUseDynamicDensity' : False}
			geoUpdateBmDialog.frameOptions.setModelParams(listModelItem, params )
		#
		geoUpdateBmDialog.exec()
		if geoUpdateBmDialog.is_cancelled:
			self._is_cancelled = True
			return

		# считываем параметры интерполяции
		for listModelItem in listModelItems:
			idwModelParams[listModelItem.filePath()] = geoUpdateBmDialog.frameInteroplation.modelParams(listModelItem)
			idwModelParams[listModelItem.filePath()][ID_IDWRADIUS] = list(zip(*idwModelParams[listModelItem.filePath()][ID_IDWRADIUS]))[0]
		# считываем параметры ураганных содержаний
		topCutGrades = geoUpdateBmDialog.frameTopCutGrade.gridTopCutGrade.userData()
		# считываем вмещающие породы
		# qtmsgbox.show_error(self.main_app, "", "OLD: %s" % emptyCode)
		emptyCode = dict(zip(allOreDescr, allOreCodes))[geoUpdateBmDialog.frameGeologyParams.getEmptyRockCode()]
		# qtmsgbox.show_error(self.main_app, "", "NEW: %s" % emptyCode)
		# qtmsgbox.show_error(self.main_app, "", "OLD: %s || %s" % (str(emptyIndex), str(emptyDensity)))
		for (oreCode, oreDescription, oreType), assayAndBmIndexes in self.main_app.settings.getValue(ID_GEOCODES):
			if oreCode != emptyCode:
				continue
			emptyBmIndexAndDensity = assayAndBmIndexes["BM"][0]
			emptyIndex, emptyDensity, emptyDensityWaste = (None, None, None) if not emptyBmIndexAndDensity else emptyBmIndexAndDensity[0]
		# qtmsgbox.show_error(self.main_app, "", "NEW: %s || %s" % (str(emptyIndex), str(emptyDensity)))
		# return

		# считываем ксенолиты
		xenoliths = {}
		useXenolith = geoUpdateBmDialog.frameGeologyParams.groupBoxXenolith.isChecked()
		if useXenolith:
			values = geoUpdateBmDialog.frameGeologyParams.xenolithTree.values()
			for tripath, triname in values:
				if not tripath in xenoliths:
					xenoliths[tripath] = []
				xenoliths[tripath].append(triname)
		else:
			for modelItem in modelItems:
				xenoliths[BlockModel(modelItem.filePath(), self.main_app).tri_wireframes_path] = []
		# детальная разведка
		detailedDrillholeSamples = geoUpdateBmDialog.frameDetailedDhdb.getSelectedDetailedDrillholeSamples()
		detailedTrenchesSamples = geoUpdateBmDialog.frameDetailedDhdb.getSelectedTrenchSamples()
		idwAddSamplesData = [detailedDrillholeSamples, detailedTrenchesSamples]


		#
		oreCodes, oreTypes, oreIndexes = [], [], []
		for (oreCode, oreDescription, oreType), assayAndBmIndexes in self.main_app.settings.getValue(ID_GEOCODES):
			if oreCode == emptyCode or oreType > 2:
				continue
			# if not oreType in [0,1]:
			# 	continue
			#
			oreCodes.append(oreCode)
			oreTypes.append(oreType)
			oreIndexes.append([row[0] for row in assayAndBmIndexes["ASSAY"]])

		oreIndexesDict = dict(zip(oreCodes, oreIndexes))
		oreTypesDict = dict(zip(oreCodes, oreTypes))
	
		oreCodesOrder = [i+1 for i in range(len(oreCodes))]
		oreCodesOrderDict = dict(zip(oreCodes, oreCodesOrder))

		#
		progress.setRange(0,0)

		### =================================================================================== ###
		### =========================== ПРОВЕРКА АТРИБУТОВ КАРКАСОВ =========================== ###
		### =================================================================================== ###

		# глобальная проверка атрибутов каркасов перед запуском процесса обновления
		geoTriPaths = [BlockModel(modelItem.filePath(), self.main_app).tri_wireframes_path for k, modelItem in enumerate(modelItems)]
		try:
			progress_value = progress.value()
			wfvalidation = GeoWireframeAttributeValidation(self.main_app)
			wfvalidation.isSilent = True
			wfvalidation.exec(filelist = geoTriPaths)
			if not wfvalidation.result:
				return
			progress.setValue(progress_value)
		except Exception as e:
			raise Exception("Невозможно произвести проверку атрибутов каркасов. %s" % str(e))

		### =================================================================================== ###
		### ======================= СЧИТЫВАНИЕ ТИПОВ РУДЫ ИЗ КАРКАСОВ ========================= ###
		### =================================================================================== ###
		
		progress.setText("Считывание атрибутов каркасов")

		oreCodesDict = {}
		for modelItem, geoTriPath in zip(modelItems, geoTriPaths):
			netBmPath = modelItem.filePath()
			net_bm = BlockModel(netBmPath, self.main_app)

			oreCodesDict[netBmPath] = oreCodes.copy()

			tridb = Tridb(geoTriPath)
			codes = set()
			for wfname in tridb.wireframe_names:
				wireframe = tridb[wfname]
				code = wireframe.user_attribute(GEO_ATTR_MATERIAL)
				if code in oreCodes:
					codes.add(code)
			tridb.close()

			missingCodes = list(set(oreCodes).difference(codes))
			if missingCodes:
				msg = "В каркасах модели '%s' отсутствуют следующие коды: %s. Продолжить процесс создания блочной модели?" % (net_bm.name, ", ".join(missingCodes))
				if not qtmsgbox.show_question(self.main_app, "Внимание", msg):
					self._is_cancelled = True
					return
				else:
					oreCodesDict[netBmPath] = list(codes)
			del missingCodes



		### =================================================================================== ###
		### ===================== КОПИРОВАНИЕ ФАЙЛА ОПРОБОВАНИЯ ДЛЯ IDW ======================= ###
		### =================================================================================== ###

		progress.setText("Копирование опробования")
		err = self.exec_proc(self.copy_file, netExpAssayPath, locExpAssayPath)
		if err:					raise Exception("Невозможно скопировать файл %s. %s" % (netExpAssayPath, err))
		if self.is_cancelled:	return

		progress.setText("Анализ файла опробования")
		err = self.exec_proc(self.processAssayFile, locExpAssayPath, idwElements, topCutGrades)
		if err:					raise Exception("Невозможно применить ограничение ураганных содержаний. %s" % err)
		if self.is_cancelled:	return

		### =================================================================================== ###
		### ======= ОБЪЕДИНЕНИЕ ФАЙЛА ОПРОБОВАНИЯ С ФАЙЛАМИ ДЕТАЛЬНОЙ РАЗВЕДКИ И БОРОЗД ======= ###
		### =================================================================================== ###

		locExpAssayFile = MicromineFile()
		if not locExpAssayFile.open(locExpAssayPath):
			raise Exception("Невозможно открыть файл '%s'" % locExpAssayPath)
		locExpAssayHeader = locExpAssayFile.header
		#
		for idwAddSamplesItem in idwAddSamplesData:
			locDetAssayHeader = idwAddSamplesItem['header']
			locDetAssayData = idwAddSamplesItem['data']
			locExpColumnIndexes = [locExpAssayHeader.index(columnName) if columnName in locExpAssayHeader else None for columnName in locDetAssayHeader]

			# присоединяем данные
			for detRowIndex in range(len(locDetAssayData)):

				locExpAssayFile.add_record()
				for detColumnIndex, (expColumnIndex, columnName) in enumerate(zip(locExpColumnIndexes, locDetAssayHeader)):
					if not columnName in locExpAssayHeader or expColumnIndex is None:
						continue
					value = locDetAssayData [detRowIndex] [detColumnIndex]
					locExpAssayFile.set_field_value(expColumnIndex, locExpAssayFile.records_count, value)
		locExpAssayFile.close()

		### =================================================================================== ###
		### ========================== СОЗДАНИЕ/ОБНОВЛЕНИЕ БЛОЧНЫХ МОДЕЛЕЙ ==================== ###
		### =================================================================================== ###

		for k, (listModelItem, modelItem, geoTriPath, listModelItemZ) in enumerate(zip(listModelItems, modelItems, geoTriPaths, listModelItemsZ)):
			netBmPath = modelItem.filePath()
			net_bm = BlockModel(netBmPath, self.main_app)

			locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % net_bm.name)
			bkpBmPath = net_bm.backup_path
			# locBmPathOpt = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_OPT.DAT" % net_bm.name)
			locBmPathMerged = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_MERGED.DAT" % net_bm.name)

			netStrBoundsPath = net_bm.str_bounds_path
			locStrBoundsPath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(netStrBoundsPath))


			prefix = self.main_app.settings.getValue(ID_GEO_UPREFIX)
			layer = net_bm.layer
			unit = net_bm.unit[len(prefix):]

			# флаг, что блочная модель создается
			bm_existed = os.path.exists(netBmPath)
			if bm_existed:
				currdate = date_to_fieldname(net_bm.blockmodel_date)
			# Update 15 chars
				f = MicromineFile()
				if not f.open(netBmPath):
					raise Exception("Невозможно открыть файл '%s'" % netBmPath)
				if BMFLD_LAYER in f.header:
					fld_id = f.get_field_id(BMFLD_LAYER)
					structure = f.structure
					structure.set_field_width(fld_id, 15)
					f.structure = structure
				f.close()
			else:
				currdate = date_to_fieldname(sysdate)
			prevdate = prev_date(currdate, "str")

			#
			params = geoUpdateBmDialog.frameOptions.modelParams(listModelItemZ)
			
			zoffset = int(params['expansion']) if params['expansion'] != 'None' else None
			  

			#
			if net_bm.has_metadata and not bm_existed and not self._replies[k]:
				continue

			### =================================================================================== ###
			### ================ ФОРМИРОВАНИЕ ДАННЫХ ДЛЯ СОЗДАНИЯ НАБОРА КАРКАСОВ ================= ###
			### =================================================================================== ###

			progress.setText("Формирование набора каркасов")

			wfdata = []
			xenodata = []
			tridb = Tridb(geoTriPath)
			wireframe_names = tridb.wireframe_names
			for orecode in oreCodesDict[netBmPath] + [emptyCode]:
				for wfname in wireframe_names:
					wf = tridb[wfname]
					if xenoliths and geoTriPath in xenoliths and wfname in xenoliths[geoTriPath] and wf.user_attribute(BMFLD_MATERIAL) == orecode:
						xenodata.append([geoTriPath, DEFAULT_ATTR_NAME, wfname, oreCodesOrderDict[orecode] if orecode in oreCodesOrderDict else len(oreCodes)+1])
					elif orecode != emptyCode:
						if wf.user_attribute(BMFLD_MATERIAL) == orecode:
							wfdata.append([geoTriPath, DEFAULT_ATTR_NAME, wfname, oreCodesOrderDict[orecode]])

			tridb.close()

			# сортируем в обратном порядке (от нерудных до богатых)
			wfdata.sort(key = lambda x: x[-1], reverse = True)
			wfdata = [row[:-1] for row in wfdata]
			# добавляем ксенолиты в конец, если имеются
			if xenoliths:
				xenodata.sort(key = lambda x: x[-1], reverse = True)
				wfdata.extend([row[:-1] for row in xenodata])

			### =================================================================================== ###
			### ============ СОЗДАНИЕ РЕЗЕРВНОЙ КОПИИ БЛОЧНОЙ МОДЕЛИ В СЕТЕВОМ ПРОЕКТЕ ============ ###
			### =================================================================================== ###

			if bm_existed:
				progress.setText("Создание резервной копии БМ")
				err = self.exec_proc(self.copy_file, netBmPath, bkpBmPath)
				if err:					raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
				if self.is_cancelled:	return

			### =================================================================================== ###
			### =========== КОПИРОВАНИЕ ФАЙЛА ГРАНИЦ БЛОЧНОЙ МОДЕЛИ В ЛОКАЛЬНЫЙ ПРОЕКТ ============ ###
			### =================================================================================== ###

			progress.setText("Копирование границ БМ")
			err = self.exec_proc(self.copy_file, netStrBoundsPath, locStrBoundsPath)
			if err:					raise Exception("Ошибка", "Невозможно скопировать файл %s. %s" % (netStrBoundsPath, err))
			if self.is_cancelled:	return

			### =================================================================================== ###
			### =================== ОПРЕДЕЛЕНИЕ ГРАНИЦ, КРАТНЫХ РАЗМЕРУ БЛОКА ===================== ###
			### =================================================================================== ###

			wfextents = get_wireframes_extents(geoTriPath, [row[-1] for row in wfdata])

			# определение границ, кратных размеру блока
			xmin = int(wfextents["xmin"]) if wfextents["xmin"] else None
			xmax = int(wfextents["xmax"]) if wfextents["xmax"] else None
			ymin = int(wfextents["ymin"]) if wfextents["ymin"] else None
			ymax = int(wfextents["ymax"]) if wfextents["ymax"] else None
			zmin = int(wfextents["zmin"]) - zoffset if wfextents["zmin"] else None
			zmax = int(wfextents["zmax"]) + zoffset if wfextents["zmax"] else None

			if xmin is None or xmax is None:	raise Exception("Невозможно определить границы модели по оси X")
			if ymin is None or ymax is None:	raise Exception("Невозможно определить границы модели по оси Y")
			if zmin is None or zmax is None:	raise Exception("Невозможно определить границы модели по оси Z")

			### =================================================================================== ###
			### ============================= ОПРЕДЕЛЕНИЕ ГРАНИЦ В ПЛАНЕ ========================== ###
			### =================================================================================== ###

			f = MicromineFile()
			if not f.open(locStrBoundsPath):
				raise Exception("Невозможно открыть файл '%s'" % locStrBoundsPath)
			if f.records_count < 2:
				raise Exception("Файл границ '%s' содержит неверное число точек" % locStrBoundsPath)
			xx, yy = zip(*f.read(columns = ["EAST", "NORTH"]))
			xmin, xmax = min(xx), max(xx)
			ymin, ymax = min(yy), max(yy)
			f.close()

			xmin = xmin - (xBlockSize - xmin % xBlockSize)
			xmax = xmax + (xBlockSize - xmax % xBlockSize)
			ymin = ymin - (yBlockSize - ymin % yBlockSize)
			ymax = ymax + (yBlockSize - ymax % yBlockSize)
			zmin = zmin - (zBlockSize - zmin % zBlockSize)
			zmax = zmax + (zBlockSize - zmax % zBlockSize)

			zoff = zmax - zmin

			### =================================================================================== ###
			### ================================= СОЗДАНИЕ СОЛИДА ================================= ###
			### =================================================================================== ###

			progress.setText("Создание солида, ограничивающего блочную модель")
			# опускаем границы на нижнюю отметку
			f = MicromineFile()
			if not f.open(locStrBoundsPath):
				raise Exception("Невозможно открыть файл '%s'" % locStrBoundsPath)
			iz = f.get_field_id("RL")
			for i in range(f.records_count):
				f.set_field_value(iz, i+1, zmin)
			f.close()

			triSolidPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "SOLID.TRIDB")
			Tridb.create(triSolidPath)
			triSolidName = "SOLID"
			try:					extrude_wireframe(triSolidPath, triSolidName, locStrBoundsPath, zoff)
			except Exception as e:	raise Exception("Невозможно создать солид")

			### =================================================================================== ###
			### ======================== СОЗДАНИЕ ПУСТОЙ БЛОЧНОЙ МОДЕЛИ =========================== ###
			### =================================================================================== ###

			progress.setText("Создание пустой БМ")

			bmparams = {}
			bmparams["bmpath"] = locBmPath
			bmparams["tripath"] = triSolidPath
			bmparams["triname"] = triSolidName
			unit_name = self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper()
			if is_blank(unit_name):
				raise Exception("Невозможно создать блочну юмодель. Не задано имя юнита.")
			bmparams["fields"] = ["%s:C:10:0:%s" % (unit_name, unit), "%s:C:15:0:%s" % (BMFLD_LAYER, layer)]
			bmparams["xmin"], bmparams["xmax"] = xmin, xmax
			bmparams["ymin"], bmparams["ymax"] = ymin, ymax
			bmparams["zmin"], bmparams["zmax"] = zmin, zmax
			bmparams["xsize"] = xBlockSize
			bmparams["ysize"] = yBlockSize
			bmparams["zsize"] = zBlockSize
			bmparams["sub_xsize"] = xSubBlockSize
			bmparams["sub_ysize"] = ySubBlockSize
			bmparams["sub_zsize"] = zSubBlockSize
			blank_bm = BlockModel(locBmPath, self.main_app)
			try:					blank_bm.create_blank(bmparams)
			except Exception as e:	raise Exception("Невозможно создать пустую блочную модель. %s" % str(e))
			# os.remove(triSolidPath)

			### =================================================================================== ###
			### ==================== ОБРЕЗАЕМ БЛОЧНУЮ МОДЕЛЬ ПО КОНТУРУ КАРКАСОВ ================== ###
			### =================================================================================== ###
			
			progress.setText("Удаление лишних блоков")
			progress.setRange(0,0)

			wfset = create_wireframe_set(wfdata)

			polygons = {}
			startValues = [ymin, xmin]
			coordFields = [["EAST", "RL"], ["NORTH", "RL"]]
			directions = ["NORTH", "WEST"]

			polycoords = []
			for i, (direction, startvalue, fields) in enumerate(zip(directions, startValues, coordFields), start = 1):
				# определяем контур блочной модели в плане
				strpath = os.path.join(self._tmpfolder, get_temporary_prefix() + "SILHOUETTE.STR")
				try:					wireframe_silhouette(wfset, strpath, direction, startvalue)
				except Exception as e:	raise Exception("Невозможно определить силуэт каркаса. %s" % str(e))

				f = MicromineFile()
				if not f.open(strpath):
					raise Exception("Невозможно открыть файл '%s'" % strpath)
				strdata = f.read(columns = fields)
				f.close()

				polygons[direction] = sgeom.MultiPoint(strdata).convex_hull.buffer(zoffset)
				if direction == "NORTH":	polycoords = [[pt[0], ymin-zoffset, pt[1], i] for pt in polygons[direction].exterior.coords]
				if direction == "WEST":		polycoords = [[xmin-zoffset, pt[0], pt[1], i] for pt in polygons[direction].exterior.coords]

				hullpath = os.path.join(self._tmpfolder, get_temporary_prefix() + "CONVEX_HULL.STR")
				MicromineFile.write(hullpath, polycoords, ["EAST", "NORTH", "RL", "JOIN"])
				assign_string(hullpath, locBmPath, direction)

			progress.setText("Удаление лишних полей")
			f = MicromineFile()
			if not f.open(locBmPath):
				raise Exception("Невозможно открыть файл '%s'" % locBmPath)
			inorth, iwest = f.get_field_id("NORTH_"), f.get_field_id("WEST_")
			filteredBmData = list(filter(lambda row: not is_blank(row[inorth]) and not is_blank(row[iwest]), f.read(asstr = True)))
			structure = f.structure
			for index in sorted([inorth, iwest], reverse = True):
				structure.delete_field(index)
			f.structure = structure
			structure = f.structure
			f.close()
			MicromineFile.write(locBmPath, filteredBmData, structure = structure)
			# ndeleted, n = 0, f.records_count
			# for i in range(n-1, -1, -1):
			# 	if is_blank(f.get_str_field_value(inorth, i+1)) or is_blank(f.get_str_field_value(iwest, i+1)):
			# 		f.delete_record(i+1)
			# 		ndeleted += 1
			# 		progress.setText("Удаление лишних блоков (%d / %d)" % (ndeleted, n))
			# structure = f.structure
			# for index in sorted([inorth, iwest], reverse = True):
			# 	structure.delete_field(index)
			# f.structure = structure
			# f.close()

			#
			if not bm_existed:
				### =================================================================================== ###
				### ==================== ИЗМЕНЕНИЕ СТРУКТУРЫ ФАЙЛА БЛОЧНОЙ МОДЕЛИ ===================== ###
				### =================================================================================== ###

				# изменение структуры файла блочной модели
				progress.setText("Изменение структуры БМ")
				f = MicromineFile()
				if not f.open(locBmPath):
					raise Exception("Невозможно изменить структуру файла БМ.\nНе удалось открыть файл %s" % locBmPath)
				f.structure = modifyBlockModelStructure(f.structure, currdate, self.main_app.settings.getValue(ID_SVY_MINEBINDATTRNAME).upper(), idwElements)
				f.close()
				# необходимо проверять после каждого процесса
				if self.is_cancelled:	return
			else:
				### =================================================================================== ###
				### ========================== ОБЪЕДИНЕНИЕ БЛОЧНЫХ МОДЕЛЕЙ ============================ ###
				### =================================================================================== ###
				progress.setText("Объединение блочных моделей")
				# Фильтр
				dbfilter = MMpy.FormSet("DBFILTER","16.0.896.3")
				datagrid = MMpy.DataGrid(4,1)
				datagrid.set_column_info(0,1,4)
				datagrid.set_column_info(1,2,6)
				datagrid.set_column_info(2,3,-1)
				datagrid.set_column_info(3,4,14)
				datagrid.set_row(0, [BMFLD_CURRMINED,"1","",""])
				datagrid.set_row(1, [BMFLD_TOTALMINED,"1","",""])
				dbfilter.set_field("GRID", datagrid.serialise())
				dbfilter.set_field("NEGATE","0")
				dbfilter.set_field("COMBINE_EQ","0")
				dbfilter.set_field("OR","1")
				dbfilter.set_field("AND","0")
				dbfilter.set_field("FTYPE","0")
				dbfilter.set_field("FILE", locBmPath)

				bmparams["dbfilter1"] = dbfilter
				bmparams["bmpath2"] = locBmPath
				bmparams["outpath"] = locBmPathMerged
				try:					merged_bm = net_bm.merge(bmparams)
				except Exception as e:	raise Exception("Не удалось объединить блочные модели. %s" % str(e))
				if self.is_cancelled:	return

				os.remove(locBmPath)
				os.rename(locBmPathMerged, locBmPath)

			loc_bm = BlockModel(locBmPath, self.main_app)


			

			

			### =================================================================================== ###
			### =============================== ОГРАНИЧЕНИЕ МОДЕЛИ ПО Z============================ ###
			### =================================================================================== ###
			progress.setText("Ограничение блочной модели по Z ")



			params = geoUpdateBmDialog.frameOptions.modelParams(listModelItemZ)

				
			resizeZmax = params['zmax'] if params['zmax'] != 'None' else None
			resizeZmin = params['zmin']	if params['zmin'] != 'None' else None


			sub_x_geo = self.main_app.settings.getValue(ID_SUBXGEO)
			sub_y_geo = self.main_app.settings.getValue(ID_SUBYGEO)
			sub_z_geo = self.main_app.settings.getValue(ID_SUBZGEO)


			if  (not resizeZmax is None) and (not resizeZmin is None ):
				
				if  is_blank(resizeZmax) or  is_blank(resizeZmin) :
					raise Exception("Блочная модель {0} имеет незаполненные ограничения по Z".format(net_bm.name))

				try:
					resizeZmax = float(resizeZmax)
					resizeZmin = float(resizeZmin)
				except:
					raise Exception("Блочная модель {0} имеет не числовые ограничения по Z".format(net_bm.name))
				if  resizeZmax <  resizeZmin:
					raise Exception("Блочная модель {0}. Ограничение по верхней координате Z меньше нижней ".format(net_bm.name))

				if not (zmin == resizeZmin and zmax == resizeZmax):
					logger.debug('Ограничение блочной модели по Z.')

					progress.setText("Ограничение блочной модели по Z. Копирование границ БМ для создания ЦМП. ")
					locStrBoundsZmaxPath = os.path.join(self._tmpfolder, get_temporary_prefix() +"zmax_"+ os.path.basename(netStrBoundsPath))
					locStrBoundsZminPath = os.path.join(self._tmpfolder, get_temporary_prefix() +"zmin_" + os.path.basename(netStrBoundsPath))

					err = self.exec_proc(self.copy_file, locStrBoundsPath, locStrBoundsZmaxPath)
					if err:					raise Exception("Ошибка", "Невозможно скопировать файл %s. %s" % (locStrBoundsZmaxPath, err))
					if self.is_cancelled:	return
					err = self.exec_proc(self.copy_file, locStrBoundsPath, locStrBoundsZminPath)
					if err:					raise Exception("Ошибка", "Невозможно скопировать файл %s. %s" % (locStrBoundsZminPath, err))
					if self.is_cancelled:	return


					f = MicromineFile()
					if not f.open(locStrBoundsZmaxPath):
						raise Exception("Невозможно открыть файл '%s'" % locStrBoundsZmaxPath)
					idxSysRl = f.get_field_id(ID_RL)
					for i in range(f.records_count):
						f.set_field_value(idxSysRl, i+1, resizeZmax)
					f.close()

					f = MicromineFile()
					if not f.open(locStrBoundsZminPath):
						raise Exception("Невозможно открыть файл '%s'" % locStrBoundsZminPath)
					idxSysRl = f.get_field_id(ID_RL)
					for i in range(f.records_count):
						f.set_field_value(idxSysRl, i+1, resizeZmin)
					f.close()
				
					progress.setText("Ограничение блочной модели по Z. Создание ЦМП. ")

					triCutPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "CUT.TRIDB")
					Tridb.create(triCutPath)
					triCutName = "CUT"
					create_dtm_from_string(locStrBoundsZmaxPath, triCutPath, "zmax")
					create_dtm_from_string(locStrBoundsZminPath, triCutPath, "zmin")

					progress.setText("Ограничение блочной модели по Z. Удаление участков БМ выходящих за пределы ограничений. ")
					bmparams = {}
					bmparams["typepath"] = triCutPath
					bmparams["dtmname"] = "zmax"
					bmparams["isCutAbove"] = True
					bmparams["sub_x_geo"] = sub_x_geo
					bmparams["sub_y_geo"] = sub_y_geo
					bmparams["sub_z_geo"] = sub_z_geo
					loc_bm.cut_bm_by_dtm(bmparams)

					bmparams = {}
					bmparams["typepath"] = triCutPath
					bmparams["dtmname"] = "zmin"
					bmparams["isCutAbove"] = False
					bmparams["sub_x_geo"] = sub_x_geo
					bmparams["sub_y_geo"] = sub_y_geo
					bmparams["sub_z_geo"] = sub_z_geo
					loc_bm.cut_bm_by_dtm(bmparams)

					zmax = resizeZmax
					zmin = resizeZmin 
				
			if self.is_cancelled:	return

			### =================================================================================== ###
			### =============================== ПРИСВАИВАНИЕ АТРИБУТОВ ============================ ###
			### =================================================================================== ###

			sysFieldMaterial = "SYS_%s" % BMFLD_MATERIAL
			sysFieldIndex = "SYS_%s" % BMFLD_INDEX
			sysFieldDensity = "SYS_%s" % BMFLD_DENSITY
			sysFieldDir = "SYS_DIR"
			sysFieldDip = "SYS_DIP"
			sysFieldRot = "SYS_ROT"

			allSystemFields = (sysFieldMaterial, sysFieldIndex, sysFieldDensity, sysFieldDir, sysFieldRot, sysFieldDip)

			# присваивание в хукополя
			assignparams = {}
			assignparams["attributes"] = []
			assignparams["attributes"].append(["0", GEO_ATTR_MATERIAL, sysFieldMaterial])
			assignparams["attributes"].append(["0", DEFAULT_ATTR_CODE, sysFieldIndex])
			assignparams["attributes"].append(["0", GEO_ATTR_DENSITY, sysFieldDensity])
			assignparams["attributes"].append(["0", GEO_ATTR_DIR, sysFieldDir])
			assignparams["attributes"].append(["0", GEO_ATTR_DIP, sysFieldDip])
			assignparams["attributes"].append(["0", GEO_ATTR_ROT, sysFieldRot])
			assignparams["wireframe_set"] = wfset
			assignparams["overwrite_bool"] = True
			assignparams["sub_xsize"] = xSubBlockSize
			assignparams["sub_ysize"] = ySubBlockSize
			assignparams["sub_zsize"] = zSubBlockSize
			try:					loc_bm.assign_wireframes(assignparams)
			except Exception as e:	raise Exception("Невозможно присвоить атрибуты. %s" % str(e))
			if self.is_cancelled:	return

			### =================================================================================== ###
			### =================================== ПЕРЕНОС ДАННЫХ ================================ ###
			### =================================================================================== ###


			f = MicromineFile()
			if not f.open(locBmPath):
				raise Exception("Невозможно открыть файл '%s'" % locBmPath)

			idxSysMaterial = f.get_field_id(sysFieldMaterial)
			idxSysIndex = f.get_field_id(sysFieldIndex)
			idxSysDensity = f.get_field_id(sysFieldDensity)
			idxSysDir = f.get_field_id(sysFieldDir)
			idxSysRot = f.get_field_id(sysFieldRot)
			idxSysDip = f.get_field_id(sysFieldDip)

			idxGeoMaterial = f.get_field_id(BMFLD_GEOMATERIAL)
			idxGeoIndex = f.get_field_id(BMFLD_GEOINDEX)
			idxMaterial = f.get_field_id(BMFLD_MATERIAL)
			idxIndex = f.get_field_id(BMFLD_INDEX)
			idxDensity = f.get_field_id(BMFLD_DENSITY)
			idxPrevDate = f.get_field_id(prevdate)
			idxCurrDate = f.get_field_id(currdate)
			idxCurrMined = f.get_field_id(BMFLD_CURRMINED)
			idxTotalMined = f.get_field_id(BMFLD_TOTALMINED)

			sysIndexes = (idxSysMaterial, idxSysIndex, idxSysMaterial, idxSysIndex, idxSysDensity)
			bmIndexes = (idxGeoMaterial, idxGeoIndex, idxMaterial, idxIndex, idxDensity)

			allSystemIndexes = (idxSysMaterial, idxSysIndex, idxSysDensity, idxSysDir, idxSysRot, idxSysDip)

			# если присвоилось,
			# 	если минед пустой
			# 		то переносим хукополя и хукоматериал в превдейт
			# 		если курминед
			# 			то переносим хукоматериал в курдейт
			# если нет и минед и курминед пустые,
			# 	то вмещающие везде
			for i in range(f.records_count):
				sysMaterial = f.get_str_field_value(idxSysMaterial, i+1)
				# если что-то присвоилось
				if not is_blank(sysMaterial):
					if is_blank(f.get_str_field_value(idxTotalMined, i+1)):
						for bmIndex, sysIndex in zip(bmIndexes, sysIndexes):
							f.set_field_value(bmIndex, i+1, f.get_str_field_value(sysIndex, i+1))
						f.set_field_value(idxPrevDate, i+1, f.get_str_field_value(idxSysMaterial, i+1))
						#
						if is_blank(f.get_str_field_value(idxCurrMined, i+1)):
							f.set_field_value(idxCurrDate, i+1, f.get_str_field_value(idxMaterial, i+1))
				elif is_blank(f.get_str_field_value(idxCurrMined, i+1)) and is_blank(f.get_str_field_value(idxTotalMined, i+1)):
					f.set_field_value(idxGeoMaterial, i+1, emptyCode)
					f.set_field_value(idxGeoIndex, i+1, emptyIndex)
					f.set_field_value(idxMaterial, i+1, emptyCode)
					f.set_field_value(idxIndex, i+1, emptyIndex)
					f.set_field_value(idxDensity, i+1, emptyDensity)
					f.set_field_value(idxPrevDate, i+1, emptyCode)
					f.set_field_value(idxCurrDate, i+1, emptyCode)

			f.close()
				
			### =================================================================================== ###
			### ======================================== IDW ====================================== ###
			### =================================================================================== ###

			idwParams = idwModelParams[listModelItem.filePath()]
			n = len(oreCodesDict[netBmPath]) * len(idwElements) * len(idwParams[ID_IDWRADIUS])
			for i, oreCode in enumerate(oreCodesDict[netBmPath]):
				# пропускаем вмещающие и нерудные породы
				if oreTypesDict[oreCode] in [1,2]:
					continue

				#
				for element in idwElements:
					for radius in idwParams[ID_IDWRADIUS]:
						status = "IDW (%s, %s, %s)" % (oreCode, element, radius)
						progress.setText(status)
						err = self.exec_mmproc(idw, locBmPath, locExpAssayPath, indexFld, oreCode, element, radius, oreIndexesDict[oreCode], sysFieldDir, sysFieldRot, sysFieldDip,
							idwParams[ID_IDWMINPTS], idwParams[ID_IDWMAXPTS], idwParams[ID_IDWSECTOR], idwParams[ID_IDWFACTOR1], idwParams[ID_IDWFACTOR2], idwParams[ID_IDWFACTOR3])
						if err:						print ("Ошибка. %s" % err)
						if self.is_cancelled:		return
					if self.is_cancelled:		return
				if self.is_cancelled:		return


			### =================================================================================== ###
			### ================= ЗАПОЛНЕНИЕ НУЛЯМИ НЕПРОИНТЕРПОЛИРОВАННЫХ ЭЛЕМЕНТОВ ============== ###
			### =================================================================================== ###

			if elemToFill:
				f = MicromineFile()
				if not f.open(locBmPath):
					raise Exception("Невозможно открыть файл '%s'" % locBmPath)
				#
				indexes = list(filter(lambda x: x > -1, [f.get_field_id(elem) for elem in elemToFill]))

				for i in range(f.records_count):
					if self.is_cancelled:
						break
					#
					for index in indexes:
						if is_blank(f.get_str_field_value(index, i+1)):
							f.set_field_value(index, i+1, 0)
					#
					if i % 500 == 0:
						QtCore.QCoreApplication.processEvents()
				f.close()

			### =================================================================================== ###
			### ============== ЗАПОЛНЕНИЕ ПОЛЯ ОБЪЁМНОГО ВЕСА РАССЧЁТНЫМ ЗНАЧЕНИЕМ ================ ###
			### =================================================================================== ###

			params = geoUpdateBmDialog.frameOptions.modelParams(listModelItemZ)
			
			isUseDynamicDensity = params['isUseDynamicDensity'] if params['isUseDynamicDensity'] != 'None' else None


			if isUseDynamicDensity:
				geocodes = self.main_app.settings.getValue(ID_GEOCODES)

				# elements кортеж хим элементов из српавочника
				elements = []
				indexes = list(zip(*geocodes))[1]
				codes = [i['BM'] for i in indexes]
			
				for bm_indexes in codes:
					for bm_index in bm_indexes:
						for factor in bm_index[1]['FACTOR']:
							elements.append(factor[0])
				elements = set(elements)
				#

				f = MicromineFile()
				if not f.open(locBmPath):
					raise Exception("Невозможно открыть файл '%s'" % locBmPath)

				# Заголовки полей БМ
				header = f.header

				# Словарь индексов граф с химическими элементами в блочной модели
				idxElements = {}
				for element in elements:
					idx = f.get_field_id(element)
					idxElements[element] = idx

				idxDensity = f.get_field_id(BMFLD_DENSITY)
				file_record_count = f.records_count
				for i in range(file_record_count): 
					# список значений текущей строки БМ
					row = []
					for col in header:
						j = f.get_field_id(col)
						row.append(f.get_str_field_value(j, i+1))

					idxMaterial = header.index(BMFLD_MATERIAL)
					idxIndex = header.index(BMFLD_INDEX)
					# getDynamicDensity - функция возвращает динаически рассчитанный объёмный вес
					# header - заголовки полей БМ
					# row  - список значений  строки БМ, для которой рассчитываем объёмный вес
					# geocodes  - геологические коды, индексы, плотности, коэффициенты учавствующие в рассчёте
					# idxElements - словарь {'хим. элемент': индекс поля в БМ}
					# idxMaterial - индекс поля материала БМ
					# idxIndex - индекс поля  индекса БМ
					dynamicDensity = getDynamicDensity(header, row, geocodes, idxElements, idxMaterial, idxIndex)
					if not dynamicDensity is None:
						f.set_field_value(idxDensity, i+1, dynamicDensity)

					if i % 1000 == 0:
						progress.setText("Расчёт динамического объёмного веса (%d / %d)" % (i, file_record_count))

				f.close()


			### =================================================================================== ###
			### ==================== ОПТИМИЗАЦИЯ БЛОЧНОЙ МОДЕЛИ НА СЕРВЕР ========================= ###
			### =================================================================================== ###

			# удаление полей АЗИМУТ, ПОГРУЖЕНИЕ, ВРАЩЕНИЕ
			f = MicromineFile()
			if not f.open(locBmPath):
				raise Exception("Невозможно открыть файл '%s" % locBmPath)
			structure = f.structure
			for index in sorted(allSystemIndexes, reverse = True):
				structure.delete_field(index)
			f.structure = structure
			f.close()

			#
			progress.setText("Оптимизация блочной модели")
			bmparams = {}
			bmparams["key_fields"] = [BMFLD_MINENAME, BMFLD_INTERVAL, BMFLD_INDEX]
			bmparams["outpath"] = netBmPath
			try:					net_bm = loc_bm.optimize(bmparams)
			except Exception as e:	raise Exception("Не удалось произвести оптимизацию блочной модели. %s" % str(e))
			if self.is_cancelled:	return
			
			### =================================================================================== ###
			### =================================== СОЗДАНИЕ ЛОГА ================================= ###
			### =================================================================================== ###

			progress.setText("Запись метаданных")

			if not net_bm.has_metadata:
				net_bm.add()

			net_bm.metadata.current_date = fieldname_to_date(currdate)

			# метаданные
			data = {}
			net_bm.metadata.layer = layer
			net_bm.metadata.geounit = unit
			# экстенты
			net_bm.metadata.xmin = xmin
			net_bm.metadata.xmax = xmax
			net_bm.metadata.ymin = ymin
			net_bm.metadata.ymax = ymax
			net_bm.metadata.zmin = zmin
			net_bm.metadata.zmax = zmax
			# способ рассчёта объёмного веса
			net_bm.metadata.is_use_dynamic_density = isUseDynamicDensity  
			net_bm.metadata.expansion = zoffset  
			net_bm.save()

		progress.setText("")
		progress.setRange(0, 100)

		qtmsgbox.show_information(self.main_app, "Выполнено", "Блочные модели успешно созданы/обновлены")



