try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore
import os

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.modes.dsn.DsnAssignBm import DsnAssignBm

from mm.formsets import loadTriangulationSet, loadBlockModel

from utils.blockmodel import BlockModel
from utils.constants import *
from utils.osutil import get_temporary_prefix
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class DsnUpdateBm(DsnAssignBm):
	def run(self):
		bmItems = self.selectedItems()[ITEM_BM]
		self.createLockFiles([bmItem.filePath() for bmItem in bmItems])			# создание LCK файлов

		if not super(DsnUpdateBm, self).run():
			return

		# копирование файлов на сервер
		for locBmPath, netBmPath in zip(self._locBmPathList, self._netBmPathList):
			err = self.exec_proc(self.copy_file, locBmPath, netBmPath)
			if err:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно скопировать файл '%s'. %s" % (os.path.basename(locBmPath), err))
				continue
			net_bm = BlockModel(netBmPath, self.main_app)
			net_bm.metadata.trigger_update()

		# загрузка объектов в Визекс
		userFormsets = list(zip(*self.main_app.settings.getValue(ITEM_CATEGORY_FORMSET_DICT[ITEM_CATEGORY_DSN])))[1]
		userFormsets = list([int(setId) if setId else None for setId in userFormsets])
		formsetsDict = dict(zip(MMTYPE_LIST, userFormsets))
		
		# загрузка каркасов
		setId = formsetsDict[MMTYPE_TRIDB]
		for i, (wfset, display_name) in enumerate(self._wireframe_sets):
			loadTriangulationSet(wfset, display_name, setId)

		# загрузка блочной модели
		setId = formsetsDict[MMTYPE_BM]
		for i, locBmPath in enumerate(self._locBmPathList):
			bm = BlockModel(locBmPath, self.main_app)
			bm.load(bm.name[len(get_temporary_prefix()):], setId)

		qtmsgbox.show_information(self.main_app, "Выполнено", "Блочная модель успешно обновлена")
		return True