try:			import MMpy
except:			pass

from PyQt4 import QtGui, QtCore

from datetime import datetime
import os, sqlite3, pyodbc
from itertools import groupby
from copy import deepcopy

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.dialogs.BlockModelPreviewDialog import BlockModelPreviewDialog, bmDataPreviewReport, createBmPreviewListTreeItem
from gui.qt.ProcessWindow import ProcessWindow
from gui.qt.modes.WireframeAttributeValidation import DsnWireframeAttributeValidation
from gui.qt.modes.svy.SvyAssignBm import AssignBm

from mm.mmutils import Tridb, MicromineFile
from mm.formsets import create_wireframe_set, loadTriangulationSet

from utils.nnutil import prev_date, date_to_fieldname
from utils.validation import is_blank, is_number
from utils.blockmodel import BlockModel
from utils.constants import *
from utils.osutil import get_file_owner, get_temporary_prefix, get_current_time
from utils.logger import  NLogger

import xlsxwriter as xl
from xlsxwriter.utility import xl_range, xl_rowcol_to_cell, xl_col_to_name
from collections import OrderedDict
import re

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

def metal_percision(descr):
	metal_weight_percisions = {'NI' : 1,
								'CU' : 1,
								'CO' : 2,
								'TE' : 2,
								'SE' : 2,
								'S[" ","("]': 0,
								'PT' : 2,
								'PD' : 2,
								'RH' : 2,
								'OS' : 2,
								'IR' : 2,
								'RU' : 2,
								'AU' : 2,
								'AG' : 2
							}
	metal_percentage_percisions={ 'NI' : 2,
								'CU' : 2,
								'CO' : 3,
								'TE' : 4,
								'SE' : 4,
								'S[" ","("]' : 2,
								'PT' : 2,
								'PD' : 2,
								'RH' : 2,
								'OS' : 2,
								'IR' : 2,
								'RU' : 2,
								'AU' : 2,
								'AG' : 2
							}
	up_descr = descr.upper()
	metal_dict_percisions = {}
	if re.search('\(КГ\)', up_descr) or re.search('\(Т\)', up_descr):
		metal_dict_percisions = metal_weight_percisions
	else:
		metal_dict_percisions = metal_percentage_percisions
	percision = None
	for item in metal_dict_percisions:
		if  re.search(item, up_descr):
			percision =	metal_dict_percisions[item]
			break
	return percision

class DsnAssignBm(AssignBm):
	def __init__(self, *args, **kw):
		super(DsnAssignBm, self).__init__(*args, **kw)

	def prepareBlockModels(self, extracted, restoreFields, dataTransfer):
		progress = self.main_app.mainWidget.progressBar
		progress.setText("Обработка выбранных данных")
		# извлечение данных
		if extracted and not self.extractBlockModels():
			return False
		# восстановление полей
		if restoreFields and not self.restoreFields(extracted):
			return False
		# перенос данных
		if dataTransfer and not self.transferBlockModelData(extracted):
			return False
		#
		return True

	def prepareReport(self, setBackfillGradesToNull = True, dilutionCoef = 0):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		extracted, restoreFields, dataTransfer = True, True, True
		if not self.prepareBlockModels(extracted, restoreFields, dataTransfer):
			progress.setText(oldProgressText)
			return None, None
		#
		BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)
		#
		bmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		bmData, bmHeader = self.addBlockModelsData(bmPathList)
		# Такой порядок полей важен для ПЛАН-ЗАМЕРА
		# ДАТА, ЗАЛЕЖЬ, БЛОК, ПАНЕЛЬ, ЛЕНТА, КАРКАС, ВЫРАБОТКА, ИНТЕРВАЛ, КАТЕГОРИЯ, РАБОЫ, МАТЕРИАЛ, ИНДЕКС, ОБЪЕМ, ТОННАЖ, ПЛОТНОСТЬ, МЕТАЛЛЫ 
		groupFields = (
			BMFLD_LAYER, self.main_app.settings.getValue(ID_GEO_UDSPNAME).upper(), 
			BMFLD_PROJECT_NAME, BMFLD_PROJECT_MINENAME, BMFLD_PROJECT_INTERVAL, BMFLD_PROJECT_STAGE, BMFLD_PROJECT_CATEGORY, BMFLD_PROJECT_WORKTYPE, 
			BMFLD_MATERIAL, BMFLD_INDEX)

		#

		elemFields = self.getIdwElements()
		elemUnits = self.getIdwUnits()
		try:
			reportData, reportHeader = self.bmDataReport(bmData, bmHeader, groupFields, BMFLD_DENSITY, elemFields, elemUnits, elemTonnageFlag = True)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно создать отчет по блочной модели. %s" % str(e))
			return None, None

		# добавление разубоживания бетоном, если необходимо
		if isinstance(dilutionCoef, float) and dilutionCoef > 0:
			backfillPurpose, backfillCategory = self.getBackfillPurpose(), self.getBackfillCategory()
			oreCodes = {code: codeDescription for code, codeDescription, codeType in self.getGeoCodes() if codeType == 0}
			purposeCodeDict = self.getMineWorkPurposeCodeDict()
			idxIndex = reportHeader.index(BMFLD_INDEX)
			idxMaterial = reportHeader.index(BMFLD_MATERIAL)
			idxTonnage = reportHeader.index(RPTFLD_FACT_TONNAGE)
			idxDensity = reportHeader.index(RPTFLD_FACT_DENSITY)
			idxProjectMineName = reportHeader.index(BMFLD_PROJECT_MINENAME)
			idxProjectInterval = reportHeader.index(BMFLD_PROJECT_INTERVAL)
			# вычисление соотношение закладочных кодов по отношению к общему тоннажу
			backfillRatio = {}
			oreTonnage = 0
			for key, values in groupby(sorted(reportData), key = lambda x: tuple(x[k] for k in range(idxIndex+1))):
				key, values = tuple(key), tuple(values)
				if key[idxMaterial] == backfillCategory:
					backfillRatio[key] = values[0][idxTonnage]
				elif key[idxMaterial] in oreCodes:
					oreTonnage += values[0][idxTonnage]
			# исходное бетонное разубоживание
			backfillTonnage = sum(backfillRatio.values())
			backfillDilution = backfillTonnage / oreTonnage
			for item in backfillRatio:
				backfillRatio[item] /= backfillTonnage
			# увеличение бетонного разубоживания на заданное пользователем количество процентов
			userBackfillDilution = backfillDilution + dilutionCoef / 100
			userBackfillTonnage = oreTonnage * userBackfillDilution
			# разница тоннажа исходного и с учетом добавленного бетона
			backfillDiff = userBackfillTonnage - backfillTonnage
			# определяем тоннаж для каждого кода, соответствующего закладочным работам
			userBackfillTonnageAdd = {key: backfillRatio[key] * backfillDiff for key in backfillRatio}
			# определяем объем для каждого кода, соответствующего закладочным работам
			userBackfillVolumeAdd, userBackfillDensityAdd = {}, {}
			for key in backfillRatio:
				density = self.getDensityByMaterialAndIndex(key[idxMaterial], key[idxIndex])
				if key[idxMaterial] == backfillCategory:
					density = float(self.main_app.settings.getValue(ID_CONCDENS))
				userBackfillVolumeAdd[key] = userBackfillTonnageAdd[key] / density
				userBackfillDensityAdd[key] = density
			# добавление к исходным данным недостающих блоков с закладкой нужного объема
			idxCenterX, idxCenterY, idxCenterZ = bmHeader.index(ID_EAST), bmHeader.index(ID_NORTH), bmHeader.index(ID_RL)
			idxSizeX, idxSizeY, idxSizeZ = bmHeader.index("_{}".format(ID_EAST)), bmHeader.index("_{}".format(ID_NORTH)), bmHeader.index("_{}".format(ID_RL))
			idxBmMaterial = bmHeader.index(BMFLD_MATERIAL)
			idxBmIndex = bmHeader.index(BMFLD_INDEX)
			idxBmDensity = bmHeader.index(BMFLD_DENSITY)
			groupColumns = tuple(reportHeader[k] for k in range(idxIndex+1))
			for i, key in enumerate(userBackfillVolumeAdd, start = 1):
				bmData.append(bmData[-1].copy())
				bmData[-1][idxSizeX], bmData[-1][idxSizeY], bmData[-1][idxSizeZ] = userBackfillVolumeAdd[key], 1, 1
				bmData[-1][idxBmDensity] = userBackfillDensityAdd[key]
				for keyIndex, column in enumerate(groupColumns):
					bmIndex = bmHeader.index(column)
					bmData[-1][bmIndex] = key[keyIndex]
			# подготавливаем отчет заново
			try:
				reportData, reportHeader = self.bmDataReport(bmData, bmHeader, groupFields, BMFLD_DENSITY, elemFields, elemUnits, elemTonnageFlag = True)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно создать отчет по блочной модели. %s" % str(e))
				return None, None

		# переименование полей SYS_ВЫРАБОТКА и CURRDATE
		if BMFLD_SYS_MINENAME in reportHeader:
			idxSysMineName = reportHeader.index(BMFLD_SYS_MINENAME)
			reportHeader[idxSysMineName] = "КАРКАС"
		if BMFLD_PROJECT_CATEGORY in reportHeader:
			idxCurrDate = reportHeader.index(BMFLD_PROJECT_CATEGORY)
			reportHeader[idxCurrDate] = DSN_ATTR_CATEGORY
		if BMFLD_PROJECT_WORKTYPE in reportHeader:
			idxWorkType = reportHeader.index(BMFLD_PROJECT_WORKTYPE)
			reportHeader[idxWorkType] = "ВИД_РАБОТ"
		# обнуление содержаний для закладочных работ
		if setBackfillGradesToNull:
			progress.setText("Обнуление содержаний для закладочных работ")
			columnCount = len(reportHeader)
			idxDensity = reportHeader.index(RPTFLD_FACT_DENSITY)
			idxIndex = reportHeader.index(BMFLD_INDEX)
			for rowIndex, row in enumerate(reportData):
				index = row[idxIndex]
				if index in self.getBackfillIndexes():
					for columnIndex in range(idxDensity+1, columnCount):
						reportData[rowIndex][columnIndex] = 0

		return reportData, reportHeader

	def deleteProjectData(self, bmpath, projectList):
		f = MicromineFile()
		f.open(bmpath)

		ifields = [f.get_field_id(field) for field in BMFIELDS_DSN]
		for i in range(f.records_count):
			if self.is_cancelled:
				f.close()
				return
			#
			if i % 100 == 0:
				QtCore.QCoreApplication.processEvents()
			#
			if f.get_str_field_value(ifields[0], i+1) in projectList:
				for ifield in ifields:
					f.set_field_value(ifield, i+1, "")
		f.close()

	def transferBlockModelData(self, extracted = False):
		progress = self.main_app.mainWidget.progressBar
		#
		locBmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		#
		(includeValues, restoreMiningValues, restoreGeologyValues, simulateConcreteValues), includeFields = self.selectedKeysAndFields()
		for netBmPath, locBmPath in zip(self._netBmPathList, locBmPathList):
			netBm = BlockModel(netBmPath, self.main_app)
			BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)
			#
			progress.setText("%s. Определение индексов выбранных записей" % netBm.name)
			try:
				includeIndexes = self.getRowIndexesByKeys(locBmPath, includeFields, includeValues)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось определить индексы строк для ключевых записей. %s" % str(e))
				return False
			# перенос данных
			try:
				self.copyDsnFieldValues(locBmPath, includeIndexes, clearSource = False)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось скопировать данные из системных полей. %s" % str(e))
				return False
		#
		return True

	def displayData(self, extracted = False):
		progress = self.main_app.mainWidget.progressBar
		progress.setText("Загрузка объектов")
		#
		userFormsets = list(zip(*self.main_app.settings.getValue(ITEM_CATEGORY_FORMSET_DICT[ITEM_CATEGORY_DSN])))[1]
		userFormsets = list([int(setId) if setId else None for setId in userFormsets])
		formsetsDict = dict(zip(MMTYPE_LIST, userFormsets))
		#
		locBmPathList = self._locBmPathList if not extracted else self._locBmExtractedPathList
		# загрузка блочной модели
		setId = formsetsDict[MMTYPE_BM]
		for i, (netBmPath, locBmPath) in enumerate(zip(self._netBmPathList, locBmPathList)):
			if not os.path.exists(locBmPath):
				continue
			netBm = BlockModel(netBmPath, self.main_app)
			locBm = BlockModel(locBmPath, self.main_app)
			locBm.load(netBm.name, setId)
		# загрузка каркасов
		setId = formsetsDict[MMTYPE_TRIDB]
		for wfset in self._wireframe_sets:
			loadTriangulationSet(wfset, wfset.displayName(), setId)

	def loadToVizex(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()

		# перенос данных из системных полей
		extracted, restoreFields, dataTransfer = True, False, True
		try:
			if not self.prepareBlockModels(extracted, restoreFields, dataTransfer):
				progress.setText(oldProgressText)
				return False
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось подготовить блочные модели. %s" % str(e))
			return
		# удаление системных полей
		try:
			if not self.removeBlockModelSystemFields(extracted, excludeFields = BMFIELDS_DSN):
				progress.setText(oldProgressText)
				return False
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось удалить системные поля. %s" % str(e))
			return
		# загрузка
		try:
			self.displayData(extracted = extracted)
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно загрузить данные. %s" % str(e))
			progress.setText(oldProgressText)
			return
		progress.setText(oldProgressText)

	def _saveToDb(self):
		progress = self.main_app.mainWidget.progressBar
		oldProgressText = progress.text()
		#
		mineSiteId = self._bmPreviewDialog.mineSiteId()
		if mineSiteId is None:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Выберите участок")
			return False
		# считывание текущей даты блочных моделей
		netBm = BlockModel(self._netBmPathList[0], self.main_app)
		bmDate = netBm.blockmodel_date
		#
		reportPath = os.path.join(self._tmpfolder, get_temporary_prefix() +  "CSV_REPORT.csv")
		try:
			reportFile = open(reportPath, "w")
			reportFile.close()
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Невозможно создать отчет. Возможно файл используется")
			return False
		# подготовка отчета
		# считывание коэффициента разубоживания
		concCoef = self._bmPreviewDialog.paramsFrame.dilutionCoef()
		try:
			reportData, reportHeader = self.prepareReport(dilutionCoef = concCoef)
			if reportData is None:
				return False			
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", str(e))
			return False
		#
		# экспорт во временный CSV файл
		with open(reportPath, "w") as reportFile:
			reportFile.write(",".join(map(str, reportHeader)) + "\n")
			for row in reportData:
				reportFile.write(",".join(map(str, row)) + "\n")
		#
		progress.setText("Запись отчета в базу данных")
		# считывание файла отчета в бинарном виде
		with open(reportPath, "rb") as reportFile:
			bindata = reportFile.read()
		#
		dataSource = "N-MINE"
		try:
			# определяем версию отчета за текущий месяц
			cmd = """
				SELECT CASE WHEN RPTVERSION IS NULL THEN 1 ELSE RPTVERSION+1 END RPTVERSION
				FROM
				(
					SELECT MAX(CONVERT(INT, SUBSTRING(REPORT_VERSION, 2, LEN(REPORT_VERSION)))) RPTVERSION
					FROM [{0}].[dbo].[MONTHLY_REPORT_DATA]
					WHERE YEAR(REPORT_DATE) = {1} AND MONTH(REPORT_DATE) = {2} AND DepartId = {3}
				) T
			""".format(self.main_app.srvcnxn.dbsvy, bmDate.year, bmDate.month, mineSiteId)
			# 
			newVer = "v%d" % self.main_app.srvcnxn.exec_query(cmd).get_column("RPTVERSION")[0]
			dataGroup = 2
			cmd = """
				INSERT INTO [{0}].[dbo].[MONTHLY_REPORT_DATA] (DepartID, REPORT_DATE, REPORT_VERSION, REPORT_FILE, LOADED, DATA_GROUP, DATA_SOURCE)
				VALUES (?, ?, ?, ?, ?, ?, ?)
			""".format(self.main_app.srvcnxn.dbsvy)
			params = (mineSiteId, get_current_time(), newVer, pyodbc.Binary(bindata), 0, dataGroup, dataSource)
			#
			self.main_app.srvcnxn.exec_query(cmd, params)
			self.main_app.srvcnxn.commit()
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Необработанная ошибка", str(e))
			raise
			return False
		#
		progress.setText(oldProgressText)
		qtmsgbox.show_information(self._bmPreviewDialog, "Выполнено", "Отчет успешно сохранен в базу данных")
		#
		return True
	def saveToDb(self):
		self._bmPreviewDialog.buttonSave.setDisabled(True)
		ret = self._saveToDb()
		self._bmPreviewDialog.buttonSave.setDisabled(False)
		return ret

	def exportToExcel(self):
		try:
			#import xlsxwriter as xl
			#from xlsxwriter.utility import xl_range, xl_rowcol_to_cell, xl_col_to_name

			#
			def exportSource(workbook, worksheet, srcdata, srcheader):
				fmtHeader = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "border": 1, "bold": True})
				fmtRptData = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "right", "valign": "vcenter", "border": 1})

				column_percisions = {}
				for icol, colname in enumerate(srcheader):
					worksheet.write(0, icol, colname, fmtHeader)
					column_percisions[icol] = metal_percision(colname)

				for irow, row in enumerate(srcdata, start = 1):
					for icol, value in enumerate(row):
						if not column_percisions[icol] is None:
							try:
								present_value = float(value)
								present_value = round(present_value, column_percisions[icol])
							except:
								present_value = value
						else:
							present_value = value 
						worksheet.write(irow, icol, present_value, fmtRptData)
			#
			def exportSurvey(workbook, worksheet, srcdata, srcheader, concCoef):
				# Форматы
				svyFmtHeader = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "text_wrap": True, "border": 1})
				svyFmtHeaderBold = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "text_wrap": False, "border": 1, "bold": True})
				svyFmtRptData = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "right", "valign": "vcenter", "text_wrap": True, "border": 1})
				svyFmtRptDataBold = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "right", "valign": "vcenter", "text_wrap": False, "border": 1, "bold": True})

				### =================================================================================== ###
				### ================================= СОЗДАНИЕ ЗАГОЛОВКА ============================== ###
				### =================================================================================== ###

				rpt_cat_indexes = self.main_app.settings.getValue(ID_RPT_GEO_SVY_INDEXES)

				if len(rpt_cat_indexes) == 0:
					qtmsgbox.show_error(self.main_app, "Ошибка", "Не заполнены отчётные категории маркшейдерского отчёта. Выполнение не возможно.")
					return

				allSvyCategories = self.getCategories()
				concPurpose, concCategory = self.getBackfillPurpose(), self.getBackfillCategory()
				concCatIndex = allSvyCategories.index(concCategory)
				rptCategoryDict = {}
				rptCategories = []
				for rptCategory, catIndex, bmIndexes in rpt_cat_indexes:
					index_data_list = list(zip(*bmIndexes['INDEX']))
					if len(index_data_list) > 0:
						indexes = index_data_list[0]
					else:
						indexes = []
					if catIndex == concCatIndex:
						catName = rptCategory + "\n{}%".format(concCoef)
					else:
						catName = rptCategory
					rptCategories.append(catName)
					rptCategoryDict[catName] = indexes

				rptCategoriesToPrint = deepcopy(rptCategories)
				svyRptCats = rptCategories + ["Потери"]

				catNumber = len(svyRptCats)

				rptCatVolIndexDict, rptCatTonIndexDict = {}, {}

				colwidths = [(0, 20)]	 
				for icol, icolWidth in colwidths:
					worksheet.set_column("{0}:{0}".format(xl_col_to_name(icol)), icolWidth)


				#region HEADER DEFENITIONS
				h_name = {
										'rowspan': 3,
										'colspan': 1,
										'fmt' : svyFmtHeader,
										'data': 'Наименование'}
				h_volume_header= {
										'rowspan': 1,
										'colspan': len(rptCategories) + 2,
										'fmt' : svyFmtHeader,
										'data': 'ОБЪЕМ (КУБ.М)'}

				

				h_loss_header = {  
								'rowspan': 1,
								'colspan': 2,
								'fmt' : svyFmtHeader,
								'data': 'Потери'}
				h_loss_heavy = {  
								'rowspan': 1,
								'colspan': 1,
								'fmt' : svyFmtHeader,
								'data': 'Тяжелая'}
				h_loss_light = {  
								'rowspan': 1,
								'colspan': 1,
								'fmt' : svyFmtHeader,
								'data': 'Легкая'}


				h_loss_heavy_light = {'direction' : 'h',
										'fmt' : svyFmtHeader,
										'data': [h_loss_heavy, h_loss_light]}

				h_loss = {'direction' : 'v',
										'fmt' : svyFmtHeader,
										'data': [h_loss_header, h_loss_heavy_light]}


				h_categories = []
				for cat in rptCategoriesToPrint:
					h_cat = {  
						'rowspan': 2,
						'colspan': 1,
						'fmt' : svyFmtHeader,
						'data': cat.strip()
					}
					h_categories.append(h_cat)

				h__group_cat = {'direction' : 'h',
								'fmt' : svyFmtHeader,
								'data': h_categories}


				h__group_cat_and_loss = {'direction' : 'h',
										'fmt' : svyFmtHeader,
										'data': [h__group_cat , h_loss]}


				h_group_volume = {'direction' : 'v',
										'fmt' : svyFmtHeader,
										'data': [h_volume_header , h__group_cat_and_loss]}



				h_tonnage_header= {
									'rowspan': 1,
									'colspan': len(rptCategories) +2,
									'fmt' : svyFmtHeader,
									'data': 'МАССА (Т)'}

				h_group_tonnage = {'direction' : 'v',
									'fmt' : svyFmtHeader,
									'data': [h_tonnage_header , h__group_cat_and_loss]}
				

				h__header = {'direction' : 'h',
										'fmt' : svyFmtHeader,
										'data': [h_name, h_group_volume, h_group_tonnage]}


				#endregion 

				smartHeaderCell = SmartHeaderCell(h__header, 0, 0)
				smartHeaderCell.write_to_worksheet(worksheet)



				### =================================================================================== ###
				### ================================= ПОДГОТОВКА ДАННЫХ =============================== ###
				### =================================================================================== ###

				geocodes = self.main_app.settings.getValue(ID_GEOCODES)

				# объединяем поля ВЫРАБОТКА и ИНТЕРВАЛ
				idxProjectMineName = srcheader.index(BMFLD_PROJECT_MINENAME)
				idxProjectInterval = srcheader.index(BMFLD_PROJECT_INTERVAL)
				for i, row in enumerate(srcdata):
					srcdata[i][idxProjectMineName] = ":".join([row[idxProjectMineName], row[idxProjectInterval]])
				del idxProjectMineName, idxProjectInterval

				# удаляем строки, соответствующие каркасам закладки
				idxProjCat = srcheader.index(SVY_ATTR_CATEGORY)
				srcdata = list(filter(lambda x: x[idxProjCat] != concCategory, srcdata))
				# удаляем ненужные столбцы
				pivotSourceData = list(zip(*srcdata))
				idxDensity = srcheader.index("ПЛОТНОСТЬ")
				srcdata = list(map(list, zip(*pivotSourceData[:idxDensity])))



				# заменяем индексы	
				svyRptCatDictExt = {}
				for catname, indexes in rptCategoryDict.items():
					for index in indexes:
						svyRptCatDictExt[index] = catname

				idxIndex = srcheader.index(BMFLD_INDEX)
				for i, row in enumerate(srcdata):
					if not row[idxIndex] in svyRptCatDictExt:
						raise ValueError("%s - неизвестный индекс. В Блочной модели присутствует индекс отсутствующий в настройках отчёта." % row[idxIndex])
					srcdata[i][idxIndex] = svyRptCatDictExt[row[idxIndex]]

				### =================================================================================== ###
				### ================================== ЭКСПОРТ ДАННЫХ ================================= ###
				### =================================================================================== ###

				currow = 3
				filledCells = []

				majorGroup = [BMFLD_PROJECT_MINENAME]
				majorGroupIndexes = [srcheader.index(column) for column in majorGroup]
				# Иссключаем BMFLD_MATERIAL так как теперь индексы включаются в отчётную категорию из разных гео материалов
				# сортировка по гео материалу приведет к неправильной групировке
				#minorGroup = [BMFLD_PROJECT_MINENAME, BMFLD_MATERIAL, BMFLD_INDEX]
				minorGroup = [BMFLD_PROJECT_MINENAME,  BMFLD_INDEX]
				minorGroupIndexes = [srcheader.index(column) for column in minorGroup]

				# Аттрибуты группировки второго уровня длжны содержать аттрибуты группировки первого уровня
				srcdata = sorted(srcdata, key = lambda x:  tuple(x[k] for k in minorGroupIndexes))

				for majorGroup, majorGroupValues in groupby(srcdata, key = lambda x: tuple(x[k] for k in majorGroupIndexes)):
					majorGroupValues = list(majorGroupValues)
					#
					worksheet.write(currow, 0, majorGroup[0], svyFmtRptDataBold)
					filledCells.append((currow, 0))

					for minorGroup, minorGroupValues in groupby(majorGroupValues, key = lambda x: tuple(x[k] for k in minorGroupIndexes)):
						if minorGroup[0] != majorGroup[0]:
							continue

						minorGroupValues = list(minorGroupValues)
						categories = []
						volumeCategories, tonnageCategories = {}, {}
						for row in minorGroupValues:
							category, volume, tonnage = row[-3:]
							if not category in categories:			categories.append(category)
							if not category in volumeCategories:	volumeCategories[category] = 0
							if not category in tonnageCategories:	tonnageCategories[category] = 0
							#
							volumeCategories[category] += row[-2]
							tonnageCategories[category] += row[-1]
						#

						for category in categories:
							idxCategory = svyRptCats.index(category)
							worksheet.write(currow, len(majorGroup) + idxCategory, volumeCategories[category], svyFmtRptData)
							filledCells.append((currow, len(majorGroup) + idxCategory))
							worksheet.write(currow, len(majorGroup) + catNumber + 1 + idxCategory, tonnageCategories[category], svyFmtRptData)
							filledCells.append((currow, len(majorGroup) + catNumber + 1 + idxCategory))
					currow += 1

				### =================================================================================== ###
				### ========================= ФОРМАТИРОВАНИЕ ПУСТЫХ ЯЧЕЕК ============================= ###
				### =================================================================================== ###

				rptColNumber = 1 + len(rptCategories) * 2 + 4
				for irow in range(3, currow):
					for icol in range(rptColNumber):
						if not (irow, icol) in filledCells:
							worksheet.write(irow, icol,  "", svyFmtRptData)
			#
			def exportProject1(workbook, worksheet, srcdata, srcheader):
				# Форматы
				fmtTitle = workbook.add_format({"font": "ISOCPEUR", "size": 14, "align": "center", "valign": "vcenter", "italic": True, "bold": True})
				fmtHeader = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "text_wrap": True, "border": 1})
				fmtHeaderBold = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "text_wrap": True, "bold": True, "border": 1})
				fmtDataRight = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "right", "valign": "vcenter", "border": 1})
				fmtDataRightBold = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "right", "valign": "vcenter", "bold": True, "border": 1})
				fmtDataLeft = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "left", "valign": "vcenter", "border": 1})

				### =================================================================================== ###
				### ================================= СОЗДАНИЕ ЗАГОЛОВКА ============================== ###
				### =================================================================================== ###

				worksheet.merge_range(xl_range(0, 0, 0, 6), "Таблица №1. Порядок и объем горных работ", fmtTitle)

				headerSpanSpec = ( (1,0,3,1), (1,1,3,1), (1,2,3,1), (1,3,3,1), (1,4,2,2), (1,6,2,1), (3,4,1,1), (3,5,1,1), (3,6,1,1) )
				dsnHeaderData = ["Очередность\nведения работ", "Вид работ", "Выработки для\nзаезда СТО", "Условия ведения\nгорных работ", "Объем горных\nработ", "Потери", "м", "м³", "%"]

				colwidths = [(0, 20), (1, 20), (2, 20), (3, 20), (4, 10), (5, 10), (6, 10)]
				for icol, icolWidth in colwidths:
					worksheet.set_column("{0}:{0}".format(xl_col_to_name(icol)), icolWidth)
				# 
				for (irow, icol, rowspan, colspan), headerData in zip(headerSpanSpec, dsnHeaderData):
					fmt = fmtHeaderBold if icol == 0 else fmtHeader
					if rowspan == 1 and colspan == 1:	worksheet.write(irow, icol, headerData, fmt)
					else:								worksheet.merge_range(xl_range(irow, icol, irow + rowspan - 1, icol + colspan - 1), headerData, fmt)

				### =================================================================================== ###
				### ================================= ПОДГОТОВКА ДАННЫХ =============================== ###
				### =================================================================================== ###

				# объединяем поля ВЫРАБОТКА и ИНТЕРВАЛ
				idxProjectMineName = srcheader.index(BMFLD_PROJECT_MINENAME)
				idxProjectInterval = srcheader.index(BMFLD_PROJECT_INTERVAL)
				for i, row in enumerate(srcdata):
					srcdata[i][idxProjectMineName] = ":".join([row[idxProjectMineName], row[idxProjectInterval]])
				del idxProjectInterval

				### =================================================================================== ###
				### =================================== ЭКСПОРТ ДАННЫХ ================================ ###
				### =================================================================================== ###

				idxVolume = srcheader.index(RPTFLD_VOLUME)
				currow = 4
				filledCells = []

				groupColumn = BMFLD_PROJECT_MINENAME
				for group, groupValues in groupby(srcdata, key = lambda x: x[idxProjectMineName]):
					groupValues = list(groupValues)
					#
					worksheet.write(currow, 1, group, fmtDataLeft)
					filledCells.append((currow, 1))
					#
					worksheet.write(currow, 5, round(sum(row[idxVolume] for row in groupValues), 2) if groupValues else "", fmtDataRight)
					filledCells.append((currow, 5))
					currow += 1

				# итогая строка
				worksheet.merge_range(xl_range(currow, 0, currow, 3), "Всего", fmtDataRightBold)
				filledCells.append((currow, 0))
				worksheet.write(currow, 5, '=IF(SUM({0}{1}:{0}{2})=0,"",SUM({0}{1}:{0}{2}))'.format(xl_col_to_name(5), 4, currow), fmtDataRight)
				filledCells.append((currow, 5))
				currow += 1

				### =================================================================================== ###
				### ========================= ФОРМАТИРОВАНИЕ ПУСТЫХ ЯЧЕЕК ============================= ###
				### =================================================================================== ###

				rptColNumber = 7
				for irow in range(3, currow):
					for icol in range(rptColNumber):
						if not (irow, icol) in filledCells:
							worksheet.write(irow, icol,  "", fmtDataRight)
			#
			def exportProject2(workbook, worksheet, srcdata, srcheader):

				# Форматы
				fmtTitle = workbook.add_format({"font": "ISOCPEUR", "size": 14, "align": "center", "valign": "vcenter", "italic": True, "bold": True})
				fmtHeader = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "text_wrap": True, "border": 1})
				fmtHeaderBold = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "text_wrap": True, "border": 1, "bold": True})
				fmtData = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "border": 1})
				fmtDataBold = workbook.add_format({"font": "ISOCPEUR", "size": 10, "align": "center", "valign": "vcenter", "border": 1, "bold": True})

				### =================================================================================== ###
				### ================================= СОЗДАНИЕ ЗАГОЛОВКА ============================== ###
				### =================================================================================== ###

				worksheet.merge_range(xl_range(0, 0, 0, 19), "Таблица №2. Таблица показателей извлечения и содержания металлов", fmtTitle)

				#dsnRptCats = ("Балансовая богатая руда", "Балансовая медистая руда", "Балансовая вкрапленная руда", "Товарная руда")
				rptdsn = self.main_app.settings.getValue(ID_RPT_DSN_INDEXES)

				geocodes = self.main_app.settings.getValue(ID_GEOCODES)
				oreCodes = {code: codeDescription for code, codeDescription, codeType in self.getGeoCodes() if codeType == 0}

				geocodes_balance_indexes =[]
				for idx, i in enumerate(geocodes):
					if i[0][2] == 0:	# 0 - "Рудные породы" -> GEO_VARIATIONS = ["Рудные породы", "Нерудные породы", "Вмещающие породы", "Прочее"]
						geocodes_balance_indexes.append(idx+1) # +1 потому что в комбобохе категорий в настройках отчёта первой строкой добавляется пустое значение

				#rptdsn_balance = filter(lambda x: x[1] in geocodes_balance_indexes ,rptdsn)	
				rptdsn_balance = list(filter(lambda x: x[2] == 1 ,rptdsn))
				
				if len(rptdsn_balance) > 0:				 
					dsnRptBalansCats = list(list(zip(*rptdsn_balance))[0])
				else:
					dsnRptBalansCats = []

				dsnRptBalansOffbalanceCats = list(zip(*rptdsn))[0] 

				dsnRptBalansOffbalanceCats = dsnRptBalansCats + [i for i in dsnRptBalansOffbalanceCats if i not in dsnRptBalansCats]


				dsnRptCatsSalable = "Товарная руда"
				dsnRptCats = dsnRptBalansOffbalanceCats + [dsnRptCatsSalable]

				rptdsn_elements = self.main_app.settings.getValue(ID_RPT_DSN_ELEMENTS)
				dsnElements = list(zip(*rptdsn_elements))[0]


				#region HEADER DEFENITIONS
				h_nomer = {
										'rowspan': 3,
										'colspan': 1,
										'fmt' : fmtHeader,
										'data': '№ П/П'}
				h_cat_work = {
										'rowspan': 3,
										'colspan': 1,
										'fmt' : fmtHeader,
										'data': 'Вид работ'}

				

				h_group_vol = {  
								'rowspan': 2,
								'colspan': 1,
								'fmt' : fmtHeader,
								'data': 'м³'}
				h_group_ton = {  
								'rowspan': 2,
								'colspan': 1,
								'fmt' : fmtHeader,
								'data': 'Т'}
				h__group_vol_ton = {'direction' : 'h',
										'fmt' : fmtHeader,
										'data': [h_group_vol, h_group_ton]}
				h_group_elements = []
				for element in dsnElements:
					h_group_element = {  
									'rowspan': 1,
									'colspan': 1,
									'fmt' : fmtHeader,
									'data': element.strip()}
					h_group_elements.append(h_group_element)

				h__group_e = {'direction' : 'h',
										'fmt' : fmtHeader,
										'data': h_group_elements}


				h_group_e_name = {
										'rowspan': 1,
										'colspan': len(dsnElements),
										'fmt' : fmtHeader,
										'data': 'Содержание, %'}
				h__group_ee_name = {'direction' : 'v',
										'fmt' : fmtHeader,
										'data': [h_group_e_name, h__group_e]}

				h__group_vol_ton_ee_name = {'direction' : 'h',
										'fmt' : fmtHeader,
										'data': [h__group_vol_ton, h__group_ee_name]}

				h__groups = []
				for cat in dsnRptCats:
					h_group_name = {
											'rowspan': 1,
											'colspan': 2 + len (dsnElements),
											'fmt' : fmtHeader,
											'data': cat}
					h__group = {'direction' : 'v',
											'fmt' : fmtHeader,
											'data': [h_group_name, h__group_vol_ton_ee_name]}

					h__groups.append(h__group)

				h_dilution = {
										'rowspan': 3,
										'colspan': 1,
										'fmt' : fmtHeader,
										'data': 'Разубоживание %'}

				h_loss = {
										'rowspan': 3,
										'colspan': 1,
										'fmt' : fmtHeader,
										'data': 'Потери %'}

				h__header_data =   [h_nomer, h_cat_work,  h_dilution, h_loss]
				h__header_data [2:2] = h__groups

				h__header = {'direction' : 'h',
										'fmt' : fmtHeader,
										'data': h__header_data}

				#endregion 


				colwidths = [(0, 8), (1, 20)] + [(i, 8) for i in range(2, 20)]
				for icol, icolWidth in colwidths:
					worksheet.set_column("{0}:{0}".format(xl_col_to_name(icol)), icolWidth)
				# 



				smartHeaderCell = SmartHeaderCell(h__header, 1, 0)
				smartHeaderCell.write_to_worksheet(worksheet)

				### =================================================================================== ###
				### ================================= ПОДГОТОВКА ДАННЫХ =============================== ###
				### =================================================================================== ###

				idxMaterial = srcheader.index(BMFLD_MATERIAL)	
				idxIndex = srcheader.index(BMFLD_INDEX)	
				idxVolume = srcheader.index(RPTFLD_VOLUME)
				idxTonnage = srcheader.index(RPTFLD_FACT_TONNAGE)

				# объединяем поля ВЫРАБОТКА и ИНТЕРВАЛ
				idxProjectMineName = srcheader.index(BMFLD_PROJECT_MINENAME)
				idxProjectInterval = srcheader.index(BMFLD_PROJECT_INTERVAL)
				for i, row in enumerate(srcdata):
					srcdata[i][idxProjectMineName] = ":".join([row[idxProjectMineName], row[idxProjectInterval]])
				del idxProjectMineName, idxProjectInterval

				# заменяем материалы
				geocodes = self.main_app.settings.getValue(ID_GEOCODES)
				concCategory = self.getBackfillCategory()


				lst_index_cat = {}
				for cat in rptdsn:
					for lst_index in cat[3]['INDEX']:
						index = lst_index[0]
						lst_index_cat[index] = cat[0]

				for i, row in enumerate(srcdata):
					index = row[idxIndex]
					if index in lst_index_cat.keys():		srcdata[i][idxMaterial] = lst_index_cat[index]
					else:									srcdata[i][idxMaterial] = dsnRptCatsSalable 



				### =================================================================================== ###
				### =================================== ЭКСПОРТ ДАННЫХ ================================ ###
				### =================================================================================== ###

				idxDensity = srcheader.index(RPTFLD_FACT_DENSITY)
				idxVolume = srcheader.index(RPTFLD_VOLUME)
				idxTonnage = srcheader.index(RPTFLD_FACT_TONNAGE)

				num_dsnRptCats = len(dsnRptCats)
				elementsParams =  OrderedDict()
				num_elements = len(dsnElements)
				for element in dsnElements:
					elementsParams[element]	= {'idxPercent' : 0,
												'idxTonnage' : 0,
												'rptIndexes' : [],
												'percentage_percision' : metal_percision(element)}
					elementsParams.move_to_end(element)
					for idx, field in enumerate(srcheader):
						if element in field and '(%)' in field:
							elementsParams[element]['idxPercent'] = idx
						if element in field and '(т)' in field:
							elementsParams[element]['idxTonnage'] = idx
					
					current_pos = len (elementsParams)-1
					elementsParams[element]['rptIndexes'] = [current_pos + 4 + i * (2+num_elements) for i in range (0,num_dsnRptCats)]


				rptVolIndexes = [2 + i*(2 + num_elements)  for i in range (0,num_dsnRptCats)]
				rptTonIndexes = [3 + i*(2 + num_elements)  for i in range (0,num_dsnRptCats)]
				rptDilIndex = 2 + num_dsnRptCats * (2 + num_elements)

				num_dsnRptBalansCats = len(dsnRptBalansCats)

				currow = 4
				filledCells = []

				groupColumns = [BMFLD_PROJECT_MINENAME]
				groupIndexes = [srcheader.index(column) for column in groupColumns]
				for group, groupValues in groupby(srcdata, key = lambda x: tuple(x[k] for k in groupIndexes)):
					#
					groupValues = list(groupValues)
					worksheet.write(currow, 1, group[0], fmtData)
					filledCells.append((currow, 1))
					#
					for i, rptCat in enumerate(dsnRptCats):
						if rptCat == dsnRptCats[-1]:	data = groupValues.copy()
						else:							data = [row for row in groupValues if row[idxMaterial] == rptCat]

						#
						icol = rptVolIndexes[i]
						sum_ = round(sum([row[idxVolume] for row in data]), 2)
						worksheet.write(currow, icol, sum_ if sum_ else "", fmtData)
						filledCells.append((currow, icol))
						#
						icol = rptTonIndexes[i]
						sum_ = round(sum([row[idxTonnage] for row in data]), 2)
						worksheet.write(currow, icol, sum_ if sum_ else "", fmtData)
						filledCells.append((currow, icol))
						#
						for element in elementsParams:
							icol = elementsParams[element]['rptIndexes'][i]
							idxElTonnage = elementsParams[element] ['idxTonnage']
							sum_ = round(sum([row[idxElTonnage] for row in data]), 2)
							percision = elementsParams[element]['percentage_percision']
							formula = '=IFERROR(ROUND({2}*100/{0}{1},{3}),"")'.format(xl_col_to_name(rptTonIndexes[i]), currow+1, sum_, percision) if sum_ else ""
							worksheet.write(currow, icol, formula, fmtData)
							filledCells.append((currow, icol))

					# тоннаж товарной руды
					totalTonnage = sum([row[idxTonnage] for row in groupValues])
					#oreData = [row[idxTonnage] for row in groupValues if row[idxMaterial] in lst_index_cat.values()]	  
					oreData = [row[idxTonnage] for row in groupValues if row[idxMaterial] in dsnRptBalansCats]	  
					# тоннаж балансовой руды
					oreTonnage = sum(oreData) if oreData else 0
					# рассчет разубоживания
					dilution = round(100 * (totalTonnage - oreTonnage) / totalTonnage, 2) if totalTonnage != 0 else ""
					worksheet.write(currow, rptDilIndex, dilution, fmtData)

					# tonnageCells = [xl_rowcol_to_cell(currow, idx) for idx in rptTonIndexes]
					# formula = '=IFERROR(ROUND(100-100*(SUM({0})/{1}),2),"")'.format(",".join(tonnageCells[:num_dsnRptBalansCats]), tonnageCells[-1])
					# worksheet.write(currow, rptDilIndex, formula, fmtData)
					filledCells.append((currow, rptDilIndex))

					currow += 1
						
				# запись итого
				worksheet.write(currow, 0, "Всего", fmtDataBold)
				filledCells.append((currow, 0))
				#
				formulas = ['=IF(SUM({0}:{1})=0,"",ROUND(SUM({0}:{1}),2))']*2 + ['=IFERROR(ROUND(SUMPRODUCT({0}:{1},{2}:{3})/SUM({0}:{1}),{4}),"")']*num_elements
				formulas = formulas*num_dsnRptCats
				
				for i, formula in enumerate(formulas):
					index = i // (2+num_elements)
					if i % (2+num_elements) in [0,1]:
						formula = formula.format(xl_rowcol_to_cell(4, 2+i), xl_rowcol_to_cell(currow-1, 2+i))
					elif i % (2+num_elements) not in [0,1]:
						# узнаём точность после десятичной точки
						percision = 2 # 2 - по умолчанию
						for element in elementsParams:
							if 2+i in elementsParams[element]['rptIndexes']:
								percision = elementsParams[element]['percentage_percision']	
								break
						#
						formula = formula.format(
							xl_rowcol_to_cell(4, rptTonIndexes[index]), xl_rowcol_to_cell(currow-1, rptTonIndexes[index]),
							xl_rowcol_to_cell(4, 2+i), xl_rowcol_to_cell(currow-1, 2+i)	, percision
						)

					worksheet.write(currow, 2+i, formula, fmtData)
					filledCells.append((currow, 2+i))

				#
				# тоннаж товарной руды
				totalTonnage = sum([row[idxTonnage] for row in srcdata])
				#oreData = [row[idxTonnage] for row in srcdata if row[idxMaterial] in lst_index_cat.values()]  
				oreData = [row[idxTonnage] for row in srcdata if row[idxMaterial] in dsnRptBalansCats]  
				# тоннаж балансовой руды
				oreTonnage = sum(oreData) if oreData else 0
				# рассчет разубоживания
				dilution = round(100 * (totalTonnage - oreTonnage) / totalTonnage, 2) if totalTonnage != 0 else ""
				worksheet.write(currow, rptDilIndex, dilution, fmtData)
				filledCells.append((currow, rptDilIndex))

				currow += 1

				### =================================================================================== ###
				### ========================= ФОРМАТИРОВАНИЕ ПУСТЫХ ЯЧЕЕК ============================= ###
				### =================================================================================== ###

				#rptColNumber = 20
				rptColNumber = 4 +  num_dsnRptCats * (2 + num_elements)
				for irow in range(4, currow):
					for icol in range(rptColNumber):
						if not (irow, icol) in filledCells:
							worksheet.write(irow, icol,  "", fmtData)

			#
			filetypes = "EXCEL (*.XLSX)"
			try:			initialdir = MMpy.Project.path()
			except:			initialdir = os.getcwd()
			reportPath = QtGui.QFileDialog.getSaveFileName(self._bmPreviewDialog, "Выберите файл", initialdir, filetypes).replace("/", "\\")
			if not reportPath:
				return False

			# считывание коэффициента разубоживания
			concCoef = self._bmPreviewDialog.paramsFrame.dilutionCoef()
			concCoefPercent = 1 + concCoef / 100
			concCategory = self.getBackfillCategory()
			#
			# подготовка отчета
			try:
				reportData, reportHeader = self.prepareReport(dilutionCoef = concCoef)
				if reportData is None:
					return False			
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", str(e))
				return False			
			#
			workbook = xl.Workbook(reportPath)

			# Листы
			svyWorksheet = workbook.add_worksheet()
			svyWorksheet.name = "Маркшейдерия"
			dsnWorksheet1 = workbook.add_worksheet()
			dsnWorksheet1.name = "Проектирование №1"
			dsnWorksheet2 = workbook.add_worksheet()
			dsnWorksheet2.name = "Проектирование №2"
			dsnWorksheetFull = workbook.add_worksheet()
			dsnWorksheetFull.name = "Полный отчет"

			# idxMaterial = reportHeader.index(BMFLD_MATERIAL)
			# idxVolume = reportHeader.index(RPTFLD_VOLUME)
			# idxTonnage = reportHeader.index(RPTFLD_FACT_TONNAGE)
			# for i, row in enumerate(reportData):
			# 	material = row[idxMaterial]
			# 	if material != concCategory:
			# 		continue
			# 	reportData[i][idxVolume] = reportData[i][idxVolume] * concCoefPercent
			# 	reportData[i][idxTonnage] = reportData[i][idxTonnage] * concCoefPercent

			exportSurvey(workbook, svyWorksheet, deepcopy(reportData), reportHeader, concCoef)
			exportProject1(workbook, dsnWorksheet1, deepcopy(reportData), reportHeader)
			exportProject2(workbook, dsnWorksheet2, deepcopy(reportData), reportHeader)
			exportSource(workbook, dsnWorksheetFull, deepcopy(reportData), reportHeader)

			workbook.close()

			# запуск отчета
			try:
				os.startfile(reportPath)
			except Exception as e:
				qtmsgbox.show_error(self._bmPreviewDialog, "Ошибка", "Не удалось открыть файл отчета")
				return False
		except Exception as e:
			qtmsgbox.show_error(self._bmPreviewDialog, "Необработанная ошибка", str(e))
			raise
			return False
		return True

	def check(self):
		# проверяем среди выбранных каркасов наличие маркшейдерских каркасов
		tridbItems = tuple(filter(lambda it: it.itemCategory() == ITEM_CATEGORY_DSN, self.selectedItems()[ITEM_TRIDB]))
		if not tridbItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите проектные каркасы")
			return

		# проверяем, что все блочные модели имеют метаданные
		bmItems = self.selectedItems()[ITEM_BM]
		if not bmItems:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выберите блочные модели")
			return

		# #
		sysdate = datetime.now()
		currdates = []
		for bmItem in bmItems:
			netBmPath = bmItem.filePath()

			# проверка наличия файла блокировки
			if self.lockFileExists(netBmPath):
				lockpath = self.getLockPath(netBmPath)
				msg = "Блочная модель используется другим пользователем %s." % get_file_owner(lockpath)
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			bm = BlockModel(netBmPath, self.main_app)
			# проверка наличия метаданных блочных моделей
			if not bm.has_metadata:
				msg = "Блочная модель '%s' не имеет метаданных. Обратитесь к геологу" % bm.name
				qtmsgbox.show_error(self.main_app, "Ошибка", msg)
				return

			# проверка на соответствие метаданных содержанию блочной модели
			metadate = bm.metadata.current_date
			bmdate = bm.blockmodel_date
			if bmdate.year != metadate.year or bmdate.month != metadate.month:
				qtmsgbox.show_error(self.main_app, "Ошибка", "Метаданные не соответствуют содержанию блочной модели '%s'. Обратитесь к геологу." % bm.name)
				return

			currdates.append(metadate)


		currdates = list(sorted(set(currdates)))
		if len(currdates) > 1:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Выбранные блочные модели переведены в разные месяца. Выберите блочные модели, переведенные в один и тот же месяц")
			return
		elif (currdates[0].year == sysdate.year and currdates[0].month != sysdate.month) or (currdates[0].year < sysdate.year):
			if not qtmsgbox.show_question(self, "Внимание", "Состояние блочных моделей не соответствует текущей календарной дате. Продолжить?"):
				return

		return True

	# подготовка к запуску
	def prepare(self):
		if not super(DsnAssignBm, self).prepare():
			return

		# if not self.check():
		# 	return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			if self._use_connection:
				self.main_app.srvcnxn.rollback()
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))
		#
		self.after_run()

	def run(self):
		if not super(DsnAssignBm, self).run():
			return

		progress = self.main_app.mainWidget.progressBar

		bmItems = self.selectedItems()[ITEM_BM]
		# считывание текущей даты
		netBm = BlockModel(bmItems[0].filePath(), self.main_app)
		self._bmCurrentDate = netBm.metadata.current_date
		BMFLD_CURRDATE = date_to_fieldname(self._bmCurrentDate)
		BMFLD_SYS_CURRDATE = "SYS_%s" % BMFLD_CURRDATE
		#
		tridbItems = tuple(filter(lambda it: it.itemCategory() == ITEM_CATEGORY_DSN, self.selectedItems()[ITEM_TRIDB]))
		netTridbPathsDict = self.getTridbPathsDict(tridbItems, copy = False)

		subxsize = self.main_app.settings.getValue(ID_SUBXSVY)
		subysize = self.main_app.settings.getValue(ID_SUBYSVY)
		subzsize = self.main_app.settings.getValue(ID_SUBZSVY)

		backfillCategory = self.getBackfillCategory()

		categories = self.getCategories()[:5]
		filterString = '"%s"' % '", "'.join(categories)

		### =================================================================================== ###
		### =========================== ПРОВЕРКА АТРИБУТОВ КАРКАСОВ =========================== ###
		### =================================================================================== ###

		# запускаем проверку атрибутов, если присваивание каркасов запущено из-под админа или маркшейдера
		if self.main_app.userRole() in [ID_USERDSN, ID_USERADMIN]:
			progress.setText("Проверка атрибутов каркасов")
			try:
				progress_value = progress.value()
				wfvalidation = DsnWireframeAttributeValidation(self.main_app)
				wfvalidation.isSilent = True
				wfvalidation.exec(netTridbPathsDict)
				if not wfvalidation.result:
					return
				progress.setValue(progress_value)
			except Exception as e:
				raise Exception("Невозможно произвести проверку атрибутов каркасов. %s" % str(e))

		### =================================================================================== ###
		### ============================== КОПИРОВАНИЕ КАРКАСОВ =============================== ###
		### =================================================================================== ###

		try:					loctripaths = self.getTridbPathsDict(tridbItems, copy = True)
		except Exception as e:	raise Exception(str(e))
		
		# создаем наборы по проекту
		try:					self._wireframe_sets = self.generateDsnWireframeSets(loctripaths, self._bmCurrentDate, groupByProject = True)
		except Exception as e:	raise Exception("Невозможно создать наборы каркасов по проектам. '{0}'".format(e))

		# создаем единый набор каркасов
		try:					wfset = self.generateDsnWireframeSets(loctripaths, self._bmCurrentDate, groupByProject = False)
		except Exception as e:	raise Exception("Невозможно создать единый набор каркасов по проектам. '{0}'".format(e))

		assignedData = []
		for bmItem in bmItems:
			netBm = BlockModel(bmItem.filePath(), self.main_app)

			netBmPath = netBm.path
			locBmPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s.DAT" % netBm.name)
			locBmPartPath = os.path.join(self._tmpfolder, get_temporary_prefix() + "%s_%s.DAT" % (netBm.name, BMFLD_CURRDATE))
			bkpBmPath = netBm.backup_path

			self._netBmPathList.append(netBmPath)
			self._locBmPathList.append(locBmPath)

			# проверка наличия столбца данных за текущий месяц
			f = MicromineFile()
			if not f.open(netBmPath):			raise Exception("Невозможно открыть файл %s" % netBmPath)
			if not BMFLD_CURRDATE in f.header:	raise Exception("Блочная модель '%s' не содержит поле '%s'" % (netBm.name, BMFLD_CURRDATE))
			f.close()

			### =================================================================================== ###
			### ============================ КОПИРОВАНИЕ БЛОЧНОЙ МОДЕЛИ =========================== ###
			### =================================================================================== ###

			progress.setText("Копирование блочной модели")
			err = self.exec_proc(self.copy_file, netBmPath, locBmPath)
			if err:							raise Exception("Невозможно скопировать файл %s. %s" % (netBmPath, err))
			if self.is_cancelled:			return

			### =================================================================================== ###
			### ======================= УДАЛЕНИЕ ПРОЕКТА ИЗ БЛОЧНОЙ МОДЕЛИ ======================== ###
			### =================================================================================== ###

			# progress.setText("Удаление проекта из блочной модели")
			# try:							self.deleteProjectData(locBmPath, projectList)
			# except Exception as e:			raise Exception("Невозможно удалить из блочной модели проектные данные. %s" % e)
			# if self.is_cancelled:			return

			### =================================================================================== ###
			### ======================= ИЗМЕНЕНИЕ СТРУКТУРЫ БЛОЧНОЙ МОДЕЛИ ======================== ###
			### =================================================================================== ###

			structure = ["%s:C:25:0" % field for field in BMFIELDS_DSN + BMSYSFIELDS_DSN]
			try:							self.addFieldsToBlockModel(locBmPath, structure)
			except Exception as e:			raise Exception("Невозможно добавить поля в файл блочной модели. %s" % str(e))

			### =================================================================================== ###
			### ============================= ПРИСВАИВАНИЕ АТРИБУТОВ ============================== ###
			### =================================================================================== ###

			progress.setText("Присваивание атрибутов")
			loc_bm = BlockModel(locBmPath, self.main_app)

			dbfilter = MMpy.FormSet("DBFILTER","16.0.896.3")
			dataGrid = MMpy.DataGrid(4,1)
			dataGrid.set_column_info(0,1,4)
			dataGrid.set_column_info(1,2,6)
			dataGrid.set_column_info(2,3,-1)
			dataGrid.set_column_info(3,4,14)
			dataGrid.set_row(0, ['=IN([%s], %s)' % (BMFLD_CURRDATE, filterString), "8", "", ""])
			dbfilter.set_field("GRID", dataGrid.serialise())
			dbfilter.set_field("NEGATE","0")
			dbfilter.set_field("COMBINE_EQ","0")
			dbfilter.set_field("OR","0")
			dbfilter.set_field("AND","1")
			dbfilter.set_field("DBFLT_EQ","")
			dbfilter.set_field("FTYPE","0")
			dbfilter.set_field("FILE", locBmPath)

			#
			assing_params = {}
			# assing_params["dbfilter"] = dbfilter
			assing_params["attributes"] = []
			assing_params["attributes"].append(["0", DSN_ATTR_CATEGORY, BMFLD_SYS_PROJECT_CATEGORY])
			assing_params["attributes"].append(["0", DEFAULT_ATTR_CODE, BMFLD_SYS_PROJECT_WORKTYPE])
			assing_params["attributes"].append(["0", DEFAULT_ATTR_TITLE, BMFLD_SYS_PROJECT_MINENAME])
			assing_params["attributes"].append(["0", DSN_ATTR_PROJECT, BMFLD_SYS_PROJECT_NAME])
			assing_params["attributes"].append(["0", DSN_ATTR_STAGE, BMFLD_SYS_PROJECT_STAGE])
			assing_params["attributes"].append(["0", DEFAULT_ATTR_NAME, BMFLD_SYS_PROJECT_INTERVAL])
			assing_params["sub_xsize"] = subxsize
			assing_params["sub_ysize"] = subysize
			assing_params["sub_zsize"] = subzsize
			assing_params["overwrite_bool"] = True
			assing_params["delete_bool"] = True
			assing_params["wireframe_set"] = wfset
			try:					loc_bm.assign_wireframes(assing_params)
			except Exception as e:	raise Exception("Не удалось присвоить атрибуты. %s" % str(e))
			if self.is_cancelled:	return

			### =================================================================================== ###
			### ========================== ИЗВЛЕЧЕНИЕ ПРИСВОЕННЫХ ДАННЫХ ========================== ###
			### =================================================================================== ###

			progress.setText("Извлечение присвоенных данных")
			fBm = MicromineFile()
			if not fBm.open(locBmPath):
				raise Exception("Невозможно открыть файл '%s'" % locBmPath)
			bmHeader = fBm.header
			idxCategory = bmHeader.index(BMFLD_SYS_PROJECT_CATEGORY) if BMFLD_SYS_PROJECT_CATEGORY in bmHeader else None
			if idxCategory is None:
				fBm.close()
				raise Exception("Поле '%s' отсутствует" % BMFLD_PROJECT_CATEGORY)

			bmData = [row for row in fBm.read() if row[idxCategory] != backfillCategory]
			isUseDynamicDensity = netBm.metadata.is_use_dynamic_density
			for  i, row in enumerate(bmData):
				bmData[i].append(isUseDynamicDensity) 
			assignedData.extend(bmData)
			fBm.close()
		bmHeader.append('isUseDynamicDensity')

		### =================================================================================== ###
		### ============================= ОТЧЕТ ПО БЛОЧНЫМ МОДЕЛЯМ ============================ ###
		### =================================================================================== ###

		progress.setText("Создание отчета")
		#
		groupFields = (BMFLD_SYS_PROJECT_MINENAME, BMFLD_SYS_PROJECT_INTERVAL)
		groupIndexes = tuple(bmHeader.index(keyField) for keyField in groupFields)
		assignedData.sort(key = lambda x: tuple(x[index] for index in groupIndexes))
		#
		# инициализация диалога
		canSeeGrades = self.main_app.userRole() in [ID_USERGEO, ID_USERADMIN]

		self._bmPreviewDialog = BlockModelPreviewDialog(self)
		self._bmPreviewDialog.setCanSeeGrades(canSeeGrades)
		self._bmPreviewDialog.setNeedsRestoreGeology(False)
		self._bmPreviewDialog.setNeedsRestoreMiningWork(False)
		self._bmPreviewDialog.setNeedsSimulateConcrete(True)
		self._bmPreviewDialog.setFldCurrdate(BMFLD_CURRDATE)
		self._bmPreviewDialog.setGroupFields(groupFields)
		# настройка функциональности кнопок
		self._bmPreviewDialog.buttonLoad.clicked.connect(self.loadToVizex)
		self._bmPreviewDialog.buttonSave.clicked.connect(self.saveToDb)
		self._bmPreviewDialog.buttonExport.clicked.connect(self.exportToExcel)
		# настройка видимости кнопок
		self._bmPreviewDialog.buttonLoad.setVisible(True)
		self._bmPreviewDialog.buttonContinue.setVisible(False)
		self._bmPreviewDialog.buttonCancel.setText("Закрыть")
		self._bmPreviewDialog.buttonExport.setVisible(True)
		self._bmPreviewDialog.buttonUpdate.setVisible(False)
		self._bmPreviewDialog.buttonSave.setVisible(True)
		self._bmPreviewDialog.paramsFrame.setVisible(True)
		self._bmPreviewDialog.paramsFrame.groupBoxReportType.setVisible(False)

		#
		reportParams = dict(
			bmData = assignedData,
			bmHeader = bmHeader,
			fieldCurrDate = BMFLD_CURRDATE,
			concDensity = float(self.main_app.settings.getValue(ID_CONCDENS)),
			elemFields = self.getIdwElements(),
			elemUnits = self.getIdwUnits(),
			geoCodes = self.main_app.settings.getValue(ID_GEOCODES),
			svyCatsAndCodes = self.main_app.settings.getValue(ID_SVYCAT)
		)
		sysKeys = []
		for group, groupValues in groupby(assignedData, key = lambda x: tuple(x[index] for index in groupIndexes)):
			sysKey = tuple(group)
			bmData = tuple(groupValues)

			reportParams["bmData"] = bmData
			reportParams["bmHeader"] = bmHeader

			reportData, reportHeader = bmDataPreviewReport(**reportParams)

			listItem = createBmPreviewListTreeItem(":".join(sysKey), ITEM_WIREFRAME, ITEM_CATEGORY_DSN)
			listItem.setKey(sysKey)
			self._bmPreviewDialog.addBlockModelPreview(listItem, reportData, reportHeader)
			#
			self._bmPreviewDialog.setMaterialSelected(True)





		progress.setText("Выбор данных для занесения в блочную модель")

		if not self._bmPreviewDialog.is_empty:
			self._bmPreviewDialog.exec()
		else:
			qtmsgbox.show_information(self.main_app, "Внимание", "Нет данных. Вероятно выбранные блочные модели не пересекаются с выбранными каркассами.")

		return False

class SmartHeaderCell():
	def __init__(self, SmartHeaderCellDict, RestRow = 0, RestCol = 0, **kwargs):
		self._cDict = SmartHeaderCellDict
		self._p_row = RestRow
		self._p_col = RestCol
		self.__createVariables()

	def __createVariables(self):

		self._rowspan = self._cDict['rowspan'] if 'rowspan' in self._cDict else 0
		self._colspan = self._cDict['colspan'] if 'colspan' in self._cDict else 0
		self._fmt = self._cDict['fmt']
		self._data = self._cDict['data']



	def write_to_worksheet (self, worksheet) :

		if isinstance(self._data, list):
			max_span = 0
			_direction =  self._cDict['direction']
			for cell in self._data:

				if not isinstance(cell, dict):
					continue

				child_cell = SmartHeaderCell(cell, self._p_row, self._p_col)
				child_cell.write_to_worksheet(worksheet)

				if _direction == 'v':
					self._rowspan = self._rowspan + child_cell._rowspan
					if max_span < child_cell._colspan:
						max_span = child_cell._colspan

					self._p_row = self._p_row + child_cell._rowspan

				else:
					self._colspan = self._colspan + child_cell._colspan
					if max_span < child_cell._rowspan:
						max_span = child_cell._rowspan

					self._p_col = self._p_col + child_cell._colspan


			if _direction == 'v':
				self._colspan = max_span
			else:
				self._rowspan = max_span

		else:
			if self._rowspan == 1 and self._colspan  == 1:
				worksheet.write(self._p_row, self._p_col, self._data, self._fmt)
			else:
				range = xl_range(self._p_row, self._p_col, self._p_row + self._rowspan - 1, self._p_col + self._colspan  - 1)
				worksheet.merge_range(range, self._data, self._fmt)