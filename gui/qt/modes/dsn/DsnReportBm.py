try:			import MMpy
except:			pass

import os

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.modes.dsn.DsnAssignBm import DsnAssignBm
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DsnReportBm(DsnAssignBm):
	def __init__(self, *args, **kw):
		super(DsnReportBm, self).__init__(*args, **kw)
