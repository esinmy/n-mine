from PyQt4 import QtGui, QtCore
import os

from mm.formsets import *

from utils.constants import *
from utils.blockmodel import BlockModel
from utils.nnutil import date_to_fieldname
from utils.osutil import get_temporary_prefix

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.ProcessWindow import ProcessWindow
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DisplayData(ProcessWindow):
	def __init__(self, *args, **kw):
		super(DisplayData, self).__init__(*args, **kw)

	def check(self):
		if len(self.selectedItems()) == 0:
			qtmsgbox.show_error(self.main_app, "Ошибка", "Нет объектов для загрузки")
			return
		return True

	def prepare(self):
		if not super(DisplayData, self).prepare():
			return

		if not self.check():
			return

		#
		try:
			self._is_running = True
			self.run()
			self._is_running = False
			self._is_success = True
		except Exception as e:
			self._is_running = False
			self._in_process = False
			qtmsgbox.show_error(self.main_app, "Ошибка", str(e))

		self.after_run()

	def run(self):
		if not super(DisplayData, self).run():
			return

		progress = self.main_app.mainWidget.progressBar
		selex = self.main_app.mainWidget.selectionExplorer
		wfilter = selex.tableWireframeFilter

		selectedCategories = list(it.itemCategory() for it in selex.explorer.children())
		selectedItems = list(it for it in selex.explorer.iterChildren() if it.itemType() not in [ITEM_DIR, ITEM_MODEL])

		FS_ITEM_TYPE_DICT = {ITEM_BM: loadBlockModel, ITEM_DATA: loadPoints, ITEM_STRING: loadStrings, ITEM_TRIDB: loadTriangulationSet,
								ITEM_WIREFRAME: loadTriangulation, ITEM_ANNOTATION: defaultLoadAnnotation}

		# Хотелка (поиск: Василий Радзион) грузить все по умолчанию
		displayCopyFlag = selex.displayParams.checkBoxSelectAll.isChecked()
		for i in range(len(selectedItems)):
			item = selectedItems[i]
			itcat = item.itemCategory()
			userRole = self.main_app.userRole()
			isOwner = userRole == ID_USERADMIN or itcat == ITEM_ROLE_CATEGORY_DICT[userRole] or itcat == ITEM_CATEGORY_USER
			if displayCopyFlag and item.canModify() and isOwner:
				item.setModifyMode(True)
			elif hasattr(item, "setModifyMode"):
				item.setModifyMode(False)

		#
		WFSET_DICT = {}

		#
		n = len(selectedItems) + len([item for item in selectedItems if item.canModify() and item.modifyMode()])

		if n == 0:
			raise Exception("Нет выбранных объектов")

		self.progress_step = 100 / n
		current_progress = 0
		for item in selectedItems:
			if self.is_cancelled:
				return

			ittype = item.itemType()
			itpath = "|".join([x.text(0) for x in reversed(item.iterParents()[:-2])])
			filepath = item.filePath()
			itname = item.text(0)
			itparent = item.parent()
			itcat = item.itemCategory()
			itcatName = item.itemCategoryName()
			#
			setId = item.formsetId()

			copyFlag = False
			if (item.canModify() and not item.modifyMode()) or (ittype == ITEM_BM):
				progress.setText("Копирование %s" % itname)
				tmppath = os.path.join(self._tmpfolder, get_temporary_prefix() + os.path.basename(filepath))
				self.copy_file(filepath, tmppath)
				filepath = tmppath
				copyFlag = True

			if not copyFlag:
				itname = ">>"  + itname

			#
			if selex.displayParams.checkBoxGroupCategories.isChecked() or not itpath:
				display_name = "|".join([itcatName, itname])
			else:
				display_name = "|".join([itcatName, itpath, itname])

			if ittype == ITEM_TRIDB:
				if not copyFlag:	cat = ">>" + itcatName
				else:				cat = itcatName
				cat = itcatName

				if selex.displayParams.checkBoxGroupCategories.isChecked():
					key = ("|".join([cat, MMTYPE_TRIDB]), itcat, setId)
					if not key in WFSET_DICT:
						WFSET_DICT[key] = []
				else:
					if itpath:	key = ("|".join([cat, itpath, MMTYPE_TRIDB]), itcat, setId)
					else:		key = ("|".join([cat, MMTYPE_TRIDB]), itcat, setId)
					if not key in WFSET_DICT:
						WFSET_DICT[key] = []

				if not item.childCount():
					WFSET_DICT[key].append([filepath, DEFAULT_ATTR_NAME, "*"])
				else:
					for k in range(item.childCount()):
						WFSET_DICT[key].append([filepath, DEFAULT_ATTR_NAME, item.child(k).text(0)])

			progress.setText("Загрузка %s" % itname)
			current_progress += self.progress_step
			progress.setValue(current_progress)

			if ittype == ITEM_BM:
				bmdate = BlockModel(filepath, self.main_app).blockmodel_date
				hatchfield = date_to_fieldname(bmdate)
				try:					FS_ITEM_TYPE_DICT[ittype](filepath, display_name, setId, hatchfield)
				except Exception as e:	qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно загрузить '%s'. %s" % (itname, str(e)))
			elif ittype == ITEM_ANNOTATION:
				try:					FS_ITEM_TYPE_DICT[ittype](filepath, display_name)
				except Exception as e:	qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно загрузить '%s'. %s" % (itname, str(e)))
			elif not ittype in [ITEM_TRIDB, ITEM_WIREFRAME]:
				try:					FS_ITEM_TYPE_DICT[ittype](filepath, display_name, setId)
				except Exception as e:	qtmsgbox.show_error(self.main_app, "Ошибка", "Невозможно загрузить '%s'. %s" % (itname, str(e)))

		if self.is_cancelled:
			return

		#
		WF_AND = str(int(wfilter.radioButtonAnd.isChecked()))
		for display_name, itcat, setId in WFSET_DICT:
			if self.is_cancelled:
				return

			key = (display_name, itcat, setId)

			if not WFSET_DICT[key]:
				continue

			# если имеются условия в фильтре
			userFilter = []
			for iRow in range(wfilter.gridEditorFilter.dataGrid.rowCount()):
				cat = wfilter.gridEditorFilter.dataGrid.cellWidget(iRow, 0).currentText()
				if cat == itcatName:
					attr = wfilter.gridEditorFilter.dataGrid.cellWidget(iRow, 1).currentText()
					value = wfilter.gridEditorFilter.dataGrid.cellWidget(iRow, 2).currentText()
					userFilter.append([attr, value])

			wfsetData = []
			if not userFilter:
				wfsetData = [row for row in WFSET_DICT[key]]
			else:
				for i, row in enumerate(WFSET_DICT[key]):
					tripath = row[0]
					# задаем пользовательский фильтр
					for attr, value in userFilter:
						wfsetData.append([tripath, attr, value])

			wfset = create_wireframe_set(wfsetData, _and = WF_AND)

			try:
				loadTriangulationSet(wfset, display_name, setId)
			except Exception as e:
				print ("error", str(e))
				pass
