from PyQt4 import QtGui, QtCore

from utils.qtutil import iterWidgetParents

from utils.constants import *
from utils.osutil import is_number
import geometry as geom
import math
from directions.svydbapi import SvyPoint
from directions.measure import OffsetsMeasure
import gui.qt.dialogs.messagebox as qtmsgbox
import os
from utils.logger import  NLogger
from mm.mmutils import MicromineFile

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class ProjectNavigationBar(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(ProjectNavigationBar, self).__init__(*args, **kw)

		self.__createWidgets()
		self.__gridWidgets()

	def __createWidgets(self):
		self.mainLayout = QtGui.QHBoxLayout()
		self.mainLayout.setContentsMargins(1, 1, 1, 5)

		self.axis_button = QtGui.QPushButton(self)
		self.axis_button.setMinimumSize(QtCore.QSize(120, 0))
		self.axis_button.setMaximumSize(QtCore.QSize(120, 30))
		self.axis_button.setText("Ось из файла")

		self.lbl_project_num = QtGui.QLabel(self)
		self.lbl_project_num.setMinimumSize(QtCore.QSize(120, 0))
		self.lbl_project_num.setText("Проект 1/1")
		self.lbl_project_num.setAlignment(QtCore.Qt.AlignCenter)

		self.prev_project_button = QtGui.QPushButton(self)
		self.prev_project_button.setMinimumSize(QtCore.QSize(120, 0))
		self.prev_project_button.setMaximumSize(QtCore.QSize(120, 30))
		self.prev_project_button.setText("Предыдущий")
		self.prev_project_button.setDisabled(True)

		self.next_project_button = QtGui.QPushButton(self)
		self.next_project_button.setMinimumSize(QtCore.QSize(120, 0))
		self.next_project_button.setMaximumSize(QtCore.QSize(120, 30))
		self.next_project_button.setText("Добавить проект")

		self.delete_project_button = QtGui.QPushButton(self)
		self.delete_project_button.setMinimumSize(QtCore.QSize(120, 0))
		self.delete_project_button.setMaximumSize(QtCore.QSize(120, 30))
		self.delete_project_button.setText("Удалить проект")

		self.load_project_vzx_button = QtGui.QPushButton(self)
		self.load_project_vzx_button.setMinimumSize(QtCore.QSize(170, 0))
		self.load_project_vzx_button.setMaximumSize(QtCore.QSize(170, 30))
		self.load_project_vzx_button.setText("Загрузить проект в Визекс")

	def __gridWidgets(self):
		self.mainLayout.setStretch(1, 1)
		self.setLayout(self.mainLayout)
		self.mainLayout.addWidget(self.axis_button)
		self.mainLayout.addWidget(self.lbl_project_num)
		self.mainLayout.addWidget(self.prev_project_button)
		self.mainLayout.addWidget(self.next_project_button)
		self.mainLayout.addWidget(self.delete_project_button)
		self.mainLayout.addWidget(self.load_project_vzx_button)

class ProjectStackWidget(QtGui.QStackedWidget):
	def __init__(self, *args, **kw):
		super(ProjectStackWidget, self).__init__(*args, **kw)
		self._projects = []
	def add_project(self, project):
		self.addWidget(project)
		self._projects.append(project)

	def clear(self):
		self._projects[0].clear()
	@property
	def projects(self):			return self._projects
	@property
	def projectsCount(self):		return len(self._projects)
	def deleteProject(self, index):
		if index > 0:
			for i in range(self.projectsCount-1, index-1, -1):
				self._projects[i].setParent(None)
				self._projects.pop()
		else:
			self.clear()
		self.projects[-1].doCalculation()
	def doCalculation(self, item):
		# проверяем, если было изменено имя передней точки
		# по какой-то причине в каких-то случаях текущим гридом оказывается ControlGrid,
		# в то время как работает в DirectionGrid. Не нашел проблему, поэтому проверяем
		# сначала, еслить текущий грид в списке
		pass

class ProjectDirectionGrid(QtGui.QTableWidget):
	def __init__(self, *args, **kw):
		super(ProjectDirectionGrid, self).__init__(*args, **kw)

		self.setColumnCount(6)
		self.setRowCount(2)

		self.__buildGrid()
	def __buildGrid(self):
		self.verticalHeader().setVisible(False)
		self.horizontalHeader().setStretchLastSection(True)

		self.setMinimumSize(QtCore.QSize(1000, 100))
		self.setMaximumSize(QtCore.QSize(1000, 100))

		columnWidth = [245, 150, 150, 150, 150, 150]
		columnNames = ["Точка", "Имя", "Виски", "Восток (Y)", "Север (X)", "Z"]
		for i, name in enumerate(columnNames):
			self.setColumnWidth(i, columnWidth[i])
			# header
			item = QtGui.QTableWidgetItem(name)
			self.setHorizontalHeaderItem(i, item)
			# items
			item = QtGui.QTableWidgetItem()
			item.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
			self.setItem(0, i, item)

			item = QtGui.QTableWidgetItem()
			item.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
			self.setItem(1, i, item)

		self.item(0,0).setText("Стояния")
		self.item(1,0).setText("Направления")

class ProjectWidget(QtGui.QWidget):
	def __init__(self, parent):
		super(ProjectWidget, self).__init__(parent)
		self.__create_variables()
		self.__create_widgets()
		self.__grid_widgets()

	def __create_variables(self):
		self.init_east = self.init_north = self.init_z = ''
		self.end_east = self.end_north = self.end_z = ''
		self.bias_step = self.bias_accuracy = self.bias_max_dist = self.bias_up = self.bias_down = self.bias_width = ''

	def __create_widgets(self):
		self.project_parameters = QtGui.QWidget(self)

		self.init_point_grpbx = QtGui.QGroupBox(self.project_parameters)
		self.init_point_grpbx.setTitle('Начальная точка проекта')
		self.init_east_label = QtGui.QLabel(self.init_point_grpbx)
		self.init_east_label.setText("Y (Восток)")
		self.init_east_label.setMinimumSize(QtCore.QSize(65, 0))
		self.init_east_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.init_east_eline = QtGui.QLineEdit(self.init_point_grpbx)
		self.init_east_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.init_north_label = QtGui.QLabel(self.init_point_grpbx)
		self.init_north_label.setText("X (Север)")
		self.init_north_label.setMinimumSize(QtCore.QSize(65, 0))
		self.init_north_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.init_north_eline = QtGui.QLineEdit(self.init_point_grpbx)
		self.init_north_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.init_z_label = QtGui.QLabel(self.init_point_grpbx)
		self.init_z_label.setText("Z (Отметка)")
		self.init_z_label.setMinimumSize(QtCore.QSize(65, 0))
		self.init_z_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.init_z_eline = QtGui.QLineEdit(self.init_point_grpbx)
		self.init_z_eline.setMaximumSize(QtCore.QSize(110, 20))

		self.end_point_grpbx = QtGui.QGroupBox(self.project_parameters)
		self.end_point_grpbx.setTitle('Конечная точка проекта')
		self.end_east_label = QtGui.QLabel(self.end_point_grpbx)
		self.end_east_label.setText("Y (Восток)")
		self.end_east_label.setMinimumSize(QtCore.QSize(65, 0))
		self.end_east_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.end_east_eline = QtGui.QLineEdit(self.end_point_grpbx)
		self.end_east_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.end_north_label = QtGui.QLabel(self.end_point_grpbx)
		self.end_north_label.setText("X (Север)")
		self.end_north_label.setMinimumSize(QtCore.QSize(65, 0))
		self.end_north_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.end_north_eline = QtGui.QLineEdit(self.end_point_grpbx)
		self.end_north_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.end_z_label = QtGui.QLabel(self.end_point_grpbx)
		self.end_z_label.setText("Z (Отметка)")
		self.end_z_label.setMinimumSize(QtCore.QSize(65, 0))
		self.end_z_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.end_z_eline = QtGui.QLineEdit(self.end_point_grpbx)
		self.end_z_eline.setMaximumSize(QtCore.QSize(110, 20))

		self.bias_grpbx = QtGui.QGroupBox(self.project_parameters)
		self.bias_grpbx.setTitle('Параметры домеров')
		self.bias_step_label = QtGui.QLabel(self.bias_grpbx)
		self.bias_step_label.setText("Размер шага")
		self.bias_step_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.bias_step_eline = QtGui.QLineEdit(self.bias_grpbx)
		self.bias_step_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.bias_tol_label = QtGui.QLabel(self.bias_grpbx)
		self.bias_tol_label.setText("Допуск домеров")
		self.bias_tol_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.bias_tol_eline = QtGui.QLineEdit(self.bias_grpbx)
		self.bias_tol_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.bias_max_dist_label = QtGui.QLabel(self.bias_grpbx)
		self.bias_max_dist_label.setText("Макс. расстояние")
		self.bias_max_dist_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.bias_max_dist_eline = QtGui.QLineEdit(self.bias_grpbx)
		self.bias_max_dist_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.bias_up_label = QtGui.QLabel(self.bias_grpbx)
		self.bias_up_label.setText("Смещение вверх")
		self.bias_up_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.bias_up_eline = QtGui.QLineEdit(self.bias_grpbx)
		self.bias_up_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.bias_down_label = QtGui.QLabel(self.bias_grpbx)
		self.bias_down_label.setText("Смещение вниз")
		self.bias_down_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.bias_down_eline = QtGui.QLineEdit(self.bias_grpbx)
		self.bias_down_eline.setMaximumSize(QtCore.QSize(110, 20))
		self.bias_width_label = QtGui.QLabel(self.bias_grpbx)
		self.bias_width_label.setText("Ширина выработки")
		self.bias_width_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
		self.bias_width_eline = QtGui.QLineEdit(self.bias_grpbx)
		self.bias_width_eline.setMaximumSize(QtCore.QSize(110, 20))


		self.project_options = QtGui.QWidget(self)
		self.reverse_chbox = QtGui.QCheckBox(self.project_options)
		self.reverse_chbox.setText("Направление назад")
		self.reverse_chbox.setMinimumSize(QtCore.QSize(110, 0))

	def __grid_widgets(self):
		self.main_layout = QtGui.QVBoxLayout(self)
		self.main_layout.setContentsMargins(2, 5, 2, 0)
		self.main_layout.setSpacing(0)

		self.main_layout.addWidget(self.project_parameters)
		self.project_parameters_layout = QtGui.QHBoxLayout(self.project_parameters)
		self.project_parameters_layout.setMargin(0)
		
		self.project_parameters_layout.addWidget(self.init_point_grpbx)
		self.init_point_grpbx_layout = QtGui.QFormLayout(self.init_point_grpbx)
		self.init_point_grpbx_layout.setContentsMargins(40, 9, 40, 9)
		self.init_point_grpbx_layout.setWidget(0, QtGui.QFormLayout.LabelRole, self.init_east_label)
		self.init_point_grpbx_layout.setWidget(0, QtGui.QFormLayout.FieldRole, self.init_east_eline)
		self.init_point_grpbx_layout.setWidget(1, QtGui.QFormLayout.LabelRole, self.init_north_label)
		self.init_point_grpbx_layout.setWidget(1, QtGui.QFormLayout.FieldRole, self.init_north_eline)
		self.init_point_grpbx_layout.setWidget(2, QtGui.QFormLayout.LabelRole, self.init_z_label)
		self.init_point_grpbx_layout.setWidget(2, QtGui.QFormLayout.FieldRole, self.init_z_eline)
		
		self.project_parameters_layout.addWidget(self.end_point_grpbx)
		self.end_point_grpbx_layout = QtGui.QFormLayout(self.end_point_grpbx)
		self.end_point_grpbx_layout.setContentsMargins(40, 9, 40, 9)
		self.end_point_grpbx_layout.setWidget(0, QtGui.QFormLayout.LabelRole, self.end_east_label)
		self.end_point_grpbx_layout.setWidget(0, QtGui.QFormLayout.FieldRole, self.end_east_eline)
		self.end_point_grpbx_layout.setWidget(1, QtGui.QFormLayout.LabelRole, self.end_north_label)
		self.end_point_grpbx_layout.setWidget(1, QtGui.QFormLayout.FieldRole, self.end_north_eline)
		self.end_point_grpbx_layout.setWidget(2, QtGui.QFormLayout.LabelRole, self.end_z_label)
		self.end_point_grpbx_layout.setWidget(2, QtGui.QFormLayout.FieldRole, self.end_z_eline)

		self.project_parameters_layout.addWidget(self.bias_grpbx)
		self.bias_grpbx_layout = QtGui.QGridLayout(self.bias_grpbx)
		self.bias_grpbx_layout.setContentsMargins(40, 9, 40, 9)
		self.bias_grpbx_layout.addWidget(self.bias_step_label, 1, 1, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_step_eline, 1, 2, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_tol_label, 2, 1, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_tol_eline, 2, 2, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_max_dist_label, 3, 1, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_max_dist_eline, 3, 2, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_up_label, 1, 3, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_up_eline, 1, 4, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_down_label, 2, 3, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_down_eline, 2, 4, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_width_label, 3, 3, 1, 1)
		self.bias_grpbx_layout.addWidget(self.bias_width_eline, 3, 4, 1, 1)

		self.main_layout.addWidget(self.project_options)
		self.project_options_layout = QtGui.QHBoxLayout(self.project_options)
		self.project_options_layout.setContentsMargins(1, 0, 0, 0)
		self.project_options_layout.addWidget(self.reverse_chbox)

	def clear(self):
		self.init_east_eline.setText('')
		self.init_north_eline.setText('')
		self.init_z_eline.setText('')

		self.end_east_eline.setText('')
		self.end_north_eline.setText('')
		self.end_z_eline.setText('')

		self.bias_step_eline.setText('')
		self.bias_tol_eline.setText('')
		self.bias_max_dist_eline.setText('')
		self.bias_up_eline.setText('')
		self.bias_down_eline.setText('')
		self.bias_width_eline.setText('')

class ProjectInformationFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(ProjectInformationFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		window = iterWidgetParents(self)[-2]
		self._parent = iterWidgetParents(self)[-2]
		self._cnxn = window.main_app.srvcnxn

		stpt = SvyPoint(self._cnxn)
		fwpt = SvyPoint(self._cnxn)
		self._measure = [OffsetsMeasure(stpt, fwpt), ]

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxDirection = QtGui.QGroupBox(self)
		self.groupBoxDirection.setTitle("Направление")
		self.layoutDirection = QtGui.QVBoxLayout()

		self.gridSurvey = ProjectDirectionGrid(self.groupBoxDirection)

		self.project_group_box = QtGui.QGroupBox(self)
		self.project_group_box.setTitle("Рассчитать домеры")
		self.project_group_box.setCheckable(True)
		self.project_group_box.setChecked(False)

		self.project_stack = ProjectStackWidget(self.project_group_box)
		self.project_stack.add_project(ProjectWidget(self.project_stack))

		self.project_nav_bar = ProjectNavigationBar(self.project_group_box)

	def __createBindings(self):
		self.project_nav_bar.prev_project_button.clicked.connect(self.goto_prev_project)
		self.project_nav_bar.next_project_button.clicked.connect(self.goto_next_project)
		self.project_nav_bar.delete_project_button.clicked.connect(self.delete_current_project)
		self.project_nav_bar.axis_button.clicked.connect(self.get_axis_from_file)

		self.__create_bindings_for_new_project()

	def __create_bindings_for_new_project(self):
		self.project_stack.currentWidget().init_east_eline.textChanged.connect(self.doCalculation)
		self.project_stack.currentWidget().init_north_eline.textChanged.connect(self.doCalculation)
		self.project_stack.currentWidget().init_z_eline.textChanged.connect(self.doCalculation)
		self.project_stack.currentWidget().end_east_eline.textChanged.connect(self.doCalculation)
		self.project_stack.currentWidget().end_north_eline.textChanged.connect(self.doCalculation)
		self.project_stack.currentWidget().end_z_eline.textChanged.connect(self.doCalculation)
		self.project_stack.currentWidget().bias_down_eline.textChanged.connect(self.doCalculation)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxDirection)
		self.groupBoxDirection.setLayout(self.layoutDirection)
		self.layoutDirection.addWidget(self.gridSurvey)

		self.layoutMain.addWidget(self.project_group_box)
		self.project_group_box_layout = QtGui.QVBoxLayout(self.project_group_box)
		self.project_group_box_layout.setContentsMargins(2, 5, 2, 0)
		self.project_group_box_layout.addWidget(self.project_stack)
		self.project_group_box_layout.addWidget(self.project_nav_bar)

	def goto_prev_project(self):
		index = self.project_stack.currentIndex()
		if index > 0:
			self.project_stack.setCurrentIndex(index-1)
		self.setActions()
		self.doCalculation()

	def goto_next_project(self):
		index = self.project_stack.currentIndex()
		if index < self.project_stack.count() - 1:
			self.project_stack.setCurrentIndex(index+1)
		else:
			self.project_stack.add_project(ProjectWidget(self.project_stack))
			self._measure.append(OffsetsMeasure(self._measure[0].standpoint, self._measure[0].dirpoint, seq_num=index+2))
			# self.setStandPoint
			self.project_stack.setCurrentIndex(index+1)
			self.__create_bindings_for_new_project()
		self.setActions()
		self.doCalculation()

	def delete_current_project(self):
		index = self.project_stack.currentIndex()
		num_projects = self.project_stack.count()
		if index == 0 and num_projects == 1:
			self.project_stack.projects[0].clear()
		else:
			if qtmsgbox.show_question(self, "Внимание", "Вы уверены, что хотите удалить текущий проект?"):
				self.project_stack.removeWidget(self.project_stack.projects[index])
				self.project_stack.projects.pop(index)
				self._measure.pop(index)
		# заново нумеруем экземпляры домеров
		for index, m in enumerate(self._measure):
			m.seq_num = index + 1

		self.setActions()
		self.doCalculation()

	def load_project_vzx(self):
		pass

	def setActions(self):
		index = self.project_stack.currentIndex()
		if index == 0 or index == self.project_stack.count() - 1:
			if index == 0:		self.project_nav_bar.prev_project_button.setDisabled(True)
			else:				self.project_nav_bar.prev_project_button.setDisabled(False)

		else:
			self.project_nav_bar.prev_project_button.setDisabled(False)
			self.project_nav_bar.next_project_button.setDisabled(False)

		if index == self.project_stack.count() - 1:
			self.project_nav_bar.next_project_button.setText('Добавить проект')
		else:
			self.project_nav_bar.next_project_button.setText('Следующий')

		#
		self.project_nav_bar.lbl_project_num.setText("Проект %d/%d" % (index+1, self.project_stack.count()))

	def set_index_and_do_actions(self, i):
		self.project_stack.setCurrentIndex(i)
		self.setActions()
		self.doCalculation()

	def get_axis_from_file(self):
		path = self.browse_open_file()
		if path:
			self.fill_project_fields_from_axis_file(path)

	def browse_open_file(self):
		filetypes = "ДАННЫЕ (*.DAT);;СТРИНГ (*.STR)"
		try:
			initialdir = self._parent.settings.getValue(ID_NETPATH)
		except:
			initialdir = os.getcwd()
		path = QtGui.QFileDialog.getOpenFileName(self._parent, "Выберите файл оси", initialdir, filetypes).replace("/", "\\")
		if not path:
			return None
		return path

	def fill_project_fields_from_axis_file(self, path):
		f = MicromineFile()
		if not f.open(path):
			qtmsgbox.show_error(self._parent, "Ошибка", "Невозможно открыть файл %s" % path)
			return
		if f.records_count != 2:
			qtmsgbox.show_error(self._parent, "Ошибка",
			                    "Количество записей в файле: {}.\nВ файле должна быть информация только о двух точках.".format(f.records_count))
			return
		try:
			data = f.read(columns=["EAST", "NORTH", "RL"])
		except Exception as e:
			qtmsgbox.show_error(self._parent, "Ошибка", "Невозможно считать файл %s. %s" % (path, str(e)))
			return
		f.close()
		print('заполнение осей')
		self.project_stack.currentWidget().init_east_eline.setText('{:.4f}'.format(data[0][0]))
		self.project_stack.currentWidget().init_east = data[0][0]
		self.project_stack.currentWidget().init_north_eline.setText('{:.4f}'.format(data[0][1]))
		self.project_stack.currentWidget().init_north = data[0][1]
		self.project_stack.currentWidget().init_z_eline.setText('{:.4f}'.format(data[0][2]))
		self.project_stack.currentWidget().init_z = data[0][2]

		self.project_stack.currentWidget().end_east_eline.setText('{:.4f}'.format(data[1][0]))
		self.project_stack.currentWidget().end_east = data[1][0]
		self.project_stack.currentWidget().end_north_eline.setText('{:.4f}'.format(data[1][1]))
		self.project_stack.currentWidget().end_north = data[1][1]
		self.project_stack.currentWidget().end_z_eline.setText('{:.4f}'.format(data[1][2]))
		self.project_stack.currentWidget().end_z = data[1][2]

	@property
	def measure(self):			return self._measure
	@measure.setter
	def measure(self, m):
		self._measure = m
		for index, every_m in enumerate(self._measure):
			if type(every_m) != OffsetsMeasure:
				raise TypeError("%s - неверный тип измерения. Ожидался OffsetsMeasure" % str(type(OffsetsMeasure)))
			self.setStandPoint(index, every_m.standpoint)

			if index > 0:
				self.project_stack.add_project(ProjectWidget(self.project_stack))
				self.project_stack.setCurrentIndex(index)
				self.__create_bindings_for_new_project()
				self.setActions()

			self.setParams(index)
			if index == len(m) - 1:
				self.project_stack.setCurrentIndex(0)
				self.setActions()

	def setStandPoint(self, index, pt):
		self.measure[index].standpoint = pt
		if not pt.name:
			self.measure[index].standpoint.delete_coords()
			self.gridSurvey.item(0, 1).setText("")
			self.gridSurvey.item(0, 3).setText("")
			self.gridSurvey.item(0, 4).setText("")
			self.gridSurvey.item(0, 5).setText("")
		else:
			self.gridSurvey.item(0, 1).setText(pt.name)
			self.gridSurvey.item(0, 3).setText("%.3f" % pt.x if not math.isnan(pt.x) else "")
			self.gridSurvey.item(0, 4).setText("%.3f" % pt.y if not math.isnan(pt.y) else "")
			self.gridSurvey.item(0, 5).setText("%.3f" % pt.z if not math.isnan(pt.z) else "")

	def setDirPoint(self, index, pt):
		self.measure[index].dirpoint = pt
		if not pt.name:
			self.measure[index].dirpoint.delete_coords()
			self.gridSurvey.item(1, 1).setText("")
			self.gridSurvey.item(1, 3).setText("")
			self.gridSurvey.item(1, 4).setText("")
			self.gridSurvey.item(1, 5).setText("")
		else:
			self.gridSurvey.item(1, 1).setText(pt.name)
			self.gridSurvey.item(1, 3).setText("%.3f" % pt.x)
			self.gridSurvey.item(1, 4).setText("%.3f" % pt.y)
			self.gridSurvey.item(1, 5).setText("%.3f" % pt.z)

	def getParams(self, index):
		self.project_stack.setCurrentIndex(index)
		inv = self.project_stack.currentWidget().reverse_chbox.isChecked()
		step = self.project_stack.currentWidget().bias_step_eline.text()
		tol = self.project_stack.currentWidget().bias_tol_eline.text()
		maxdist = self.project_stack.currentWidget().bias_max_dist_eline.text()
		uoff = self.project_stack.currentWidget().bias_up_eline.text()
		doff = self.project_stack.currentWidget().bias_down_eline.text()
		width = self.project_stack.currentWidget().bias_width_eline.text()

		startx, starty, startz = self.project_stack.currentWidget().init_east_eline.text(), self.project_stack.currentWidget().init_north_eline.text(), self.project_stack.currentWidget().init_z_eline.text()
		endx, endy, endz = self.project_stack.currentWidget().end_east_eline.text(), self.project_stack.currentWidget().end_north_eline.text(), self.project_stack.currentWidget().end_z_eline.text()

		if all(map(lambda x: is_number(x), [startx, starty, startz])):		startpoint = geom.Point(float(startx), float(starty), float(startz))
		else:																startpoint = None
		if all(map(lambda x: is_number(x), [endx, endy, endz])):			endpoint = geom.Point(float(endx), float(endy), float(endz))
		else:																endpoint = None

		self._measure[index].startpoint = startpoint
		self._measure[index].endpoint = endpoint

		self._measure[index].is_reverse = inv
		self._measure[index].step = float(step) if step else None
		self._measure[index].tolerance = int(tol) if tol else None
		self._measure[index].maxdist = float(maxdist) if maxdist else None
		self._measure[index].uoffset = float(uoff) if uoff else None
		self._measure[index].doffset = float(doff) if doff else None
		self._measure[index].width = float(width) if width else None

	def setParams(self, index):
		startpoint = self._measure[index].startpoint
		endpoint = self._measure[index].endpoint

		startx, starty, startz = "%.3f" % startpoint.x if startpoint else "", "%.3f" % startpoint.y if startpoint else "", "%.3f" % startpoint.z if startpoint else ""
		endx, endy, endz = "%.3f" % endpoint.x if endpoint else "", "%.3f" % endpoint.y if endpoint else "", "%.3f" % endpoint.z if endpoint else ""

		if startpoint and endpoint:	self.project_group_box.setChecked(True)
		else:						self.project_group_box.setChecked(False)

		inv = self._measure[index].is_reverse
		step = "%.3f" % self._measure[index].step if self._measure[index].step is not None else ""
		tol = "%d" % self._measure[index].tolerance if self._measure[index].tolerance is not None else ""
		maxdist = "%.3f" % self._measure[index].maxdist if self._measure[index].maxdist is not None else ""
		uoff = "%.3f" % self._measure[index].uoffset if self._measure[index].uoffset is not None else ""
		doff = "%.3f" % self._measure[index].doffset if self._measure[index].doffset is not None else ""
		width = "%.3f" % self._measure[index].width if self._measure[index].width is not None else ""

		self.project_stack.setCurrentIndex(index)
		self.project_stack.currentWidget().init_east_eline.setText(startx)
		self.project_stack.currentWidget().init_north_eline.setText(starty)
		self.project_stack.currentWidget().init_z_eline.setText(startz)
		self.project_stack.currentWidget().end_east_eline.setText(endx)
		self.project_stack.currentWidget().end_north_eline.setText(endy)
		self.project_stack.currentWidget().end_z_eline.setText(endz)

		self.project_stack.currentWidget().reverse_chbox.setChecked(inv)
		self.project_stack.currentWidget().bias_step_eline.setText(step)
		self.project_stack.currentWidget().bias_tol_eline.setText(tol)
		self.project_stack.currentWidget().bias_max_dist_eline.setText(maxdist)
		self.project_stack.currentWidget().bias_up_eline.setText(uoff)
		self.project_stack.currentWidget().bias_down_eline.setText(doff)
		self.project_stack.currentWidget().bias_width_eline.setText(width)

	def doCalculation(self):
		self.getParams(self.project_stack.currentIndex())
		plumbs = self._measure[self.project_stack.currentIndex()].calc_plumbs()
		if not plumbs:
			self.gridSurvey.item(0, 2).setText("")
			self.gridSurvey.item(1, 2).setText("")
		else:
			svyPlumb, dirPlumb = plumbs

			svyDist = (svyPlumb - self._measure[0].standpoint).norm
			dirDist = (dirPlumb - self._measure[0].dirpoint).norm

			self.gridSurvey.item(0, 2).setText("%.3f" % svyDist if not math.isnan(svyDist) else "")
			self.gridSurvey.item(1, 2).setText("%.3f" % dirDist if not math.isnan(dirDist) else "")

	def updateOffsetMeasureNames(self):
		self.comboBoxOffsetMeasureName.clear()

		cmd = """
			SELECT MEASURENAME
			FROM [{0}].[dbo].[MP_TRAVERSALDRIVES]
			WHERE MEASURE_ID = {1}
		""".format(self._cnxn.dbsvy, self._measure.name)
		names = [name[0] for name in self._cnxn.exec_query(cmd).get_rows()]

		self.comboBoxOffsetMeasureName.addItems(names)