from PyQt4 import QtGui, QtCore

from utils.qtutil import iterWidgetParents
from directions.svydbapi import SvyPoint
from gui.qt.widgets.SurveyGrid import SvyDirectionGrid, SvyControlGrid, SvyGridStackWidget
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GridNavigationBar(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(GridNavigationBar, self).__init__(*args, **kw)

		self.__createWidgets()
		self.__gridWidgets()

	def __createWidgets(self):
		self.mainLayout = QtGui.QHBoxLayout()
		self.mainLayout.setMargin(0)

		self.checkBoxIsDirection = QtGui.QCheckBox(self)
		self.checkBoxIsDirection.setText("Точка направления")
		# self.checkBoxIsDirection.setDisabled(True)

		self.labelSeqNo = QtGui.QLabel(self)

		self.buttonDelete = QtGui.QPushButton(self)
		self.buttonDelete.setText("Удалить")
		self.buttonDelete.setMaximumSize(QtCore.QSize(100, 25))

		self.buttonPrev = QtGui.QPushButton(self)
		self.buttonPrev.setMaximumSize(QtCore.QSize(100, 25))
		self.buttonPrev.setText("Предыдущая")

		self.buttonNext = QtGui.QPushButton(self)
		self.buttonNext.setMaximumSize(QtCore.QSize(100, 25))
		self.buttonNext.setText("Следующая")
	def __gridWidgets(self):
		self.setLayout(self.mainLayout)
		self.mainLayout.addWidget(self.checkBoxIsDirection)
		self.mainLayout.addWidget(self.labelSeqNo)
		self.mainLayout.addWidget(self.buttonDelete)
		self.mainLayout.addWidget(self.buttonPrev)
		self.mainLayout.addWidget(self.buttonNext)

class ControlFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(ControlFrame, self).__init__(*args, **kw)

		self.__createWidgets()
		self.__gridWidgets()

		self.addGrid()

	def __createWidgets(self):
		self.mainLayout = QtGui.QVBoxLayout()
		self.gridWidget = SvyGridStackWidget(self)

	def __gridWidgets(self):
		self.setLayout(self.mainLayout)
		self.mainLayout.addWidget(self.gridWidget)

	def addGrid(self):
		self.gridWidget.addGrid(SvyControlGrid(self.gridWidget))
	def clear(self):
		self.gridWidget.deleteGrid(0)
		self.addGrid()

class CalculationFrame(ControlFrame):
	def __init__(self, *args, **kw):
		super(CalculationFrame, self).__init__(*args, **kw)

		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createWidgets(self):
		self.navibar = GridNavigationBar(self)
	def __createBindings(self):
		self.navibar.buttonPrev.clicked.connect(self.gotoPrevGrid)
		self.navibar.buttonNext.clicked.connect(self.gotoNextGrid)
		self.navibar.buttonDelete.clicked.connect(self.deleteCurrentGrid)
		self.navibar.checkBoxIsDirection.clicked.connect(self.isDirectionClicked)
	def __gridWidgets(self):
		self.mainLayout.addWidget(self.navibar)
	def addGrid(self):
		self.gridWidget.addGrid(SvyDirectionGrid(self.gridWidget))
	def gotoPrevGrid(self):
		index = self.gridWidget.currentIndex()
		if index > 0:
			self.gridWidget.setCurrentIndex(index-1)
		self.setActions()
	def gotoNextGrid(self):
		currindex = self.gridWidget.currentIndex()

		if currindex < self.gridWidget.count()-1:
			self.gridWidget.setCurrentIndex(currindex+1)
		elif self.gridWidget.grids[-1].hasResult:
			self.addGrid()
			self.gridWidget.setCurrentIndex(currindex+1)
		self.setActions()

	def deleteCurrentGrid(self):
		currindex = self.gridWidget.currentIndex()
		if currindex == 0:
			self.gridWidget.deleteGrid(currindex+1)
			self.gridWidget.grids[0].clear()
		else:
			self.gridWidget.deleteGrid(currindex)
		self.setActions()
	def isDirectionClicked(self, value):
		gw = self.gridWidget
		currindex = self.gridWidget.currentIndex()
		gw.grids[currindex].measure.dirpoint.is_direction = value
		#
		self.setActions()

	def setActions(self):
		checkbox = self.navibar.checkBoxIsDirection 
		gw = self.gridWidget
		currindex = gw.currentIndex()

		#
		if currindex == gw.count()-1:# and gw.count() > 1:
			checkbox.setDisabled(False)
			checkbox.setChecked(gw.grids[-1].measure.dirpoint.is_direction)
		else:
			checkbox.setDisabled(True)

		#
		if currindex == 0 or currindex == gw.count()-1:
			if currindex == 0:		self.navibar.buttonPrev.setDisabled(True)
			else:					self.navibar.buttonPrev.setDisabled(False)

			if ((currindex == gw.count()-1 and not gw.grids[-1].hasResult)) or (checkbox.isEnabled() and checkbox.isChecked()):
				self.navibar.buttonNext.setDisabled(True)
			else:
				self.navibar.buttonNext.setDisabled(False)
		else:
			self.navibar.buttonPrev.setDisabled(False)
			self.navibar.buttonNext.setDisabled(False)

		#
		self.navibar.labelSeqNo.setText("Точка %d/%d" % (currindex+1, gw.count()))

		# задание точки стояния и направления для расчета домеров
		window = iterWidgetParents(self)[-2]
		frameProject = window.frameProject
		for i_project in range(len(frameProject.measure)):
			if gw.grids[-1].measure.dirpoint.is_direction and gw.hasResult:
				frameProject.setStandPoint(i_project, gw.grids[-1].measure.standpoint)
				frameProject.setDirPoint(i_project, gw.grids[-1].measure.dirpoint)
			else:
				frameProject.setStandPoint(i_project, SvyPoint(window.main_app.srvcnxn))
				frameProject.setDirPoint(i_project, SvyPoint(window.main_app.srvcnxn))
		frameProject.doCalculation()