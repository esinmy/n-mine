from PyQt4 import QtGui, QtCore
from datetime import datetime

from gui.qt.dialogs.SelectFromTableDialog import SelectFromTableDialog
from gui.qt.widgets.TableDataView import CustomDataModel

from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SurveyInformationFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(SurveyInformationFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self._mainwindow = self.parent().parent()
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxInfo = QtGui.QGroupBox(self)
		self.groupBoxInfo.setTitle("Информация о съемке")

		self.layoutWidget = QtGui.QWidget(self.groupBoxInfo)
		self.layoutInfo = QtGui.QGridLayout(self.layoutWidget)

		self.labelDate = QtGui.QLabel(self.groupBoxInfo)
		self.labelDate.setText("Дата")
		self.labelDate.setMinimumSize(QtCore.QSize(136, 0))
		self.labelDate.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditDate = QtGui.QDateEdit(self.groupBoxInfo)
		self.lineEditDate.setMinimumSize(QtCore.QSize(250, 0))
		self.lineEditDate.setMaximumSize(QtCore.QSize(250, 20))
		self.lineEditDate.setCalendarPopup(True)
		self.lineEditDate.calendarWidget().setFirstDayOfWeek(QtCore.Qt.Monday)
		now = datetime.now()
		self.lineEditDate.setDate(QtCore.QDate(now.year, now.month, now.day))

		self.labelRegister = QtGui.QLabel(self.groupBoxInfo)
		self.labelRegister.setText("Журнал")
		self.labelRegister.setMinimumSize(QtCore.QSize(136, 0))
		self.labelRegister.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditRegister = QtGui.QLineEdit(self.groupBoxInfo)
		self.lineEditRegister.setMaximumSize(QtCore.QSize(250, 20))
		self.lineEditRegister.setMinimumSize(QtCore.QSize(250, 0))
		self.lineEditRegister.setReadOnly(True)


		self.labelSurveyorName = QtGui.QLabel(self.groupBoxInfo)
		self.labelSurveyorName.setText("Маркшейдер")
		self.labelSurveyorName.setMinimumSize(QtCore.QSize(136, 0))
		self.labelSurveyorName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditSurveyorName = QtGui.QLineEdit(self.groupBoxInfo)
		self.lineEditSurveyorName.setMaximumSize(QtCore.QSize(250, 20))
		self.lineEditSurveyorName.setMinimumSize(QtCore.QSize(250, 0))
		self.pushButtonAddName = QtGui.QToolButton(self)
		self.pushButtonAddName.setMinimumSize(QtCore.QSize(20, 20))
		self.pushButtonAddName.setMaximumSize(QtCore.QSize(20, 20))
		self.pushButtonAddName.setIconSize(QtCore.QSize(13, 13))
		self.pushButtonAddName.setToolTip("Добавить")
		self.pushButtonAddName.setIcon(QtGui.QIcon("/".join([IMG_ICONS_PATH, "add.png"])))

		self.labelMineName = QtGui.QLabel(self.groupBoxInfo)
		self.labelMineName.setText("Выработка")
		self.labelMineName.setMinimumSize(QtCore.QSize(136, 0))
		self.labelMineName.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelMineName.setMinimumWidth(80)
		self.lineEditMineName = QtGui.QLineEdit(self.groupBoxInfo)
		self.lineEditMineName.setMaximumSize(QtCore.QSize(250, 20))
		self.lineEditMineName.setMinimumSize(QtCore.QSize(250, 0))
		self.lineEditMineName.setReadOnly(True)
		self.pushButtonAddMineName = QtGui.QToolButton(self)
		self.pushButtonAddMineName.setMinimumSize(QtCore.QSize(20, 20))
		self.pushButtonAddMineName.setMaximumSize(QtCore.QSize(20, 20))
		self.pushButtonAddMineName.setIconSize(QtCore.QSize(13, 13))
		self.pushButtonAddMineName.setToolTip("Добавить")
		self.pushButtonAddMineName.setIcon(QtGui.QIcon("/".join([IMG_ICONS_PATH, "add.png"])))

		self.labelLayer = QtGui.QLabel(self.groupBoxInfo)
		self.labelLayer.setText("Залежь")
		self.labelLayer.setMinimumSize(QtCore.QSize(136, 0))
		self.labelLayer.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelLayer.setMinimumWidth(80)
		self.lineEditLayer = QtGui.QLineEdit(self.groupBoxInfo)
		self.lineEditLayer.setMaximumSize(QtCore.QSize(250, 20))
		self.lineEditLayer.setMinimumSize(QtCore.QSize(250, 0))
		self.lineEditLayer.setEnabled(False)

		self.labelUnit = QtGui.QLabel(self.groupBoxInfo)
		geoUnitDisplayName = self._mainwindow.main_app.settings.getValue(ID_GEO_UDSPNAME)
		self.labelUnit.setText(geoUnitDisplayName)
		self.labelUnit.setMinimumSize(QtCore.QSize(136, 0))
		self.labelUnit.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
		self.labelUnit.setMinimumWidth(80)
		self.lineEditUnit = QtGui.QLineEdit(self.groupBoxInfo)
		self.lineEditUnit.setMaximumSize(QtCore.QSize(250, 20))
		self.lineEditUnit.setMinimumSize(QtCore.QSize(250, 0))
		self.lineEditUnit.setEnabled(False)


		self.labelEquipment = QtGui.QLabel(self.groupBoxInfo)
		self.labelEquipment.setText("Инструмент")
		self.labelEquipment.setMinimumSize(QtCore.QSize(136, 0))
		self.labelEquipment.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditEquipment = QtGui.QLineEdit(self.groupBoxInfo)
		self.lineEditEquipment.setMinimumSize(QtCore.QSize(250, 0))
		self.lineEditEquipment.setMaximumSize(QtCore.QSize(250, 20))
		self.pushButtonAddEquipment = QtGui.QToolButton(self)
		self.pushButtonAddEquipment.setMinimumSize(QtCore.QSize(20, 20))
		self.pushButtonAddEquipment.setMaximumSize(QtCore.QSize(20, 20))
		self.pushButtonAddEquipment.setIconSize(QtCore.QSize(13, 13))
		self.pushButtonAddEquipment.setToolTip("Добавить")
		self.pushButtonAddEquipment.setIcon(QtGui.QIcon("/".join([IMG_ICONS_PATH, "add.png"])))

	def __createBindings(self):
		self.lineEditRegister.mouseDoubleClickEvent = lambda e: self._selectFromLookup(e, self.lineEditRegister)
		self.lineEditSurveyorName.mouseDoubleClickEvent = lambda e: self._selectFromLookup(e, self.lineEditSurveyorName)
		self.lineEditMineName.mouseDoubleClickEvent = lambda e: self._selectFromLookup(e, self.lineEditMineName)
		self.lineEditEquipment.mouseDoubleClickEvent = lambda e: self._selectFromLookup(e, self.lineEditEquipment)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.groupBoxInfo)
		self.layoutWidget.setGeometry(10, 10, 450, 200)

		self.layoutInfo.addWidget(self.labelDate, 0, 0, 1, 1)
		self.layoutInfo.addWidget(self.lineEditDate, 0, 1, 1, 1)
		self.layoutInfo.addWidget(self.labelRegister, 1, 0, 1, 1)
		self.layoutInfo.addWidget(self.lineEditRegister, 1, 1, 1, 1)
		self.layoutInfo.addWidget(self.labelSurveyorName, 2, 0, 1, 1)
		self.layoutInfo.addWidget(self.lineEditSurveyorName, 2, 1, 1, 1)
		self.layoutInfo.addWidget(self.pushButtonAddName, 2, 2, 1, 1)
		self.layoutInfo.addWidget(self.labelMineName, 3, 0, 1, 1)
		self.layoutInfo.addWidget(self.lineEditMineName, 3, 1, 1, 1)
		self.layoutInfo.addWidget(self.pushButtonAddMineName, 3, 2, 1, 1)
		self.layoutInfo.addWidget(self.labelLayer, 4, 0, 1, 1)
		self.layoutInfo.addWidget(self.lineEditLayer, 4, 1, 1, 1)
		self.layoutInfo.addWidget(self.labelUnit, 5, 0, 1, 1)
		self.layoutInfo.addWidget(self.lineEditUnit, 5, 1, 1, 1)
		self.layoutInfo.addWidget(self.labelEquipment, 6, 0, 1, 1)
		self.layoutInfo.addWidget(self.lineEditEquipment, 6, 1, 1, 1)
		self.layoutInfo.addWidget(self.pushButtonAddEquipment, 6, 2, 1, 1)

	def getParams(self):
		measure = self._mainwindow.traversal_measure
		measure.register_id = self._getRegisterId(self.lineEditRegister.text())
		measure.surveyor_id = self._getSurveyorId(self.lineEditSurveyorName.text())
		measure.equipment_id = self._getEquipmentId(self.lineEditEquipment.text())
		measure.mine_id = self._getMineId(self.lineEditMineName.text())
		measure.unit_id = self._getUnitId(self.lineEditLayer.text(), self.lineEditUnit.text())
		measure.timestamp = datetime.strptime(self.lineEditDate.text(), "%d.%m.%Y")


	def setParams(self):
		measure = self._mainwindow.traversal_measure
		if measure:
			#self.lineEditRegister.setText(measure.register_name if measure.register_name else self._mainwindow.getActualRegister())
			self.lineEditRegister.setText(measure.register_name if measure.register_name else "")
			self.lineEditSurveyorName.setText(measure.surveyor_name if measure.surveyor_name else "")
			self.lineEditEquipment.setText(measure.equipment_name if measure.equipment_name else "")
			self.lineEditMineName.setText(measure.mine_name if measure.mine_name else "")
			self.lineEditDate.setDate(measure.timestamp if measure.timestamp else datetime.now())
			self.lineEditUnit.setText(measure.unit_name if measure.unit_name else "")
			self.lineEditLayer.setText(measure.layer_name if measure.layer_name else "")

	def _getRegisterId(self, name):
		id_ = None
		for sid, value, date in self._mainwindow.data["REGISTERS"][0]:
			if value.upper() == name.upper():
				id_ = sid
				break
		return  id_ if not id_ is None else self._mainwindow.traversal_measure.register_id

	def _getSurveyorId(self, name):
		id_ = None
		for sid, value in self._mainwindow.data["SURVEYORS"][0]:
			if value.upper() == name.upper():
				id_ = sid
				break
		return id_
	def _getEquipmentId(self, name):
		id_ = None
		for eid, value, model, desc in self._mainwindow.data["EQUIPMENT"][0]:
			if value.upper() == name.upper():
				id_ = eid
				break
		return id_
	def _getMineId(self, name):
		id_ = None
		for mid, uid, lvalue, uvalue, mvalue in self._mainwindow.data["UGMINES"][0]:
			if mvalue.upper() == name.upper():
				id_ = mid
				break
		return id_
	def _getUnitId(self, layer, unit):
		id_ = None
		for mid, uid, lvalue, uvalue, mvalue in self._mainwindow.data["UGMINES"][0]:
			lvalue =  lvalue if not lvalue is None else ""
			uvalue =  uvalue if not uvalue is None else ""
			mvalue =  mvalue if not mvalue is None else ""
			if lvalue.upper() == layer.upper() and uvalue.upper() == unit.upper():
				id_ = uid
				break
		return id_
				   
	def _selectFromLookup(self, event, widget):
		if not widget.isEnabled():
			return

		keys = {self.lineEditSurveyorName: "SURVEYORS", self.lineEditMineName: "UGMINES", self.lineEditEquipment: "EQUIPMENT", self.lineEditRegister: "REGISTERS"}
		
		dialog = SelectFromTableDialog(self)
		dialog.setWindowTitle("Выберите значение")
		model = CustomDataModel(dialog.view, *self._mainwindow.data[keys[widget]])
		dialog.view.setModel(model)
		dialog.view.hideColumn(0)
		if widget == self.lineEditMineName:	 dialog.view.hideColumn(1)
		dialog.view.setColumnWidth(1, 250)
		dialog.resize(QtCore.QSize(600, 500))
		dialog.exec()
		#
		ret = dialog.getResult()
		if not ret:
			return
		if widget != self.lineEditMineName:
			widget.setText(ret[0][1])

		if widget == self.lineEditRegister:	self._mainwindow.traversal_measure.register_id = ret[0][0]
		if widget == self.lineEditSurveyorName:	self._mainwindow.traversal_measure.surveyor_id = ret[0][0]
		if widget == self.lineEditMineName:		
			self._mainwindow.traversal_measure.mind_id = ret[0][0]
			self._mainwindow.traversal_measure.unit_id = ret[0][1]
			self.lineEditMineName.setText(ret[0][4])
			self.lineEditLayer.setText(ret[0][2])
			self.lineEditUnit.setText(ret[0][3])


		if widget == self.lineEditEquipment:	self._mainwindow.traversal_measure.equipment_id = ret[0][0]

		return None
