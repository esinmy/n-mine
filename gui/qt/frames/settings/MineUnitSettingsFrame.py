from PyQt4 import QtCore, QtGui

from gui.qt.widgets.GridEditor import *

from utils.qtutil import *
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()


class MineUnitSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(MineUnitSettingsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.__setAccessibleNames()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()

		self.groupBoxParentDirectory = QtGui.QGroupBox(self)
		self.groupBoxParentDirectory.setTitle("Директория с выемочными единицами")
		self.groupBoxParentDirectory.setMinimumHeight(55)

		self.layoutParentDirectory = QtGui.QFormLayout()
		self.layoutParentDirectory.setSpacing(5)
		self.labelParentDirectory = QtGui.QLabel(self.groupBoxParentDirectory)
		self.labelParentDirectory.setText("Директория")
		self.labelParentDirectory.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelParentDirectory.setMinimumWidth(100)
		self.lineEditParentDirectory = QtGui.QLineEdit(self.groupBoxParentDirectory)
		
		self.groupBoxChildDirectories = QtGui.QGroupBox(self)
		self.groupBoxChildDirectories.setTitle("Вложенные директории")
		self.layoutChildDirectories = QtGui.QHBoxLayout()
		self.layoutChildDirectories.setSpacing(0)

		self.gridEditorUnitSubdirs = GridEditorFolders(self.groupBoxChildDirectories)
		self.gridEditorUnitSubdirs.toolBar.labelHint.setText("Введите имена директорий, создаваемых в папке выемочной единицы")

	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		
		self.layoutMain.addWidget(self.groupBoxParentDirectory)
		self.groupBoxParentDirectory.setLayout(self.layoutParentDirectory)
		self.layoutParentDirectory.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelParentDirectory)
		self.layoutParentDirectory.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditParentDirectory)
		
		self.layoutMain.addWidget(self.groupBoxChildDirectories)
		self.groupBoxChildDirectories.setLayout(self.layoutChildDirectories)
		self.layoutChildDirectories.addWidget(self.gridEditorUnitSubdirs)

	def __setAccessibleNames(self):
		self.lineEditParentDirectory.setAccessibleName(ID_MUNIT_MAINDIR)
		self.gridEditorUnitSubdirs.setAccessibleName(ID_MUSUBDIR)	

