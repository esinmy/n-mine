from PyQt4 import QtCore, QtGui

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.GridEditor import *

from utils.validation import is_blank
from utils.constants import *
from utils.qtutil import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SimSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(SimSettingsFrame, self).__init__(*args, **kw)

		self.__create_variables()
		self.__create_widgets()
		self.__set_accessible_names()
		self.__create_bindings()
		
		self.__grid_widgets()
		
		self.__retranslate_ui()

	def __create_variables(self):
		self._categoryList = []

	def __create_widgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.groupBoxCategory = QtGui.QGroupBox(self)

		self.layoutCategory = QtGui.QVBoxLayout()
		self.gridEditorCategory = GridEditor(self.groupBoxCategory, ["Атрибут", "Значение"])
		self.gridEditorCategory.getValue = self.categoryList
		self.gridEditorCategory.setValue = self.setCategoryList
		self._build_category_grid()

	def __create_bindings(self):
		self.gridEditorCategory.dataGrid.itemChanged.connect(self.updateCategory)

	def __grid_widgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxCategory)
		self.groupBoxCategory.setLayout(self.layoutCategory)
		self.layoutCategory.addWidget(self.gridEditorCategory)

	def _build_category_grid(self):
		self.gridEditorCategory.dataGrid.setColumnWidth(0, 250)
		self.gridEditorCategory.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.gridEditorCategory.dataGrid.horizontalHeader().setHighlightSections(False)
		self.gridEditorCategory.dataGrid.verticalHeader().setVisible(False)

		self.categories = ['Использование ШАС', 'Время на закладочные работы, ч', 'Наличие крепи', 'Тип крепи',
					  'Среднее количество шпуров, шт', 'Среднее количество скважин в веере, шт',
					  'Средняя длина взрывной скважины, м', 'Количество вееров в стадии взрыва, шт', 'ЛНС, м']

		for irow, cat in enumerate(self.categories):
			self.gridEditorCategory.dataGrid.appendRow()
			item = DataGridItem(cat)
			item.setLocked(True)
			self.gridEditorCategory.dataGrid.setItem(irow, 0, item)
		for cat in self.categories:
			self._categoryList.append([cat, ""])
		#
		self.gridEditorCategory.toolBar.pushButtonRefresh.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonAppend.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonInsert.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonMoveUp.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonMoveDown.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonDelete.setVisible(False)
		#
		self.gridEditorCategory.dataGrid.setAppendByKeyboard(False)

	def __set_accessible_names(self):
		self.gridEditorCategory.setAccessibleName(ID_SIM_SET)

	def __retranslate_ui(self):
		self.groupBoxCategory.setTitle("Значения по умолчанию")

	def updateCategory(self):
		catGrid = self.gridEditorCategory.dataGrid

		nRows = catGrid.rowCount()
		nCols = catGrid.columnCount()
		for iRow in range(nRows):
			for iCol in range(nCols):
				item = catGrid.item(iRow, iCol)
				# На случай введения ограничений на заполнение полей - оставлю этот код
				# if iCol == 1:
				# 	if catGrid.item(iRow, iCol - 1).text() == self.categories[0]:
				# 		if item.text().lower() not in ['', 'да', 'нет']:
				# 			qtmsgbox.show_error(self, "Ошибка", "Введите Да или Нет")
				# 			catGrid.setCurrentCell(iRow, iCol)
				# 			item.setText('')
				# 			return
				if item:
					self._categoryList[iRow][iCol] = item.text()

	def insertCode(self, iRow, values = ""):
		catGrid = self.gridEditorCategory.dataGrid
		currIndex = catGrid.currentIndex()
		itemCat = catGrid.item(currIndex.row(), 1)
		currCat = itemCat.text() if itemCat else None
		if not itemCat or not currCat or is_blank(currCat):
			qtmsgbox.show_error(self, "Ошибка", "Введите код категории")
			catGrid.setFocus()
			return

	def categoryList(self):
		return self._categoryList

	def setCategoryList(self, d):
		catGrid = self.gridEditorCategory.dataGrid
		catGrid.itemChanged.disconnect()
		catGrid.removeAll()

		self._categoryList = d
		# Заполнение поля "Значение" в таблице
		for i, category in enumerate(d):
			catGrid.item(i, 1).setText(category[1])

		catGrid.itemChanged.connect(self.updateCategory)
		if self._categoryList:
			catGrid.setCurrentCell(0, 1)
	