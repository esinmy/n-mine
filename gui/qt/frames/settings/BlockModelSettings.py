from PyQt4 import QtCore, QtGui


from utils.qtutil import *
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class XYZFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(XYZFrame, self).__init__(*args, **kw)

		self.__create_widgets()
		self.__set_accessible_names()
		self.__grid_widgets()

		self.__retranslate_ui()
	def __create_widgets(self):
		self.formLayout = QtGui.QFormLayout()
		
		self.labelX = QtGui.QLabel(self)
		self.labelX.setMinimumWidth(30)
		self.labelX.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditX = QtGui.QLineEdit(self)
		self.lineEditX.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditX.setMaximumWidth(50)

		self.labelY = QtGui.QLabel(self)
		self.labelY.setMinimumWidth(30)
		self.labelY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditY = QtGui.QLineEdit(self)
		self.lineEditY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditY.setMaximumWidth(50)

		self.labelZ = QtGui.QLabel(self)
		self.labelZ.setMinimumWidth(30)
		self.labelZ.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditZ = QtGui.QLineEdit(self)
		self.lineEditZ.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditZ.setMaximumWidth(50)

	def __grid_widgets(self):
		self.setLayout(self.formLayout)

		labels = [self.labelX, self.labelY, self.labelZ]
		fields = [self.lineEditX, self.lineEditY, self.lineEditZ]

		for i, (label, field) in enumerate(zip(labels, fields)):
			self.formLayout.setWidget(i, QtGui.QFormLayout.LabelRole, label)
			self.formLayout.setWidget(i, QtGui.QFormLayout.FieldRole, field)

	def __retranslate_ui(self):
		self.labelX.setText("X")
		self.labelY.setText("Y")
		self.labelZ.setText("Z")

	def __set_accessible_names(self):
		self.lineEditX.setAccessibleName(ID_X)
		self.lineEditY.setAccessibleName(ID_Y)
		self.lineEditZ.setAccessibleName(ID_Z)

class BlockModelSettings(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(BlockModelSettings, self).__init__(*args, **kw)

		self.__create_widgets()
		self.__grid_widgets()

		self.__set_accessible_names()

	def __create_widgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(3)

		self.groupBoxBmParams = QtGui.QGroupBox(self)
		self.groupBoxBmParams.setTitle("Параметры блочной модели")
		self.layoutParams = QtGui.QHBoxLayout()
		self.layoutParams.setSpacing(3)

		self.groupBoxParentBlocks = QtGui.QGroupBox(self.groupBoxBmParams)
		self.groupBoxParentBlocks.setTitle("Материнские блоки")
		self.layoutParentBlocks = QtGui.QHBoxLayout()
		self.layoutParentBlocks.setSpacing(3)
		

		self.groupBoxBlockSizes = QtGui.QGroupBox(self.groupBoxParentBlocks)
		self.groupBoxBlockSizes.setTitle("Размеры")
		self.layoutBlockSizes = QtGui.QHBoxLayout()
		self.layoutBlockSizes.setSpacing(3)
		self.xyzFrameParentBlocks = XYZFrame(self.groupBoxBlockSizes)

		
		self.groupBoxSubblockNumber = QtGui.QGroupBox(self.groupBoxBmParams)
		self.groupBoxSubblockNumber.setTitle("Количество субблоков модели")
		self.layoutSubblocks = QtGui.QHBoxLayout()
		self.layoutSubblocks.setSpacing(3)

		self.groupBoxGeoSubblocks = QtGui.QGroupBox(self.groupBoxSubblockNumber)
		self.groupBoxGeoSubblocks.setTitle("Геология")
		self.layoutGeoSubblocks = QtGui.QHBoxLayout()
		self.layoutGeoSubblocks.setSpacing(3)
		self.xyzFrameGeoSubblocks = XYZFrame(self.groupBoxGeoSubblocks)
		
		self.groupBoxSvySubblocks = QtGui.QGroupBox(self.groupBoxSubblockNumber)
		self.groupBoxSvySubblocks.setTitle("Маркшейдерия")
		self.layoutSvySubblocks = QtGui.QHBoxLayout()
		self.layoutSvySubblocks.setSpacing(3)
		self.xyzFrameSvySubblocks = XYZFrame(self.groupBoxSvySubblocks)
		
		self.groupBoxDsnSubblocks = QtGui.QGroupBox(self.groupBoxSubblockNumber)
		self.groupBoxDsnSubblocks.setTitle("Проектирование")
		self.layoutDsnSubblocks = QtGui.QHBoxLayout()
		self.layoutDsnSubblocks.setSpacing(3)
		self.xyzFrameDsnSubblocks = XYZFrame(self.groupBoxDsnSubblocks)

		#
		self.groupBoxAutoBounds = QtGui.QGroupBox(self)
		self.groupBoxAutoBounds.setTitle("Автоматическое определение границ")
		self.formLayoutAutoBounds = QtGui.QFormLayout()

		self.lineEditOffsetZ = QtGui.QLineEdit(self.groupBoxAutoBounds)
		self.lineEditOffsetZ.setMaximumWidth(100)
		self.lineEditOffsetZ.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.labelOffsetZ = QtGui.QLabel(self.groupBoxAutoBounds)
		self.labelOffsetZ.setText("Отступ по Z")
		self.labelOffsetZ.setMinimumWidth(100)
		self.labelOffsetZ.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		
		self.checkBoxSmartDetection = QtGui.QCheckBox(self.groupBoxAutoBounds)
		self.checkBoxSmartDetection.setText("Интеллектуальное определение границ")

	def __grid_widgets(self):
		self.setLayout(self.layoutMain)
		
		self.layoutMain.addWidget(self.groupBoxBmParams)
		self.groupBoxBmParams.setLayout(self.layoutParams)

		self.layoutParams.addWidget(self.groupBoxParentBlocks)
		self.groupBoxParentBlocks.setLayout(self.layoutParentBlocks)

		self.layoutParentBlocks.addWidget(self.groupBoxBlockSizes)
		self.groupBoxBlockSizes.setLayout(self.layoutBlockSizes)
		self.layoutBlockSizes.addWidget(self.xyzFrameParentBlocks)

		
		self.layoutParams.addWidget(self.groupBoxSubblockNumber)
		self.groupBoxSubblockNumber.setLayout(self.layoutSubblocks)
		
		self.layoutSubblocks.addWidget(self.groupBoxGeoSubblocks)
		self.groupBoxGeoSubblocks.setLayout(self.layoutGeoSubblocks)
		self.layoutGeoSubblocks.addWidget(self.xyzFrameGeoSubblocks)

		self.layoutSubblocks.addWidget(self.groupBoxSvySubblocks)
		self.groupBoxSvySubblocks.setLayout(self.layoutSvySubblocks)
		self.layoutSvySubblocks.addWidget(self.xyzFrameSvySubblocks)

		self.layoutSubblocks.addWidget(self.groupBoxDsnSubblocks)
		self.groupBoxDsnSubblocks.setLayout(self.layoutDsnSubblocks)
		self.layoutDsnSubblocks.addWidget(self.xyzFrameDsnSubblocks)

		# Автоматическое определение границ
		self.layoutMain.addWidget(self.groupBoxAutoBounds)
		self.groupBoxAutoBounds.setLayout(self.formLayoutAutoBounds)
		self.formLayoutAutoBounds.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelOffsetZ)
		self.formLayoutAutoBounds.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditOffsetZ)
		self.formLayoutAutoBounds.setWidget(1, QtGui.QFormLayout.FieldRole, self.checkBoxSmartDetection)

	def __set_accessible_names(self):
		self.xyzFrameParentBlocks.lineEditX.setAccessibleName(ID_XSIZE)
		self.xyzFrameParentBlocks.lineEditY.setAccessibleName(ID_YSIZE)
		self.xyzFrameParentBlocks.lineEditZ.setAccessibleName(ID_ZSIZE)
		self.xyzFrameGeoSubblocks.lineEditX.setAccessibleName(ID_SUBXGEO)
		self.xyzFrameGeoSubblocks.lineEditY.setAccessibleName(ID_SUBYGEO)
		self.xyzFrameGeoSubblocks.lineEditZ.setAccessibleName(ID_SUBZGEO)
		self.xyzFrameSvySubblocks.lineEditX.setAccessibleName(ID_SUBXSVY)
		self.xyzFrameSvySubblocks.lineEditY.setAccessibleName(ID_SUBYSVY)
		self.xyzFrameSvySubblocks.lineEditZ.setAccessibleName(ID_SUBZSVY)
		self.xyzFrameDsnSubblocks.lineEditX.setAccessibleName(ID_SUBXDSN)
		self.xyzFrameDsnSubblocks.lineEditY.setAccessibleName(ID_SUBYDSN)
		self.xyzFrameDsnSubblocks.lineEditZ.setAccessibleName(ID_SUBZDSN)
		self.lineEditOffsetZ.setAccessibleName(ID_ZOFFSET)
		self.checkBoxSmartDetection.setAccessibleName(ID_AUTOZ)


