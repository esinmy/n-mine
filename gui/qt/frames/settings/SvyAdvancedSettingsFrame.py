from PyQt4 import QtCore, QtGui

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.GridEditor import *

from utils.validation import is_blank
from utils.constants import *
from utils.qtutil import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyAdvancedSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(SvyAdvancedSettingsFrame, self).__init__(*args, **kw)

		self.__create_variables()
		self.__create_widgets()
		self.__set_accessible_names()
		self.__create_bindings()
		
		self.__grid_widgets()
		
	def __create_variables(self):
		self._categoryList = []
	def __create_widgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setAlignment(QtCore.Qt.AlignTop)
		self.groupBoxMineBindingParams = QtGui.QGroupBox(self)
		self.groupBoxMineBindingParams.setTitle("Тип привязки выработки")
		self.groupBoxMineBindingParams.setMaximumHeight(80)
		self.layoutMineBindingParams = QtGui.QGridLayout(self.groupBoxMineBindingParams)
		self.layoutMineBindingParams.setSpacing(5)

		self.radioButtonTechGrid = QtGui.QRadioButton(self.groupBoxMineBindingParams)
		self.radioButtonTechGrid.setText("К тех.сети")
		self.radioButtonTechGrid.setChecked(True)
		
		self.radioButtonSubUnit = QtGui.QRadioButton(self.groupBoxMineBindingParams)
		self.radioButtonSubUnit.setText("К подюниту")
		self.radioButtonSubUnit.setChecked(False)

		self.labelNameBindAttr = QtGui.QLabel(self.groupBoxMineBindingParams)
		self.labelNameBindAttr.setText("Название атрибута привязки")
		self.labelNameBindAttr.setMinimumWidth(100)
		self.labelNameBindAttr.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineNameBindAttr = QtGui.QLineEdit(self.groupBoxMineBindingParams)
		self.lineNameBindAttr.setMaximumWidth(100)

	def __create_bindings(self):
		pass
	def __grid_widgets(self):
		self.setLayout(self.layoutMain)

		# Параметры привязки выработок
		self.layoutMain.addWidget(self.groupBoxMineBindingParams)
		self.layoutMineBindingParams.addWidget(self.radioButtonTechGrid,1,1)
		self.layoutMineBindingParams.addWidget(self.radioButtonSubUnit,1,2)
		self.layoutMineBindingParams.addWidget(self.labelNameBindAttr,1,3)
		self.layoutMineBindingParams.addWidget(self.lineNameBindAttr,1,4)

	

	def __set_accessible_names(self):
		self.radioButtonTechGrid.setAccessibleName(ID_SVY_IS_MINETECHGRIDBIND)
		self.radioButtonSubUnit.setAccessibleName(ID_SVY_IS_MINESUBUNITBIND)	
		self.lineNameBindAttr.setAccessibleName(ID_SVY_MINEBINDATTRNAME)	




