from PyQt4 import QtCore, QtGui

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.GridEditor import *
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog

from mm.mmutils import MicromineFile

from utils.qtutil import *
from utils.validation import DoubleValidator, is_blank, StringValueValidator
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class MasterGridEditor(GridEditor):
	"""
		Добавляет в классы наследники GridEditor функционал мастер грида над слайв гридами в  QtGui.QStackedWidget
	"""
	def __init__(self, parent, columns = [], widgets = [],  *args, **kw):

		super(MasterGridEditor, self).__init__(parent, columns, widgets, *args, **kw)
		
		self.__createVariables()
		#self.build()

	def __createVariables(self):
		self._stackedIndexes = None

	def setSlaveStackedWidget(self, stackedWidget):
		self._stackedIndexes = stackedWidget

	def appendRow(self):
		nRows = self.dataGrid.rowCount()
		self.insertRow(nRows)
		return nRows
	def clear(self):
		DataGrid.clear(self.dataGrid)
		for i in range(self._stackedIndexes.count(), -1, -1):
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(i))	
		#
		self.appendRow()
	def insertRow(self, iRow):
		DataGrid.insertRow(self.dataGrid, iRow)

	def removeSelectedRows(self):
		selectedRows = sorted(self.dataGrid.getRowsFromIndexes(self.dataGrid.selectedIndexes()), reverse = True)
		DataGrid.removeSelectedRows(self.dataGrid)
		#
		for iRow in selectedRows:
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(iRow))
		#
		if self.dataGrid.rowCount() == 0:
			self.appendRow()
	def moveSelectedRowsUp(self):
		selectedRows = sorted(self.dataGrid.getRowsFromIndexes(self.dataGrid.selectedIndexes()))
		DataGrid.moveSelectedRowsUp(self.dataGrid)
		#
		if selectedRows[0] == 0:
			return
		#
		for iRow in selectedRows:
			self._stackedIndexes.insertWidget(iRow-1, self._stackedIndexes.widget(iRow))
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(iRow+1))
	def moveSelectedRowsDown(self):
		selectedRows = sorted(self.dataGrid.getRowsFromIndexes(self.dataGrid.selectedIndexes()), reverse = True)
		DataGrid.moveSelectedRowsDown(self.dataGrid)
		#
		nRows = self.dataGrid.rowCount()
		if selectedRows[0] == nRows-1:
			return
		#
		for iRow in selectedRows:
			self._stackedIndexes.insertWidget(iRow+2, self._stackedIndexes.widget(iRow))
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(iRow))


class OreCodeGridEditor(MasterGridEditor):
	def __init__(self, parent):
		super(OreCodeGridEditor, self).__init__(parent,  
												["Материал отчёта", "Материал БМ"],
												[None, QtGui.QComboBox]
												)
		self.__createVariables()
		self.__createBindings()
		self.build()
	
	def __createVariables(self):
		self._settings = self.parent().parent()._settings
		self.rep_indexes = self.build_rep_indexes()
	def build(self):
		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)
		self.dataGrid.setColumnWidth(0, 150)
		self.dataGrid.setColumnWidth(1, 150)
		self.toolBar.pushButtonRefresh.setVisible(False)

	def __createBindings(self):
		self.dataGrid.comboItemChanged.connect(self._attributeChanged)


	def insertRow(self, iRow):
		super(OreCodeGridEditor, self).insertRow(iRow)

		bmMaterials = list(zip(*self.rep_indexes))[0]
		newSlaveWidget = SlaveContainer(self._stackedIndexes)
		newSlaveWidget.setMasterGrid(self)
		self._stackedIndexes.insertWidget(iRow, newSlaveWidget)
		self.dataGrid.cellWidget(iRow, 1).addItems(bmMaterials)
		#
		
	def build_rep_indexes(self):
		result = []
		result.append(["",[]])
		geocodes = self._settings.getValue(ID_GEOCODES)	
		if not geocodes:
			return 	result

		filtered_geocodes = list(filter(lambda x: x[0][2] != GEO_VARIATIONS.index("Прочее"), geocodes)) 
		for material in filtered_geocodes:
			result_item = []
			result_item.append(material[0][1])
			bm_indexes_branch = material[1]["BM"]
			if len(bm_indexes_branch) == 0:
				bm_indexes_list = []
			else:
				bm_indexes = list(zip(*bm_indexes_branch))[0]
				bm_indexes_list = list(zip(*bm_indexes))[0]
			result_item.append(bm_indexes_list)
			result.append(result_item)

		svycodes = self._settings.getValue(ID_SVYCAT)
		filtered_svycodes = list(filter(lambda x: x[0][1]=='ЗАКЛ', svycodes))
		for material in filtered_svycodes:
			result_item = []
			result_item.append(material[0][0])
			bm_indexes_list = list(zip(*material[1]))[0]
			result_item.append(bm_indexes_list)
			result.append(result_item)

		return result

	def checkRepIndexesSettings_CatHasOnlyOwnIndexes(self):
		is_correct = True
		for r_master in range(0,self.dataGrid.rowCount()):
			cur_rep_material_index = self.dataGrid.getCellValue(r_master, 1)
			slave_container = self._stackedIndexes.widget(r_master)
			slave_grid = slave_container.gridEditorBmIndex
			cur_rep_indexes = []
			for r_slave in range(0,slave_grid.dataGrid.rowCount()):
				cur_rep_indexes.append(slave_grid.dataGrid.item(r_slave, 0).text())
	
			bm_indexes = self.rep_indexes[cur_rep_material_index][1]

			diff = set(cur_rep_indexes)	- set(bm_indexes) - set([""])
			if len(diff) > 0:
				slave_grid.dataGrid.clear()
				is_correct = False

		return 	is_correct

	def checkRepIndexesSettings_IsAllIndexesBoundToRepCats(self):
		is_correct = True
		all_rep_indexes = []
		for r_master in range(0,self.dataGrid.rowCount()):
			cur_rep_material_index = self.dataGrid.getCellValue(r_master, 1)
			slave_container = self._stackedIndexes.widget(r_master)
			slave_grid = slave_container.gridEditorBmIndex
			cur_rep_indexes = []
			for r_slave in range(0,slave_grid.dataGrid.rowCount()):
				all_rep_indexes.append(slave_grid.dataGrid.item(r_slave, 0).text())

		bm_indexes_lists = list(zip(*self.rep_indexes))[1]
		bm_indexes = [ item for sublist in bm_indexes_lists for item in sublist]

		diff = set(bm_indexes)	- set(all_rep_indexes) 

		return 	diff
										
	def _attributeChanged(self,  index, row): 
		if self._stackedIndexes:
			slave_container = self._stackedIndexes.widget(row)
			slave_container.setBindKey(index)
			# При выборе другого материала БМ, теперь ранее выбранные индексы не очищаются,
			# позволяя добавить индексы других материалов БМ в тот же самый материал отчета
			#slave_container.gridEditorBmIndex.dataGrid.clear()


class BmIndexItemDelegate(QtGui.QStyledItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()

		editor = QtGui.QLineEdit(widget)		   
		if icol == 0:
			_validator = StringValueValidator()
			editor.setValidator(_validator)
			editor.setReadOnly(True)
		return editor

class BmIndexGridEditor(GridEditor):
	def __init__(self, parent):
		super(BmIndexGridEditor, self).__init__(parent,
										  ["Индексы БМ"]
										  )

		self.dataGrid.setColumnWidth(0, 110)
		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)
		self.toolBar.pushButtonRefresh.setVisible(False)
		self.toolBar.pushButtonInsert.setVisible(False)

		self.dataGrid.setItemDelegate(BmIndexItemDelegate())

		self.toolBar.pushButtonAppend.clicked.disconnect(self.appendRow)
		self.toolBar.pushButtonAppend.clicked.connect(self.appendRestIndexes)
		self._slave_container = parent
		self._stackedWidget = self._slave_container.parent()
		

	def appendRestIndexes(self):
		answer = qtmsgbox.show_question(self,"Добавление индексов","Добавить все незарезервированные индексы\n текущщего материала БМ?")
		if answer:
			n_idx_grids = self._stackedWidget.count()
			bound_indexes = []
			for i in range(0, n_idx_grids):
				geBmIndex = self._stackedWidget.widget(i).gridEditorBmIndex
				nRowCount = geBmIndex.dataGrid.rowCount()
				for r in range (0, nRowCount):
					bound_indexes.append(geBmIndex.dataGrid.item(r, 0).text())

			bindKey = self.parent().bindKey  # потому что первая строка списка пустое значение
			bind_geocodes = self._slave_container.masterGrid.rep_indexes[bindKey]
			bm_indexes =  bind_geocodes[1]
			
			rest_bm_indexes = set(bm_indexes) - set(bound_indexes)
			if len(rest_bm_indexes) == 0:
				qtmsgbox.show_information(self, "Информация", "У данного материала нет свободных индексов.\n Удалите индексы в других отчётных материалах.")
				return
			nRows = self.dataGrid.rowCount()
			for idx, bm_index in enumerate(rest_bm_indexes, start = nRows):
				self.appendRow()
				self.dataGrid.item(idx, 0).setText(bm_index)





class SlaveContainer(QtGui.QWidget):
	def __init__(self, parent):
		super(SlaveContainer, self).__init__(parent)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		self.bindKey = None
		self.masterGrid	= None
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setSpacing(5)
		self.layoutMain.setMargin(0)

		self.gridEditorBmIndex = BmIndexGridEditor(self)

		self.gridEditorBmIndex.dataGrid.setItemDelegate(BmIndexItemDelegate())

		
	def __createBindings(self):
		self.gridEditorBmIndex.dataGrid.itemSelectionChanged.connect(self.on_gridEditorBmIndex_SelectionChanged)
	

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.gridEditorBmIndex)


	def on_gridEditorBmIndex_SelectionChanged(self):
		iRow = self.gridEditorBmIndex.dataGrid.currentIndex().row()

	def setBindKey(self, value):
		self.bindKey = value

	def setMasterGrid(self, value):
		self.masterGrid = value


class SvyReportsSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(SvyReportsSettingsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.__setAccessibleNames()
	def __createVariables(self):
		self._settings = self.parent().parent().parent().settings
		


	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(5)
		self.layoutMain.setAlignment(QtCore.Qt.AlignTop)

		#
		self.groupBoxCategory = QtGui.QGroupBox(self)
		self.groupBoxCategory.setTitle("Соответствие отчётных материалов и индексов блочной модели")
		self.groupBoxCategory.setMaximumHeight(300)
		self.layoutCategory = QtGui.QHBoxLayout()
		self.layoutCategory.setSpacing(5)
		#
		self.stackedIndexes = QtGui.QStackedWidget(self.groupBoxCategory)
		self.indexGrids = SlaveContainer(self.stackedIndexes)
		#
		self.gridEditorCodes = OreCodeGridEditor(self.groupBoxCategory)
		self.gridEditorCodes.setSlaveStackedWidget( self.stackedIndexes)
		self.gridEditorCodes.getValue = self.values
		self.gridEditorCodes.setValue = self.setValues

	def __createBindings(self):
		self.gridEditorCodes.dataGrid.itemSelectionChanged.connect(self.oreCodeSelectionChanged)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxCategory)

		self.groupBoxCategory.setLayout(self.layoutCategory)
		self.layoutCategory.addWidget(self.gridEditorCodes)

		self.layoutCategory.addWidget(self.stackedIndexes)
		self.stackedIndexes.addWidget(self.indexGrids)

	def __setAccessibleNames(self):
		self.gridEditorCodes.setAccessibleName(ID_RPT_GEO_SVY_INDEXES)

	def oreCodeSelectionChanged(self):
		iRow = self.gridEditorCodes.dataGrid.currentIndex().row()
		self.stackedIndexes.setCurrentIndex(iRow)

	def setValues(self, values):
		codeGE = self.gridEditorCodes
		codeGE.clear()
		nValues = len(values)
		for iRow, (rptDescription, oreDescription, objects) in enumerate(values):
			codeGE.dataGrid.item(iRow, 0).setText(rptDescription)
			codeGE.dataGrid.cellWidget(iRow, 1).setCurrentIndex(oreDescription)
			bmGE = self.stackedIndexes.widget(iRow).gridEditorBmIndex
			bmGE.clear()
			bmGE.appendRow()
			bmGE_nValues = len(objects["INDEX"])
			for iRow_bmGE, bmIndex in enumerate(objects["INDEX"]):
				bmGE.dataGrid.item(iRow_bmGE, 0).setText(bmIndex[0])
				if iRow_bmGE < bmGE_nValues-1:
					bmGE.appendRow()

			if iRow < nValues-1:
				codeGE.appendRow()

		# Отключено в связи с возможностьб привязки к одному отчетному материалу индексов из нескольких материалов БМ
		#if not self.gridEditorCodes.checkRepIndexesSettings_CatHasOnlyOwnIndexes():
		#	qtmsgbox.show_information(self, "Внимание", "Были обнаружены не корретные настройки индексов отчёта маркшейдера.\n Некоректные материалы были очищены.")
		rest_indexes = self.gridEditorCodes.checkRepIndexesSettings_IsAllIndexesBoundToRepCats()
		if len(rest_indexes) > 0:
			qtmsgbox.show_information(self, "Внимание", "Были обнаружены индексы БМ не связанные с отчётными категориями: {0}\n В отчёте данные объёмы не будут учитаны.".format(",".join(rest_indexes)))


	def values(self):
		
		ret = []
		for iRow, row in enumerate(self.gridEditorCodes.dataGrid.to_list()):
			row[1] = 0 # сбрасываем значение материала БМ так как решили что отчётный материал отвязывается от материала БМ. Данное поле теперь		
						# служит только для удобства выбора индексов . Один отчётный материал может включать индексы из нескольких материалов БМ.
			bmGE = self.stackedIndexes.widget(iRow).gridEditorBmIndex
			bmGE_list = bmGE.dataGrid.to_list()
			objects = {	"INDEX": bmGE_list}
			row.append(objects)
			ret.append(row)
		return ret
