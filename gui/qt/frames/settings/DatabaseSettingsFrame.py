from PyQt4 import QtCore, QtGui

from gui.qt.widgets.GridEditor import *

from utils.qtutil import *
from utils.constants import *
from utils.validation import SqlInjectionValidator
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DatabaseSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DatabaseSettingsFrame, self).__init__(*args, **kw)

		self.__create_variables()

		self.__create_widgets()
		self.__retranslate_ui()
		self.__set_accessible_names()

		self.__create_bindings()
		self.__grid_widgets()

	def __create_variables(self):
		pass
	def __create_widgets(self):
		self.mainLayout = QtGui.QVBoxLayout()

		self.groupBoxDatabaseProperties = QtGui.QGroupBox(self)
		self.groupBoxDatabaseProperties.setMaximumSize(QtCore.QSize(16777215, 120))

		self.dbpropLayout = QtGui.QVBoxLayout()
		self.formLayoutConnectionProps = QtGui.QFormLayout()
		self.formLayoutConnectionProps.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
		self.formLayoutConnectionProps.setSpacing(5)

		self.labelServerName = QtGui.QLabel(self.groupBoxDatabaseProperties)
		self.labelServerName.setMinimumSize(QtCore.QSize(160, 0))
		self.labelServerName.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)


		self.lineEditServerName = QtGui.QLineEdit(self.groupBoxDatabaseProperties)

		self.labelDbNameGeo = QtGui.QLabel(self.groupBoxDatabaseProperties)
		self.labelDbNameGeo.setMinimumSize(QtCore.QSize(160, 0))
		self.labelDbNameGeo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

		self.lineEditDbNameGeo = QtGui.QLineEdit(self.groupBoxDatabaseProperties)
		self.lineEditDbNameGeo.setValidator(SqlInjectionValidator())

		self.labelDbNameSvy = QtGui.QLabel(self.groupBoxDatabaseProperties)
		self.labelDbNameSvy.setMinimumSize(QtCore.QSize(160, 0))
		self.labelDbNameSvy.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

		self.lineEditDbNameSvy = QtGui.QLineEdit(self.groupBoxDatabaseProperties)
		self.lineEditDbNameSvy.setValidator(SqlInjectionValidator())

		self.groupBoxMineList = QtGui.QGroupBox(self)


		self.mineListLayout = QtGui.QVBoxLayout(self.groupBoxMineList)
		self.gridEditorMineList = GridEditor(self.groupBoxMineList, columns = ["Наименование рудника"])
		self.gridEditorMineList.getValue = self.gridEditorMineList.dataGrid.to_list
		self.gridEditorMineList.setValue = self.gridEditorMineList.dataGrid.from_list

		self._build_grid_mine_list()

		self.formLayoutDefaultMine = QtGui.QFormLayout()
		self.formLayoutDefaultMine.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
		self.formLayoutDefaultMine.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.formLayoutDefaultMine.setSpacing(5)

		# self.labelDefaultMine = QtGui.QLabel(self.groupBoxMineList)
		# self.labelDefaultMine.setMinimumSize(QtCore.QSize(160, 0))
		# self.labelDefaultMine.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

		# self.comboBoxDefaultMine = QtGui.QComboBox(self.groupBoxMineList)
		# self.comboBoxDefaultMine.setMaximumSize(QtCore.QSize(150, 16777215))
	def __create_bindings(self):
		pass

	def __retranslate_ui(self):
		self.groupBoxDatabaseProperties.setTitle("Параметры подключения к базе данных SQL")
		self.labelServerName.setText("Имя сервера")
		self.labelDbNameGeo.setText("База данных (геология)")
		self.labelDbNameSvy.setText("База данных (маркшейдерия)")
		self.groupBoxMineList.setTitle("Список рудников")
		# self.labelDefaultMine.setText("По умолчанию")

	def __set_accessible_names(self):
		self.lineEditServerName.setAccessibleName(ID_SRVNAME)
		self.lineEditDbNameGeo.setAccessibleName(ID_DBGEO)
		self.lineEditDbNameSvy.setAccessibleName(ID_DBSVY)
		self.gridEditorMineList.setAccessibleName(ID_MINELIST)

	def __grid_widgets(self):
		self.setLayout(self.mainLayout)

		self.mainLayout.addWidget(self.groupBoxDatabaseProperties)
		self.groupBoxDatabaseProperties.setLayout(self.formLayoutConnectionProps)
		self.formLayoutConnectionProps.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelServerName)
		self.formLayoutConnectionProps.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditServerName)
		self.formLayoutConnectionProps.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelDbNameGeo)
		self.formLayoutConnectionProps.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditDbNameGeo)
		self.formLayoutConnectionProps.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelDbNameSvy)
		self.formLayoutConnectionProps.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditDbNameSvy)


		self.mainLayout.addWidget(self.groupBoxMineList)
		self.groupBoxMineList.setLayout(self.mineListLayout)
		self.mineListLayout.addWidget(self.gridEditorMineList)
		# self.mineListLayout.addLayout(self.formLayoutDefaultMine)
		# self.formLayoutDefaultMine.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelDefaultMine)
		# self.formLayoutDefaultMine.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxDefaultMine)

	def _build_grid_mine_list(self):
		self.gridEditorMineList.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.gridEditorMineList.dataGrid.verticalHeader().setVisible(False)
		#
		self.gridEditorMineList.toolBar.pushButtonRefresh.setVisible(False)
		self.gridEditorMineList.toolBar.pushButtonMoveUp.setVisible(False)
		self.gridEditorMineList.toolBar.pushButtonMoveDown.setVisible(False)


