from PyQt4 import QtCore, QtGui

import tkinter as tk
import tkinter.messagebox as tkmsgbox

from gui.tk.libinstaller import LibInstaller

from utils.qtutil import *
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class GeneralSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(GeneralSettingsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.__setAccessibleNames()

		self.setActions()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout(self)
			
		self.groupBoxUserRoles = QtGui.QGroupBox(self)
		self.groupBoxUserRoles.setTitle("Роль пользователя")
		self.groupBoxUserRoles.setMaximumHeight(150)
		self.layoutUserRoles = QtGui.QVBoxLayout()
		self.labelUserRole = QtGui.QLabel(self.groupBoxUserRoles)
		self.labelUserRole.setText("Выберите роль пользователя")
		self.radioButtonBox = QtGui.QButtonGroup(self.groupBoxUserRoles)
		self.radioButtonGeo = QtGui.QRadioButton(self.groupBoxUserRoles)
		self.radioButtonGeo.setText("Геолог")
		self.radioButtonSvy = QtGui.QRadioButton(self.groupBoxUserRoles)
		self.radioButtonSvy.setText("Маркшейдер")
		self.radioButtonDsn = QtGui.QRadioButton(self.groupBoxUserRoles)
		self.radioButtonDsn.setText("Проектировщик")
		self.radioButtonAdmin = QtGui.QRadioButton(self.groupBoxUserRoles)
		self.radioButtonAdmin.setText("Администратор")
		self.radioButtonBox.addButton(self.radioButtonGeo)
		self.radioButtonBox.addButton(self.radioButtonSvy)
		self.radioButtonBox.addButton(self.radioButtonDsn)
		self.radioButtonBox.addButton(self.radioButtonAdmin)
		
		self.groupBoxNetworkProject = QtGui.QGroupBox(self)
		self.groupBoxNetworkProject.setTitle("Сетевой проект")
		self.groupBoxNetworkProject.setMaximumHeight(80)
		self.layoutNetworkProject = QtGui.QFormLayout()
		self.lineEditNetworkPath = QtGui.QLineEdit(self.groupBoxNetworkProject)
		self.labelNetworkPath = QtGui.QLabel(self.groupBoxNetworkProject)
		self.labelNetworkPath.setText("Сетевой путь")
		self.labelNetworkPath.setMinimumWidth(100)
		self.labelNetworkPath.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.labelProjectName = QtGui.QLabel(self.groupBoxNetworkProject)
		self.labelProjectName.setText("Имя проекта")
		self.labelProjectName.setMinimumWidth(100)
		self.labelProjectName.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditProjectName = QtGui.QLineEdit(self.groupBoxNetworkProject)
		
		self.groupBoxSecurity = QtGui.QGroupBox(self)
		self.groupBoxSecurity.setTitle("Безопасность")
		self.groupBoxSecurity.setMinimumHeight(80)
		self.layoutSecurity = QtGui.QFormLayout()
		self.checkBoxUsePassword = QtGui.QCheckBox(self.groupBoxSecurity)
		self.checkBoxUsePassword.setText("Использовать пароль для доступа к настройкам")
		self.checkBoxUsePassword.setMinimumWidth(450)
		self.checkBoxUsePassword.clicked.connect(self.setActions)
		self.pushButtonPasswordChange = QtGui.QPushButton(self)
		self.pushButtonPasswordChange.setText("Изменить")
		
		self.groupBoxPythonLibs = QtGui.QGroupBox(self)
		self.groupBoxPythonLibs.setTitle("Библиотеки Python")
		self.groupBoxPythonLibs.setMinimumHeight(60)
		self.layoutPythonLibs = QtGui.QFormLayout()
		self.labelInstall = QtGui.QLabel(self.groupBoxPythonLibs)
		self.labelInstall.setText("Нажмите на кнопку для запуска установки библиотек")
		self.labelInstall.setMinimumWidth(450)
		self.pushButtonInstall = QtGui.QPushButton(self.groupBoxPythonLibs)
		self.pushButtonInstall.setText("Установить")

		self.groupBoxPythonLibs.setVisible(False)

	def __createBindings(self):
		self.pushButtonInstall.clicked.connect(self.install_libs)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		
		self.layoutMain.addWidget(self.groupBoxUserRoles)
		self.groupBoxUserRoles.setLayout(self.layoutUserRoles)
		self.layoutUserRoles.addWidget(self.labelUserRole)
		self.layoutUserRoles.addWidget(self.radioButtonGeo)
		self.layoutUserRoles.addWidget(self.radioButtonSvy)
		self.layoutUserRoles.addWidget(self.radioButtonDsn)
		self.layoutUserRoles.addWidget(self.radioButtonAdmin)
		
		self.layoutMain.addWidget(self.groupBoxNetworkProject)
		self.groupBoxNetworkProject.setLayout(self.layoutNetworkProject)
		self.layoutNetworkProject.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelNetworkPath)
		self.layoutNetworkProject.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditNetworkPath)
		self.layoutNetworkProject.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelProjectName)
		self.layoutNetworkProject.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditProjectName)
		
		self.layoutMain.addWidget(self.groupBoxSecurity)
		self.groupBoxSecurity.setLayout(self.layoutSecurity)
		self.layoutSecurity.setWidget(1, QtGui.QFormLayout.LabelRole, self.checkBoxUsePassword)
		self.layoutSecurity.setWidget(1, QtGui.QFormLayout.FieldRole, self.pushButtonPasswordChange)

		self.layoutMain.addWidget(self.groupBoxPythonLibs)
		self.groupBoxPythonLibs.setLayout(self.layoutPythonLibs)
		self.layoutPythonLibs.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelInstall)
		self.layoutPythonLibs.setWidget(0, QtGui.QFormLayout.FieldRole, self.pushButtonInstall)

	def __setAccessibleNames(self):
		self.radioButtonGeo.setAccessibleName(ID_USERGEO)
		self.radioButtonSvy.setAccessibleName(ID_USERSVY)
		self.radioButtonDsn.setAccessibleName(ID_USERDSN)
		self.radioButtonAdmin.setAccessibleName(ID_USERADMIN)
		self.lineEditNetworkPath.setAccessibleName(ID_NETPATH)
		self.lineEditProjectName.setAccessibleName(ID_PROJNAME)
		self.checkBoxUsePassword.setAccessibleName(ID_PASS)

	def setActions(self):
		flag = self.checkBoxUsePassword.isChecked()
		self.pushButtonPasswordChange.setEnabled(flag)

	def change_pass(self):
		old_password = ""

		# window = ChangePasswordDialog()
		window.setPassword(old_password)
		window.exec()

		new_password = window.getPassword()

	def install_libs(self):
		root = tk.Tk()
		try:
			app = LibInstaller(root)
			root.mainloop()
		except Exception as e:
			tkmsgbox.showerror("Ошибка", str(e), parent = root)
			root.destroy()
