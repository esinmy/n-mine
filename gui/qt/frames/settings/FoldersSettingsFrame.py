from PyQt4 import QtCore, QtGui

from gui.qt.widgets.GridEditor import *
from utils.qtutil import *
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class FoldersSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(FoldersSettingsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(5)

		# Директория с залежами
		self.groupBoxLayer = QtGui.QGroupBox(self)
		self.groupBoxLayer.setTitle("Директория с залежами")
		self.groupBoxLayer.setMaximumHeight(55)
		self.layoutParentDirectory = QtGui.QFormLayout()
		self.layoutParentDirectory.setLabelAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.layoutParentDirectory.setSpacing(5)
		self.labelParentDirectory = QtGui.QLabel(self.groupBoxLayer)
		self.labelParentDirectory.setText("Директория")
		self.labelParentDirectory.setMinimumWidth(100)
		self.labelParentDirectory.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditParentDirectory = QtGui.QLineEdit(self.groupBoxLayer)
		
		self.groupBoxUnitParams = QtGui.QGroupBox(self)
		self.groupBoxUnitParams.setTitle("Параметры юнитов")
		self.groupBoxUnitParams.setMaximumHeight(80)
		self.layoutPrefixes = QtGui.QFormLayout()
		self.layoutPrefixes.setLabelAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.layoutPrefixes.setSpacing(5)

		self.labelUnitPrefix = QtGui.QLabel(self.groupBoxUnitParams)
		self.labelUnitPrefix.setText("Префикс юнита")
		self.labelUnitPrefix.setMinimumWidth(100)
		self.labelUnitPrefix.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditUnitPrefix = QtGui.QLineEdit(self.groupBoxUnitParams)
		self.lineEditUnitPrefix.setMaximumWidth(100)
		
		self.labelUnitDisplayName = QtGui.QLabel(self.groupBoxUnitParams)
		self.labelUnitDisplayName.setText("Название юнита")
		self.labelUnitDisplayName.setMinimumWidth(100)
		self.labelUnitDisplayName.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditUnitDisplayName = QtGui.QLineEdit(self.groupBoxUnitParams)
		self.lineEditUnitDisplayName.setMaximumWidth(100)



		self.groupBoxDirectories = QtGui.QGroupBox(self)
		self.groupBoxDirectories.setTitle("Вложенные директории")
		self.groupBoxDirectories.setMaximumHeight(200)
		self.layoutDirectories = QtGui.QHBoxLayout()
		self.layoutDirectories.setSpacing(5)

		self.gridEditorUnitSubdirs = GridEditorFolders(self.groupBoxDirectories)
		self.splitterSubdirs = QtGui.QSplitter(self.groupBoxDirectories)

		self.gridEditorHideDirs = GridEditor(self.groupBoxDirectories, ["Скрыть", "Директория"])
		self.gridEditorHideDirs.toolBar.labelHint.setText("Скрыть директории")
		self.gridEditorHideDirs.getValue = self.gridEditorHideDirs.dataGrid.to_list
		self.gridEditorHideDirs.setValue = self._setHideDirs
		self._buildHideDirsTable()

		self.groupBoxDisplayParams = QtGui.QGroupBox(self)
		self.groupBoxDisplayParams.setTitle("Параметры отображения")
		self.groupBoxDisplayParams.setMaximumHeight(200)
		self.layoutDisplayParams = QtGui.QVBoxLayout()
		self.gridEditorDisplayParams = GridEditorFormsets(self.groupBoxDisplayParams)

	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		# Директория с залежами
		self.layoutMain.addWidget(self.groupBoxLayer)
		self.groupBoxLayer.setLayout(self.layoutParentDirectory)
		self.layoutParentDirectory.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelParentDirectory)
		self.layoutParentDirectory.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditParentDirectory)

		# Параметры юнитов
		self.layoutMain.addWidget(self.groupBoxUnitParams)
		self.groupBoxUnitParams.setLayout(self.layoutPrefixes)
		self.layoutPrefixes.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelUnitPrefix)
		self.layoutPrefixes.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditUnitPrefix)
		self.layoutPrefixes.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelUnitDisplayName)
		self.layoutPrefixes.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditUnitDisplayName)


		# Вложенные директории
		self.layoutMain.addWidget(self.groupBoxDirectories)
		self.groupBoxDirectories.setLayout(self.layoutDirectories)
		
		# Список юнитов и директорий внутри юнитов
		self.layoutDirectories.addWidget(self.gridEditorUnitSubdirs)
		self.layoutDirectories.addWidget(self.splitterSubdirs)
		self.layoutDirectories.addWidget(self.gridEditorHideDirs)

		self.splitterSubdirs.addWidget(self.gridEditorUnitSubdirs)
		self.splitterSubdirs.addWidget(self.gridEditorHideDirs)

		self.splitterSubdirs.setStretchFactor(0, 1)
		self.splitterSubdirs.setStretchFactor(1, 1)

		# параметры отображения
		self.layoutMain.addWidget(self.groupBoxDisplayParams)
		self.groupBoxDisplayParams.setLayout(self.layoutDisplayParams)
		self.layoutDisplayParams.addWidget(self.gridEditorDisplayParams)

	#
	def _buildHideDirsTable(self):
		self.gridEditorHideDirs.dataGrid.horizontalHeader().setHighlightSections(False)
		self.gridEditorHideDirs.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.gridEditorHideDirs.dataGrid.verticalHeader().setVisible(False)
		self.gridEditorHideDirs.dataGrid.setMinimumWidth(150)

		self.gridEditorHideDirs.dataGrid.setAppendByKeyboard(False)
		self.gridEditorHideDirs.dataGrid.resizeColumnToContents(0)
		self.gridEditorHideDirs.dataGrid.setDeleteByKeyboard(False)

		# toolbar
		self.gridEditorHideDirs.dataGrid.setReadOnly(True)
		self.gridEditorHideDirs.toolBar.pushButtonClear.setVisible(False)
		self.gridEditorHideDirs.toolBar.pushButtonAppend.setVisible(False)
		self.gridEditorHideDirs.toolBar.pushButtonInsert.setVisible(False)
		self.gridEditorHideDirs.toolBar.pushButtonMoveUp.setVisible(False)
		self.gridEditorHideDirs.toolBar.pushButtonMoveDown.setVisible(False)
		self.gridEditorHideDirs.toolBar.pushButtonDelete.setVisible(False)


	def insertDefaultData(self, grid, data):
		grid.setRowCount(0)
		for irow, row in enumerate(data):
			grid.appendRow()
			for icol, val in enumerate(row):
				item = DataGridItem(val)
				if icol == 0:
					item.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEnabled)
					item.setLocked(True)
				grid.setItem(irow, icol, item)

	#
	def addChildDirectory(self, dirname):
		grid = self.gridEditorHideDirs.dataGrid
		irow = self.gridEditorHideDirs.dataGrid.rowCount()
		grid.appendRow()
		
		grid.setCellWidget(irow, 0, CheckBoxCellWidget())

		dirname_item = DataGridItem()
		dirname_item.setText(dirname)
		grid.setItem(irow, 1, dirname_item)

	def _setHideDirs(self, values):
		grid = self.gridEditorHideDirs.dataGrid
		grid.removeAll()
		nRows = len(values)
		nCols = grid.columnCount()
		for iRow in range(nRows):
			self.addChildDirectory(values[iRow][1])
			widget = grid.cellWidget(iRow, 0)
			setWidgetValue(widget, values[iRow][0])