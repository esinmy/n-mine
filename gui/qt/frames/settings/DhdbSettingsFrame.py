from PyQt4 import QtCore, QtGui

from utils.qtutil import *
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DhdbSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DhdbSettingsFrame, self).__init__(*args, **kw)

		self.__create_variables()
		
		self.__create_widgets()
		self.__set_accessible_names()
		self.__retranslate_ui()
		
		self.__create_bindings()
		self.__grid_widgets()
	
	def __create_variables(self):
		pass
	def __create_widgets(self):
		self.verticalLayout = QtGui.QVBoxLayout()

		self.groupBoxDbDirectory = QtGui.QGroupBox(self)
		self.groupBoxDbDirectory.setMaximumHeight(55)
		self.formLayoutDbDirectory = QtGui.QFormLayout()
		self.formLayoutDbDirectory.setSpacing(5)
		self.labelDbDirectory = QtGui.QLabel(self.groupBoxDbDirectory)
		self.labelDbDirectory.setMinimumWidth(100)
		self.labelDbDirectory.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditDbDirectory = QtGui.QLineEdit(self.groupBoxDbDirectory)

		self.groupBoxOperExpl = QtGui.QGroupBox(self)
		self.formLayoutOperExpl = QtGui.QFormLayout()
		self.formLayoutOperExpl.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.formLayoutOperExpl.setSpacing(5)
		self.labelCollarsOperExpl = QtGui.QLabel(self.groupBoxOperExpl)
		self.labelCollarsOperExpl.setMinimumWidth(100)
		self.labelCollarsOperExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditCollarsOperExpl = QtGui.QLineEdit(self.groupBoxOperExpl)
		self.labelAssayOperExpl = QtGui.QLabel(self.groupBoxOperExpl)
		self.labelAssayOperExpl.setMinimumWidth(100)
		self.labelAssayOperExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditAssayOperExpl = QtGui.QLineEdit(self.groupBoxOperExpl)
		self.labelSurveyOperExpl = QtGui.QLabel(self.groupBoxOperExpl)
		self.labelSurveyOperExpl.setMinimumWidth(100)
		self.labelSurveyOperExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditSurveyOperExpl = QtGui.QLineEdit(self.groupBoxOperExpl)
		self.labelLithologyOperExpl = QtGui.QLabel(self.groupBoxOperExpl)
		self.labelLithologyOperExpl.setMinimumWidth(100)
		self.labelLithologyOperExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditLithologyOperExpl = QtGui.QLineEdit(self.groupBoxOperExpl)
		
		self.groupBoxDetailExpl = QtGui.QGroupBox(self)
		self.formLayoutDetailExpl = QtGui.QFormLayout()
		self.formLayoutDetailExpl.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.formLayoutDetailExpl.setSpacing(5)
		self.labelCollarsDetailExpl = QtGui.QLabel(self.groupBoxDetailExpl)
		self.labelCollarsDetailExpl.setMinimumWidth(100)
		self.labelCollarsDetailExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditCollarsDetailExpl = QtGui.QLineEdit(self.groupBoxDetailExpl)
		self.labelAssayDetailExpl = QtGui.QLabel(self.groupBoxDetailExpl)
		self.labelAssayDetailExpl.setMinimumWidth(100)
		self.labelAssayDetailExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditAssayDetailExpl = QtGui.QLineEdit(self.groupBoxDetailExpl)
		self.labelSurveyDetailExpl = QtGui.QLabel(self.groupBoxDetailExpl)
		self.labelSurveyDetailExpl.setMinimumWidth(100)
		self.labelSurveyDetailExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditSurveyDetailExpl = QtGui.QLineEdit(self.groupBoxDetailExpl)
		self.labelLithologyDetailExpl = QtGui.QLabel(self.groupBoxDetailExpl)
		self.labelLithologyDetailExpl.setMinimumWidth(100)
		self.labelLithologyDetailExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditLithologyDetailExpl = QtGui.QLineEdit(self.groupBoxDetailExpl)

		self.groupBoxTrenchExpl = QtGui.QGroupBox(self)
		self.formLayoutTrenchExpl = QtGui.QFormLayout()
		self.formLayoutTrenchExpl.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.formLayoutTrenchExpl.setSpacing(5)
		self.labelCollarsTrenchExpl = QtGui.QLabel(self.groupBoxTrenchExpl)
		self.labelCollarsTrenchExpl.setMinimumWidth(100)
		self.labelCollarsTrenchExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditCollarsTrenchExpl = QtGui.QLineEdit(self.groupBoxTrenchExpl)
		self.labelAssayTrenchExpl = QtGui.QLabel(self.groupBoxTrenchExpl)
		self.labelAssayTrenchExpl.setMinimumWidth(100)
		self.labelAssayTrenchExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditAssayTrenchExpl = QtGui.QLineEdit(self.groupBoxTrenchExpl)
		self.labelSurveyTrenchExpl = QtGui.QLabel(self.groupBoxTrenchExpl)
		self.labelSurveyTrenchExpl.setMinimumWidth(100)
		self.labelSurveyTrenchExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditSurveyTrenchExpl = QtGui.QLineEdit(self.groupBoxTrenchExpl)
		self.labelLithologyTrenchExpl = QtGui.QLabel(self.groupBoxTrenchExpl)
		self.labelLithologyTrenchExpl.setMinimumWidth(100)
		self.labelLithologyTrenchExpl.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter)
		self.lineEditLithologyTrenchExpl = QtGui.QLineEdit(self.groupBoxTrenchExpl)

	def __create_bindings(self):
		 pass
		 #self.lineEditCollarsOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditCollarsOperExpl)
		 #self.lineEditAssayOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditAssayOperExpl)
		 #self.lineEditSurveyOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditSurveyOperExpl)
		 #self.lineEditLithologyOperExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditLithologyOperExpl)
		 #self.lineEditCollarsDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditCollarsDetailExpl)
		 #self.lineEditAssayDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditAssayDetailExpl)
		 #self.lineEditSurveyDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditSurveyDetailExpl)
		 #self.lineEditLithologyDetailExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditLithologyDetailExpl)
		 #self.lineEditCollarsTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditCollarsTrenchExpl)
		 #self.lineEditAssayTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditAssayTrenchExpl)
		 #self.lineEditSurveyTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditSurveyTrenchExpl)
		 #self.lineEditLithologyTrenchExpl.mouseDoubleClickEvent = lambda e: self._browse_openfile(e, self.lineEditLithologyTrenchExpl)

	def __grid_widgets(self):
		self.setLayout(self.verticalLayout)
		self.verticalLayout.addWidget(self.groupBoxDbDirectory)
		self.verticalLayout.addWidget(self.groupBoxOperExpl)
		self.verticalLayout.addWidget(self.groupBoxDetailExpl)
		self.verticalLayout.addWidget(self.groupBoxTrenchExpl)

		self.groupBoxDbDirectory.setLayout(self.formLayoutDbDirectory)
		self.formLayoutDbDirectory.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelDbDirectory)
		self.formLayoutDbDirectory.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditDbDirectory)

		self.groupBoxOperExpl.setLayout(self.formLayoutOperExpl)
		self.formLayoutOperExpl.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelCollarsOperExpl)
		self.formLayoutOperExpl.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditCollarsOperExpl)
		self.formLayoutOperExpl.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelAssayOperExpl)
		self.formLayoutOperExpl.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditAssayOperExpl)
		self.formLayoutOperExpl.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSurveyOperExpl)
		self.formLayoutOperExpl.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditSurveyOperExpl)
		self.formLayoutOperExpl.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelLithologyOperExpl)
		self.formLayoutOperExpl.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditLithologyOperExpl)
		
		self.groupBoxDetailExpl.setLayout(self.formLayoutDetailExpl)
		self.formLayoutDetailExpl.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelCollarsDetailExpl)
		self.formLayoutDetailExpl.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditCollarsDetailExpl)
		self.formLayoutDetailExpl.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelAssayDetailExpl)
		self.formLayoutDetailExpl.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditAssayDetailExpl)
		self.formLayoutDetailExpl.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSurveyDetailExpl)
		self.formLayoutDetailExpl.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditSurveyDetailExpl)
		self.formLayoutDetailExpl.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelLithologyDetailExpl)
		self.formLayoutDetailExpl.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditLithologyDetailExpl)

		self.groupBoxTrenchExpl.setLayout(self.formLayoutTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelCollarsTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditCollarsTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelAssayTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditAssayTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSurveyTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditSurveyTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelLithologyTrenchExpl)
		self.formLayoutTrenchExpl.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditLithologyTrenchExpl)
		
	def __retranslate_ui(self):
		self.groupBoxDbDirectory.setTitle("Директория базы данных")
		self.labelDbDirectory.setText("Директория")

		self.groupBoxOperExpl.setTitle("Файлы эксплуатационной разведки")
		self.labelCollarsOperExpl.setText("Устья")
		self.labelAssayOperExpl.setText("Опробование")
		self.labelSurveyOperExpl.setText("Инклинометрия")
		self.labelLithologyOperExpl.setText("Литология")

		self.groupBoxDetailExpl.setTitle("Файлы детальной разведки")
		self.labelCollarsDetailExpl.setText("Устья")
		self.labelAssayDetailExpl.setText("Опробование")
		self.labelSurveyDetailExpl.setText("Инклинометрия")
		self.labelLithologyDetailExpl.setText("Литология")

		self.groupBoxTrenchExpl.setTitle("Файлы бороздового опробования")
		self.labelCollarsTrenchExpl.setText("Борозды")
		self.labelAssayTrenchExpl.setText("Опробование")
		self.labelSurveyTrenchExpl.setText("Траектории")
		self.labelLithologyTrenchExpl.setText("Литология")

	def __set_accessible_names(self):
		self.lineEditDbDirectory.setAccessibleName(ID_DBDIR)
		self.lineEditCollarsOperExpl.setAccessibleName(ID_EXPCOLLAR)
		self.lineEditAssayOperExpl.setAccessibleName(ID_EXPASSAY)
		self.lineEditSurveyOperExpl.setAccessibleName(ID_EXPSVY)
		self.lineEditLithologyOperExpl.setAccessibleName(ID_EXPLITH)
		self.lineEditCollarsDetailExpl.setAccessibleName(ID_DETCOLLAR)
		self.lineEditAssayDetailExpl.setAccessibleName(ID_DETASSAY)
		self.lineEditSurveyDetailExpl.setAccessibleName(ID_DETSVY)
		self.lineEditLithologyDetailExpl.setAccessibleName(ID_DETLITH)
		self.lineEditCollarsTrenchExpl.setAccessibleName(ID_TRCOLLAR)
		self.lineEditAssayTrenchExpl.setAccessibleName(ID_TRASSAY)
		self.lineEditSurveyTrenchExpl.setAccessibleName(ID_TRSVY)
		self.lineEditLithologyTrenchExpl.setAccessibleName(ID_TRLITH)

	# def _browse_openfile(self, e, edit):
	# 	filters = "ДАННЫЕ (*.DAT);;СТРИНГ (*.STR)"
	# 	initialdir = os.path.dirname(edit.text())
	# 	path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", initialdir, filters).replace("/", "\\")
	# 	if path:
	# 		setWidgetValue(edit, path)
