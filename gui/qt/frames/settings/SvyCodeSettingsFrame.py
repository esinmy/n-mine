from PyQt4 import QtCore, QtGui

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.GridEditor import *

from utils.validation import is_blank
from utils.constants import *
from utils.qtutil import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class SvyCodeSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(SvyCodeSettingsFrame, self).__init__(*args, **kw)

		self.__create_variables()
		self.__create_widgets()
		self.__set_accessible_names()
		self.__create_bindings()
		
		self.__grid_widgets()
		
		self.__retranslate_ui()

	def __create_variables(self):
		self._categoryList = []
	def __create_widgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.groupBoxCategory = QtGui.QGroupBox(self)

		self.layoutCategory = QtGui.QVBoxLayout()
		self.gridEditorCategory = GridEditor(self.groupBoxCategory, ["Назначение", "Категория"])
		self.gridEditorCategory.getValue = self.categoryList
		self.gridEditorCategory.setValue = self.setCategoryList
		self._build_category_grid()
	
		self.gridEditorCode = GridEditor(self.groupBoxCategory, ["Код", "Описание"])
		self._build_code_grid()

		self.groupBoxConcrete = QtGui.QGroupBox(self)
		self.groupBoxConcrete.setMaximumSize(QtCore.QSize(16777215, 85))
		self.formLayoutConcrete = QtGui.QFormLayout()
		self.formLayoutConcrete.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
		self.formLayoutConcrete.setSpacing(5)
		self.labelConcreteCode = QtGui.QLabel(self.groupBoxConcrete)
		self.labelConcreteCode.setMinimumSize(QtCore.QSize(100, 0))
		self.labelConcreteCode.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.lineEditConcreteCode = QtGui.QLineEdit()
		self.lineEditConcreteCode.setMaximumSize(QtCore.QSize(100, 16777215))
		self.labelConcreteDensity = QtGui.QLabel()
		self.labelConcreteDensity.setMinimumSize(QtCore.QSize(100, 0))
		self.labelConcreteDensity.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.lineEditConcreteDensity = QtGui.QLineEdit()
		self.lineEditConcreteDensity.setMaximumSize(QtCore.QSize(100, 16777215))
	def __create_bindings(self):
		self.gridEditorCategory.dataGrid.itemSelectionChanged.connect(self.selectCurrentCategory)
		self.gridEditorCategory.dataGrid.itemChanged.connect(self.updateCategory)

		self.gridEditorCode.dataGrid.itemChanged.connect(self.updateCodeList)
		self.gridEditorCode.dataGrid.keyPressEvent = self.codeGridKeyPressEvent

		self.gridEditorCode.toolBar.pushButtonAppend.clicked.disconnect()
		self.gridEditorCode.toolBar.pushButtonInsert.clicked.disconnect()
		self.gridEditorCode.toolBar.pushButtonClear.clicked.disconnect()
		self.gridEditorCode.toolBar.pushButtonDelete.clicked.disconnect()
		self.gridEditorCode.toolBar.pushButtonMoveUp.clicked.disconnect()
		self.gridEditorCode.toolBar.pushButtonMoveDown.clicked.disconnect()
		self.gridEditorCode.toolBar.pushButtonAppend.clicked.connect(self.appendCode)
		self.gridEditorCode.toolBar.pushButtonInsert.clicked.connect(self.insertCode)
		self.gridEditorCode.toolBar.pushButtonClear.clicked.connect(self.clearCodes)
		self.gridEditorCode.toolBar.pushButtonDelete.clicked.connect(self.removeSelectedCodes)
		self.gridEditorCode.toolBar.pushButtonMoveUp.clicked.connect(self.moveUpSelectedCodes)
		self.gridEditorCode.toolBar.pushButtonMoveDown.clicked.connect(self.moveDownSelectedCodes)
	def __grid_widgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxConcrete)
		self.groupBoxConcrete.setLayout(self.formLayoutConcrete)
		self.formLayoutConcrete.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelConcreteCode)
		self.formLayoutConcrete.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditConcreteCode)
		self.formLayoutConcrete.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelConcreteDensity)
		self.formLayoutConcrete.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditConcreteDensity)

		self.layoutMain.addWidget(self.groupBoxCategory)
		self.groupBoxCategory.setLayout(self.layoutCategory)
		self.layoutCategory.addWidget(self.gridEditorCategory)
		self.layoutCategory.addWidget(self.gridEditorCode)

	def _build_category_grid(self):
		self.gridEditorCategory.dataGrid.setColumnWidth(0, 250)
		self.gridEditorCategory.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.gridEditorCategory.dataGrid.horizontalHeader().setHighlightSections(False)
		self.gridEditorCategory.dataGrid.verticalHeader().setVisible(False)

		categories = ["Горно-капитальные работы", "Камерно-кубажные работы", "Горно-подготовительные работы", "Очистные работы", "Оперативный учет"]
		categories += ["Неучитываемые объемы", "Закладка", "Сыпучий материал"]
		for irow, cat in enumerate(categories):
			self.gridEditorCategory.dataGrid.appendRow()
			item = DataGridItem(cat)
			item.setLocked(True)
			self.gridEditorCategory.dataGrid.setItem(irow, 0, item)
		for cat in categories:
			self._categoryList.append([[cat, ""], []])
		#
		self.gridEditorCategory.toolBar.pushButtonRefresh.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonAppend.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonInsert.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonMoveUp.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonMoveDown.setVisible(False)
		self.gridEditorCategory.toolBar.pushButtonDelete.setVisible(False)
		#
		self.gridEditorCategory.dataGrid.setAppendByKeyboard(False)
	def _build_code_grid(self):
		self.gridEditorCode.dataGrid.setColumnWidth(0, 150)
		self.gridEditorCode.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.gridEditorCode.dataGrid.horizontalHeader().setHighlightSections(False)
		self.gridEditorCode.dataGrid.verticalHeader().setVisible(False)

		#
		self.gridEditorCode.toolBar.pushButtonRefresh.setVisible(False)

	def __set_accessible_names(self):
		self.gridEditorCategory.setAccessibleName(ID_SVYCAT)
		self.lineEditConcreteCode.setAccessibleName(ID_CONCCODE)
		self.lineEditConcreteDensity.setAccessibleName(ID_CONCDENS)

	def __retranslate_ui(self):
		self.groupBoxCategory.setTitle("Маркшейдерские коды")
		self.groupBoxConcrete.setTitle("Бетон")
		self.labelConcreteCode.setText("Код")
		self.labelConcreteDensity.setText("Плотность")



	#
	def selectCategory(self, iRow):
		catGrid = self.gridEditorCategory.dataGrid
		codeGrid = self.gridEditorCode.dataGrid

		catGrid.itemSelectionChanged.disconnect()

		# insert into code grid
		codeGrid.itemChanged.disconnect()
		#
		codeGrid.removeAll()
		for i, val in enumerate(self._categoryList[iRow][1]):
			self.appendCode(val)
		codeGrid.itemChanged.connect(self.updateCodeList)

		catGrid.itemSelectionChanged.connect(self.selectCurrentCategory)
	def selectCurrentCategory(self):
		codeGrid = self.gridEditorCategory.dataGrid
		iRow = codeGrid.currentIndex().row()
		if iRow > -1:
			self.selectCategory(iRow)
	def updateCategory(self):
		codeGrid = self.gridEditorCode.dataGrid
		catGrid = self.gridEditorCategory.dataGrid

		nRows = catGrid.rowCount()
		nCols = catGrid.columnCount()
		for iRow in range(nRows):
			for iCol in range(nCols):
				item = catGrid.item(iRow, iCol)
				if item:
					self._categoryList[iRow][0][iCol] = item.text()
					if is_blank(item.text()):
						codeGrid.removeAll()
						self._categoryList[iRow][1] = []



	#
	def codeGridKeyPressEvent(self, event):
		grid = self.gridEditorCode.dataGrid
		super(type(grid), DataGrid).keyPressEvent(grid, event)

		key = event.key()

		# Delete and Backspace
		if key == QtCore.Qt.Key_Delete or key == QtCore.Qt.Key_Backspace:
			for index in grid.selectedIndexes():
				item = grid.itemFromIndex(index)
				if not item.isLocked():
					item.setText("")

		# Ctrl+Delete
		if event.modifiers() & QtCore.Qt.ControlModifier and key == QtCore.Qt.Key_Delete and grid._deleteByKeyboard:
			self.removeSelectedCodes()

		# return
		if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
			grid.clearSelection()
			#
			currIndex = grid.currentIndex()
			nRows = grid.rowCount()
			# append row if the last row is a current row
			iRow, iCol = currIndex.row(), currIndex.column()
			if iRow == nRows-1 and grid._appendByKeyboard:
				self.appendCode()
			# select cell
			if currIndex.model():		grid.setCurrentCell(iRow+1, iCol)
			else:						grid.setCurrentCell(0,0)
	def appendCode(self, values = ""):
		self.insertCode(self.gridEditorCode.dataGrid.rowCount(), values)
	def insertCode(self, iRow, values = ""):
		catGrid = self.gridEditorCategory.dataGrid
		codeGrid = self.gridEditorCode.dataGrid
		#
		currIndex = catGrid.currentIndex()
		itemCat = catGrid.item(currIndex.row(), 1)
		currCat = itemCat.text() if itemCat else None
		if not itemCat or not currCat or is_blank(currCat):
			qtmsgbox.show_error(self, "Ошибка", "Введите код категории")
			catGrid.setFocus()
			return
		#
		if not iRow:
			selectedIndexes = codeGrid.selectedIndexes()
			if not selectedIndexes:	iRow = 0
			else:					iRow = codeGrid.getRowsFromIndexes(selectedIndexes, reverse = False)[0]

		DataGrid.insertRow(codeGrid, iRow)
		for iCol in range(codeGrid.columnCount()):
			item = codeGrid.item(iRow, iCol)
			if values and values[iCol]:
				item.setText(values[iCol])

	def clearCodes(self):
		codeGrid = self.gridEditorCode.dataGrid
		codeGrid.itemChanged.disconnect()
		DataGrid.clear(codeGrid)
		codeGrid.itemChanged.connect(self.updateCodeList)
		self.updateCodeList()
	def removeCode(self, iRow):
		codeGrid = self.gridEditorCode.dataGrid
		#
		codeGrid.itemChanged.disconnect()
		DataGrid.removeRow(codeGrid, iRow)
		codeGrid.itemChanged.connect(self.updateCodeList)
		#
		self.updateCodeList()
	def removeSelectedCodes(self):
		codeGrid = self.gridEditorCode.dataGrid
		#
		selectedIndexes = codeGrid.selectedIndexes()
		if not selectedIndexes:
			return
		iRows = codeGrid.getRowsFromIndexes(selectedIndexes, reverse = True)
		for iRow in iRows:
			self.removeCode(iRow)
	def moveUpSelectedCodes(self):
		codeGrid = self.gridEditorCode.dataGrid
		codeGrid.itemChanged.disconnect()
		DataGrid.moveSelectedRowsUp(codeGrid)
		codeGrid.itemChanged.connect(self.updateCodeList)
		self.updateCodeList()
	def moveDownSelectedCodes(self):
		codeGrid = self.gridEditorCode.dataGrid
		codeGrid.itemChanged.disconnect()
		DataGrid.moveSelectedRowsDown(codeGrid)
		codeGrid.itemChanged.connect(self.updateCodeList)
		self.updateCodeList()

	def updateCodeList(self):
		catGrid = self.gridEditorCategory.dataGrid
		codeGrid = self.gridEditorCode.dataGrid
		#
		currCode = catGrid.currentIndex().row()
		if currCode < 0:
			return

		self._categoryList[currCode][1] = []
		for iRow in range(codeGrid.rowCount()):
			self._categoryList[currCode][1].append([])
			for iCol in range(codeGrid.columnCount()):
				item = codeGrid.item(iRow, iCol)
				if item:
					self._categoryList[currCode][1][-1].append(item.text())

	def categoryList(self):
		return self._categoryList
	def setCategoryList(self, d):
		catGrid = self.gridEditorCategory.dataGrid
		codeGrid = self.gridEditorCode.dataGrid

		catGrid.itemSelectionChanged.disconnect()
		catGrid.itemChanged.disconnect()
		codeGrid.itemChanged.disconnect()

		catGrid.removeAll()
		codeGrid.removeAll()

		self._categoryList = d
		for i, category in enumerate(d):
			catGrid.item(i, 1).setText(category[0][1])

		catGrid.itemSelectionChanged.connect(self.selectCurrentCategory)
		codeGrid.itemChanged.connect(self.updateCodeList)
		catGrid.itemChanged.connect(self.updateCategory)
		
		if self._categoryList:
			catGrid.setCurrentCell(0, 1)
	