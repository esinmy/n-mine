from PyQt4 import QtCore, QtGui

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.GridEditor import *

from utils.validation import is_blank
from utils.constants import *
from utils.qtutil import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class DsnAdvancedSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(DsnAdvancedSettingsFrame, self).__init__(*args, **kw)

		self.__create_variables()
		self.__create_widgets()
		self.__set_accessible_names()
		self.__create_bindings()
		
		self.__grid_widgets()
		
	def __create_variables(self):
		self._categoryList = []
	def __create_widgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setAlignment(QtCore.Qt.AlignTop)
		self.groupBoxMineNamingParams = QtGui.QGroupBox(self)
		self.groupBoxMineNamingParams.setTitle("Методика формирование имени выработки")
		self.groupBoxMineNamingParams.setMaximumHeight(80)
		self.layoutMineNamingParams = QtGui.QGridLayout(self.groupBoxMineNamingParams)
		self.layoutMineNamingParams.setSpacing(5)

		self.radioButton1 = QtGui.QRadioButton(self.groupBoxMineNamingParams)
		self.radioButton1.setText("Распоряжение № ЗФ-58/86-р-а")
		self.radioButton1.setChecked(True)
		
		self.radioButton2 = QtGui.QRadioButton(self.groupBoxMineNamingParams)
		self.radioButton2.setText("Письмо № ГМК/12695-исх от 16.08.2017")
		self.radioButton2.setChecked(False)

	def __create_bindings(self):
		pass
	def __grid_widgets(self):
		self.setLayout(self.layoutMain)

		# Параметры привязки выработок
		self.layoutMain.addWidget(self.groupBoxMineNamingParams)
		self.layoutMineNamingParams.addWidget(self.radioButton1,1,1)
		self.layoutMineNamingParams.addWidget(self.radioButton2,2,1)

	

	def __set_accessible_names(self):
		self.radioButton1.setAccessibleName(ID_DSN_IS_MINENAMING1)
		self.radioButton2.setAccessibleName(ID_DSN_IS_MINENAMING2)	




