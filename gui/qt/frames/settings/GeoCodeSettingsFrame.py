from PyQt4 import QtCore, QtGui

import gui.qt.dialogs.messagebox as qtmsgbox
from gui.qt.widgets.GridEditor import *
from gui.qt.dialogs.SelectFromListDialog import SelectFromListDialog

from mm.mmutils import MicromineFile

from utils.qtutil import *
from utils.validation import DoubleValidator, is_blank, StringValueValidator
from utils.constants import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class MasterGridEditor(GridEditor):
	"""
		Добавляет в классы наследники GridEditor функционал мастер грида над слайв гридами в  QtGui.QStackedWidget
	"""
	def __init__(self, parent, columns = [], widgets = [],  *args, **kw):

		super(MasterGridEditor, self).__init__(parent, columns, widgets, *args, **kw)
		
		self.__createVariables()
		#self.build()

	def __createVariables(self):
		self._stackedIndexes = None

	def setSlaveStackedWidget(self, stackedWidget):
		self._stackedIndexes = stackedWidget

	def appendRow(self):
		nRows = self.dataGrid.rowCount()
		self.insertRow(nRows)
		return nRows
	def clear(self):
		DataGrid.clear(self.dataGrid)
		for i in range(self._stackedIndexes.count(), -1, -1):
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(i))	
		#
		self.appendRow()
	def insertRow(self, iRow):
		DataGrid.insertRow(self.dataGrid, iRow)

	def removeSelectedRows(self):
		selectedRows = sorted(self.dataGrid.getRowsFromIndexes(self.dataGrid.selectedIndexes()), reverse = True)
		DataGrid.removeSelectedRows(self.dataGrid)
		#
		for iRow in selectedRows:
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(iRow))
		#
		if self.dataGrid.rowCount() == 0:
			self.appendRow()
	def moveSelectedRowsUp(self):
		selectedRows = sorted(self.dataGrid.getRowsFromIndexes(self.dataGrid.selectedIndexes()))
		DataGrid.moveSelectedRowsUp(self.dataGrid)
		#
		if selectedRows[0] == 0:
			return
		#
		for iRow in selectedRows:
			self._stackedIndexes.insertWidget(iRow-1, self._stackedIndexes.widget(iRow))
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(iRow+1))
	def moveSelectedRowsDown(self):
		selectedRows = sorted(self.dataGrid.getRowsFromIndexes(self.dataGrid.selectedIndexes()), reverse = True)
		DataGrid.moveSelectedRowsDown(self.dataGrid)
		#
		nRows = self.dataGrid.rowCount()
		if selectedRows[0] == nRows-1:
			return
		#
		for iRow in selectedRows:
			self._stackedIndexes.insertWidget(iRow+2, self._stackedIndexes.widget(iRow))
			self._stackedIndexes.removeWidget(self._stackedIndexes.widget(iRow))


class OreCodeGridEditor(MasterGridEditor):
	def __init__(self, parent):
		super(OreCodeGridEditor, self).__init__(parent,  
												["Материал", "Описание", "Геологическая разность"],
												[None, None, QtGui.QComboBox]
												)

		self.build()

	def __createVariables(self):
		pass

	def build(self):
		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)
		self.dataGrid.setColumnWidth(0, 100)
		self.dataGrid.setColumnWidth(1, 200)
		self.toolBar.pushButtonRefresh.setVisible(False)

	def insertRow(self, iRow):
		super(OreCodeGridEditor, self).insertRow(iRow)
		#self.dataGrid.cellWidget(iRow, 2).addItems(["Рудные породы", "Нерудные породы", "Вмещающие породы", "Прочее"])
		self.dataGrid.cellWidget(iRow, 2).addItems(GEO_VARIATIONS)
		#
		self._stackedIndexes.insertWidget(iRow, IndexGridsWidget(self._stackedIndexes))


class BmIndexItemDelegate(QtGui.QStyledItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()

		editor = QtGui.QLineEdit(widget)		   
		if icol == 0:
			_validator = StringValueValidator()
			editor.setValidator(_validator)
		if icol in (1, 2):
			self._validator = DoubleValidator(0, 10, 4)
			editor.setValidator(self._validator)
		return editor
class BmIndexGridEditor(MasterGridEditor):
	def __init__(self, parent):
		super(BmIndexGridEditor, self).__init__(parent,
										  ["Индексы в БМ", "ρ", "ρ породы"]
										  )

		self.dataGrid.setColumnWidth(0, 110)
		self.dataGrid.setColumnWidth(1, 60)

		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)
		self.toolBar.pushButtonRefresh.setVisible(False)

		self.dataGrid.setItemDelegate(BmIndexItemDelegate())

	def insertRow(self, iRow):
		super(BmIndexGridEditor, self).insertRow(iRow)

		self.gridEditorElemFactor = ElemFactorGridEditor(self._stackedIndexes)
		self.gridEditorElemFactor.dataGrid.setItemDelegate(BmIndexItemDelegate())
		self._stackedIndexes.insertWidget(iRow, self.gridEditorElemFactor)



class AssayIndexGridEditor(GridEditor):
	def __init__(self, parent):
		super(AssayIndexGridEditor, self).__init__(parent, ["Индексы в опробовании"])

		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)
		self.toolBar.pushButtonRefresh.setVisible(False)


class ElemFactorGridEditor(GridEditor):
	def __init__(self, parent):
		super(ElemFactorGridEditor, self).__init__(parent, ["Элемент", "ᶄ₁", "ᶄ₂"])

		self.dataGrid.setColumnWidth(0, 70)
		self.dataGrid.setColumnWidth(1, 70)
		self.dataGrid.setToolTip("Для формулы ρ = ρ породы + (ᶄ₁*Э₁² + ᶄ₂*Э₁) + ... +(ᶄ₁*Эn² + ᶄ₂*Эn)")

		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)
		self.toolBar.pushButtonRefresh.setVisible(False)

		self.dataGrid.setItemDelegate(BmIndexItemDelegate())

class IndexGridsWidget(QtGui.QWidget):
	def __init__(self, parent):
		super(IndexGridsWidget, self).__init__(parent)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutMain = QtGui.QHBoxLayout()
		self.layoutMain.setSpacing(5)
		self.layoutMain.setMargin(0)

		self.gridEditorAssayIndex = AssayIndexGridEditor(self)

		self.stackedFactors = QtGui.QStackedWidget(self)

		self.gridEditorBmIndex = BmIndexGridEditor(self)
		self.gridEditorBmIndex.setSlaveStackedWidget(self.stackedFactors)
		self.gridEditorBmIndex.dataGrid.setItemDelegate(BmIndexItemDelegate())

		self.gridEditorElemFactor = ElemFactorGridEditor(self)
		self.gridEditorElemFactor.dataGrid.setItemDelegate(BmIndexItemDelegate())
		
	def __createBindings(self):
		self.gridEditorBmIndex.dataGrid.itemSelectionChanged.connect(self.on_gridEditorBmIndex_SelectionChanged)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)
		self.layoutMain.addWidget(self.gridEditorAssayIndex)
		self.layoutMain.addWidget(self.gridEditorBmIndex)

		self.layoutMain.addWidget(self.stackedFactors)
		self.stackedFactors.addWidget(self.gridEditorElemFactor)
		#

		SettingsWindow_objects = list(filter(lambda x: type(x).__name__ == "SettingsWindow", iterWidgetParents(self.gridEditorAssayIndex)))
		if not SettingsWindow_objects:
			raise Exception("Не найдено окно настроек. Что-то пошло не так.")

		self.gridEditorAssayIndex.dataGrid.bindColumnDoubleClick(0, SettingsWindow_objects[0].askAssayIndex)

	def on_gridEditorBmIndex_SelectionChanged(self):
		iRow = self.gridEditorBmIndex.dataGrid.currentIndex().row()
		self.stackedFactors.setCurrentIndex(iRow)

class GeoCodeSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(GeoCodeSettingsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

		self.__setAccessibleNames()
	def __createVariables(self):
		pass

	def __createWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout()
		self.layoutMain.setSpacing(5)

		#
		self.groupBoxCategory = QtGui.QGroupBox(self)
		self.groupBoxCategory.setTitle("Геологические разности")
		self.layoutCategory = QtGui.QVBoxLayout()
		self.layoutCategory.setSpacing(5)
		#
		self.stackedIndexes = QtGui.QStackedWidget(self.groupBoxCategory)
		self.indexGrids = IndexGridsWidget(self.stackedIndexes)
		#
		self.gridEditorCodes = OreCodeGridEditor(self.groupBoxCategory)
		self.gridEditorCodes.setSlaveStackedWidget( self.stackedIndexes)
		self.gridEditorCodes.getValue = self.values
		self.gridEditorCodes.setValue = self.setValues

		#
		self.layoutIndex = QtGui.QHBoxLayout()
		self.layoutIndex.setSpacing(5)
		self.labelIndexField = QtGui.QLabel(self.groupBoxCategory)
		self.labelIndexField.setText("Поле индекса в файле опробования")
		self.labelIndexField.setMinimumSize(QtCore.QSize(100, 0))
		self.labelIndexField.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditIndexField = QtGui.QLineEdit(self.groupBoxCategory)
		self.lineEditIndexField.setMaximumWidth(100)

	def __createBindings(self):
		self.gridEditorCodes.dataGrid.itemSelectionChanged.connect(self.oreCodeSelectionChanged)

	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxCategory)

		self.groupBoxCategory.setLayout(self.layoutCategory)
		self.layoutCategory.addWidget(self.gridEditorCodes)

		self.layoutCategory.addLayout(self.layoutIndex)
		self.layoutIndex.addWidget(self.labelIndexField)
		self.layoutIndex.addWidget(self.lineEditIndexField)

		self.layoutCategory.addWidget(self.stackedIndexes)
		self.stackedIndexes.addWidget(self.indexGrids)

	def __setAccessibleNames(self):
		self.lineEditIndexField.setAccessibleName(ID_IDXFLD)
		self.gridEditorCodes.setAccessibleName(ID_GEOCODES)

	def oreCodeSelectionChanged(self):
		iRow = self.gridEditorCodes.dataGrid.currentIndex().row()
		self.stackedIndexes.setCurrentIndex(iRow)

	def setValues(self, values):
		codeGE = self.gridEditorCodes
		codeGE.clear()
		nValues = len(values)
		for iRow, ((oreCode, oreDescription, oreType), indexes) in enumerate(values):
			codeGE.dataGrid.item(iRow, 0).setText(oreCode)
			codeGE.dataGrid.item(iRow, 1).setText(oreDescription)
			codeGE.dataGrid.cellWidget(iRow, 2).setCurrentIndex(oreType)
			#
			assayGE = self.stackedIndexes.widget(iRow).gridEditorAssayIndex
			assayGE.dataGrid.from_list([[index] if type(index) != list else index for index in indexes["ASSAY"]])
			#


			bmGE = self.stackedIndexes.widget(iRow).gridEditorBmIndex
			bmGE.clear()
			bmGE_nValues = len(indexes["BM"])
			for iRow_bmGE, ((bmIndex, pIndex, pWst),factors) in enumerate(indexes["BM"]):
				bmGE.dataGrid.item(iRow_bmGE, 0).setText(bmIndex)
				bmGE.dataGrid.item(iRow_bmGE, 1).setText(pIndex)
				bmGE.dataGrid.item(iRow_bmGE, 2).setText(pWst)

				factorGE = self.stackedIndexes.widget(iRow).stackedFactors.widget(iRow_bmGE) 
				factorGE.dataGrid.from_list([[index] if type(index) != list else index for index in factors["FACTOR"]])

				if iRow_bmGE < bmGE_nValues-1:
					bmGE.appendRow()


			if iRow < nValues-1:
				codeGE.appendRow()



	def values(self):
		ret = []
		for iRow, row in enumerate(self.gridEditorCodes.dataGrid.to_list()):
			assayGE = self.stackedIndexes.widget(iRow).gridEditorAssayIndex
			bmGE = self.stackedIndexes.widget(iRow).gridEditorBmIndex
			bmGE_list = bmGE.dataGrid.to_list()
			bmGE_factorGE_list = []
			for iRow_bmGE, row_bmGE in enumerate(bmGE_list):
				factorGE = self.stackedIndexes.widget(iRow).stackedFactors.widget(iRow_bmGE)
				factors = factorGE.dataGrid.to_list()
				bmGE_factorGE_list.append([row_bmGE, {"FACTOR": factors}]) 

			indexes = {	"ASSAY": assayGE.dataGrid.to_list(),
						"BM": bmGE_factorGE_list}
			ret.append([row, indexes])
		return ret
