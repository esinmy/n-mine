from PyQt4 import QtCore, QtGui

from gui.qt.widgets.GridEditor import *

from utils.qtutil import *
from utils.validation import DoubleValidator , StringValueValidator
from utils.constants import *

from mm.mmutils import MicromineFile
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class RadiusCellValidator(QtGui.QStyledItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()

		editor = QtGui.QLineEdit(widget)
		if icol == 0:
			self._validator = DoubleValidator(0, 9999, 3)
		editor.setValidator(self._validator)
		return editor

class InterpolationCellValidator(QtGui.QStyledItemDelegate):
	def createEditor(self, widget, option, index):
		if not index.isValid():
			return 0
		icol = index.column()

		editor = QtGui.QLineEdit(widget)
		if icol == 0:
			_validator = StringValueValidator()
			editor.setValidator(_validator)
		if icol == 4:
			self._validator = DoubleValidator(0, 100000, 3)
		editor.setValidator(self._validator)
		return editor

class InterpolationGridEditor(GridEditor):
	def __init__(self, *args, **kw):
		super(InterpolationGridEditor, self).__init__(*args, **kw)

		self.__createVariables()
		self.build()
		
	def __createVariables(self):
		self._gradeUnits = ["Процент (т)", "г/т"]
		self._metalUnits = ["Тонны", "Унции", "Граммы", "Карат", "Фунты", "пеннивейты", "Килограммы"]

	def build(self):
		self.dataGrid.horizontalHeader().setMinimumSectionSize(10)
		self.dataGrid.horizontalHeader().setStretchLastSection(True)
		self.dataGrid.verticalHeader().setVisible(False)

		self.dataGrid.setColumnWidth(0, 70)
		self.dataGrid.setColumnWidth(1, 100)
		self.dataGrid.setColumnWidth(2, 100)
		self.dataGrid.setColumnWidth(3, 120)
		self.dataGrid.setItemDelegate(InterpolationCellValidator())

		self.toolBar.pushButtonRefresh.setVisible(False)

	def keyPressEvent(self, event):
		grid = self.dataGrid
		super(type(grid), DataGrid).keyPressEvent(grid, event)

		key = event.key()

		# Delete and Backspace
		if key == QtCore.Qt.Key_Delete or key == QtCore.Qt.Key_Backspace:
			for index in grid.selectedIndexes():
				item = grid.itemFromIndex(index)
				if not item.isLocked():
					item.setText("")

		# Ctrl+Delete
		if event.modifiers() & QtCore.Qt.ControlModifier and key == QtCore.Qt.Key_Delete and grid._deleteByKeyboard:
			grid.removeSelectedRows()

		# return
		if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
			grid.clearSelection()
			#
			currIndex = grid.currentIndex()
			nRows = grid.rowCount()
			# append row if the last row is a current row
			iRow, iCol = currIndex.row(), currIndex.column()
			if iRow == nRows-1 and grid._appendByKeyboard:
				self.appendElement()
			# select cell
			if currIndex.model():		grid.setCurrentCell(iRow+1, iCol)
			else:						grid.setCurrentCell(0,0)

	def appendElement(self, values = []):
		elemGrid = self.dataGrid
		self.insertElement(elemGrid.rowCount(), values)
	def insertElement(self, iRow, values = []):
		elemGrid = self.dataGrid

		if type(iRow) == bool:
			selectedIndexes = elemGrid.selectedIndexes()
			if not selectedIndexes:		iRow = 0
			else:						iRow = elemGrid.getRowsFromIndexes(selectedIndexes, reverse = False)[0]

		DataGrid.insertRow(elemGrid, iRow)
		# первый столбец
		item = elemGrid.item(iRow, 0)
		item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
		if values:						item.setText(values[0])
		# второй столбец
		widget = elemGrid.cellWidget(iRow, 1)
		units = []
		for units in self._gradeUnits:	widget.addItem(units)
		if values:						setWidgetValue(widget, values[1])
		else:							setWidgetValue(widget, 0)
		# третий столбец
		widget = elemGrid.cellWidget(iRow, 2)
		for units in self._metalUnits:	widget.addItem(units)
		if values:						setWidgetValue(widget, values[2])
		else:							setWidgetValue(widget, 0)
		try:
			# четвертый столбец
			widget = elemGrid.cellWidget(iRow, 3)
			if values:					setWidgetValue(widget, values[3])
		except:
			pass
		# пятый столбец
		elemGrid.item(iRow, 4).setText(values[4])

	def setElements(self, values):
		elemGrid = self.dataGrid
		elemGrid.removeAll()
		for row in values:
			self.appendElement(row)

class RaduisGridEditor(GridEditor):
	def __init__(self, parent):
		super(RaduisGridEditor, self).__init__(parent, ["Радиус"])

		self.dataGrid.verticalHeader().setVisible(False)
		self.dataGrid.horizontalHeader().setStretchLastSection(True)	
		self.dataGrid.setItemDelegate(RadiusCellValidator())

		self.toolBar.pushButtonRefresh.setVisible(False)
		self.toolBar.pushButtonAppend.setVisible(False)
		self.toolBar.pushButtonInsert.setVisible(False)

	def hasCorrectInput(self):
		values = self.dataGrid.to_list()
		if not values or [""] in values:
			return
		return True

class InterpolationSearchGroupBox(QtGui.QGroupBox):
	def __init__(self, parent):
		super(InterpolationSearchGroupBox, self).__init__(parent)

		self.setTitle("Параметры поиска")

		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutSearchParams = QtGui.QFormLayout()
		self.labelSearchParamsSectors = QtGui.QLabel(self)
		self.labelSearchParamsSectors.setText("Секторы")
		self.labelSearchParamsSectors.setMinimumWidth(100)
		self.labelSearchParamsSectors.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.comboBoxSearchParamsSectors = QtGui.QComboBox(self)
		self.comboBoxSearchParamsSectors.setMaximumWidth(100)
		self.comboBoxSearchParamsSectors.addItems(["Один", "Два", "Четыре", "Восемь", "Шестнадцать"])
		self.labelSearchParamsMaxPoints = QtGui.QLabel(self)
		self.labelSearchParamsMaxPoints.setText("Мин. кол-во точек")
		self.labelSearchParamsMaxPoints.setMinimumWidth(100)
		self.labelSearchParamsMaxPoints.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditSearchParamsMaxPoints = QtGui.QLineEdit(self)
		self.lineEditSearchParamsMaxPoints.setMaximumWidth(100)
		self.lineEditSearchParamsMaxPoints.setValidator(QtGui.QIntValidator())

		self.lineEditSearchParamsMaxPoints.setMaximumWidth(100)
		self.lineEditSearchParamsMaxPoints.setAlignment(QtCore.Qt.AlignRight)
		self.labelSearchParamsMinPoints = QtGui.QLabel(self)
		self.labelSearchParamsMinPoints.setText("Макс. кол-во точек")
		self.labelSearchParamsMinPoints.setMinimumWidth(100)
		self.labelSearchParamsMinPoints.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditSearchParamsMinPoints = QtGui.QLineEdit(self)
		self.lineEditSearchParamsMinPoints.setMaximumWidth(100)
		self.lineEditSearchParamsMinPoints.setValidator(QtGui.QIntValidator())

		self.lineEditSearchParamsMinPoints.setMaximumWidth(100)
		self.lineEditSearchParamsMinPoints.setAlignment(QtCore.Qt.AlignRight)
	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutSearchParams)

		self.layoutSearchParams.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelSearchParamsSectors)
		self.layoutSearchParams.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBoxSearchParamsSectors)
		self.layoutSearchParams.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSearchParamsMaxPoints)
		self.layoutSearchParams.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditSearchParamsMaxPoints)
		self.layoutSearchParams.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSearchParamsMinPoints)
		self.layoutSearchParams.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditSearchParamsMinPoints)

class InterpolationAxesGroupBox(QtGui.QGroupBox):
	def __init__(self, parent):
		super(InterpolationAxesGroupBox, self).__init__(parent)
		self.setTitle("Оси")
		
		self.__createVariables()
		self.__createWidgets()
		self.__createBindings()
		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		self.layoutSearchAxes = QtGui.QFormLayout()

		self.labelSearchAxesFactor1 = QtGui.QLabel(self)
		self.labelSearchAxesFactor1.setText("Фактор оси 1")
		self.labelSearchAxesFactor1.setMinimumWidth(100)
		self.labelSearchAxesFactor1.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditSearchAxesFactor1 = QtGui.QLineEdit(self)
		self.lineEditSearchAxesFactor1.setMaximumWidth(100)
		self.lineEditSearchAxesFactor1.setValidator(QtGui.QIntValidator())

		self.lineEditSearchAxesFactor1.setMaximumWidth(100)
		self.lineEditSearchAxesFactor1.setAlignment(QtCore.Qt.AlignRight)
		self.labelSearchAxesFactor2 = QtGui.QLabel(self)
		self.labelSearchAxesFactor2.setText("Фактор оси 2")
		self.labelSearchAxesFactor2.setMinimumWidth(100)
		self.labelSearchAxesFactor2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditSearchAxesFactor2 = QtGui.QLineEdit(self)
		self.lineEditSearchAxesFactor2.setMaximumWidth(100)
		self.lineEditSearchAxesFactor2.setValidator(QtGui.QIntValidator())

		self.lineEditSearchAxesFactor2.setMaximumWidth(100)
		self.lineEditSearchAxesFactor2.setAlignment(QtCore.Qt.AlignRight)
		self.labelSearchAxesFactor3 = QtGui.QLabel(self)
		self.labelSearchAxesFactor3.setText("Фактор оси 3")
		self.labelSearchAxesFactor3.setMinimumWidth(100)
		self.labelSearchAxesFactor3.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.lineEditSearchAxesFactor3 = QtGui.QLineEdit(self)
		self.lineEditSearchAxesFactor3.setMaximumWidth(100)
		self.lineEditSearchAxesFactor3.setValidator(QtGui.QIntValidator())

		self.lineEditSearchAxesFactor3.setMaximumWidth(100)
		self.lineEditSearchAxesFactor3.setAlignment(QtCore.Qt.AlignRight)
	def __createBindings(self):
		pass
	def __gridWidgets(self):
		self.setLayout(self.layoutSearchAxes)

		self.layoutSearchAxes.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelSearchAxesFactor1)
		self.layoutSearchAxes.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditSearchAxesFactor1)
		self.layoutSearchAxes.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelSearchAxesFactor2)
		self.layoutSearchAxes.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditSearchAxesFactor2)
		self.layoutSearchAxes.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelSearchAxesFactor3)
		self.layoutSearchAxes.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditSearchAxesFactor3)


class InterpolationSettingsFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(InterpolationSettingsFrame, self).__init__(*args, **kw)

		self.__createVariables()
		self.__createWidgets()
		self.__setAccessibleNames()
		self.__createBindings()

		self.__gridWidgets()

	def __createVariables(self):
		pass
	def __createWidgets(self):
		
		self.layoutMain = QtGui.QVBoxLayout()
		self.groupBoxParams = QtGui.QGroupBox(self)
		self.groupBoxParams.setTitle("Параметры IDW")

		self.layoutParams = QtGui.QHBoxLayout()
		self.layoutParams.setSpacing(5)

		self.gridEditorElements = InterpolationGridEditor(self.groupBoxParams, ["Элемент", "Содержание", "Масса", "Дополнять нулями", "Ураган"],
										[None, QtGui.QComboBox, QtGui.QComboBox, CheckBoxCellWidget, None])
		self.gridEditorElements.getValue = self.gridEditorElements.dataGrid.to_list
		self.gridEditorElements.setValue = self.gridEditorElements.setElements
		self.gridEditorElements.setMinimumWidth(470)

		self.gridEditorRadius = RaduisGridEditor(self.groupBoxParams)
		self.gridEditorRadius.getValue = self.gridEditorRadius.dataGrid.to_list
		self.gridEditorRadius.setValue = self.gridEditorRadius.dataGrid.from_list
		self.gridEditorRadius.setMaximumWidth(120)

		#
		self.layoutSearch = QtGui.QHBoxLayout()
		#
		self.groupBoxInterpolationSearch = InterpolationSearchGroupBox(self)
		self.groupBoxInterpolationAxes = InterpolationAxesGroupBox(self)
		
	def __createBindings(self):
		self.gridEditorElements.toolBar.pushButtonAppend.clicked.disconnect()
		self.gridEditorElements.toolBar.pushButtonInsert.clicked.disconnect()
		self.gridEditorElements.toolBar.pushButtonAppend.clicked.connect(self.gridEditorElements.appendElement)
		self.gridEditorElements.toolBar.pushButtonInsert.clicked.connect(self.gridEditorElements.insertElement)
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addWidget(self.groupBoxParams)
		self.groupBoxParams.setLayout(self.layoutParams)
		self.layoutParams.addWidget(self.gridEditorElements)
		self.layoutParams.addWidget(self.gridEditorRadius)

		self.layoutMain.addLayout(self.layoutSearch)
		self.layoutSearch.addWidget(self.groupBoxInterpolationSearch)
		self.layoutSearch.addWidget(self.groupBoxInterpolationAxes)

	def __setAccessibleNames(self):
		self.gridEditorElements.setAccessibleName(ID_IDWELEM)
		self.gridEditorRadius.setAccessibleName(ID_IDWRADIUS)

		self.groupBoxInterpolationSearch.comboBoxSearchParamsSectors.setAccessibleName(ID_IDWSECTOR)
		self.groupBoxInterpolationSearch.lineEditSearchParamsMaxPoints.setAccessibleName(ID_IDWMINPTS)
		self.groupBoxInterpolationSearch.lineEditSearchParamsMinPoints.setAccessibleName(ID_IDWMAXPTS)
		self.groupBoxInterpolationAxes.lineEditSearchAxesFactor1.setAccessibleName(ID_IDWFACTOR1)
		self.groupBoxInterpolationAxes.lineEditSearchAxesFactor2.setAccessibleName(ID_IDWFACTOR2)
		self.groupBoxInterpolationAxes.lineEditSearchAxesFactor3.setAccessibleName(ID_IDWFACTOR3)

