# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

from gui.qt.dialogs.messagebox import *
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class WireframeSelectSetFrame(QtGui.QWidget):
	def __init__(self, *args, **kw):
		super(WireframeSetSelectFrame, self).__init__(*args, **kw)

		self.__create_variables()
		
		self.__create_widgets()
		self.___retranslate_ui()

		self.__create_bindings()
		self.__grid_widgets()

	def __create_variables(self):
		pass
	def __create_widgets(self):
		self.gridLayoutWidget = QtGui.QWidget(self)
		self.gridLayoutWidget.setGeometry(QtCore.QRect(0, 0, 340, 70))

		self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
		self.gridLayout.setSpacing(5)
		self.gridLayout.setSizeConstraint(QtGui.QLayout.SetFixedSize)
		self.gridLayout.setColumnMinimumWidth(0, 50)
		self.gridLayout.setColumnMinimumWidth(1, 50)
		self.gridLayout.setColumnMinimumWidth(2, 200)

		self.RadioButtonSingle = QtGui.QRadioButton()
		self.RadioButtonSingle.setChecked(True)
		self.RadioButtonSingle.toggled.connect(self.set_actions)
		self.RadioButtonSet = QtGui.QRadioButton()
		self.RadioButtonSet.toggled.connect(self.set_actions)

		self.LabelType = QtGui.QLabel(self.gridLayoutWidget)
		self.LabelType.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.LineEditType = QtGui.QLineEdit(self.gridLayoutWidget)
		self.LabelName = QtGui.QLabel(self.gridLayoutWidget)
		self.LabelName.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.LineEditName = QtGui.QLineEdit(self.gridLayoutWidget)

	def __create_bindings(self):
		self.LineEditType.mouseDoubleClickEvent = lambda e: self._select_type(e, self.LineEditType)
		self.LineEditName.mouseDoubleClickEvent = lambda e: self._select_name(e, self.LineEditName)
	def __grid_widgets(self):
		self.gridLayout.addWidget(self.RadioButtonSingle, 0, 0, 1, 1)
		self.gridLayout.addWidget(self.RadioButtonSet, 1, 0, 1, 1)
		self.gridLayout.addWidget(self.LabelName, 1, 1, 1, 1)
		self.gridLayout.addWidget(self.LabelType, 0, 1, 1, 1)
		self.gridLayout.addWidget(self.LineEditType, 0, 2, 1, 1)
		self.gridLayout.addWidget(self.LineEditName, 1, 2, 1, 1)

	def __retranslate_ui(self):
		self.RadioButtonSingle.setText("Один")
		self.RadioButtonSet.setText("Набор")
		self.LabelName.setText("Имя")
		self.LabelType.setText("Тип")

	def _get_wireframe_names(self):
		tritype = self.LineEditType.text()
		try:
			names = get_wireframe_names(tritype)
		except Exception as e:
			show_error(self, "Ошибка", "Невозможно считать имена каркасов. %s" % str(e))
			return
		return names

	def _select_name(self, event, sender):
		if not self.check_type():
			return
		trinames = self._get_wireframe_names()
		if trinames is None:
			return
		dialog = SelectFromListDialog(self)
		dialog.setWindowTitle("Выберите каркас")
		dialog.LabelTitle.setText("Выберите каркас")
		dialog.set_values(trinames)
		dialog.exec()
		values = dialog.get_values()
		if values:
			self.LineEditName.setText(", ".join(values))
	def _select_type(self, event, sender):
		filetypes = "КАРКАС (*.TRIDB)"
		path = QtGui.QFileDialog.getOpenFileName(self, "Выбрать файл", "", filetypes).replace("/", "\\")
		if not path.replace(" ", ""):
			return
		sender.setText(path)

	def set_actions(self):
		if self.RadioButtonSet.isChecked():
			self.LabelType.setDisabled(True)
			self.LineEditType.setDisabled(True)
		else:
			self.LabelType.setDisabled(False)
			self.LineEditType.setDisabled(False)

	def check_type(self):
		if self.RadioButtonSet.isChecked():
			return
		tritype = self.LineEditType.text()
		if not tritype.replace(" ", ""):
			show_error(self, "Ошибка", "Выберите тип")
			self.LineEditType.setFocus()
			return False
		if not os.path.exists(tritype):
			show_error(self, "Ошибка", "Файл '%s' не существует" % tritype)
			self.LineEditType.setFocus()
			return False
		return True
	def check_name(self):
		if self.RadioButtonSingle.isChecked():
			triname = self.LineEditName.text()
			if not triname.replace(" ", ""):
				show_error(self, "Ошибка", "Выберите имя")
				self.LineEditName.setFocus()
				return False
			if not triname in self._get_wireframe_names():
				show_error(self, "Ошибка", "Каркас '%s' не найден" % triname)
				self.LineEditName.setFocus()
				return False
			return True
		else:
			return
	def check(self):
		if not self.check_type():
			return
		if not self.check_name():
			return
		return True

class WireframeSingleSelectFrame(WireframeSetSelectFrame):
	def __init__(self, *args, **kw):
		super(WireframeSingleSelectFrame, self).__init__(*args, **kw)

		self.__create_variables()
		self.__create_widgets()
		self.__create_bindings()
		self.__grid_widgets()

	def __create_variables(self):
		pass
	def __create_widgets(self):
		self.RadioButtonSingle.setVisible(False)
		self.RadioButtonSet.setVisible(False)

		self.gridLayout.setColumnMinimumWidth(0, 50)
		self.gridLayout.setColumnMinimumWidth(1, 50 + self.gridLayout.spacing()*2-1)
		self.gridLayout.setColumnMinimumWidth(2, 200)
	def __create_bindings(self):
		pass
	def __grid_widgets(self):
		pass
