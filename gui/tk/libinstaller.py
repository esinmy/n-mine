import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as tkmsgbox

import os, pip, shutil

import gui.tk.tkwgt as tkw

class LibInstaller:
	def __init__(self, root):
		self.root = root
		self.root.title("Установка расширений для Python")
		self.root.resizable(False, False)
		self.root.protocol("WM_DELETE_WINDOW", self.close)

		self.__create_variables()
		self.__create_widgets()
		self.__create_bindings()
		self.__grid_widgets()

		self._buildLibList()

		self.screen_center()

		self.root.mainloop()
	def __create_variables(self):
		self._is_running = False
		self._process_id = None

		path = os.getcwd()
		libpath = os.path.join(path, "libs")
		guipath = os.path.join(libpath, "pyqt")
		extpath = os.path.join(libpath, "extensions")
		self._liblist = []
		self._liblist.append(["PyQt", os.path.join(guipath, "PyQt4-4.11.4-cp35-none-win_amd64.whl")])
		self._liblist.append(["PyWin32", os.path.join(extpath, "pywin32-220.1-cp35-cp35m-win_amd64.whl")])
		self._liblist.append(["Six", os.path.join(extpath, "six-1.10.0-py2.py3-none-any.whl")])
		self._liblist.append(["DateUtil", os.path.join(extpath, "python_dateutil-2.5.0-py2.py3-none-any.whl")])
		self._liblist.append(["PyTz", os.path.join(extpath, "pytz-2015.7-py2.py3-none-any.whl")])
		self._liblist.append(["NumPy", os.path.join(extpath, "numpy-1.10.4+mkl-cp35-none-win_amd64.whl")])
		self._liblist.append(["NumExpr", os.path.join(extpath, "numexpr-2.5-cp35-cp35m-win_amd64.whl")])

		self._liblist.append(["XlsxWriter", os.path.join(extpath, "XlsxWriter-0.8.4-py2.py3-none-any.whl")])
		self._liblist.append(["PyMSSQL", os.path.join(extpath, "pymssql-2.1.3-cp35-cp35m-win_amd64.whl")])
		self._liblist.append(["PyODBC", os.path.join(extpath, "pyodbc-3.0.10-cp35-none-win_amd64.whl")])
		
		self._liblist.append(["Et_XmlFile", os.path.join(extpath, "et_xmlfile-1.0.1-py2.py3-none-any.whl")])
		self._liblist.append(["jdcal", os.path.join(extpath, "jdcal-1.3-py2.py3-none-any.whl")])
		self._liblist.append(["OpenPyxl", os.path.join(extpath, "openpyxl-2.4.0-py2.py3-none-any.whl")])

		self._liblist.append(["Pandas", os.path.join(extpath, "pandas-0.17.1-cp35-none-win_amd64.whl")])
		self._liblist.append(["SciPy", os.path.join(extpath, "scipy-0.17.0-cp35-none-win_amd64.whl")])

		self._liblist.append(["Shapely", os.path.join(extpath, "Shapely-1.5.17-cp35-cp35m-win_amd64.whl")])

		self._liblist.append(["Decorator", os.path.join(extpath, "decorator-4.2.1-py2.py3-none-any.whl")])
		self._liblist.append(["Networkx", os.path.join(extpath, "networkx-2.1-py2.py3-none-any.whl")])
		self._liblist.append(["Sklearn", os.path.join(extpath, "scikit_learn-0.19.1-cp35-cp35m-win_amd64.whl")])

	def __create_widgets(self):
		self.frmTable = tkw.TableFrame(self.root)

		#
		columns = ["Имя", "Сообщение"]
		self.frmTable.table["columns"] = columns
		self.frmTable.table.column(columns[1], width = 400)
		self.frmTable.table["height"] = 20

		#
		self.frmButtons = tkw.ButtonsFrame(self.root, buttons = "Установить|Доустановить|Закрыть")
		self.frmButtons.buttons[0]["command"] = self.check
		self.frmButtons.buttons[1]["command"] = self.patch
		self.frmButtons.buttons[2]["command"] = self.root.destroy
	def __create_bindings(self):
		pass
	def __grid_widgets(self):
		padding = dict(padx = 3, pady = 2)
		self.frmTable.grid(row = 0, column = 0, sticky = "nsew", **padding)
		self.frmButtons.grid(row = 0, column = 1, sticky = "nsew", **padding)

	def _buildLibList(self):
		for row in self._liblist:
			self.frmTable.table.insert("end", [row[0], ""])

	def close(self):
		if self._is_running:
			return
		self.root.destroy()

	def screen_center(self):
		self.root.update()
		wscreen = self.root.winfo_screenwidth()
		hscreen = self.root.winfo_screenheight()
		wroot, hroot = tuple(int(_) for _ in self.root.geometry().split('+')[0].split('x'))
		self.root.geometry("+%d+%d" % ((int(wscreen/2 - wroot/2), int(hscreen/2 - hroot/2))))

	def install_libraries(self):
		def _install_libraries(i):
			table = self.frmTable.table
			children = self.frmTable.table.get_children()

			if i < len(self._liblist):
				try:
					libpath = self._liblist[i][1]
					pip.main(["install", libpath])
					table.set(children[i], 1, "Успешно установлено")
					if i < len(self._liblist) - 1:
						table.set(children[i+1], 1, "Установка")
				except Exception as e:
					table.set(children[i], 1, str(e))
				self.root.after(1, _install_libraries, i+1)
			else:
				self.root.after_cancel(self._process_id)

		self._process_id = self.root.after(1, _install_libraries, 0)

	def check(self):
		children = self.frmTable.table.get_children()

		err = None
		for i, (libname, libpath) in enumerate(self._liblist):
			if not os.path.exists(libpath):
				err = True
				tkmsgbox.showerror("Ошибка", "Библиотека '%s' не найдена" % os.path.basename(libpath))
		if err:
			return
		
		self._is_running = True
		try:						self.run()
		except Exception as e:		tkmsgbox.showerror("Ошибка", "Невозможно установить библиотеки. Ошибка: %s" % str(e))
		self._is_running = False

	def patch(self):
		try:
			import win32security
		except:
			libpath = None
			for path in os.sys.path:
				if path.lower().endswith("site-packages"):
					libpath = path
					break
			srcpath = []
			srcpath.append(os.path.join(libpath, "pywin32_system32", "pythoncom35.dll"))
			srcpath.append(os.path.join(libpath, "pywin32_system32", "pywintypes35.dll"))
			dstpath = []
			dstpath.append(os.path.join(libpath, "win32", "pythoncom35.dll"))
			dstpath.append(os.path.join(libpath, "win32", "pywintypes35.dll"))
			try:
				for src, dst in zip(srcpath, dstpath):
					shutil.copy(src, dst)
				tkmsgbox.showinfo("Выполнено", "Библиотека PyWin32 успешно доустановлена", parent = self.root)
			except Exception as e:
				tkmsgbox.showerror("Ошибка", "Невозможно установить библиотеку PyWin32. %s" % str(e), parent = self.root)

	def run(self):
		table = self.frmTable.table
		children = table.get_children()

		for i, (libame, libpath) in enumerate(self._liblist):
			table.set(children[i], 1, "В очереди")
		table.set(children[0], 1, "Установка")
		#
		self.install_libraries()




# root = tk.Tk()
# try:
# 	app = LibInstaller(root)
# 	root.mainloop()
# except Exception as e:
# 	tkmsgbox.showerror("Ошибка", str(e), parent = root)
# 	root.destroy()

