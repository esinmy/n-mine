try:	import MMpy
except:	pass

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.filedialog as dialog
import tkinter.messagebox as msgbox
import tkinter.font as tkfont

from utils.constants import *

# ======================================================================
# ============================ FUNCTIONS ===============================
# ======================================================================

def get_widget_children(wgt, allChildren = True):
	def _get_widget_children(wgt, result = []):
		if not wgt.winfo_children():
			return result
		for child in wgt.winfo_children():
			result.append(child)
			_get_widget_children(child, result)
		return result
	if allChildren:	return _get_widget_children(wgt)
	else:			return wgt.winfo_children()

def get_widget_parent(wgt, stopList = []):
	ntw = wgt.nametowidget
	parent = ntw(wgt.winfo_parent())
	if not stopList:
		return parent
	while parent and parent not in stopList:
		wgt = ntw(parent)
		parent = ntw(wgt.winfo_parent())
	return ntw(wgt.winfo_parent())

# ======================================================================
# ========================= VARIABLE CLASSES ===========================
# ======================================================================

class IntVar(tk.IntVar):
	def __init__(self, name = None, trace_command = None):
		self.__name = name
		self.__trace_command = trace_command
		tk.IntVar.__init__(self)
		if trace_command is not None:
			self.trace("w", lambda name, index, mode, sv = self: trace_command())
	@property
	def name(self):					return self.__name
	@property
	def trace_command(self):		return self._tracer
	@trace_command.setter
	def trace_command(self, cmd):
		if cmd is None:
			self.__trace_command = None
		else:
			self.__trace_command = cmd
			self.trace("w", lambda name, index, mode, sv = self: cmd())

class BooleanVar(tk.BooleanVar):
	def __init__(self, name = None, trace_command = None):
		self.__name = name
		self.__trace_command = trace_command
		tk.BooleanVar.__init__(self)
		if trace_command is not None:
			self.trace("w", lambda name, index, mode, sv = self: trace_command())
	@property
	def name(self):					return self.__name
	@property
	def trace_command(self):		return self._tracer
	@trace_command.setter
	def trace_command(self, cmd):
		if cmd is None:
			self.__trace_command = None
		else:
			self.__trace_command = cmd
			self.trace("w", lambda name, index, mode, sv = self: cmd())

class StringVar(tk.StringVar):
	def __init__(self, name = None, trace_command = None):
		self.__name = name
		self.__trace_command = trace_command
		tk.StringVar.__init__(self)
		if trace_command is not None:
			self.trace("w", lambda name, index, mode, sv = self: trace_command())
	@property
	def name(self):					return self.__name
	@property
	def trace_command(self):		return self._tracer
	@trace_command.setter
	def trace_command(self, cmd):
		if cmd is None:
			self.__trace_command = None
		else:
			self.__trace_command = cmd
			self.trace("w", lambda name, index, mode, sv = self: cmd())

# ======================================================================
# ========================== WIDGET CLASSES ============================
# ======================================================================

class Radiobutton(ttk.Radiobutton):
	def __setitem__(self, item, value):
		if item in ["var", "variable"]:		self.__var = value
		if item  == "value":				self.__value = value
		super(Radiobutton, self).__setitem__(item, value)
	def __init__(self, *args, **kw):
		if "var" in kw:				self.__var = kw["var"]
		elif "variable" in kw:		self.__var = kw["variable"]
		else:						self.__var = None
		if "value" in kw:			self.__value = kw["value"]
		ttk.Radiobutton.__init__(self, *args, **kw)
	@property
	def var(self):				return self.__var
	@property
	def is_selected(self):
		if self.__var is not None:
			return self.__var.get() == self.__value

class Checkbox(ttk.Checkbutton):
	def __setitem__(self, item, value):
		if item in ["var", "variable"]:		self.__var = value
		ttk.Checkbutton.__setitem__(self, item, value)
	def __init__(self, *args, **kw):
		if "var" in kw:				self.__var = kw["var"]
		elif "variable" in kw:		self.__var = kw["variable"]
		else:						self.__var = None
		ttk.Checkbutton.__init__(self, *args, **kw)
	@property
	def var(self):				return self.__var
	@property
	def is_checked(self):
		if self.__var is not None:
			return self.__var.get() == self["onvalue"]	
	@is_checked.setter
	def is_checked(self, val):
		if bool(val):		self.__var.set(self["onvalue"])
		else:				self.__var.set(self["offvalue"])

class Entry(ttk.Entry):
	def __setitem__(self, item, value):
		if item == "textvariable":			self.__var = value
		ttk.Entry.__setitem__(self, item, value)
	def __init__(self, *args, **kw):
		if "textvariable" in kw:	self.__var = kw["textvariable"]
		else:						self.__var = None
		ttk.Entry.__init__(self, *args, **kw)
		style = ttk.Style()
		style.layout("TEntry", [('Entry.field', {'children': [('Entry.background', {'children': [('Entry.padding', {'children': [('Entry.textarea', {'sticky': 'nswe'})],'sticky': 'nswe'})], 'sticky': 'nswe'})],'border':'1', 'sticky': 'nswe'})])
		self.bind("<Control-ocircumflex>", lambda e: self.selection_range(0, "end"))
		self.bind("<Control-igrave>", lambda e:	self.__paste_from_clipboard())
		self.bind("<Control-ntilde>", lambda e:	self.__copy_to_clipboard())
	def __copy_to_clipboard(self):
		if self.selection_present():
			self.clipboard_clear()
			self.clipboard_append(self.selection_get())
		else:
			self.clipboard_set("")
	def __paste_from_clipboard(self):
		clipboard = self.clipboard_get()
		if self.selection_present():
			self.delete(tk.SEL_FIRST, tk.SEL_LAST)
			self.insert(tk.INSERT, clipboard)
		else:
			self.insert(tk.INSERT, clipboard)
	def add_tag(self, tag):		self.bindtags(self.bindtags() + (tag,))
	def add_tags(self, tags):
		for tag in tags:
			self.add_tag(tag)
	@property
	def var(self):				return self.__var

class Combobox(ttk.Combobox):
	def __setitem__(self, item, value):
		if item == "textvariable":			self.__var = value
		ttk.Combobox.__setitem__(self, item, value)
	def __init__(self, *args, **kw):
		if "textvariable" in kw:	self.__var = kw["textvariable"]
		else:						self.__var = None
		ttk.Combobox.__init__(self, *args, **kw)
		self.bind("<Control-ocircumflex>", lambda e: self.selection_range(0, "end"))
		self.bind("<Control-igrave>", lambda e:	self.__paste_from_clipboard())
		self.bind("<Control-ntilde>", lambda e:	self.__copy_to_clipboard())
	def __copy_to_clipboard(self):
		if self.selection_present():
			self.clipboard_clear()
			self.clipboard_append(self.selection_get())
		else:
			self.clipboard_set("")
	def __paste_from_clipboard(self):
		clipboard = self.clipboard_get()
		if self.selection_present():
			self.delete(tk.SEL_FIRST, tk.SEL_LAST)
			self.insert(tk.INSERT, clipboard)
		else:
			self.insert(tk.INSERT, clipboard)
	@property
	def var(self):				return self.__var
	def add_tag(self, tag):		self.bindtags(self.bindtags() + (tag,))
	def add_tags(self, tags):
		for tag in tags:
			self.add_tag(tag)

class Labelframe(ttk.Labelframe):
	def __init__(self, *args, **kw):
		ttk.Labelframe.__init__(self, *args, **kw)

class Frame(ttk.Frame):
	def __init__(self, *args, **kw):
		ttk.Frame.__init__(self, *args, **kw)

class Label(ttk.Label):
	def __init__(self, *args, **kw):
		ttk.Label.__init__(self, *args, **kw)

class Button(ttk.Button):
	def __init__(self, *args, **kw):
		ttk.Button.__init__(self, *args, **kw)

class Treeview(ttk.Treeview):
	def __init__(self, *args, **kw):
		ttk.Treeview.__init__(self, *args, **kw)

class Scrollbar(ttk.Scrollbar):
	def __init__(self, *args, **kw):
		ttk.Scrollbar.__init__(self, *args, **kw)

class Listbox(tk.Listbox):
	def __setitem__(self, item, v):
		if item == "state" and  v == "readonly":
			self.__readonly = bool(v)
		elif item == "unique":
			self.__unique = bool(v)
		else:
			super(Listbox, self).__setitem__(item, v)
	def __init__(self, *args, **kw):
		self.__readonly = False
		self.__unique = False
		if "unique" in kw:		self.__unique = bool(kw["unique"])
		if "state" in kw:		self.__readonly = kw["state"] == "readonly"
		if self.__readonly:		kw.pop("state")
		if self.__unique:		kw.pop("unique")
		kw["activestyle"] = kw["activestyle"] if "activestyle" in kw else "none"
		kw["selectmode"] = kw["selectmode"] if "selectmode" in kw else "extended"
		tk.Listbox.__init__(self, *args, **kw)
		self.bind("<Delete>", lambda e: self.__delete_selection())
		self.bind("<Control-a>", lambda e: self.__select_all())
		self.bind("<Control-ocircumflex>", lambda e: self.__select_all())
	def __delete_selection(self):
		if self.__readonly:		return
		for i in reversed(self.curselection()):
			self.delete(i)
	def __select_all(self):
		if self["selectmode"] == "extended":
			self.selection_set(0, "end")
		return "break"
	def set_values(self, values):
		self.delete(0, "end")
		for value in values:
			self.insert("end", value)
	def insert(self, index, *elements):
		if self.__unique:
			values = self.get(0, "end")
			elements = tuple(item for item in elements if not item in values)
		super(Listbox, self).insert(index, *elements)


class Toplevel(tk.Toplevel):
	def __init__(self, *args, **kw):
		tk.Toplevel.__init__(self, *args, **kw)
	def screen_center(self):
		self.update()
		wscreen, hscreen = self.winfo_screenwidth(), self.winfo_screenheight()
		wroot, hroot = tuple(int(_) for _ in self.geometry().split('+')[0].split('x'))
		self.geometry("+%d+%d" % (((wscreen - wroot) // 2, (hscreen - hroot) // 2)))

class ToplevelDialog(Toplevel):
	def __init__(self, *args, **kw):
		topmost = kw["topmost"] if "topmost" in kw else False
		if "topmost" in kw:
			kw.pop("topmost")
		Toplevel.__init__(self, *args, **kw)
		self.resizable(False, False)
		self.attributes("-topmost", topmost)

		self._result = None
		self.bind("<Escape>", lambda e: self.close())
	@property
	def result(self):		return self._result
	def close(self):		self.destroy()

class Progressbar(ttk.Progressbar):
	def __init__(self, *args, **kw):
		ttk.Progressbar.__init__(self, *args, **kw)

class Notebook(ttk.Notebook):
	def __init__(self, *args, **kw):
		ttk.Notebook.__init__(self, *args, **kw)

class Table(Treeview):
	def __setitem__(self, item, v):
		super(Table, self).__setitem__(item, v)
		if item == "columns":
			self.__init_columns(v)
	def __init__(self, *args, **kw):
		Treeview.__init__(self, *args, **kw)
		
		columns = [] if not "columns" in kw else kw["columns"]

		self["show"] = "headings" if not "show" in kw else kw["show"]
		self["selectmode"] = "browse" if not "selectmode" in kw else kw["selectmode"]
		
		self.__init_columns(columns)
		self.__create_variables()
		self.__create_bindings()

	def __init_columns(self, columns):
		self.__ro_cols = {}
		for col in columns:
			self.heading(col, text = col)
			self.column(col, width = 150)
			self.__ro_cols[col] = True
		self["displaycolumns"] = columns
	def __create_variables(self):
		self.__editor = None
	def __create_bindings(self):
		self.bind("<Button-1>", lambda e: self._edit_cell(e))
		self.bind("<MouseWheel>", lambda e: self._yview())
	def _edit_cell(self, e):
		irow, icol = self._identify_cell(e)
		if self.__editor is not None:
			self.hide_editor()
		if irow is None or icol is None:
			return
		if self["columns"][icol] in self.readonly_columns:
			return
		item = self.identify_row(e.y)
		if not item in self.selection():
			return
			
		col = self["columns"][icol]
		value = self.item(item)["values"][icol]

		self.see(item)
		# get cell position info
		x, y, width, height = self.bbox(item, col)
		pady = height // 2
		widths = [0] + [self.column(c)["width"] for c in self["columns"]]
		xpos = sum(widths[:icol+1])
		sum_width = sum(widths)
		# calculating offset
		scroll_offset = self.xview()[0]
		if xpos + self.column(col)["width"] > self.winfo_width() or xpos < scroll_offset*sum_width:
			self.xview("moveto", xpos / sum(widths))
		self.update()
		scroll_offset = self.xview()[0]
		xoffset = sum_width*scroll_offset
		# # popup
		self.__editor = ttk.Entry(self)
		self.__editor.item = item
		self.__editor.column = col
		self.__editor.place(x = xpos - xoffset, y = y + pady, anchor = "w", width = self.column(col)["width"] + 1, height = 20)
		self.__editor.insert(0, value)
		self.__editor.selection_range(0, "end")
		self.__editor.bind("<MouseWheel>", lambda e: self.hide_editor())
		self.__editor.bind("<Escape>", lambda e: self.hide_editor(save = False))
		self.__editor.bind("<Return>", lambda e: self.hide_editor(save = True))
		self.__editor.focus_force()
	def _identify_cell(self, e):
		irow, icol = None, None
		if self.identify_region(e.x, e.y) == "cell":
			irow = self.index(self.identify_row(e.y))
			icol = int(str(self.identify_column(e.x))[1:]) - 1
		return irow, icol
	def _set_cell_value(self, item, icol, value):
		self.set(item, icol, value)
	def _yview(self, *args, **kw):
		self.hide_editor(save = True)
		super(Table, self).yview(*args, **kw)
	@property
	def has_changes(self):	return self.__has_changes
	@property
	def editor(self):		return self.__editor
	@property
	def readonly_columns(self):
		return tuple([col for col in self["columns"] if self.__ro_cols[col]])
	def hide_editor(self, save = True, master = None):
		if save:
			self.save()
		if self.__editor is not None:
			self.__editor = self.__editor.place_forget()
			self.focus_force()
		if master:
			master.focus_set()
		return "break"
	def insert(self, index, values, tag = None):
		if tag is None:
			super(Table, self).insert("", index, values = values)
		else:
			super(Table, self).insert("", index, values = values, tag = tag)
	def set_readonly_column(self, icol, flag):
		self.__ro_cols[self["columns"][icol]] = bool(flag)
	def save(self):
		e = self.__editor
		if e is not None:
			value = e.get()
			self._set_cell_value(e.item, e.column, value)
			self.something_changed()
	def something_changed(self):
		self.__has_changes = True
	def get_values(self):
		return [self.item(item)["values"] for item in self.get_children()]
	def delete_all(self):
		if self.get_children():
			self.delete(*self.get_children())

class MmFsEditor(Table):
	def __setitem__(self, item, val):
		if item == "state":
			self._state = val
			if not self._state in ["normal", "readonly"]:
				raise ValueError("Invalid table state")
		else:
			super(MmFsEditor, self).__setitem__(item, val)

	def __init__(self, *args, **kw):
		self._state = "normal"
		if "state" in kw:
			self._state = kw["state"]
			kw.pop("state")
		if not self._state in ["normal", "readonly"]:
			raise ValueError("Invalid table state")

		Table.__init__(self, *args, **kw)

		columns = ["ИМЯ", "ТИП", "ШИРИНА", "ПОСЛЕ ЗАП."]
		colw = [100, 50, 100, 100]

		self.tag_configure("odd", background = "#E8FCFF")
		self.tag_configure("even", background = "#EEEEEE")
		self["columns"] = columns

		for i, w in enumerate(colw):
			self.column(columns[i], width = w)
			self.set_readonly_column(i, False)

		self.__create_variables()
		self.__create_bindings()

	def __create_variables(self):
		self.__editor = None
		self.__empty_row = [""]*4
	def __create_bindings(self):
		self.bind("<Button-1>", lambda e: self._edit_cell(e))
		self.bind("<MouseWheel>", lambda e: self._yview())
		self.bind("<Return>", lambda e: self.hide_editor())
		self.bind("<Delete>", lambda e: self._delete_row())
	def _add_row(self):
		if self._state == "readonly":
			return

		children = self.get_children()
		nrows = len(children)
		if nrows == 0:
			self.insert("end", self.__empty_row, tag = "odd")
			item = self.get_children()[-1]
			self.selection_set(item)
			self.focus(item)
		elif self.focus() == children[-1]:
			if len(children) % 2:
				self.insert("end", self.__empty_row, tag = "even")
			else:
				self.insert("end", self.__empty_row, tag = "odd")
			item = self.get_children()[-1]
			self.selection_set(item)
			self.focus(item)
	def _delete_row(self):
		children = self.get_children()
		item = self.focus()
		if not item:		return
		index = children.index(item)
		if index == len(children) - 1:
			self.focus(children[index-1])
			self.selection_set(self.focus(children[index-1]))
		else:
			self.focus(children[index])
			self.selection_set(self.focus(children[index]))
		self.delete(item)
		colors = {0: "even", 1: "odd"}
		for i, item in enumerate(self.get_children(), start = 1):
			self.item(item, tag = colors[i % 2])
	def _edit_cell(self, e):
		if self._state == "readonly":
			return
		
		irow, icol = self._identify_cell(e)
		if self.__editor is not None:
			self.hide_editor()
		if irow is None or icol is None:
			return
		if self["columns"][icol] in self.readonly_columns:
			return
		item = self.identify_row(e.y)
		if not item in self.selection():
			return
		
		if icol != 0:
			if self.item(item)["values"][0].replace(" ", "") == "":
				return
		if icol == 2:
			if self.item(item)["values"][1] != "C":
				return
		if icol == 3:
			if self.item(item)["values"][1] in ["", "C"]:
				return

		col = self["columns"][icol]
		value = self.item(item)["values"][icol]

		self.see(item)
		# get cell position info
		x, y, width, height = self.bbox(item, col)
		pady = height // 2
		widths = [0] + [self.column(c)["width"] for c in self["columns"]]
		xpos = sum(widths[:icol+1])
		sum_width = sum(widths)
		# calculating offset
		scroll_offset = self.xview()[0]
		if xpos + self.column(col)["width"] > self.winfo_width() or xpos < scroll_offset*sum_width:
			self.xview("moveto", xpos / sum(widths))
		self.update()
		scroll_offset = self.xview()[0]
		xoffset = sum_width*scroll_offset
		# # popup
		if icol != 1:
			self.__editor = ttk.Entry(self)
			self.__editor.insert(0, value)
			self.__editor.selection_range(0, "end")
		else:
			self.__editor = ttk.Combobox(self, state = "readonly")
			self.__editor["values"] = ["N", "C", "R", "F", "S", "L"]
		self.__editor.item = item
		self.__editor.column = col
		self.__editor.place(x = xpos - xoffset, y = y + pady, anchor = "w", width = self.column(col)["width"] + 1, height = 20)
		self.__editor.bind("<MouseWheel>", lambda e: self.hide_editor())
		self.__editor.bind("<Escape>", lambda e: self.hide_editor(save = False))
		self.__editor.bind("<Return>", lambda e: self.hide_editor(save = True))
		self.__editor.focus_force()
	def hide_editor(self, save = True, master = None):
		if save:
			self.save()
		if self.__editor is not None:
			self.__editor = self.__editor.place_forget()
			self.focus_force()
		else:
			self._add_row()
		if master:
			master.focus_set()
		return "break"
	def save(self):
		e = self.__editor
		if e is not None:
			value = e.get()
			self._set_cell_value(e.item, e.column, value)
			self.something_changed()

# class Style(ttk.Style):
# 	def __init__(self, *args, **kw):
# 		ttk.Style.__init__(self, *args, **kw)

# ======================================================================
# ======================== CUSTOMIZED FRAMES ===========================
# ======================================================================

class ListboxFrame(Frame):
	def __init__(self, *args, **kw):
		Frame.__init__(self, *args, **kw)
		# Create widgets
		self.__Listbox = Listbox(self, width = 30, height = 15, activestyle = "none", selectmode = "single")
		self.__ScrollbarH = Scrollbar(self, orient = "horizontal", command = self.__Listbox.xview)
		self.__ScrollbarV = Scrollbar(self, orient = "vertical", command = self.__Listbox.yview)
		self.__Listbox["xscrollcommand"] = self.__ScrollbarH.set
		self.__Listbox["yscrollcommand"] = self.__ScrollbarV.set
		# Grid widgets
		self.rowconfigure(0, weight = 1)
		self.columnconfigure(0, weight = 1)
		self.__Listbox.grid(row = 0, column = 0, sticky = "nsew")
		self.__ScrollbarH.grid(row = 1, column = 0, sticky = "ew")
		self.__ScrollbarV.grid(row = 0, column = 1, sticky = "ns")
	@property
	def listbox(self):		return self.__Listbox

class ListEditorFrame(Frame):
	def __init__(self, *args, **kw):
		Frame.__init__(self, *args, **kw)
		# Create widgets
		self.frmListbox = ListboxFrame(self)
		self.frmListbox.listbox["selectmode"] = "extended"
		self.lblInput = Label(self, text = "Введите значение")
		self.entInput = Entry(self, width = 15)
		self.frmListbox.grid(row = 0, column = 0, columnspan = 2, **GRDPARAMS_FRAMES)
		self.lblInput.grid(row = 1, column = 0, **GRDPARAMS_LABELS)
		self.entInput.grid(row = 1, column = 1, **GRDPARAMS_ENTRIES)
	@property
	def entry(self):		return self.entInput
	@property
	def label(self):		return self.lblInput
	@property
	def listbox(self):		return self.frmListbox.listbox

class TableFrame(Frame):
	def __init__(self, *args, **kw):
		Frame.__init__(self, *args, **kw)
		# Create widgets
		self.__Table = Table(self)
		self.__ScrollbarH = Scrollbar(self, orient = "horizontal", command = self.__Table.xview)
		self.__ScrollbarV = Scrollbar(self, orient = "vertical", command = self.__Table.yview)
		self.__Table["xscrollcommand"] = self.__ScrollbarH.set
		self.__Table["yscrollcommand"] = self.__ScrollbarV.set
		# Grid widgets
		self.rowconfigure(0, weight = 1)
		self.columnconfigure(0, weight = 1)
		self.__Table.grid(row = 0, column = 0, sticky = "nsew")
		self.__ScrollbarH.grid(row = 1, column = 0, sticky = "ew")
		self.__ScrollbarV.grid(row = 0, column = 1, sticky = "ns")

		self.__create_bindings()
	def __create_bindings(self):
		self.bind("<Configure>", lambda e: self.__hide_editor())
		self.__ScrollbarH.bind("<Configure>", lambda e: self.__hide_editor())
		self.__ScrollbarH.bind("<B1-Motion>", lambda e: self.__hide_editor())
		self.__ScrollbarH.bind("<Button-1>", lambda e: self.__hide_editor())
		self.__ScrollbarV.bind("<Configure>", lambda e: self.__hide_editor())
		self.__ScrollbarV.bind("<B1-Motion>", lambda e: self.__hide_editor())
		self.__ScrollbarV.bind("<Button-1>", lambda e: self.__hide_editor())
	def __hide_editor(self):
		if self.__Table.editor is not None:
			self.__Table.hide_editor(save = False)
	@property
	def table(self):		return self.__Table

class MmFsEditorFrame(Frame):
	def __init__(self, *args, **kw):
		Frame.__init__(self, *args, **kw)
		# Create widgets
		self.__Table = MmFsEditor(self)
		self.__ScrollbarH = Scrollbar(self, orient = "horizontal", command = self.__Table.xview)
		self.__ScrollbarV = Scrollbar(self, orient = "vertical", command = self.__Table.yview)
		self.__Table["xscrollcommand"] = self.__ScrollbarH.set
		self.__Table["yscrollcommand"] = self.__ScrollbarV.set
		# Grid widgets
		self.rowconfigure(0, weight = 1)
		self.columnconfigure(0, weight = 1)
		self.__Table.grid(row = 0, column = 0, sticky = "nsew")
		self.__ScrollbarH.grid(row = 1, column = 0, sticky = "ew")
		self.__ScrollbarV.grid(row = 0, column = 1, sticky = "ns")

		self.__create_bindings()
	def __create_bindings(self):
		self.bind("<Configure>", lambda e: self._hide_editor())
		self.__ScrollbarH.bind("<Configure>", lambda e: self._hide_editor())
		self.__ScrollbarH.bind("<B1-Motion>", lambda e: self._hide_editor())
		self.__ScrollbarH.bind("<Button-1>", lambda e: self._hide_editor())
		self.__ScrollbarV.bind("<Configure>", lambda e: self._hide_editor())
		self.__ScrollbarV.bind("<B1-Motion>", lambda e: self._hide_editor())
		self.__ScrollbarV.bind("<Button-1>", lambda e: self._hide_editor())
	def _hide_editor(self):
		if self.__Table.editor is not None:
			self.__Table.hide_editor(save = False)
	@property
	def table(self):		return self.__Table

class ButtonsFrame(Frame):
	def __init__(self, master, *args, **kw):
		self.__butnames = kw["buttons"] if "buttons" in kw else "OK|Отмена"
		self.__orient = kw["orient"] if "orient" in kw else "vertical"
		self.__sticky = kw["sticky"] if "sticky" in kw else "n"
		if self.__orient not in ["vertical", "horizontal"]:
			raise ValueError("Bad option orientation - '%s'" % self.__orient)
		if not self.__sticky in ["n", "s", "e", "w"]:
			raise ValueError("Bad option sticky - '%s'" % str(self.__sticky))
		if self.__orient[0] == "h" and self.__sticky in "sn":
			self.__sticky = "e"
		if self.__orient[0] == "v" and self.__sticky in "ew":
			self.__sticky = "n"

		for prop in ["buttons", "orient", "sticky"]:
			if prop in kw:
				kw.pop(prop)
		
		self.__butnames = tuple(self.__butnames.split("|"))
		if "" in self.__butnames:
			self.__butnames.remove("")		

		Frame.__init__(self, master, *args, **kw)

		self.__create_widgets()
		self.__grid_widgets()
	def __create_widgets(self):
		self.__buttons = {i: Button(self, width = 15, text = name) for i, name in enumerate(self.__butnames)}
	def __grid_widgets(self):
		orient, sticky = self.__orient, self.__sticky
		side = "top"
		if orient[0] == "v" and "n" in sticky:
			side = "top"
		if orient[0] == "v" and "s" in sticky:
			side = "bottom"
		if orient[0] == "h" and "e" in sticky:
			side = "right"
		if orient[0] == "h" and "w" in sticky:
			side = "left"
			
		for i, name in enumerate(self.__butnames):
			self.__buttons[i].pack(side = side, anchor = sticky, **PADDING)
	@property
	def buttons(self):			return self.__buttons
	@property
	def button_names(self):		return self.__butnames

class StatusFrame(Frame):
	def __init__(self, *args, **kw):
		Frame.__init__(self, *args, **kw)

		self.__lblStatus = Label(self)
		self.__prbCurrent = Progressbar(self)
		self.__prbCurrent.progress_step = 1

		self.columnconfigure(0, weight = 1)
		self.__lblStatus.grid(row = 0, column = 0, **GRDPARAMS_ENTRIES)
		self.__prbCurrent.grid(row = 1, column = 0, **GRDPARAMS_FRAMES)

	@property
	def status(self):					return self.__lblStatus
	@property
	def current_progress(self):			return self.__prbCurrent
	@property
	def progress_step(self):			return self.__prbCurrent.progress_step
	@progress_step.setter
	def progress_step(self, v):			self.__prbCurrent.progress_step = v


