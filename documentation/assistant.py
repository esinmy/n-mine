from PyQt4 import QtGui, QtCore
from utils.constants import *
import os

helpDictUrl = {}

helpDictUrl[MODE_DISPLAY_DATA] = "qthelp://title.of.this.help.project/doc/Newtopic10.html"
helpDictUrl[MODE_GEO_WF_VALIDATION] = "qthelp://title.of.this.help.project/doc/Newtopic24.html"
helpDictUrl[MODE_GEO_UPDATE_BM] = "qthelp://title.of.this.help.project/doc/Newtopic28.html"
helpDictUrl[MODE_GEO_MERGE_BM] = "qthelp://title.of.this.help.project/doc/Newtopic108.html"
helpDictUrl[MODE_GEO_TRANSFER_BM] = "qthelp://title.of.this.help.project/doc/Newtopic29.html"
helpDictUrl[MODE_SVY_WF_VALIDATION] = "qthelp://title.of.this.help.project/doc/Newtopic24.html"
helpDictUrl[MODE_SVY_UPDATE_BM] = "qthelp://title.of.this.help.project/doc/Newtopic37.html"
helpDictUrl[MODE_SVY_REFRESH_BM] = "qthelp://title.of.this.help.project/doc/Newtopic38.html"
helpDictUrl[MODE_SVY_REPORT_BM] = "qthelp://title.of.this.help.project/doc/Newtopic42.html"
helpDictUrl[MODE_SVY_RESTORE_GEOLOGY] = "qthelp://title.of.this.help.project/doc/Newtopic14.html"
helpDictUrl[MODE_SVY_UPDATE_GRAPHIC] = "qthelp://title.of.this.help.project/doc/Newtopic43.html"
helpDictUrl[MODE_SVY_STOCK_CALCPLOT] = "qthelp://title.of.this.help.project/doc/Newtopic44.html"
helpDictUrl[MODE_DSN_WF_VALIDATION] = "qthelp://title.of.this.help.project/doc/Newtopic24.html"
helpDictUrl[MODE_DSN_UPDATE_BM] = "qthelp://title.of.this.help.project/doc/Newtopic15.html"
helpDictUrl[MODE_DSN_REPORT_BM] = "qthelp://title.of.this.help.project/doc/Newtopic50.html"
helpDictUrl[MODE_CUSTOM_REPORT_BM] = "qthelp://title.of.this.help.project/doc/Newtopic25.html"
helpDictUrl[MODE_AUTO_VIZEX_PLOT] = "qthelp://title.of.this.help.project/doc/Newtopic26.html"
helpDictUrl[COMMAND_GEO_CREATE_UNIT] = "qthelp://title.of.this.help.project/doc/Newtopic19.html"
helpDictUrl[COMMAND_GEO_DEFINE_MODEL] = "qthelp://title.of.this.help.project/doc/Newtopic20.html"
helpDictUrl[COMMAND_GEO_ADD_ELEMENT] = "qthelp://title.of.this.help.project/doc/Newtopic27.html"
helpDictUrl[COMMAND_SVY_CREATE_UNIT] = "qthelp://title.of.this.help.project/doc/Newtopic30.html"
helpDictUrl[COMMAND_SVY_DEFINE_MODEL] = "qthelp://title.of.this.help.project/doc/Newtopic34.html"
helpDictUrl[COMMAND_SVY_ADD_ELEMENT] = "qthelp://title.of.this.help.project/doc/Newtopic40.html"
helpDictUrl[COMMAND_LOAD_DRILLHOLES] = "qthelp://title.of.this.help.project/doc/Newtopic104.html"
helpDictUrl[COMMAND_UPDATE_DHDB] = "qthelp://title.of.this.help.project/doc/Newtopic103.html"
helpDictUrl[COMMAND_LOAD_SVY_POINTS] = "qthelp://title.of.this.help.project/doc/Newtopic61.html"
helpDictUrl[COMMAND_SVY_CREATE_POINT] = "qthelp://title.of.this.help.project/doc/Newtopic32.html"
helpDictUrl[COMMAND_SVY_EDIT_POINT] = "qthelp://title.of.this.help.project/doc/Newtopic32.html"
helpDictUrl[COMMAND_SVY_REMOVE_POINT] = "qthelp://title.of.this.help.project/doc/Newtopic32.html"
helpDictUrl[COMMAND_SVY_ADD_DIRECTION] = "qthelp://title.of.this.help.project/doc/Newtopic112.html"
helpDictUrl[COMMAND_SVY_EDIT_DIRECTION] = "qthelp://title.of.this.help.project/doc/Newtopic112.html"
helpDictUrl[COMMAND_DSN_CREATE_UNIT] = "qthelp://title.of.this.help.project/doc/Newtopic46.html"
helpDictUrl[COMMAND_DSN_DEFINE_MODEL] = "qthelp://title.of.this.help.project/doc/Newtopic47.html"
helpDictUrl[COMMAND_DSN_ADD_ELEMENT] = "qthelp://title.of.this.help.project/doc/Newtopic48.html"
helpDictUrl[COMMAND_DSN_AUTO_CREATE_SOLID] = "qthelp://title.of.this.help.project/doc/Newtopic41.html"
helpDictUrl[COMMAND_IMPORT_GSI] = "qthelp://title.of.this.help.project/doc/Newtopic1.html"
helpDictUrl[COMMAND_SURVEY_TO_DB_TRDB] = "qthelp://title.of.this.help.project/doc/Newtopic1.html"

class HelpAssistant(QtCore.QObject):
	def __init__(self, parent):
		super().__init__()
		self.__main_app = parent
		self.__process = None
	def assistantPath(self):
		sitePackagesPaths = list(filter(lambda path: path.lower().endswith("site-packages"), os.sys.path))
		if len(sitePackagesPaths) != 1:
			QtGui.QMessageBox.critical(self, "Ошибка", "Найдено более одного хранилища модулей Python\n{}".format("\n".join(sitePackagesPaths)))
			return None

		assistantPath = os.path.join(sitePackagesPaths[0], "pyqt4", "assistant.exe")
		return assistantPath
	def show(self):
		qhcPath = os.path.join(os.getcwd(), "documentation", "help.qhc")
		assistantPath = self.assistantPath()
		if not assistantPath:
			return

		page = helpDictUrl.get(self.__main_app.currentMode)
		if page is None:
			page = helpDictUrl.get(MODE_DISPLAY_DATA)

		cmd = '"{}" -collectionFile "{}" -showUrl "{}"'.format(
			assistantPath, 
			qhcPath, 
			page
		)

		try:
			self.__process = QtCore.QProcess(self)
			self.__process.finished.connect(self.__processFinished)
			self.__process.start(cmd)
		except Exception as e:
			QtGui.QMessageBox.critical(self, "Ошибка", "Невозможно открыть справку. Ошибка: '{}'".format(e))
	def __processFinished(self):
		self.__process = None
	def isVisible(self):
		return self.__process is not None
	def close(self):
		self.__process.terminate()

