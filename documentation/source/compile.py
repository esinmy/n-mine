import os
from xml.etree import ElementTree

try:
	from PyQt4 import QtGui, QtCore
except ImportError:
	import tkinter as tk
	import tkinter.messagebox as msgbox

	root = tk.root()
	root.withdraw()
	msgbox.show_error("Ошибка", "Невозможно импортировать PyQt4", parent = root)
	root.mainloop()
	os.sys.exit()

class HelpCompiler(QtGui.QWidget):
	def __init__(self, parent = None):
		super().__init__(parent)

		self.setWindowTitle("Копмилятор справки")

		self.initVars()
		self.initUi()
		self.initBindings()

	def initVars(self):
		self.__canCompile = True
		self.__pyqtPath = self.getPyqtPath()
		#
		self.__qCollectionGenPath = os.path.join(self.__pyqtPath, "qcollectiongenerator.exe")
		self.__qHelpGenPath = os.path.join(self.__pyqtPath, "qhelpgenerator.exe")
		self.__qAssistantPath = os.path.join(self.__pyqtPath, "assistant.exe")
		#
		qPathList = [self.__qCollectionGenPath, self.__qHelpGenPath, self.__qAssistantPath]
		for i, path in enumerate(qPathList):
			if not os.path.exists(path):
				QtGui.QMessageBox.critical("Ошибка", "Не найден файл '{}'".format(qPathList[i]))
				self.__canCompile = False

	def initUi(self):
		self.__initWidgets()
		self.__gridWidgets()

	def __initWidgets(self):
		self.layoutMain = QtGui.QVBoxLayout(self)

		self.layoutWidgets = QtGui.QFormLayout()

		self.labelHtmlFolderPath = QtGui.QLabel(self)
		self.labelHtmlFolderPath.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelHtmlFolderPath.setMinimumWidth(150)
		self.labelHtmlFolderPath.setText("Директория с справкой")
		self.lineEditHtmlFolderPath = QtGui.QLineEdit(self)
		self.lineEditHtmlFolderPath.setMinimumWidth(450)

		self.labelQhpPath = QtGui.QLabel(self)
		self.labelQhpPath.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelQhpPath.setMinimumWidth(150)
		self.labelQhpPath.setText("Файл проекта")
		self.lineEditQhpPath = QtGui.QLineEdit(self)
		self.lineEditQhpPath.setMinimumWidth(450)

		self.labelQhcpPath = QtGui.QLabel(self)
		self.labelQhcpPath.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelQhcpPath.setMinimumWidth(150)
		self.labelQhcpPath.setText("Файл коллекции")
		self.lineEditQhcpPath = QtGui.QLineEdit(self)
		self.lineEditQhcpPath.setMinimumWidth(450)

		self.labelOutputHelpPath = QtGui.QLabel(self)
		self.labelOutputHelpPath.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
		self.labelOutputHelpPath.setMinimumWidth(150)
		self.labelOutputHelpPath.setText("Файл вывода")
		self.lineEditOutputHelpPath = QtGui.QLineEdit(self)
		self.lineEditOutputHelpPath.setMinimumWidth(450)

		self.buttonBox = QtGui.QDialogButtonBox(self)
		self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).setText("Отмена")
	def __gridWidgets(self):
		self.setLayout(self.layoutMain)

		self.layoutMain.addLayout(self.layoutWidgets)
		self.layoutWidgets.setWidget(0, QtGui.QFormLayout.LabelRole, self.labelHtmlFolderPath)
		self.layoutWidgets.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditHtmlFolderPath)
		self.layoutWidgets.setWidget(1, QtGui.QFormLayout.LabelRole, self.labelQhpPath)
		self.layoutWidgets.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditQhpPath)
		self.layoutWidgets.setWidget(2, QtGui.QFormLayout.LabelRole, self.labelQhcpPath)
		self.layoutWidgets.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEditQhcpPath)
		self.layoutWidgets.setWidget(3, QtGui.QFormLayout.LabelRole, self.labelOutputHelpPath)
		self.layoutWidgets.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEditOutputHelpPath)
		self.layoutMain.addWidget(self.buttonBox)

	def initBindings(self):
		self.lineEditHtmlFolderPath.mouseDoubleClickEvent = lambda e: self.browseOpenFolderDialog(e, self.lineEditHtmlFolderPath)
		self.lineEditQhpPath.mouseDoubleClickEvent = lambda e: self.browseOpenFileDialog(e, self.lineEditQhpPath, extfilter = "Файл QHC (*.qhp)")
		self.lineEditQhcpPath.mouseDoubleClickEvent = lambda e: self.browseOpenFileDialog(e, self.lineEditQhcpPath, extfilter = "Файл QHC (*.qhcp)")
		self.lineEditOutputHelpPath.mouseDoubleClickEvent = lambda e: self.browseSaveFileDialog(e, self.lineEditOutputHelpPath, extfilter = "Файл QHC (*.qhc)")

		self.buttonBox.button(QtGui.QDialogButtonBox.Ok).clicked.connect(self.prepare)
		self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.close)

	def canCompile(self):
		return self.__canCompile

	def getPyqtPath(self):
		sitePackagesPaths = list(filter(lambda path: path.lower().endswith("site-packages"), os.sys.path))
		if len(sitePackagesPaths) != 1:
			QtGui.QMessageBox.critical(self, "Ошибка", "Найдено более одного хранилища модулей Python\n{}".format("\n".join(sitePackagesPaths)))
			return None

		pyqtPath = os.path.join(sitePackagesPaths[0], "pyqt4")
		return pyqtPath

	def browseOpenFileDialog(self, e, sender, extfilter = ""):
		path = QtGui.QFileDialog.getOpenFileName(self, "Выберите файл", filter = extfilter)
		if not path:
			return
		sender.setText(os.path.abspath(path))
	def browseOpenFolderDialog(self, e, sender):
		path = QtGui.QFileDialog.getExistingDirectory(self, "Выберите директорию")
		if not path:
			return
		sender.setText(os.path.abspath(path))
		# autofill
		if sender == self.lineEditHtmlFolderPath:
			qhpPath = os.path.join(sender.text(), "project.qhp")
			if os.path.exists(qhpPath):
				self.lineEditQhpPath.setText(qhpPath)
			qhcpPath = os.path.join(sender.text(), "collection.qhcp")
			if os.path.exists(qhcpPath):
				self.lineEditQhcpPath.setText(qhcpPath)
	def browseSaveFileDialog(self, e, sender, extfilter = ""):
		path = QtGui.QFileDialog.getSaveFileName(self, "Сохранить как", filter = extfilter)
		if not path:
			return
		sender.setText(os.path.abspath(path))

	def cleanUpHtml(self, folderPath):
		replaceData = (
			"""Created with the Personal Edition of HelpNDoc: """,
			"""href="https://www.helpndoc.com""",
			"""Easy EPub and documentation editor""",
			"""href="https://www.helpndoc.com/help-authoring-tool""",
			"""Easy to use tool to create HTML Help files and Help web sites""",
			"""/create-epub-ebooks""",
			"""/help-authoring-tool""",
			"""Full-featured EPub generator""",
			"""Generate Kindle eBooks with ease""",
			"""Write eBooks for the Kindle""",
			"""Free Kindle producer""",
			"""Create iPhone web-based documentation""",
			"""Create help files for the Qt Help Framework""",
			"""News and information about help authoring tools and software""",
			"""Free HTML Help documentation generator""",
			"""<div id="topic_footer">

			<div id="topic_footer_content">
				Copyright &copy; &lt;Dates&gt; by &lt;Authors&gt;. All Rights Reserved.
			</div>
		</div>"""
		)

		for root, folderNames, fileNames in os.walk(folderPath):
			for fileName in fileNames:
				if not fileName.endswith(".html"):
					continue
				filePath = os.path.join(root, fileName)
				with open(filePath, "r", encoding = "utf8") as htmlFile:
					htmlData = htmlFile.read()
					cleanedData = htmlData[:]
					for data in replaceData:
						cleanedData = cleanedData.replace(data, "")
				with open(filePath, "w", encoding = "utf8") as htmlFile:
					htmlFile.write(cleanedData)

	def check(self):
		helpFolderPath = self.lineEditHtmlFolderPath.text()
		qhpPath = self.lineEditQhpPath.text()
		qhcpPath = self.lineEditQhcpPath.text()
		helpPath = self.lineEditOutputHelpPath.text()
		helpOutputFolderPath = os.path.dirname(helpPath)

		folderPaths = [helpFolderPath, helpOutputFolderPath]
		for folderPath in folderPaths:
			if not folderPath or not os.path.exists(folderPath):
				QtGui.QMessageBox.critical(self, "Ошибка", "Директория '{}' не существует".format(folderPath))
				lineEdit.setFocus()
				return False
		
		filePaths = [qhpPath, qhcpPath]
		for filePath in filePaths:
			if not filePath or not os.path.exists(filePath):
				QtGui.QMessageBox.critical(self, "Ошибка", "Файл '{}' не существует".format(filePath))
				lineEdit.setFocus()
				return False

		return True
	def prepare(self):
		if not self.check():
			return

		try:
			self.run()
		except Exception as e:
			QtGui.QMessageBox.critical(self, "Ошибка", "{}".format(e))
			return
	def run(self):
		helpFolderPath = self.lineEditHtmlFolderPath.text()
		qhpPath = self.lineEditQhpPath.text()
		qhcpPath = self.lineEditQhcpPath.text()
		qhcPath = self.lineEditOutputHelpPath.text()
		helpOutputFolderPath = os.path.dirname(qhcPath)

		self.cleanUpHtml(helpFolderPath)

		qhcFileName = os.path.splitext(os.path.basename(qhcPath))[0]
		qchPath = os.path.join(helpOutputFolderPath, qhcFileName + ".qch")

		os.chdir(self.__pyqtPath)
		# compile project file
		cmd = '{} "{}" -o "{}"'.format("qhelpgenerator", qhpPath, qchPath)
		os.system(cmd)
		# compile collection file
		cmd = '{} "{}" -o "{}"'.format("qcollectiongenerator", qhcpPath, qhcPath)
		os.system(cmd)
		# run assistant
		cmd = '{} -collectionFile "{}"'.format("assistant.exe", qhcPath)
		os.system(cmd)

app = QtGui.QApplication([])
# try:
compilerWindow = HelpCompiler()
compilerWindow.show()
# except Exception as e:
# 	QtGui.QMessageBox.critical("Ошибка", "Невозможно открыть окно компилятора справки. {}".format(e))
app.exec_()