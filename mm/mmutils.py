try:		import MMpy
except:		pass

from utils.osutil import *
import sqlite3, shutil, os

from utils.logger import  NLogger
LOG_INJECT_THIS_MODULE_ALLOW = False
logger = NLogger()

DEBUG_MODE = 0

def get_micromine_version():
	major, minor, servicepack, build = map(int, MMpy.Micromine.version().split("."))
	if major > 15:
		tmp = servicepack
		servicepack = build
		build = tmp
	return major, minor, servicepack, build
def get_fldval_path():
	major, minor, servicepack, build = get_micromine_version()
	return os.path.join(MMpy.Project.path(), "FLDVAL%d0.BDB" % major)
	# return r"D:\Workspace\Micromine\Projects\Норильский Никель\Октябрьский\FLDVAL160.BDB"

class MicromineFile(MMpy.File if not DEBUG_MODE else object):
	@staticmethod
	def determine_structure(data, header, nrows = 100):
		if len(data) < nrows:
			nrows = len(data)
		coldata = tuple(zip(*data))
		if len(coldata) != len(header):
			raise Exception("Header and data have different number of columns")
		structure = MMpy.FileStruct()
		for k, cdata in enumerate(coldata):
			# determine field type
			ctype = "R"
			for i in range(nrows):
				if type(cdata[i]) == str:
					ctype = "C"
					break
			# convert column data to string
			sdata = map(str, cdata)
			# determine field width if character
			if ctype == "C":
				w = max(map(len, sdata))
				if w == 0:		w = 1
				if w > 255:		w = 255
				structure.add_field(header[k], MMpy.FieldType.character, w, 0)
			# determine field precision otherwise
			else:
				p = max(map(lambda x: len(x[x.rfind(".")+1:]) if "." in x else 0, sdata))
				structure.add_field(header[k], MMpy.FieldType.real, 0, p)
		return structure
	@staticmethod
	def to_filestruct(s):
		types = {ft[0]: MMpy.FieldType.names[ft] for ft in MMpy.FieldType.names}
		structure = MMpy.FileStruct()
		for fld in s:
			name, ft, width, prec = fld.split(":")
			structure.add_field(name, types[ft.lower()], int(width), int(prec))
		return structure
	@staticmethod
	def from_filestruct(s):
		structure = []
		for i in range(s.fields_count):
			n = s.get_field_name(i)
			t = str(s.get_field_type(i))[0].upper()
			w = s.get_field_width(i)
			p = s.get_field_precision(i)
			structure.append("%s:%s:%d:%d" % (n, t, w, p))
		return structure
	@staticmethod
	def create(path, structure):		
		s = structure if type(structure) == MMpy.FileStruct else MicromineFile.to_filestruct(structure)
		return MMpy.File.create_from_template(path, "", s)
	@property
	def header(self):
		s = self.structure
		return [s.get_field_name(i) for i in range(s.fields_count)]
	def read(self, columns = None, asstr = False):
		if columns is None:
			columns = self.header
		ret = []
		if asstr:
			for i in range(self.records_count):
				ret.append([])
				for col in columns:
					j = self.get_field_id(col)
					ret[-1].append(self.get_str_field_value(j, i+1))
		else:
			for i in range(self.records_count):
				ret.append([])
				for col in columns:
					j = self.get_field_id(col)
					if self.structure.get_field_type(j) == MMpy.FieldType.character:
						ret[-1].append(self.get_str_field_value(j, i+1))
					else:
						ret[-1].append(self.get_num_field_value(j, i+1))
		return ret

	@staticmethod
	def has_field(path, name):
		f = MMpy.File()
		if not f.open(path):	return False
		exists = f.get_field_id(name) != -1
		f.close()
		return exists
	@staticmethod
	def write(path, data, header = None, structure = None):
		if header is None and structure and None:
			raise Exception("Необходимо задать либо заголовок файла, либо его структуру")

		if header is not None:
			if structure is None:
				structure = MicromineFile.determine_structure(data, header)
			elif type(structure) != MMpy.FileStruct:
				structure = MicromineFile.to_filestruct(structure)
				header = [structure.get_field_name(i) for i in range(structure.fields_count)]
		elif type(structure) != MMpy.FileStruct:
			structure = MicromineFile.to_filestruct(structure)
			header = [structure.get_field_name(i) for i in range(structure.fields_count)]
		else:
			header = [structure.get_field_name(i) for i in range(structure.fields_count)]


		nrows, ncols = len(data), len(header)

		if os.path.exists(path):
			try:					os.remove(path)
			except Exception as e:	raise Exception("Невозможно удалить файл '%s'. %s" % (path, str(e)))
	
		f = MMpy.File()
		if not os.path.exists(path):
			if not MicromineFile.create(path, structure):
				raise Exception("Couldn't create the file %s" % path)
			if not f.open(path):
				raise Exception("Couldn't open the file %s" % path)
		else:
			# удаление данных из файла
			f = MMpy.File()
			if not f.open(path):
				raise Exception("Couldn't open the file %s" % path)
			for i in range(f.records_count):
				f.delete_record(1)
			# добавление полей
			f.structure = structure
		# запись данных
		for i in range(nrows):
			f.add_record()
			for j in range(ncols):
				try:
					value = data[i][j] if data[i][j] is not None else ""
				except:
					value = ""
				f.set_field_value(j, f.records_count, value)
		f.close()

class Wireframe:
	def __init__(self, cursor, parent, name):
		self.__cursor = cursor
		self._parent = parent
		self.__name = name
		cmd = """select * 
				from GeneralInformation
			    where name = ?"""
		params = (	name,)
		cursor.execute(cmd, params)
		vals = cursor.fetchone()
		description = [col[0] for col in cursor.description]
		self.__dfltAttrValue = {}
		for i, attrName in enumerate(description):
			setattr(self, attrName.lower().replace(" ", "_"), vals[i])
			self.__dfltAttrValue[attrName] = vals[i] if vals[i] else ""

		self.__userAttrValue = {}
		cmd = "select D.NAME, V.VALUETEXT from UserAttributeValue V join UserAttributeDefinition D on V.AttributeId = D.Id where triangulationId = %d" % self.id
		cursor.execute(cmd)
		result = cursor.fetchall()
		for row in result:
			self.__userAttrValue[row[0]] = row[1] if row[1] else ""

	def default_attribute(self, attrName):
		return self.__dfltAttrValue[attrName] if self.__dfltAttrValue[attrName] else ""
	def default_attributes(self):				return self.__dfltAttrValue
	def user_attribute(self, attrName):
		if attrName in self._parent.user_attributes:
			return self.__userAttrValue[attrName] if attrName in self.__userAttrValue and self.__userAttrValue[attrName] else ""
		else:
			raise Exception("Атрибут '%s' в каркасе не найден" % attrName)
	def user_attribute_for_mdl_line(self, attrName):
		if attrName in self._parent.user_attributes:
			return self.__userAttrValue[attrName] if attrName in self.__userAttrValue and self.__userAttrValue[attrName] else ""
		else:
			return False
	def user_attributes(self):				return self.__userAttrValue
	def set_user_attribute(self, attrName, attrValue, commit = False):
		self.__cursor.execute("select Id from UserAttributeDefinition where Name = '%s'" % attrName)
		attrId, = self.__cursor.fetchone()
		if attrId is None:			return
		cmd = "delete from UserAttributeValue where TriangulationId = %d and AttributeId = %d" % (self.id, attrId)
		self.__cursor.execute(cmd)
		cmd = "insert into UserAttributeValue (TriangulationId, AttributeId, ValueText) values (%d, %d, '%s')" % (self.id, attrId, attrValue)
		self.__cursor.execute(cmd)
		if commit:			self.__cursor.connection.commit()
	def set_default_attribute(self, attrName, attrValue, commit = False):
		cmd = "update GeneralInformation set %s = ? where Id = ?"
		#cmd =  cmd % (attrName, attrValue, self.id)
		cmd =  cmd % (attrName)
		params = (	attrValue, self.id)
		self.__cursor.execute(cmd, params)
		if commit:
			self.__cursor.connection.commit()

class Tridb:
	def __getitem__(self, wfname):
		if not wfname in self.wireframe_names:
			raise ValueError("Каркас %s не найден" % wfname)
		return Wireframe(self.__cursor, self, wfname)
	def __init__(self, tridbPath):
		if not os.path.exists(tridbPath):
			raise FileNotFoundError("Файл %s не существует" % tridbPath)
		self.__path = tridbPath
		cnxn = sqlite3.connect(self.__path)
		self.__cursor = cnxn.cursor()
	def __iter__(self):
		for wfname in self.wireframe_names:
			yield Wireframe(self.__cursor, self, wfname)

	def add_wireframe(self, wfName,wfBlob):
		self.__cursor.execute("select Id from GeneralInformation")
		values = self.__cursor.fetchall()
		if not values:		ids = []
		else:				ids= list(zip(*values))[0]

		nextId = 1 if ids == [] else max(ids) + 1
		cmd = """
		INSERT INTO GeneralInformation ([ID], [NAME], [CODE], [TITLE])
				VALUES (?, ?, ?, ?)"""

		params = [nextId, wfName, wfName, wfName]
		self.__cursor.execute(cmd, params)
				
		cmd = """
		INSERT INTO Geometry ([ID], [GEOMETRY])
				VALUES (?, ?)"""
		params = [nextId, wfBlob]
		self.__cursor.execute(cmd, params)

		self.__cursor.connection.commit()

	def add_attribute(self, attrName):
		self.__cursor.execute("select Id, Name from UserAttributeDefinition")
		values = self.__cursor.fetchall()
		if not values:		ids, attrNames = [], []
		else:				ids, attrNames = tuple(zip(*values))
		if attrName in attrNames:
			self.close()
			raise ValueError("Атрибут %s уже существует" % attrName)
		nextId = 1 if ids == [] else max(ids) + 1
		self.__cursor.execute("insert into UserAttributeDefinition values (?,?,?,?,?,?,?)", [nextId, attrName, 0, 2, "", 0, 0])
		self.__cursor.connection.commit()
	def clear_user_attributes(self):
		self.__cursor.execute("delete from  UserAttributeDefinition")
		self.__cursor.execute("delete from  UserAttributeValue")
		self.__cursor.connection.commit()
	def commit(self):
		self.__cursor.connection.commit()
	def close(self):
		self.__cursor.connection.close()
	@staticmethod
	def create(tridbPath, attributes = []):
		if os.path.exists(tridbPath):
			os.remove(tridbPath)
		try:			shutil.copy("files\\tridb_templates\\empty.tridb", tridbPath)
		except Exception as e:
			raise Exception("Невозможно создать файл tridb. %s" % str(e))
		tridb = Tridb(tridbPath)
		for attr in attributes:
			tridb.add_attribute(attr)
		tridb.close()
	@property
	def attributes(self):			return self.default_attributes + self.user_attributes
	@property
	def default_attributes(self):
		self.__cursor.execute("select * from GeneralInformation limit 0")
		attrNames = tuple(zip(*self.__cursor.description))[0][1:]
		return attrNames
	def remove(self, wfname):
		wireframe = self[wfname]
		cmd = """
			DELETE FROM UserAttributeValue WHERE TriangulationId = {0};
			DELETE FROM TriangleAttributeValue WHERE TriangulationId = {0};
			DELETE FROM PointAttributeValue WHERE TriangulationId = {0};
			DELETE FROM Geometry WHERE ID = {0};
			DELETE FROM GeneralInformation WHERE ID = {0};
		""".format(wireframe.id)
		del wireframe
		self.__cursor.executescript(cmd)
	@property
	def user_attributes(self):
		self.__cursor.execute("select Name from UserAttributeDefinition order by id")
		values = self.__cursor.fetchall()
		values = tuple() if not values else tuple(zip(*values))[0]
		return values
	@property
	def wireframe_names(self):
		self.__cursor.execute("select Name from GeneralInformation order by id")
		values = self.__cursor.fetchall()
		values = tuple() if not values else tuple(zip(*values))[0]
		return values
	@property
	def name(self):
		return os.path.basename(self.__path)

class Annotation:
	@staticmethod
	def create(path):
		f = open(path, "wb")
		srcString = b'\xef\xbb\xbf<?xml version="1.0" encoding="utf-8"?>\n<annotation-layer starttime="42593.29901620370200000" starttime-user="2016-08-11T07:10:35.000" run-by="pyakovlev" project-name="Temp" project-path="D:\\Workspace\\Micromine\\Projects\\temp\\" project-title="" mm-version="16.0.2">\n\t<plane-point X="0.00000000000000" Y="0.00000000000000" Z="0.00000000000000"/>\n\t<normal-direction X="0.00000000000000" Y="0.00000000000000" Z="1.00000000000000"/>\n\t<up-direction X="0.00000000000000" Y="1.00000000000000" Z="0.00000000000000"/>\n\t<properties>\n\t\t<dimensioning-font font-value="0=0=0=0=0=0=0=0FFFF16.00=1=2147483647=2147483647=2147483647=0=0=TF=(0,0,-1)(0,1,0)=FF=5=2=0 Arial"/>\n\t\t<dimensioning-font-colour colour-value="640000"/>\n\t\t<dimensioning-precision precision-value="1"/>\n\t\t<dimensioning-prefix prefix=""/>\n\t\t<dimensioning-suffix suffix=""/>\n\t\t<dimensioning-dimension-lines-colour colour-value="640000"/>\n\t\t<dimensioning-dimension-lines-type line-type="0"/>\n\t\t<dimensioning-dimension-lines-width width="0.13000000000000"/>\n\t\t<dimensioning-dimension-lines-first-end-type line-end-type="0"/>\n\t\t<dimensioning-dimension-lines-second-end-type line-end-type="0"/>\n\t\t<dimensioning-dimension-lines-first-end-size line-end-size="16.00000000000000000"/>\n\t\t<dimensioning-dimension-lines-second-end-size line-end-size="16.00000000000000000"/>\n\t\t<dimensioning-first-extension-line-display enable="true"/>\n\t\t<dimensioning-first-extension-line-colour colour-value="640000"/>\n\t\t<dimensioning-first-extension-line-type line-type="0"/>\n\t\t<dimensioning-first-extension-line-width width="0.13000000000000"/>\n\t\t<dimensioning-first-extension-line-offset offset="0.00000000000000000"/>\n\t\t<dimensioning-second-extension-line-display enable="true"/>\n\t\t<dimensioning-second-extension-line-colour colour-value="640000"/>\n\t\t<dimensioning-second-extension-line-type line-type="0"/>\n\t\t<dimensioning-second-extension-line-width width="0.13000000000000"/>\n\t\t<dimensioning-second-extension-line-offset offset="0.00000000000000000"/>\n\t\t<alternate-text alternate-text-value=""/>\n\t\t<text-font font-value="0=0=0=0=0=0=0=0FFFF16.00=1=2147483647=2147483647=2147483647=0=0=TF=(0,0,-1)(0,1,0)=FF=5=2=0 Arial"/>\n\t\t<text-font-colour colour-value="640000"/>\n\t\t<text-alignment-horizontal-alignment alignment="2"/>\n\t\t<text-alignment-horizontal-offset offset="0.00000000000000000"/>\n\t\t<text-alignment-vertical-alignment alignment="2"/>\n\t\t<text-alignment-vertical-offset offset="0.00000000000000000"/>\n\t\t<text-alignment-angle angle="0.00000000000000000"/>\n\t\t<text-leader-line-colour colour-value="640000"/>\n\t\t<text-leader-line-type line-type="0"/>\n\t\t<text-leader-line-width width="0.13000000000000"/>\n\t\t<text-leader-line-first-end-type line-end-type="0"/>\n\t\t<text-leader-line-second-end-type line-end-type="5"/>\n\t\t<text-leader-line-first-end-size line-end-size="16.00000000000000000"/>\n\t\t<text-leader-line-second-end-size line-end-size="16.00000000000000000"/>\n\t</properties>\n</annotation-layer>\n'
		f.write(srcString)
		f.close()

class FLDVAL:
	def __init__(self):
		path = get_fldval_path()
		if not os.path.exists(path):
			raise FileNotFoundError("Файл базы данных форм не найден")
		try:
			cnxn = sqlite3.connect(path)
		except Exception as e:
			raise Exception("Невозможно подключиться к базе данных форм. Возможно, файл поврежден. %s" % str(e))

		self._cursor = cnxn.cursor()
		self.exec_query("PRAGMA journal_mode = memory")

	def exec_query(self, cmd, *args):
		try:
			self._cursor.execute(cmd, *args)
		except Exception as e:
			self.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))

		#
		if self._cursor.description is None:
			return None

		#
		try:
			data = self._cursor.fetchall()
			if len(data) == 1:		ret = {name[0]: value[0] for name, value in zip(self._cursor.description, zip(*data))}
			else:					ret = {name[0]: value for name, value in zip(self._cursor.description, zip(*data))}
		except Exception as e:
			self.close()
			raise Exception(e)
		#
		return ret

	def get_folder_structure(self, cmdId = None):
		if cmdId is None:
			dirCmd = "select CASE WHEN F.path = '' THEN F.cmdId ELSE F.cmdId || '|' || F.path END FolderPath from FOLDERS F"
			setCmd = """
			select CASE WHEN F.path = '' THEN F.cmdId || '|' || P.title	ELSE F.cmdId || '|' || F.path || '|' || P.title	END FormsetPath,
				P.setId,
				P.szEditAuthor,
				P.lEditDateTime,
				P.szCreateAuthor,
				P.lCreateDateTime
			from PRMSETS P join FOLDERS F on P.cmdId = F.cmdId and P.folderId = F.folderId
			where P.setId != 0
			order by P.setId
			"""
		else:
			dirCmd = "select F.path FolderPath from FOLDERS F where cmdId = %d and F.path != ''" % cmdId
			setCmd = """
			select
				IFNULL(F.path, '') || '|' || P.title FormsetPath,
				P.setId,
				P.szEditAuthor,
				P.lEditDateTime,
				P.szCreateAuthor,
				P.lCreateDateTime
			from PRMSETS P left join FOLDERS F on P.cmdId = F.cmdId and P.folderId = F.folderId
			where P.cmdId = %d and P.setId != 0
			order by P.setId
			""" % cmdId

		folders = self.exec_query(dirCmd)
		formsets = self.exec_query(setCmd)
		return folders, formsets

	def close(self):
		self._cursor.connection.close()

	def __is_valid_cmd_id(self, cmdId):
		self.__cursor.execute("select distinct cmdId from PRMSETS")
		return cmdId in tuple(zip(*self.__cursor.fetchall()))[0]
	def get_folder_id(self, cmdId, folderPath):
		if not self.__is_valid_cmd_id(cmdId):
			self.close(ValueError("Номер команды %s не существует" % cmdId))
		self.__cursor.execute("select folderId from Folders where cmdId = %d and path = '%s'" % (cmdId, folderPath))
		folderId = self.__cursor.fetchall()
		folderId = None if not folderId else folderId[0]
		return folderId
	def get_set_id(self, cmdId, title = None, folderPath = None):
		"""	Извлекает номер формы по ее имени и номеру команды и пути """
		if not self.__is_valid_cmd_id(cmdId):
			self.close(ValueError("Номер команды %s не существует" % cmdId))
		cmd = "select P.setId, P.title from PRMSETS P where setId != 0 and cmdId = %d" % cmdId
		if title is not None:		cmd += " and P.title = '%s'" % title
		if folderPath is not None:
			folderId = self.get_folder_id(cmdId, folderPath)
			if folderId is not None:
				cmd += " and P.folderId = %d" % folderId
			else:
				cmd += " and P.folderId = 0"
				# self.close(ValueError("Папка '%s' в базе данных форм не существует" % folderPath))
			#cmd += " and P.folderId = %d" % folderId
		self.__cursor.execute(cmd)
		sets = self.__cursor.fetchall()
		sets = None if not sets else tuple(zip(*sets))[0]
		return sets
	def get_set_title(self, cmdId, setId):
		"""	Извлекает номер формы по ее имени и номеру команды и пути """
		if not self.__is_valid_cmd_id(cmdId):
			self.close(ValueError("Номер команды %s не существует" % cmdId))
		cmd = "select P.title from PRMSETS P where setId = %d and cmdId = %d" % (setId, cmdId)
		self.__cursor.execute(cmd)
		title = self.__cursor.fetchone()
		title = None if not title else title[0]
		return title 

	@property
	def table_names(self):
		self.__cursor.execute("select tbl_name from sqlite_master where type = 'table'")
		return tuple(zip(*self.__cursor.fetchall()))[0]

class DrillholesDb:
	def __init__(self, path):
		if not os.path.exists(path):
			raise FileNotFoundError("Файл '%s' не существует" % path)
		cnxn = sqlite3.connect(path)
		self._cursor = cnxn.cursor()
		self._cursor.execute("PRAGMA journal_mode = memory")
		self._path = path

	def _exec_query(self, *args):
		try:
			self._cursor.execute(*args)
		except Exception as e:
			self.close()
			raise Exception("Невозможно выполнить запрос. %s" % str(e))

	@classmethod
	def create(cls, path):
		if not path.lower().startswith(MMpy.Project.path().lower()):
			raise Exception("Невозможно создать базу данных вне проекта")

		if os.path.exists(path):
			try:
				os.remove(path)
			except Exception as e:
				raise Exception("Невозможно перезаписать файл %s. %s" % (path, str(e)))
		#
		cmd = """
			CREATE TABLE [CollarFiles] (
				[File Name] TEXT(255),
				[Hole ID Field1] TEXT(50),
				[Hole ID Field2] TEXT(50),
				[Hole ID Field3] TEXT(50),
				[East Field] TEXT(50),
				[North Field] TEXT(50),
				[RL Field] TEXT(50),
				[Total Depth Field] TEXT(50),
				[Azimuth Field] TEXT(50),
				[Dip Field] TEXT(50),
				[Filter Number] TEXT(50),
				[Attribute Field1] TEXT(50),
				[Attribute Field2] TEXT(50),
				[Attribute Field3] TEXT(50),
				[Attribute Field4] TEXT(50),
				[Attribute Field5] TEXT(50),
				[Attribute Field6] TEXT(50),
				[Attribute Field7] TEXT(50),
				[Attribute Field8] TEXT(50),
				[Attribute Field9] TEXT(50),
				[Attribute Field10] TEXT(50)
			);
			CREATE TABLE [EventFiles] (
				[File Name] TEXT(255),
				[Hole ID Field1] TEXT(50),
				[Hole ID Field2] TEXT(50),
				[Hole ID Field3] TEXT(50),
				[Depth Field] TEXT(50),
				[Link Field] TEXT(255)
			);
			CREATE TABLE [GenInfo] (
				[Azimuth Correction] FLOAT DEFAULT 0,
				[Correct First Az] BIT DEFAULT 0,
				[Trace Accuracy] FLOAT DEFAULT 0,
				[Trace Last Updated] DATETIME,
				[Database Type] BIT NOT NULL,
				[EXTERNAL LINK INFO] MEMO
			);
			CREATE TABLE [HoleInfo] (
				[Hole ID1] TEXT(255) NOT NULL,
				[Hole ID2] TEXT(255) NOT NULL,
				[Hole ID3] TEXT(255) NOT NULL,
				[Unique Drill Hole Num] INTEGER DEFAULT 0,
				[Minimum Easting] FLOAT DEFAULT 0,
				[Minimum Northing] FLOAT DEFAULT 0,
				[Minimum RL] FLOAT DEFAULT 0,
				[Maximum Easting] FLOAT DEFAULT 0,
				[Maximum Northing] FLOAT DEFAULT 0,
				[Maximum RL] FLOAT DEFAULT 0,
				[CollarEast] FLOAT DEFAULT 0,
				[CollarNorth] FLOAT DEFAULT 0,
				[CollarRL] FLOAT DEFAULT 0,
				[CollarAz] FLOAT DEFAULT 0,
				[CollarDip] FLOAT DEFAULT 0,
				[Total Depth] FLOAT DEFAULT 0,
				[Filtered] BIT DEFAULT 0,
				[Visible] BIT DEFAULT 0,
				[Checksum] FLOAT DEFAULT 0,
				[Attribute 1] TEXT(255),
				[Attribute 2] TEXT(255),
				[Attribute 3] TEXT(255),
				[Attribute 4] TEXT(255),
				[Attribute 5] TEXT(255),
				[Attribute 6] TEXT(255),
				[Attribute 7] TEXT(255),
				[Attribute 8] TEXT(255),
				[Attribute 9] TEXT(255),
				[Attribute 10] TEXT(255),
				[Survey Start Line] INTEGER DEFAULT 0,
				[Survey Total Lines] INTEGER DEFAULT 0,
				PRIMARY KEY ([Hole ID1], [Hole ID2], [Hole ID3])
			);
			CREATE TABLE [HoleTraces] (
				[Unique Drill Hole Num] INTEGER DEFAULT 0,
				[Northing] FLOAT DEFAULT 0,
				[Easting] FLOAT DEFAULT 0,
				[RL] FLOAT DEFAULT 0,
				[Depth] FLOAT DEFAULT 0,
				FOREIGN KEY ([Unique Drill Hole Num]) REFERENCES [HoleInfo] ([Unique Drill Hole Num])
			);
			CREATE TABLE [IntervalFiles] (
				[File Name] TEXT(255),
				[Hole ID Field1] TEXT(50),
				[Hole ID Field2] TEXT(50),
				[Hole ID Field3] TEXT(50),
				[From Field] TEXT(50),
				[To Field] TEXT(50),
				[Link Field] TEXT(255)
			);
			CREATE TABLE [SurveyFiles] (
				[File Name] TEXT(255),
				[Hole ID Field1] TEXT(50),
				[Hole ID Field2] TEXT(50),
				[Hole ID Field3] TEXT(50),
				[Survey Depth Field] TEXT(50),
				[Azimuth Field] TEXT(50),
				[Dip Field] TEXT(50),
				[Filter Number] TEXT(50)
			);
			CREATE TABLE [TrenchFiles] (
				[File Name] TEXT(255),
				[Hole ID Field1] TEXT(50),
				[Hole ID Field2] TEXT(50),
				[Hole ID Field3] TEXT(50),
				[East Field] TEXT(50),
				[North Field] TEXT(50),
				[RL Field] TEXT(50),
				[Depth Field] TEXT(50),
				[Filter Number] TEXT(50)
			);
			CREATE INDEX [CollarFiles.FileName] ON [CollarFiles] ([File Name] ASC);
			CREATE INDEX [EventFiles.FileName] ON [EventFiles] ([File Name] ASC);
			CREATE INDEX [HoleTraces.Depth] ON [HoleTraces] (Depth ASC);
			CREATE INDEX [IntervalFiles.FileName] ON [IntervalFiles] ([File Name] ASC);
			CREATE INDEX [SurvetFiles.FileName] ON [SurveyFiles] ([File Name] ASC);
			CREATE INDEX [TrenchFiles.FileName] ON [TrenchFiles] ([File Name] ASC);
			CREATE UNIQUE INDEX [Hole] ON [HoleInfo] ([Hole ID1] ASC, [Hole ID2] ASC, [Hole ID3] ASC);
			CREATE UNIQUE INDEX [HoleInfo.UniqueHoleNum] ON [HoleInfo] ([Unique Drill Hole Num] ASC);

			INSERT INTO GenInfo VALUES (0, 0, 0, NULL, {0}, NULL);
		""".format('1' if cls.__name__ == 'DrillholesDb' else '0')
		
		cnxn = sqlite3.connect(path)
		cursor = cnxn.cursor()
		cursor.execute("PRAGMA journal_mode = memory")
		cursor.executescript(cmd)
		cnxn.close()

		return cls(path)

	def set_collars(self, collarpath, fldHole, fldEast, fldNorth, fldRl, fldDepth, attributes = []):
		if not collarpath.lower().startswith(MMpy.Project.path().lower()):
			raise Exception("Пытаетесь подсунуть лажу не из проекта")

		path = collarpath[len(MMpy.Project.path()):]
		self._exec_query("select [File Name] from CollarFiles where [File Name] = '%s'" % path)
		exists = self._cursor.fetchone()

		if not exists:
			cmd = """
				INSERT INTO CollarFiles ([File Name], [Hole ID Field1], [East Field], [North Field], [RL Field], [Total Depth Field], [Filter Number])
				VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', -1)""".format(path, fldHole, fldEast, fldNorth, fldRl, fldDepth)
		else:
			cmd = """
				UPDATE [CollarFiles] SET
					[Hole ID Field1] = '{0}',
					[East Field] = '{1}',
					[North Field] = '{2}',
					[RL Field] = '{3}',
				WHERE [File Name] = '{5}'
			""".format(fldHole, fldEast, fldNorth, fldRl, fldDepth, path)
		self._exec_query(cmd)
		
		if attributes:
			attrstring = ",\n".join(["[Attribute Field%d] = '%s'" % (i, attr) for i, attr in enumerate(attributes, start = 1)])
			cmd = "UPDATE [CollarFiles] SET {0} WHERE [File Name] = '{1}'".format(attrstring, path)
			self._exec_query(cmd)

	def set_survey(self, surveypath, fldHole, fldDepth, fldAzimuth, fldDip):
		if not surveypath.lower().startswith(MMpy.Project.path().lower()):
			raise Exception("Пытаетесь подсунуть лажу не из проекта")

		path = surveypath[len(MMpy.Project.path()):]
		self._exec_query("select [File Name] from CollarFiles where [File Name] = '%s'" % path)
		exists = self._cursor.fetchone()
		
		if exists:
			cmd = """
				UPDATE [SurveyFiles] SET
					[Hole ID Field1] = '{0}',
					[Survey Depth Field] = '{1}',
					[Azimuth Field] = '{2}',
					[Dip Field] = '{3}'
				WHERE [File Name] = '{4}'
			""".format(fldHole, fldDepth, fldAzimuth, fldDip, path)
		else:
			cmd = """
				INSERT INTO SurveyFiles ([File Name], [Hole ID Field1], [Survey Depth Field], [Azimuth Field], [Dip Field], [Filter Number])
					VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', -1)
			""".format(path, fldHole, fldDepth, fldAzimuth, fldDip)
		self._exec_query(cmd)

	def add_intervals(self, interpath, fldHole, fldFrom, fldTo):
		if not interpath.lower().startswith(MMpy.Project.path().lower()):
			raise Exception("Пытаетесь подсунуть лажу не из проекта")

		path = interpath[len(MMpy.Project.path()):]
		
		cmd = """
			INSERT INTO IntervalFiles ([File Name], [Hole ID Field1], [From Field], [To Field])
			VALUES ('{0}', '{1}', '{2}', '{3}')
		""".format(path, fldHole, fldFrom, fldTo)
		self._exec_query(cmd)

	def save(self):
		self._cursor.connection.commit()
	def close(self):
		self._cursor.connection.close()
	def refresh(self):
		formset = MMpy.FormSet("DHDB_LINK_UPDATE","16.1.70.0")
		formset.set_field("LINKS_BOOL","1")
		formset.set_field("FILE1", self._path)
		formset.set_field("ALL_BOOL","0")
		formset.run()

	@property
	def has_traces(self):
		self._exec_query("SELECT COUNT(*) RECORDS_COUNT FROM HoleTraces")
		count, = self._cursor.fetchone()
		return count != 0

class TrenchesDb(DrillholesDb):
	def __init__(self, path):
		super(TrenchesDb, self).__init__(path)

	def set_collars (self, *args, **kw):
		raise Exception ("Not implemented")

	def set_survey(self, *args, **kw):
		raise Exception ("Not implemented")

	def add_intervals(self, interpath, fldHole, fldFrom, fldTo):
		super(TrenchesDb, self).add_intervals(interpath, fldHole, fldFrom, fldTo)

	def set_trenches(self, trenchpath, fldHole, fldEast, fldNorth, fldRl, fldDepth, attributes = []):
		if not trenchpath.lower().startswith(MMpy.Project.path().lower()):
			raise Exception("Пытаетесь подсунуть лажу не из проекта")

		path = trenchpath[len(MMpy.Project.path()):]
		self._exec_query("select [File Name] from TrenchFiles where [File Name] = '%s'" % path)
		exists = self._cursor.fetchone()

		if not exists:
			cmd = """
				INSERT INTO TrenchFiles ([File Name], [Hole ID Field1], [East Field], [North Field], [RL Field], [Depth Field], [Filter Number])
				VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', -1)""".format(path, fldHole, fldEast, fldNorth, fldRl, fldDepth)
		else:
			cmd = """
				UPDATE [CollarFiles] SET
					[Hole ID Field1] = '{0}',
					[East Field] = '{1}',
					[North Field] = '{2}',
					[RL Field] = '{3}',
				WHERE [File Name] = '{5}'
			""".format(fldHole, fldEast, fldNorth, fldRl, fldDepth, path)
		self._exec_query(cmd)
		
		if attributes:
			attrstring = ",\n".join(["[Attribute Field%d] = '%s'" % (i, attr) for i, attr in enumerate(attributes, start = 1)])
			cmd = "UPDATE [CollarFiles] SET {0} WHERE [File Name] = '{1}'".format(attrstring, path)
			self._exec_query(cmd)


def create_macro(mcrpath):
	structure = ['ПРОЦЕСС:C:30:0', 'Форма:N:8:0', 'Файл чертежа:C:250:0', 'Файл BMP:C:250:0', 'ПрисоедROP:C:8:0', 'ПАР:C:50:0']
	structure += ['%d:C:250:0' for i in range(1, 31)]
	MicromineFile.create(mcrpath, structure)


class WireframeFormSet(MMpy.FormSet if not DEBUG_MODE else object):
	def __init__(self):
		super(WireframeFormSet, self).__init__("WIREFRAME_SET_DEFINITION","16.0.875.2")
		self.set_field("AND", "0")
		#
		self._wfsetData = tuple()
		self._expressionData = tuple()
		#
		self._displayName = ""
	def gridField(self):
		major, minor, servicepack, build = get_micromine_version()
		if major > 15:
			if (minor > 0 and build > 60) or major == 18:	return "DEF_GRID"
			else:								return "GRID"
		else:									return "GRID"
	#
	def setWireframeSetData(self, wfsetData):
		self._wfsetData = tuple(tuple(row[:]) for row in wfsetData)
		#
		wfSetDataGrid = MMpy.DataGrid(3,1)
		wfSetDataGrid.set_column_info(0,1,11)
		wfSetDataGrid.set_column_info(1,2,8)
		wfSetDataGrid.set_column_info(2,3,9)
		for i, row in enumerate(wfsetData):
			wfSetDataGrid.set_row(i, row)
		#
		self.set_field(self.gridField(), wfSetDataGrid.serialise())
	def setExpressionData(self, expressionData):
		self._expressionData = tuple(tuple(row[:]) for row in expressionData)
		#
		expressionDataGrid = MMpy.DataGrid(2,1)
		expressionDataGrid.set_column_info(0,1,11)
		expressionDataGrid.set_column_info(1,2,-1)
		for i, row in enumerate(expressionData):
			expressionDataGrid.set_row(i, row)
		#
		self.set_field("EXPRESSIONS_GRID", expressionDataGrid.serialise())

	def setAndFlag(self, flag):
		self.set_field("AND", "%d" % int(flag))

	def wireframeSetData(self):					return self._wfsetData
	def expressionData(self):					return self._expressionData
	#
	def useExpressions(self):
		self.set_field("EXPRESSIONS", "1")
		self.set_field("WILDCARDS","0")
	def useWildcards(self):
		self.set_field("EXPRESSIONS", "0")
		self.set_field("WILDCARDS","1")
	#
	def displayName(self):						return self._displayName
	def setDisplayName(self, value):			self._displayName = str(value)

class SvyWireframeFormSet(WireframeFormSet):
	def __init__(self):
		super(SvyWireframeFormSet, self).__init__()
		self._isBackfillSet = False
	def isBackfillSet(self):					return self._isBackfillSet
	def setIsBackfillSet(self, value):			self._isBackfillSet = bool(value)

#f = MicromineFile()
#path = r"D:\BM_REPORT.RPT"
#f.open(path)
#for row in f.read(asstr = False):
#	print (row, ",", sep = "")
#print (f.header)
#f.close()




def isBlockModelFile(path):
	reqfields = ["EAST", "NORTH", "RL", "_EAST", "_NORTH", "_RL"]
	try:
		f = MicromineFile()
		if not f.open(path):
			return False
		header = f.header
		f.close()
		return all(fld in header for fld in reqfields)
	except:
		return False

def isSectionFile(path):
	reqfields = ["NAME", "X_CENTRE", "Y_CENTRE", "Z_CENTRE", "LENGTH", "TOWARDS", "AWAY", "AZIMUTH", "INCLINATION", "ROLL", "STEP"]
	try:
		f = MicromineFile()
		if not f.open(path):
			return False
		header = f.header
		f.close()
		return all(fld in header for fld in reqfields)
	except:
		return False
