import os
from datetime import datetime


from utils.dbconnection import DbConnection
from directions.measure import DirectionMeasure, ControlMeasure, OffsetsMeasure
from utils.constants import *
from utils.validation import is_blank

import geometry as geom
import math
from utils.logger import NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

MTYPE_HT = "HT"
MTYPE_KI = "KI"

def getNextMeasureId(cnxn):
	cmd = """
		SELECT
		CASE
			WHEN (select MAX(MEASURE_ID) from [{0}].[dbo].[MP_MEASURES]) IS NULL
			THEN 1
			ELSE (select MAX(MEASURE_ID) from [{0}].[dbo].[MP_MEASURES])+1
		END MAXID
		""".format(cnxn.dbsvy)
	newid = cnxn.exec_query(cmd).get_row(0)
	return newid

def getNextTraversalDriveId(cnxn):
	cmd = """
		SELECT
		CASE
			WHEN (select MAX(DRIVE_ID) from [{0}].[dbo].[MP_TRAVERSALDRIVES]) IS NULL
			THEN 1
			ELSE (select MAX(DRIVE_ID) from [{0}].[dbo].[MP_TRAVERSALDRIVES])+1
		END MAXID
		""".format(cnxn.dbsvy)
	newid = cnxn.exec_query(cmd).get_row(0)
	return newid

def getNextPointId(cnxn):
	cmd = """
		SELECT
		CASE
			WHEN (select MAX(BASEPOINT_ID) from [{0}].[dbo].[MP_BASE_POINTS]) IS NULL
			THEN 1
			ELSE (select MAX(BASEPOINT_ID) from [{0}].[dbo].[MP_BASE_POINTS])+1
		END MAXID
		""".format(cnxn.dbsvy)
	newid = cnxn.exec_query(cmd).get_row(0)
	return newid

def getNextStationMeasureId(cnxn):
	cmd = """
		SELECT
		CASE
			WHEN (select MAX(STATMEAS_ID) from [{0}].[dbo].[MP_STATIONMEASURES]) IS NULL
			THEN 1
			ELSE (select MAX(STATMEAS_ID) from [{0}].[dbo].[MP_STATIONMEASURES])+1
		END MAXID
		""".format(cnxn.dbsvy)
	newid = cnxn.exec_query(cmd).get_row(0)
	return newid

def canAccessToMultipleRegisterGroup(cnxn):
	cmd = """
			SELECT  count(*) 
			FROM 
			( SELECT [GROUP] FROM [{0}].[dbo].[MP_REGISTER] 
				GROUP BY [GROUP]
			) AS R
			WHERE exists (
				SELECT * FROM fn_my_permissions ('[{0}].[dbo].'+R.[GROUP], 'TYPE')
			)

		""".format(cnxn.dbsvy)

	access_groups_count	= cnxn.exec_query(cmd).get_row(0)
	return access_groups_count > 1 

class SvyPoint(geom.Vector):
	def __init__(self, cnxn, pid = None, cmid = None, cmname = None):
		if not isinstance(cnxn, DbConnection):
			raise Exception("Неверный тип подключения к базе данных")

		super(SvyPoint, self).__init__(0,0,0)

		self._cnxn = cnxn
		self._id = pid
		self._cmid = cmid
		self._cmname = cmname
		self._name = ""
		self._original_name = ""
		self._base_point_group_id = None
		self._class_id = None

		self._isNeedOvercoordinate = False

		self.__initvars()

	def __str__(self):		
		return "SVYPOINT ('%s', %.6f, %.6f, %.6f)" % (self._name, self._x, self._y, self._z)

	def keysEqualTo(self, pt):
		return self._id == pt.id and self._cmid == pt.cmid
	def similarTo(self, pt):
		"""Возвращает True, если имя и координаты совпадают"""
		return self._name == pt.name and self.coordinatesAlmostEqualTo(pt)
	def coordinatesAlmostEqualTo(self, pt, tol = 3):
		"""Возвращает True, если координаты совпадают по умолчанию до третьего знака"""
		if tol is None:
			return self.x == pt.x and self.y == pt.y and self.z == pt.z
		else:
			return round(self.x, tol) == round(pt.x, tol) and round(self.y, tol) == round(pt.y, tol) and round(self.z, tol) == round(pt.z, tol)
	
	def __initvars(self):
		data = None
		if self.id:
			#Galkin
			params = []
			cmd = """
				SELECT
					CREATEMEASURE_ID
					, POINTNAME
					, BPDESC
					, POINTCODE_ID
					, BASE_X, BASE_Y, BASE_Z
					, CREATEMEATYPE_ID, BASEREMOVED
					, BASE_POINT_GROUP_ID 
					, MEASURENAME
					, CLASS_ID
				FROM [{0}].[dbo].[MP_BASE_POINTS] B WHERE BASEPOINT_ID = ?
			""".format(self._cnxn.dbsvy)
			params.append( self.id)

			if  self._cmid is not None:
				cmd += """ AND CREATEMEASURE_ID = ?
				"""
				params.append(self._cmid)

			if  self._cmname is not None:
				cmd += """ AND MEASURENAME = ?
				"""
				params.append(self._cmname)
				 

			if self._cnxn.location is not None:
				cmd += """ AND CREATEMEASURE_ID IN (
					SELECT MEASURE_ID 
					FROM [{0}].[dbo].[MP_MEASURES] M
					WHERE M.LOCATION = ?
				)""".format(self._cnxn.dbsvy)
				params.append(self._cnxn.location)
			#
			data = self._cnxn.exec_query(cmd, params).get_row(0)
			#Galkin
			
		if data:
			self._measure_id = data[0]
			self._name = data[1]
			self._original_name = self._name
			self._description = data[2]
			self._pointcode_id = data[3]
			self._x = round (data[4], 3)		# пришлось округлять, чтобы соответствовало результатам MineScape
			self._y = round (data[5], 3)
			self._z = round (data[6], 3)
			# self._x = data[4]
			# self._y = data[5]
			# self._z = data[6]
			self._type_name = data[7] if data[7] == MTYPE_HT else MTYPE_KI
			self._is_removed = int(data[8])
			self._base_point_group_id = int(data[9])
			self._cmname = 	self._cmname if not self._cmname  is None else data[10]
			self._class_id = 	int(data[11])
		else:
			self._measure_id = None
			self._name = ""
			self._original_name = self._name
			self._description = ""
			self._pointcode_id = 4
			self._x = math.nan
			self._y = math.nan
			self._z = math.nan
			self._is_removed = 0
			self._base_point_group_id = None
			self._class_id = None

	def copy(self):
		pt = SvyPoint(self._cnxn, pid = self.id, cmid = self.cmid, cmname = self.cmname)
		pt.name = self.name
		pt.original_name = self.original_name
		if not self.class_id is None:
			pt.class_id = self.class_id
		pt.description = self.description
		pt.x, pt.y, pt.z = self.x, self.y, self.z


		return pt


	@property
	def description(self):			return self._description
	@description.setter
	def description(self, value):	self._description = str(value)

	

	@property
	def id(self):					return self._id
	@property
	def cmid(self):					return self._cmid if not self._cmid is None else self._measure_id  
	@property
	def cmname(self):					return self._cmname
	@property
	def name(self):					return self._name
	@name.setter
	def name(self, val):			self._name = str(val) if val else ""

	@property
	def original_name(self):					return self._original_name
	@original_name.setter
	def original_name(self, val):			self._original_name = str(val) if val else ""

	@property
	def measure(self):
		if self._type_name == MTYPE_HT:		return DbTraversalMeasure(self._cnxn, self.cmid,  measure_name = self._cmname)
		elif self._type_name == MTYPE_KI:	return DbKeyInMeasure(self._cnxn, self.cmid, self._type_name, self._cmname)
		else: 
			return None



	@property
	def has_permissions(self):
		cmd = """
			SELECT  * 
			FROM [{0}].[dbo].[MP_BASE_POINTS] AS BP
				INNER JOIN [{0}].[dbo].MP_MEASURES AS M
				ON M.MEASURE_ID = BP.CREATEMEASURE_ID
				AND M.MEASURENAME = BP.MEASURENAME
					INNER JOIN [{0}].[dbo].[MP_REGISTER] AS R
					ON R.ID = M.REGISTER_ID
			WHERE exists (
				SELECT * FROM fn_my_permissions ('[{0}].[dbo].'+R.[GROUP], 'TYPE')
			)
			AND BP.BASEPOINT_ID = ?
			AND BP.CREATEMEASURE_ID = ?
		""".format(self._cnxn.dbsvy)
		params = (self.id, self.cmid)
		return self._cnxn.exec_query(cmd, params).get_row(0) is not None

	@property
	def has_coords(self):			return all(map(lambda x: not math.isnan(x), self.coords))
	def delete_coords(self):		self._x, self._y, self._z = math.nan, math.nan, math.nan

	@property
	def code(self):
		return self._pointcode_id
	@code.setter
	def code(self, value):
		# проверяем, что код соответствует кодам маркшейдерской или направленческой точек
		if not value in [4,5]:
			raise ValueError("%s - неверный код точки" % str(value))
		self._pointcode_id = value

	@property
	def is_direction(self):
		return self._pointcode_id in [2,5]
	@is_direction.setter
	def is_direction(self, val):
		if bool(val):			self._pointcode_id = 5
		else:					self._pointcode_id = 4
		
	@property
	def is_removed(self):			return bool(self._is_removed)
	@is_removed.setter
	def is_removed(self, value):	self._is_removed = 1 if not self.id or bool(value) else 0

	@property
	def hnorm(self):				return (self.x**2 + self.y**2)**0.5

	@property
	def exists(self):
		if self.id is None:
			return False
		cmd = """
			SELECT BASEPOINT_ID 
			FROM [{0}].[dbo].[MP_BASE_POINTS] 
			WHERE BASEPOINT_ID = ?
			AND CREATEMEASURE_ID = ?""".format(self._cnxn.dbsvy)
		params = (self.id, self.cmid)
		return self._cnxn.exec_query(cmd, params).get_row(0) is not None
	#@property	 # Используем параметр имени журнала, чтобы проверять наличие имени в их разрезе. Параметр при создании нового направления. 
	def get_name_exists(self, register_name = None ):
		register_id = None
		if not register_name is None:
			cmd = """
				SELECT ID FROM [{0}].[dbo].[MP_REGISTER] as R
				WHERE	NAME = ?
			""".format (self._cnxn.dbsvy)
			params = (	register_name)
			register_id = self._cnxn.exec_query(cmd, params).get_row(0)



		cmd = """
				SELECT BASEPOINT_ID FROM [{0}].[dbo].[MP_BASE_POINTS] as B
					INNER JOIN [{0}].[dbo].[MP_MEASURES] as M
					ON		M.MEASURE_ID = B.CREATEMEASURE_ID
						AND	M.MEASURENAME = B.MEASURENAME 
				WHERE	POINTNAME = ?
					AND M.REGISTER_ID = ?
					AND LOCATION = ? """.format (self._cnxn.dbsvy)
		params = [	self.name,
					register_id if not register_id is None else self.measure.register_id,
					self._cnxn.location]
		if not self.base_point_group_id is None:
			cmd = cmd + """ AND BASE_POINT_GROUP_ID != ? """
			params.append(self.base_point_group_id)
		
		return self._cnxn.exec_query(cmd, params).get_row(0) is not None

	name_exists	 = property(get_name_exists)

	@property
	def name_removed(self):
		cmd = """
		SELECT BASEPOINT_ID FROM [{0}].[dbo].[MP_BASE_POINTS] as B
			LEFT JOIN [{0}].[dbo].[MP_MEASURES] as M
			ON		M.MEASURE_ID = B.CREATEMEASURE_ID
				AND	M.MEASURENAME = B.MEASURENAME 
		WHERE	POINTNAME = ?
			AND BASEREMOVED = 1
			AND LOCATION = ?""".format (self._cnxn.dbsvy)
		params = (self.name, self._cnxn.location)
		return self._cnxn.exec_query(cmd, params).get_row(0) is not None

	def save(self):
		if not self.name:
			raise Exception("Невозможно сохранить точку без имени")
		if self.exists:
			cmd = """
			UPDATE [%s].[dbo].[MP_BASE_POINTS]
			SET POINTNAME = ?, BPDESC = ?, POINTCODE_ID = ?, BASE_X = ?, BASE_Y = ?, BASE_Z = ?, BASEREMOVED = ?, BASE_POINT_GROUP_ID = ?, CLASS_ID	= ?
			WHERE BASEPOINT_ID = ?
			AND CREATEMEASURE_ID = ?
			""" % (self._cnxn.dbsvy)
			params = (self.name, self.description, self.code, self.x, self.y, self.z, self.is_removed, self._base_point_group_id, self.class_id,  self.id, self.cmid )
			self._cnxn.exec_query(cmd, params)

	@property
	def x(self):			return self._x
	@x.setter
	def x(self, value):		self._x = round(float(value), 3) if value else value
	@property
	def y(self):			return self._y
	@y.setter
	def y(self, value):		self._y = round(float(value), 3) if value else value
	@property
	def z(self):			return self._z
	@z.setter
	def z(self, value):		self._z = round(float(value), 3) if value else value

	@property
	def isNeedOvercoordinate(self): return self._isNeedOvercoordinate
	@isNeedOvercoordinate.setter
	def isNeedOvercoordinate(self,value):  self._isNeedOvercoordinate = bool(value)

	@property
	def class_id(self):	 	return self._class_id
	@class_id.setter
	def class_id(self, value) :  	self._class_id = int(value)
	@property
	def class_code(self):
		class_code = None
		if not _class_id is None:
			cmd = """
				SELECT CODE FROM [{0}].[dbo].[MP_CLASS] as C
				WHERE	ID = ?
			""".format (self._cnxn.dbsvy)
			params = (	_class_id)
			class_code = self._cnxn.exec_query(cmd, params).get_row(0)
		return class_code

	@class_code.setter
	def class_code(self,value):
		if not value in ID_CLASS_CODES:
			raise Exception ("Значение не допустимо")
		cmd = """
			SELECT ID FROM [{0}].[dbo].[MP_CLASS] as C
			WHERE	CODE = ?
		""".format (self._cnxn.dbsvy)
		params = (value.strip())
		_class_id = self._cnxn.exec_query(cmd, params).get_row(0)
		self._class_id = int(_class_id)

	@property
	def class_name(self):
		class_name = None
		if not self._class_id is None:
			cmd = """
				SELECT NAME FROM [{0}].[dbo].[MP_CLASS] as C
				WHERE	ID = ?
			""".format (self._cnxn.dbsvy)
			params = (	self._class_id)
			class_name = self._cnxn.exec_query(cmd, params).get_row(0)
		return class_name

	@property
	def original_name(self):					return self._original_name
	@original_name.setter
	def original_name(self, val):			self._original_name = str(val) if val else ""

	@property
	def base_point_group_id(self):
		return self._base_point_group_id
	@base_point_group_id.setter
	def base_point_group_id(self, value):
		self._base_point_group_id = int (value)
	@property
	def base_point_group(self):
		return  DbPointGroup (self._cnxn, self.base_point_group_id)



class DbPointGroup:
	def __init__(self, cnxn, point_group_id):
		if not isinstance(cnxn, DbConnection):
			raise TypeError("Неверный тип подключения к базе данных")
		self._cnxn = cnxn

		self._id = point_group_id
		
		self.__initvars()

	def __initvars(self):
		if self.id:
			cmd = """
			SELECT NAME 
			FROM [{0}].[dbo].[MP_BASE_POINT_GROUP]
			WHERE ID = ?
			""".format(self._cnxn.dbsvy)
			params = (self._id)
			self._name = self._cnxn.exec_query(cmd, params).get_column("NAME")
		else:
			self._name = None

	@property
	def id(self):					return self._id

	@property
	def name(self):					return self._name
	@name.setter
	def name(self, val):			self._name = str(val) if val else ""

	@property
	def exists(self):
		if not self.id:
			return False
		else:
			return True

	def actual_points(self, class_code):
		"""
		Возвращает список всех актуальных точек данной группы, принадлежащих тому же классу
		"""
		if not self.id:
			return False

		if not class_code in ID_CLASS_CODES:
			return False

		cmd = """
		SELECT BASEPOINT_ID, CREATEMEASURE_ID, MEASURENAME 
		FROM  [{0}].[dbo].[MP_BASE_POINTS] AS P
		INNER JOIN  [{0}].[dbo].[MP_CLASS] AS C
		ON C.ID = P.CLASS_ID
		WHERE 
				P.BASE_POINT_GROUP_ID = ?
		AND		C.CODE = ?
		""".format(self._cnxn.dbsvy)
		params = (self.id, class_code)
		data = self._cnxn.exec_query(cmd, params).get_rows()
		result_point_list = []
		for row in data:
			result_point_list.append(SvyPoint(self._cnxn, row[0] ,row[1], row[2]))
		return result_point_list


	
	def save(self):
		if not self.name:
			raise Exception("Невозможно сохранить группу без имени")
		if self.exists:
			cmd = """
			UPDATE [%s].[dbo].[MP_BASE_POINT_GROUP]
			SET NAME = ?
			WHERE ID = ?
			""" % (self._cnxn.dbsvy)
			params = (self.name, self.id)
			self._cnxn.exec_query(cmd, params)
	
	


class DbMeasure:
	def __init__(self, cnxn, measure_id = None, mtype = MTYPE_KI, measure_name = None):
		if not isinstance(cnxn, DbConnection):
			raise TypeError("Неверный тип подключения к базе данных")
		self._cnxn = cnxn

		cmd = """SELECT MEASURE_ID, MEASURENAME FROM [{0}].[dbo].[MP_MEASURES]""".format(self._cnxn.dbsvy)
		if self._cnxn.location is not None:
			cmd += """ WHERE LOCATION = ?"""
			params = (self._cnxn.location)
		#
		measure_ids = self._cnxn.exec_query(cmd, params).get_column("MEASURE_ID")
		measure_names = self._cnxn.exec_query(cmd, params).get_column("MEASURENAME")
		exists = measure_ids and measure_id in measure_ids
		name_exists = measure_names and measure_name in measure_names
	
		self._id = measure_id if exists else None
		self._measurename = measure_name if name_exists else None
		

		if mtype == None and exists:
			params = []
			cmd = """
				SELECT MTYPENAME 
				FROM [{0}].[dbo].[MP_LKP_MEASURETYPES]
				WHERE MTYPE_ID = (
					SELECT MTYPE_ID
					FROM [{0}].[dbo].[MP_MEASURES]
					WHERE MEASURE_ID = ?
					
				)
			""".format(self._cnxn.dbsvy )
			params.append(self._id)
			if mtype == None and name_exists:
				cmd = cmd + """
					AND  MEASURENAME = ?
				"""
				params.append(self._measurename )

			cmd = cmd + """
				)
			"""
			mtype = self._cnxn.exec_query(cmd, params).get_row(0)

		if not mtype in [MTYPE_HT, MTYPE_KI]:
			raise ValueError("Неверный тип измерения")

		self._type_name = mtype
		self.__initvars()

	def __initvars(self):
		if self.id:
			params = []
			cmd = """
			SELECT MEASURENAME, MEASDESC, MEASTIMESTAMP, EXCAVATION_ID, EXECUTOR_ID, INSTRUMENT_ID, MEASREMOVED, REGISTER_ID, UNIT_ID
			FROM [{0}].[dbo].[MP_MEASURES] WHERE MEASURE_ID = ? 
				AND MEASURENAME = ?
				AND MTYPE_ID = (
					SELECT TOP 1 MTYPE_ID 
					FROM [{0}].[dbo].[MP_LKP_MEASURETYPES] 
					WHERE MTYPENAME = ?
			)
			""".format(self._cnxn.dbsvy)
			params.extend([self.id, self._measurename, self._type_name])
			if self._cnxn.location is not None:
				params.append(self._cnxn.location)
				cmd += """ AND LOCATION = ?"""

			data = self._cnxn.exec_query(cmd, params).get_row(0)

			self._name, self._description = data[:2]
			self._timestamp = datetime.strptime(data[2], "%Y-%m-%d")
			self._mine_id = data[3]
			self._surveyor_id = data[4]
			self._equipment_id = data[5]
			self._is_removed = int(data[6])
			self._register_id = data[7]
			self._unit_id = data[8]

			self._new_measurename = self._name
		else:
			self._name, self._description = None, None
			self._timestamp = None
			self._mine_id = None
			self._surveyor_id = None
			self._equipment_id = None
			self._is_removed = 0
			self._register_id = None
			self._unit_id = None

			self._new_measurename = None


	@property
	def id(self):					return self._id
	@property
	def measurename(self):					return self._measurename
	@property
	def exists(self):
		if not self.id:
			return False
		else:
			params = []
			cmd = """
				SELECT MEASURE_ID 
				FROM [{0}].[dbo].[MP_MEASURES] 
				WHERE MEASURE_ID = ?
			""".format(self._cnxn.dbsvy)
			params.append( self.id)
			#
			if self._name is not None:
				cmd += """ AND MEASURENAME = ? """
				params.append( self._measurename)

			#
			if self._cnxn.location is not None:
				cmd += """ AND LOCATION = ? """
				params.append( self._cnxn.location)
			#
			return self._cnxn.exec_query(cmd, params).get_row(0) is not None

	@property
	def name(self):					return self._name
	@name.setter
	def name(self, val):			
		self._name = str(val) if val else ""

	@property
	def new_measurename(self): return self._new_measurename
	@new_measurename.setter
	def new_measurename(self, val):
		self._new_measurename = str(val) if val else ""


	@property
	def description(self):			return self._description
	@description.setter
	def description(self, val):		self._description = str(val) if val else ""

	@property
	def register_id(self):			return self._register_id
	@register_id.setter
	def register_id(self, value):	self._register_id = int(value)

	@property
	def register_name(self):		
		cmd = """
				SELECT NAME FROM [{0}].[dbo].[MP_REGISTER] 
				WHERE	ID = ?
		""".format (self._cnxn.dbsvy)
		params = (self._register_id)
		return self._cnxn.exec_query(cmd, params).get_row(0)

	@property
	def unit_id(self):			return self._unit_id
	@unit_id.setter
	def unit_id(self, value):	self._unit_id = value

	@property
	def unit_name(self):		
		cmd = """
				SELECT PANEL_DESCRIPTION FROM [{0}].[dbo].[PANEL] 
				WHERE	PANEL_ID = ?
		""".format (self._cnxn.dbsvy)
		params = (self._unit_id)
		return self._cnxn.exec_query(cmd, params).get_row(0)

	@property
	def layer_name(self):
		name = None
		if self.unit_id:		
			cmd = """
					SELECT LAYER_DESCRIPTION 
					FROM [{0}].[dbo].[LAYER] AS L
					INNER JOIN [{0}].[dbo].[PANEL] AS PN
					ON PN.LAYER_ID = L.LAYER_ID
					WHERE PANEL_ID = ?
			""".format (self._cnxn.dbsvy)
			params = (self.unit_id)
			name =  self._cnxn.exec_query(cmd, params).get_row(0)

		return name



	@property
	def timestamp(self):			return self._timestamp
	@timestamp.setter
	def timestamp(self, value):
		if not isinstance(value, datetime):
			raise TypeError("%s - неверный тип. Ожидался datetime")
		self._timestamp = value

	@property
	def type_name(self):				return self._type_name
	@property
	def type_id(self):
		cmd = "SELECT MTYPE_ID FROM [%s].[dbo].[MP_LKP_MEASURETYPES] WHERE MTYPENAME = ?" % (self._cnxn.dbsvy)
		params = (self.type_name)
		return self._cnxn.exec_query(cmd, params).get_row(0)

	@property
	def mine_id(self):				return self._mine_id
	@mine_id.setter
	def mine_id(self, id_):
		if id_ is not None:
			params = []
			cmd = """
				SELECT V.VYRID 
				FROM [{0}].[dbo].[VYRAB_P] V
				INNER JOIN [{0}].[dbo].[LOCAL_PROJECT_MINES] AS P_M
				ON P_M.MINE_ID = V.VyrId
				WHERE V.VYRID = ?
			""".format(self._cnxn.dbsvy)
			params.append(id_)
			#
			if self._cnxn.location is not None:
				cmd += """
				AND
				(P_M.LOC_PROJECT_ID IN (
						SELECT PD.LOC_PROJECT_ID 
						FROM [{0}].[dbo].[LOCAL_PROJECT_DEPARTS] PD
						WHERE PD.DepartID IN (
							SELECT D.DepartID 
							FROM [{0}].[dbo].[DEPARTS] D
							WHERE D.DepartPitID IN (
								SELECT P.PitID
								FROM [{0}].[dbo].[PITS] P
								WHERE P.LOCATION = ?
							)
						)
					)

				)""".format(self._cnxn.dbsvy)

					# Убрал из запроса в связи с появлением расшивочной таблицы, возможно там выработок непривязанных к проектам не будет. Галкин.
					#OR
					#V.LOC_PROJECT_ID is null

				params.append(self._cnxn.location)
			#

			invalid = self._cnxn.exec_query(cmd, params).get_row(0) is None
			if invalid:
				raise Exception("Идентификационный номер выработки (ID: %d) не найден в справочнике" % id_)
		self._mine_id = id_
	@property
	def mine_name(self):
		cmd = """
			SELECT VYR 
			FROM [{0}].[dbo].[VYRAB_P] 
			WHERE VYRID = ?
		""".format(self._cnxn.dbsvy)
		params = ( self.mine_id if self.mine_id else -9999)
		return self._cnxn.exec_query(cmd, params).get_row(0)

	@property
	def surveyor_id(self):			return self._surveyor_id
	@surveyor_id.setter
	def surveyor_id(self, id_):
		if id_ is not None:
			cmd = """
				SELECT FIOID 
				FROM [{0}].[dbo].[SURVEY_FIO] 
				WHERE FIOID = ?
			""".format(self._cnxn.dbsvy)
			params = ( id_)
			is_valid = self._cnxn.exec_query(cmd, params).get_row(0)
			if is_valid is None:
				raise Exception("Идентификационный номер маркшейдера (ID: %d) не найден в справочнике" % id_)
		self._surveyor_id = id_
	@property
	def surveyor_name(self):
		cmd = """
			SELECT SURNAME + ' ' + FIRSTNAME + ' ' + SUVMIDDLE 
			FROM [{0}].[dbo].[SURVEY_FIO]
			WHERE FIOID = ?
		""".format(self._cnxn.dbsvy)
		params = (self.surveyor_id if self.surveyor_id is not None else -9999)
		return self._cnxn.exec_query(cmd, params).get_row(0)

	@property
	def equipment_id(self):			return self._equipment_id
	@equipment_id.setter
	def equipment_id(self, id_):
		if id_ is not None:
			cmd = """
				SELECT INSTRUMENT_ID 
				FROM [{0}].[dbo].[MP_LKP_INSTRUMENTS] 
				WHERE INSTRUMENT_ID = ?
			""".format(self._cnxn.dbsvy)
			params = (id_)
			is_valid = self._cnxn.exec_query(cmd, params).get_row(0)
			if is_valid is None:
				raise Exception("Идентификационный номер оборудования (ID: %d) не найден в справочнике" % id_)
		self._equipment_id = id_
	@property
	def equipment_name(self):
		cmd = """
			SELECT INSTNAME 
			FROM [{0}].[dbo].[MP_LKP_INSTRUMENTS] 
			WHERE INSTRUMENT_ID = ?
		""".format(self._cnxn.dbsvy)
		params = (self.equipment_id if self.equipment_id else -9999)
		return self._cnxn.exec_query(cmd, params).get_row(0)

	@property
	def is_removed(self):			return self._is_removed
	@is_removed.setter
	def is_removed(self, value):	self._is_removed = 1 if bool(value) else 0

	def add_point(self, pt):
		if self.id is None:
			raise Exception("Невозможно добавить точку. Необходимо сначала сохранить измерение")

		mtype = self.type_name
		if mtype is None:
			raise Exception("Невозможно добавить точку. Тип измерения не задан")

		if not pt.name:
			raise Exception("Невозможно сохранить точку без имени")

		# if (pt.exists and not pt.is_removed) or (pt.name_exists and not pt.is_removed):
		# 	raise Exception("Невозможно добавить точку '%s'. Точка уже существует" % pt.name)


		if pt.base_point_group_id is None :

			cmd = """
				INSERT INTO [%s].[dbo].[MP_BASE_POINT_GROUP] (NAME)
				VALUES ('')
				""" % (self._cnxn.dbsvy)
			self._cnxn.exec_query(cmd)

			# извлекаем id добавленной выработки
			cmd = """
				select SCOPE_IDENTITY() 
			"""	
			data = self._cnxn.exec_query(cmd).get_rows()
			base_point_group_id = data[0][0]

			cmd = """
				UPDATE  [%s].[dbo].[MP_BASE_POINT_GROUP] 
				SET NAME = ?
				WHERE ID = ?
				""" % (self._cnxn.dbsvy)
			params = (pt.name, base_point_group_id)
			self._cnxn.exec_query(cmd, params)

			pt.base_point_group_id =  base_point_group_id

		pid = getNextPointId(self._cnxn)
		cmd = """
			INSERT INTO [%s].[dbo].[MP_BASE_POINTS] (BASEPOINT_ID, CREATEMEASURE_ID, MEASURENAME, POINTNAME, BPDESC, POINTCODE_ID, CREATEMEATYPE_ID, BASE_X, BASE_Y, BASE_Z, BASEREMOVED,  BASE_POINT_GROUP_ID, CLASS_ID)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?, ?)
			""" % (self._cnxn.dbsvy)
		params = (pid, self.id, self.name, pt.name, pt.description, pt.code, mtype, pt.x, pt.y, pt.z, pt.base_point_group_id, pt.class_id)
		self._cnxn.exec_query(cmd, params)
		return SvyPoint(self._cnxn, pid = pid)

	def save(self):
		if canAccessToMultipleRegisterGroup(self._cnxn):
			raise Exception ("Вы имеете доступ к более чем двум группам доступа журналов. Это не корректная ситуация для сохранения направлений.")

		# если старый замер
		if self.exists:
			cmd = """
				UPDATE [{0}].[dbo].[MP_MEASURES] 
				SET %s 
				WHERE MEASURE_ID = %d AND MTYPE_ID = {1}
			""".format(self._cnxn.dbsvy, self.type_id)
			if self._name is not None:
				cmd += """ AND MEASURENAME = '{0}'""".format(self._measurename)
			#

			columns = ["MEASURENAME", "EXCAVATION_ID", "EXECUTOR_ID", "INSTRUMENT_ID", "MEASDESC", "MEASREMOVED", "REGISTER_ID", "UNIT_ID", "MEASTIMESTAMP"]
			variables = [self.name, self.mine_id, self.surveyor_id, self.equipment_id, self.description, self.is_removed, self.register_id, self.unit_id, self.timestamp.strftime("%Y-%m-%d")]
			values = []
			for var in variables:
				if var is None:			values.append("NULL")
				elif type(var) == str:	values.append("N'%s'" % var)
				else:					values.append("%d" % var)
			updatelist = []
			for column, value in zip(columns, values):
				updatelist.append(" = ".join([column, value]))
			self._cnxn.exec_query(cmd % (",".join(updatelist), self.id))
		# если новый замер
		else:
			if not self.id:
				self._id = getNextMeasureId(self._cnxn)

			cmd = """
			INSERT INTO [{0}].[dbo].[MP_MEASURES] (
					MEASURE_ID,
					MEASURENAME,
					MTYPE_ID,
					EXCAVATION_ID,
					EXECUTOR_ID,
					INSTRUMENT_ID,
					MEASTIMESTAMP,
					MEASDESC,
					MEASREMOVED,
					REGISTER_ID,
					UNIT_ID,
					LOCATION
				)
			VALUES (%s)""".format(self._cnxn.dbsvy)
			variables = (
					self.id, self.name, self.type_id, 
					self.mine_id, self.surveyor_id, self.equipment_id, 
					self.timestamp.strftime("%Y-%m-%d"), self.description,
					self.is_removed, self.register_id, self._unit_id, self._cnxn.location
				)
			values = []
			for var in variables:
				if var is None:			values.append("NULL")
				elif type(var) == str:	values.append("N'%s'" % var)
				else:					values.append("%f" % var)
			self._cnxn.exec_query(cmd % ",".join(values))

	def remove(self):
		if self.exists:
			cmd = """
				DELETE [{0}].[dbo].[MP_MEASURES] 
				WHERE MEASURE_ID = ? AND MEASURENAME = ?
			""".format(self._cnxn.dbsvy)
			params = (self.id, self._measurename)
			self._cnxn.exec_query(cmd, params)
		else:
			raise Exception("Измерение не существует в БД")

	def isPointNameNeedToEdit(self, point):
		"""
		Узнаём необходимо ли минять имя перекоординирующей точки в связи с тем, что такое имя уже есть в текущем журнале
		и эта точка не принадлежит группе перекоорденируемой точки
		"""

		cmd = """
		SELECT * FROM  [{0}].[dbo].[MP_BASE_POINTS]	AS BP
			INNER JOIN [{0}].[dbo].MP_MEASURES AS M
			ON M.MEASURE_ID = BP.CREATEMEASURE_ID
			AND M.MEASURENAME = BP.MEASURENAME
	    WHERE BP.POINTNAME = ?
		AND M.REGISTER_ID = ?
		AND BP.BASEREMOVED = 0
		AND (BP.BASEPOINT_ID != ? OR BP.CREATEMEASURE_ID != ?)
		""".format(self._cnxn.dbsvy)
		params = (point.name, self.register_id, point.id, point.cmid)
		data = self._cnxn.exec_query(cmd, params).get_row(0)
		return True if not data is None else False 


class DbKeyInMeasure(DbMeasure):
	def __init__(self, cnxn, measure_id = None, type_name = None, measure_name = None):
		if type_name == None:
			mtype = MTYPE_KI if measure_id is None else None
		else:
			mtype = type_name
		super(DbKeyInMeasure, self).__init__(cnxn, measure_id, mtype, measure_name )

		self._pt = SvyPoint(self._cnxn)

	@property
	def point(self):		return self._pt
	@point.setter
	def point(self, pt):	self._pt = pt

	def save(self):
		if not self._pt:
			return

		# если точка не существует, то добавляем новое измерение (с суффиксом KI) и саму точку
		if not self._pt.exists:
			if not self.name.upper().endswith("_%s" % self._type_name):
				self._name += "_%s" % self._type_name

			super(DbKeyInMeasure, self).save()
			self.add_point(self._pt)
		# если точка существует, то обновляем старое измерение и саму точку
		else:
			super(DbKeyInMeasure, self).save()
			self._pt.save()

class DbOffsetsMeasure:
	def __init__(self, cnxn, traversal_measure):
		self._cnxn = cnxn
		self._parent = traversal_measure
		self._measure = []

		self.__initvars()

	def __initvars(self):
		if self._parent.id:
			cmd = """
				SELECT
					TD.STANDPOINT_ID,
					B1.CREATEMEASURE_ID as STANDPOINT_MID,
					B1.MEASURENAME as STANDPOINT_NAME,
					B1.CREATEMEATYPE_ID as STANDPOINT_CMTYPE, 
					TD.DIRPOINT_ID, 
					B1.CREATEMEASURE_ID as DIRPOINT_MID,
					B1.MEASURENAME as DIRPOINT_NAME,
					B1.CREATEMEATYPE_ID as DIRPOINT_CMTYPE, 
					TD.STARTCENTRE_X, TD.STARTCENTRE_Y, TD.STARTCENTRE_Z, TD.ENDCENTRE_X, TD.ENDCENTRE_Y, TD.ENDCENTRE_Z,
					TD.STEPSIZE, TD.MAXDISTANCE,
					TD.DRIVEWIDTH, TD.UPOFFSET, TD.DOWNOFFSET,
					TD.BACKSIGHT, 
					TD.OFFSETOLER,
					TD.SEQUENCE_NUM
				FROM [{0}].[dbo].[MP_TRAVERSALDRIVES] as TD
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B1 
				ON B1.BASEPOINT_ID = TD.STANDPOINT_ID
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B2 
				ON B2.BASEPOINT_ID = TD.DIRPOINT_ID
			    WHERE TD.MEASURE_ID = ?  and TD.MEASURENAME = ?
			    ORDER BY TD.SEQUENCE_NUM ASC
			""".format (self._cnxn.dbsvy)
			params = (self._parent.id, self._parent.measurename)

			
			rows = self._cnxn.exec_query(cmd, params).get_rows()

			if rows and rows[0]:
				for row in rows:
					stpt, fwpt = SvyPoint(self._cnxn, row[0], row[1], row[2]), SvyPoint(self._cnxn, row[4], row[5], row[6])
					startpt, endpt = geom.Point(*row[8:11]), geom.Point(*row[11:14])
					step = row[14]
					maxdist = row[15]
					width = row[16]
					uoff, doff = row[17:19]
					is_reverse = row[19]
					tolerance = row[20]
					seq_num = row[21]
					self._measure.append(OffsetsMeasure(stpt, fwpt, startpt, endpt, maxdist, step, width, uoff, doff,
					                               is_reverse, tolerance, seq_num))
			else:
				stpt, fwpt = SvyPoint(self._cnxn), SvyPoint(self._cnxn)
				startpt, endpt = None, None
				step = None
				maxdist = None
				width = None
				uoff, doff = None, None
				is_reverse = False
				tolerance = 1
				self._measure.append(OffsetsMeasure(stpt, fwpt, startpt, endpt, maxdist, step, width, uoff, doff, is_reverse, tolerance))
		else:
			stpt = SvyPoint(self._cnxn)
			fwpt = SvyPoint(self._cnxn)
			self._measure.append(OffsetsMeasure(stpt, fwpt))

	@property
	def measure(self):
		return self._measure
	@measure.setter
	def measure(self, m):
		for el in m:
			if isinstance(el, OffsetsMeasure):
				raise TypeError("%s - неверный тип измерения. Ожидался OffsetMeasure" % type(el))
		self._measure = m
	def delete(self):
		cmd = "DELETE FROM [%s].[dbo].[MP_TRAVERSALDRIVES] WHERE MEASURE_ID = ?" % (self._cnxn.dbsvy)
		params = (self._parent.id)
		self._cnxn.exec_query(cmd, params)
	def save(self):
		if self._parent.name is None:
			raise Exception("Невозможно сохранить измерение. Не задано имя измерения")
		self.delete()
		cmd = """
		INSERT INTO [{0}].[dbo].[MP_TRAVERSALDRIVES] (
				DRIVE_ID, MEASURE_ID, MEASURENAME,
				STANDPOINT_ID, DIRPOINT_ID,
				BACKSIGHT,
				STARTCENTRE_X, STARTCENTRE_Y, STARTCENTRE_Z,
				ENDCENTRE_X, ENDCENTRE_Y, ENDCENTRE_Z,
				STEPSIZE,
				MAXDISTANCE,
				DRIVEWIDTH,
				UPOFFSET, DOWNOFFSET,
				OFFSETOLER, SEQUENCE_NUM
			)
		VALUES (%s)""".format(self._cnxn.dbsvy)

		for m in self._measure:
			did = getNextTraversalDriveId(self._cnxn)
			variables = (did, self._parent.id, self._parent.name, m.standpoint.id, m.dirpoint.id, m.is_reverse,
						m.startpoint.x if m.startpoint else 0, m.startpoint.y if m.startpoint else 0, m.startpoint.z if m.startpoint else 0,
						m.endpoint.x if m.endpoint else 0, m.endpoint.y if m.endpoint else 0, m.endpoint.z if m.endpoint else 0,
						m.step, m.maxdist, m.width, m.uoffset, m.doffset, m.tolerance, m.seq_num)
			values = []
			for var in variables:
				if var is None:			values.append("NULL")
				elif type(var) == str:	values.append("N'%s'" % var)
				else:					values.append("%f" % var)

			self._cnxn.exec_query(cmd % ",".join(values))

class DbControlMeasure:
	def __init__(self, cnxn, traversal_measure, type_name = None):
		self._cnxn = cnxn
		self._parent = traversal_measure
		self._type_name = type_name
		self.__initvars()

	def __initvars(self):
		if self._parent.id:
			cmd = """
				SELECT
					TM.CTRLSTANDPOINT_ID,
					B1.CREATEMEASURE_ID as CTRLSTANDPOINT_MID,
					B1.MEASURENAME as CTRLSTANDPOINT_NAME,
					B1.CREATEMEATYPE_ID as CTRLSTANDPOINT_CMTYPE, 
					TM.CTRLBACKPOINT_ID,
					B2.CREATEMEASURE_ID as CTRLBACKPOINT_MID,
					B2.MEASURENAME as CTRLBACKPOINT_NAME,
					B2.CREATEMEATYPE_ID as CTRLBACKPOINT_CMTYPE,
					TM.CTRLDIRPOINT_ID,
					B3.CREATEMEASURE_ID as CTRLDIRPOINT_MID,
					B3.MEASURENAME as CTRLDIRPOINT_NAME,
					B3.CREATEMEATYPE_ID as CTRLDIRPOINT_CMTYPE,
					TM.CTRLHORIZANGLE,
					CASE
						WHEN TM.VERTANGORIGIN = 90
						THEN TM.CTRLVERTANGLE
						ELSE 90 - TM.CTRLVERTANGLE
					END CTRLVERTANGLE,
					TM.CTRLSLOPEDIST,
					TM.CTRLOFFSETINSTR,
					TM.CTRLOFFSETTARGET
				FROM [{0}].[dbo].[MP_TRAVERSALMEASURES] as TM
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B1 
				ON B1.BASEPOINT_ID = TM.CTRLSTANDPOINT_ID
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B2 
				ON B2.BASEPOINT_ID = TM.CTRLBACKPOINT_ID
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B3 
				ON B3.BASEPOINT_ID = TM.CTRLDIRPOINT_ID
				WHERE TM.MEASURE_ID = ?  and TM.MEASURENAME = ?
			""".format (self._cnxn.dbsvy)
			params = (self._parent.id, self._parent.measurename)

			row = self._cnxn.exec_query(cmd, params).get_row(0)

			stpt, bkpt, fwpt = SvyPoint(self._cnxn, row[0], row[1], row[2]), SvyPoint(self._cnxn, row[4], row[5], row[6]), SvyPoint(self._cnxn, row[8], row[9], row[10])
			hang, vang = row[12:14]	#3:5
			vdist = row[14] #5
			ioff, voff = row[15:17] #6:8

			self._measure = ControlMeasure(stpt, bkpt, fwpt, hang, vang, vdist, ioff, voff)
		else:
			stpt = SvyPoint(self._cnxn)
			bkpt = SvyPoint(self._cnxn)
			fwpt = SvyPoint(self._cnxn)
			self._measure = ControlMeasure(stpt, bkpt, fwpt)

	@property
	def measure(self):
		return self._measure
	@measure.setter
	def measure(self, m):
		if type(m) != ControlMeasure:
			raise Exception("%s- неверный тип измерения. Ожидался ControlMeasure" % str(type(m)))
		self._measure = m
	def delete(self):
		cmd = "DELETE FROM [%s].[dbo].[MP_TRAVERSALMEASURES] WHERE MEASURE_ID = ?" % (self._cnxn.dbsvy)
		params = (self._parent.id)
		self._cnxn.exec_query(cmd, params)
	def save(self):
		if self._parent.name is None:
			raise Exception("Невозможно сохранить измерени. Не задано имя измерения")
		self.delete()

		m = self._measure
		variables = (self._parent.id, self._parent.name, m.standpoint.id, m.backpoint.id, m.dirpoint.id, m.hangle, m.vangle, m.vdist, m.ioffset, m.voffset)
		values = []
		for var in variables:
			if var is None:			values.append("NULL")
			elif type(var) == str:	values.append("N'%s'" % var)
			else:					values.append("%f" % var)
		values.append("90")
		
		cmd = """
		INSERT INTO [{0}].[dbo].[MP_TRAVERSALMEASURES]
			(MEASURE_ID, MEASURENAME, CTRLSTANDPOINT_ID, CTRLBACKPOINT_ID, CTRLDIRPOINT_ID, CTRLHORIZANGLE, CTRLVERTANGLE, CTRLSLOPEDIST, CTRLOFFSETINSTR, CTRLOFFSETTARGET, VERTANGORIGIN)
		VALUES (%s)""".format(self._cnxn.dbsvy) % ",".join(values)
		self._cnxn.exec_query(cmd)

class DbStationMeasure:
	def __init__(self, cnxn, traversal_measure):
		self._cnxn = cnxn
		self._parent = traversal_measure

		self.__initvars()

	def __initvars(self):
		if self._parent.id:
			cmd = """
				SELECT
					SM.STANDPOINT_ID,
					B1.CREATEMEASURE_ID as STANDPOINT_MID,
					B1.MEASURENAME as STANDPOINT_NAME,
					B1.CREATEMEATYPE_ID as STANDPOINT_CMTYPE, 
					SM.BACKPOINT_ID,
					B2.CREATEMEASURE_ID as BACKPOINT_MID,									
					B2.MEASURENAME as BACKPOINT_NAME,
					B2.CREATEMEATYPE_ID as BACKPOINT_CMTYPE, 
					SM.NEWPOINT_ID,
					B3.CREATEMEASURE_ID as NEWPOINT_MID,
					B3.MEASURENAME as NEWPOINT_NAME,
					B3.CREATEMEATYPE_ID as NEWPOINT_CMTYPE, 
					SM.HORIZONTALANGLE,
					SM.SLOPEDISTANCE,
					CASE
						WHEN SM.VERTANGORIGIN = 90
						THEN SM.VERTICALANGLE
						ELSE 90 - SM.VERTICALANGLE
					END VERTICALANGLE,
					SM.OFFSETINSTR,
					SM.OFFSETTARGET,
					CASE
						WHEN SM.VERTANGORIGIN = 90
						THEN SM.VERTANGFRNEWPNT
						ELSE 90 - SM.VERTANGFRNEWPNT
					END VERTANGFRNEWPNT,
					SM.SLOPDISTFRNEWPNT,
					SM.OFFSETFROMINST,
					SM.OFFSETFROMTRGET
				FROM [{0}].[dbo].[MP_STATIONMEASURES] as SM
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B1 
				ON B1.BASEPOINT_ID = SM.STANDPOINT_ID and B1.CREATEMEASURE_ID = SM.MEASURE_ID
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B2 
				ON B2.BASEPOINT_ID = SM.BACKPOINT_ID   and B2.CREATEMEASURE_ID = SM.MEASURE_ID	
				LEFT JOIN [{0}].[dbo].[MP_BASE_POINTS] as B3 
				ON B3.BASEPOINT_ID = SM.NEWPOINT_ID	and B3.CREATEMEASURE_ID = SM.MEASURE_ID	
				WHERE SM.MEASURE_ID = ? 
				and SM.MEASURENAME = ?
				ORDER BY SM.SEQ_NO""".format (self._cnxn.dbsvy)
			params = (self._parent.id, self._parent.measurename)


			self._measures = []
			data = self._cnxn.exec_query(cmd, params).get_rows()
			for row in self._cnxn.exec_query(cmd, params).get_rows():
				stpt, bkpt, fwpt = SvyPoint(self._cnxn, row[0], row[1], row[2]), SvyPoint(self._cnxn, row[4], row[5], row[6]), SvyPoint(self._cnxn, row[8], row[9], row[10])
				# прямые измерения
				hang, vang = row[12], row[14]
				vdist = row[13]	#4
				ioff, voff = row[15], row[16]
				# обратные измерения
				vangInv = row[17]  #8
				vdistInv = row[18]  #9
				ioffInv, voffInv = row[19:21] #10:12 

				self._measures.append(DirectionMeasure(stpt, bkpt, fwpt, hang, vang, vdist, ioff, voff, 0, vdistInv, vangInv, ioffInv, voffInv))
		else:
			stpt = SvyPoint(self._cnxn)
			bkpt = SvyPoint(self._cnxn)
			fwpt = SvyPoint(self._cnxn)
			self._measures = [DirectionMeasure(stpt, bkpt, fwpt)]
			# self._measures = []

	@property
	def measures_count(self):
		return len(self._measures)
	def _add_measure(self, m):
		if type(m) != DirectionMeasure:
			raise TypeError("%s - неверный тип измерения. Ожидался DirectionMeasure")
		if not m.calc_coords():
			raise ValueError("Невозможно добавить измерение, не имеющего результата")
		#
		# prev_measure = self._measures[-1] if self.measures_count > 0 else None
		# if not prev_measure or (prev_measure.dirpoint.id == m.standpoint.id and prev_measure.standpoint.id == m.backpoint.id):
		self._measures.append(m)
		# else:
		# 	raise Exception("Невозможно добавить измерение. Точки измерения не соответствуют предыдущему измерению")
	@property
	def measures(self):
		return tuple(self._measures)
	@measures.setter
	def measures(self, mlist):
		if not type(mlist) in [list, tuple]:
			raise TypeError("%s - неверный тип. Ожидался list или tuple" % str(type(mlist)))
		if not all(map(lambda x: type(x) == DirectionMeasure, mlist)):
			raise TypeError("Не все измерения имеют тип DirectionMeasure")

		self._measures = []
		for m in mlist:
			self._add_measure(m)
	def delete_measure(self, seqno):
		if seqno > 0:	self._measures = self._measures[:seqno]
		else:			self._measures = self._measures[:1]

	def save(self):
		if self._parent.name is None:
			raise Exception("Невозможно сохранить измерение. Не задано имя измерения")

		cmd = "DELETE FROM [%s].[dbo].[MP_STATIONMEASURES] WHERE MEASURE_ID = ?" % (self._cnxn.dbsvy)
		params = (self._parent.id)
		self._cnxn.exec_query(cmd, params)

		#
		cmd = """
		INSERT INTO [{0}].[dbo].[MP_STATIONMEASURES] (
				STATMEAS_ID, MEASURE_ID, MEASURENAME,
				SEQ_NO,
				STANDPOINT_ID, BACKPOINT_ID, NEWPOINT_ID,
				HORIZONTALANGLE, SLOPEDISTANCE, VERTICALANGLE,
				OFFSETINSTR, OFFSETTARGET,
				VERTANGFRNEWPNT, SLOPDISTFRNEWPNT,
				OFFSETFROMINST,	OFFSETFROMTRGET,
				VERTANGORIGIN
			)
		VALUES (%s)""".format(self._cnxn.dbsvy)		
		
		for seqno, m in enumerate(self._measures, start = 1):
			mid = getNextStationMeasureId(self._cnxn)

			variables = (mid, self._parent.id, self._parent.name, seqno, m.standpoint.id, m.backpoint.id, m.dirpoint.id,
									m.hangle, m.vdist, m.vangle, m.ioffset, m.voffset, m.vangleInv, m.vdistInv, m.ioffsetInv, m.voffsetInv)
			values = []
			for var in variables:
				if var is None:			values.append("NULL")
				elif type(var) == str:	values.append("N'%s'" % var)
				else:					values.append("%f" % var)
			values.append("90")
			self._cnxn.exec_query(cmd % ",".join(values))		

class DbTraversalMeasure(DbMeasure):
	def __init__(self, cnxn, measure_id = None, measure_name = None):
		mtype = MTYPE_HT
		super(DbTraversalMeasure, self).__init__(cnxn, measure_id, mtype, measure_name = measure_name)

		self._control_measure = DbControlMeasure(self._cnxn, self)
		self._station_measure = DbStationMeasure(self._cnxn, self)
		self._offsets_measure = DbOffsetsMeasure(self._cnxn, self)

		self._needs_offsets = False

	@property
	def offsets_measure(self):		return self._offsets_measure
	@property
	def control_measure(self):		return self._control_measure
	@property
	def station_measure(self):		return self._station_measure

	@property
	def needs_offsets(self):		return self._needs_offsets
	@needs_offsets.setter
	def needs_offsets(self, val):	self._needs_offsets = bool(val)

	def save(self):
		if canAccessToMultipleRegisterGroup(self._cnxn):
			raise Exception ("Вы имеете доступ к более чем двум группам доступа журналов. Это не корректная ситуация для сохранения направлений.")

		if not self.exists:
			self.name = self.station_measure.measures[-1].standpoint.name
		else:
			self.new_measurename = self.station_measure.measures[-1].standpoint.name

		super(DbTraversalMeasure, self).save()

		self._control_measure.save()
		# сохранение точек
		n = len(self._station_measure.measures)
		for i, m in enumerate(self._station_measure.measures):
			# print ("Measure %d" % (i+1))
			# print (m.backpoint, m.backpoint.id)
			# print (m.standpoint, m.standpoint.id)
			# print (m.dirpoint, m.dirpoint.id)
			# print ()
			# если не существует или помечена удаленной, тогда добавляем новую точку
			if not self._station_measure.measures[i].dirpoint.exists or self._station_measure.measures[i].dirpoint.isNeedOvercoordinate :
				# Если точка не новая, то пометить не актуальными все точки данной группы, если это точка журнала к которому есть доступ
				if  self._station_measure.measures[i].dirpoint.exists:
					all_actual_points = self._station_measure.measures[i].dirpoint.base_point_group.actual_points(ID_CLASS_CODE_CURRENT)
					for pt in all_actual_points:
						if  pt.has_permissions and not pt.is_removed:
							pt.is_removed = True
							pt.save()

				# Делаем добавляемую точку текущей
				# Добавить новую точку
				# Связать точки в группу
				self._station_measure.measures[i].dirpoint.class_code = ID_CLASS_CODE_CURRENT
				self._station_measure.measures[i].dirpoint = self.add_point(self._station_measure.measures[i].dirpoint)
				if i < n-1:
					self._station_measure.measures[i+1].backpoint = self._station_measure.measures[i].dirpoint
				print (self._station_measure.measures[i].dirpoint, "added")
			# в противном случае обновляем
			else:
				print (self._station_measure.measures[i].dirpoint, "saved")
				self._station_measure.measures[i].dirpoint.save()
			if i < n-1:
				self._station_measure.measures[i+1].standpoint = self._station_measure.measures[i].dirpoint#.copy()
			# меняем тип точек стояния на маркшейдерский
			m.standpoint.code = 4

		#

		# for m in self._station_measure.measures:
		# 	print (m.backpoint, m.backpoint.id)
		# 	print (m.standpoint, m.standpoint.id)
		# 	print (m.dirpoint, m.dirpoint.id)
		# 	print ()
		#
		self._station_measure.save()

		if self.needs_offsets:
			# обновляем точки в измерении домеров (т.к. добавили в БД только на предыдущем шаге)
			self._offsets_measure.measure[0].standpoint = self._station_measure.measures[-1].standpoint#.copy()
			self._offsets_measure.measure[0].dirpoint = self._station_measure.measures[-1].dirpoint#.copy()
			self._offsets_measure.save()
		
		# Переименовывание после всех остальных операций с направлениями, т.к. имя направления  - ключевое поле
		# Поле MEASURENAME обновляется в FOREIGN KEY связанных таблиц каскадно
		if not self.new_measurename is None and not is_blank(self.new_measurename)  and self.name != self.new_measurename:
			self.name = self.new_measurename
			super(DbTraversalMeasure, self).save()
