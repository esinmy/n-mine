try:			import MMpy
except:			pass

import math
import geometry as geom
from mm.mmutils import MicromineFile
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()

class OffsetsMeasure:
	"""Класс для вычисления домеров"""
	def __init__(self, stpt, fwpt, startpt = None, endpt = None, maxdist = None,	
				step = None, width = None, uoff = None, doff = None, reverse = False, tolerance=1, seq_num=1):
		self._stpt = stpt 			# точка стояния
		self._fwpt = fwpt 			# направленческая точка

		self._startpt = startpt 	# начальная точка проекта
		self._endpt = endpt 		# конечная точка проекта
		
		self._maxdist = maxdist 	# максимальное расстояние
		self._step = step 			# шаг, через который считаются домеры

		self._width = width 		# ширина выработки
		self._uoff = uoff			# смещение вверх от оси выработки
		self._doff = doff 			# смещение вниз от оси выработки

		self._seq_num = seq_num
		self._is_reverse = reverse
		self._tolerance = tolerance

	@property
	def standpoint(self):			return self._stpt
	@standpoint.setter
	def standpoint(self, pt):		self._stpt = pt
	@property
	def dirpoint(self):				return self._fwpt
	@dirpoint.setter
	def dirpoint(self, pt):			self._fwpt = pt

	@property
	def startpoint(self):			return self._startpt
	@startpoint.setter
	def startpoint(self, pt):		self._startpt = pt
	@property
	def endpoint(self):				return self._endpt
	@endpoint.setter
	def endpoint(self, pt):			self._endpt = pt

	@property
	def maxdist(self):				return self._maxdist
	@maxdist.setter
	def maxdist(self, value):		self._maxdist = value
	@property
	def step(self):					return self._step
	@step.setter
	def step(self, value):			self._step = value
	@property
	def width(self):				return self._width
	@width.setter
	def width(self, value):			self._width = value
	@property
	def uoffset(self):				return self._uoff
	@uoffset.setter
	def uoffset(self, value):		self._uoff = value
	@property
	def doffset(self):				return self._doff
	@doffset.setter
	def doffset(self, value):		self._doff = value

	@property
	def is_reverse(self):			return self._is_reverse
	@is_reverse.setter
	def is_reverse(self, value):	self._is_reverse = bool(value)

	@property
	def tolerance(self):			return self._tolerance
	@tolerance.setter
	def tolerance(self, value):		self._tolerance = value

	@property
	def seq_num(self):              return self._seq_num
	@seq_num.setter
	def seq_num(self, num):         self._seq_num = num
	#
	def calculate(self):
		variables = [self._stpt, self._fwpt, self._startpt, self._endpt, self._maxdist, self._step, self._width, self._uoff, self._doff, self._tolerance]
		if any(map(lambda x: x is None, variables)):
			if self._stpt and self._fwpt:
				outdata = []
				outdata.append(list(self._stpt.coords) + [0, "", "МТ_%s" % self._stpt.name if not self._stpt.name.upper().startswith("МТ") else self._stpt.name, ""])
				outdata.append(list(self._stpt.coords) + [0, "", "МТ_%s" % self._stpt.name if not self._stpt.name.upper().startswith("МТ") else self._stpt.name, ""])
				outdata.append(list(self._fwpt.coords) + [1, "", self._fwpt.name, ""])
				outdata.append(list(self._fwpt.coords) + [1, "", self._fwpt.name, ""])
				return outdata
			elif not self._stpt:
				raise Exception("Не задана точка стояния")
			else:
				raise Exception("Не задана точка направления")

		# направление
		maxdist = self._maxdist
		is_reversed = self._is_reverse
		# параметры выработки
		width = self._width
		uoffset = self._uoff
		doffset = self._doff
		step = self._step
		tolerance = self._tolerance

		height = uoffset + doffset

		standpoint = self._stpt.copy()
		dirpoint = self._fwpt.copy()

		floorStartPoint = self._startpt.copy()
		floorEndPoint = self._endpt.copy()
		floorAxis = geom.LineString([floorStartPoint, floorEndPoint])

		roofStartPoint, roofEndPoint = floorStartPoint.copy(), floorEndPoint.copy()
		roofStartPoint.z += height
		roofEndPoint.z += height

		projectVector = floorEndPoint - floorStartPoint

		# борта
		leftWall = floorAxis.parallel_offset(width / 2, "left")
		rightWall = floorAxis.parallel_offset(width / 2, "right")
		floorPolygon = geom.SimplePolygon([leftWall[0], leftWall[1], rightWall[0], rightWall[1]])

		#
		v1 = floorEndPoint - floorStartPoint
		v2 = rightWall[0] - floorStartPoint

		# плоскости
		normal = v1**v2
		# плоскость, соответствующая подошве выработки
		roofPlane = geom.Plane(normal, point = roofStartPoint)
		# плоскость, соответствующая кровле выработки
		floorPlane = geom.Plane(normal, point = floorStartPoint)

		# маркшейдерский висок
		svyPlumb = floorPlane.intersection(geom.Line(standpoint, vector = geom.Vector(0,0,1)))
		# направленческиий висок
		dirPlumb = floorPlane.intersection(geom.Line(dirpoint, vector = geom.Vector(0,0,1)))

		# вектор направления
		directionVector = dirPlumb - svyPlumb
		if is_reversed:
			directionVector = -directionVector
		# обрезаем ось направления границами проектной выработки
		directionAxis = floorPolygon.intersection(geom.LineString([svyPlumb, svyPlumb + maxdist*directionVector.unit]))
		if directionAxis is None:
			raise Exception("Невозможно найти пересечение проекта с направлением")

		lastPoint = directionAxis.point2

		# вывод
		outdata = []
		join = 1
		# если пересечение линии направления и полигона выработки пусто,
		# например, если ошибочно выбрали опцию "Направление назад", то пропускаем
		# вычисление домеров и возвращаем только точки, проектную ось и выработку
		if directionAxis is not None:
			# вставляем точки через заданное расстояние
			directionAxis = directionAxis.insert_points(distance = step)

			# вычисление домеров
			offsets = {"AXIS": [], "RIGHT": [], "LEFT": [], "UP": [], "DOWN": []}
			# удлиняем борта, чтобы проекции на концах отрезков вычислялись корректно
			leftWallLong = geom.LineString([(leftWall[0] - projectVector).to_point(), (leftWall[1] + projectVector).to_point()])
			rightWallLong = geom.LineString([(rightWall[0] - projectVector).to_point(), (rightWall[1] + projectVector).to_point()])
			for i, pt in enumerate(directionAxis):
				lpt = leftWallLong.projection(pt)
				rpt = rightWallLong.projection(pt)
				# вертикальная линиая, проходящая через текущую точку
				vline = geom.Line(pt, vector = geom.Vector(0,0,1))
				upt = roofPlane.intersection(vline)
				dpt = floorPlane.intersection(vline)

				offsets["AXIS"].append(pt)
				offsets["LEFT"].append(lpt)
				offsets["RIGHT"].append(rpt)
				offsets["UP"].append(upt)
				offsets["DOWN"].append(dpt)

			for i in range(len(offsets["AXIS"])):
				pt = offsets["AXIS"][i]
				lpt, rpt = offsets["LEFT"][i], offsets["RIGHT"][i]
				upt, dpt = offsets["UP"][i], offsets["DOWN"][i]

				apt = pt.copy()
				apt.z += doffset
				lpt.z, rpt.z = lpt.z + doffset, rpt.z + doffset

				loff = str(round(apt.distance(lpt), tolerance))
				roff = str(round(apt.distance(rpt), tolerance))

				outdata.extend([list(apt.coords) + [join, "", loff, "ДОМЕР_ВЛЕВО"], list(lpt.coords) + [join, "", loff, "ДОМЕР_ВЛЕВО"]])
				outdata.extend([list(apt.coords) + [join+1, "", roff, "ДОМЕР_ВПРАВО"], list(rpt.coords) + [join+1, "", roff, "ДОМЕР_ВПРАВО"]])
				outdata.extend([list(apt.coords) + [join+2, "", "", "ДОМЕР_ВВЕРХ"], list(upt.coords) + [join+2, "", "", "ДОМЕР_ВВЕРХ"]])
				outdata.extend([list(apt.coords) + [join+3, "", "", "ДОМЕР_ВНИЗ"], list(dpt.coords) + [join+3, "", "", "ДОМЕР_ВНИЗ"]])
				join += 4

			for i, pt in enumerate(directionAxis):
				directionAxis[i].z += doffset
			directionAxisList = directionAxis.to_list()
			if lastPoint.x != directionAxisList[-1][0] or lastPoint.y != directionAxisList[-1][1]: 
				directionAxisList.append([lastPoint.x, lastPoint.y, lastPoint.z + doffset])
			outdata.extend([row + [join, "", "", "ОСЬ_УСЛОВНАЯ"] for row in directionAxisList])
			join += 1

		outdata.extend([row + [join, "", "", "ПРОЕКТ_ВЫР-КА"] for row in floorPolygon.to_list()])
		join += 1

		outdata.extend([row + [join, "", "", "ОСЬ_ПОЧВА"] for row in floorAxis.to_list()])
		join += 1

		#
		svyPlumb.z += doffset
		outdata.append(list(standpoint.coords) + [join, "", "МТ_%s" % self._stpt.name if not self._stpt.name.upper().startswith("МТ") else self._stpt.name, "ВИСОК_МАРК"])
		outdata.append(list(svyPlumb.coords) + [join, "", "МТ_%s" % self._stpt.name if not self._stpt.name.upper().startswith("МТ") else self._stpt.name, "ВИСОК_МАРК"])
		join += 1
		dirPlumb.z += doffset
		outdata.append(list(dirpoint.coords) + [join, "", self._fwpt.name, "ВИСОК_НАПР"])
		outdata.append(list(dirPlumb.coords) + [join, "", self._fwpt.name, "ВИСОК_НАПР"])

		return outdata
	def calc_diraxis(self):
		variables = [self._stpt, self._fwpt, self._startpt, self._endpt, self._maxdist, self._width]
		if any(map(lambda x: x is None, variables)):
			return None

		# направление
		maxdist = self._maxdist
		width = self._width
		is_reversed = self._is_reverse

		standpoint, dirpoint = self._stpt.copy(), self._fwpt.copy()

		floorStartPoint = self._startpt.copy()
		floorEndPoint = self._endpt.copy()
		floorAxis = geom.LineString([floorStartPoint, floorEndPoint])


		# борта
		leftWall = floorAxis.parallel_offset(width / 2, "left")
		rightWall = floorAxis.parallel_offset(width / 2, "right")
		floorPolygon = geom.SimplePolygon([leftWall[0], leftWall[1], rightWall[0], rightWall[1]])

		#
		v1 = floorEndPoint - floorStartPoint
		v2 = rightWall[0] - floorStartPoint

		# плоскости
		normal = v1**v2
		# плоскость, соответствующая кровле выработки
		floorPlane = geom.Plane(normal, point = floorStartPoint)

		# маркшейдерский висок
		svyPlumb = floorPlane.intersection(geom.Line(standpoint, vector = geom.Vector(0,0,1)))
		# направленческиий висок
		dirPlumb = floorPlane.intersection(geom.Line(dirpoint, vector = geom.Vector(0,0,1)))

		# вектор направления
		directionVector = dirPlumb - svyPlumb
		if is_reversed:
			directionVector = -directionVector
		# обрезаем ось направления границами проектной выработки
		directionAxis = floorPolygon.intersection(geom.LineString([svyPlumb, svyPlumb + maxdist*directionVector.unit]))

		# если пересечение направления и полигона выработки пусто,
		# например, если ошибочно выбрали опцию "Направление назад"
		return directionAxis

	def calc_offsets(self):
		offsets = {"DISTANCE": [], "RIGHT": [], "LEFT": []}
		variables = [self._stpt, self._fwpt, self._startpt, self._endpt, self._maxdist, self._step, self._width, self._uoff, self._doff, self._tolerance]
		if any(map(lambda x: x is None, variables)):
			return offsets

		# направление
		maxdist = self._maxdist
		is_reversed = self._is_reverse
		# параметры выработки
		width, uoffset, doffset, step, tolerance = self._width, self._uoff, self._doff, self._step, self._tolerance

		height = uoffset + doffset

		standpoint, dirpoint = self._stpt.copy(), self._fwpt.copy()

		floorStartPoint = self._startpt.copy()
		floorEndPoint = self._endpt.copy()
		floorAxis = geom.LineString([floorStartPoint, floorEndPoint])

		roofStartPoint, roofEndPoint = floorStartPoint.copy(), floorEndPoint.copy()
		roofStartPoint.z += height
		roofEndPoint.z += height

		projectVector = floorEndPoint - floorStartPoint

		# борта
		leftWall = floorAxis.parallel_offset(width / 2, "left")
		rightWall = floorAxis.parallel_offset(width / 2, "right")
		floorPolygon = geom.SimplePolygon([leftWall[0], leftWall[1], rightWall[0], rightWall[1]])

		#
		v1 = floorEndPoint - floorStartPoint
		v2 = rightWall[0] - floorStartPoint

		# плоскости
		normal = v1**v2
		# плоскость, соответствующая подошве выработки
		roofPlane = geom.Plane(normal, point = roofStartPoint)
		# плоскость, соответствующая кровле выработки
		floorPlane = geom.Plane(normal, point = floorStartPoint)

		# маркшейдерский висок
		svyPlumb = floorPlane.intersection(geom.Line(standpoint, vector = geom.Vector(0,0,1)))
		# направленческиий висок
		dirPlumb = floorPlane.intersection(geom.Line(dirpoint, vector = geom.Vector(0,0,1)))

		# вектор направления
		directionVector = dirPlumb - svyPlumb
		if is_reversed:
			directionVector = -directionVector
		# обрезаем ось направления границами проектной выработки
		directionAxis = floorPolygon.intersection(geom.LineString([svyPlumb, svyPlumb + maxdist*directionVector.unit]))

		# если пересечение направления и полигона выработки пусто,
		# например, если ошибочно выбрали опцию "Направление назад"
		if directionAxis is None:
			return offsets

		# вставляем точки через заданное расстояние
		directionAxis = directionAxis.insert_points(distance = step)

		# вычисление домеров
		# удлиняем борта, чтобы проекции на концах отрезков вычислялись корректно
		leftWallLong = geom.LineString([(leftWall[0] - projectVector).to_point(), (leftWall[1] + projectVector).to_point()])
		rightWallLong = geom.LineString([(rightWall[0] - projectVector).to_point(), (rightWall[1] + projectVector).to_point()])
		for i, pt in enumerate(directionAxis):
			lpt = leftWallLong.projection(pt)
			rpt = rightWallLong.projection(pt)

			ldist = round(lpt.distance(pt), tolerance)
			rdist = round(rpt.distance(pt), tolerance)

			# горизонтальное расстояние
			hdist = round(((pt.x - self._stpt.x)**2 + (pt.y - self._stpt.y)**2)**0.5, tolerance)
			offsets["DISTANCE"].append(hdist)
			offsets["LEFT"].append(ldist)
			offsets["RIGHT"].append(rdist)
		return offsets
	def calc_plumbs(self):
		variables = [self._stpt, self._fwpt, self._startpt, self._endpt, self._doff]
		if any(map(lambda x: x is None, variables)):
			return None

		standpoint = self._stpt.copy()
		dirpoint = self._fwpt.copy()

		floorStartPoint = self._startpt.copy()
		floorEndPoint = self._endpt.copy()
		floorAxis = geom.LineString([floorStartPoint, floorEndPoint])

		# борта
		rightWall = floorAxis.parallel_offset(5, "right")

		# вектора для вычисления нормали к плоскости подошвы
		v1 = floorEndPoint - floorStartPoint
		v2 = rightWall[0] - floorStartPoint

		# плоскости
		normal = v1**v2
		# плоскость, соответствующая кровле выработки
		floorPlane = geom.Plane(normal, point = floorStartPoint)

		# маркшейдерский висок
		svyPlumb = floorPlane.intersection(geom.Line(standpoint, vector = geom.Vector(0,0,1)))
		# направленческиий висок
		dirPlumb = floorPlane.intersection(geom.Line(dirpoint, vector = geom.Vector(0,0,1)))

		svyPlumb.z += self._doff
		dirPlumb.z += self._doff

		return svyPlumb, dirPlumb
	def calc_project(self):
		variables = [self._stpt, self._fwpt, self._startpt, self._endpt, self._maxdist, self._step, self._width, self._uoff, self._doff, self._tolerance]
		if any(map(lambda x: x is None, variables)):
			return None

		# направление
		maxdist = self._maxdist
		is_reversed = self._is_reverse
		# параметры выработки
		width, uoffset, doffset, step, tolerance = self._width, self._uoff, self._doff, self._step, self._tolerance

		height = uoffset + doffset

		standpoint, dirpoint = self._stpt.copy(), self._fwpt.copy()

		floorStartPoint = self._startpt.copy()
		floorEndPoint = self._endpt.copy()
		floorAxis = geom.LineString([floorStartPoint, floorEndPoint])

		leftWall = floorAxis.parallel_offset(width / 2, "left")
		rightWall = floorAxis.parallel_offset(width / 2, "right")

		# вектора для вычисления нормали к плоскости подошвы
		v1 = floorEndPoint - floorStartPoint
		v2 = rightWall[0] - floorStartPoint

		normal = v1**v2
		# плоскость, соответствующая кровле выработки
		floorPlane = geom.Plane(normal, point = floorStartPoint)

		# маркшейдерский висок
		svyPlumb = floorPlane.intersection(geom.Line(standpoint, vector = geom.Vector(0,0,1)))
		# направленческиий висок
		dirPlumb = floorPlane.intersection(geom.Line(dirpoint, vector = geom.Vector(0,0,1)))

		directionVector = dirPlumb - svyPlumb
		projectVector = floorEndPoint - floorStartPoint

		directionLine = geom.Line(svyPlumb, vector = directionVector)
		plane = geom.Plane(projectVector, point = self._endpt)
		projectPoint = plane.intersection(directionLine)
		projectPoint.z += doffset
		return projectPoint

	def calc_alamo2(self):
		# расчет условной точки
		plumbs = self.calc_plumbs()
		if plumbs is None:
			return None

		svyPlumb, dirPlumb = plumbs
		# виски
		svyv, dirv = round((svyPlumb - self._stpt).norm, 3), round((dirPlumb - self._fwpt).norm, 3)
		pt1, pt2 = self._stpt.copy(), self._fwpt.copy()
		pt1.z -= svyv
		pt2.z -= dirv
		# линия направления
		direction = geom.Line(pt1, pt2)
		# плоскость, соответствующая концу выработки
		axPoint1 = self._startpt.copy()
		axPoint1.z = 0
		axPoint2 = self._endpt.copy()
		axPoint2.z = 0
		ax = geom.Line(axPoint1, axPoint2)
		plane = geom.Plane(ax.vector, self._endpt)
		#
		intersection = plane.intersection(direction)
		return intersection
	def calc_alamo(self):
		variables = [self._stpt, self._fwpt, self._startpt, self._endpt, self._maxdist, self._step, self._width, self._uoff, self._doff, self._tolerance]
		if any(map(lambda x: x is None, variables)):
			return None

		# направление
		maxdist = self._maxdist
		is_reversed = self._is_reverse
		# параметры выработки
		width, uoffset, doffset, step, tolerance = self._width, self._uoff, self._doff, self._step, self._tolerance

		height = uoffset + doffset

		standpoint, dirpoint = self._stpt.copy(), self._fwpt.copy()

		floorStartPoint = self._startpt.copy()
		floorEndPoint = self._endpt.copy()
		floorAxis = geom.LineString([floorStartPoint, floorEndPoint])

		roofStartPoint, roofEndPoint = floorStartPoint.copy(), floorEndPoint.copy()
		roofStartPoint.z += height
		roofEndPoint.z += height

		# борта
		leftWall = floorAxis.parallel_offset(width / 2, "left")
		rightWall = floorAxis.parallel_offset(width / 2, "right")
		floorPolygon = geom.SimplePolygon([leftWall[0], leftWall[1], rightWall[0], rightWall[1]])

		#
		v1 = floorEndPoint - floorStartPoint
		v2 = rightWall[0] - floorStartPoint

		# плоскости
		normal = v1**v2
		# плоскость, соответствующая кровле выработки
		floorPlane = geom.Plane(normal, point = floorStartPoint)

		# маркшейдерский висок
		svyPlumb = floorPlane.intersection(geom.Line(standpoint, vector = geom.Vector(0,0,1)))
		# направленческиий висок
		dirPlumb = floorPlane.intersection(geom.Line(dirpoint, vector = geom.Vector(0,0,1)))

		# вектор направления
		directionVector = dirPlumb - svyPlumb
		if is_reversed:
			directionVector = -directionVector
		# обрезаем ось направления границами проектной выработки
		directionAxis = floorPolygon.intersection(geom.LineString([svyPlumb, svyPlumb + maxdist*directionVector.unit]))

		if directionAxis is None:
			return None

		alamo = directionAxis.point2
		alamo.z += doffset
		return alamo



class ControlMeasure:
	"""Класс для вычисления контрольного замера"""
	def __init__(self, stpt, bkpt, fwpt, hang = None, vang = None, vdist = None, ioff = None, voff = None, origin = 0):

		self._stpt = stpt 			# точка стояния
		self._bkpt = bkpt 			# задняя точка
		self._fwpt = fwpt 			# передняя точка

		# прямой замер
		self._hang = geom.Angle(hang) if hang is not None and not isinstance(hang, geom.Angle) else hang	# горизонтальный угол
		self._vang = geom.Angle(vang) if vang is not None and not isinstance(vang, geom.Angle) else vang	# вертикальный угол
		self._vdist = vdist 		# наклонное расстояние
		self._ioff = ioff 			# смещение инструмента
		self._voff = voff 			# смещение виска

		self._origin = origin

	@property
	def standpoint(self):			return self._stpt
	@standpoint.setter
	def standpoint(self, pt):		self._stpt = pt
	@property
	def backpoint(self):			return self._bkpt
	@backpoint.setter
	def backpoint(self, pt):		self._bkpt = pt
	@property
	def dirpoint(self):				return self._fwpt
	@dirpoint.setter
	def dirpoint(self, pt):			self._fwpt = pt
	
	# прямой замер
	@property
	def hangle(self):				return self._hang
	@hangle.setter
	def hangle(self, value):		self._hang = geom.Angle(value) if value and not isinstance(value, geom.Angle) else value
	@property
	def vangle(self):				return self._vang
	@vangle.setter
	def vangle(self, value):		self._vang = geom.Angle(value) if value and not isinstance(value, geom.Angle) else value
	@property
	def vdist(self):				return self._vdist
	@vdist.setter
	def vdist(self, value):			self._vdist = value
	@property
	def ioffset(self):				return self._ioff
	@ioffset.setter
	def ioffset(self, value):		self._ioff = value
	@property
	def voffset(self):				return self._voff
	@voffset.setter
	def voffset(self, value):		self._voff = value

	@property
	def has_all_parameters(self):
		return all(map(lambda x: x is not None, [self._hang, self._vang, self._vdist, self._ioff, self._voff]))
	@property
	def distance_satisfies(self):
		truedist, measdist = self.calc_true_hdist(), self.calc_hdist()
		if not truedist or not measdist:
			return False
		if abs(truedist-measdist) > truedist / 1000:
			return False
		return True
	@property
	def angle_satisfies(self):
		trueang, measang = self.calc_hangle(), self._hang
		if trueang is None or measang is None:
			return False
		decDiff = abs(trueang - measang).decimal
		dec2Min = geom.Angle.from_tuple((0,2,0)).decimal
		if round(decDiff, 3) > round(dec2Min, 3):
			return False
		return True
	@property
	def satisfies(self):
		return self.angle_satisfies and self.distance_satisfies

	def clear(self):
		self._hang = None
		self._vang = None
		self._vdist = None
		self._ioff = None
		self._voff = None

	# вычисление углов
	def calc_hangle(self):
		"""Вычисляет горизонтальный угол в градусах между отрезками	standpoint, backpoint] и [standpoint, dirpoint]; standpoint - центр"""
		if any(map(lambda x: not x.name, [self._stpt, self._bkpt, self._fwpt])):
			return None

		if not self._fwpt.name:
			return None

		a, b, c = self.backpoint-self.standpoint, self.dirpoint-self.standpoint, self.dirpoint-self.backpoint

		d = a**b
		if abs(d.z) < 1e-6:
			return None

		aHorNorm, bHorNorm, cHorNorm = a.horizontal_norm, b.horizontal_norm, c.horizontal_norm
		aSqrNorm, bSqrNorm, nSqrNorm = aHorNorm**2, bHorNorm**2, cHorNorm**2

		if aHorNorm == 0 or bHorNorm == 0:
			return None

		cos = -0.5 * (nSqrNorm - aSqrNorm - bSqrNorm) / aHorNorm / bHorNorm
		if (abs(cos) - 1) > 1:
			cos = 1 if cos > 0 else -1

		angle = geom.Angle(math.degrees(math.acos(cos)))
		#
		if d.z > 0:
			angle = 360 - angle
		#
		ret = angle
		return ret if not math.isnan(ret) else None

	# вычисление горизонтального проложения
	def calc_true_hdist(self):
		"""Вычисляет из координат горизонтальное расстояние между точкой стояния и точкой визирования"""
		if any(map(lambda x: not x.name, [self._stpt, self._bkpt])):
			return None
		ret = ( (self.standpoint.x - self.backpoint.x) ** 2 + (self.standpoint.y - self.backpoint.y) ** 2) ** 0.5
		return ret if not math.isnan(ret) else None
	def calc_hdist(self):
		"""Вычисляет горизонтальное расстояние для прямого замера на основе измеренных наклонного расстояния и вертикального угла"""
		if self._vdist is None or self._vang is None:
			return None
		ret = self._vdist * self._vang.sin
		return ret if not math.isnan(ret) else None

	# вычисление превышения
	def calc_true_dz(self):
		"""Вычисляет из координат превышение"""
		if not self.backpoint.name or not self.standpoint.name:
			return None
		ret = abs(self.backpoint.z - self.standpoint.z)
		return ret if not math.isnan(ret) else None
	def calc_dz(self):
		"""Вычисляет превышение для прямого замера на основе измеренных наклонного расстояния и вертикального угла"""
		variables = [self._vdist, self._vang, self._ioff, self._voff]
		if any(map(lambda x: x is None, variables)):
			return None
		ret = self._vdist * self._vang.cos + self._ioff + self._voff
		return ret if not math.isnan(ret) else None

	# другие вычисления
	def calc_lsin(self):
		if self._vdist is None or self._vang is None:
			return None
		ret = self._vdist * self._vang.cos
		return ret if not math.isnan(ret) else None
	def calc_lsinInv(self):
		if self._vdistInv is None or self._vangInv is None:
			return None
		ret = self._vdistInv * self._vangInv.cos
		return ret if not math.isnan(ret) else None

class DirectionMeasure(ControlMeasure):
	"""Класс для вычисления направления для левых углов"""
	def __init__(self, stpt, bkpt, fwpt, hang = None, vang = None, vdist = None, ioff = None, voff = None, origin = 0,
				vdistInv = None, vangInv = None, ioffInv = None, voffInv = None):

		super(DirectionMeasure, self).__init__(stpt, bkpt, fwpt, hang, vang, vdist, ioff, voff, origin)

		# обратный замер
		self._vdistInv = vdistInv	# наклонное расстояние
		self._vangInv = geom.Angle(vangInv) if vangInv and not isinstance(vangInv, geom.Angle) else vangInv		# вертикальный угол 
		self._ioffInv = ioffInv		# смещение инструмента
		self._voffInv = voffInv		# смещение виска

	# обратный замер
	@property
	def vdistInv(self):				return self._vdistInv
	@vdistInv.setter
	def vdistInv(self, value):		self._vdistInv = value
	@property
	def vangleInv(self):			return self._vangInv
	@vangleInv.setter
	def vangleInv(self, value):		self._vangInv = geom.Angle(value) if value and not isinstance(value, geom.Angle) else value
	@property
	def ioffsetInv(self):			return self._ioffInv
	@ioffsetInv.setter
	def ioffsetInv(self, value):	self._ioffInv = value
	@property
	def voffsetInv(self):			return self._voffInv
	@voffsetInv.setter
	def voffsetInv(self, value):	self._voffInv = value

	@property
	def has_all_parameters(self):
		ret = super(DirectionMeasure, self).has_all_parameters
		return ret and all(map(lambda x: x is not None, [self._vdistInv, self._vangInv, self._ioffInv, self._voffInv]))

	def clear(self):
		super(DirectionMeasure, self).clear()
		self._vdistInv = None
		self._vangInv = None
		self._ioffInv = None
		self._voffInv = None

	# вычисление углов
	def calc_bkdirangle(self):
		if not self._stpt.name or not self._bkpt.name:
			return None
		ret = geom.Angle((self.standpoint - self.backpoint).azimuth)
		return ret if not math.isnan(ret) else None
	def calc_dirangle(self):
		bkdirangle = self.calc_bkdirangle()
		if self._hang is None or bkdirangle is None:
			return None
		ret = (bkdirangle + self._hang + 180) % 360
		return ret if not math.isnan(ret) else None

	# вычисление горизонтального проложения
	def calc_hdistInv(self):
		"""Вычисляет горизонтальное расстояние для обратного замера на основе измеренных наклонного расстояния и вертикального угла"""
		if self._vdistInv is None or self._vangInv is None:
			return None
		ret =  self._vdistInv * self._vangInv.sin
		return ret if not math.isnan(ret) else None

	# вычисление превышения
	def calc_dzInv(self):
		"""Вычисляет превышение для обратного замера на основе измеренных наклонного расстояния и вертикального угла"""
		variables = [self._vdistInv, self._vangInv, self._ioffInv, self._voffInv]
		if any(map(lambda x: x is None, variables)):
			return None
		ret = self._vdistInv * self._vangInv.cos + self._ioffInv + self._voffInv
		return ret if not math.isnan(ret) else None

	# вычисление приращений
	def calc_dx(self):
		"""Вычисляет приращение по восточным координатам"""
		dirangle, avgdist = self.calc_dirangle(), self.calc_hdistAvg()
		if dirangle is None or avgdist is None:
			return None
		ret = avgdist * dirangle.sin
		return ret if not math.isnan(ret) else None
	def calc_dy(self):
		"""Вычисляет приращение по северным координатам"""
		dirangle, avgdist = self.calc_dirangle(), self.calc_hdistAvg()
		if dirangle is None or avgdist is None:
			return None
		ret = avgdist * dirangle.cos
		return ret if not math.isnan(ret) else None

	#
	def calc_coords(self):
		"""Вычисляет координаты новой точки"""
		dx, dy, dz = self.calc_dx(), self.calc_dy(), self.calc_dz()
		dzavg = self.calc_dzAvg()

		if dx is None or dy is None or dz is None or dzavg is None:
			self.dirpoint.delete_coords()
			return None

		if not self.dirpoint.name or not self.standpoint.name:
			self.dirpoint.delete_coords()
			return None

		self.dirpoint.x = self.standpoint.x + dx
		self.dirpoint.y = self.standpoint.y + dy
		self.dirpoint.z = self.standpoint.z + dzavg #if dz > 0 else self.standpoint.z - dzavg
		return self.dirpoint

	# другие вычисления
	def calc_dzAvg(self):
		dirdz, invdz = self.calc_dz(), self.calc_dzInv()
		if dirdz is None or invdz is None:
			return None
		ret = 0.5 * (abs(dirdz) + abs(invdz))
		if dirdz < 0:
			ret = -ret
		return ret if not math.isnan(ret) else None
	def calc_hdistAvg(self):
		dirdist, invdist = self.calc_hdist(), self.calc_hdistInv()
		if dirdist is None or invdist is None:
			return None
		ret = 0.5 * (dirdist + invdist)
		return ret if not math.isnan(ret) else None

