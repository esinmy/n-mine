import shapely.geometry as geom
import shapely.affinity as afty
import math

TOLERANCE = 1e-6

class Angle(float):
	def __init__(self, value, degrees = True):
		if value > 0:		angle = value % 360 if degrees else math.degrees(angle) % 360
		else:				angle = - (abs(value) % 360 if degrees else math.degrees(angle) % 360)
		self._angle = angle

	def __str__(self):
		text = self.to_text()
		if text:					return "ANGLE (%.6f; (%s))" % (self._angle, text)
		else:						return "ANGLE (%.6f)" % self._angle

	def __add__(self, value):		return Angle(self._angle + value)
	def __radd__(self, value):		return Angle(self._angle + value)
	def __sub__(self, value):		return Angle(self._angle - value)
	def __rsub__(self, value):		return Angle(value - self._angle)
	def __mul__(self, value):		return Angle(self._angle * value)
	def __rmul__(self, value):		return Angle(self._angle * value)
	def __truediv__(self, value):	return Angle(self._angle / value)
	def __mod__(self, value):		return Angle(self._angle % value)
	def __abs__(self):				return Angle(abs(self._angle))
	def __neg__(self):				return Angle(-self._angle)

	@property
	def radians(self):		return self._radians
	@property
	def decimal(self):		return self._angle

	@staticmethod
	def from_text(text):
		textLen = len(text)
		isNegative = text.startswith("-")
		if (not isNegative and textLen > 9) or textLen > 10:
			return None
		s = text.split()
		n = len(s)
		if n > 3 or n < 1:				return None
		try:							ret = list(map(int, s))
		except:							return None
		for i in range(n, 3):
			ret.append(0)
			if i > 0:
				ret[-1] = abs(ret[-1])
		if ret[2] == 60:
			ret[1] += 1
			ret[2] = 0
		if ret[1] == 60:
			if ret[0] >= 0:				ret[0] += 1
			else:						ret[0] -= 1
			ret[1] = 0
		if abs(ret[0]) == 360:
			ret[0] = 0

		if abs(ret[0]) > 360:			return None
		if ret[1] < 0 or ret[1] > 60:	return None
		if ret[2] < 0 or ret[2] > 60:	return None
		angle = Angle.from_tuple(ret)
		return -Angle.from_tuple(ret) if isNegative and abs(angle) < 1 else angle
	@staticmethod
	def from_tuple(t):
		degrees, minutes, seconds = t
		minutes = abs(minutes)
		seconds = abs(seconds)
		if degrees >= 0:	angle = degrees + (minutes + seconds / 60) / 60
		else:				angle = degrees - (minutes + seconds / 60) / 60
		return Angle(angle)

	def to_text(self):
		if any(map(lambda x: math.isnan(x), (self.degrees, self.minutes, self.seconds))):
			return ""
		degrees, minutes, seconds = self.degrees, abs(self.minutes), abs(self.seconds)
		if seconds == 60:
			minutes += 1
			seconds = 0
		if minutes == 60:
			if degrees < 0:		degrees -= 1
			else:				degrees += 1
			minutes = 0
		if abs(degrees) == 360:
			degrees = 0
		if self._angle < 0 and degrees == 0:	ret = "-%d %d %d" % (degrees, minutes, seconds)
		else:									ret = "%d %d %d" % (degrees, minutes, seconds)
		return ret

	@property
	def degrees(self):
		try:		return int(self._angle)
		except:		return math.nan
	@property
	def minutes(self):
		try:		return int (60 * (self._angle - self.degrees))
		except:		return math.nan
	@property
	def seconds(self):
		try:		return round (60 * (60 * (self._angle - self.degrees) - self.minutes))
		except:		return math.nan

	@property
	def cos(self):		return math.cos(math.radians(self._angle))
	@property
	def sin(self):		return math.sin(math.radians(self._angle))
	@property
	def tan(self):		return math.tan(math.radians(self._angle))

class Point(object):
	def __init__(self, x = 0, y = 0, z = 0):
		if isinstance(x, tuple) or isinstance(x, list):
			if len(x) == 1:		x, y, z = x[0], 0, 0
			elif len(x) == 2:	x, y, z = x[0], x[1], 0
			elif len(x) == 3:	x, y, z = x
			else:					raise Exception("Неверная размерность")
		elif isinstance(x, Point) or isinstance(x, Vector):
			x, y, z = x.coords
		elif not (isinstance(x, float) or isinstance(x, int)):
			raise TypeError("Unexpected type %s" % str(type(x)))
		self._x = x
		self._y = y
		self._z = z
	def __add__(self, v):
		if isinstance(v, Point):
			return Vector(self.x + v.x, self.y + v.y, self.z + v.z)
		elif isinstance(v, float) or isinstance(v, int):
			return Vector(self.x + v, self.y + v, self.z + v)
		else:
			raise TypeError("Invalid type. Vector expected")
	def __radd__(self, v):
		if isinstance(v, Point):
			return Vector(self.x + v.x, self.y + v.y, self.z + v.z)
		elif isinstance(v, float) or isinstance(v, int):
			return Vector(self.x + v, self.y + v, self.z + v)
		else:
			raise TypeError("Invalid type. Vector expected")
	def __sub__(self, v):
		if isinstance(v, Point):
			return Vector(self.x - v.x, self.y - v.y, self.z - v.z)
		elif isinstance(v, float) or isinstance(v, int):
			return Vector(self.x - v, self.y - v, self.z - v)
		else:
			raise TypeError("Invalid type. Vector expected")
	def __rsub__(self, v):
		if not isinstance(v, Point):
			raise TypeError("Invalid type. Vector expected")
		return Vector(v.x - self.x, v.y - self.y, v.z - self.z)
	def __truediv__(self, v):
		if type(v) in [int, float]:
			return Vector(self.x / v, self.y / v, self.z / v)
	def __mul__(self, v):
		if isinstance(v, int) or isinstance(v, float):		return Vector(self.x*v, self.y*v, self.z*v)
		elif isinstance(v, Vector):							return self.x*v.x + self.y*v.y + self.z*v.z
		else:												raise TypeError("'%s' is unsupported type for * operator" % str(type(v)))
	def __rmul__(self, v):
		if isinstance(v, int) or isinstance(v, float):		return Vector(self.x*v, self.y*v, self.z*v)
		elif isinstance(v, Vector):							return self.x*v.x + self.y*v.y + self.z*v.z
		else:												raise TypeError("'%s' is unsupported type for * operator" % str(type(v)))
	def __str__(self):	return "POINT (%.6f, %.6f, %.6f)" % self.coords
	def __repr__(self):	return "POINT (%.6f, %.6f, %.6f)" % self.coords
	def distance(self, other):
		if isinstance(other, Point):
			return (self.to_vector() - other.to_vector()).norm
	@property
	def coords(self):		return self._x, self._y, self._z
	@property
	def x(self):			return self._x
	@x.setter
	def x(self, val):		self._x = float(val)
	@property
	def y(self):			return self._y
	@y.setter
	def y(self, val):		self._y = float(val)
	@property
	def z(self):			return self._z
	@z.setter
	def z(self, val):		self._z = float(val)
	def copy(self):			return Point(self._x, self._y, self._z)
	def almost_equals(self, other, decimal = 6):
		p1 = self.to_geom()
		p2 = other.to_geom() if isinstance(other, Point) else other
		return p1.almost_equals(p2, decimal)
	def to_geom(self):
		return geom.Point(self.coords)
	def to_vector(self):
		return Vector(self.coords)

class Vector(Point):
	def __str__(self):
		return "VECTOR (%.6f %.6f %.6f)" % (self.x, self.y, self.z)
	def __neg__(self):	return Vector(-self.x, -self.y, -self.z)
	def __pow__(self, v):
		if not isinstance(v, Vector):
			raise TypeError("Unsupported type for ** operator")
		x = self.y*v.z - self.z*v.y
		y = self.z*v.x - self.x*v.z
		z = self.x*v.y - self.y*v.x
		return Vector(x, y, z)
	def copy(self):		return Vector(self.coords)
	@property
	def azimuth(self):
		if self.x == 0 and self.y == 0:
			return 0
		beta = math.degrees(math.atan2(self.y, self.x))
		if self.x >= 0:
			angle = 90 - beta
		else:
			if self.y >= 0:	angle = 450 - beta
			else:			angle = 90 + abs(beta)
		return angle
	@property
	def inclination(self):
		norm = self.norm
		if self.z == 0 or norm < TOLERANCE:
			return Angle(0)
		return Angle(math.degrees(math.asin(round(self.z / norm, 6))))
	@property
	def norm(self):		return (self.x**2+self.y**2+self.z**2)**0.5
	@property
	def horizontal_norm(self):
		return (self.x**2 + self.y**2) ** 0.5
	@property
	def unit(self):
		norm = self.norm
		if norm == 0:	return None
		else:			return Vector(self.x/norm, self.y/norm, self.z/norm)
	def to_point(self):
		return Point(self.coords)
	@staticmethod
	def from_angles(phi, theta):
		x = theta.sin * phi.cos
		y = theta.sin * phi.sin
		z = theta.cos
		return Vector(x, y, z)

class LineSegment(object):
	def __init__(self, pt1, pt2):
		self._data = [Point(pt1), Point(pt2)]
		self._vector = self._data[1].to_vector() - self._data[0].to_vector()
	def __str__(self):	return "LINESEGMENT (%s, %s)" % (str(self.point), str(self.point2))
	@property
	def coords(self):	return [self._data[0].coords, self._data[1].coords]
	@property
	def vector(self):	return self._vector
	@property
	def length(self):	return self._vector.norm
	@property
	def point(self):	return self._data[0]
	@property
	def point2(self):	return self._data[1]
	@property
	def centroid(self):
		v = 0.5*(self.point.to_vector() + self.point2.to_vector())
		return Point(v)
	def interpolate(self, distance, normalized = False):
		pt = self.to_geom().interpolate(distance, normalized)
		if pt:
			pt = Point(list(pt.coords)[0])
		return pt
	def insert_points(self, npoints = None, distance = None):
		if npoints is None and distance is None:
			raise ValueError("Необходимо указать либо число точек, либо расстояние")
		if npoints is None:		npoints = int(self.length / distance)
		else:									distance = self.length / (npoints+1)

		points = [self.point]
		for i in range(npoints):
			points.append(self.point.to_vector() + self._vector.unit*(i+1)*distance)
		if not points[-1].almost_equals(self.point2, TOLERANCE):
			points.append(self.point2)
		return LineString(points)
	def to_geom(self):
		return geom.LineString([self.point.coords, self.point2.coords])
	def reverse(self):
		self._data.reverse()
		self._vector = - self._vector
	def parallel_offset(self, distance, side, resolution = 16, join_style = 1, mitre_limit = 1.0):
		line = geom.LineString(self.coords)
		line = line.parallel_offset(distance, side, resolution, join_style, mitre_limit)
		if side == "left":	coords = [[c[0], c[1], self[i].z] for i, c in enumerate(line.coords)]
		else:								coords = [[c[0], c[1], self[-i-1].z] for i, c in enumerate(line.coords)]
		return LineSegment(coords)
	def rotate(self, angle, origin = "center", use_radians = False, inplace = False):
		rotated = afty.rotate(self.to_geom(), angle, origin, use_radians)
		coords = [[p[0], p[1], p[2] if len(p) > 2 else self[i].z] for i, p in enumerate(rotated.coords)]
		if inplace:		self._data = [Point(c) for c in coords]
		else:			return LineSegment(*coords)
	def translate(self, xoff = 0, yoff = 0, zoff = 0, inplace = False):
		translated = afty.translate(self.to_geom(), xoff, yoff, zoff)
		coords = [[p[0], p[1], p[2] if len(p) > 2 else self[i].z] for i, p in enumerate(translated.coords)]
		if inplace:		self.segment = [Point(c) for c in coords]
		else:			return LineString(coords)
	def to_linestring(self):
		return LineString(self.coords)
	def to_line(self):
		return Line(self.point, self.point2)

class Line(object):
	def __init__(self, pt1, pt2 = None, vector = None):
		self._point = Point(pt1)
		if pt2 is None and vector is None:				raise ValueError("Not enough input parameters")
		if pt2 is not None and vector is not None:		raise ValueError("Only one way of declaration is possible")

		if vector is None:		self._vector = Point(pt2).to_vector() - self._point.to_vector()
		if pt2 is None:			self._vector = vector
		if self._vector.norm < TOLERANCE:
			raise Exception("Zero vector cannot be used for the Line")
	@property
	def vector(self):	return self._vector
	@property
	def point(self):	return self._point
	def contains(self, other):
		if isinstance(other, Point):
			v2 = Point(other).to_vector() - self._point.to_vector()
			v = self.vector**v2
			return abs(v.z) < TOLERANCE
		elif isinstance(other, LineSegment):
			v = self.vector**other.vector
			return abs(v.z) < TOLERANCE
		else:
			raise Exception("Unexpected type %s" % str(type(other)))
	def interpolate(self, distance):
		return Point(self._point.to_vector() + distance*self._vector.unit)
	def interpolate_t(self, t):
		return Point(self._point.to_vector() + t*self._vector)

class Plane(object):
	def __init__(self, vector, point = None, d = 0):
		self._normal = vector.unit
		if point is None:
			self._d = d
			self._point = Point(0, 0, d)
		else:
			self._d = -(self._normal*point.to_vector())
	@property
	def normal(self):	return self._normal
	@property
	def d(self):		return self._d
	def contains(self, other):
		if isinstance(other, Point):
			return abs(sum(map(lambda x,y: x*y, self.normal.coords, other.coords)) + self._d) < TOLERANCE
		elif isinstance(other, LineSegment) or isinstance(other, Line):
			return abs(self.normal*other.vector) < TOLERANCE and self.contains(other.point)
	def intersection(self, other):
		if isinstance(other, Line) or isinstance(other, LineSegment):
			if self.contains(other):
				return other
			prod = other.vector * self.normal
			if abs(prod) < TOLERANCE:		return None
			t = (-self._d - self.normal*other.point.to_vector()) / prod
			if isinstance(other, Line):
				return other.interpolate_t(t)
			elif isinstance(other, LineSegment) and t >= 0 and t <= 1:
				return other.to_line().interpolate(other.length*t)
			else:
				return None
		elif isinstance(other, LineString):
			intersections = []
			for segment in other.segments:
				intersection = self.intersection(segment)
				if intersection:
					intersections.append(intersection)
			count = len(intersections)
			if count == 0:		return None
			elif count == 1:	return intersections[0]
			else:				return intersections
	def is_parallel(self, other):
		if isinstance(other, Line):			return abs(other.vector * self.normal) < TOLERANCE
		if isinstance(other, Plane):		return (other.normal**self.normal).norm < TOLERANCE

class LineString(object):
	def __str__(self):
		ret = "LINESTRING ("
		for i, pt in enumerate(self._data, start = 1):
			ret += "\n\t%s\t%s" % (str(i), str(pt))
		ret += "\n)"
		return ret
	def __add__(self, p):				return LineString([pt + p for pt in self])
	def __radd__(self, p):			return LineString([pt + p for pt in self])
	def __sub__(self, p):				return LineString([pt - p for pt in self])
	def __getitem__(self, index):
		if isinstance(index, int):			return self._data[index]
		elif isinstance(index, slice):	return LineString(self._data[index])
	def __init__(self, data):
		self._data = [pt if type(pt) == Point else Point(pt) for pt in data]
		# удаляем дубликаты
		for i in range(len(self._data)-1, -1, -1):
			if self._data[i].distance(self._data[i-1]) < 1e-3:
				self._data.remove(self._data[i])
		#
		self._length = sum([segment.vector.norm for segment in self.segments])
		if len(self._data) < 2:
			raise ValueError("Стринг должен иметь хотя бы две различные точки")
	def __contains__(self, pt):	return pt in self._data
	def __len__(self):					return len(self._data)
	def buffer(self, distance, resolution = 16, cap_style = 1, join_style = 1, mitre_limit = 1.0):
		buf = self.to_geom().buffer(distance, resolution, cap_style, join_style, mitre_limit)
		return buf
	@property
	def centroid(self):
		return self.to_geom().centroid
	@property
	def length(self):						return self._length
	@property
	def npoints(self):					return len(self._data)
	@property
	def segments(self):					return tuple(LineSegment(self._data[i-1], self._data[i]) for i in range(1, len(self._data)))
	def copy(self):							return LineString(self.coords)
	@property
	def coords(self):						return [pt.coords for pt in self._data]
	def parallel_offset(self, distance, side, resolution = 16, join_style = 1, mitre_limit = 1.0):
		line = geom.LineString(self.coords)
		line = line.parallel_offset(distance, side, resolution, join_style, mitre_limit)
		if side == "left":	coords = [[c[0], c[1], self[i].z] for i, c in enumerate(line.coords)]
		else:								coords = [[c[0], c[1], self[-i-1].z] for i, c in enumerate(line.coords)]
		return LineString(coords)
	def interpolate(self, distance, normalized = False):
		if distance > self.length:
			return self[-1]
		#
		cumlen = self.cumlength
		for i, length in enumerate(cumlen, start = 1):
			if length >= distance:
				break
		#
		i -= 1
		remdist = distance - cumlen[i-1] if i > 0 else distance
		segment = self.segments[i]
		vector = segment.vector
		#
		t = remdist / segment.length
		pt = (self[i].to_vector() + t*vector).to_point()
		return pt
	def project(self, pt):
		segments = self.segments
		vector = pt.to_vector()
		k = None
		for i, segment in enumerate(segments, start = 1):
			v1 = vector - self[i-1].to_vector()
			v2 = vector - self[i].to_vector()
			vlen1, vlen2 = v1.norm, v2.norm
			s = vlen1 + vlen2
			if abs(s - segment.length) < 1e-3:
				k = i
				break
		if k is None:
			return None
		cumlen = self.cumlength
		ret = cumlen[k-2] + vlen1 if k > 1 else vlen1
		return round(ret, 6)
	def intersection(self, other):
		ret = self.to_geom().intersection(other.to_geom())
		if ret.is_empty:							return None
		if ret.geom_type == "Point":	return Point(ret.coords[0])
	@property
	def length(self):						return sum(segment.length for segment in self.segments)
	def scale(self, xfact = 1, yfact = 1, zfact = 1, inplace = False):
		#		ret = afty.scale(self.to_geom(), xfact, yfact, zfact)
		#		data = [Point(pt) for pt in ret.coords]
		# data = [Point(pt.x*xfact, pt.y*yfact, pt.z*zfact) for pt in self]
		c = self.center
		data = [Point((pt[0]-c.x)*xfact+c.x, (pt[1]-c.y)*yfact+c.y, (pt[2]-c.z)*zfact+c.z) for pt in self.coords]
		if inplace:				self._data = data
		else:							return LineString(data)

	def projection(self, pt, mode = "orthogonal", allow_external = False):
		proj = None
		if mode == "vertical":
			for i, seg in enumerate(self.segments):
				s = seg.vector.copy()
				s.z = 0

				t = (pt.to_vector() - self._data[i].to_vector())*s / (s*s)
				if not allow_external and t < 0-1e-6 or t > 1+1e-6:			continue
				proj = (self._data[i].to_vector() + t*seg.vector).to_point()
				break
		if mode == "orthogonal":
			for i, seg in enumerate(self.segments):
				t = (pt - self._data[i])*seg.vector / (seg.vector*seg.vector)
				if not allow_external and t < 0-1e-6 or t > 1+1e-6:			continue
				proj = (self._data[i].to_vector() + t*seg.vector).to_point()
				break
		return proj
	def insert_point(self, pt, inplace = False):
		if type(pt) != Point:
			raise TypeError("Cannot insert %s. Point expected" % type(pt))

		segments = self.segments
		vector = pt.to_vector()
		k = None
		for i, segment in enumerate(segments, start = 1):
			v1 = vector - self[i-1].to_vector()
			v2 = vector - self[i].to_vector()
			vlen1, vlen2 = v1.norm, v2.norm
			s = vlen1 + vlen2
			if abs(s - segment.length) < TOLERANCE:
				k = i
				break
		if k is None:
			if not inplace:
				return self.copy()

		data = self._data.copy()
		data.insert(k, pt)
		if inplace:				self._data = data.copy()
		else:							return LineString(data)
	def insert_points(self, npts, inplace = False):
		""" возвращает только вставленные точки """
		# общая длина стринга
		slen = self.length
		# расстояние, через которое будут вставлены точки
		step = slen / (npts+1)
		distances = [(i+1)*step for i in range(npts)]
		line = self.copy()
		for dist in distances:
			line.insert_point(line.interpolate(dist), inplace = True)
		if inplace:		self._data = line.coords
		else:					return LineString(line.coords)

	def remove(self, i, inplace = True):
		data = self._data.copy()
		data = data[:i] + data[i+1:]
		if inplace:			self._data = data.copy()
		else:						return LineString(data)
	def rotate(self, ang, center = None, ax = Vector(0,0,1), inplace = False):
		radians = math.radians
		cos, sin = math.cos, math.sin

		center = self.center if center is None else center
		ax = (self.segments[0]**self.segments[1]).unit if ax is None else ax.unit

		rang = radians(ang)
		s = self - center
		#
		cosA = cos(rang)
		sinA = sin(rang)
	
		# матрица поворота
		rotm = []
		rotm.append(Vector(cosA + (1-cosA)*ax.x*ax.x, (1-cosA)*ax.x*ax.y - ax.z*sinA, (1-cosA)*ax.x*ax.z + ax.y*sinA))
		rotm.append(Vector((1-cosA)*ax.x*ax.y + ax.z*sinA, cosA + (1-cosA)*ax.y*ax.y, (1-cosA)*ax.y*ax.z - ax.x*sinA))
		rotm.append(Vector((1-cosA)*ax.x*ax.z - ax.y*sinA, (1-cosA)*ax.y*ax.z + ax.x*sinA, cosA + (1-cosA)*ax.z*ax.z))
		#
		ret = [(Vector(list(map(lambda x: x*pt.to_vector(), rotm))) + center.to_vector()).to_point() for pt in s]
		if inplace:								self._data = list(ret)
		else:									return LineString(ret)
	def rotate2(self, angle, origin = "center", use_radians = False, inplace = False):
		rotated = afty.rotate(self.to_geom(), angle, origin, use_radians)
		coords = [[p[0], p[1], p[2] if len(p) > 2 else self[i].z] for i, p in enumerate(rotated.coords)]
		if inplace:		self._data = [Point(c) for c in coords]
		else:			return LineString(coords)
	def set_orientation(self, vector, center = None, inplace = False):
		degrees, radians, acos, atan2 = math.degrees, math.radians, math.acos, math.atan2

		center = self.center if center is None else center
		vector = vector.unit
		normal = self.normal
		if not normal:
			raise Exception("Для ориентирования стринга необходимо минимум 3 точки")

		if normal.z > 0:
			normal.z = -normal.z

		dirangle = degrees(atan2(vector.y, vector.x))
		angle = degrees(acos(normal*vector))
		ax = (normal**vector).unit

		vector_orientation = 0
		vector_orientation += vector.x * (normal.y*ax.z - normal.z*ax.y)
		vector_orientation -= vector.y * (normal.x*ax.z - normal.z*ax.x)
		vector_orientation += vector.z * (normal.x*ax.y - normal.y*ax.x)
		
		if round(vector_orientation, 6) == 0:		angle = 0
		elif vector_orientation < 0:				angle = 180 + angle

		rotated = self.rotate(angle, center = center, ax = ax, inplace = False)
		rotated = rotated.rotate(dirangle-270, center = center, ax = vector, inplace = False)
		if inplace:								self._data = [pt for pt in rotated]
		else:											return rotated

	@property
	def center(self):
		med = [0.5*(self[i-1]+self[i]) for i in range(1, self.npoints)]
		center = sum([med[i]*seg.vector.norm for i, seg in enumerate(self.segments)]) / self.length
		return center
	@property
	def normal(self):
		segments = self.segments
		if self.npoints > 3:
			for i in range(1, len(segments)):
				ret = (self.segments[i-1].vector**self.segments[i].vector).unit
				if ret:
					break
		return ret
	@property
	def x(self):								return tuple(pt.x for pt in self)
	@property
	def y(self):								return tuple(pt.y for pt in self)
	@property
	def z(self):								return tuple(pt.z for pt in self)
	def to_list(self, join = None):				return [list(c) + [join] if join else list(c) for c in self.coords]
	def reverse(self, inplace = False):
		if inplace:		self._data.reverse()
		else:					return LineString(list(reversed(self._data)))
	def translate(self, xoff = 0, yoff = 0, zoff = 0, inplace = False):
		translated = afty.translate(self.to_geom(), xoff, yoff, zoff)
		coords = [[p[0], p[1], p[2] if len(p) > 2 else self[i].z] for i, p in enumerate(translated.coords)]
		if inplace:		self._data = [Point(c) for c in coords]
		else:			return LineString(coords)
	def to_geom(self):
		return geom.LineString(self.coords)
	@property
	def segments(self):	return tuple(LineSegment(self[i-1], self[i]) for i in range(1, len(self)))
	@property
	def cumlength(self):
		lengths = [s.length for s in self.segments]
		return tuple(sum(lengths[:i+1]) for i in range(len(self)-1))

class SimplePolygon(LineString):
	def __getitem__(self, index):
		if isinstance(index, int):		return self._data[index]
		elif isinstance(index, slice):	return LineString(self._data[index])
	def __str__(self):
		return "SIMPLE POLYGON\n\t" + "\n\t".join(["%d\t%s" % (i, str(pt)) for i, pt in enumerate(self)]) 
	def __init__(self, coords):
		coords = list(coords)
		if coords[0] != coords[1]:
			coords.append(coords[0])
		super(SimplePolygon, self).__init__(coords)
		if len(self) < 3:
			raise ValueError("Полигон должен содержать минимум 3 точки")
	def intersection(self, other):
		polygon = self.to_geom()
		if isinstance(other, LineString) or isinstance(other, Point) or isinstance(other, LineSegment):
			other = other.to_geom()
		
		ret = polygon.intersection(other)
		if ret.is_empty:
			return None

		if ret.geom_type == "LineString":
			if len(ret.coords) > 2:			ret = LineString([[c[0], c[1], c[2] if len(c) > 1 else 0] for c in ret.coords])
			else:							ret = LineSegment(Point(ret.coords[0]), Point(ret.coords[1]))
		elif ret.geom_type == "Point":		ret = Point(ret.x, ret.y, ret.z if ret.has_z else 0)
		return ret
	def to_geom(self):
		return geom.Polygon(self.coords)
	@property
	def normal(self):
		segments = self.segments
		unit = None
		for i in range(1, len(segments)):
			v = -segments[i-1].vector**segments[i].vector
			if v.norm:
				unit = v.unit
				break
		return unit
	def to_list(self, join = None):
		data = [list(c) + [join] if join else list(c) for c in self.coords]
		data.append(data[0])
		return data

class Section:
	def __init__(self, data):
		self._name = data[0]
		self._point = Point(*data[1:4])
		self._length = data[4]
		self._towards = data[5]
		self._away = data[6]
		self._azimuth = Angle(data[7])
		self._inclination = Angle(data[8])
		self._vector = Vector.from_angles(90-self._azimuth, 90-self._inclination).unit
		self._step = data[9]

	@property
	def center_plane(self):
		return Plane(self._vector, self._point)
	@property
	def towards_plane(self):
		return Plane(self._vector, self._point + self._vector*self._towards)
	@property
	def away_plane(self):
		return Plane(self._vector, self._point - self._vector*self._away)
	@property
	def name(self):					return self._name
	@property
	def center(self):				return self._point
	@property
	def length(self):				return self._length
	@property
	def towards(self):			return self._towards
	@property
	def away(self):					return self._away
	@property
	def azimuth(self):			return self._azimuth
	@property
	def inclination(self):			return self._inclination
	@property
	def step(self):					return self._step
	@property
	def vector(self):				return self._vector

	def is_parallel(self, other):
		if isinstance(other, Section):
			other = other.center_plane
		return self.center_plane.is_parallel(other)

	def offset(self, point, distance, direction = 1):
		sign = 1 if direction > 0 else -1
		x = point.x + distance*sign*math.cos(math.radians(-self.azimuth))
		y = point.y + distance*sign*math.sin(math.radians(-self.azimuth))
		z = point.x
		return Point(x, y, z)

