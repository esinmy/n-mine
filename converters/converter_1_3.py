﻿
import os, sys
from shutil import copyfile

#from mm.mmutils import Tridb
from utils.fsettings import FormSet
from utils.constants import *
import sqlite3, shutil
from utils.logger import  NLogger

LOG_INJECT_THIS_MODULE_ALLOW = True
logger = NLogger()



from __init__ import __version__  as nm_version, __settings_version__ as nm_settings_version

CONVERTER_VERSION = '1.3'
CONVERTER_SETTINGS_OUTDATED_VERSION = '1.1'
CONVERTER_SETTINGS_UPGRADE_VERSION = '1.3'

MASTER_SETTINGS_PATH = ".\\Converters\\MasterSettings_1_3.nmdb"

class FormSet_Update(FormSet):
	def __init__(self, settings_path = SETTINGS_PATH):
		super(FormSet_Update, self).__init__(ID_SETTINGS, settings_path)

		self._setid_list = self.__get_setid_list()

	def open(self, setId):
		
		cmd = "select prmName, prmValue from FLDVALUE where cmdId = %d and setId = %d" % (self._cmdId, setId)
		self.exec_query(cmd)

		data = self._cursor.fetchall()
		try:
			self._params = {} if not data else dict(data)	#{prmName: pickle.loads(prmValue) for prmName, prmValue in data}
		except Exception as e:
			raise Exception("Невозможно считать форму. %s" % str(e))
	def __get_setid_list(self):
		cmd = "select setId from FLDVALUE where cmdId = %d group by setId" % (self._cmdId)
		try:
			self.exec_query(cmd)
			data = self._cursor.fetchall()
		except Exception as e:
			raise Exception("Невозможно выполнить запрос. %s" % str(e))
		return [row[0] for row in data]

	def update_all_sets_settings(self, splash):
		for setid in self._setid_list:
			current_set_version = self.__getVersion(setid)
			if current_set_version != CONVERTER_SETTINGS_OUTDATED_VERSION:
				logger.info('SetId - {0} - v.{1} - Settings version is not operated by this converter.'.format(setid, current_set_version))
				print ('SetId - {0} - v.{1} - Settings version is not operated by this converter.'.format(setid, current_set_version)) 
				splash.showMessage ('SetId - {0} - v.{1} - Settings version is not operated by this converter.'.format(setid, current_set_version)) 
				continue

			self.open(setid)
			
			_value_ID_RPT_DSN_INDEXES = self.getValue(ID_RPT_DSN_INDEXES)
			if _value_ID_RPT_DSN_INDEXES:
				if not self.__check_param_ID_RPT_DSN_INDEXES(_value_ID_RPT_DSN_INDEXES):
					logger.info	('SetId - {0} - v.{1}, - Not possible to update'.format(setid, current_set_version))
					print ('SetId - {0} - v.{1}, - Not possible to update'.format(setid, current_set_version))
					splash.showMessage ('SetId - {0} - v.{1}, - Not possible to update'.format(setid, current_set_version))
					continue

				self.__set_new_param_ID_RPT_DSN_INDEXES(_value_ID_RPT_DSN_INDEXES)
			else:
				_value_ID_RPT_DSN_INDEXES = {}
				
			self.__setVersion(setid, CONVERTER_SETTINGS_UPGRADE_VERSION)
			logger.info	('SetId - {0} - v.{1} - Ok'.format(setid, current_set_version))
			print ('SetId - {0} - v.{1} - Ok'.format(setid, current_set_version))
			splash.showMessage ('SetId - {0} - v.{1} - Ok'.format(setid, current_set_version))
			self.save(None, setid)
					

			#else:
			#	logger.info	('SetId - {0} - v.{1} - Not possible to get ID_RPT_DSN_INDEXES branch'.format(setid, current_set_version))
			#	print ('SetId - {0} - v.{1} - Not possible to get ID_RPT_DSN_INDEXES branch'.format(setid, current_set_version))
			#	splash.showMessage ('SetId - {0} - v.{1} - Not possible to get ID_RPT_DSN_INDEXES branch'.format(setid, current_set_version))


			

	def __check_param_ID_RPT_DSN_INDEXES(self, _value_ID_RPT_DSN_INDEXES):

		if not isinstance(_value_ID_RPT_DSN_INDEXES, list):
			return
		for category in _value_ID_RPT_DSN_INDEXES:
			if len(category) != 3:
				return
			if not isinstance(category, list):
				return

			if not isinstance(category[2], dict):
				return

		return True

	def __set_new_param_ID_RPT_DSN_INDEXES(self, _value_ID_RPT_DSN_INDEXES):
		for category in _value_ID_RPT_DSN_INDEXES:
			category.insert(2, 0)

	def __getVersion(self, setId):
		cmd = "select version from PRMSETS where setId = '%s' and cmdId = %d" % (setId, self._cmdId)
		self.exec_query(cmd)
		result = self._cursor.fetchone()
		result = result["version"] if result else None
		return result

	def __setVersion(self, setId, value):
		cmd = """update PRMSETS set
					version = "{2}"
				where
					cmdId = {0} and	setId = {1}
		""".format(self._cmdId, setId, value)

		self.exec_query(cmd)
		
		return value

	def update_stored_settings_by_master_set(self, formSet_Master, list_of_ids, splash):

		formSet_Master_setid_list = formSet_Master.__get_setid_list()
		master_settings = [id for id in formSet_Master_setid_list if id != 0]
		for master_setid in master_settings:
			try:
				formSet_Master.open(master_setid)
			except:
				logger.info ('MasterSetId - {0} - Not possible to load.'.format(master_setid)) 
				print ('MasterSetId - {0} - Not possible to load.'.format(master_setid)) 
				splash.showMessage ('MasterSetId - {0} - Not possible to load.'.format(master_setid))  
				continue

			master_PROJNAME = formSet_Master.getValue(ID_PROJNAME)
			
			dict_of_up_vals = {}
			for val_id in list_of_ids:
				dict_of_up_vals[val_id] = formSet_Master.getValue(val_id)

			stored_settings = [id for id in self._setid_list]
			for setid in stored_settings:
				current_set_version = self.__getVersion(setid)
				if current_set_version != CONVERTER_SETTINGS_UPGRADE_VERSION:
					logger.info	('SetId - {0} - v.{1} - Settings version is not operated by this converter.'.format(setid, current_set_version)) 
					print ('SetId - {0} - v.{1} - Settings version is not operated by this converter.'.format(setid, current_set_version)) 
					splash.showMessage('SetId - {0} - v.{1} - Settings version is not operated by this converter.'.format(setid, current_set_version)) 
					continue

				self.open(setid)
				client_PROJNAME = self.getValue(ID_PROJNAME)
				if client_PROJNAME == master_PROJNAME:
					for val_id in list_of_ids:
						self.setValue(val_id, dict_of_up_vals[val_id])

					self.save(None, setid)

					logger.info	('SetId - {0} - v.{1} - Settings values were updated by set {2}.'.format(setid, current_set_version, master_setid)) 
					print ('SetId - {0} - v.{1} - Settings values were updated by set {2}.'.format(setid, current_set_version, master_setid)) 
					splash.showMessage('SetId - {0} - v.{1} - Settings values were updated by set {2}.'.format(setid, current_set_version, master_setid))
				else:
					logger.info	('SetId - {0} - v.{1} - Settings not equals target project {2}.'.format(setid, current_set_version, master_PROJNAME)) 
					print ('SetId - {0} - v.{1} - Settings not equals target project {2}.'.format(setid, current_set_version, master_PROJNAME)) 
					splash.showMessage('SetId - {0} - v.{1} - Settings not equals target project {2}.'.format(setid, current_set_version, master_PROJNAME))


				

		formSet_Master.close()



def start (splash):

	# сохранение предыдущей версии настроек
	root, ext = os.path.splitext(SETTINGS_PATH)
	old_version = CONVERTER_SETTINGS_OUTDATED_VERSION if not CONVERTER_SETTINGS_OUTDATED_VERSION is None else '0' 
	arch_path = root+'_bkp1_'+old_version+ext
	if os.path.exists(arch_path):
		arch_path = root+'_bkp2_'+old_version+ext
	copyfile(SETTINGS_PATH, arch_path)

	#
	cnxn = sqlite3.connect(SETTINGS_PATH)

	cmd = """
	SELECT * FROM sqlite_master WHERE type='table' and tbl_name = 'PRMSETS' and instr( sql, 'version TEXT') > 0
	"""

	cursor = cnxn.cursor()
	cursor.execute(cmd)
	data = cursor.fetchone()
	cnxn.commit()
	if data is None:
		cmd = """
		ALTER TABLE PRMSETS ADD version TEXT
		"""
		cnxn.execute(cmd)
		cnxn.commit()
	cnxn.close()
	logger.info	("Конвертация настроек. Версии: Конвертер - {0}, Настройки: из {1} в {2}".format(CONVERTER_VERSION, CONVERTER_SETTINGS_OUTDATED_VERSION,CONVERTER_SETTINGS_UPGRADE_VERSION))
	print("Конвертация настроек. Версии: Конвертер - {0}, Настройки: из {1} в {2}".format(CONVERTER_VERSION, CONVERTER_SETTINGS_OUTDATED_VERSION,CONVERTER_SETTINGS_UPGRADE_VERSION))
	splash.showMessage ("Конвертация настроек. Версии: Конвертер - {0}, Настройки: из {1} в {2}".format(CONVERTER_VERSION, CONVERTER_SETTINGS_OUTDATED_VERSION,CONVERTER_SETTINGS_UPGRADE_VERSION))


	formSet_Update = FormSet_Update()

	# конвертация структуры
	logger.info	("Конвертация структуры")
	print("Конвертация структуры")
	splash.showMessage("Конвертация структуры")

	formSet_Update.update_all_sets_settings(splash)

	


	# запись значений по умолчанию
	logger.info("Запись значений по умолчанию")
	print("Запись значений по умолчанию")
	splash.showMessage("Запись значений по умолчанию")

	if os.path.exists(MASTER_SETTINGS_PATH):
		formSet_Master = FormSet_Update(MASTER_SETTINGS_PATH)

		# Список параметров для импорта в настройки пользователей
		list_of_ids = [ID_RPT_DSN_INDEXES, ID_SUBXGEO, ID_SUBYGEO, ID_SUBZGEO, ID_SUBXSVY, ID_SUBYSVY, ID_SUBZSVY, ID_SUBXDSN, ID_SUBYDSN, ID_SUBZDSN]

		formSet_Update.update_stored_settings_by_master_set(formSet_Master, list_of_ids,splash)
	else: 
		logger.info	("Не найден файл мастер настроек - {0}. Запись настроек по-умолчанию будет пропущена.".format(MASTER_SETTINGS_PATH))
		print ("Не найден файл мастер настроек - {0}. Запись настроек по-умолчанию будет пропущена.".format(MASTER_SETTINGS_PATH))
		splash.showMessage ("Не найден файл мастер настроек - {0}. Запись настроек по-умолчанию будет пропущена.".format(MASTER_SETTINGS_PATH))

	formSet_Update.close()
	formSet_Master.close()
	


if __name__ == "__main__":
	start()
	input("Press enter to exit ")
	
	
		

